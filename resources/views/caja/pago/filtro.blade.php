<div class="col-md-2 col-sm-2 col-xs-12 form-group">
    {!! Form::label('fecha', 'Fecha Inicio') !!}
    {!! Form::date('fechafiltroinicio', date('Y-m-d'), ['class' => 'form-control', 'id' => 'fechafiltroinicio']) !!}
</div>
<div class="col-md-2 col-sm-2 col-xs-12 form-group">
    {!! Form::label('fecha', 'Fecha Fin') !!}
    {!! Form::date('fechafiltrofin', date('Y-m-d'), ['class' => 'form-control', 'id' => 'fechafiltrofin']) !!}
</div>
@if(count($proveedor) > 0)
<div class="col-md-2 col-sm-2 col-xs-12 form-group">
	{!! Form::label('proveedores', 'Proveedores') !!}
    {!! Form::select('proveedorfiltro', $proveedor, null, ['class' => 'form-control selectpicker', 'id' => 'proveedorfiltro', 'data-live-search' => true, 'required' => 'required']) !!}
</div>
@endif
<div class="col-md-2 col-sm-2 col-xs-12 form-group">
    {!! Form::label('guia', 'Guia') !!}
    {!! Form::text('guiafiltro', '', ['class' => 'form-control', 'id' => 'guiafiltro']) !!}
</div>
<div class="col-md-3 col-sm-3 col-xs-12 form-group">
	<button type="button" class="btn btn-success btn-search pull-right"><i class="fa fa-search"></i></button>
	<!-- <button type="button" class="btn btn-danger btn-send-cloud pull-right"><i class="fa fa-cloud-upload"></i></button> -->
</div>