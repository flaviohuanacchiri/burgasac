<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="modal-detalle">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">


      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Registro de Pagos</h4>
      </div>


      <div class="modal-body">
        <div class="row-fluid">
          <div class="col-md-12">
            @include('caja/pago/form')
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="ln_solid"></div>
            <table class="table table-striped table-bordered dt-responsive nowrap" id="modal-detalle-table"  cellspacing="0" width="100%">
                <thead>
                  <tr>
                      <th>F. Registro</th>
                      <th>T.Comprobante</th>
                      <th>Comprobante</th>
                      <th>Moneda</th>
                      <th>Monto</th>
                      <th>[]</th>
                  </tr>
                </thead>
            </table>

      </div>
      <div class="modal-footer">
        <!-- 
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('master.back') }}</button>
        <button type="button" class="btn btn-primary">{{ trans('master.add') }}</button>
        -->
      </div>

    </div>
  </div>
</div>