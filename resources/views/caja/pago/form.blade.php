<div class="col-md-4 col-sm-4 col-xs-12 form-group">
    {!! Form::label('fecha', 'Fecha') !!}
    {!! Form::date('fechapago', date('Y-m-d'), ['class' => 'form-control', 'id' => 'fechapago']) !!}
</div>
<div class="col-md-4 col-sm-4 col-xs-12 form-group">
    {!! Form::label('proveedor', 'Proveedor') !!}
    {!! Form::text('proveedor', '', ['class' => 'form-control', 'id' => 'proveedorpago', 'disabled' => true]) !!}
</div>
<div class="col-md-4 col-sm-4 col-xs-12 form-group">
    {!! Form::label('guia', 'Guia') !!}
    {!! Form::text('guia', '', ['class' => 'form-control', 'id' => 'guiapago', 'disabled' => true]) !!}
</div>
<div class="col-md-4 col-sm-4 col-xs-12 form-group">
	{!! Form::label('tipocomprobante', 'T. Comprobante') !!}
    {!! Form::select('comprobantepago', ['' => 'Elige', 1 => 'Boleta', 2 => 'Factura'], null, ['class' => 'form-control selectpicker', 'id' => 'comprobantepago', 'data-live-search' => true, 'required' => 'required']) !!}
</div>
<div class="col-md-4 col-sm-4 col-xs-12 form-group">
    {!! Form::label('nrocomprobante', '# Comprobante') !!}
    {!! Form::text('nrocomprobante', '', ['class' => 'form-control', 'id' => 'nrocomprobante']) !!}
</div>
<div class="clearfix"></div>
<div class="ln_solid"></div>
<div class="col-md-3 col-sm-3 col-xs-12 form-group">
    {!! Form::label('monto', 'Monto') !!}
    {!! Form::text('montopago', '', ['class' => 'form-control', 'id' => 'montopago', 'disabled' => true]) !!}
</div>
<div class="col-md-3 col-sm-3 col-xs-12 form-group">
    {!! Form::label('saldo', 'Saldo') !!}
    {!! Form::text('saldopago', '', ['class' => 'form-control', 'id' => 'saldopago', 'disabled' => true]) !!}
</div>
<div class="col-md-3 col-sm-3 col-xs-12 form-group">
    {!! Form::label('moneda', 'Moneda') !!}
    {!! Form::select('monedapago', ['' => 'Elige', 1 => 'Soles(s/.)', 2 => 'Dolares(USD)'], null, ['class' => 'form-control selectpicker', 'id' => 'monedapago', 'data-live-search' => true, 'required' => 'required']) !!}
</div>
<div class="col-md-3 col-sm-3 col-xs-12 form-group">
    {!! Form::label('montopagar', 'Monto a Pagar') !!}
    {!! Form::text('montopagar', '', ['class' => 'form-control onlynumbers', 'id' => 'montopagar']) !!}
</div>
<div class="col-md-3 col-sm-3 col-xs-12 form-group">
    <button type="button" class="btn btn-primary btn-pagar pull-right">Pagar</button>
</div>