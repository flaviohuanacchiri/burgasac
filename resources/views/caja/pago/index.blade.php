@extends('backend.layouts.appv2')
<link href="{{ asset("css/sweetalert2.min.css") }}" rel="stylesheet">
@section('content')
      <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Bandeja de saldos de Servicios y Tintorería</div>
                    <div class="panel-body">
                      <div>
                        @include('caja/pago/filtro')
                      </div>
                      <div>
                        <table class="table table-striped table-bordered dt-responsive nowrap" id="table-reporte"  cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Fecha</th>
                              <th>Proveedor</th>
                              <th>Color</th>
                              <th>Guia</th>
                              <th>T. Servicio</th>
                              <th>Color</th>
                              <th>Moneda</th>
                              <th>Monto</th>
                              <th>Saldo</th>
                              <th>[]</th>                           
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     @include('caja/pago/form_detalle')
@endsection
@push('scripts')
{{ Html::script('plugins/sweetalert/sweetalert.min.js') }}
{{ Html::script('js/caja/pago.js') }}
@endpush
