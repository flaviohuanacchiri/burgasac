@extends('backend.layouts.appv2')

@section('after-styles')
    <style type="text/css">
        #select_accesorio{display: none;}
    </style>
@stop

@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">DESPACHO A TERCEROS</div>
                    <div class="panel-body">

                        <!--
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        -->

                        {!! Form::model([], [
                            'method' => 'POST',
                            'url' => '/despacho-terceros/despacho-terceros',
                            'class' => 'form-horizontal',
                            'id' => 'planeamiento-form'
                        ]) !!}

                        @include ('despacho-terceros.form', ['submitButtonText' => 'Actualizar'])

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">

                                <a href="{{ url('despacho-terceros/despacho-terceros') }}" class="btn btn-warning">Cancelar</a>

                                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Guardar despacho de terceros', ['class' => 'btn btn-primary']) !!}

                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
@endsection

@section('after-scripts')
    <script>
        $(function() {

            




            /* planeamiento action */
            var i = 0;


            function add_hidden_button(j, fieldname, value) {
                return '<input type="hidden" name="detalles[' + j + '][' + fieldname + ']" value="' + value + '">';
            }
            $('#compra-form select#proveedor').attr('disabled', true);
            function add_hidden_button(j, fieldname, value) {
                return '<input type="hidden" name="detalles[' + j + '][' + fieldname + ']" value="' + value + '">';
            }
            $('#add_to_grid').click(function () {

                var fecha_registro = $("[name='fecha']").val(),
                    producto = $("[name='producto']").val(),
                    color = $("[name='color']").val(),
                    proveedor = $("[name='proveedor']").val(),
                    rollos = $("[name='rollos']").val(),
                    kg = $("[name='kg']").val(),
                    nrolote = $("[name='nrolote']").val();

                var cod = producto + proveedor;
                if (fecha_registro!='' && producto != '' && color != '' && rollos != '' && kg != '' && proveedor != '')
                {
                    //if(productos_in_details().indexOf(cod)>=0) return Mensaje.alerta('Ya existe un trabajador con esa maquina y turno');

                    $('#despachos_grid tbody tr:last').after('<tr>\
                        <td>' + add_hidden_button(i, 'fecha', fecha_registro) + fecha_registro + '</td>\
                        <td class="proveedor">' +  add_hidden_button(i, 'proveedor', proveedor) + $("[name='proveedor'] option:selected").text() + '</td>\
                        <td class="producto">' +  add_hidden_button(i, 'producto', producto) + $("[name='producto'] option:selected").text() + '</td><td>' +  add_hidden_button(i, 'kg', kg) + $("[name='kg']").val() + '</td>\
                        <td>' +  add_hidden_button(i, 'rollos', rollos) + add_hidden_button(i, 'nro_lote', nrolote) + $("[name='rollos']").val() + '</td>\
                        <td><a class="eliminar" style="cursor:pointer" data-position="'+i+'"><i class="fa fa-remove"></i></a>'
                        + '</td></tr>'
                    );
                    if (typeof detalleTintoreria[producto] == "undefined" || typeof detalleTintoreria[producto] == undefined) {
                      detalleTintoreria[producto] = {};
                    }
                    if (typeof detalleTintoreria[producto][nrolote] == "undefined" || typeof detalleTintoreria[producto][nrolote] == undefined) {
                      detalleTintoreria[producto][nrolote] = {peso : cantidad_max, rollos : rollos_max};
                    }
                    detalleTintoreria[producto][nrolote].peso = parseFloat(detalleTintoreria[producto][nrolote].peso) - parseFloat($("[name=kg]").val());
                    detalleTintoreria[producto][nrolote].rollos = parseFloat(detalleTintoreria[producto][nrolote].rollos) - parseFloat($("[name=rollos]").val());

                    $("[name=kg]").val(detalleTintoreria[producto][nrolote].peso);
                    $("[name=rollos]").val(detalleTintoreria[producto][nrolote].rollos);
                    i++;
                }
                else
                {
                    Mensaje.alerta('Para agregar al detalle complete los campos requeridos:\
                        \n- Fecha Registro\
                        \n- Número de Lote\
                        \n- Producto\
                        \n- Marca\
                        \n- Titulo\
                        \n- Peso Bruto\
                        \n- Peso Tara\
                        \n- Cantidad de Caja/Bolsas')
                }
                

                return false;
            });

            /* actualizar peso_neto */
            $('#compras_grid tbody tr td.show_peso_bruto input').on('keyup', function() {
                show_peso_bruto = $(this).val();
                show_peso_tara = $(this).parent().parent().find('td.show_peso_tara input').val();
                show_peso_neto = show_peso_bruto - show_peso_tara;
                $(this).parent().parent().find('td.show_peso_neto').html(show_peso_neto);
            });

            $('#compras_grid tbody tr td.show_peso_tara input').on('keyup', function() {
                show_peso_tara = $(this).val();
                show_peso_bruto = $(this).parent().parent().find('td.show_peso_bruto input').val();
                show_peso_neto = show_peso_bruto - show_peso_tara;
                show_peso_neto = parseFloat(Math.round( show_peso_neto * 100) / 100).toFixed(2);
                $(this).parent().parent().find('td.show_peso_neto').html(show_peso_neto);
            });

            /* eliminar tr */
            $('body').on('click', 'a.eliminar', function () {
                let indicegenerado = $(this).data("position");
                let productofila = "";
                let lotefila = "";
                let pesofila = 0;
                let rollofila = 0;
                $(this).closest('tr').find("input[type=hidden]").each(function() {
                    let name = $(this).attr("name");
                    switch(name) {
                      case "detalles["+indicegenerado+"][producto]":
                        productofila = $(this).val();
                        break;

                      case "detalles["+indicegenerado+"][nro_lote]":
                        lotefila = $(this).val();
                        break;
                      case "detalles["+indicegenerado+"][kg]":
                        pesofila = $(this).val();
                        break;
                      case "detalles["+indicegenerado+"][rollos]":
                        rollofila = $(this).val();
                        break;
                      default:
                        break;
                    }
                });

                detalleTintoreria[productofila][lotefila].peso = parseFloat(pesofila) + parseFloat(detalleTintoreria[productofila][lotefila].peso);
                detalleTintoreria[productofila][lotefila].rollos = parseFloat(rollofila) + parseFloat(detalleTintoreria[productofila][lotefila].rollos);
                //detalleTintoreria[producto][nro_lote].peso -= $("[name=kg]").val();
                //detalleTintoreria[producto][nro_lote].rollos -=$("[name=rollos]").val();

                $("[name=kg]").val(detalleTintoreria[productofila][lotefila].peso);
                $("[name=rollos]").val(detalleTintoreria[productofila][lotefila].rollos);
                $(this).parent().parent().remove();
                i--;
            });

        });
    </script>
@stop
@push('scripts')
<script type="text/javascript">
    let detalleTintoreria = {};
</script>
{{ Html::script('js/procesos/despacho_tercero.js') }}
@endpush