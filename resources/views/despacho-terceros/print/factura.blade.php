<?php
/**
 * Clase que implementa un conversor de números a letras. 
 * @author AxiaCore S.A.S
 *
 */
class NumberToLetterConverter {
  private $UNIDADES = array(
        '',
        'UN ',
        'DOS ',
        'TRES ',
        'CUATRO ',
        'CINCO ',
        'SEIS ',
        'SIETE ',
        'OCHO ',
        'NUEVE ',
        'DIEZ ',
        'ONCE ',
        'DOCE ',
        'TRECE ',
        'CATORCE ',
        'QUINCE ',
        'DIECISEIS ',
        'DIECISIETE ',
        'DIECIOCHO ',
        'DIECINUEVE ',
        'VEINTE '
  );
  private $DECENAS = array(
        'VEINTI',
        'TREINTA ',
        'CUARENTA ',
        'CINCUENTA ',
        'SESENTA ',
        'SETENTA ',
        'OCHENTA ',
        'NOVENTA ',
        'CIEN '
  );
  private $CENTENAS = array(
        'CIENTO ',
        'DOSCIENTOS ',
        'TRESCIENTOS ',
        'CUATROCIENTOS ',
        'QUINIENTOS ',
        'SEISCIENTOS ',
        'SETECIENTOS ',
        'OCHOCIENTOS ',
        'NOVECIENTOS '
  );
  private $MONEDAS = array(
    array('country' => 'Colombia', 'currency' => 'COP', 'singular' => 'PESO COLOMBIANO', 'plural' => 'PESOS COLOMBIANOS', 'symbol', '$'),
    array('country' => 'Estados Unidos', 'currency' => 'USD', 'singular' => 'DÓLAR', 'plural' => 'DÓLARES', 'symbol', 'US$'),
    array('country' => 'El Salvador', 'currency' => 'USD', 'singular' => 'DÓLAR', 'plural' => 'DÓLARES', 'symbol', 'US$'),
    array('country' => 'Europa', 'currency' => 'EUR', 'singular' => 'EURO', 'plural' => 'EUROS', 'symbol', '€'),
    array('country' => 'México', 'currency' => 'MXN', 'singular' => 'PESO MEXICANO', 'plural' => 'PESOS MEXICANOS', 'symbol', '$'),
    array('country' => 'Perú', 'currency' => 'PEN', 'singular' => 'NUEVO SOL', 'plural' => 'NUEVOS SOLES', 'symbol', 'S/'),
    array('country' => 'Reino Unido', 'currency' => 'GBP', 'singular' => 'LIBRA', 'plural' => 'LIBRAS', 'symbol', '£'),
    array('country' => 'Argentina', 'currency' => 'ARS', 'singular' => 'PESO', 'plural' => 'PESOS', 'symbol', '$')
  );
    private $separator = '.';
    private $decimal_mark = ',';
    private $glue = ' CON ';
    /**
     * Evalua si el número contiene separadores o decimales
     * formatea y ejecuta la función conversora
     * @param $number número a convertir
     * @param $miMoneda clave de la moneda
     * @return string completo
     */
    public function to_word($number, $miMoneda = null) {
        if (strpos($number, $this->decimal_mark) === FALSE) {
          $convertedNumber = array(
            $this->convertNumber($number, $miMoneda, 'entero')
          );
        } else {
          $number = explode($this->decimal_mark, str_replace($this->separator, '', trim($number)));
          $convertedNumber = array(
            $this->convertNumber($number[0], $miMoneda, 'entero'),
            $this->convertNumber($number[1], $miMoneda, 'decimal'),
          );
        }
        return implode($this->glue, array_filter($convertedNumber));
    }
    /**
     * Convierte número a letras
     * @param $number
     * @param $miMoneda
     * @param $type tipo de dígito (entero/decimal)
     * @return $converted string convertido
     */
    public function convertNumber($number, $miMoneda = null, $type) {   
        $decimales="";
        if(strpos($number,'.')!="")
        {
            $aux_x=trim(substr($number, strpos($number,'.')+1));
            $decimales=" Y ".((strlen($aux_x)==1)?$aux_x."0":$aux_x)."/100";        
            $number=trim(substr($number, 0, strpos($number,'.')));
        }
        $converted = '';
        if ($miMoneda !== null) {
            try {
                
                $moneda = array_filter($this->MONEDAS, function($m) use ($miMoneda) {
                    return ($m['currency'] == $miMoneda);
                });
                $moneda = array_values($moneda);
                if (count($moneda) <= 0) {
                    throw new Exception("Tipo de moneda inválido");
                    return;
                }
                ($number < 2 ? $moneda = $moneda[0]['singular'] : $moneda = $moneda[0]['plural']);
            } catch (Exception $e) {
                echo $e->getMessage();
                return;
            }
        }else{
            $moneda = '';
        }
        if (($number < 0) || ($number > 999999999)) {
            return false;
        }
        $numberStr = (string) $number;
        $numberStrFill = str_pad($numberStr, 9, '0', STR_PAD_LEFT);
        $millones = substr($numberStrFill, 0, 3);
        $miles = substr($numberStrFill, 3, 3);
        $cientos = substr($numberStrFill, 6);
        if (intval($millones) > 0) {
            if ($millones == '001') {
                $converted .= 'UN MILLON ';
            } else if (intval($millones) > 0) {
                $converted .= sprintf('%sMILLONES ', $this->convertGroup($millones));
            }
        }
        
        if (intval($miles) > 0) {
            if ($miles == '001') {
                $converted .= 'MIL ';
            } else if (intval($miles) > 0) {
                $converted .= sprintf('%sMIL ', $this->convertGroup($miles));
            }
        }
        if (intval($cientos) > 0) {
            if ($cientos == '001') {
                $converted .= 'UN ';
            } else if (intval($cientos) > 0) {
                $converted .= sprintf('%s ', $this->convertGroup($cientos));
            }
        }
        //$converted .= $moneda;
        return $converted." ".$decimales." ".$moneda;
    }
    /**
     * Define el tipo de representación decimal (centenas/millares/millones)
     * @param $n
     * @return $output
     */
    private function convertGroup($n) {
        $output = '';
        if ($n == '100') {
            $output = "CIEN ";
        } else if ($n[0] !== '0') {
            $output = $this->CENTENAS[$n[0] - 1];   
        }
        $k = intval(substr($n,1));
        if ($k <= 20) {
            $output .= $this->UNIDADES[$k];
        } else {
            if(($k > 30) && ($n[2] !== '0')) {
                $output .= sprintf('%sY %s', $this->DECENAS[intval($n[1]) - 2], $this->UNIDADES[intval($n[2])]);
            } else {
                $output .= sprintf('%s%s', $this->DECENAS[intval($n[1]) - 2], $this->UNIDADES[intval($n[2])]);
            }
        }
        return $output;
    }
}
?>
    <style type="text/css">
       .contenedor-tabla
        {
            display: table;
            width: 105%;
            font-size: 15px;
        }

        .contenedor-fila
        {
            display: table-row;            
        }

        .contenedor-columna
        {
            display: table-cell;
            padding-top: 5px;
        }

        body{
            font-family: "Lucida Console", "Lucida Sans Typewriter", monaco, "Bitstream Vera Sans Mono", monospace;        
            font-weight:bold;
        }

        table, td, th {
            /*border: 1px solid black;*/            
            font-size: 15px;
            text-align: center;
            border: 1px solid white;            
        }

        table {
            border-collapse: collapse;
            width: 98%;
        }

        th {
            height: 20px;
            color: white;
        }
    </style>

<title>Factura</title>

<body style="margin-left: -25px;margin-right: 0px;">      
    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 10%;">
                <!--img src="img/logo.jpg" alt="Smiley face" height="50" width="50"-->
            </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;">
                <h3 style="margin: 0px;font-size: 20px;color:white;">TEXTILES</h3>
                <h1 style="margin: 0px;font-size: 25px;text-decoration: underline;color:white;">BURGA S.A.C.</h1>
                <h3 style="margin: 0px;font-size: 13px;color:white;">Jr. America N 472 lnt. S-104 - La Victoria</h3>
                <h3 style="margin: 0px;font-size: 13px;color:white;">Telf. 324-1224 Cel. 996342002</h3>             
            </div>   
            <div class="contenedor-columna" style="width:30%;border:2px solid white;border-radius: 7px;text-align: center;height: 80px;padding-top: 15px;" >
               <label style="color:white;">RUC. 20511794553</label><br><br>
               <label style="font-weight: bold;color:white;">FACTURA</label><br><br>
               <label style="color:white;">{{$vfacturas->cFacNumFac}}</label><br><br>
            </div>                                          
        </div>
    </div>
    <br>
    <br>
    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 15%;">
     
             </div>
            <div class="contenedor-columna" style="width: 60%; font-style: italic;">
                {{strtoupper($vfacturas["proveedor"]->nombre_comercial)}}
            </div>
            <div class="contenedor-columna" style="width: 25%; font-style: italic;">
               
            </div>
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 15%;">
          
             </div>
            <div class="contenedor-columna" style="width: 60%; font-style: italic;">
                {{$vfacturas["proveedor"]->ruc}}
            </div>
             <div class="contenedor-columna" style="width: 25%; font-style: italic;">
                
            </div>            
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna">
           
             </div>
            <div class="contenedor-columna" style="font-style: italic;">
                {{strtoupper($vfacturas["proveedor"]->direccion)}}
            </div>
        </div>      
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 15%;">
              <br>
            </div>
            <div class="contenedor-columna" style="width: 60%;">

            </div>
             <div class="contenedor-columna" style="width: 25%; font-style: italic;">
                
            </div>
        </div>
    </div>
    
    <div class="contenedor-tabla" style="border:1px solid white;border-radius: 3px;">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 25%;text-align: left;font-style: italic;">                
                

             </div>
            <div class="contenedor-columna" style="width: 20%;">
                 {{$vfacturas->nroguia}}
            </div>
            <div class="contenedor-columna" style="width: 25%;">
                 
            </div>
            <div class="contenedor-columna" style="width: 30%;text-align:center;">
                
            </div>
        </div>
    </div>

<!--div style="clear:both;"></div-->


    <br>    
    <div class="row">                                          
        <div class="col_x">
            <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
                <tr>
                    <th width="65px">
                        ITEM
                    </th>
                    <th width="60px">
                        CANT.
                    </th>  
                    <th width="60px">
                        MED.
                    </th>                                                               
                    <th width="130px">
                        DESCRIPCION
                    </th>
                    <th width="65px">
                        
                    </th>
                    <th colspan="2" width="60px">
                        P.UNIT.
                    </th>
                    <th colspan="2" width="70px">
                        IMPORTE
                    </th>
                </tr>
                <?php $i=0;
                    $total = 0;
                    $moneda = "";
                ?>
                @foreach($vfacturas->detalles as $dfac)
                 
                <?php
                    $objprecio = $dfac->getPrecio($vfacturas->proveedor_id, $vfacturas->id, $dfac->producto_id, $dfac->id);
                    $moneda = "";
                    if (!is_null($objprecio)) {
                        if ($objprecio->moneda_id == 2) {
                            $moneda = "USD";
                        }
                        if ($objprecio->moneda_id == 1) {
                            $moneda = "s/.";
                        }
                        $total+=$objprecio->total;
                    }
                ?>
                    <tr>                                                            
                        <td>
                            <?php 
                            ++$i;
                            echo $dfac->id;
                            ?>
                        </td>
                        <td style="text-align: center;">{{ number_format($dfac->cantidad,2) }}</td>
                        <td style="text-align: center;">KG</td>
                        <td style="text-align: left;">{{ $dfac->producto->nombre_generico }}</td>
                        <!--td>{{ $dfac->nVtaCant }}</td-->
                        <td width="50px"></td>                        
                        @if (!is_null($objprecio))
                            <td  style="border-right: 1px solid white;text-align: right;" width="auto">{{ $moneda }}</td> 
                            <td style="text-align: right;" width="auto">{{ number_format($objprecio->preciounitario,2) }}</td> 
                            <td style="border-right: 1px solid white;text-align: right;" width="auto">{{ $moneda }}</td> 
                            <td style="text-align: right;" width="auto">{{ number_format($objprecio->total,2) }}</td>
                        @else
                            <td  style="border-right: 1px solid white;text-align: right;" width="auto"></td> 
                            <td style="text-align: right;" width="auto"></td> 
                            <td style="border-right: 1px solid white;text-align: right;" width="auto"></td> 
                            <td style="text-align: right;" width="auto"></td>
                        @endif
                    </tr>
                @endforeach 
                @for ($j = ++$i; $j <=20; $j++)
                    <tr>                                                            
                        <td style="color:white;">{{$j}}</td>
                        <td></td>
                        <td></td>                        
                        <td width="50px"></td>
                        <td  style="border-right: 1px solid white;text-align: right;"></td> 
                        <td></td> 
                        <td style="border-right: 1px solid white;text-align: right;"></td> 
                        <td></td> 
                    </tr>
                @endfor
                <?php
                    $subtotal = $total*0.82;
                    $igv = $total*0.18;
                ?>
                    <tr height="15px">                                                            
                        <td colspan="5" style="border-bottom: 1px solid white;border-left: 1px solid white;">
                            <?php
                                $v=new NumberToLetterConverter();
                                //echo $v->convertNumber(1302587.25,"PEN","decimal");
                            ?>  
                            <label style="margin: 0px;font-size: 13px;text-align: left; color:white">Son: {{$v->convertNumber($total,"PEN","decimal")}}</label>  
                        </td>                                               
                        <th colspan="2">SUB TOTAL</th> 
                        <td style="border-right: 1px solid white;text-align: right;" width="auto">{{$moneda}}</td> 
                        <td style="text-align: right;" width="auto">{{ number_format($subtotal,2) }}</td> 
                    </tr>
                    <tr height="15px">                                                            
                        <td colspan="5" style="border-bottom: 1px solid white;border-left: 1px solid white;"></td>                     
                        <th colspan="2">IGV</th> 
                        <td style="border-right: 1px solid white;text-align: right;" width="auto">{{$moneda}}</td> 
                        <td style="text-align: right;" width="auto">{{ number_format($igv,2) }}</td> 
                    </tr>
                    <tr height="15px">                                                            
                        <td colspan="5" style="border-bottom: 1px solid white;border-left: 1px solid white;"></td>                       
                        <th colspan="2">TOTAL</th> 
                        <td style="border-right: 1px solid white;text-align: right;" width="auto">{{$moneda}}</td> 
                        <td style="text-align: right;" width="auto">{{ number_format($total,2) }}</td> 
                    </tr> 
                <?php ?>                                             
            </table>
        </div>
    </div>
    <br>
</body>
