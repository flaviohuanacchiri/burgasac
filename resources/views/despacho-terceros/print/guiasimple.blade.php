<style type="text/css">
    table{width: 100%; display: block;}
    th, td {width: 25%;}
    th{font-size: 9px;}
    h3, td, p, th {font-size: 9px; font-family: Arial;}
    thead { display: table-header-group }
    tfoot { display: table-row-group }
    tr { page-break-inside: avoid }
    h3, td, th{text-align: center;}
    td.montoapagar{width: 50%;}
    p{padding: 0px; margin: 0px; margin-bottom: 5px;}

</style>
<body>
    <div style="width: 500px;">
        <div>
            <img src="img/logo.jpg" alt="Smiley face" height="25" width="45" />
        </div>
        <p><b>Fecha: </b>{{date('Y-m-d', strtotime($despacho->fecha))}}</p>
        <h3>Despacho de Terceros - {{leadZero($despacho->id)}}</h3>
        <p><b>Proveedor: </b>{{$despacho->proveedor->nombre_comercial}}</p>
        <p><b>P. Llegada: </b>{{$despacho->direccionllegada}}</p>
        <div>
            <table>
                <thead>
                    <tr>
                        <th style='text-align:center; border: solid 1px; width: 30px; padding:5px; font-family: Arial'>Item</th>
                        <th style='text-align:center; border: solid 1px; width: 380px; padding:5px; font-family: Arial'>Producto</th>
                        <th style='text-align:center; border: solid 1px; width: 30px; padding:5px; font-family: Arial'>Lote</th>
                        <th style='text-align:center; border: solid 1px; width: 30px; padding:5px; font-family: Arial'>Rollos</th>
                        <th style='text-align:center; border: solid 1px; width: 30px; padding:5px; font-family: Arial'>Peso</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $i = 1;
                        $totalcantidad = 0;
                        $totalpeso = 0;
                    ?>
                    @foreach($despacho->detalles as $key => $value)
                        <tr>
                            <td style='text-align:center; width: 30px; padding:5px; font-family: Arial'>{{$i}}</td>
                            <td style='text-align:center; width: 30px; padding:5px; font-family: Arial'>{{$value->producto->nombre_generico}}</td>
                            <td style='text-align:center; width: 30px; padding:5px; font-family: Arial'>{{$value->nrolote}}</td>
                            <td style='text-align:center; width: 30px; padding:5px; font-family: Arial'>{{$value->rollos}}</td>
                            <td style='text-align:center; width: 30px; padding:5px; font-family: Arial'>{{number_format($value->cantidad, 2)}}</td>
                        </tr>
                    <?php
                        $totalpeso+=$value->cantidad;
                        $totalcantidad+=$value->rollos;
                        $i++;
                    ?>
                    @endforeach
                    <tr>
                        <td style='text-align:center; width: 30px; padding:5px; font-family: Arial'></td>
                        <td style='text-align:center; width: 30px; padding:5px; font-family: Arial'></td>
                        <td style='text-align:center; width: 30px; padding:5px; font-family: Arial; border: solid 1px;'><b>Total</b></td>
                        <td style='text-align:center; width: 30px; padding:5px; font-family: Arial; border: solid 1px;'>{{$totalcantidad}}</td>
                        <td style='text-align:center; width: 30px; padding:5px; font-family: Arial; border: solid 1px;'>{{number_format($totalpeso, 2)}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>
<?php dd();?>