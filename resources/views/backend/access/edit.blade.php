@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management') . ' | ' . trans('labels.backend.access.users.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.edit') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::model($user, ['route' => ['admin.access.user.update', $user], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.backend.access.users.edit') }}</h3>

                <div class="box-tools pull-right">
                    @include('backend.access.includes.partials.user-header-buttons')
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    {{ Form::label('name', trans('validation.attributes.backend.access.users.name'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.name')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('email', trans('validation.attributes.backend.access.users.email'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.email')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                @if ($user->id != 1)
                    <div class="form-group">
                        {{ Form::label('status', trans('validation.attributes.backend.access.users.active'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-1">
                            {{ Form::checkbox('status', '1', $user->status == 1) }}
                        </div><!--col-lg-1-->
                    </div><!--form control-->

                    <div class="form-group">
                        {{ Form::label('confirmed', trans('validation.attributes.backend.access.users.confirmed'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-1">
                            {{ Form::checkbox('confirmed', '1', $user->confirmed == 1) }}
                        </div><!--col-lg-1-->
                    </div><!--form control-->

                    <div class="form-group">
                        {{ Form::label('status', trans('validation.attributes.backend.access.users.associated_roles'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-3">
                            @if (count($roles) > 0)
                                @foreach($roles as $role)
                                    <input type="checkbox" value="{{$role->id}}" name="assignees_roles[{{ $role->id }}]" {{ is_array(old('assignees_roles')) ? (in_array($role->id, old('assignees_roles')) ? 'checked' : '') : (in_array($role->id, $user_roles) ? 'checked' : '') }} id="role-{{$role->id}}" /> <label for="role-{{$role->id}}">{{ $role->name }}</label>
                                        <a href="#" data-role="role_{{$role->id}}" class="show-permissions small">
                                            (
                                                <span class="show-text">{{ trans('labels.general.show') }}</span>
                                                <span class="hide-text hidden">{{ trans('labels.general.hide') }}</span>
                                                {{ trans('labels.backend.access.users.permissions') }}
                                            )
                                        </a>
                                    <br/>
                                    <div class="permission-list hidden" data-role="role_{{$role->id}}">
                                        @if ($role->all)
                                            {{ trans('labels.backend.access.users.all_permissions') }}<br/><br/>
                                        @else
                                            @if (count($role->permissions) > 0)
                                                <blockquote class="small">{{--
                                            --}}@foreach ($role->permissions as $perm){{--
                                            --}}{{$perm->display_name}}<br/>
                                                    @endforeach
                                                </blockquote>
                                            @else
                                                {{ trans('labels.backend.access.users.no_permissions') }}<br/><br/>
                                            @endif
                                        @endif
                                    </div><!--permission list-->
                                @endforeach
                            @else
                                {{ trans('labels.backend.access.users.no_roles') }}
                            @endif
                        </div><!--col-lg-3-->
                    </div><!--form control-->
                @endif


                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Perfil  
                    </label>
                    <div class="col-lg-5">                        
                        <select class="form-control" data-show-subtext="true" data-live-search="true" id="alm" name="alm" tabindex="2">  
                            <option value="">Seleccione Almacén...</option>                       
                            @foreach($alm as $a)
                                @if($a->nAlmCod==$codalm)
                                    <option value="{{$a->nAlmCod}}" selected>{{$a->cAlmNom}}</option>
                                @else
                                    <option value="{{$a->nAlmCod}}">{{$a->cAlmNom}}</option>
                                @endif
                            @endforeach                        
                        </select>
                    </div><!--col-lg-1-->
                    <div class="col-lg-5 {{ $errors->has('areas') ? 'has-error' :'' }}">                        
                        <select class="form-control" data-show-subtext="true" data-live-search="true" id="areas" name="areas" tabindex="2">  
                            <option value="">Seleccione Área...</option>                       
                            @foreach($areasX as $ax)
                                @if($ax->nAreCod==$codare)
                                    <option value="{{$ax->nAreCod}}" selected>{{$ax->areas->cAreNom}}</option>
                                @else
                                    <option value="{{$ax->nAreCod}}">{{$ax->areas->cAreNom}}</option>
                                @endif
                            @endforeach
                        </select>                        
                        <input type="hidden" name="codarerol" id="codarerol" value="{{$nRolAreCod}}">
                    </div><!--col-lg-1-->
                    {!! $errors->first('areas','<span class="help-block">:message</span>') !!} 
                </div><!--form control-->
                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Detalles del Área
                    </label>
                    <div class="col-lg-10">
                        <div class="row-fluid">                            
                              <div class="form-control" style="background-color: #f4f4f4;min-height: 100px; height: 200px; color:#3c8dbc;" id="dareas" name="dareas" readonly>
                                  @foreach($darea_x as $aax)
                                    <li> Módulo {{$aax->cNomMod}} : {{$aax->cDesMod}}</li>
                                  @endforeach
                              </div>                          
                        </div>
                    </div>                                  
                </div>


            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.access.user.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

        @if ($user->id == 1)
            {{ Form::hidden('status', 1) }}
            {{ Form::hidden('confirmed', 1) }}
            {{ Form::hidden('assignees_roles[]', 1) }}
        @endif

    {{ Form::close() }}
@stop

@section('after-scripts')
    {{ Html::script('js/backend/access/users/script.js') }}
@stop

@push('scripts')
<script type="text/javascript">

/***********************combo anidado almacén1 a almacén2 ***********************/
$("#alm").change(function(event){
    $("#areas").empty();    
    
    $.get("../almarea/"+event.target.value+"",function(response)
    {
        
        $("#areas").append("<option data-subtext='' value='' >Seleccione las áreas...</option>");           
        for(i=0;i<response.length;i++)
        {
            $("#areas").append("<option value='"+response[i].nAreCod+"' > "+response[i].cAreNom+"</option>");           
        }  
    })
});
/*******************************************************************************/

/***********************combo anidado producto color ***********************/
   $("#areas").change(function(event)
   {   
        $("#dareas").empty();       
        $.get("../detallearea/"+event.target.value,function(response)
        {       
            for(i=0;i<response.length;i++)
            {
                $("#dareas").append("<li> Módulo: "+response[i].cNomMod+
                        //(($.trim(response[i].url)!="")?" Url: "+response[i].url:"")+
                        (($.trim(response[i].cDesMod)!="")?" | Desc.: "+response[i].cDesMod:"")+"</li>");
            }  
        })
   });
/*******************************************************************************/

</script>
@endpush

 