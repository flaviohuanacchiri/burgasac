<style type="text/css">
    .contenedor-tabla, .contenedor-fila, .contenedor-columna {width: 100%;}
    .contenedor-tabla{display: table;width: 100%;font-size: 10px;}
    .contenedor-fila{display: table-row;}
    .contenedor-columna{display: table-cell; padding-top: 10px;}
    body{font-family: "Helvetica";font-weight:bold;}
    table, td, th {font-size: 10px;text-align: center;border: 1px solid white;}
    table {border-collapse: collapse;width: 100%;}
    th {height: 18px; color: white;}
    .cuerpo tr td {font-family: "Helvetica"; font-size: 9px; padding: -1px;}
</style>