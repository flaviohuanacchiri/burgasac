@extends('backend.layouts.appv2')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Crear Costo de Producto por Proveedor</div>
                    <div class="panel-body">

                        {!! Form::open(['url' => '/costo_proveedor_producto', 'class' => 'form-horizontal', 'files' => true]) !!}

                        @include ('master.costo_proveedor_producto.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
{{ Html::script('js/master/costo_proveedor_producto.js') }}
@endpush