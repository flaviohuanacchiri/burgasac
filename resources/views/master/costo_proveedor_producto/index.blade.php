@extends('backend.layouts.appv2')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Costo de Productos por Proveedor</div>
                    <div class="panel-body">

                        <a href="{{ url('/costo_proveedor_producto/create') }}" class="btn btn-primary btn-xs" title="Add New TiposPago"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered dt-responsive nowrap" id="table-color">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Proveedor</th>
                                        <th>Producto</th>
                                        <th>Titulo</th>
                                        <th>Costo</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if (count($data) > 0)
                                @foreach($data as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ isset($item['proveedor'])? $item['proveedor']->nombre_comercial : ''}}</td>
                                        <td>{{ isset($item['producto'])? $item['producto']->nombre_generico : ''}}</td>
                                        <td>{{ isset($item['titulo'])? $item['titulo']->nombre : ''}}</td>
                                        <td>
                                            @if ($item->moneda_id == 1)
                                                {{'s/.'.$item->precio}}
                                            @endif
                                            @if ($item->moneda_id == 2)
                                                {{'USD.'.$item->precio}}
                                            @endif
                                        </td>
                                        <td>
                                            <!--<a href="{{ url('/costo_proveedor_producto/' . $item->id) }}" class="btn btn-success btn-xs" title="View TiposPago"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>-->
                                            <a href="{{ url('/costo_proveedor_producto/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit TiposPago"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/costo_proveedor_producto', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Borrar Maquina" />', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Borrar Costo',
                                                        'onclick'=>'return confirm("Confirmar?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                @endif
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $data->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
{{ Html::script('js/master/costo_proveedor_producto.js') }}
@endpush