@extends('backend.layouts.appv2')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Costo Proveedor Producto {{ $obj->id }}</div>
                    <div class="panel-body">

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($obj, [
                            'method' => 'PATCH',
                            'url' => ['/costo_proveedor_producto', $obj->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('master.costo_proveedor_producto.form', ['submitButtonText' => 'Actualizar'])

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
{{ Html::script('js/master/costo_proveedor_producto.js') }}

<script type="text/javascript">
    let obj = <?php  echo json_encode($obj);?>;
    CostoProveedorProducto.getIndicadoresPorProducto(obj.producto_id);
</script>
@endpush