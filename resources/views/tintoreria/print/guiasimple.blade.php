<style type="text/css">
    th, td {}
    h3, td, th{text-align: center;}
    td.montoapagar{width: 50%;}
    p{padding: 0px; margin: 0px; margin-bottom: 5px;}
            body{
            /**font-family: monospace;*/
            //font-family: "Lucida Console", "Lucida Sans Typewriter", monaco, "Bitstream Vera Sans Mono", monospace; 
            font-family: "Helvetica";
            font-weight: 700;
            padding-left: 5px;
        }
    .contenedor-columna { display: table-cell; padding-top: 1px;}
    .contenedor-tabla {display: table; width: 83%; font-size: 11px;}
    .contenedor-fila {display: table-row;}
    td, th { border: 1px solid black; font-size: 12px; text-align: center;}
    table {border-collapse: collapse; width: 85%;}
    th {height: 5px;}
    .cuerpo, .cuerpo tr td {border: none;}
    .cuerpo tr td {padding: 0px;}
    td {border: none;}
</style>
<body style="margin-left: 4px;margin-right: 55px; margin-top: 5px;">
    <div>
        <div class="contenedor-tabla">
            <div class="contenedor-fila">
                <div class="contenedor-columna" style="width: 20%;text-align: center;padding-top: 20px;">
                    <img src="img/logo.jpg" alt="Smiley face" height="50" width="60">
                 </div>
                <div class="contenedor-columna" style="width: 60%;text-align: center;padding-top: 20px;padding-left: 10px; vertical-align: top;">
                    <h1 style="margin: 0px;font-size: 20px;">TEXTILES BURGA S.A.C.</h1>
                    <h3 style="margin: 0px;font-size: 12px;">JR.AMERICA N° 472 LNT. S-104 - LA VICTORIA</h3>
                     <h3 style="margin: 0px;font-size: 12px;">Telf. 324-1224 Cel. 996342002</h3> 
                </div>
                <div class="contenedor-columna" style="width: 20%;text-align: center;">
                </div>
            </div>
        </div>
        <div class="contenedor-tabla" style="margin-top: 5px;">
            <div class="contenedor-fila">
                <div class="contenedor-columna" style="width: 50%">
                    <b>Fecha: </b>{{date('Y-m-d', strtotime($despacho->fecha))}}
                </div>
                <div class="contenedor-columna" style="width: 50%">
                    Despacho de Tintorería - {{leadZero($despacho->id)}}
                </div>
            </div>
            <div class="contenedor-fila">
                <div class="contenedor-columna" style="width: 50%">
                    <b>Proveedor: </b><span style="text-transform: uppercase;">{{Config::get("app.name")}}</span>
                </div>
                <div class="contenedor-columna" style="width: 50%">
                    <b>Tintoreria: </b>{{$despacho->proveedor->nombre_comercial}}
                </div>
            </div>
            <div  class="contenedor-fila" style="width: 100%">
                <div class="contenedor-columna" style="">
                    <b>P. Llegada: </b>{{$despacho->direccion_llegada}}
                </div>
            </div>
        </div>
        <div style="margin-top: 5px;">
            <table>
                <thead>
                    <tr>
                        <th width="15px;">Item</th>
                        <th width="240px;">Producto</th>
                        <th>Lote</th>
                        <th>Color</th>
                        <th>Rollos</th>
                        <th>Peso</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $i = 1;
                        $totalcantidad = 0;
                        $totalpeso = 0;
                    ?>
                    @foreach($despacho->detalledespachotintoreria as $key => $value)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$value->producto->nombre_generico}}</td>
                            <td>{{$value->nro_lote}}</td>
                            <td>{{$value->color->nombre}}</td>
                            <td>{{$value->rollos}}</td>
                            <td>{{number_format($value->cantidad, 2)}}</td>
                        </tr>
                    <?php
                        $totalpeso+=$value->cantidad;
                        $totalcantidad+=$value->rollos;
                        $i++;
                    ?>
                    @endforeach
                    @for ($j = ++$i; $j <=14; $j++)
                    <tr>                                                            
                        <td style="color:white;">{{$j}}</td>
                        <td></td>
                        <td></td>
                        <td></td>                 
                        <td width="50px"></td>
                        <td  style="border-right: 1px solid white;text-align: right;"></td>
                    </tr>
                    @endfor
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="border: solid 1px;"><b>Total</b></td>
                        <td style="border: solid 1px;">{{$totalcantidad}}</td>
                        <td style="border: solid 1px;">{{number_format($totalpeso, 2)}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>