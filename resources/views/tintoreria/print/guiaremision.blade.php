<?php
/**
 * Clase que implementa un conversor de números a letras. 
 * @author AxiaCore S.A.S
 *
 */
class NumberToLetterConverter {
  private $UNIDADES = array(
        '',
        'UN ',
        'DOS ',
        'TRES ',
        'CUATRO ',
        'CINCO ',
        'SEIS ',
        'SIETE ',
        'OCHO ',
        'NUEVE ',
        'DIEZ ',
        'ONCE ',
        'DOCE ',
        'TRECE ',
        'CATORCE ',
        'QUINCE ',
        'DIECISEIS ',
        'DIECISIETE ',
        'DIECIOCHO ',
        'DIECINUEVE ',
        'VEINTE '
  );
  private $DECENAS = array(
        'VEINTI',
        'TREINTA ',
        'CUARENTA ',
        'CINCUENTA ',
        'SESENTA ',
        'SETENTA ',
        'OCHENTA ',
        'NOVENTA ',
        'CIEN '
  );
  private $CENTENAS = array(
        'CIENTO ',
        'DOSCIENTOS ',
        'TRESCIENTOS ',
        'CUATROCIENTOS ',
        'QUINIENTOS ',
        'SEISCIENTOS ',
        'SETECIENTOS ',
        'OCHOCIENTOS ',
        'NOVECIENTOS '
  );
  private $MONEDAS = array(
    array('country' => 'Colombia', 'currency' => 'COP', 'singular' => 'PESO COLOMBIANO', 'plural' => 'PESOS COLOMBIANOS', 'symbol', '$'),
    array('country' => 'Estados Unidos', 'currency' => 'USD', 'singular' => 'DÓLAR', 'plural' => 'DÓLARES', 'symbol', 'US$'),
    array('country' => 'El Salvador', 'currency' => 'USD', 'singular' => 'DÓLAR', 'plural' => 'DÓLARES', 'symbol', 'US$'),
    array('country' => 'Europa', 'currency' => 'EUR', 'singular' => 'EURO', 'plural' => 'EUROS', 'symbol', '€'),
    array('country' => 'México', 'currency' => 'MXN', 'singular' => 'PESO MEXICANO', 'plural' => 'PESOS MEXICANOS', 'symbol', '$'),
    array('country' => 'Perú', 'currency' => 'PEN', 'singular' => 'NUEVO SOL', 'plural' => 'NUEVOS SOLES', 'symbol', 'S/'),
    array('country' => 'Reino Unido', 'currency' => 'GBP', 'singular' => 'LIBRA', 'plural' => 'LIBRAS', 'symbol', '£'),
    array('country' => 'Argentina', 'currency' => 'ARS', 'singular' => 'PESO', 'plural' => 'PESOS', 'symbol', '$')
  );
    private $separator = '.';
    private $decimal_mark = ',';
    private $glue = ' CON ';
    /**
     * Evalua si el número contiene separadores o decimales
     * formatea y ejecuta la función conversora
     * @param $number número a convertir
     * @param $miMoneda clave de la moneda
     * @return string completo
     */
    public function to_word($number, $miMoneda = null) {
        if (strpos($number, $this->decimal_mark) === FALSE) {
          $convertedNumber = array(
            $this->convertNumber($number, $miMoneda, 'entero')
          );
        } else {
          $number = explode($this->decimal_mark, str_replace($this->separator, '', trim($number)));
          $convertedNumber = array(
            $this->convertNumber($number[0], $miMoneda, 'entero'),
            $this->convertNumber($number[1], $miMoneda, 'decimal'),
          );
        }
        return implode($this->glue, array_filter($convertedNumber));
    }
    /**
     * Convierte número a letras
     * @param $number
     * @param $miMoneda
     * @param $type tipo de dígito (entero/decimal)
     * @return $converted string convertido
     */
    public function convertNumber($number, $miMoneda = null, $type) {   
        $decimales="";
        if(strpos($number,'.')!="")
        {
            $aux_x=trim(substr($number, strpos($number,'.')+1));
            $decimales=" Y ".((strlen($aux_x)==1)?$aux_x."0":$aux_x)."/100";        
            $number=trim(substr($number, 0, strpos($number,'.')));
        }
        $converted = '';
        if ($miMoneda !== null) {
            try {
                
                $moneda = array_filter($this->MONEDAS, function($m) use ($miMoneda) {
                    return ($m['currency'] == $miMoneda);
                });
                $moneda = array_values($moneda);
                if (count($moneda) <= 0) {
                    throw new Exception("Tipo de moneda inválido");
                    return;
                }
                ($number < 2 ? $moneda = $moneda[0]['singular'] : $moneda = $moneda[0]['plural']);
            } catch (Exception $e) {
                echo $e->getMessage();
                return;
            }
        }else{
            $moneda = '';
        }
        if (($number < 0) || ($number > 999999999)) {
            return false;
        }
        $numberStr = (string) $number;
        $numberStrFill = str_pad($numberStr, 9, '0', STR_PAD_LEFT);
        $millones = substr($numberStrFill, 0, 3);
        $miles = substr($numberStrFill, 3, 3);
        $cientos = substr($numberStrFill, 6);
        if (intval($millones) > 0) {
            if ($millones == '001') {
                $converted .= 'UN MILLON ';
            } else if (intval($millones) > 0) {
                $converted .= sprintf('%sMILLONES ', $this->convertGroup($millones));
            }
        }
        
        if (intval($miles) > 0) {
            if ($miles == '001') {
                $converted .= 'MIL ';
            } else if (intval($miles) > 0) {
                $converted .= sprintf('%sMIL ', $this->convertGroup($miles));
            }
        }
        if (intval($cientos) > 0) {
            if ($cientos == '001') {
                $converted .= 'UN ';
            } else if (intval($cientos) > 0) {
                $converted .= sprintf('%s ', $this->convertGroup($cientos));
            }
        }
        //$converted .= $moneda;
        return $converted." ".$decimales." ".$moneda;
    }
    /**
     * Define el tipo de representación decimal (centenas/millares/millones)
     * @param $n
     * @return $output
     */
    private function convertGroup($n) {
        $output = '';
        if ($n == '100') {
            $output = "CIEN ";
        } else if ($n[0] !== '0') {
            $output = $this->CENTENAS[$n[0] - 1];   
        }
        $k = intval(substr($n,1));
        if ($k <= 20) {
            $output .= $this->UNIDADES[$k];
        } else {
            if(($k > 30) && ($n[2] !== '0')) {
                $output .= sprintf('%sY %s', $this->DECENAS[intval($n[1]) - 2], $this->UNIDADES[intval($n[2])]);
            } else {
                $output .= sprintf('%s%s', $this->DECENAS[intval($n[1]) - 2], $this->UNIDADES[intval($n[2])]);
            }
        }
        return $output;
    }
}
?>
    <style type="text/css">
       .contenedor-tabla
        {
            display: table;
            width: 70%;
            font-size: 10px;
            
        }

        .contenedor-fila
        {
            display: table-row;            
        }

        .contenedor-columna
        {
            display: table-cell;
            padding-top: 10px;
        }

        body{
            font-family: "Lucida Console", "Lucida Sans Typewriter", monaco, "Bitstream Vera Sans Mono", monospace;        
            font-weight:bold;
        }

        table, td, th {
            /*border: 1px solid black;*/            
            font-size: 10px;
            text-align: center;
            border: 1px solid white;            
        }

        table {
            border-collapse: collapse;
            width: 102%;
        }

        th {
            height: 18px;
            color: white;
        }
    </style>

<title>Guía de Remisión</title>

<body style="margin-left: -30px;margin-right: 0px;""> 
    <div class="contenedor-tabla">
        <div class="contenedor-fila">            
            <div class="contenedor-columna" style="width:30%;border:2px solid white;border-radius: 7px;text-align: center;height: 0px;padding-top: 0px;" >
               <label style="color:white;">RUC. 20511794553</label><br><br>
               <label style="font-weight: bold;color:white;">BOLETA</label><br><br>
               <label style="color:white;"></label>
            </div>                                          
        </div>
    </div>
    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 100px;">
              
             </div>
            <div class="contenedor-columna" style="width: 500px; font-style: italic;">
                @if ($resultado["proveedor"]!=null)
                    {{strtoupper($resultado["proveedor"]->nombre_comercial)}}
                @endif
                
            </div>
            <div class="contenedor-columna" style="font-style: italic;width: 5%;">
            </div>
            <div class="contenedor-columna" style="font-style: italic;width: 5%;">
            </div>
        </div>
        <div class="contenedor-fila">                        
            <div class="contenedor-columna" style="width: 100px;">
               
             </div>
            <div class="contenedor-columna" style="font-style: italic; width: 500px;">
                @if ($resultado["proveedor"]!=null)
                    {{strtoupper($resultado["proveedor"]->direccion)}}
                @endif
            </div>
            <div class="contenedor-columna" style="font-style: italic;width: 5%;">
            </div>
            <div class="contenedor-columna" style="font-style: italic;width: 5%;">
            </div>
        </div>
        <div class="contenedor-fila">            
            <div class="contenedor-columna" style="width: 100px;">
        
             </div>
            <div class="contenedor-columna" style="width: 500px; font-style: italic;">
                @if ($resultado["proveedor"]!=null)
                    {{strtoupper($resultado["proveedor"]->ruc)}}
                @endif
                <span style="margin-left: 330px;">{{date("d-m-Y")}}</span>
            </div>
            <div class="contenedor-columna" style="width: 200px;text-align: center;font-style: italic;">                
                
            </div>
        </div>             
    </div>

<br>
<!--div style="clear:both;"></div-->

    <div class="row">                                          
        <div class="col_x">
            <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
                <tr>
                    <th width="90px">
                        ITEM
                    </th>
                    <th width="90px">
                        CANT.
                    </th>  
                    <th width="10px">
                        MED.
                    </th>                                                               
                    <th width="300px">
                        DESCRIPCION
                    </th>
                    <th colspan="1" width="60px">
                        P.UNIT.
                    </th>
                    <th colspan="1" width="70px">
                        IMPORTE
                    </th>
                </tr>
               <?php 
                $i = 0;
               ?>
                @foreach($resultado["detalle"] as $detalle1)
                    @foreach($detalle1 as $detalle)
                        <tr>                                                            
                            <td>
                                <?php 
                                ++$i;
                                echo $detalle->producto->id;
                                ?>
                            </td>
                            <td style="text-align: left;">{{ $detalle->rollos }}</td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: left;">{{ $detalle->producto->nombre_generico }}</td>                       
                            <td  style="border-right: 1px solid white;text-align: left;" width="auto">{{$detalle->nro_lote}}</td> 
                            <td style="text-align: center;" width="auto">KG</td> 
                            <td style="border-right: 1px solid white;text-align: right; color:white" width="auto">S/. </td> 
                            <td style="text-align: left;" width="auto">{{ number_format($detalle->cantidad,2) }}</td> 
                        </tr>
                    @endforeach
                @endforeach
                @for ($j = ++$i; $j <=11; $j++)
                    <tr>                                                            
                        <td style="color:white;">{{$j}}</td>
                        <td></td>
                        <td></td>                        
                        <td  style="border-right: 1px solid white;text-align: right;" width="auto"></td> 
                        <td style="text-align: left;" width="auto"></td> 
                        <td style="border-right: 1px solid white;text-align: right;" width="auto"></td> 
                        <td style="text-align: left;" width="auto"></td> 
                    </tr>
                @endfor
                    <tr style="padding-top: 0px;">                                                        
                        <td style="border: 1px solid white;text-align: center;" colspan="4">
                            {{ strtoupper(substr($resultado["direccion_partida"], 0, 40)) }}
                        </td>                                                
                        <td style="text-align: center;" colspan="4">
                            {{strtoupper(substr($resultado["direccion_llegada"], 0, 40))}}
                        </td>
                    </tr>
                    <tr>                                                            
                        <td style="color:white;">1</td>
                        <td></td>
                        <td></td>                        
                        <td  style="border-right: 1px solid white;text-align: right;" width="auto"></td> 
                        <td style="text-align: left;" width="auto"></td> 
                        <td style="border-right: 1px solid white;text-align: right;" width="auto"></td> 
                        <td style="text-align: left;" width="auto"></td> 
                    </tr>
                    <tr>                                                            
                        <td style="color:white;">1</td>
                        <td></td>
                        <td></td>                        
                        <td  style="border-right: 1px solid white;text-align: right;" width="auto"></td> 
                        <td style="text-align: left;" width="auto"></td> 
                        <td style="border-right: 1px solid white;text-align: right;" width="auto"></td> 
                        <td style="text-align: left;" width="auto"></td> 
                    </tr>  
                    <tr>
                        <td colspan="4" style="border-bottom: 1px solid white;border-left: 1px solid white;text-align: left; padding-left: 100px;">
                            @if (!is_null($resultado["transportista"]))
                                {{strtoupper($resultado["transportista"]->nombre)}}
                            @endif
                        </td>
                        <td colspan="2" style="border-bottom: 1px solid white;border-left: 1px solid white;text-align: left; padding-left: 100px;">
                            
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="border-bottom: 1px solid white;border-left: 1px solid white;text-align: left; padding-left: 100px;">
                            {{strtoupper($resultado["transportista"]->direccion)}}
                        </td>
                        <td colspan="2" style="border-bottom: 1px solid white;border-left: 1px solid white;text-align: left; padding-left: 100px;">
                            
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border-bottom: 1px solid white;border-left: 1px solid white;text-align: left; padding-left: 180px;">
                            {{strtoupper($resultado["transportista"]->ruc)}}
                        </td>
                        <td colspan="3" style="border-bottom: 1px solid white;border-left: 1px solid white;text-align: left; padding-left: 180px;">
                            {{strtoupper($resultado["transportista"]->marca).strtoupper($resultado["transportista"]->placa)}}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border-bottom: 1px solid white;border-left: 1px solid white;text-align: left; padding-left: 180px;">
                        </td>
                        <td colspan="3" style="border-bottom: 1px solid white;border-left: 1px solid white;text-align: left; padding-left: 180px;">
                            {{strtoupper($resultado["transportista"]->licencia)}}
                        </td>
                    </tr>
                    <!--<tr>                                                            
                        <td colspan="4" style="border-bottom: 1px solid white;border-left: 1px solid white;text-align: center;">
                            
                        </td>                                                
                        <td colspan="4" style="text-align: center;color: white;font-weight: bold;">
                            X
                        </td>
                    </tr> 
                    <tr>                                                            
                        <td colspan="4" style="border-bottom: 1px solid white;border-left: 1px solid white;text-align: center;">
                            
                        </td>                                                
                        <td colspan="4" style="text-align: left;">
                            X
                        </td>
                    </tr>   -->                                            
            </table>
        </div>
    </div>
    <br>
</body>
