<div class="row">
  <div class="col-md-12">
    <fieldset>
    <legend>Datos del Transportista</legend>
    <div class="col-md-4">
         {!! Form::label('nombretransportista', 'Nombre', ['class' => 'fillable control-label']) !!}
        {!! Form::text('transportista[nombre]', null, ['class' => 'form-control','required'=> '', 'id' => 'nombretransportista']) !!}
    </div>
    <div class="col-md-4">
         {!! Form::label('direccion', 'Direccion', ['class' => 'fillable control-label']) !!}
        {!! Form::text('transportista[direccion]', null, ['class' => 'form-control','required'=> '', 'id' => 'direcciontransportista']) !!}
    </div>
    <div class="col-md-4">
         {!! Form::label('ructransportista', 'RUC', ['class' => 'fillable control-label']) !!}
        {!! Form::text('transportista[ruc]', null, ['class' => 'form-control','required'=> '', 'id' => 'ructransportista']) !!}
    </div>
    <div class="col-md-4">
         {!! Form::label('marcatransportista', 'Marca', ['class' => 'fillable control-label']) !!}
        {!! Form::text('transportista[marca]', null, ['class' => 'form-control','required'=> '', 'id' => 'marcatransportista']) !!}
    </div>
    <div class="col-md-4">
         {!! Form::label('placatransportista', 'Placa', ['class' => 'fillable control-label']) !!}
        {!! Form::text('transportista[placa]', null, ['class' => 'form-control','required'=> '', 'id' => 'placatransportista']) !!}
    </div>
    <div class="col-md-4">
         {!! Form::label('licenciatransportista', 'Licencia', ['class' => 'fillable control-label']) !!}
        {!! Form::text('transportista[licencia]', null, ['class' => 'form-control','required'=> '', 'id' => 'licenciatransportista']) !!}
    </div>
  </div>
  
  </fieldset>
</div>

<hr>
<div class="row">
  <div class="col-md-6">
        {!! Form::label('direccionpartida', 'P. Partida', ['class' => 'fillable control-label']) !!}
        {!! Form::text('direccion_partida', $direccion, ['class' => 'form-control','required'=> '']) !!}
    </div>
    <div class="col-md-6">
        {!! Form::label('direccionllegada', 'P. Llegada', ['class' => 'fillable control-label']) !!}
         {!! Form::text('direccion_llegada', null, ['class' => 'form-control', 'id' => 'direccionllegada']) !!}
    </div>
</div>
<hr>
<div class="row">
    
    <div class="col-md-4">
        {!! Form::label('fecha', 'Fecha', ['class' => 'fillable control-label']) !!}
        {!! Form::date('fecha', date('Y-m-d'), ['class' => 'form-control','required'=> '', 'id' => 'fecha']) !!}
    </div>
    <div class="col-md-4">
        {!! Form::label('guia', 'Guia', ['class' => 'fillable control-label']) !!}
        {!! Form::text('nroguia', '', ['class' => 'form-control','required'=> '', 'id' => 'nroguia']) !!}
    </div>
    <div class="col-md-4 pull-right">
      <div class="form-group">
        <div class="col-md-6">
            {!! Form::radio('peso_balanza', '2', ['checked' => 'checked']) !!}
            {!! Form::label('manual', 'P.Manual') !!}
                      
        </div>
        <div class="col-md-6">
            {!! Form::radio('peso_balanza', '1') !!}
            {!! Form::label('automatico', 'P.Automático') !!}
                      
        </div>
      </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
      {!! Form::label('producto', 'Producto', ['class' => 'fillable control-label ']) !!}
      <select class="form-control selectpicker" name="producto" data-live-search="true">
        <option value="">Seleccione</option>
        @foreach ($productos as $key => $producto)
            <option value="{{$producto->id}}">{{$producto->nombre_generico}}</option>
        @endforeach
      </select>

    </div>
    <div class="col-md-4">
      {!! Form::label('lote', 'Lotes', ['class' => 'fillable control-label ']) !!}
      <select class="form-control selectpicker" name="lote" data-live-search="true" id="lote" required="" disabled="">
      </select>

    </div>

    <div class="col-md-4">
        {!! Form::label('proveedor', 'Tintoreria', ['class' => 'fillable control-label']) !!}
        <select class="form-control selectpicker" name="proveedor" data-live-search="true" id="proveedor" >
          <option value="">Seleccione</option>
          @foreach ($proveedores as $key => $proveedor)
            <option value="{{$proveedor->proveedor_id}}">{{$proveedor->nombre_comercial}}</option>
        @endforeach
        </select>
    </div>

    <div class="col-md-4">
        {!! Form::label('turno', 'Color', ['class' => 'fillable control-label']) !!}
        <select class="form-control selectpicker" name="color" data-live-search="true" >
          <option value="">Seleccione</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        {!! Form::label('cantidad', 'Kg', ['class' => 'fillable control-label']) !!}
        <input class="form-control" type="text" name="kg" value="">
    </div>
    <div class="col-md-4">
      {!! Form::label('cantidad', 'Rollos', ['class' => 'fillable control-label']) !!}
      <input class="form-control" type="text" name="rollos" value="">
    </div>


    <div class="col-md-4">
      <br>
      <a class="btn btn-primary" id="add_to_grid" href="#">Agregar</a>
    </div>
</div>
<br>
<br>
<table id="despachos_grid" class="table table-bordered table-striped table-hover">
<thead>
    <tr>
      <th>
        Fecha
      </th>
      <th>Tintoreria</th>
      <th width="2">Producto</th>
      <th>Lote</th>
      <th>Color</th>
      <th>KG</th>
      <th>Rollos</th>
      <th>Eliminar</th>
    </tr>
</thead>
    <tbody>
        <tr class="hide">
          <td>
          </td>
          <td>
          </td>
          <td></td>
          <td>
          </td>
          <td>
          </td>
          <td>
          </td>
          <td class="cajas">
          </td>
        </tr>
     </tbody>
</table>
<script type="text/javascript">
  proveedoresdata = <?php echo json_encode($proveedores)?>;
  listproveedores = {};
  for (var i in proveedoresdata) {
    listproveedores[proveedoresdata[i].proveedor_id] = proveedoresdata[i];
  }
</script>
<script type="text/javascript">
  let proveedorburga = <?php echo json_encode($proveedorburga);?>;
</script>