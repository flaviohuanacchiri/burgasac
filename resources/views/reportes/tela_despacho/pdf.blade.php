<style type="text/css">
    table{width: 100%; display: block;}
    th{font-size: 14px;}
    h3, td, p, th {font-size: 13px; font-family: Arial;}
    td {padding: 2px;}
    thead { display: table-header-group }
    tfoot { display: table-row-group }
    tr { page-break-inside: avoid }
    h3, td, th{text-align: center;}
    td.montoapagar{width: 50%;}
    p{padding: 0px; margin: 0px; margin-bottom: 5px;}
</style>
<div style='font-family: Helvetica, font-size:12px'>
    <h3>REPORTE GENERAL DE TELA EN DESPACHO</h1>
    <p>F. Reporte: {{date('Y-m-d h:i:s a') }}</p>
    <div>
        <table>
            <tbody>
                <tr>
                    <td colspan="7" style="border: solid 2px; width: 300px;"><b>Tela Producida</b></td>
                    <td colspan="3" style="border: solid 2px;"><b>Despacho Tercero</b></td>
                    <td style="border: solid 2px;"><b>Por Despachar</b></td>
                </tr>
                <tr>
                    <td style="width: 25px; border: solid 1px;"><b>ID</b></td>
                    <td style="width: 80px; border: solid 1px;"><b>Fecha</b></td>
                    <td style="width: 200px; border: solid 1px;"><b>Proveedor</b></td>
                    <td style="width: 150px; border: solid 1px;"><b>Producto</b></td>
                    <td style="width: 80px; border: solid 1px;"><b>Lote</b></td>
                    <td style="width: 80px; border: solid 1px;"><b>P. Neto(kg)</b></td>
                    <td style="width: 80px; border: solid 1px;"><b>Cant.</b></td>
                    <td style="width: 80px; border: solid 1px;"><b>Guia</b></td>
                    <td style="width: 80px; border: solid 1px;"><b>P.Neto(kg)</b></td>
                    <td style="width: 80px; border: solid 1px;"><b>Cant.</b></td>
                    <td style="width: 80px; border: solid 1px;"><b>P.Neto(kg)</b></td>
                </tr>
                @php
                    $totales = ["pesoneto" => 0, "cantidad" => 0, "pesotienda" => 0, "cantidadtienda" => 0, "pesopordespachar" => 0];
                @endphp
                @foreach($data as $key => $value)
                @php
                    $pesoNeto = $value["kg_producidos"] - $value["kg_falla"];
                    $rollos = $value["rollos"] - $value["rollos_falla"];
                    $totales["pesoneto"]+=$pesoNeto;
                    $totales["cantidad"]+=$rollos;
                    $despachoTienda = $value["despacho_tercero"];
                    $rowspan = count($despachoTienda);
                    if ($rowspan <= 0) {
                        $rowspan = 1;
                    }

                    //$totales["pesotienda"]+=$value->pesotienda;
                    //$totales["cantidadtienda"]+=$value->cantidadtienda;
                    $pesopordespachar = $pesoNeto;
                    $agrupadoDetalle = [];
                    $agrupadoDetallePlaneamiento = [];
                    
                @endphp
                <tr>
                    <td style="width: 25px; border-bottom: solid 1px;" rowspan="{{$rowspan}}">{{$value["id"]}}</td>
                    <td style="width: 80px; border-bottom: solid 1px;" rowspan="{{$rowspan}}">{{date('Y-m-d', strtotime($value["fecha"]))}}</td>
                    <td style="width: 200px; border-bottom: solid 1px;" rowspan="{{$rowspan}}">{{$value["proveedor"]["nombre_comercial"]}}</td>
                    <td style="width: 150px; border-bottom: solid 1px;" rowspan="{{$rowspan}}">{{$value["producto"]["nombre_generico"]}}</td>
                    @php
                        $lote = "";
                        if (isset($value["indicador"]["lote"])) {
                            $lote = $value["indicador"]["lote"];
                        }
                    @endphp
                    <td style="width: 80px; border-bottom: solid 1px;" rowspan="{{$rowspan}}"><b>{{$lote}}</b></td>
                    <td style="width: 80px; border-bottom: solid 1px;" rowspan="{{$rowspan}}">{{number_format($pesoNeto, 2)}}</td>
                    <td style="width: 80px; border-bottom: solid 1px; border-right: solid 1px;" rowspan="{{$rowspan}}">{{number_format($rollos, 2)}}</td>
                    @php
                        $cantidad = 0;
                    @endphp
                    @if ($rowspan == 1)
                        @php
                            $guia = "";
                            $peso = 0;
                            $cantidad = 0;
                            foreach ($despachoTienda as $key2 => $value2) {
                                $guia = $value2["despacho_tercero"]["nroguia"];
                                $peso = $value2["peso"];
                                $cantidad = $value2["despacho_tercero_detalle"]["rollos"];
                                $pesopordespachar-= $peso;
                            }
                        @endphp
                        <td style="width: 80px; border-bottom: solid 1px;">{{$guia}}</td>
                        <td style="width: 80px; border-bottom: solid 1px;">{{number_format($peso, 2)}}</td>
                        <td style="width: 80px; border-bottom: solid 1px; border-right: solid 1px; border-left: solid 1px;" rowspan="{{$rowspan}}">{{$cantidad}}</td>
                        <td style="width: 80px; border-bottom: solid 1px;">{{number_format($pesopordespachar, 2)}}</td>
                    @else
                        @php
                            $guia = $despachoTienda[0]["despacho_tercero"]["nroguia"];
                            $peso = $despachoTienda[0]["peso"];
                            $cantidad = $despachoTienda[0]["despacho_tercero_detalle"]["rollos"];
                            $pesopordespachar-= $peso;
                        @endphp
                        <td style="width: 80px; border-bottom: solid 1px;">{{$guia}}</td>
                        <td style="width: 80px; border-bottom: solid 1px;">{{number_format($peso, 2)}}</td>
                        <td style="width: 80px; border-bottom: solid 1px; border-right: solid 1px; border-left: solid 1px;" rowspan="{{$rowspan}}">{{$cantidad}}</td>
                        <td style="width: 80px; border-bottom: solid 1px;">{{number_format($pesopordespachar, 2)}}</td>
                    @endif
                </tr>
                @if ($rowspan > 1)
                    @php
                        if (count($despachoTienda) > 0) {
                            unset($despachoTienda[0]);
                        }
                        
                    @endphp
                    @if (isset($despachoTienda[1]))
                    @php
                        $peso = $despachoTienda[1]["peso"];
                        $pesopordespachar-= $peso;
                    @endphp
                    <tr>
                        <td style="width: 80px; border-bottom: solid 1px;">{{$despachoTienda[1]["despacho_tercero"]["nroguia"]}}</td>
                        <td style="width: 80px; border-bottom: solid 1px;">{{number_format($despachoTienda[1]["peso"], 2)}}</td>
                        <td style="width: 80px; border-bottom: solid 1px;">{{number_format($pesopordespachar, 2)}}</td>
                    </tr>
                    @endif
                    @foreach ($despachoTienda as $key2 => $value2)
                    
                    @endforeach
                    @foreach ($despachoTienda as $key2 => $value2)
                    <tr>
                        
                    </tr>
                    @endforeach
                @endif
                @php
                    $totales["pesopordespachar"]+=$pesopordespachar;
                @endphp
                @endforeach
                <tr>
					<td style="width: 25px;"></td>
					<td style="width: 80px;"></td>
					<td style="width: 200px;"></td>
					<td style="width: 150px; border: solid 1px;"><b>TOTALES</b></td>
					<td style="width: 80px; border: solid 1px;"></td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['pesoneto'],2)}}</td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['cantidad'], 2)}}</td>
					<td style="width: 80px; border: solid 1px;"></td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['pesotienda'], 2)}}</td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['cantidadtienda'], 2)}}</td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['pesopordespachar'], 2)}}</td>
				</tr>
			</tbody>
        </table>
    </div>
</div>
@php
	dd();
@endphp