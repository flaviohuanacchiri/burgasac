<style type="text/css">
    table{width: 100%; display: block;}
    th{font-size: 14px;}
    h3, td, p, th {font-size: 13px; font-family: Arial;}
    td {padding: 2px;}
    thead { display: table-header-group }
    tfoot { display: table-row-group }
    tr { page-break-inside: avoid }
    h3, td, th{text-align: center;}
    td.montoapagar{width: 50%;}
    p{padding: 0px; margin: 0px; margin-bottom: 5px;}
</style>
<div style='font-family: Helvetica, font-size:12px'>
	<h3>REPORTE GENERAL DE DEUDA DE TELA EN TINTORERIA</h1>
	<p>F. Reporte: {{date('Y-m-d h:i:s a') }}</p>
	<div>
		<table>
			<tbody>
				<tr>
					<td style="width: 25px; border: solid 1px;"><b>ID</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Fecha</b></td>
					<td style="width: 200px; border: solid 1px;"><b>Proveedor</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Guia</b></td>
					<td style="width: 150px; border: solid 1px;"><b>Producto</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Color</b></td>
					<td style="width: 80px; border: solid 1px;"><b>P. Neto(kg)</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Total</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Pend. Pago</b></td>
				</tr>
				<?php 
					$totales = ["pesoneto" => 0, "total" => 0];
				?>
				@foreach($data as $key => $value)
				<?php 
					$pesoneto = $value->total/$value->preciounitario;
					$totales["pesoneto"]+=$pesoneto;
					$totales["total"]+=$value->total;
				?>
				<tr>
					<td style="width: 25px;">{{$value->detalle_despacho_id}}</td>
					<td style="width: 80px;">{{date('Y-m-d', strtotime($value->created_at))}}</td>
					<td style="width: 200px;">{{$value->proveedor}}</td>
					<td style="width: 80px;">{{$value->nroguia}}</td>
					<td style="width: 150px;">{{$value->producto}}</td>
					<td style="width: 80px;">{{$value->color}}</td>
					<td style="width: 80px;">{{number_format($pesoneto,2)}}</td>
					<td style="width: 80px;">{{number_format($value->total, 2)}}</td>
					<td style="width: 80px;">{{number_format($value->total, 2)}}</td>
				</tr>
				@endforeach
				<tr>
					<td style="width: 25px;"></td>
					<td style="width: 80px;"></td>
					<td style="width: 200px;"></td>
					<td style="width: 150px;"></td>
					<td style="width: 80px;"></td>
					<td style="width: 80px; border: solid 1px;"><b>TOTAL</b></td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['pesoneto'],2)}}</td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['total'], 2)}}</td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['total'], 2)}}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<?php dd();?>