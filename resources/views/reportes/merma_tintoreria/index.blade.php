@extends('backend.layouts.appv2')

@section('after-styles')
    <style>
    .dropdown{padding: 0;}
    .dropdown .dropdown-menu{border: 1px solid #999}
    .detallescompra{
        display: none;
        background-color: #ececec;
    }
    </style>
@stop

@section('content')
    <div>
      <div>
        @include('reportes/merma_tintoreria/filtro')
      </div>
      <div>
        <table class="table table-striped table-bordered dt-responsive nowrap" id="table-reporte"  cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Fecha</th>
              <th>Planeamiento</th>
              <th>Lote</th>
              <th>Producto</th>
              <th>Peso (Kg)</th>
              <th>Cantidad</th>                         
            </tr>
          </thead>
        </table>
      </div>
    </div>

@endsection
@push('scripts')
<script type="text/javascript">
  url = "merma_tintoreria";
</script>
{{ Html::script('js/reportes.js') }}
{{ Html::script('js/reportes/merma_tintoreria.js') }}
<script type="text/javascript">
  Reporte.buscar();
  $(".btn-search").click(function() {
    Reporte.buscar();
  });
</script>
@endpush
