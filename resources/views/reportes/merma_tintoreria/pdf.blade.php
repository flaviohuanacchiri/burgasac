<style type="text/css">
    table{width: 100%; display: block;}
    th{font-size: 14px;}
    h3, td, p, th {font-size: 13px; font-family: Arial;}
    td {padding: 2px;}
    thead { display: table-header-group }
    tfoot { display: table-row-group }
    tr { page-break-inside: avoid }
    h3, td, th{text-align: center;}
    td.montoapagar{width: 50%;}
    p{padding: 0px; margin: 0px; margin-bottom: 5px;}
</style>
<div style='font-family: Helvetica, font-size:12px'>
	<h3>REPORTE DE MERMA EN EL PROCESO PRODUCTIVO</h1>
	<p>F. Reporte: {{date('Y-m-d h:i:s a') }}</p>
	<div>
		<table>
			<tbody>
				<tr>
					<td colspan="6" style="border: solid 2px; width: 300px;"><b>Produccion</b></td>
					<td colspan="2" style="border: solid 2px;"><b>Despacho en Tintoreria</b></td>
					<td colspan="2" style="border: solid 2px;"><b>Despacho a PV</b></td>
					<td style="border: solid 2px;"><b>Merma P - DT</b></td>
					<td style="border: solid 2px;"><b>Merma DT - PV</b></td>
					<td style="border: solid 2px;"><b>Merma Parcial</b></td>
				</tr>
				<tr>
					<td style="width: 25px; border: solid 1px;"><b>Fecha</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Pla</b></td>
					<td style="width: 200px; border: solid 1px;"><b>Lotes</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Producto</b></td>
					<td style="width: 150px; border: solid 1px;"><b>Peso(kg)</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Cantidad</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Peso(kg)</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Cantidad</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Peso(kg)</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Cantidad</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Peso(kg)</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Peso(kg)</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Peso(kg)</b></td>
				</tr>
				<?php 
					$totales = ["pesoneto" => 0, "cantidadneta" => 0, "pesotintoreria" => 0, "cantidadtintoreria" =>0, "pesoingresotienda" => 0, "cantidadingresotienda" => 0, "mermadespacho" => 0, "mermaingreso" => 0, "mermaparcial" => 0];
				?>
				@foreach($data as $key => $value)
				<?php 
					$totales["pesoneto"]+=$value->pesoneto;
					$totales["cantidadneta"]+=$value->cantidadneta;
					$totales["pesotintoreria"]+=$value->pesotintoreria;
					$totales["cantidadtintoreria"]+=$value->rollostintoreria;
					$totales["pesoingresotienda"]+=$value->pesoingresotienda;
					$totales["cantidadingresotienda"]+=$value->cantidadingresotienda;
					$pesomermadespacho = $value->pesoneto - $value->pesotintoreria;
					$pesomermaingreso = $value->pesotintoreria - $value->pesoingresotienda;
					$pesomermapacial = $pesomermadespacho + $pesomermaingreso;
					$totales["mermadespacho"]+=$pesomermadespacho;
					$totales["mermaingreso"]+=$pesomermaingreso;
					$totales["mermaparcial"]+=$pesomermapacial;
				?>
				<tr>
					<td style="width: 25px;">{{$value->fecha}}</td>
					<td style="width: 80px;">{{$value->planeamiento_id}}</td>
					<td style="width: 200px;">{{$value->lotes}}</td>
					<td style="width: 80px;">{{$value->producto}}</td>
					<td style="width: 150px;">{{number_format($value->pesoneto, 2)}}</td>
					<td style="width: 80px;">{{$value->cantidadneta}}</td>
					<td style="width: 80px;">{{number_format($value->pesotintoreria,2)}}</td>
					<td style="width: 80px;">{{number_format($value->rollostintoreria, 2)}}</td>
					<td style="width: 80px;">{{number_format($value->pesoingresotienda, 2)}}</td>
					<td style="width: 80px;">{{number_format($value->cantidadingresotienda, 2)}}</td>
					<td style="width: 80px;">{{number_format($pesomermadespacho, 2)}}</td>
					<td style="width: 80px;">{{number_format($pesomermaingreso, 2)}}</td>
					<td style="width: 80px;">{{number_format($pesomermapacial, 2)}}</td>
				</tr>
				@endforeach
				<tr>
					<td style="width: 25px;"></td>
					<td style="width: 80px;"></td>
					<td style="width: 200px;"></td>
					<td style="width: 150px; border: solid 1px;"><b>TOTAL</b></td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['pesoneto'],2)}}</td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['cantidadneta'], 2)}}</td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['pesotintoreria'], 2)}}</td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['cantidadtintoreria'], 2)}}</td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['pesoingresotienda'], 2)}}</td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['cantidadingresotienda'], 2)}}</td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['mermadespacho'], 2)}}</td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['mermaingreso'], 2)}}</td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['mermaparcial'], 2)}}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<?php dd();?>