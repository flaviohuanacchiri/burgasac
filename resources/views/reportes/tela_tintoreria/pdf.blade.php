<style type="text/css">
    table{width: 100%; display: block;}
    th{font-size: 14px;}
    h3, td, p, th {font-size: 13px; font-family: Arial;}
    td {padding: 2px;}
    thead { display: table-header-group }
    tfoot { display: table-row-group }
    tr { page-break-inside: avoid }
    h3, td, th{text-align: center;}
    td.montoapagar{width: 50%;}
    p{padding: 0px; margin: 0px; margin-bottom: 5px;}
</style>
<div style='font-family: Helvetica, font-size:12px'>
	<h3>REPORTE GENERAL DE TELA EN TINTORERIA</h1>
	<p>F. Reporte: {{date('Y-m-d h:i:s a') }}</p>
	<div>
		<table>
			<tbody>
				<tr>
					<td colspan="9" style="border: solid 2px; width: 300px;"><b>Entrega en Tintoreria</b></td>
					<td colspan="3" style="border: solid 2px;"><b>Despacho en Tienda</b></td>
					<td style="border: solid 2px;"><b>Por Despachar</b></td>
				</tr>
				<tr>
					<td style="width: 25px; border: solid 1px;"><b>ID</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Fecha</b></td>
					<td style="width: 200px; border: solid 1px;"><b>Proveedor</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Guia</b></td>
					<td style="width: 150px; border: solid 1px;"><b>Producto</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Color</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Lote</b></td>
					<td style="width: 80px; border: solid 1px;"><b>P. Neto(kg)</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Cant.</b></td>
					<td style="width: 80px; border: solid 1px;"><b>F.Tienda</b></td>
					<td style="width: 80px; border: solid 1px;"><b>P.Neto(kg)</b></td>
					<td style="width: 80px; border: solid 1px;"><b>Cant.</b></td>
					<td style="width: 80px; border: solid 1px;"><b>P.Neto(kg)</b></td>
				</tr>
				@php 
					$totales = ["pesoneto" => 0, "cantidad" => 0, "pesotienda" => 0, "cantidadtienda" => 0, "pesopordespachar" => 0];
					$pesopordespachar = 0;
				@endphp
				@foreach($agrupar as $key => $value)
					@foreach($value as $key2 => $value2)
						@foreach($value2 as $key3 => $value3)
							@foreach($value3 as $key4 => $value4)
								@foreach($value4 as $key5 => $value5)
									@php
										$filas = count($value5);
									@endphp
									@if (isset($value5[0]))
										<?php
											$pesodespacho = $value5[0]->cantidad;
											$pesopordespachar = $pesodespacho - $value5[0]->pesotienda;
											$pesodespacho = $pesopordespachar;
											$totales["pesotienda"]+=$value5[0]->pesotienda;
											$totales["cantidadtienda"]+=$value5[0]->cantidadtienda;
											$totales["pesoneto"]+=$value5[0]->cantidad;
											$totales["cantidad"]+=$value5[0]->rollos;
										?>
										<tr>
											<td style="width: 25px;" rowspan="{{$filas}}">{{$value5[0]->detalle}}</td>
											<td style="width: 80px;" rowspan="{{$filas}}">{{date('Y-m-d', strtotime($value5[0]->fecha))}}</td>
											<td style="width: 200px;" rowspan="{{$filas}}">{{$value5[0]->proveedor}}</td>
											<td style="width: 80px;" rowspan="{{$filas}}">{{$value5[0]->nroguia}}</td>
											<td style="width: 150px;" rowspan="{{$filas}}">{{$value5[0]->producto}}</td>
											<td style="width: 80px;" rowspan="{{$filas}}">{{$value5[0]->color}}</td>
											<td style="width: 80px;" rowspan="{{$filas}}">{{$value5[0]->lote}}</td>
											<td style="width: 80px;" rowspan="{{$filas}}">{{number_format($value5[0]->cantidad,2)}}</td>
											<td style="width: 80px;" rowspan="{{$filas}}">{{number_format($value5[0]->rollos, 2)}}</td>
											@if ($value5[0]->fechaingreso!="")
												<td style="width: 80px;">{{date('Y-m-d', strtotime($value5[0]->fechaingreso))}}</td>
											@else
												<td style="width: 80px;"></td>
											@endif
											<td style="width: 80px;">{{number_format($value5[0]->pesotienda, 2)}}</td>
											<td style="width: 80px;">{{number_format($value5[0]->cantidadtienda, 2)}}</td>
											<td style="width: 80px;">{{number_format($pesopordespachar, 2)}}</td>
										</tr>
										@for($i = 1; $i < count($value5); $i++)
										<?php
											$pesopordespachar = $pesodespacho - $value5[$i]->pesotienda;
											$pesodespacho = $pesopordespachar;
											$totales["pesotienda"]+=$value5[$i]->pesotienda;
											$totales["cantidadtienda"]+=$value5[$i]->cantidadtienda;
										?>
										<tr>
											@if ($value5[$i]->fechaingreso!="")
												<td style="width: 80px;">{{date('Y-m-d', strtotime($value5[$i]->fechaingreso))}}</td>
											@else
												<td style="width: 80px;"></td>
											@endif
											<td style="width: 80px;">{{number_format($value5[$i]->pesotienda, 2)}}</td>
											<td style="width: 80px;">{{number_format($value5[$i]->cantidadtienda, 2)}}</td>
											<td style="width: 80px;">{{number_format($pesopordespachar, 2)}}</td>
										</tr>
										@endfor
										<tr>
											<td style="width: 25px; border-bottom: solid 1px;"></td>
											<td style="width: 80px; border-bottom: solid 1px;"></td>
											<td style="width: 200px; border-bottom: solid 1px;"></td>
											<td style="width: 80px; border-bottom: solid 1px;"></td>
											<td style="width: 150px; border-bottom: solid 1px;"></td>
											<td style="width: 80px; border-bottom: solid 1px;"></td>
											<td style="width: 80px; border-bottom: solid 1px;"></td>
											<td style="width: 80px; border-bottom: solid 1px;"></td>
											<td style="width: 80px; border-bottom: solid 1px;"></td>
											<td style="width: 80px; border-bottom: solid 1px;"></td>
											<td style="width: 80px; border-bottom: solid 1px;"></td>
											<td style="width: 80px; border-bottom: solid 1px;"></td>
											<td style="width: 80px; border-bottom: solid 1px;"></td>
										</tr>
									@endif
								@endforeach
							@endforeach
						@endforeach
					@endforeach
				@endforeach
				
				<tr>
					<td style="width: 25px;"></td>
					<td style="width: 80px;"></td>
					<td style="width: 200px;"></td>
					<td style="width: 80px;"></td>
					<td style="width: 150px; border: solid 1px;"><b>TOTALES</b></td>
					<td style="width: 80px; border: solid 1px;"></td>
					<td style="width: 80px; border: solid 1px;"></td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['pesoneto'],2)}}</td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['cantidad'], 2)}}</td>
					<td style="width: 80px; border: solid 1px;"></td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['pesotienda'], 2)}}</td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['cantidadtienda'], 2)}}</td>
					<td style="width: 80px; border: solid 1px;">{{number_format($totales['pesopordespachar'], 2)}}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<?php dd();?>