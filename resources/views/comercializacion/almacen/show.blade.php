@extends('backend.layouts.appv2')
@section('subtitulo', 'Almacén')
@section('content')

<link href="{{ asset('css/form.css') }}" rel="stylesheet">

<div class="row">
	<div class="col-sm-12">
		<section class="content">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title">@yield('subtitulo') | {{ $alm->cProvNom }}</h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->
		        <div class="box-body">
	                <div class="form-group">
		                <div class="row">
		                	<div class="col-md-3">
			                		<label>Nombre</label>
			                    	<input class="form-control" type="text" name="nombre" id="nombre" placeholder="Nombre" value="{{ $alm->cAlmNom }}" onkeypress="return tabular(event,this)" disabled>
			                	</div>
			                	<div class="col-md-4">
			                		<label>Dirección</label>
			                    	<input class="form-control" type="text" name="ubic" id="ubic" placeholder="Dirección" value="{{ $alm->cAlmUbi }}" maxlength="255" disabled>                  	
			                    </div>
			                    <div class="col-md-2">
			                    	<label>Serie</label>
			                    	<input class="form-control" type="text" name="serie" id="serie" placeholder="000" value="{{ $alm->nAlmSerAct }}" maxlength="3" onkeypress="return tabular(event,this)" disabled>
			                	</div>
			                	<div class="col-md-3">
			                		<label>Número</label>
			                    	<input class="form-control" type="text" name="numero" id="numero" placeholder="000000" value="{{ $alm->nAlmNumAct }}" maxlength="6" onkeypress="return tabular(event,this)" disabled>
			                	</div>
		                </div>
		            </div>

	                <a href="{{ route('almacen.edit',$alm->nAlmCod) }}" class=" signbuttons btn btn-primary">Editar</a> <a href="{{ route('almacen.index') }}" class=" signbuttons btn btn-primary">Listado</a>
		          
		        </div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>

@endsection