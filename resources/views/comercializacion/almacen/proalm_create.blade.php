@extends('backend.layouts.appv2')

@section('subtitulo', 'Agregar producto a ')

@section('content')


<link href="{{ asset('css/form.css') }}" rel="stylesheet">

<div class="row">
	<div class="col-sm-12">
		<section class="content">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title">Productos registrados en: {{ $nombrealmacen->cAlmNom}}</h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->
		        <div class="box-body">
		        	<div class="col-md-12" style="text-align: right;"> 
			        	<form action="{{ route('almacen.proalm_create',$id) }}" method="GET" >
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="GET">

				            <select class="selectpicker" data-show-subtext="true" data-live-search="true" autofocus="autofocus" name="nombre" id="nombre">
						      	<option data-subtext="productos" value="">Todos</option>
						      	@foreach($prodalm_Bus as $produ)
							        <option data-subtext="({{$produ->nombre_especifico}})" value="{{$produ->id}}">{{$produ->nombre_generico}}</option>							        
						        @endforeach
						     </select>			
						     <select class="selectpicker" data-show-subtext="true" data-live-search="true" autofocus="autofocus" name="color_bus" id="color_bus">
								<option data-subtext="color" value="">Todos</option>
						     </select>				     
				            <button type="submit" class="btn btn-success btn-xs" style="width: 36px; height: 33px; margin: -10px;border-radius: 0px 5px 4px 0px;"><span class="fa fa-search" aria-hidden="true"></span></button>
			    		</form>       
			        </div>
			        <br>
			        <br>
		        	<div class="row">
			            <div class="col-md-12"> 
				        	<table class="table table-striped table-bordered table-hover">
		                        <thead>
		                            <th>PRODUCTO</th>
		                            <th>COLOR</th>
		                            <th>CAP.MIN</th>
		                            <th>CAP.MAX</th>
		                            <th>P.UNIT.</th>
		                            <th>STOCK</th>                                                
		                            <th colspan="2">ACCIONES</th>		                                                    
		                        </thead>
		                    @foreach($prodalm as $palm)
		                        <tr colspan="5">
		                        	@if(is_null($palm->producto))
		                        		<td></td>
		                        	@else                                            
		                            <td>{{ $palm->producto->nombre_generico }} ({{ $palm->producto->nombre_especifico }})</td>
		                            @endif
		                            <td>{{ $palm->color->nombre }}</td>
		                            <td>{{ $palm->nProdAlmMin }}</td>
		                            <td>{{ $palm->nProdAlmMax }}</td>
		                            <td>{{ $palm->nProdAlmPuni }}</td>
		                            <td>{{ $palm->nProdAlmStock }}</td>
		                            <td>
		                        		<a href="{{ route('almacen.proalm_edit',$palm->nProAlmCod) }}" class="btn btn-xs btn-primary">
											<i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar"></i>
										</a>
									</td>
									<td>
		                            	<a data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Eliminar" data-trans-title="¿Está seguro?" class="btn btn-xs btn-danger" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
											<i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar"></i>
											
											<form action="{{ route('almacen.proalm_destroy',$palm->nProAlmCod) }}" method="POST" name="delete_item" style="display:none">
											   {{ csrf_field() }}
												<input type="hidden" name="_method" value="DELETE">
											</form>
										</a>
									</td>
		                        </tr>
		                    @endforeach
		                    </table>
		                    {!! $prodalm->render() !!}
		                </div>
		        </div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<section class="content">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title">@yield('subtitulo'){{ $nombrealmacen->cAlmNom}}</h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->
		        <div class="box-body">
		            @include('comercializacion.almacen.fragment.error')
		             @include('comercializacion.almacen.fragment.info')

		            <form action="{{ route('almacen.proalm_store',$id) }}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="POST">
		                <div class="form-group">
			                <div class="row">
			                	<div class="col-md-8">
			                		<div class="row-fluid">
			                			<label>Producto</label>
									      <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" autofocus="autofocus" id="selprod" name="selprod">
									      	<option data-subtext="" value="">Seleccione el producto</option>
									      	@foreach($productos as $prod)
										        <option data-subtext="({{$prod->nombre_especifico}})" value="{{$prod->id}}">{{$prod->nombre_generico}}</option>
									        @endforeach
									      </select>								      
								    </div>
			                	</div>
			                	<div class="col-md-4">
			                		<div class="row-fluid">
			                			<label>Color</label>
									      <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" autofocus="autofocus" id="colprod" name="colprod">
									      </select>
								    </div>
			                	</div>
			                </div>
			            </div>

			            <div class="form-group">
			                <div class="row">
			                	<div class="col-md-3">
			                		<label>Capacidad Min.</label>
			                    	<input class="form-control" type="number" min="0" name="mini" id="mini" placeholder="Cap. MIN" value="0" maxlength="11">
			                	</div>
			                	<div class="col-md-3">
			                		<label>Capacidad Max.</label>
			                    	<input class="form-control" type="number" min="0" name="maxi" id="maxi" placeholder="Cap. MAX" value="0" maxlength="11">
			                    </div>
			                    <div class="col-md-3">
			                		<label>P. Uni</label>
			                    	<input class="form-control" type="text" name="puni" id="puni" placeholder="precio unit." maxlength="17">
			                    </div>
			                    <div class="col-md-3">
			                    	<label>Stock Inicial</label>
			                    	<input class="form-control" type="text" name="stock" id="stock" placeholder="Stock" value="0" maxlength="11">
			                    </div>
			                </div>
			            </div>

		                <button type="submit" class=" signbuttons btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button> <a href="{{ route('almacen.index') }}" class=" signbuttons btn btn-primary">Listado</a>
		            </form>

    			</div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
	$(document).ready(function() {
      $("form").keypress(function(e) {
            if (e.which == 13) {
                return false;
            }
        });});
	$('#mini').numeric(false).numeric({negative: false});
	$('#maxi').numeric(false).numeric({negative: false});	
	$('#puni').numeric(",").numeric({decimalPlaces: 2,negative: false});
	/***********************combo anidado producto color ***********************/
       $("#selprod").change(function(event){
       		$("#colprod").empty();
       		$('#colprod').selectpicker('refresh');
       		$.get("../productocol/{{$id}}/"+event.target.value,function(response,alm_id){
       			
       			for(i=0;i<response.length;i++){
       				$("#colprod").append("<option data-subtext='' value='"+response[i].id+"' > "+response[i].nombre+"</option>");       	
       			}  

       		$('#colprod').selectpicker('refresh');     			
       		})
       });
    /*******************************************************************************/

    /***********************combo anidado producto color ***********************/
       $("#nombre").change(function(event){
       		$("#color_bus").empty();
       		$('#color_bus').selectpicker('refresh');
       		$.get("../productocolSi/{{$id}}/"+event.target.value,function(response,alm_id){
       			$("#color_bus").append("<option data-subtext='' value='' >Seleccione color...</option>");       	
       			for(i=0;i<response.length;i++){
       				$("#color_bus").append("<option data-subtext='' value='"+response[i].id+"' > "+response[i].nombre+"</option>");       	
       			}  

       		$('#color_bus').selectpicker('refresh');     			
       		})
       });
    /*******************************************************************************/
</script>
@endpush