@extends('backend.layouts.appv2')

@section('subtitulo', 'Agregar Área al Almacén')

@section('content')


<link href="{{ asset('css/form.css') }}" rel="stylesheet">

<div class="row">
	<div class="col-sm-12">
		<section class="content">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title">Áreas en el Almacén: {{ $nombrealmacen->cAlmNom}}</h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->
		        <div class="box-body">
		        	<div class="col-md-12" style="text-align: right;"> 
			        	<form action="{{ route('areaalmacen.proalm_create',$id) }}" method="GET" >
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="GET">

				            <select class="selectpicker" data-show-subtext="true" data-live-search="true" autofocus="autofocus" name="codbus" id="codbus">
						      	<option data-subtext="" value="">Todas las áreas...</option>
						      	@foreach($areas as $a)
							        <option data-subtext="" value="{{$a->areas->nAreCod}}">{{$a->areas->cAreNom}}</option>	
						        @endforeach
						    </select>	

				            <button type="submit" class="btn btn-success btn-xs" style="width: 36px; height: 33px; margin: -10px;border-radius: 0px 5px 4px 0px;"><span class="fa fa-search" aria-hidden="true"></span></button>
			    		</form>       
			        </div>
			        <br>
			        <br>
		        	<div class="row">
			            <div class="col-md-12"> 
				        	<table class="table table-striped table-bordered table-hover">
		                        <thead>
		                            <th>ID</th>
		                            <th>AREAS</th>		                                                                       
		                            <th>ACCIONES</th>		                                                    
		                        </thead>
		                    @foreach($areaalm as $palm)
		                        <tr colspan="5">		                                                    
		                            <td>{{ $palm->areas->nAreCod }}</td>
		                            <td>{{ $palm->areas->cAreNom }}</td>		                         		                           
									<td>
		                            	<a data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Eliminar" data-trans-title="¿Está seguro?" class="btn btn-xs btn-danger" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
											<i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar"></i>
											
											<form action="{{ route('areaalmacen.proalm_destroy',$palm->nAreAlmCod) }}" method="POST" name="delete_item" style="display:none">
											   {{ csrf_field() }}
												<input type="hidden" name="_method" value="DELETE">
											</form>
										</a>
									</td>
		                        </tr>
		                    @endforeach
		                    </table>
		                    {!! $areaalm->render() !!}
		                </div>
		        </div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<section class="content">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title">@yield('subtitulo'): {{ $nombrealmacen->cAlmNom}}</h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->
		        <div class="box-body">
		            @include('comercializacion.areaalmacen.fragment.error')
		             @include('comercializacion.areaalmacen.fragment.info')

		            <form action="{{ route('areaalmacen.proalm_store',$id) }}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="POST">
		                <div class="form-group">
			                <div class="row">
			                	<div class="col-md-8">
			                		<div class="row-fluid">
			                			<label>Áreas</label>
									      <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" autofocus="autofocus" id="area2" name="area2">
									      	<option data-subtext="" value="">Seleccione las áreas...</option>
									      	@foreach($areas2 as $a2)
										        <option data-subtext="" value="{{$a2->nAreCod}}">{{$a2->cAreNom}}</option>
									        @endforeach
									      </select>								      
								    </div>
			                	</div>	
			                	<div class="col-md-2">
			                		<div class="row-fluid">
			                			<br>
			                			<button type="submit" class=" signbuttons btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Agregar y Guardar</button>						      
								    </div>
			                	</div>	
			                	<div class="col-md-2">
			                		<div class="row-fluid">
			                			<br>
			                			<a href="{{ route('areaalmacen.index') }}" class=" signbuttons btn btn-primary">Listado</a>	
								    </div>
			                	</div>			                	
			                </div>

			                <div class="row">
			                	<div class="col-md-12">
			                		<div class="row-fluid">
			                			<label>Detalles del Área</label>
									      <div class="form-control" style="background-color: #f4f4f4;min-height: 100px; height: 100%; color:#3c8dbc;" id="dareas" name="dareas" readonly></div>					      
								    </div>
			                	</div>				                	
			                </div>
			            </div>
		                
		            </form>

    			</div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>


@endsection

@push('scripts')
<script type="text/javascript">

	/***********************combo anidado producto color ***********************/
       $("#area2").change(function(event){
       
       		$("#dareas").empty();       
       		$.get("../detallearea/"+event.target.value,function(response){       
       			for(i=0;i<response.length;i++){
       				$("#dareas").append("<li> Módulo: "+response[i].cNomMod+
       					//(($.trim(response[i].url)!="")?" Url: "+response[i].url:"")+
       					(($.trim(response[i].cDesMod)!="")?" | Desc.: "+response[i].cDesMod:"")+"</li>");
       			}  
       		})
       });
    /*******************************************************************************/

    /***********************combo anidado producto color ***********************/
       /*$("#nombre").change(function(event){
       		$("#color_bus").empty();
       		$('#color_bus').selectpicker('refresh');
       		$.get("../productocolSi/{{$id}}/"+event.target.value,function(response,alm_id){
       			$("#color_bus").append("<option data-subtext='' value='' >Seleccione color...</option>");       	
       			for(i=0;i<response.length;i++){
       				$("#color_bus").append("<option data-subtext='' value='"+response[i].id+"' > "+response[i].nombre+"</option>");       	
       			}  

       		$('#color_bus').selectpicker('refresh');     			
       		})
       });*/
    /*******************************************************************************/
</script>
@endpush