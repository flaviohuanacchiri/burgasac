@extends('backend.layouts.appv2')

@section('title', 'Listado de Almacenes y Configuración de ÁREAS')

@section('after-styles')
    <style>
    .dropdown{padding: 0;}
    .dropdown .dropdown-menu{border: 1px solid #999}
    .detallescompra{
        display: none;
        background-color: #ececec;
    }
    </style>
@stop

@section('content')

	<div class="row">
		<div class="col-sm-12">
			<section class="content">
			    <div class="box box-info">
			        <div class="box-header with-border">
			            <h3 class="box-title">@yield('title')</h3>
			            <div class="box-tools pull-right">
			                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			            </div><!-- /.box tools -->
			        </div><!-- /.box-header -->
			        <div class="box-body">
			       
						@include('comercializacion.almacen.fragment.info')
						<table class="table table-hover table-striped">
							<thead>
								<tr>
									<th>ÁREAS</th>
									<!--th width="20px">ID</th-->																
									<th>ALMACÉN</th>									
									<th>UBICACIÓN</th>
									<th>SERIE</th>									
									<th>N° FACTURA</th>									
									<th>AG.ÁREAS</th>
								</tr>
							</thead>
							<tbody style="text-align: center;">
								@foreach($almacens as $alm)
								<tr>
									<td>
										<a class="btn btn-xs btn-success detalle">
											<i class="fa fa-eye fa-1x" id="ver" aria-hidden="true"></i>
										</a>
										<!--a class="btn btn-xs btn-info">
											<i class="fa fa-plus-square fa-lg" aria-hidden="true"></i>
										</a-->
									</td>
									<!--td>{{ $alm->nAlmCod }}</td-->
									<td>{{ $alm->cAlmNom }}</td>									
									<td>{{ $alm->cAlmUbi }}</td>
									<td>{{ $alm->nAlmSerAct }}</td>									
									<td>{{ $alm->nAlmNumAct }}</td>
									<td>
										<!--a href="{{ route('almacen.show',$alm->nAlmCod) }}" class="btn btn-xs btn-info">
											<i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"></i>
										</a> 
										<a href="{{ route('almacen.edit',$alm->nAlmCod) }}" class="btn btn-xs btn-primary">
											<i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>
										</a-->
										<a href="{{ route('areaalmacen.proalm_create',$alm->nAlmCod) }}" class="btn btn-xs btn-warning">
											<i class="fa fa-users" data-toggle="tooltip" data-placement="top" title="" data-original-title="Configuración"></i>
										</a>
										<!--a data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Delete" data-trans-title="¿Está seguro?" class="btn btn-xs btn-danger" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
											<i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
											
											<form action="{{ route('almacen.destroy',$alm->nAlmCod) }}" method="POST" name="delete_item" style="display:none">
											   {{ csrf_field() }}
												<input type="hidden" name="_method" value="DELETE">
											</form>
										</a-->
									</td>
								</tr>

								<tr class="detallescompra">
                                    <td colspan="6">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <th>ID</th>
		                            			<th>AREAS</th>                                              
                                                <th>ACCIONES</th>                       
                                            </thead>
                                        @foreach($alm->areasAlmacen as $palm)
                                            <tr colspan="6">		                                                    
                                                <td>{{ $palm->areas->nAreCod }}</td>
		                            			<td>{{ $palm->areas->cAreNom }}</td>	        
												<td>
					                            	<a data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Eliminar" data-trans-title="¿Está seguro?" class="btn btn-xs btn-danger" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
														<i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar"></i>
														
														<form action="{{ route('areaalmacen.proalm_destroy',$palm->nAreAlmCod) }}" method="POST" name="delete_item" style="display:none">
														   {{ csrf_field() }}
															<input type="hidden" name="_method" value="DELETE">
														</form>
													</a>
												</td>
                                            </tr>
                                        @endforeach
                                        </table>

                                    </td>
                                </tr>
								@endforeach
							</tbody>
						</table>
						{!! $almacens->render() !!}
						
			        </div><!-- /.box-body -->
			    </div><!--box box-success-->
			</section>
		</div>
	</div>

@endsection

@push('scripts')

<script type="text/javascript">	
    $(document).bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
        	$(location).attr('href',"{{ route('almacen.create') }}");
        };
    });

    /* show / hide order details */
        $(".detalle").click(function() {
          $(this).closest("tr").next().toggle('fast');
          if($("#ver").attr("class") == 'fa fa-eye fa-1x')
            $("#ver").attr("class", "fa fa-eye-slash fa-1x");
          else
            $("#ver").attr("class", "fa fa-eye fa-1x");
        });
</script>

@endpush('scripts')
