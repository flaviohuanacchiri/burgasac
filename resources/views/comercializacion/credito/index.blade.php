@extends('backend.layouts.appv2')

@section('title', 'Bandeja de Créditos')

@section('after-styles')
    <style>
    .dropdown{padding: 0;}
    .dropdown .dropdown-menu{border: 1px solid #999}
    .detallescompra{
        display: none;
        background-color: #ececec;
    }
    </style>
@stop

@section('content')
	
	<div class="row">
		<div class="col-sm-12">
			<section class="content">
			    <div class="box box-info">
			        <div class="box-header with-border">
			            <h3 class="box-title">@yield('title')</h3>
			            <div class="box-tools pull-right">
			                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			            </div><!-- /.box tools -->
			        </div><!-- /.box-header -->
			        <div class="box-body">
			        	<div class="row">
			        		<form action="{{ route('credito.index') }}" method="GET">
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="GET">
							   
							    <div class="col-md-3">
							    	<label>RUC/DNI</label> 
							        <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" id="rdni" name="rdni" tabindex="1">    
							        	<option data-subtext="" value="">Todos</option>                       
	                                    @foreach($cli as $u)	                                    
	                                        @if($u->nClieCod==$rq->rdni)
	                                        	<option data-subtext="({{$u->cClieNdoc}})" value="{{$u->nClieCod}}" selected>{{$u->cClieDesc}}</option>
	                                        @else
	                                        	<option data-subtext="({{$u->cClieNdoc}})" value="{{$u->nClieCod}}">{{$u->cClieDesc}}</option>
	                                        @endif

	                                    @endforeach
	                                </select>
							    </div>
							    <div class="col-md-4">
							    	<label>Factura/Cod.Venta</label> 
							    	@if($rq->rdni=='')
							        	<input type="text" class="form-control" name="ngven_b" id="ngven_b" maxlength="250" placeholder="Todos..." tabindex="2" value="{{$rq->ngven_b}}">						     
							        @else
							        	<input type="text" class="form-control" name="ngven_b" id="ngven_b" maxlength="250" placeholder="Todos..." tabindex="2" value="">						     
							        @endif
							    </div>
							    <div class="col-md-3">
					        		<label>Saldo</label> 
							        <input type="text" class="form-control" name="saldo_b" id="saldo_b" maxlength="250" tabindex="3" autofocus="autofocus" value="{{$rq->saldo_b}}">	
							    </div>
							    
							    <div class="col-md-2">
							    	<label style="color:white;">1</label>                                	
                                	<button type="submit" class="btn btn-primary form-control"><span class="fa fa-search" aria-hidden="true"></span></button>
	                            </div>
							</form>       
			            </div>
			            <br>

						@include('comercializacion.credito.fragment.info')
						@include('comercializacion.credito.fragment.error')
						<table class="table table-hover table-striped">
							<thead>
								<tr>
									<th>FECHA</th>	
									<!--th>T.DOCUMENTO</th-->	
									<!--th>F.PAGO</th-->	
									
									<th>Cliente</th>
									<th>DNI / RUC</th>
									<th>Factura/Cod.Venta</th>
									<!--th>IMPORTE</th>
									<th>IGV</th-->			
									<th width="70px">Monto de Venta S/.</th>

									<th>Pago S/.</th>
									<th>Saldo S/.</th>
									<th>N.Dev.</th>
									<th colspan="3">ACCIONES</th>
								</tr>
							</thead>
							<tbody style="text-align: center;">
								@foreach($vfacturas as $fac)
								<tr>											
									<td>{{ $fac->dVFacFemi }}</td>	

									<td>{{ $fac->cliente->cClieDesc }}</td>								
									<td>{{ $fac->cliente->cClieNdoc }}</td>	
									<td>{{ ($fac->nTipPagCod==3)?$fac->cFacNumFac:$fac->nVtaCod }}</td>
								
									<td style="text-align: right;">
										{{ number_format($fac->dVFacVTot - $fac->devolucion['dMonFavor'] + $fac->devolucion['dMonContra'],2) }}
									</td>
									
									<td style="text-align: right;color:green;">{{ number_format($fac->pago,2)}}</td>
									<td style="text-align: right;">{{ number_format($fac->saldo ,2)}}</td>								
									<td>
										@if(number_format($fac->dVFacVTot,2)!=(number_format($fac->dVFacVTot - $fac->devolucion['dMonFavor'] + $fac->devolucion['dMonContra'],2)))
											<a href="{{ route('notadevolucion.reporte',$fac->nVtaCod) }}" class="btn btn-xs btn-warning" target="_blank">
												<i class="fa fa-envelope-o" data-toggle="tooltip" data-placement="top" data-original-title="Nota de Devolución"></i>
											</a>  
										@endif
									</td>
									<td style="text-align: left;">
										<a href="{{ route('credito.show',$fac->nVtaCod) }}" class="btn btn-xs btn-info">
											<i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver"></i>
										</a> 
										@if(number_format($fac->dVFacVTot - $fac->devolucion['dMonFavor'] + $fac->devolucion['dMonContra'],2)!=0)
											<a href="{{ route('credito.edit',$fac->nVtaCod) }}" class="btn btn-xs btn-success">
												<i class="fa fa-money" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pagarcredito"></i>
											</a> 
										@endif
                                    </td>
								</tr>
								@endforeach
							</tbody>
						</table>
						{!! $vfacturas->render() !!}
						
			        </div><!-- /.box-body -->
			    </div><!--box box-success-->
			</section>
		</div>
	</div>

@endsection

@push('scripts')

<script type="text/javascript">	

    /* show / hide order details */
        $(".detalle").click(function() {
          $(this).closest("tr").next().toggle('fast');
          if($("#ver").attr("class") == 'fa fa-eye fa-1x')
            $("#ver").attr("class", "fa fa-eye-slash fa-1x");
          else
            $("#ver").attr("class", "fa fa-eye fa-1x");
        });
</script>

@endpush('scripts')