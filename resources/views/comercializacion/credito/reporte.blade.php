<?php
/**
 * Clase que implementa un conversor de números a letras. 
 * @author AxiaCore S.A.S
 *
 */
class NumberToLetterConverter {
  private $UNIDADES = array(
        '',
        'UN ',
        'DOS ',
        'TRES ',
        'CUATRO ',
        'CINCO ',
        'SEIS ',
        'SIETE ',
        'OCHO ',
        'NUEVE ',
        'DIEZ ',
        'ONCE ',
        'DOCE ',
        'TRECE ',
        'CATORCE ',
        'QUINCE ',
        'DIECISEIS ',
        'DIECISIETE ',
        'DIECIOCHO ',
        'DIECINUEVE ',
        'VEINTE '
  );
  private $DECENAS = array(
        'VEINTI',
        'TREINTA ',
        'CUARENTA ',
        'CINCUENTA ',
        'SESENTA ',
        'SETENTA ',
        'OCHENTA ',
        'NOVENTA ',
        'CIEN '
  );
  private $CENTENAS = array(
        'CIENTO ',
        'DOSCIENTOS ',
        'TRESCIENTOS ',
        'CUATROCIENTOS ',
        'QUINIENTOS ',
        'SEISCIENTOS ',
        'SETECIENTOS ',
        'OCHOCIENTOS ',
        'NOVECIENTOS '
  );
  private $MONEDAS = array(
    array('country' => 'Colombia', 'currency' => 'COP', 'singular' => 'PESO COLOMBIANO', 'plural' => 'PESOS COLOMBIANOS', 'symbol', '$'),
    array('country' => 'Estados Unidos', 'currency' => 'USD', 'singular' => 'DÓLAR', 'plural' => 'DÓLARES', 'symbol', 'US$'),
    array('country' => 'El Salvador', 'currency' => 'USD', 'singular' => 'DÓLAR', 'plural' => 'DÓLARES', 'symbol', 'US$'),
    array('country' => 'Europa', 'currency' => 'EUR', 'singular' => 'EURO', 'plural' => 'EUROS', 'symbol', '€'),
    array('country' => 'México', 'currency' => 'MXN', 'singular' => 'PESO MEXICANO', 'plural' => 'PESOS MEXICANOS', 'symbol', '$'),
    array('country' => 'Perú', 'currency' => 'PEN', 'singular' => 'NUEVO SOL', 'plural' => 'NUEVOS SOLES', 'symbol', 'S/'),
    array('country' => 'Reino Unido', 'currency' => 'GBP', 'singular' => 'LIBRA', 'plural' => 'LIBRAS', 'symbol', '£'),
    array('country' => 'Argentina', 'currency' => 'ARS', 'singular' => 'PESO', 'plural' => 'PESOS', 'symbol', '$')
  );
    private $separator = '.';
    private $decimal_mark = ',';
    private $glue = ' CON ';
    /**
     * Evalua si el número contiene separadores o decimales
     * formatea y ejecuta la función conversora
     * @param $number número a convertir
     * @param $miMoneda clave de la moneda
     * @return string completo
     */
    public function to_word($number, $miMoneda = null) {
        if (strpos($number, $this->decimal_mark) === FALSE) {
          $convertedNumber = array(
            $this->convertNumber($number, $miMoneda, 'entero')
          );
        } else {
          $number = explode($this->decimal_mark, str_replace($this->separator, '', trim($number)));
          $convertedNumber = array(
            $this->convertNumber($number[0], $miMoneda, 'entero'),
            $this->convertNumber($number[1], $miMoneda, 'decimal'),
          );
        }
        return implode($this->glue, array_filter($convertedNumber));
    }
    /**
     * Convierte número a letras
     * @param $number
     * @param $miMoneda
     * @param $type tipo de dígito (entero/decimal)
     * @return $converted string convertido
     */
    public function convertNumber($number, $miMoneda = null, $type) {   
        $decimales="";
        if(strpos($number,'.')!="")
        {
            $aux_x=trim(substr($number, strpos($number,'.')+1));
            $decimales=" Y ".((strlen($aux_x)==1)?$aux_x."0":$aux_x)."/100";        
            $number=trim(substr($number, 0, strpos($number,'.')));
        }
        $converted = '';
        if ($miMoneda !== null) {
            try {
                
                $moneda = array_filter($this->MONEDAS, function($m) use ($miMoneda) {
                    return ($m['currency'] == $miMoneda);
                });
                $moneda = array_values($moneda);
                if (count($moneda) <= 0) {
                    throw new Exception("Tipo de moneda inválido");
                    return;
                }
                ($number < 2 ? $moneda = $moneda[0]['singular'] : $moneda = $moneda[0]['plural']);
            } catch (Exception $e) {
                echo $e->getMessage();
                return;
            }
        }else{
            $moneda = '';
        }
        if (($number < 0) || ($number > 999999999)) {
            return false;
        }
        $numberStr = (string) $number;
        $numberStrFill = str_pad($numberStr, 9, '0', STR_PAD_LEFT);
        $millones = substr($numberStrFill, 0, 3);
        $miles = substr($numberStrFill, 3, 3);
        $cientos = substr($numberStrFill, 6);
        if (intval($millones) > 0) {
            if ($millones == '001') {
                $converted .= 'UN MILLON ';
            } else if (intval($millones) > 0) {
                $converted .= sprintf('%sMILLONES ', $this->convertGroup($millones));
            }
        }
        
        if (intval($miles) > 0) {
            if ($miles == '001') {
                $converted .= 'MIL ';
            } else if (intval($miles) > 0) {
                $converted .= sprintf('%sMIL ', $this->convertGroup($miles));
            }
        }
        if (intval($cientos) > 0) {
            if ($cientos == '001') {
                $converted .= 'UN ';
            } else if (intval($cientos) > 0) {
                $converted .= sprintf('%s ', $this->convertGroup($cientos));
            }
        }
        //$converted .= $moneda;
        return $converted." ".$decimales." ".$moneda;
    }
    /**
     * Define el tipo de representación decimal (centenas/millares/millones)
     * @param $n
     * @return $output
     */
    private function convertGroup($n) {
        $output = '';
        if ($n == '100') {
            $output = "CIEN ";
        } else if ($n[0] !== '0') {
            $output = $this->CENTENAS[$n[0] - 1];   
        }
        $k = intval(substr($n,1));
        if ($k <= 20) {
            $output .= $this->UNIDADES[$k];
        } else {
            if(($k > 30) && ($n[2] !== '0')) {
                $output .= sprintf('%sY %s', $this->DECENAS[intval($n[1]) - 2], $this->UNIDADES[intval($n[2])]);
            } else {
                $output .= sprintf('%s%s', $this->DECENAS[intval($n[1]) - 2], $this->UNIDADES[intval($n[2])]);
            }
        }
        return $output;
    }
}
?>
    <style type="text/css">
       .contenedor-tabla
        {
            display: table;
            width: 100%;
            font-size: 8px;
        }

	.contenedor-fila
        {
            display: table-row;            
        }

        .contenedor-columna
        {
            display: table-cell;
            padding-top: 1px;  
	}


        body{
            /**font-family: monospace;*/
            font-family: "Lucida Console", "Lucida Sans Typewriter", monaco, "Bitstream Vera Sans Mono", monospace; 
        }
    </style>
    <title>Guía de Venta</title>
<body>     
    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 100%;text-align: center;font-weight:bold;">
                RECIBO DE PAGO N° {{$datos->cPagCre}}
            </div>
        </div>      
    </div>    

    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="color: white;">
                ff
            </div>
        </div>      
    </div>        

    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 40%;">
                Cliente:
             </div>
             <div class="contenedor-columna" style="width: 5%;">

             </div>
            <div class="contenedor-columna" style="width: 45%;text-align: right;">
                {{$datos->ventas->cliente->cClieDesc}}
            </div>          
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 40%;">
                   Tipo de Documento:
             </div> 
             <div class="contenedor-columna" style="width: 5%;">

             </div>
            <div class="contenedor-columna" style="width: 45%;text-align: right;">
                {{$datos->ventas->tipodocv->cDescTipPago}}
            </div>          
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 40%;">
                N° de Documento:
             </div>
             <div class="contenedor-columna" style="width: 5%;">

             </div>
            <div class="contenedor-columna" style="width: 45%;text-align: right;">
                {{$datos->nVtaCod}}
            </div>          
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 40%;">
                Fecha:
             </div>
             <div class="contenedor-columna" style="width: 5%;">

             </div>
            <div class="contenedor-columna" style="width: 45%;text-align: right;">
               {{$datos->tPagCreFecReg}} 
            </div>          
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 40%;">
                Monto Pagado:
            </div>
            <div class="contenedor-columna" style="width: 5%;">
                S/.
            </div>
            <div class="contenedor-columna" style="width: 45%;text-align: right;">
                {{$datos->dPagCreMonto}}
            </div>          
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 40%;">
                Saldo:
            </div>
            <div class="contenedor-columna" style="width: 5%;">
                S/.
            </div>
            <div class="contenedor-columna" style="width: 45%;text-align: right;">
                {{$datos->ventas->saldo}}
            </div>          
        </div>
    </div>
    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 100%;color:white;">
                    1212
            </div>       
        </div>
        
    </div>
    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 50%;text-align: center;">
                ___________________
            </div>
            <div class="contenedor-columna" style="width: 2%;color: white;">
                _
            </div>
            <div class="contenedor-columna" style="width: 48%;text-align: center;">
                ___________________
            </div>
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 50%;text-align: center;font-size: 7px;">
                {{$datos->ventas->cliente->cClieDesc}}
            </div>
            <div class="contenedor-columna" style="width: 2%;color: white;">
                _
            </div>
            <div class="contenedor-columna" style="width: 48%;text-align: center;font-size: 7px;">
                TEXTIL BURGA SAC
            </div> 
        </div>
    </div>
</body>
