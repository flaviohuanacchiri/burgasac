@extends('backend.layouts.appv2')

@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<style>
  .ui-autocomplete-loading {
    background: white url("../../img/ui-anim_basic_16x16.gif") right center no-repeat;
  }  
</style>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">VISTA DE PAGOS</div>
                <div class="panel-body">

                    <form action="{{ route('credito.store') }}" method="POST">
                
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="POST">
                        <input type="hidden" name="codint" id="codint" value="{{$id}}">
                        <input type="hidden" name="conta" id="conta" value="">
                        <input type="hidden" name="eliminados" id="eliminados" value="">
                        <input type="hidden" name="actualizar" id="actualizar" value="">
                        <input type="hidden" name="cad_actt" id="cad_actt" value="">                        

                        @include('comercializacion.credito.fragment.info')     

                        <div class="panel panel-default">
                            <div class="panel-body" id="dina_control" name="dina_control">
                                <div class="row">                                   
                                    <div class="col-md-4">
                                        <label for="">Cliente</label>        
                                        <input id="clie" type="text" class="form-control" name="clie" readonly value="{{ $fac->cliente->cClieDesc }}">
                                    </div>    
                                    <div class="col-md-4">
                                        <label for="">DNI / RUC</label>
                                        <input id="dniruc" type="text" class="form-control" name="dniruc" readonly value="{{ $fac->cliente->cClieNdoc }}">
                                    </div>          
                                    <div class="col-md-4">
                                        <label for="">Factura/Cod.Venta</label>
                                        <input id="faccodven" type="text" class="form-control" name="faccodven" value="{{ ($fac->nTipPagCod==3)?$fac->cFacNumFac:$fac->nVtaCod }}" readonly>
                                    </div>                                         
                                </div>

                                <div class="row">                                
                                    <div class="col-md-4">
                                        <label for="">Monto de Venta</label>
                                        <input id="mventa" type="text" class="form-control" name="mventa" value="{{ number_format($fac->dVFacVTot - $fac->devolucion['dMonFavor'] + $fac->devolucion['dMonContra'],2) }}" readonly>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Saldo S/.</label>
                                        <input id="saldo_p" type="text" class="form-control" name="saldo_p" value="{{ number_format(($fac->dVFacVTot - $fac->devolucion['dMonFavor'] + $fac->devolucion['dMonContra'])-$fac->pago ,2)}}" readonly>
                                    </div>
                                    <div class="col-md-4 {{ $errors->has('montop') ? 'has-error' :'' }}">
                                        <label for="">Monto a Pagar S/.</label>
                                        <input id="montop" type="number" class="form-control" name="montop" min="0.00" placeholder="00.00" maxlength="17" value="" readonly>
                                        {!! $errors->first('montop','<span class="help-block">:message</span>') !!}                                
                                    </div>
                                                                                              
                                </div>

                            </div>
                        </div>
                    </form>

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <br>
                                        <div id="capa" style="position: absolute;width: 97%;height: 92%;background-color: rgba(19, 19, 19, 0.35);display: none;top: 1px;"></div>
                                        <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
                                            <thead>                     
                                                <th>
                                                    Id
                                                </th>                
                                                <th>
                                                    Fecha
                                                </th>                
                                                <th>
                                                    Cliente
                                                </th>
                                                <th>
                                                    Factura/Cod.Venta
                                                </th>
                                                <th>
                                                    Cod.Pago
                                                </th>
                                                <th>
                                                    Monto S/.
                                                </th>
                                                <th>
                                                    Impresión
                                                </th>
                                              
                                            </thead>
                                            <tbody style="text-align: center;">
                                                <?php $i=0; ?>
                                                @foreach($tabla as $pagos)
                                                <tr>
                                                    <td>
                                                        {{++$i}}
                                                    </td>                
                                                    <td>
                                                        {{$pagos->tPagCreFecReg}}
                                                    </td>                
                                                    <td>
                                                        {{$pagos->ventas->cliente->cClieDesc}}
                                                    </td>
                                                    <td>
                                                        {{ ($pagos->ventas->nTipPagCod==3)?$pagos->ventas->cFacNumFac:$pagos->ventas->nVtaCod }}
                                                    </td>
                                                    <td>
                                                        {{$pagos->cPagCre}}
                                                    </td>
                                                    <td>
                                                        {{$pagos->dPagCreMonto}}
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('credito.reporte',$pagos->cPagCre) }}" target="_blank" class="btn btn-xs btn-default">
                                                            <i class="fa fa-print" data-toggle="tooltip" data-placement="top" title="" data-original-title="Comprobante"></i>
                                                        </a>
                                                    </td>
                                                    
                                                </tr>
                                                @endforeach                                                
                                            </tbody>
                                        </table>
                                       
                                        {!! $tabla->render() !!}

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">                                
                                        <a href="{{ route('credito.index')}}" class="btn btn-danger">Bandeja</a>
                                    </div>
                                </div>

                            </div>
                        </div>

                    
                </div>
            </div>
        </div>
    </div>

<div id="loaderDiv" style="display:none;width:169px;height:189px;position:absolute;top:50%;left:50%;padding:2px;z-index: 2;"><img src='../../img/wait.gif' width="64" height="64" /><br>Loading..</div>


@endsection

@push('scripts')
<script type="text/javascript">
    var indice_tabla=0;
    var key_enter=true;
    var cant_items_real=0;
    var indice_act=1;
    var ultima_pag=1;
    var cantxpag=7;
    var tabla=[];
 
    $(document).ready(function() {
       @if(Session::has('cod'))            
            window.open("{{ route('credito.reporte',Session::get('cod')) }}");
        @endif

        $("form").keypress(function(e) {
            if (e.which == 13) {
                return false;
            }
        });

        doTheClock();

    });

    function doTheClock() 
    {
       window.setTimeout( "doTheClock()", 1000 );
       /*var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
       var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");*/

       t = new Date();
       
       if(document.all || document.getElementById)
       {
          /*var cad = diasSemana[t.getDay()] + ", " + t.getDate() + " de " + meses[t.getMonth()] + " del " + t.getFullYear() + ' ' + t.getHours()+':'+t.getMinutes()+':'+t.getSeconds();*/
          var dia=(t.getDate())+"";
          var mes=(t.getMonth()+1)+"";
          //alert(mes+"");
          $("#fecp").val(t.getFullYear()+"-"+((mes.length==1)?"0"+mes:mes)+"-"+((dia.length==1)?"0"+dia:dia));
          $("#horp").val(t.getHours()+':'+t.getMinutes()+':'+t.getSeconds());
       }else
       {   
          self.status = diasSemana[t.getDay()] + ", " + t.getDate() + " de " + meses[t.getMonth()] + " del " + t.getFullYear() + ' ' + t.getHours()+':'+t.getMinutes()+':'+t.getSeconds();
       }
    }

    
 
</script>
@endpush('scripts')