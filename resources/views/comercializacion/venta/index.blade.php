@extends('backend.layouts.appv2')

@section('title', 'Bandeja de Punto de Ventas')

@section('after-styles')
    <style>
    .dropdown{padding: 0;}
    .dropdown .dropdown-menu{border: 1px solid #999}
    .detallescompra{
        display: none;
        background-color: #ececec;
    }
    </style>
@stop

@section('content')
	
	<div class="row">
		<div class="col-sm-12">
			<section class="content">
			    <div class="box box-info">
			        <div class="box-header with-border">
			            <h3 class="box-title">@yield('title')</h3>
			            <div class="box-tools pull-right">
			                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			            </div><!-- /.box tools -->
			        </div><!-- /.box-header -->
			        <div class="box-body">
			        	<div class="row">
			        		<form action="{{ route('venta.index') }}" method="GET">
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="GET">
					        	
					        	<div class="col-md-3">
					        		<label>Fecha</label> 
							        <input class="form-control" type="date" name="fech_b" id="fech_b" step="1" min="2000-01-01" value="{{$rq->fech_b}}" tabindex="1">
							    </div>
							    <div class="col-md-3">
							    	<label>T. Documento</label> 
							        <select class="form-control" name="tdoc_b" id="tdoc_b">
								      	<option value="">...</option>
								      	@foreach($tdoc as $t)
								      		@if($t->nTipPagCod==$rq->tdoc_b)
									        	<option value="{{$t->nTipPagCod}}" selected>{{$t->cDescTipPago}}</option>	
									        @else
									        	<option value="{{$t->nTipPagCod}}">{{$t->cDescTipPago}}</option>	
									        @endif
								        @endforeach
								    </select>							     
							    </div>
							    <div class="col-md-3">
							    	<label>Forma de Pago</label> 
							        <select class="form-control" name="fpago_b" id="fpago_b">
								      	<option value="">...</option>
								      	@foreach($fpago as $t)
								      		@if($t->nForPagCod==$rq->fpago_b)
									        	<option value="{{$t->nForPagCod}}" selected>{{$t->cDescForPag}}</option>	
									        @else
									        	<option value="{{$t->nForPagCod}}">{{$t->cDescForPag}}</option>	
									        @endif									        
								        @endforeach
								    </select>						     
							    </div>
							    <div class="col-md-3">
							    	<label>N°G.Venta/Factura</label> 
							        <input type="text" class="form-control" name="ngven_b" id="ngven_b" maxlength="250" tabindex="1" value="{{$rq->ngven_b}}">						     
							    </div>
							    <div class="col-md-4">
							    	<label>RUC/DNI</label> 
							        <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" id="rdni" name="rdni">    
							        	<option data-subtext="" value="">...</option>                       
	                                    @foreach($cli as $u)
	                                    	@if($rq->rdni == $u->nClieCod)
	                                    		<option data-subtext="({{$u->cClieNdoc}})" value="{{$u->nClieCod}}" selected>{{$u->cClieDesc}}</option>
	                                    	@else
	                                    		<option data-subtext="({{$u->cClieNdoc}})" value="{{$u->nClieCod}}">{{$u->cClieDesc}}</option>
	                                    	@endif	                                        
	                                    @endforeach
	                                </select>
							    </div>
							    <div class="col-md-4">
							    	<label>Tienda</label> 
							        <select class="form-control" name="tie_b" id="tie_b">
								      	<option value="">...</option>
								      	@foreach($alm as $al)
								      		@if($al->nAlmCod==$rq->tie_b)
									        	<option value="{{$al->nAlmCod}}" selected>{{$al->cAlmNom}}</option>	
									        @else
									        	<option value="{{$al->nAlmCod}}">{{$al->cAlmNom}}</option>	
									        @endif									        
								        @endforeach
								    </select>						     
							    </div>
							    <div class="col-md-4">  
							    	<label style="color:white;">dd</label>                              	
                                	<button type="submit" class="btn btn-primary form-control"><span class="fa fa-search" aria-hidden="true"></span></button>
	                            </div>
							</form>       
			            </div>
			            <br>

						@include('comercializacion.venta.fragment.info')
						@include('comercializacion.venta.fragment.error')
						<table class="table table-hover table-striped">
							<thead>
								<tr>
									<th>DETALLES</th>																		
									<th>TIENDA</th>
									<th>FECHA</th>	
									<th>T.DOCUMENTO</th>	
									<th>F.PAGO</th>	
									<th>G.VENTA/FACTURA</th>
									<th>CLIENTE</th>
									<th>IMPORTE</th>									
									<th>IGV</th>									
									<th>TOTAL</th>
									<th>ESTADO</th>
									<th>PAGO</th>
									<th colspan="3">ACCIONES</th>
								</tr>
							</thead>
							<tbody style="text-align: center;">
								@foreach($vfacturas as $fac)
								<tr>
									<td>
										<a class="btn btn-xs btn-success detalle">
											<i class="fa fa-eye fa-1x" id="ver" aria-hidden="true"></i>
										</a>
									</td>						
									<td>{{ $fac->almacen->cAlmNom }}</td>										
									<td>{{ $fac->dVFacFemi }}</td>	
									<td>{{ $fac->tipodocv->cDescTipPago }}</td>									
									<td>{{ $fac->forpago->cDescForPag }}</td>	
									<td>{{ ($fac->nTipPagCod==3)?$fac->cFacNumFac:$fac->nVtaCod }}</td>									
									<td>{{ $fac->cliente->cClieNdoc }}</td>										
									<td>{{ number_format($fac->dVFacSTot,2) }}</td>									
									<td>{{ number_format($fac->dVFacIgv,2) }}</td>
									<td>{{ number_format($fac->dVFacVTot,2) }}</td>
									<td>{{ ($fac->anulado==1)?'Anulado':'Activo'}}</td>
									<td style="color:green;">{{ number_format($fac->pago,2) }}</td>
								
									<td style="text-align: left;">
										@if($fac->anulado==0)										
										
												<a href="{{ route('venta.reporte',$fac->nVtaCod) }}" target="_blank" class="btn btn-xs btn-default">
													<i class="fa fa-print" data-toggle="tooltip" data-placement="top" title="" data-original-title="Comprobante"></i>
												</a> 
												@if($fac->nTipPagCod==2 || $fac->nTipPagCod==3)
													<a href="{{ route('venta.reportesologuia',$fac->nVtaCod) }}" target="_blank" class="btn btn-xs btn-default">
														<i class="fa fa-sticky-note" data-toggle="tooltip" data-placement="top" title="" data-original-title="Guía de Remisión"></i>
													</a>
												@endif
										
											@if(substr($fac->dVFacFemi,0,strpos($fac->dVFacFemi,' ')) == $fecha)
												<a data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Anular" data-trans-title="¿Está seguro?" class="btn btn-xs btn-danger" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
													<i class="fa fa-ban" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
													
													<form action="{{ route('venta.update',$fac->nVtaCod) }}" method="POST" name="delete_item" style="display:none">
													   {{ csrf_field() }}
														<input type="hidden" name="_method" value="PUT">
													</form>
												</a>
											@endif
										@endif
									
                                    	@if($fac->bDevuelto==1)
                                        	<a href="{{ route('notadevolucion.reporte',$fac->nVtaCod) }}" class="btn btn-xs btn-warning" target="_blank">
												<i class="fa fa-envelope-o" data-toggle="tooltip" data-placement="top" data-original-title="Nota de Devolución"></i>
											</a>                   
										@endif                              	
                                    </td>
								</tr>

								<tr class="detallescompra">
                                    <td colspan="14">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                            	<th>ITEM</th>
                                                <th>PRODUCTO</th>
                                                <th>PESO</th>
                                                <th>ROLLO</th>
                                                <th>PRECIO UNI.</th>                                            
                                                <th>P.VENTA</th>                                                  
                                            </thead>
                                            <?php $conta_x=1;?>
                                        @foreach($fac->ventadetalle as $dfac)
                                            <tr colspan="14">		                         
                                                <td>{{ $conta_x }}</td>                         
						                        @if(is_null($dfac->nProAlmCod)==0)
						                            <td>{{ $dfac->productoAlmacen->producto->nombre_generico }} {{ $dfac->productoAlmacen->color->nombre }}</td>
						                        @else
						                            @if(is_null($dfac->nCodTC)==0)
						                                <td>{{ $dfac->resStocktelas->producto->nombre_generico }} (TC)</td>
						                            @else
						                                @if($dfac->resStockMP->insumo_id==0)
						                                    <td>
						                                        {{ $dfac->resStockMP->accesorio->nombre }} (MP)
						                                    </td>
						                                @else
						                                    <td>
						                                        {{ $dfac->resStockMP->insumo->nombre_generico }} (MP)
						                                    </td>
						                                @endif
						                            @endif
						                        @endif
                                                
                                                <td>{{ $dfac->nVtaPeso }}</td>
                                                <td>{{ $dfac->nVtaCant }}</td>
                                                <td>{{ $dfac->nVtaPrecioU }}</td> 
                                                <td>{{ $dfac->nVtaPreST }}</td>                                                          
                                            </tr>
                                            <?php $conta_x++;?>
                                        @endforeach
                                        </table>
                                    </td>
                                </tr>
								@endforeach
							</tbody>
						</table>
						{!! $vfacturas->render() !!}
						
			        </div><!-- /.box-body -->
			    </div><!--box box-success-->
			</section>
		</div>
	</div>

@endsection

@push('scripts')

<script type="text/javascript">	

    /* show / hide order details */
        $(".detalle").click(function() {
          $(this).closest("tr").next().toggle('fast');
          if($("#ver").attr("class") == 'fa fa-eye fa-1x')
            $("#ver").attr("class", "fa fa-eye-slash fa-1x");
          else
            $("#ver").attr("class", "fa fa-eye fa-1x");
        });
</script>

@endpush('scripts')