@extends('backend.layouts.appv2')

@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<style>
  .ui-autocomplete-loading {
    background: white url("../../img/ui-anim_basic_16x16.gif") right center no-repeat;
  }  
</style>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">PUNTO DE VENTA</div>
                <div class="panel-body">

                <form action="{{ route('venta.store') }}" method="POST" id="venta" name="venta">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default" style="margin-bottom: 8px;">                            
                            <div class="panel-body" style="padding-top: 6px;padding-bottom: 13px;">
                                <div class="row">
                                    <div class="col-md-6" style="    margin: 0px;">
                                        <input id="soles_x" type="text" class="form-control" name="soles_x" readonly value="S/. 0.00" style="    text-align: center;font-weight: bold;font-size: 25px;"> 
                                    </div>
                                    <div class="col-md-6" style="    margin: 0px;">
                                        <input id="dolar_x" type="text" class="form-control" name="dolar_x" readonly value="$/. 0.00" style="    text-align: center;font-weight: bold;font-size: 25px;"> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="codint" id="codint" value="{{$id}}">
                    <input type="hidden" name="conta" id="conta" value="">
                    <input type="hidden" name="eliminados" id="eliminados" value="">
                    <input type="hidden" name="actualizar" id="actualizar" value="">
                    <input type="hidden" name="cad_actt" id="cad_actt" value="">
                    <input type="hidden" name="key_stock" id="key_stock" value="0">
                    @include('comercializacion.venta.fragment.info')               
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="panel panel-default" style="margin-bottom: 8px;">
                                        <div class="panel-heading">Configuración</div>
                                        <div class="panel-body" style="padding-top: 6px;padding-bottom: 13px;">
                                            <div class="row">
                                                <div class="col-md-3" style="    margin: 0px;">
                                                    <label for="">Fecha</label>
                                                    <input id="fecp" type="text" class="form-control" name="fecp" readonly value="{{$fecha}}" style="    font-size: 12px;">                                    
                                                </div>

                                                <div class="col-md-3" style="    margin: 0px;">
                                                    <label for="">Hora</label>
                                                    <input id="horp" type="text" class="form-control" name="horp" readonly value="{{$hora}}" style="    font-size: 12px;">                                    
                                                </div>

                                                <div class="col-md-1" style="    margin: 0px;">
                                                    <label for="" style="margin-left: -12px;" >Mon.</label>
                                                    <input id="ctrans" type="text" class="form-control" name="ctrans" readonly value="D" style="    font-size: 12px;    width: 34px;    margin-left: -12px;">     
                                                </div>
                                                 <div class="col-md-3" style="    margin: 0px;">
                                                    <label for="">Cambio</label>
                                                    <input id="cambio" type="text" class="form-control" name="cambio" readonly value="{{$conf->dCamDolar}}" style="    font-size: 12px;">     
                                                </div>
                                                <div class="col-md-2" style="    margin: 0px;padding-top: 30px;">
                                                    <label for=""><br></label>
                                                     <input type="checkbox" name="sv" id="sv" value="1"> SV
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <BR>
                                    <div class="row">
                                        <div class="col-md-12">                                            
                                            <div class="panel panel-default" style="margin-bottom: 8px;    margin-top: -15px;">
                                                <div class="panel-heading"  style="padding: 4px 15px;">Datos del Cliente</div>
                                                <div class="panel-body" style="padding: 5px 20px 1px 20px;">
                                                    <div class="row">                               
                                                        <div class="col-md-6" style="padding: 8px;">
                                                            <input id="dniruc" type="text" class="form-control" name="dniruc" value=""  placeholder="DNI | RUC" maxlength="11" tabindex="1">
                                                        </div> 
                                                        <div class="col-md-6 {{ $errors->has('dniruc_fijo') ? 'has-error' :'' }}" style="padding: 8px;">                                                            
                                                            <input id="dniruc_fijo" type="number" class="form-control" name="dniruc_fijo" value=""  placeholder="DNI | RUC" maxlength="11" readonly>
                                                            {!! $errors->first('dniruc_fijo','<span class="help-block">:message</span>') !!}
                                                            <input type="hidden" name="cod_cliente" id="cod_cliente" value="">
                                                        </div> 
                                                         
                                                    </div>
                                                    <p id="obs_pro" name="obs_pro" style="color: red;"></p>
                                                    <div class="row"> 
                                                        <div class="col-md-12" style="padding: 0px 8px 0px 8px;">
                                                            <input id="nomcli" type="text" class="form-control" name="nomcli" placeholder="Nombre del Cliente" maxlength="255" readonly value="" style="margin: 0px 0px 10px 0px;">
                                                                                         
                                                            <input id="dircli" type="text" class="form-control" name="dircli" placeholder="Dirección" maxlength="255" readonly value="" style="margin: 0px 0px 10px 0px;">
                                                        
                                                            <input id="obscli" type="text" class="form-control" name="obscli" placeholder="Observación" maxlength="255" readonly value="" style="margin: 0px 0px 10px 0px;">
                                                        </div>
                                                    </div>
                                                    <div class="row pull-right">                 
                                                        <div class="col-md-12" style="padding: 8px;">
                                                            <div  id="savecli" name="savecli" class="btn btn-info" style="display: none;" onclick="guargarCli()">
                                                                <i class="fa fa-floppy-o" aria-hidden="true"></i> Registrar
                                                            </div>
                                                            <div  id="cancelcli" name="cancelcli" class="btn btn-danger" style="display: none;" onclick="cancelarCli()">
                                                                <i class="fa fa-times" aria-hidden="true"></i> Cancelar
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p id="resultado" style="color:red;padding: 5px;"></p>
                                                </div>                                                
                                            </div>                                                    
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="col-md-12">                                            
                                        <div class="panel panel-default" style="margin-bottom: 8px;">
                                            <div class="panel-heading"  style="padding: 4px 15px;">Tipo de Documento</div>
                                            <div class="panel-body" style="padding: 5px 20px 1px 20px;">
                                                <div class="row">                               
                                                    <div class="col-md-12">
                                                        <input id="tdocumento_x" type="text" class="form-control" name="tdocumento_x" value=""  placeholder="GUIA | BOLETA | FACTURA" maxlength="15" autofocus="autofocus">

                                                        <input id="tipdoc" type="hidden" name="tipdoc" value="">
                                                        

                                                        <!--input type="radio" id="radguia" name="tipdoc" value="1" autofocus="autofocus" checked>
                                                        <label>Guía</label>

                                                        <input type="radio" id="radbol" name="tipdoc" value="2">
                                                        <label>Boleta</label>                                                       
                                                        <input type="radio" id="radfac" name="tipdoc" value="3">
                                                        <label>Factura</label-->
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>                                                    
                                    </div>
                                    <div class="col-md-12">                                            
                                        <div class="panel panel-default" style="margin-bottom: 8px;">
                                            <div class="panel-heading" style="padding: 4px 15px;">Formas de Pago</div>
                                            <div class="panel-body" style="padding: 5px 20px 1px 20px;">
                                                <div class="row"  >                               
                                                    <div class="col-md-12">

                                                        <input id="fpago_x" type="text" class="form-control" name="fpago_x" value=""  placeholder="CONTADO | DEPOSITO | CREDITO | CHEQUE" maxlength="15">

                                                        <input id="pago" type="hidden" name="pago" value="">

                                                        <!--input type="radio" id="radcon" name="pago" value="1" checked>
                                                        <label>Contado</label>

                                                        <input type="radio" id="raddep" name="pago" value="2">
                                                        <label>Deposito</label>

                                                        <input type="radio" id="radcre" name="pago" value="3">
                                                        <label>Crédito</label>

                                                        <input type="radio" id="radche" name="pago" value="4">
                                                        <label>Cheque</label-->
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>                                                    
                                    </div>
                                    <div class="col-md-12">                                            
                                        <div class="panel panel-default" style="margin-bottom: 8px;">
                                            <div class="panel-heading"  style="padding: 4px 15px;">Datos de la Factura</div>
                                            <div class="panel-body" style="padding: 5px 20px 1px 20px;">
                                                <div class="row"  >                               
                                                    <div class="col-md-3" style="padding: 4px 8px 4px 8px;"> 
                                                        <input id="dfactu1" type="text" class="form-control" name="dfactu1" placeholder="Serie" maxlength="250"  value="{{ $almacen->nAlmSerAct }}" readonly>
                                                    </div> 
                                                    <div class="col-md-9" style="padding: 4px 8px 4px 8px;"> 
                                                        <input id="dfactu2" type="text" class="form-control" name="dfactu2" placeholder="{{ $cad2 }}" maxlength="6" value="" readonly>
                                                    </div> 
                                                </div>

                                                <div class="row"  >                               
                                                    <div class="col-md-12" style="padding: 4px 8px 4px 8px;"> 
                                                       <select class="form-control" id="banco" name="banco" disabled="disabled">
                                                            <option value='' >Seleccione banco...</option>
                                                            @foreach($banco_x as $b)
                                                                <option value='{{$b->id}}' >{{$b->nombre}}</option>
                                                            @endforeach
                                                       </select> 
                                                    </div> 
                                                    <div class="col-md-12" style="padding: 4px 8px 4px 8px;"> 
                                                        <input id="ncheq" type="text" class="form-control" name="ncheq" placeholder="N° de Cheque" maxlength="250" readonly value="">
                                                    </div>    
                                                    <div class="col-md-12" style="padding: 4px 8px 4px 8px;"> 
                                                        <input id="nope" type="text" class="form-control" name="nope" placeholder="N° Operación" maxlength="250" readonly value="">
                                                    </div> 
                                                    <div class="col-md-12" style="padding: 4px 8px 4px 8px;"> 
                                                        <input id="nguia_zz" type="text" class="form-control" name="nguia_zz" placeholder="N° Guía" maxlength="20" readonly value="">
                                                    </div> 
                                                </div>
                                                <p id="obs_pro2" name="obs_pro2" style="color: red;"></p>
                                            </div>
                                        </div>                                                    
                                    </div>                                    
                                </div>
                            </div>

                        </div>
                    </div>

                    <!--label style="color: red;">Prueba de Código de Barras:</label> <input id="cxxx" type="text"  name="cxxx" value=""  placeholder="prueba el Código aquí..." maxlength="50" style="width: 308px;border: 1px solid red;color:red;" -->

                    <div class="panel panel-default">
                        <div class="panel-body" id="dina_control" name="dina_control">
                            <div class="row">
                                <div class="col-md-2" style="padding-top: 10px;">
                                    <label for="">Tienda</label>                                    
                                    <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" id="almprodes" name="almprodes"> 
                                            <option data-subtext='(Ubi. {{$almacen->cAlmUbi}})' value='{{$almacen->nAlmCod}}' >{{$almacen->cAlmNom}}</option>
                                    </select>
                                </div>

                                <div class="col-md-3" style="padding-top: 10px;">
                                    @if($manauto==0)        
                                        <input type="radio" id="radaut" name="codigo" value="0" checked="checked" disabled>
                                    @else
                                        <input type="radio" id="radaut" name="codigo" value="0" disabled>
                                    @endif
                                    <label for="contactChoice1">Aut.</label>

                                    @if($manauto==1)        
                                        <input type="radio" id="radman" name="codigo" value="1" checked="checked" disabled>
                                    @else
                                        <input type="radio" id="radman" name="codigo" value="1" disabled>
                                    @endif
                                    
                                    <label for="contactChoice2">Man.</label>

                                    <input id="codpro" type="text" class="form-control" name="codpro" value=""  placeholder="Código" maxlength="50" tabindex="2">
                                </div>                                
                                
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <label for="">Producto</label>                                   
                                    <input id="prodes" type="text" class="form-control" name="prodes" value=""  placeholder="Eligir producto.." maxlength="30" tabindex="3">

                                    <input type="hidden" name="subtext" id="subtext" value="">
                                    <input type="hidden" name="esp" id="esp" value="">
                                    <input type="hidden" name="valu" id="valu" value="">

                                    <!--select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" id="prodes" name="prodes" tabindex="17">
                                        <option data-subtext='' value='' >Seleccione un producto...</option>
                                        @foreach($productoAlmacen as $prodAlm)                                        
                                           <option data-subtext='{{$prodAlm->nombre}}' dato-esp='{{$prodAlm->nombre_especifico}}' value='{{$prodAlm->nProAlmCod}}'>{{$prodAlm->nombre_generico}}</option>
                                        @endforeach
                                    </select-->
                                </div>    
                                <div class="col-md-2" style="padding-top: 10px;">
                                    <label for="">Peso</label>
                                    <input id="stock" type="text" class="form-control" name="stock" placeholder="0" maxlength="6" disabled style="font-size: 22px;font-weight: bold;background-color: #a4d4e4;">
                                </div> 
                                 <div class="col-md-2" style="padding-top: 10px;">
                                    <label for="">Rollos</label>
                                    <input id="cant_rollo" type="text" class="form-control" name="cant_rollo" placeholder="0" maxlength="6" disabled style="font-size: 22px;font-weight: bold;background-color: #a4d4e4;">
                                </div>                                                              
                            </div>

                            <div class="row">                                
                                <div class="col-md-2" style="padding-top: 10px;">
                                    <label for="">Peso o Cantidad</label>
                                    <input id="canti_x" type="number" class="form-control" name="canti_x" min="0.00" placeholder="00.00" maxlength="17" value="" readonly="readonly">
                                </div>
                                <div class="col-md-2" style="padding-top: 10px;">
                                    <label for="">Rollos</label>
                                    <input id="canti_rollo_i" type="number" class="form-control" name="canti_rollo_i" min="0.00" placeholder="00.00" maxlength="17" value="" readonly="readonly">
                                </div>
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <label for="">P.Unidad</label>
                                    <input id="puni_x" type="number" class="form-control" name="puni_x" min="0.00" placeholder="00.00" maxlength="17" value="" style="font-size: 22px;font-weight: bold;" readonly>
                                </div>
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <label for="">Subtotal</label>
                                    <input id="subt_x" type="number" class="form-control" name="subt_x" min="0.00" placeholder="00.00" maxlength="17" value="0.00" style="font-size: 25px;font-weight: bold;background-color: #a4d4e4;" readonly>
                                </div>

                                <div class="col-md-2" style="text-align:center;padding-top: 10px;">
                                    <br>
                                    <div  id="add" class="btn btn-primary" onclick="agredardetalle()">Agregar</div>                                    
                                </div>
                                                                
                            </div>

                        </div>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <br>
                                    <div id="capa" style="position: absolute;width: 97%;height: 92%;background-color: rgba(19, 19, 19, 0.35);display: none;top: 1px;"></div>
                                    <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <th>
                                                Id
                                            </th>   
                                            <th>
                                                Código
                                            </th>                                         
                                            <th>
                                                Descripción
                                            </th>
                                            <th>
                                                Peso
                                            </th>
                                            <th>
                                                Rollo
                                            </th>
                                            <th>
                                                P.Unit.
                                            </th>
                                            <th>
                                                Subtotal
                                            </th>
                                            <th>
                                                Quitar
                                            </th>                                            
                                        
                                        </thead>
                                        <tbody style="text-align: center;">
                                            
                                            
                                        </tbody>
                                    </table>
                                   
                                    <ul class="pagination">
                                      
                                    </ul>

                                </div>
                            </div>
                             <div class="row" style="font-size: 20px;    text-align: center;">
                                <div class="col-md-4">
                                    <label for="">Importe</label>
                                    <input id="impo" type="text" class="form-control" name="impo" placeholder="" maxlength="9" value="0.00" readonly style="    font-size: 23px;    text-align: center;">
                                </div>
                                <div class="col-md-4">
                                    <label for="">Igv</label>
                                    <input id="igv" type="text" class="form-control" name="igv" placeholder="" maxlength="9" value="0.00" readonly style="    font-size: 23px;    text-align: center;">
                                </div>
                                <div class="col-md-4">
                                    <label for="">Total</label>
                                    <input id="total" type="text" class="form-control" name="total" placeholder="" maxlength="9" value="0.00" readonly style="    font-size: 27px;    text-align: center;font-weight: bolder;">
                                </div>
                            </div>
                            <br>
                            
                        </div>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">ACCIONES</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-success" name="btn_ejecutar" id="btn_ejecutar" tabindex="4"  data-target="#cobro" title="Cobrar" onclick="totales()">
                                <img src="https://cdn6.aptoide.com/imgs/f/5/b/f5bb6f7b926fe7d82ad7cffb6cd2f8f7_icon.png?w=240" width="100" height="100">
                            </button>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    </p>
    <!-- Modal Escenario-->
    <div class="modal fade" id="cobro" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="$('#cobro').on('hidden.bs.modal', function () {$('#codpro').focus();});"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
            <h4 class="modal-title" id="myModalLabel">Cobro</h4>
          </div>
          <div id="nuevaAventura" class="modal-body">
             <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="nombreAventura">Importe</label>
                    <input type="text" class="form-control" id="p_imp" readonly style="font-size: 25px;font-weight: bold;">
                  </div>
                </div>            
                <div class="col-md-4">      
                  <div class="form-group">
                    <label for="tiempoJuego">Igv</label>
                    <input type="number" class="form-control" id="p_igv" readonly style="font-size: 25px;font-weight: bold;">                      
                  </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="nivelJuego">Total</label>
                        <input type="number" class="form-control" id="p_tot" readonly style="font-size: 25px;font-weight: bold;">
                    </div>  
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="nivelJuego">Pago</label>
                        <input type="text" class="form-control" id="efec" name="efec" style="font-size: 30px;font-weight: bold;color:red;">
                        <p id="obs_vuel" name="obs_vuel" style="color: red;"></p>
                    </div>  
                </div>
            
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="nivelJuego">Vuelto</label>
                        <input type="number" class="form-control" id="vuel" value="0" readonly style="font-size: 30px;font-weight: bold;color:green;">
                    </div>  
                </div>
            </div>
          </div>


          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="limpiarcobro()">Cancelar</button>
            <button type="button" class="btn btn-success" id="botonAventura" onClick="cobrar()">Confirmar</button>        
          </div>
        </div>
      </div>
    </div>

<div id="loaderDiv" style="display:none;width:169px;height:189px;position:absolute;top:50%;left:50%;padding:2px;z-index: 2;"><img src='../../img/wait.gif' width="64" height="64" /><br>Loading..</div>

<!-- Button trigger modal -->
<!--button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Launch demo modal
</button-->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!--h5 class="modal-title" id="exampleModalLabel">Lotes</h5-->
        <button type="button" class="close" data-dismiss="modal" onclick="Limpiar();" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">        
        <select class="form-control" id="lote_TC" name="lote_TC"> 
            <option value='0' >Elegir el Lote...</option>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="Limpiar();" data-dismiss="modal">Close</button>
        <!--button type="button" class="btn btn-primary" onclick="Limpiar()" data-dismiss="modal">Save changes</button-->
      </div>
    </div>
  </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
    var indice_tabla=0;
    var key_enter=true;
    var cant_items_real=0;
    var indice_act=1;
    var ultima_pag=1;
    var cantxpag=7;
    var tabla=[];
    var exigecli=0;
    var igv_g=parseFloat("{{$conf->dIgv}}")+1;

    var ctrl=false;
    var key_f7=true;  
    var key_f8=true;  
    var cont_item_bd=0;  

    $("body").on("keydown",function(event){
      //console.log(event.keyCode);
     
      if(event.keyCode==17) 
      {    
        ctrl=true;
      }
      else if(event.keyCode==117) 
      {
        if(ctrl==true)
        {
            location.reload(true);
        }
      }
      else if(event.keyCode==113) 
      {
        if(ctrl==true)
        {     
            limpiarcontroles();       
            totales();                     
        }
      } 
      
    });

    $(document).ready(function() {
        @if(Session::has('cod'))            
            window.open("{{ route('venta.reporte',Session::get('cod')) }}");
            @if(Session::has('fp'))  
                window.open("{{ route('venta.reportesologuia',Session::get('cod')) }}");            
            @endif
        @endif

        $("form").keypress(function(e) {
            if (e.which == 13) {
                return false;
            }
        });

        doTheClock();

        /*$('#dniruc').keyup(function (){      
          this.value = (this.value + '').replace(/[^0-9]/g, '');
        });*/
        
        $('#canti').numeric(",").numeric({decimalPlaces: 2,negative: false});
        

        /********************** PAGINACIÓN **************************/
        cant_items_real=cont_item_bd;    
        /*** cargar lista de p/ginas***/
        controlpaginacion(cantxpag,cant_items_real);        
        /*** cargar paginación inicial ***/
        paginacion(1,cantxpag);
        $('#dniruc_fijo').numeric(true).numeric({negative: false});
    });

    function doTheClock() 
    {
       window.setTimeout( "doTheClock()", 1000 );
       /*var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
       var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");*/

       t = new Date();
       
       if(document.all || document.getElementById)
       {
          /*var cad = diasSemana[t.getDay()] + ", " + t.getDate() + " de " + meses[t.getMonth()] + " del " + t.getFullYear() + ' ' + t.getHours()+':'+t.getMinutes()+':'+t.getSeconds();*/
          var dia=(t.getDate())+"";
          var mes=(t.getMonth()+1)+"";
          //alert(mes+"");
          $("#fecp").val(t.getFullYear()+"-"+((mes.length==1)?"0"+mes:mes)+"-"+((dia.length==1)?"0"+dia:dia));
          $("#horp").val(t.getHours()+':'+t.getMinutes()+':'+t.getSeconds());
       }else
       {   
          self.status = diasSemana[t.getDay()] + ", " + t.getDate() + " de " + meses[t.getMonth()] + " del " + t.getFullYear() + ' ' + t.getHours()+':'+t.getMinutes()+':'+t.getSeconds();
       }
    }

    function doscerosdecimal(num)
    {
        array=num.split('.');
        if(array.length>1)
        {
            if(array[1].length==1)
                num=num+"0";            
            else if(array[1].length==0)
                num=num+".00";  
        }
        else
            num=num+".00";  

        return num; 
    }  

    $('#sv').on( 'click', function() {
        if( $(this).is(':checked') ){
            // Hacer algo si el checkbox ha sido seleccionado
            $("#key_stock").val("1");
        } else {
            // Hacer algo si el checkbox ha sido deseleccionado
            //alert("El checkbox con valor " + $(this).val() + " ha sido deseleccionado");
            $("#key_stock").val("0");
        }
    });
    /************************** Codigodebarras *************************************/
    $("#codpro").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            codpro_xx=$.trim($("#codpro").val()).split("'").join("-");          

            if(codpro_xx!="")
            {
                $.ajax({
                  type: "GET",
                  url: "veri_codbarra/"+codpro_xx+"/"+$("#almprodes").val(),
                  //data: "5454",
                  dataType: "json",
                  error: function(){
                    alert("error petición ajax");
                    //agredardetalle();
                  },
                  beforeSend: function(){
                       $("#loaderDiv").show();
                   },
                  success: function(data){     
                    if(data!="")//data[0].ning_id
                    {   
                        //prod 
                        var key=false;   
                   
                        $("#prodes option[value="+ data[0].nProAlmCod +"]").attr("selected",true);

                        $("#prodes").val(data[0].nombre_generico+" | "+data[0].nombre);
                        $("#subtext").val(data[0].nombre);
                        $("#esp").val(data[0].nombre_especifico);
                        $("#valu").val(data[0].nProAlmCod);

                        var auxi=((typeof(tabla[data[0].nProAlmCod]) === "undefined")?data[0].nProdAlmStock:data[0].nProdAlmStock-tabla[data[0].nProAlmCod]).toFixed(2);

                        $("#stock").val(auxi);                            
                        $("#cant_rollo").val(isNaN(parseFloat(auxi)/20)?0:parseFloat(auxi)/20);
                        
                        $("#puni_x").val(data[0].nProdAlmPuni);
                        //evaluar si es pero o rollo y boquearlo
                        var puni_xx=parseFloat(data[0].nProdAlmPuni);
                        if(data[0].nombre_especifico =="TELAS")
                        {
                            $("#canti_x").val(data[0].peso_cant);
                            $("#canti_rollo_i").val(parseFloat(data[0].peso_cant)/20);
                            $("#canti_x").prop( "readonly", false );
                            //$("#canti_x").prop( "readonly", true );
                            $("#canti_rollo_i").prop( "readonly", true );
                            $("#canti_x").focus();
                            $("#subt_x").val((puni_xx*parseFloat(data[0].peso_cant)).toFixed(2));
                        }
                        else
                        {
                            $("#canti_x").val("0");
                            $("#canti_rollo_i").val(data[0].rollo);
                            $("#canti_rollo_i").prop( "readonly", false );
                            //$("#canti_rollo_i").prop( "readonly", true );
                            $("#canti_x").prop( "readonly", true );
                            $("#canti_rollo_i").focus();
                            $("#subt_x").val((puni_xx*parseFloat(data[0].rollo)).toFixed(2));
                        }    
                        
                        if(auxi>0)
                        {
                            @if($manauto==0) 
                                agredardetalle();
                            @endif
                            
                        }
                    
                        //$('#prodes').selectpicker('refresh');
                       
                    }
                    else
                    {
                        alert("No existe este código de barras: "+codpro_xx); 
                        limpiarcontroles();                  
                    }
                    $("#loaderDiv").hide();
                  }
                });
            }
            else
            {
                $("#prodes").val("");  
                $("#prodes").focus();                
            }
        }
        else if ( e.which == 27 ) 
        {
            limpiarcontroles();
            $("#dniruc").focus();
            $("#dniruc").val("");
        }
    });

    /************************** Tipo documento *************************************/
   
    $("#tdocumento_x").on('keyup', function(){
        var value = $(this).val();   
        
        $( "#tdocumento_x" ).autocomplete({
          source: "autoTipoDoc/"+value,
          minLength: 1,
          select: function( event, ui ) {
            $("#tipdoc").val(ui.item.id);  
            switch(ui.item.id+"")
            {
                case "2": /*$("#radcre").prop("disabled", true);*/
                        exigecli=0; 
                        $("#dfactu2").prop( "readonly", false ); 
                        $("#nguia_zz").prop( "readonly", false ); 
                        break;
                case "3": /*$("#radcre").prop("disabled", true);*/
                        exigecli=11; 
                        $("#dfactu2").prop( "readonly", false ); 
                        $("#nguia_zz").prop( "readonly", false ); 
                        break;
                default: /*$("#radcre").prop("disabled", false);*/
                        exigecli=0;
                        $("#dfactu2").prop( "readonly", true ); 
                        $("#dfactu2").val("");
                        $("#nguia_zz").prop( "readonly", true );
                        $("#nguia_zz").val("");
            }          
          }
        });
    }).keyup();
    /************************** Tipo documento *************************************/
    /*$("input[name=pago]").click(function () {    
        //$('input:radio[name=tipdoc]:checked').val();
        //alert($(this).val());
        var tdoc=$(this).val();   
        //alert(tdoc);
        switch(tdoc)
        {
            case "1": 
            $("#ncheq").val("");
            $("#nope").val("");
            $('#banco').val("");    
            $("#banco").prop("disabled", true);
            $("#ncheq").prop("readonly", true);
            $("#nope").prop("readonly", true);
            break;
            case "2":             
            //$("#banco").val("");
            //cargarbanco();
            $('#banco').val("");    
            $("#ncheq").val("");
            $("#nope").val("");
            $("#banco").prop("disabled", false);
            $("#ncheq").prop("readonly", true);
            $("#nope").prop("readonly", false);
            break;
            case "3": 
            //$("#radguia").prop( "checked", true );
            
            $('#banco').val("");    
            $("#ncheq").val("");
            $("#nope").val("");

            $("#banco").prop("disabled", true);
            $("#ncheq").prop("readonly", true);
            $("#nope").prop("readonly", true); 
            break;
            case "4":             
            //$("#banco").val("");
            //cargarbanco();
            $("#ncheq").val("");
            $("#nope").val("");
            $('#banco').val("");    
            $("#banco").prop("disabled", false);
            $("#ncheq").prop("readonly", false);
            $("#nope").prop("readonly", false); 
            break;
            //default: $("#radcre").prop("disabled", false);exigecli=0;
        }     
    });*/

    $("#fpago_x").on('keyup', function(){
        var value = $(this).val();   
        
        $( "#fpago_x" ).autocomplete({
          source: "autofORpaG/"+value,
          minLength: 1,
          select: function( event, ui ) {
            $("#pago").val(ui.item.id);  

            switch(ui.item.id+"")
            {
                case "1": 
                $("#ncheq").val("");
                $("#nope").val("");
                $('#banco').val(""); 
                $('#nguia_zz').val("");      
                $("#banco").prop("disabled", true);
                $("#ncheq").prop("readonly", true);
                $("#nope").prop("readonly", true);
                break;
                case "2":             
                //$("#banco").val("");
                //cargarbanco();
                $('#banco').val("");    
                $("#ncheq").val("");
                $("#nope").val("");
                $('#nguia_zz').val("");   
                $("#banco").prop("disabled", false);
                $("#ncheq").prop("readonly", true);
                $("#nope").prop("readonly", false);
                break;
                case "3": 
                //$("#radguia").prop( "checked", true );
                
                $('#banco').val("");    
                $("#ncheq").val("");
                $("#nope").val("");
                $('#nguia_zz').val("");   
                $("#banco").prop("disabled", true);
                $("#ncheq").prop("readonly", true);
                $("#nope").prop("readonly", true); 
                break;
                case "4":             
                //$("#banco").val("");
                //cargarbanco();
                $("#ncheq").val("");
                $("#nope").val("");
                $('#banco').val(""); 
                $('#nguia_zz').val("");   
                $("#banco").prop("disabled", false);
                $("#ncheq").prop("readonly", false);
                $("#nope").prop("readonly", false); 
                break;
                //default: $("#radcre").prop("disabled", false);exigecli=0;
            }              
          }
        });
    }).keyup();
    /************************** cliente, registro y busqueda ******************/
   
    $("#dniruc").on('keyup', function(){
        var value = $(this).val();   
        
        $( "#dniruc" ).autocomplete({
          source: "autocomplete/"+value,
          minLength: 1,
          select: function( event, ui ) {
            //var msn=ui.item.id ;
            /*$( "<div>" ).text(msn).prependTo( "#log" );
            $( "#log" ).scrollTop( 0 );  */         
            //alert(ui); 
            asigdniruc(ui.item.id,ui.item.cod);
          }
        });
    }).keyup();

    function asigdniruc(valor,dato)
    {

       // if($(this).val().length==10)
        //{
            //if ( e.which == 13) 
            //{
                $("#resultado").html("");                   
                $("#cod_cliente").val("");

                $("#savecli").hide();
                $("#cancelcli").hide();
                $("#dniruc").attr("readonly", false);
                //$("#dniruc").val("");

                $("#dniruc_fijo").attr("readonly", true);
                $("#nomcli").attr("readonly", true);
               
               $("#dircli").attr("readonly", true);
                $("#obscli").attr("readonly", true);

                $("#dniruc_fijo").val(""); 
                $("#cod_cliente").val("")        
                $("#nomcli").val("");
                $("#dircli").val("");
                $("#obscli").val(""); 

                //var valor = $("#dniruc").val();
                //alert(valor);
                if(valor!=0 || valor=="")
                {

                    $.get("buscli/"+valor+"",function(response,pstock)
                    {      
                        if(typeof(response.cClieNdoc) !== "undefined")
                        {       
                            $("#dniruc_fijo").val(response.cClieNdoc); 
                            $("#cod_cliente").val(response.nClieCod)        
                            $("#nomcli").val(response.cClieDesc);
                            $("#dircli").val(response.cClieDirec);
                            $("#obscli").val(response.cClieObs); 
                            //$("#prodes").focus();
                            $("#codpro").focus();

                            $("#obs_pro").empty();
                            $("#dniruc").css("border","1px solid #d2d6de");                   
                            $("#dniruc_fijo").css("border","1px solid #d2d6de");  
                            $(".help-block").empty(); 
                            $(".alert").hide(); 
                        }
                                      
                    });
                }
                else
                {
                    if(confirm('El dni|ruc no existe, desea registrarlo?'))
                    {                            
                        $("#savecli").show();
                        $("#cancelcli").show();
                        $("#dniruc").attr("readonly", true);
                        $("#dniruc").val("");                        
                        //$('#prodes').attr('disabled', this.checked).selectpicker('refresh');
                        //$("#dniruc_fijo").val(valor);
                        $("#dniruc_fijo").val(dato);
                        $("#cod_cliente").val("");
                        $("#nomcli").val("");
                        $("#dircli").val("");
                        $("#obscli").val("");

                        $("#dniruc_fijo").attr("readonly", false);
                        $("#nomcli").attr("readonly", false);
                        $("#dircli").attr("readonly", false);
                        $("#obscli").attr("readonly", false);

                        $("#dniruc_fijo").focus();
                        $("#obs_pro").empty();
                         $("#dniruc").css("border","1px solid #d2d6de"); 
                    }
                }
            //}
        //}
        //else
            //alert("Para emitir la factura necesitamos que ingrese ");
    }

    $("#dniruc").bind('keydown',function(e){
        if ( e.which == 13 && $.trim($("#dniruc").val()).length<2) 
        {
            $("#codpro").focus();
            $('body,html').animate({scrollTop : 600}, 0);
        }
        else if ( e.which == 27 ) 
        {
            //$("#radguia").focus();
            $("#tdocumento_x").focus();
        }
    });

    $("#dniruc_fijo").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#nomcli").focus();
        }
        else if ( e.which == 27 ) 
        {
            cancelarCli();
        }
    });
    $("#nomcli").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#dircli").focus();
        }
        else if ( e.which == 27 ) 
        {
            cancelarCli();
        }
    });
    $("#dircli").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#obscli").focus();
        }
        else if ( e.which == 27 ) 
        {
            cancelarCli();
        }
    });
    $("#obscli").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {   
            if(confirm('¿Desea Guardar?'))
            {
                guargarCli();
                $('body,html').animate({scrollTop : 600}, 0);
            }
            else
            {
                $("#dniruc_fijo").focus();       
            }
        };
    });
    
    $("#tdocumento_x").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#fpago_x").focus();
        }
    });
 
    $("#fpago_x").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            switch($("#pago").val())
            {
                case "1":
                    if($("#tipdoc").val()==3 || $("#tipdoc").val()==2)
                        $("#dfactu2").focus();
                    else   
                    {
                        $("#dniruc").focus();
                        $("#dniruc").val("");
                    }                
                break;
                case "2":
                    if($("#tipdoc").val()==3 || $("#tipdoc").val()==2)
                        $("#dfactu2").focus();
                    else    
                        $("#banco").focus();
                break;
                case "3":
                    if($("#tipdoc").val()==3 || $("#tipdoc").val()==2)
                        $("#dfactu2").focus();
                    else   
                    {
                        $("#dniruc").focus();
                        $("#dniruc").val("");
                    }   
                break;
                case "4":
                    if($("#tipdoc").val()==3 || $("#tipdoc").val()==2)
                        $("#dfactu2").focus();
                    else    
                        $("#banco").focus();
                break;
            }
            
        }
        else if ( e.which == 27 ) 
        {
            $("#tdocumento_x").focus();
        }
    });

    $("#dfactu2").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            switch($("#pago").val())
            {
                case "1":
                $("#nguia_zz").focus();
                //$("#dniruc").val("");
                break;
                case "2":                      
                    $("#banco").focus();
                break;
                case "3":
                $("#nguia_zz").focus();
                //$("#dniruc").val("");
                break;
                case "4":                    
                    $("#banco").focus();
                break;
            }
            
        }
        else if ( e.which == 27 ) 
        {
            $("#fpago_x").focus();
        }
    });
    
    /*$("#radcon").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#dniruc").focus();
            $("#dniruc").val("");
        }
        else if ( e.which == 27 ) 
        {
            $("#radguia").focus();
        }
    });
    */$("#dfactu1").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#dfactu2").focus();
        }
        else if ( e.which == 27 ) 
        {
            $("#fpago_x").focus();
        }
    });
    $("#banco").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#ncheq").focus();
        }
        else if ( e.which == 27 ) 
        {
             if($("#tipdoc").val()==3 || $("#tipdoc").val()==2){
                $("#dfactu2").focus();
                $("#dfactu2").val("");
            }
            else    
                $("#fpago_x").focus(); 
            
        }
    });

    $("#ncheq").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#nope").focus();
        }
        else if ( e.which == 27 ) 
        {
            //$("#banco").focus();
            $("#dfactu2").focus();
        }
    });    

    $("#nope").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#nguia_zz").focus();         
            //$('body,html').animate({scrollTop : 600}, 0);
        }
        else if ( e.which == 27 ) 
        {
            $("#ncheq").focus();
        }
    });  

    $("#nguia_zz").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#dniruc").focus();
            $("#dniruc").val("");
            //$('body,html').animate({scrollTop : 600}, 0);
        }
        else if ( e.which == 27 ) 
        {
            $("#nope").focus();
        }
    }); 

    /*$("#radcre").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#dniruc").focus();
            //$('body,html').animate({scrollTop : 600}, 0);
        }
        else if ( e.which == 27 ) 
        {
            $("#radguia").focus();
        }
    });

    $("#radche").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#banco").focus();
        }
        else if ( e.which == 27 ) 
        {
            $("#radguia").focus();
        }
    });*/

    $("#cobro").bind('keydown',function(e){
        /*if ( e.which == 13 ) 
        {
            alert("enter");
        }
        else */
        if ( e.which == 27 ) 
        {
            $("#cobro").on('hidden.bs.modal', function () {                            
                $('#codpro').focus();
            });
        }
    });

    $("#btn_ejecutar").bind('keydown',function(e){
        if ( e.which == 9 ) 
        {
            $("#dniruc").focus();
            $("#dniruc").val("");
        }
    });


    /************************** fin: sacando stock del combo productos ******************/
    $tc_venta=true;
    $("#prodes").on('keyup', function(){
        var value = $(this).val();   
        //var TC=$("#tc").is(":checked");
        //if(!TC)
        //{
            $( "#prodes" ).autocomplete({
              source: "autocomplete_producto/"+$("#almprodes option:selected").val()+"/"+value,
              minLength: 1,
              select: function( event, ui ) {
                //var msn=ui.item.id ;
                //$( "<div>" ).text(msn).prependTo( "#log" );
                $( "#log" ).scrollTop( 0 );  
                var cad_xyz=ui.item.nProAlmCod+"";
                if(cad_xyz.substring(0, 2)=="TC")
                {         
                    $("#lote_TC").empty();

                    $.get("combo_lotes/"+ui.item.nProAlmCod.substring(2),function(response)
                    {
                        $("#lote_TC").append("<option value='0' >Elegir el Lote...</option>");  
                        for(i=0;i<response.length;i++)
                        {
                            $("#lote_TC").append("<option value='"+response[i].id+"' cant='"+response[i].cantidad+"' roll='"+response[i].rollos+"' esp='TELAS'  > "+response[i].nro_lote+"</option>");
                        }
                    }); 

                    $('#exampleModal').modal('show');                    
                    $tc_venta=false;
                }
                else if(cad_xyz.substring(0, 2)=="MP")
                {   
                    traer_producto_mp(ui.item.nProAlmCod,"TELAS",ui.item.nombre,ui.item.cantidad,ui.item.rollos);
                    $tc_venta=false;
                }
                else
                {                    
                    traer_producto(ui.item.nProAlmCod,ui.item.nombre_especifico,ui.item.nombre);
                    $tc_venta=true;
                }
                
              }
            });    
        /*}      
        else
        {            
            $( "#prodes" ).autocomplete({
              source: "autocomplete_producto_TC/"+value,
              minLength: 1,
              select: function( event, ui ) {
                var msn=ui.item.id ;
                $( "<div>" ).text(msn).prependTo( "#log" );
                $( "#log" ).scrollTop( 0 );  
                    
                    //alert(ui.item.nombre_especifico); 
                 

                    $("#lote_TC").empty();

                    $.get("combo_lotes/"+ui.item.id,function(response){
                    $("#lote_TC").append("<option value='0' >Elegir el Lote...</option>");  
                    for(i=0;i<response.length;i++)
                    {
                        $("#lote_TC").append("<option value='"+response[i].id+"' cant='"+response[i].cantidad+"' roll='"+response[i].rollos+"' esp='"+ui.item.nombre_especifico+"'  > "+response[i].nro_lote+"</option>");
                    }                   
                               
                });                   
                $('#exampleModal').modal('show');

                
              }
            });  

        } */     
        
    }).keyup();

    $("#lote_TC").change(function()
    {
        if($(this).val()!="0")
        {            
            traer_producto_TC("TC"+$(this).val(),
                $("#lote_TC option:selected").attr("esp"),
                "sin color",
                parseFloat($("#lote_TC option:selected").attr("cant")),
                parseFloat($("#lote_TC option:selected").attr("roll")));
                //parseFloat($("#lote_TC option:selected").attr("puni"))); 

            $('#exampleModal').modal('hide');
        }
        
    });    


    $("#prodes").bind('keydown',function(e){
        if(""==$.trim($("#prodes").val()))
        {
            if ( e.which == 13 ) 
            {                
                $("#btn_ejecutar").focus();
            }
            else if ( e.which == 27 ) 
            {
                $("#codpro").focus();
            }           
        }
    });

    function traer_producto(valu,esp,color){
        
        $("#stock").val("0");        
        $("#canti_x").val("");
        $("#cant_rollo").val("");
        //$("#unit_x").val("0");
        $("#puni_x").val("0");            
        $("#subt_x").val("0.00"); 
        $("#canti_x").prop( "readonly", true );
        $("#codpro").val();

        $("#subtext").val(color);
        $("#esp").val(esp);
        $("#valu").val(valu);
           
        //Verifica si ingreso el usuario para jalar precios relacionados anteriormente
        if($.trim($("#cod_cliente").val())=="")
        {
            $("#dniruc").css("border","2px solid #f00");     
            $("#resultado").html("- Primero ingrese DNI|RUC\n");   
            $("#dniruc").focus(); 
        }

        $.get("prostock/"+valu+"/"+$("#cod_cliente").val(),function(response,alm_id){   
           
            var auxi=((typeof(tabla[valu]) === "undefined")?response.nProdAlmStock:response.nProdAlmStock-tabla[valu]).toFixed(2);

            if(esp=="TELAS")
            {
                $("#stock").val(auxi);
                $("#stock").css("border","1px solid #d2d6de");    
                //calcular rollo
                $("#cant_rollo").val((parseFloat(auxi)/20).toFixed(2));
                $("#cant_rollo").css("border","1px solid #d2d6de"); 
            }
            else
            {
                //alert(auxi);
                //peso =0
                $("#stock").val("1");
                $("#stock").css("border","1px solid #d2d6de");  
                //asignar rollo
                $("#cant_rollo").val(auxi);
                $("#cant_rollo").css("border","1px solid #d2d6de"); 
            }

            $("#puni_x").val(response.nProdAlmPuni);
            

            if(auxi==0){
                $("#canti_x").prop( "readonly", true );      
            }
            else
            {
                if(esp=="TELAS")
                {
                    $("#canti_x").prop( "readonly", false );  
                    $("#canti_x").focus();
                }
                else
                {
                    $("#canti_rollo_i").prop( "readonly", false );  
                    $("#canti_rollo_i").focus();   
                }
            }

        });
    }

    function Limpiar()
    {
        $("#prodes").val("");
    }

    function traer_producto_TC(valu,esp,color,cant_x,roll_x){
      
        $("#stock").val("0");        
        $("#canti_x").val("");
        $("#cant_rollo").val("");
        //$("#unit_x").val("0");
        $("#puni_x").val("0");            
        $("#subt_x").val("0.00"); 
        $("#canti_x").prop( "readonly", true );
        $("#codpro").val();

        $("#subtext").val(color);
        $("#esp").val(esp);
        $("#valu").val(valu);
           
        var cant_w=0;
        var roll_w=0;
        
            var auxi=0;

            if(typeof(tabla[valu]) === "undefined")
            {
                if(esp=="TELAS")
                {
                    auxi=cant_x.toFixed(2);
                }
                else
                {
                    auxi=roll_x.toFixed(2);
                }                
            }
            else
            {
                if(esp=="TELAS")
                {
                    auxi=(cant_x-tabla[valu]).toFixed(2);
                }
                else
                {
                    auxi=(roll_x-tabla[valu]).toFixed(2)
                }
            }
            
            if(esp=="TELAS")
            {
                $("#stock").val(auxi);
                $("#stock").css("border","1px solid #d2d6de");    
                //calcular rollo
                $("#cant_rollo").val(roll_x);
                $("#cant_rollo").css("border","1px solid #d2d6de"); 
                
            }
            else
            {
                //alert(auxi);
                //peso =0
                $("#stock").val("1");
                $("#stock").css("border","1px solid #d2d6de");  
                //asignar rollo
                $("#cant_rollo").val(auxi);
                $("#cant_rollo").css("border","1px solid #d2d6de"); 
                
            }

            //$("#puni_x").val(puni_x);
            $("#exampleModal").on('hidden.bs.modal', function () {                            
                $("#puni_x").prop( "readonly", false );  
            }); 
            
            if(auxi==0)
                $("#canti_x").prop( "readonly", true );      
            else
            {
                if(esp=="TELAS")
                {
                    $("#canti_x").prop( "readonly", false );  
                    $("#exampleModal").on('hidden.bs.modal', function () {                            
                        $("#canti_x").focus();
                    }); 
                    
                }
                else
                {
                    $("#canti_rollo_i").prop( "readonly", false );  
                    $("#exampleModal").on('hidden.bs.modal', function () {                            
                        $("#canti_rollo_i").focus();
                    }); 
                       
                }
            }

        //});
    }

    function traer_producto_mp(valu,esp,color,cant_x,roll_x){
      
        $("#stock").val("0");        
        $("#canti_x").val("");
        $("#cant_rollo").val("");
        //$("#unit_x").val("0");
        $("#puni_x").val("0");            
        $("#subt_x").val("0.00"); 
        $("#canti_x").prop( "readonly", true );
        $("#codpro").val();

        $("#subtext").val(color);
        $("#esp").val(esp);
        $("#valu").val(valu);
           
        var cant_w=0;
        var roll_w=0;        
        var auxi=0;

        if(typeof(tabla[valu]) === "undefined")
        {
            if(esp=="TELAS")
            {
                auxi=cant_x.toFixed(2);
            }
            else
            {
                auxi=roll_x.toFixed(2);
            }                
        }
        else
        {
            if(esp=="TELAS")
            {
                auxi=(cant_x-tabla[valu]).toFixed(2);
            }
            else
            {
                auxi=(roll_x-tabla[valu]).toFixed(2)
            }
        }
        
        if(esp=="TELAS")
        {
            $("#stock").val(auxi);
            $("#stock").css("border","1px solid #d2d6de");    
            //calcular rollo
            //$("#cant_rollo").val((parseFloat(auxi)/20).toFixed(2));
            $("#cant_rollo").val(roll_x);
            $("#cant_rollo").css("border","1px solid #d2d6de"); 
            
        }
        else
        {
            //alert(auxi);
            //peso =0
            $("#stock").val("1");
            $("#stock").css("border","1px solid #d2d6de");  
            //asignar rollo
            $("#cant_rollo").val(auxi);
            $("#cant_rollo").css("border","1px solid #d2d6de"); 
            
        }

        //$("#puni_x").val(puni_x);
        //$("#exampleModal").on('hidden.bs.modal', function () {                            
            $("#puni_x").prop( "readonly", false );  
        //}); 
        
        if(auxi==0)
            $("#canti_x").prop( "readonly", true );      
        else
        {
            if(esp=="TELAS")
            {
                $("#canti_x").prop( "readonly", false );  
                //$("#exampleModal").on('hidden.bs.modal', function () {                            
                    $("#canti_x").focus();
                //}); 
                
            }
            else
            {
                $("#canti_rollo_i").prop( "readonly", false );  
                //$("#exampleModal").on('hidden.bs.modal', function () {                            
                    $("#canti_rollo_i").focus();
                //}); 
                   
            }
        }

        //});
    }

   

    $("#canti_x").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            if($tc_venta)            
            {
                agredardetalle(); 
                $('body,html').animate({scrollTop : 500}, 0);
                //calcularsubtotal(num);
            }
            else
            {
                $("#puni_x").focus();
                $("#puni_x").val("");
            }            
        }
        else if ( e.which == 27 ) 
        {
            limpiarcontroles();
            $("#codpro").focus();
        }
    });

    $("#canti_x").on('keyup', function(){
        var value = $(this).val();    
        var pun=$("#puni_x").val();        

        if(typeof(value) !== "undefined" && value!="" && typeof(pun) !== "undefined" && pun!="" )
        {
            calcularsubtotal(value);
            $("#canti_rollo_i").val(calcularrollo(parseFloat(value)));
        }
        else{         
            $("#subt_x").val("0.00"); 
        }     
    }).keyup();


    $("#puni_x").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            agredardetalle(); 
            //$("#puni_x").attr("readonly", true);
            $('body,html').animate({scrollTop : 500}, 0);
            //calcularsubtotal(num);
        }
        else if ( e.which == 27 ) 
        {
            //limpiarcontroles();
            if ($("#esp").val()=="TELAS")
                $("#canti_x").focus();
            else
                $("#canti_rollo_i").focus();
        }
    });

    $("#puni_x").on('keyup', function(){
        var value = $(this).val(); 

        var valuenum = ($("#esp").val()=="TELAS")?$("#canti_x").val():$("#canti_rollo_i").val();    

        if(typeof(value) !== "undefined" && value!="" && typeof(valuenum) !== "undefined" && valuenum!="")
        {            
            var unimed=parseFloat(value);                   
            var can=parseFloat(valuenum);       
            var sub=(can*unimed);
            $("#subt_x").val(sub.toFixed(2)); 
        }
        else{         
            $("#subt_x").val("0.00"); 
        }     
    }).keyup();


    $("#canti_rollo_i").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            if($tc_venta)            
            {
                agredardetalle(); 
                $('body,html').animate({scrollTop : 500}, 0);
                //calcularsubtotal(num);
            }
            else
            {
                $("#puni_x").focus();
                $("#puni_x").val("");
            }              
        }
        else if ( e.which == 27 ) 
        {
            limpiarcontroles();
            $("#codpro").focus();
        }
    });

    $("#canti_rollo_i").on('keyup', function(){
        var value = $(this).val();            
        var pun=$("#puni_x").val();        

        if(typeof(value) !== "undefined" && value!="" && typeof(pun) !== "undefined" && pun!="" )
            calcularsubtotal(value);
        else{         
            $("#subt_x").val("0.00"); 
        }
    }).keyup();

    function guargarCli()
    {
        if(validacionCli())
        {            
            $.ajax({
              type: "POST",
              url: "regcliente/"+$("#dniruc_fijo").val()+"/"+$("#nomcli").val()+"/"+$("#dircli").val()+"/"+(($("#obscli").val()=="")?"Ninguna":$("#obscli").val()),
              /*data: "codigo="+$("#dniruc_fijo").val()+"nombre="+$("#nomcli").val()+"direc="+$("#dircli").val()+"obs="+$("#obscli").val(),*/
              dataType: "json",
              error: function(){
                //alert("error petición ajax");                
                $("#resultado").html("error petición ajax");
              },
              beforeSend: function () {
                $("#resultado").html("Procesando, espere por favor...");
                $("#dniruc_fijo").focus();
              },
              success: function(data)
              { 
                if(data=="0")
                {
                    $("#resultado").html("El cliente("+$("#dniruc_fijo").val()+") ya existe.");
                    $("#dniruc_fijo").css("border","2px solid #f00");
                }
                else
                {
                    $("#resultado").html("");                   
                    $("#cod_cliente").val(data);

                    $("#savecli").hide();
                    $("#cancelcli").hide();
                    $("#dniruc").attr("readonly", false);
                    $("#dniruc").val("");

                    $("#dniruc_fijo").attr("readonly", true);
                    $("#nomcli").attr("readonly", true);
                    $("#dircli").attr("readonly", true);
                    $("#obscli").attr("readonly", true);
                    
                    $("#codpro").focus();
                }
              }
            });    
        }       
    }

    function cancelarCli()
    {
        $("#resultado").html("");                   
        $("#cod_cliente").val("");

        $("#savecli").hide();
        $("#cancelcli").hide();
        
        $("#dniruc").attr("readonly", false);    
        $("#nomcli").attr("readonly", true);
        $("#dircli").attr("readonly", true);
        $("#obscli").attr("readonly", true);
        $("#dniruc_fijo").attr("readonly", true);

        $("#dniruc_fijo").val("");
        $("#nomcli").val("");
        $("#dircli").val("");
        $("#obscli").val("");

        $("#dniruc").focus();
        $("#dniruc").val("");
    }

    function validacionCli()
    {
        var dniruc_=$.trim($("#dniruc_fijo").val()); 
        var nom_=$.trim($("#nomcli").val());     
        var dir_=$.trim($("#dircli").val());
        var obs_=$.trim($("#obscli").val());
        var key_error=true;
        
        if(dniruc_=="")
        {
            $("#dniruc_fijo").css("border","2px solid #f00");     
            $("#resultado").html("- Falta rellenar DNI|RUC\n");   
            $("#dniruc_fijo").focus();                   
            return key_error=false;    
        }
        else
            $("#dniruc_fijo").css("border","1px solid #d2d6de");
        
        if(dniruc_.length!=8 && dniruc_.length!=11)
        {
            $("#resultado").html("- Dni:8 dígt. | Ruc:11 dígt\n");
            $("#dniruc_fijo").css("border","2px solid #f00");                
            $("#dniruc_fijo").focus();
            return key_error=false; 
        }
        else
        {
            $("#resultado").empty();
            $("#dniruc_fijo").css("border","1px solid #d2d6de");
        }

        if(nom_=="")
        {
            $("#nomcli").css("border","2px solid #f00");                
            $("#resultado").html("- Falta rellenar Nombre Cliente\n");               
            $("#nomcli").focus();
            return key_error=false;
        }
        else
            $("#nomcli").css("border","1px solid #d2d6de");

        if(dir_=="")
        {
            $("#dircli").css("border","2px solid #f00");                
            $("#resultado").html("- Falta rellenar dirección.\n");               
            $("#dircli").focus();
            return key_error=false;
        }
        else
            $("#dircli").css("border","1px solid #d2d6de");

       /* if(obs_=="")
        {
            $("#obscli").css("border","2px solid #f00");                
            key_error=false;
        }
        else
            $("#obscli").css("border","1px solid #d2d6de");*/


        return key_error;
    }

    function calcularsubtotal(num)
    {
        var unimed=parseFloat($("#puni_x").val());        
        var can=parseFloat(num);       
        var sub=(can*unimed);
        $("#subt_x").val(sub.toFixed(2));                       
    }
    /************************************************************************************/

    /********************************** INICIO: MANEJO DE TABLA *************************/

    /***********************combo anidado almacén producto ***********************/
    $("#almprodes").change(function(event){
        $("#prodes").empty();
        $('#prodes').selectpicker('refresh');
        //alert(event.target.value);
        $.get("productoalm/"+event.target.value,function(response,alm_id){
            $("#prodes").append("<option data-subtext='' value='0' >Seleccione producto...</option>");  
            for(i=0;i<response.length;i++){
                $("#prodes").append("<option data-subtext='"+response[i].nombre+"' dato-esp='"+response[i].nombre_especifico+"' value='"+response[i].nProAlmCod+"' > "+response[i].nombre_generico+"</option>");                          
            }  

        $("#stock").val("0");
        $('#prodes').selectpicker('refresh');                 
        })
    });
    /*******************************************************************************/

    $("#canti").bind('keydown',function(e){
        if ( e.which == 13 ) 
            agredardetalle();        
    });

    /*$("#add").click(function()
    {                         
        alert();
        agredardetalle();    
    });*/

    function agredardetalle()
    {
        if(validacion()==0)
        {
            // Obtenemos el total de columnas (tr) del id "tabla"  
            indice_tabla++;
            $("#conta").val(indice_tabla);
            var nuevaFila="<tr id=fila_"+indice_tabla+">";
            nuevaFila+="<td>"+'<input type="hidden" name="cod_ndi_'+indice_tabla+'" id="cod_ndi_'+indice_tabla+'" value="0">'+'<i class="fa fa-hand-o-right" aria-hidden="true"></i> '+indice_tabla+"</td>";

            nuevaFila+="<td>"+'<input type="hidden" name="codb_'+indice_tabla+'" id="codb_'+indice_tabla+'" value="'+$("#codpro").val()+'">'+$("#codpro").val()+"</td>";

            nuevaFila+="<td style='font-size: 14px;width: 150px;'>"+'<input type="hidden" name="proalm_'+indice_tabla+'" id="proalm_'+indice_tabla+'" value="'+$("#valu").val()+'">'+$("#prodes").val()+"</td>";

            nuevaFila+="<td><input type='hidden'  name='canti_"+indice_tabla+"' id='canti_"+indice_tabla+"' value='"+$("#canti_x").val()+"'>"+$("#canti_x").val()+"</td>";

             nuevaFila+="<td>"+'<input type="hidden" name="rollo_'+indice_tabla+'" id="rollo_'+indice_tabla+'" value="'+$("#canti_rollo_i").val()+'"><p id="Lpar_'+indice_tabla+'">'+(($("#esp").val()=="TELAS")?calcularrollo(parseFloat($("#canti_x").val())):$("#canti_rollo_i").val())+"</p></td>";

            nuevaFila+="<td><input type='text' name='puni_"+indice_tabla+"' id='puni_"+indice_tabla+"' value='"+parseFloat($("#puni_x").val()).toFixed(2)+"'  style='width: 70px;'' maxlength='11' onkeyup=recalsubtotal("+indice_tabla+",'"+$("#esp").val()+"')></td>";
            
            nuevaFila+="<td><input type='hidden' name='stotal_"+indice_tabla+"' id='stotal_"+indice_tabla+"' value='"+parseFloat($("#subt_x").val()).toFixed(2)+"' readonly><label id='stotalx_"+indice_tabla+"'>"+parseFloat($("#subt_x").val()).toFixed(2)+"</label></td>";

            nuevaFila+='<td><div class="btn btn-link" onclick="delTabla('+indice_tabla+')" >'+'<a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quitar"></i></a>'+'</div>';
            
            nuevaFila+="</tr>";
            
            $("#bandeja-transfer").append(nuevaFila);
            $('#puni_'+indice_tabla).numeric(",").numeric({decimalPlaces: 3,negative: false});
            $("#puni_1").bind('keydown',function(e){
                if ( e.which == 13 ) 
                {                
                    $("#puni_2").focus();
                }
                else if ( e.which == 27 ) 
                {
                    $("#puni_12").focus();
                }                      
            });
            $("#puni_2").bind('keydown',function(e){
                if ( e.which == 13 ) 
                {                
                    $("#puni_3").focus();
                }
                else if ( e.which == 27 ) 
                {
                    $("#puni_1").focus();
                }                      
            });
            $("#puni_3").bind('keydown',function(e){
                if ( e.which == 13 ) 
                {                
                    $("#puni_4").focus();
                }
                else if ( e.which == 27 ) 
                {
                    $("#puni_2").focus();
                }                      
            });
            $("#puni_4").bind('keydown',function(e){
                if ( e.which == 13 ) 
                {                
                    $("#puni_5").focus();
                }
                else if ( e.which == 27 ) 
                {
                    $("#puni_3").focus();
                }                      
            });
            $("#puni_5").bind('keydown',function(e){
                if ( e.which == 13 ) 
                {                
                    $("#puni_6").focus();
                }
                else if ( e.which == 27 ) 
                {
                    $("#puni_4").focus();
                }                      
            });
            $("#puni_6").bind('keydown',function(e){
                if ( e.which == 13 ) 
                {                
                    $("#puni_7").focus();
                }
                else if ( e.which == 27 ) 
                {
                    $("#puni_5").focus();
                }                      
            });
            $("#puni_7").bind('keydown',function(e){
                if ( e.which == 13 ) 
                {                
                    $("#puni_8").focus();
                }
                else if ( e.which == 27 ) 
                {
                    $("#puni_6").focus();
                }                      
            });
            $("#puni_8").bind('keydown',function(e){
                if ( e.which == 13 ) 
                {                
                    $("#puni_9").focus();
                }
                else if ( e.which == 27 ) 
                {
                    $("#puni_7").focus();
                }                      
            });
            $("#puni_9").bind('keydown',function(e){
                if ( e.which == 13 ) 
                {                
                    $("#puni_10").focus();
                }
                else if ( e.which == 27 ) 
                {
                    $("#puni_8").focus();
                }                      
            });
            $("#puni_10").bind('keydown',function(e){
                if ( e.which == 13 ) 
                {                
                    $("#puni_11").focus();
                }
                else if ( e.which == 27 ) 
                {
                    $("#puni_9").focus();
                }                      
            });
            $("#puni_11").bind('keydown',function(e){
                if ( e.which == 13 ) 
                {                
                    $("#puni_12").focus();
                }
                else if ( e.which == 27 ) 
                {
                    $("#puni_10").focus();
                }                      
            });
            calcular_totales(parseFloat($("#subt_x").val()),"inc");
            //alert(($("#canti_x").val()!=""));
            incdesstock($("#valu").val(),(($("#canti_x").val()!="")?parseFloat($("#canti_x").val()):parseFloat($("#canti_rollo_i").val())),"inc");
            
            limpiarcontroles();   
            
            // paginación   
            cant_items_real++;            
            // cargar lista de p/ginas
            controlpaginacion(cantxpag,cant_items_real);        
            // cargar paginación inicial            
            paginacion(ultima_pag,cantxpag);
            
        }
    }

    function recalsubtotal(indice,prod)
    {        
        var auxsal=0;
        if(prod=="TELAS"){
            auxsal=(parseFloat($("#canti_"+indice).val())*parseFloat($("#puni_"+indice).val()));
        }
        else
            auxsal=(parseFloat($("#rollo_"+indice).val())*parseFloat($("#puni_"+indice).val()));   
        auxsal=(isNaN(auxsal)?0:auxsal);
        var ant=parseFloat($("#stotal_"+indice).val());
        var act=auxsal;
        $("#stotal_"+indice).val(auxsal.toFixed(2));
        //alert(act-ant);
        calcular_totales((act-ant),"inc");

        $("#stotalx_"+indice).empty();
        $("#stotalx_"+indice).html(auxsal.toFixed(2));
        //altualizar el subtotal
        //actualizar totales
    }

    function incdesstock(indice,valor,inc_dec)
    {
        //alert("valor: "+valor);
        if(typeof(tabla[indice]) === "undefined"){//si no existe agregar
            if(inc_dec=="inc")
                tabla[indice]=valor;
            else
                tabla[indice]=-valor;
        }
        else//si existe (incrementar/decrementar)
        {
            if(inc_dec=="inc")
                tabla[indice]+=valor;
            else
                tabla[indice]-=valor;
        }
    }

    function calcular_totales(importe_bruto_ax,inc_dec)
    {
        var importe_bruto=parseFloat($("#impo").val());
        var timpuesto=parseFloat($("#igv").val());
        var ipagar=parseFloat($("#total").val());

        if(inc_dec=="inc")
        { 
            ipagar+=importe_bruto_ax;    

            var aux_importe=ipagar/igv_g;
            var aux_igv=ipagar-aux_importe;
            timpuesto=aux_igv;
            importe_bruto=aux_importe;
            /*timpuesto+=importe_bruto_ax*igv_g;
            importe_bruto+=(importe_bruto_ax-(importe_bruto_ax*igv_g));            */
        }
        else
        {
            ipagar-=importe_bruto_ax;            
            var aux_importe=ipagar/igv_g;
            var aux_igv=ipagar-aux_importe;
            timpuesto=aux_igv;
            importe_bruto=aux_importe;
            //timpuesto-=importe_bruto_ax*igv_g;
            //importe_bruto-=(importe_bruto_ax-(importe_bruto_ax*igv_g));            
        }
        $("#impo").val(importe_bruto.toFixed(2));
        $("#igv").val(timpuesto.toFixed(2));
        $("#total").val(ipagar.toFixed(2));

        $("#soles_x").val("S/."+ipagar.toFixed(2));
        $("#dolar_x").val("$/."+(ipagar/parseFloat($("#cambio").val())).toFixed(2));
    }

    function puni_calculo(id)
    {
        var cp=$("#medi_"+id).val();        
        var uni=parseFloat($("#medi_"+id+" option:selected").attr("data-uni")); 
        var unimed=parseFloat($("#medi_"+id+" option:selected").attr("data-unimed"));        
        var can=parseFloat($("#canti_"+id).val());
        var desc=parseFloat($("#descu_"+id).val());
        
        if(uni!=0)
        {
            if(can/uni>=1)
            {
                $.get("ofermedida/"+cp,function(response,alm_id)
                {  
                    calcular_totales(parseFloat($("#stotal_"+id).val()),"dec");

                    var p_uni_aux=((100-parseFloat(response.dMedProPor))/100)*parseFloat(response.dProdPpubl);
                    $("#puni_"+id).val(p_uni_aux.toFixed(2));
                    //var sub=(p_uni_aux*can)-desc;
                    var sub=(p_uni_aux*can*unimed);
                    $("#stotal_"+id).val(sub.toFixed(2));                     
                    calcular_totales(sub,"inc");
                })
            }
            else
                alert("Adv!: La cantidad "+can+", es menor que "+uni+" unidades.");
        }
        else
        {
            calcular_totales(parseFloat($("#stotal_"+id).val()),"dec");

            $("#puni_"+id).val("0");            
            $("#stotal_"+id).val("0.00"); 
            //incdesstock($("#proalm_"+id).val(),sub,"dec");
        }
   
    }

    function limpiarcontroles()
    {
        //$("#prodes option[value=0]").attr("selected",true); 
        //$("#almprodes").change();
        $("#prodes").val("");
        $("#subtext").val("");
        $("#esp").val("");
        $("#valu").val("");
                
        $("#canti_x").prop( "readonly", true );
        $("#canti_rollo_i").prop( "readonly", true );
        $("#puni_x").prop( "readonly", true );
        $("#puni_x").val("0")
        $("#canti_rollo_i").val("0");
        $("#codpro").focus(); 

        $("#stock").val("0.00"); 
        $("#cant_rollo").val("0.00");                
        $("#canti_x").val("0");
        //$("#unit_x").val("0");
        $("#puni_x").val("0");            
        $("#subt_x").val("0.00");
        $("#codpro").val("");
        /*$("#cobro").on('shown.bs.modal', function () {                            
            $('#codpro').focus();
        }); */   
    }

    function calcularrollo(num)
    {        
        var num_ceros=num/20;        
        return num_ceros.toFixed(2);
    } 

/*
    function addtabla(cod,cb,cproalm,prod,col,pesp,cant)
    {        

        var pes=0;
        var roll=0;
        var roll_cal=0;

        if(pesp=="TELAS")
        {
            pes=cant;
            roll=0;
            roll_cal=calcularrollo(parseFloat(cant));
        }
        else
        {
            pes=0;
            roll=cant;   
            roll_cal=cant;
        }

        indice_tabla++;
        $("#conta").val(indice_tabla);
        var nuevaFila="<tr id=fila_"+indice_tabla+">";
        nuevaFila+="<td>"+'<input type="hidden" name="cod_ndi_'+indice_tabla+'" id="cod_ndi_'+indice_tabla+'" value="'+cod+'">'+indice_tabla+"</td>";
        
        nuevaFila+="<td>"+'<input type="hidden" name="codb_'+indice_tabla+'" id="codb_'+indice_tabla+'" value="'+cb+'">'+cb+"</td>";

        nuevaFila+="<td>"+'<input type="hidden" name="prod_'+indice_tabla+'" id="prod_'+indice_tabla+'" value="'+cproalm+'"><p id="Lpes_'+indice_tabla+'">'+prod+"</p></td>";

        nuevaFila+="<td>"+col+"</td>";

        nuevaFila+="<td>"+'<input type="hidden" name="peso_'+indice_tabla+'" id="peso_'+indice_tabla+'" value="'+doscerosdecimal(pes)+'"><p id="Ltie_'+indice_tabla+'">'+doscerosdecimal(pes)+"</p></td>";

        nuevaFila+="<td>"+'<input type="hidden" name="rollo_'+indice_tabla+'" id="rollo_'+indice_tabla+'" value="'+roll+'"><p id="Lpar_'+indice_tabla+'">'+roll_cal+"</p></td>";
        
        nuevaFila+='<td><div class="btn btn-link" onclick="delTabla('+indice_tabla+')" >'+'<a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quitar"></i></a>'+'</div>';
        
        nuevaFila+="</tr>";
        $("#bandeja-transfer").append(nuevaFila);
        
        //------- para actualizar valores en el control ---
        //incdesstock(cproalm,((parseFloat(pes)!=0)?parseFloat(pes):parseFloat(roll)) ,"inc");

        return 1;            
    
    }
*/
    function delTabla(id)
    {     
        
        if(confirm('Nota: El registro se retirará momentaneamente, pero el cambio no se hará efecto hasta que usted guarde los cambios.'))
        {
            var datos= $("#eliminados").val()+","+$("#cod_ndi_"+id).val();
            $("#eliminados").val(datos);

            //------- para actualizar valores en el control ---
            //incdesstock($("#prod_"+id).val(),((parseFloat($("#peso_"+id).val())!=0)?parseFloat($("#peso_"+id).val()):parseFloat($("#rollo_"+id).val())) ,"des");
            calcular_totales(parseFloat($("#stotal_"+id).val()),"dec");
            //alert($("#canti_"+id).val());
            incdesstock($("#proalm_"+id).val(),(($("#canti_"+id).val()!="")?parseFloat($("#canti_"+id).val()):parseFloat($("#rollo_"+id).val())),"dec");
                         

            limpiarcontroles();
            // fin controles-----------------------------------
            
            $("#fila_"+id).remove();            

            cant_items_real--;
            // cargar lista de p/ginas
            controlpaginacion(cantxpag,cant_items_real);        
            // cargar paginación inicial 
            paginacion(indice_act,cantxpag);
        }            
    }

/*  function editar(id)
    {
        $("#dina_control").css( "box-shadow","rgba(0, 0, 0, 0.75) 0px 4px 47px -5px"); 
        
        $('#tienda > option[value="'+$("#tie_"+id).val()+'"]').attr('selected', 'selected');
        $("#partida").val($("#par_"+id).val());
        $("#peso").val($("#pes_"+id).val());
        $("#rollo").val($("#roll_"+id).val());

        $("#partida").attr('disabled','disabled');
        $("#cdel_"+id).hide();
        $("#cedi_"+id).hide();

        $("#actualizar").val(id);

        $("#act").show();
        $("#add").hide();   

        $( "#peso" ).focus();
        $("#capa").show();
        key_enter=false;
    }

    function actualiza()
    {
        if(validacion()==0)
        {
            var id=$("#actualizar").val();
            
            $("#tie_"+id).val($("#tienda").val());
            $("#par_"+id).val($("#partida").val());
            $("#pes_"+id).val($("#peso").val());
            $("#roll_"+id).val($("#rollo").val());            
            
            $("#Ltie_"+id).text($("#tienda option:selected").html());
            $("#Lpar_"+id).text($("#partida").val());
            $("#Lpes_"+id).text($("#peso").val());
            $("#Lroll_"+id).text($("#rollo").val());

            $("#partida").removeAttr('disabled');
            $("#cdel_"+id).show();
            $("#cedi_"+id).show();

            $("#dina_control").css( "box-shadow","none"); 

            //$("#tienda").val("1");
            $("#partida").val("");
            $("#peso").val("");
            $("#rollo").val("");

            $("#act").hide();
            $("#add").show();

            //verificar si hay  cambios 
            if($("#tie_"+id).val()!=$("#tienda").val() || $("#pes_"+id).val()!=$("#peso").val() || $("#roll_"+id).val()!=$("#rollo").val())
            {                
                var datos= $("#cad_actt").val()+","+$("#cod_ndi_"+id).val();
                $("#cad_actt").val(datos);
            }

            $( "#partida" ).focus();
            $("#capa").hide();
            key_enter=true;
            //si hay agregar al input .... el código
        }

    }
    function cancelar()
    {
        var id=$("#actualizar").val();
        $("#partida").removeAttr('disabled');
        $("#cdel_"+id).show();
        $("#cedi_"+id).show();

        $("#dina_control").css( "box-shadow","none"); 

        //$("#tienda").val("");
        $("#partida").val("");
        $("#peso").val("");
        $("#rollo").val("");

        $("#act").hide();
        $("#add").show();

        $( "#partida" ).focus();
        $("#capa").hide();
        key_enter=true;
    }

     function anular(e) {
          tecla = (document.all) ? e.keyCode : e.which;
          return (tecla != 13);
     }
*/
    function validacion()
    {
        var stock_=parseFloat($.trim($("#stock").val())); 
        var stock_rollo=parseFloat($.trim($("#cant_rollo").val()));     
        var sb_=parseFloat($.trim($("#subt_x").val()));
        var canti_=parseFloat($.trim($("#canti_x").val()));
        var rollo_=parseFloat($.trim($("#canti_rollo_i").val()));
        var key_error=false;
        
        if(sb_<=0)
        {
            $("#subt_x").css("border","2px solid #f00");                
            key_error=true;
        }
        else
            $("#subt_x").css("border","1px solid #d2d6de");

        
        if(stock_<=0)
        {
            $("#stock").css("border","2px solid #f00");                
            key_error=true;
        }
        else
            $("#stock").css("border","1px solid #d2d6de");


        if($("#esp").val()=="TELAS")
        {
            if(isNaN(canti_))
            {
                $("#canti_x").css("border","2px solid #f00");   
                $("#canti_x").focus();             
                key_error=true;
            }
            else
            {
                if(canti_<=0)
                {
                    $("#canti_x").css("border","2px solid #f00");   
                    $("#canti_x").focus();  
                    alert("Peso no puede ser menor o igual a 0");           
                    key_error=true;
                }
                else
                {
                    if(canti_>stock_)
                    {
                        $("#canti_x").css("border","2px solid #f00"); 
                        //$("#unit_x").css("border","2px solid #f00"); 
                          
                        $("#canti_x").focus();             
                        alert("cantidad no puede ser mayor que el stock");
                        key_error=true;
                    }
                    else{
                        $("#canti_x").css("border","1px solid #d2d6de");
                        //$("#unit_x").css("border","1px solid #d2d6de"); 
                    }
                }
            }
        }
        else
        {
            if(isNaN(rollo_))
            {
                $("#canti_rollo_i").css("border","2px solid #f00");   
                $("#canti_rollo_i").focus();             
                key_error=true;
            }
            else
            {
                if(rollo_<=0)
                {
                    $("#canti_rollo_i").css("border","2px solid #f00");   
                    $("#canti_rollo_i").focus();  
                    alert("Rollos no puede ser menor o igual a 0");           
                    key_error=true;
                }
                else
                {
                    if(rollo_>stock_rollo)
                    {
                        $("#canti_rollo_i").css("border","2px solid #f00"); 
                        //$("#unit_x").css("border","2px solid #f00"); 
                          
                        $("#canti_rollo_i").focus();             
                        alert("cantidad no puede ser mayor que el stock");
                        key_error=true;
                    }
                    else{
                        $("#canti_rollo_i").css("border","1px solid #d2d6de");
                        //$("#unit_x").css("border","1px solid #d2d6de"); 
                    }
                }
            }
        }

        return (key_error)?1:0;
    }

    function controlpaginacion(pag,total)
    {
        var cad='';
        var act=true;
        var i;
        for(i=1;i<=total/pag;i++)
        {

            if(act)
                cad='<li onclick="paginacion('+i+','+pag+')" style="cursor: pointer;"><span>«</span></li>';

            cad+='<li '+((act)?'class="active"':'')+' onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            act=false;
        }
        if(total%pag>0)
        {
            cad+='<li onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            cad+='<li onclick="paginacion('+(i)+','+pag+');" style="cursor: pointer;"><span>»</span></li>';
        }
        else
            cad+='<li onclick="paginacion('+(--i)+','+pag+')" style="cursor: pointer;"><span>»</span></li>';

        ultima_pag=i;

        $(".pagination").empty();
        $(".pagination").html(cad);
    }

    function paginacion(pag,de)
    {   
        // ocultar registros de 10 en 10 

        var fin=pag*de;;
        var ini=(pag-1)*de;


        //meter todo en un array, con indice ordenado                        
        var array_items=[];
        var contador=0;
        for (var i = 1; i <= parseInt($("#conta").val()); i++) 
        {                
            var nomfila="fila_"+i;
            if ( $("#"+nomfila).length > 0 ) 
            {
                array_items[contador]=nomfila;
                contador++;
            }                
        }
        //recorrer segun los limites mostrar lo necesario

        for(var i=0;i<contador;i++)
        {
            if(i>=ini && i<fin)
                $("#"+array_items[i]).show();        
            else
                $("#"+array_items[i]).hide();
        }

        indice_act=pag;//guarada el indice de pág

        for(i=1;i<=ultima_pag;i++)
            $("#pgitem_"+i).removeAttr('class');

        $("#pgitem_"+indice_act).attr('class','active');

        return 1;
    }
    /********************************* FIN: TABLA ***************************************/

    $("#efec").bind('keydown',function(e){
        if( e.which == 13 ) 
        {
            cobrar(); 
            //calcularsubtotal(num);
        }
    });

    $cont_sub=true;

    function cobrar()
    {   
        if(parseFloat($("#vuel").val())>=0 && parseFloat($("#efec").val())>=parseFloat($("#p_tot").val()))
        {
            if($cont_sub)
            {
                $("#venta").submit();    
                $cont_sub=false;
            }            
        }
        else
        {
            $("#vuel").css("border","2px solid #f00");                 
            $("#obs_vuel").html("¡El pago es menor al costo..!");
        }
    }

    function totales()
    {
        var dr_=$.trim($("#cod_cliente").val());
        var forpago=$('#pago').val();
         key_error=false;
         $("#obs_pro2").empty();
         //alert(forpago);
        if($("#tipdoc").val()=="")
        {
            $("#tdocumento_x").css("border","2px solid #f00");   
            $("#tdocumento_x").focus();    
            return key_error=true;
        }
        else
        {
            $("#tdocumento_x").css("border","1px solid #d2d6de");
            
            if($("#pago").val()=="")
            {
                $("#fpago_x").css("border","2px solid #f00");   
                $("#fpago_x").focus();    
                return key_error=true;
            }
            else
            {
                $("#fpago_x").css("border","1px solid #d2d6de");
            }

            if($("#tipdoc").val()==3 || $("#tipdoc").val()==2)
            {
                if($.trim($("#dfactu2").val()).length!=6)
                {
                    $("#dfactu2").css("border","2px solid #f00");   
                    $("#dfactu2").focus();    
                    $("#obs_pro2").html("El formato de factura es 000000.");
                    $('body,html').animate({scrollTop : 0}, 500);
                    return key_error=true;
                }
                else
                {
                    $("#dfactu2").css("border","1px solid #d2d6de");
                } 
            }

            if(forpago=="2")
            {
                if($("#banco").val()=="")
                {
                    $("#banco").css("border","2px solid #f00");   
                    $("#banco").focus();    
                    key_error=true;
                }
                else
                    $("#banco").css("border","1px solid #d2d6de");

                if($("#nope").val()=="")
                {
                    $("#nope").css("border","2px solid #f00");                
                    $("#nope").focus();
                    key_error=true;
                }
                else
                    $("#nope").css("border","1px solid #d2d6de");
            }
            else if(forpago=="4")
            {
                if($("#banco").val()=="")
                {
                    $("#banco").css("border","2px solid #f00");   
                    $("#banco").focus();    
                    key_error=true;
                }
                else
                    $("#banco").css("border","1px solid #d2d6de");

                if($("#nope").val()=="")
                {
                    $("#nope").css("border","2px solid #f00");                
                    $("#nope").focus();
                    key_error=true;
                }
                else
                    $("#nope").css("border","1px solid #d2d6de");

                if($("#ncheq").val()=="")
                {
                    $("#ncheq").css("border","2px solid #f00");                
                    $("#ncheq").focus();
                    key_error=true;
                }
                else
                    $("#ncheq").css("border","1px solid #d2d6de");

            }
        }

        if(!key_error)
        {
            if($("#tipdoc").val()==3 || $("#tipdoc").val()==2)
            {
                if($.trim($("#nguia_zz").val()).length==0)
                {
                    $("#nguia_zz").css("border","2px solid #f00");   
                    $("#nguia_zz").focus();    
                    $("#obs_pro2").html("Debe ingresar el Número de Guía.");
                    $('body,html').animate({scrollTop : 0}, 500);
                    return key_error=true;
                }
                else
                {
                    $("#nguia_zz").css("border","1px solid #d2d6de");
                }  
            }

            if(dr_!="")
            {
                if($.trim($("#dniruc_fijo").val()).length==exigecli || exigecli==0)
                {
                    $("#obs_pro").empty();
                    $("#dniruc").css("border","1px solid #d2d6de");                   
                    //$("#dniruc_fijo").css("border","1px solid #d2d6de");                   

                    $("#p_imp").val($("#impo").val());
                    $("#p_igv").val($("#igv").val());
                    $("#p_tot").val($("#total").val());

                    if(parseFloat($("#p_tot").val())>0)
                    {                                       
                        $('#cobro').modal("show");
                        $("#cobro").on('shown.bs.modal', function () {                            
                            $('#efec').focus();
                        });                       
                    }
                    else{
                        alert("No ha seleccionado ningún producto!");    
                        $("#codpro").focus();
                    }
                }
                else
                {
                    
                    //switch($('input:radio[name=tipdoc]:checked').val())
                    switch($('#tipdoc').val())
                    {
                        case "2":$("#obs_pro").html("El tipo de documento BOLETA exige un cliente con DNI (8 dígitos)");break;
                        case "3":$("#obs_pro").html("El tipo de documento FACTURA exige un cliente con RUC (11 dígitos)");break;
                    }
                    $('body,html').animate({scrollTop : 0}, 500);
                    $("#dniruc").css("border","2px solid #f00"); 
                    $("#dniruc").val("");
                    $("#dniruc").focus();
                }                
            }
            else{
                $("#obs_pro").html("Debe buscar el cliente.");
                //alert("Falta ingresar el cliente");
                $("#dniruc").css("border","2px solid #f00"); 
                //$("#dniruc_fijo").css("border","2px solid #f00");   
                $('body,html').animate({scrollTop : 0}, 500);                        
                $("#dniruc").focus();            
            }
        

        }
        else
        {
            $('body,html').animate({scrollTop : 0}, 500);
            $("#obs_pro2").html("Debe ingresar los campos marcados...");
            $("#fpago_x").focus();
        }

        

    }
    function limpiarcobro()
    {
        $("#obs_pro").empty();
        $("#dniruc").css("border","1px solid #d2d6de");                   
        $("#dniruc_fijo").css("border","1px solid #d2d6de");                   

        $("#p_imp").val("");
        $("#p_igv").val("");
        $("#p_tot").val("");
        $("#efec").val("");
        $("#vuel").val("0.00");        
        $("#cobro").on('hidden.bs.modal', function () {                            
            $('#codpro').focus();
        });  
    }

    $("#efec").on('keyup', function(){
            var value = $(this).val().length;            
            var resto=parseFloat($(this).val())-parseFloat($("#p_tot").val());
            $("#vuel").val(resto.toFixed(1));
            //$("obs_vuel").html(value);
        }).keyup();

    /****************************** hora ****************/

</script>
@endpush('scripts')