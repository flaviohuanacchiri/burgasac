@extends('backend.layouts.pdf')
<style type="text/css">
    body, table, tr, th, td, .contenedor-tabla, .contenedor-fila, .contenedor-columna, span, div, p {font-size: 11px !important;, font-weight: bold;}
    table td {word-break: break-all !important; }
    table tr.filauno td {white-space: nowrap !important;}
</style>
<title>Guía de Remisión {{$vfacturas->cNumGuia}}</title>

<body style="margin-left: -30px;margin-right: 0px;""> 
    <div class="contenedor-tabla">
        <div class="contenedor-fila">            
            <div class="contenedor-columna" style="width:100%; height: 128px;" >
               
            </div>                                          
        </div>
    </div>
    <div class="contenedor-tabla" style="padding-top: 0px; padding-left: 5px;">
        <div class="contenedor-fila"  style="padding-top: -3px; height: 10px; line-height: 0px;">
            <div class="contenedor-columna" style="width: 100%;">
              <p style="padding-left: 114px;">{{strtoupper($vfacturas->cliente->cClieDesc)}}</p>
             </div>
        </div>
        <div class="contenedor-fila" style="padding-top: -3px; height: 10px; line-height: 8px;" >                        
            <div class="contenedor-columna" style="width: 100%;">
              <p style="padding-left: 114px;">{{strtoupper($vfacturas->cliente->cClieDirec)}}</p>
             </div>
        </div>
        <div class="contenedor-fila"  style="padding-top: 0px; height: 8px;">
            <table>
                <tr>
                    <td style="width: 60%; padding-left: 120px; text-align: left;">{{$vfacturas->cliente->cClieNdoc}}</td>
                    <td style="width: 20%; padding-left: 0px; ">{{substr($vfacturas->dVFacFemi, 0, strpos($vfacturas->dVFacFemi, ' '))}}</td>
                    <td style="width: 20%; padding-left: 0px; ">{{substr($vfacturas->dVFacFemi, 0, strpos($vfacturas->dVFacFemi, ' '))}}</td>
                </tr>
            </table>
        </div>             
    </div>
<br>
    <div class="row" style="padding-top: 35px;">                                          
        <div class="col_x">
            <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover cuerpo">
                <tr>
                    <th width="70px">
                        ITEM
                    </th>
                    <th width="50px">
                        CANT.
                    </th>  
                    <th width="40px">
                        MED.
                    </th>                                                               
                    <th width="200px">
                        DESCRIPCION
                    </th>
                    <th colspan="2" width="60px">
                        P.UNIT.
                    </th>
                    <th colspan="2" width="50px">
                        IMPORTE
                    </th>
                </tr>
                <?php $i=0; ?>
                @foreach($vfacturas->ventadetalle as $dfac)
                    <tr>
                        @if(is_null($dfac->nProAlmCod)==0)
                            <td>
                                <?php
                                ++$i;
                                echo $dfac->productoAlmacen->producto->id;
                                ?>
                            </td>
                            <td style="text-align: left;">1</td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: left;">
                                {{ $dfac->productoAlmacen->producto->nombre_generico }} {{ $dfac->productoAlmacen->color->nombre }}
                            </td>
                        @else
                            @if(is_null($dfac->nCodTC)==0)
                                <td>
                                    <?php
                                    ++$i;
                                    echo $dfac->resStocktelas->producto->id;
                                    ?>
                                </td>  
                                <td style="text-align: left;">1</td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: left;">
                                    {{ $dfac->resStocktelas->producto->nombre_generico }} (TC)
                                </td>
                            @else
                                @if($dfac->resStockMP->insumo_id==0)
                                    <td>
                                        <?php
                                        ++$i;
                                        echo $dfac->resStockMP->accesorio->id;
                                        ?>
                                    </td>  
                                    <td style="text-align: left;">1</td>
                                    <td style="text-align: center;"></td>
                                    <td style="text-align: left;">
                                        {{ $dfac->resStockMP->accesorio->nombre }} (MP)
                                    </td>
                                @else
                                    <td>
                                        <?php
                                        ++$i;
                                        echo $dfac->resStockMP->insumo->id;
                                        ?>
                                    </td>  
                                    <td style="text-align: left;">1</td>
                                    <td style="text-align: center;"></td>
                                    <td style="text-align: left;">
                                        {{ $dfac->resStockMP->insumo->nombre_generico }} (MP)
                                    </td>
                                @endif
                            @endif
                        @endif     
                        
                        <!--td>{{ $dfac->nVtaCant }}</td-->                        
                        <td  style="border-right: 1px solid white;text-align: right;" width="auto"></td> 
                        <td style="text-align: left;" width="auto">KG</td> 
                        <td style="border-right: 1px solid white;text-align: right;" width="auto"></td> 

                        @if(is_null($dfac->nProAlmCod)==0)
                            <td style="text-align: left;" width="auto">
                                {{ (($dfac->productoAlmacen->producto->nombre_especifico=='TELAS')?number_format($dfac->nVtaPeso,2):number_format($dfac->nVtaCant,2)) }}
                            </td>
                        @else
                            @if(is_null($dfac->nCodTC)==0)
                                <td style="text-align: left;" width="auto">
                                    {{ (($dfac->resStocktelas->producto->nombre_especifico=='TELAS')?number_format($dfac->nVtaPeso,2):number_format($dfac->nVtaCant,2)) }}
                                </td>
                            @else
                                @if($dfac->resStockMP->insumo_id==0)
                                    <td style="text-align: left;">
                                        {{ (('TELAS'=='TELAS')?number_format($dfac->nVtaPeso,2):number_format($dfac->nVtaCant,2)) }}
                                    </td>
                                @else
                                    <td style="text-align: left;">
                                        {{ (('TELAS'=='TELAS')?number_format($dfac->nVtaPeso,2):number_format($dfac->nVtaCant,2)) }}
                                    </td>
                                @endif
                            @endif
                        @endif 
                    </tr>
                @endforeach 
                @for ($j = ++$i; $j <=20; $j++)
                    <tr style="width: 100%">                                                            
                        <td style="color:white;">{{$j}}</td>
                        <td></td>
                        <td></td>                        
                        <td></td> 
                        <td></td> 
                        <td></td> 
                        <td></td> 
                    </tr>
                @endfor
            </table>
            <table style="width: 100%;">
                <tr class="filauno">                                                            
                    <td style="text-align: left;padding-left: 20px; padding-top: 30px;"><span>{{ strtoupper($vfacturas->almacen->cAlmUbi) }}</span></td>                                                
                    <td style="text-align: center; padding-top: 30px; padding-left: 50px;">{{strtoupper($vfacturas->cliente->cClieDirec)}}
                </tr>                                            
            </table>
            <table>
                <tr>                                                            
                    <td style="text-align: left; padding-left: 50px; padding-top: 27px; width: 35%;">
                        <span>{{strtoupper($vfacturas->tipodocv->cDescTipPago)}}</span>
                    </td>                                                
                    <td  style="text-align: left;font-weight: bold; padding-top: 27px; padding-left: 20px; width: 15%;">
                        <span>{{strtoupper($vfacturas->cFacNumFac)}}</span>
                    </td>
                    <td style="text-align: left;padding-left: 195px; width: 50%; padding-top: 3px;">
                        <span>X</span>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>
