<?php
/**
 * Clase que implementa un conversor de números a letras. 
 * @author AxiaCore S.A.S
 *
 */
class NumberToLetterConverter {
  private $UNIDADES = array(
        '',
        'UN ',
        'DOS ',
        'TRES ',
        'CUATRO ',
        'CINCO ',
        'SEIS ',
        'SIETE ',
        'OCHO ',
        'NUEVE ',
        'DIEZ ',
        'ONCE ',
        'DOCE ',
        'TRECE ',
        'CATORCE ',
        'QUINCE ',
        'DIECISEIS ',
        'DIECISIETE ',
        'DIECIOCHO ',
        'DIECINUEVE ',
        'VEINTE '
  );
  private $DECENAS = array(
        'VEINTI',
        'TREINTA ',
        'CUARENTA ',
        'CINCUENTA ',
        'SESENTA ',
        'SETENTA ',
        'OCHENTA ',
        'NOVENTA ',
        'CIEN '
  );
  private $CENTENAS = array(
        'CIENTO ',
        'DOSCIENTOS ',
        'TRESCIENTOS ',
        'CUATROCIENTOS ',
        'QUINIENTOS ',
        'SEISCIENTOS ',
        'SETECIENTOS ',
        'OCHOCIENTOS ',
        'NOVECIENTOS '
  );
  private $MONEDAS = array(
    array('country' => 'Colombia', 'currency' => 'COP', 'singular' => 'PESO COLOMBIANO', 'plural' => 'PESOS COLOMBIANOS', 'symbol', '$'),
    array('country' => 'Estados Unidos', 'currency' => 'USD', 'singular' => 'DÓLAR', 'plural' => 'DÓLARES', 'symbol', 'US$'),
    array('country' => 'El Salvador', 'currency' => 'USD', 'singular' => 'DÓLAR', 'plural' => 'DÓLARES', 'symbol', 'US$'),
    array('country' => 'Europa', 'currency' => 'EUR', 'singular' => 'EURO', 'plural' => 'EUROS', 'symbol', '€'),
    array('country' => 'México', 'currency' => 'MXN', 'singular' => 'PESO MEXICANO', 'plural' => 'PESOS MEXICANOS', 'symbol', '$'),
    array('country' => 'Perú', 'currency' => 'PEN', 'singular' => 'NUEVO SOL', 'plural' => 'NUEVOS SOLES', 'symbol', 'S/'),
    array('country' => 'Reino Unido', 'currency' => 'GBP', 'singular' => 'LIBRA', 'plural' => 'LIBRAS', 'symbol', '£'),
    array('country' => 'Argentina', 'currency' => 'ARS', 'singular' => 'PESO', 'plural' => 'PESOS', 'symbol', '$')
  );
    private $separator = '.';
    private $decimal_mark = ',';
    private $glue = ' CON ';
    /**
     * Evalua si el número contiene separadores o decimales
     * formatea y ejecuta la función conversora
     * @param $number número a convertir
     * @param $miMoneda clave de la moneda
     * @return string completo
     */
    public function to_word($number, $miMoneda = null) {
        if (strpos($number, $this->decimal_mark) === FALSE) {
          $convertedNumber = array(
            $this->convertNumber($number, $miMoneda, 'entero')
          );
        } else {
          $number = explode($this->decimal_mark, str_replace($this->separator, '', trim($number)));
          $convertedNumber = array(
            $this->convertNumber($number[0], $miMoneda, 'entero'),
            $this->convertNumber($number[1], $miMoneda, 'decimal'),
          );
        }
        return implode($this->glue, array_filter($convertedNumber));
    }
    /**
     * Convierte número a letras
     * @param $number
     * @param $miMoneda
     * @param $type tipo de dígito (entero/decimal)
     * @return $converted string convertido
     */
    public function convertNumber($number, $miMoneda = null, $type) {   
        $decimales="";
        if(strpos($number,'.')!="")
        {
            $aux_x=trim(substr($number, strpos($number,'.')+1));
            $decimales=" Y ".((strlen($aux_x)==1)?$aux_x."0":$aux_x)."/100";        
            $number=trim(substr($number, 0, strpos($number,'.')));
        }
        $converted = '';
        if ($miMoneda !== null) {
            try {
                
                $moneda = array_filter($this->MONEDAS, function($m) use ($miMoneda) {
                    return ($m['currency'] == $miMoneda);
                });
                $moneda = array_values($moneda);
                if (count($moneda) <= 0) {
                    throw new Exception("Tipo de moneda inválido");
                    return;
                }
                ($number < 2 ? $moneda = $moneda[0]['singular'] : $moneda = $moneda[0]['plural']);
            } catch (Exception $e) {
                echo $e->getMessage();
                return;
            }
        }else{
            $moneda = '';
        }
        if (($number < 0) || ($number > 999999999)) {
            return false;
        }
        $numberStr = (string) $number;
        $numberStrFill = str_pad($numberStr, 9, '0', STR_PAD_LEFT);
        $millones = substr($numberStrFill, 0, 3);
        $miles = substr($numberStrFill, 3, 3);
        $cientos = substr($numberStrFill, 6);
        if (intval($millones) > 0) {
            if ($millones == '001') {
                $converted .= 'UN MILLON ';
            } else if (intval($millones) > 0) {
                $converted .= sprintf('%sMILLONES ', $this->convertGroup($millones));
            }
        }
        
        if (intval($miles) > 0) {
            if ($miles == '001') {
                $converted .= 'MIL ';
            } else if (intval($miles) > 0) {
                $converted .= sprintf('%sMIL ', $this->convertGroup($miles));
            }
        }
        if (intval($cientos) > 0) {
            if ($cientos == '001') {
                $converted .= 'UN ';
            } else if (intval($cientos) > 0) {
                $converted .= sprintf('%s ', $this->convertGroup($cientos));
            }
        }
        //$converted .= $moneda;
        return $converted." ".$decimales." ".$moneda;
    }
    /**
     * Define el tipo de representación decimal (centenas/millares/millones)
     * @param $n
     * @return $output
     */
    private function convertGroup($n) {
        $output = '';
        if ($n == '100') {
            $output = "CIEN ";
        } else if ($n[0] !== '0') {
            $output = $this->CENTENAS[$n[0] - 1];   
        }
        $k = intval(substr($n,1));
        if ($k <= 20) {
            $output .= $this->UNIDADES[$k];
        } else {
            if(($k > 30) && ($n[2] !== '0')) {
                $output .= sprintf('%sY %s', $this->DECENAS[intval($n[1]) - 2], $this->UNIDADES[intval($n[2])]);
            } else {
                $output .= sprintf('%s%s', $this->DECENAS[intval($n[1]) - 2], $this->UNIDADES[intval($n[2])]);
            }
        }
        return $output;
    }
}
?>
    <style type="text/css">
       .contenedor-tabla
        {
            display: table;
            width: 105%;
            font-size: 12px;
        }

        .contenedor-fila
        {
            display: table-row;            
        }

        .contenedor-columna
        {
            display: table-cell;
            padding-top: 5px;
        }

        body{
            //font-family: "Lucida Console", "Lucida Sans Typewriter", monaco, "Bitstream Vera Sans Mono", monospace; 
            font-family: "Helvetica";
            font-size: 11px;
            font-weight: bold;
        }

        table, td, th {
            /*border: 1px solid black;*/            
            font-size: 11px;
            text-align: center;
            border: 1px solid white;       
        }

        table {
            border-collapse: collapse;
            width: 98%;
        }

        th {
            height: 1px;
            color: white;
        }
        .cuerpo tr td {font-family: "Helvetica"; font-size: 11px; padding: 0px;}
    </style>

<title>Factura</title>

<body style="margin-left: -25px;margin-right: 0px;">      
    <div class="contenedor-tabla">
        <div class="contenedor-fila">            
            <div class="contenedor-columna" style="width:100%; height: 190px;" >
               
            </div>                                          
        </div>
    </div>
    <div class="contenedor-tabla" style="padding-top: -10px;">
        <div class="contenedor-fila" style="padding-top: -3px; height: 8px; line-height: 0px;">
            <div class="contenedor-columna" style="width: 15%;">
     
             </div>
            <div class="contenedor-columna" style="width: 60%; height: 8px; line-height: 0px;">
                {{strtoupper($vfacturas->cliente->cClieDesc)}}
            </div>
            <div class="contenedor-columna" style="width: 25%;">
               
            </div>
        </div>
        <div class="contenedor-fila" style="padding-top: -5px; height: 8px; line-height: 8px; ">
            <div class="contenedor-columna" style="width: 15%;">
          
             </div>
            <div class="contenedor-columna" style="width: 60%;height: 8px; line-height: 0px;">
                {{$vfacturas->cliente->cClieNdoc}}
            </div>
             <div class="contenedor-columna" style="width: 25%;">
                
            </div>            
        </div>
        <div class="contenedor-fila" style="padding-top: -5px; height: 8px; line-height: 0px;;">
            <div class="contenedor-columna">
           
             </div>
            <div class="contenedor-columna" style="height: 8px; line-height: 0px;">
                {{strtoupper($vfacturas->cliente->cClieDirec)}}
            </div>
        </div>      
        <div class="contenedor-fila" style="padding-top: -5px;">
            <div class="contenedor-columna" style="width: 15%;">
              <br>
            </div>
            <div class="contenedor-columna" style="width: 60%;">

            </div>
             <div class="contenedor-columna" style="width: 25%;">
                
            </div>
        </div>
    </div>
    
    <div class="contenedor-tabla" style="border:1px solid white;border-radius: 3px; padding-top: 20px; height: 10px; padding-bottom: 0px;">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 25%;text-align: left;">                
                {{substr($vfacturas->dVFacFemi, 0, strpos($vfacturas->dVFacFemi, ' '))}}

             </div>
            <div class="contenedor-columna" style="width: 20%;">
                 {{$vfacturas->cNumGuia}}
            </div>
            <div class="contenedor-columna" style="width: 25%;">
                 
            </div>
            <div class="contenedor-columna" style="width: 30%;text-align:center;">
                {{strtoupper($vfacturas->forpago->cDescForPag)}}
            </div>
        </div>
    </div>

<!--div style="clear:both;"></div-->


    <br>    
    <div class="row">                                          
        <div class="col_x" style="padding-left: 10px; padding-top: 30px;">
            <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover cuerpo">
                <tr>
                    <th width="70px">
                        ITEM
                    </th>
                    <th width="80px">
                        CANT.
                    </th>  
                    <th width="80px">
                        MED.
                    </th>                                                               
                    <th width="280px">
                        DESCRIPCION
                    </th>
                    <th width="40px">
                        
                    </th>
                    <th colspan="2" width="50px">
                        P.UNIT.
                    </th>
                    <th colspan="2" width="50px">
                        IMPORTE
                    </th>
                </tr>
                <?php $i=0; ?>
                @foreach($vfacturas->ventadetalle as $dfac)
                    <tr height="10px">         
                        @if(is_null($dfac->nProAlmCod)==0)
                            <td>
                                <?php 
                                ++$i;
                                echo $dfac->productoAlmacen->producto->id;
                                ?>
                            </td>
                            <td style="text-align: center;">
                                {{ (($dfac->productoAlmacen->producto->nombre_especifico=='TELAS')?number_format($dfac->nVtaPeso,2):number_format($dfac->nVtaCant,2)) }}
                            </td>
                            <td style="text-align: center;">KG</td>
                            <td style="text-align: left;">
                                {{ $dfac->productoAlmacen->producto->nombre_generico }} {{ $dfac->productoAlmacen->color->nombre }}
                            </td>
                        @else
                            @if(is_null($dfac->nCodTC)==0)
                                <td>
                                    <?php 
                                    ++$i;
                                    echo $dfac->resStocktelas->producto->id;
                                    ?>
                                </td> 
                                <td style="text-align: center;">
                                    {{ (($dfac->resStocktelas->producto->nombre_especifico=='TELAS')?number_format($dfac->nVtaPeso,2):number_format($dfac->nVtaCant,2)) }}
                                </td>
                                <td style="text-align: center;">KG</td>
                                <td style="text-align: left;">
                                    {{ $dfac->resStocktelas->producto->nombre_generico }} (TC)
                                </td>
                            @else
                                @if($dfac->resStockMP->insumo_id==0)
                                    <td>
                                        <?php 
                                        ++$i;                                        
                                        echo $dfac->resStockMP->accesorio->id;
                                        ?>
                                    </td>  
                                    <td style="text-align: right;">
                                        {{ (('TELAS'=='TELAS')?number_format($dfac->nVtaPeso,2):number_format($dfac->nVtaCant,2)) }}
                                    </td>
                                    <td style="text-align: center;">KG</td>
                                    <td style="text-align: left;">                                        
                                        {{ $dfac->resStockMP->accesorio->nombre }} (MP)
                                    </td>
                                @else
                                    <td>
                                        <?php 
                                        ++$i;                                        
                                        echo $dfac->resStockMP->insumo->id;
                                        ?>
                                    </td>  
                                    <td style="text-align: right;">
                                        {{ (('TELAS'=='TELAS')?number_format($dfac->nVtaPeso,2):number_format($dfac->nVtaCant,2)) }}
                                    </td>
                                    <td style="text-align: center;">KG</td>
                                    <td style="text-align: left;">                                        
                                        {{ $dfac->resStockMP->insumo->nombre_generico }} (MP)
                                    </td>
                                @endif
                            @endif
                        @endif 
                        
                        <!--td>{{ $dfac->nVtaCant }}</td-->
                        <td width="50px"></td>                        
                        <td  style="border-right: 1px solid white;text-align: right;" width="auto">S/</td> 
                        <td style="text-align: right;" width="auto">{{ number_format($dfac->nVtaPrecioU,2) }}</td> 
                        <td style="border-right: 1px solid white;text-align: right;" width="auto">S/</td> 
                        <td style="text-align: right;" width="auto">{{ number_format($dfac->nVtaPreST,2) }}</td> 
                    </tr>
                @endforeach 
                @for ($j = ++$i; $j <=20; $j++)
                    <tr>                                                            
                        <td style="color:white;">{{$j}}</td>
                        <td></td>
                        <td></td>                        
                        <td width="50px"></td>
                        <td  style="border-right: 1px solid white;text-align: right;"></td> 
                        <td></td> 
                        <td style="border-right: 1px solid white;text-align: right;"></td> 
                        <td></td> 
                    </tr>
                @endfor
                    
                    <tr height="10px" style="">                                                            
                        <td colspan="6" style="border-bottom: 1px solid white;border-left: 1px solid white; text-align: left;">
                            <?php
                                $v=new NumberToLetterConverter();
                                //echo $v->convertNumber(1302587.25,"PEN","decimal");
                            ?>  
                            <label style="margin: 0px;font-size: 11px;text-align: left; padding: 0px;">Son: {{$v->convertNumber($vfacturas->dVFacVTot,"PEN","decimal")}}</label>  
                        </td>                        
                        <!--td>{{ $dfac->nVtaCant }}</td-->                        
                        <th colspan="1"></th> 
                        <td style="border-right: 1px solid white;text-align: right; padding-top: 0px;" width="60px"></td> 
                        <td style="text-align: right;" width="auto"></td> 
                    </tr>
                    <tr height="10px" style="padding-top: 0px;">                                                            
                        <td colspan="6" style="border-bottom: 1px solid white;border-left: 1px solid white; color: white; padding-top: 15px;">
                            <?php
                                $v=new NumberToLetterConverter();
                                //echo $v->convertNumber(1302587.25,"PEN","decimal");
                            ?>  
                            <label style="margin: 0px;font-size: 11px;text-align: left;">Son: {{$v->convertNumber($vfacturas->dVFacVTot,"PEN","decimal")}}</label>  
                        </td>                        
                        <!--td>{{ $dfac->nVtaCant }}</td-->                        
                        <th colspan="1">SUB TOTAL</th> 
                        <td style="border-right: 1px solid white;text-align: right; padding-top: 45px;" width="60px">S/</td> 
                        <td style="text-align: right; padding-top: 45px;" width="auto">{{ number_format($vfacturas->dVFacSTot,2) }}</td> 
                    </tr>
                    <tr height="10px">                                                            
                        <td colspan="6" style="border-bottom: 1px solid white;border-left: 1px solid white;"></td>                        
                        <!--td>{{ $dfac->nVtaCant }}</td-->                        
                        <td colspan="1" style="text-align: left;">18%</td> 
                        <td style="border-right: 1px solid white;text-align: right;" width="60px">S/</td> 
                        <td style="text-align: right;" width="auto">{{ number_format($vfacturas->dVFacIgv,2) }}</td> 
                    </tr>
                    <tr height="10px">                                                            
                        <td colspan="6" style="border-bottom: 1px solid white;border-left: 1px solid white;"></td>                        
                        <!--td>{{ $dfac->nVtaCant }}</td-->                        
                        <th colspan="1">TOTAL</th> 
                        <td style="border-right: 1px solid white;text-align: right;" width="60px">S/</td> 
                        <td style="text-align: right;" width="auto">{{ number_format($vfacturas->dVFacVTot,2) }}</td> 
                    </tr>                                              
            </table>
        </div>
    </div>
    <br>
</body>
