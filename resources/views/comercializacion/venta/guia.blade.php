<?php
/**
 * Clase que implementa un conversor de números a letras. 
 * @author AxiaCore S.A.S
 *
 */
class NumberToLetterConverter {
  private $UNIDADES = array(
        '',
        'UN ',
        'DOS ',
        'TRES ',
        'CUATRO ',
        'CINCO ',
        'SEIS ',
        'SIETE ',
        'OCHO ',
        'NUEVE ',
        'DIEZ ',
        'ONCE ',
        'DOCE ',
        'TRECE ',
        'CATORCE ',
        'QUINCE ',
        'DIECISEIS ',
        'DIECISIETE ',
        'DIECIOCHO ',
        'DIECINUEVE ',
        'VEINTE '
  );
  private $DECENAS = array(
        'VEINTI',
        'TREINTA ',
        'CUARENTA ',
        'CINCUENTA ',
        'SESENTA ',
        'SETENTA ',
        'OCHENTA ',
        'NOVENTA ',
        'CIEN '
  );
  private $CENTENAS = array(
        'CIENTO ',
        'DOSCIENTOS ',
        'TRESCIENTOS ',
        'CUATROCIENTOS ',
        'QUINIENTOS ',
        'SEISCIENTOS ',
        'SETECIENTOS ',
        'OCHOCIENTOS ',
        'NOVECIENTOS '
  );
  private $MONEDAS = array(
    array('country' => 'Colombia', 'currency' => 'COP', 'singular' => 'PESO COLOMBIANO', 'plural' => 'PESOS COLOMBIANOS', 'symbol', '$'),
    array('country' => 'Estados Unidos', 'currency' => 'USD', 'singular' => 'DÓLAR', 'plural' => 'DÓLARES', 'symbol', 'US$'),
    array('country' => 'El Salvador', 'currency' => 'USD', 'singular' => 'DÓLAR', 'plural' => 'DÓLARES', 'symbol', 'US$'),
    array('country' => 'Europa', 'currency' => 'EUR', 'singular' => 'EURO', 'plural' => 'EUROS', 'symbol', '€'),
    array('country' => 'México', 'currency' => 'MXN', 'singular' => 'PESO MEXICANO', 'plural' => 'PESOS MEXICANOS', 'symbol', '$'),
    array('country' => 'Perú', 'currency' => 'PEN', 'singular' => 'NUEVO SOL', 'plural' => 'NUEVOS SOLES', 'symbol', 'S/'),
    array('country' => 'Reino Unido', 'currency' => 'GBP', 'singular' => 'LIBRA', 'plural' => 'LIBRAS', 'symbol', '£'),
    array('country' => 'Argentina', 'currency' => 'ARS', 'singular' => 'PESO', 'plural' => 'PESOS', 'symbol', '$')
  );
    private $separator = '.';
    private $decimal_mark = ',';
    private $glue = ' CON ';
    /**
     * Evalua si el número contiene separadores o decimales
     * formatea y ejecuta la función conversora
     * @param $number número a convertir
     * @param $miMoneda clave de la moneda
     * @return string completo
     */
    public function to_word($number, $miMoneda = null) {
        if (strpos($number, $this->decimal_mark) === FALSE) {
          $convertedNumber = array(
            $this->convertNumber($number, $miMoneda, 'entero')
          );
        } else {
          $number = explode($this->decimal_mark, str_replace($this->separator, '', trim($number)));
          $convertedNumber = array(
            $this->convertNumber($number[0], $miMoneda, 'entero'),
            $this->convertNumber($number[1], $miMoneda, 'decimal'),
          );
        }
        return implode($this->glue, array_filter($convertedNumber));
    }
    /**
     * Convierte número a letras
     * @param $number
     * @param $miMoneda
     * @param $type tipo de dígito (entero/decimal)
     * @return $converted string convertido
     */
    public function convertNumber($number, $miMoneda = null, $type) {   
        $decimales="";
        if(strpos($number,'.')!="")
        {
            $aux_x=trim(substr($number, strpos($number,'.')+1));
            $decimales=" Y ".((strlen($aux_x)==1)?$aux_x."0":$aux_x)."/100";        
            $number=trim(substr($number, 0, strpos($number,'.')));
        }
        $converted = '';
        if ($miMoneda !== null) {
            try {
                
                $moneda = array_filter($this->MONEDAS, function($m) use ($miMoneda) {
                    return ($m['currency'] == $miMoneda);
                });
                $moneda = array_values($moneda);
                if (count($moneda) <= 0) {
                    throw new Exception("Tipo de moneda inválido");
                    return;
                }
                ($number < 2 ? $moneda = $moneda[0]['singular'] : $moneda = $moneda[0]['plural']);
            } catch (Exception $e) {
                echo $e->getMessage();
                return;
            }
        }else{
            $moneda = '';
        }
        if (($number < 0) || ($number > 999999999)) {
            return false;
        }
        $numberStr = (string) $number;
        $numberStrFill = str_pad($numberStr, 9, '0', STR_PAD_LEFT);
        $millones = substr($numberStrFill, 0, 3);
        $miles = substr($numberStrFill, 3, 3);
        $cientos = substr($numberStrFill, 6);
        if (intval($millones) > 0) {
            if ($millones == '001') {
                $converted .= 'UN MILLON ';
            } else if (intval($millones) > 0) {
                $converted .= sprintf('%sMILLONES ', $this->convertGroup($millones));
            }
        }
        
        if (intval($miles) > 0) {
            if ($miles == '001') {
                $converted .= 'MIL ';
            } else if (intval($miles) > 0) {
                $converted .= sprintf('%sMIL ', $this->convertGroup($miles));
            }
        }
        if (intval($cientos) > 0) {
            if ($cientos == '001') {
                $converted .= 'UN ';
            } else if (intval($cientos) > 0) {
                $converted .= sprintf('%s ', $this->convertGroup($cientos));
            }
        }
        //$converted .= $moneda;
        return $converted." ".$decimales." ".$moneda;
    }
    /**
     * Define el tipo de representación decimal (centenas/millares/millones)
     * @param $n
     * @return $output
     */
    private function convertGroup($n) {
        $output = '';
        if ($n == '100') {
            $output = "CIEN ";
        } else if ($n[0] !== '0') {
            $output = $this->CENTENAS[$n[0] - 1];   
        }
        $k = intval(substr($n,1));
        if ($k <= 20) {
            $output .= $this->UNIDADES[$k];
        } else {
            if(($k > 30) && ($n[2] !== '0')) {
                $output .= sprintf('%sY %s', $this->DECENAS[intval($n[1]) - 2], $this->UNIDADES[intval($n[2])]);
            } else {
                $output .= sprintf('%s%s', $this->DECENAS[intval($n[1]) - 2], $this->UNIDADES[intval($n[2])]);
            }
        }
        return $output;
    }
}
?>
    <style type="text/css">
       .contenedor-tabla
        {
            display: table;
            width: 83%;
            font-size: 11px;
        }

	.contenedor-fila
        {
            display: table-row;            
        }

        .contenedor-columna
        {
            display: table-cell;
            padding-top: 1px;  
	}


        body{
            /**font-family: monospace;*/
            //font-family: "Lucida Console", "Lucida Sans Typewriter", monaco, "Bitstream Vera Sans Mono", monospace; 
            font-family: "Helvetica";
            font-weight: 700;
            padding-left: 5px;
        }

        table, td, th {
            border: 1px solid black;
            font-size: 12px;
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 85%;
        }

        th {
            height: 5px;     }

        .cuerpo, .cuerpo tr td {border: none;}
        .cuerpo tr td {padding: 0px;}
    </style>
    <title>Guía de Venta</title>
<body style="margin-left: 4px;margin-right: 55px; margin-top: 5px;">     
    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;text-align: center;padding-top: 20px;">
                <img src="img/logo.jpg" alt="Smiley face" height="50" width="60">
             </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;padding-top: 20px;padding-left: 10px;">
                <h1 style="margin: 0px;font-size: 20px;">TEXTILES BURGA S.A.C.</h1>
                <h3 style="margin: 0px;font-size: 12px;">JR.AMERICA N° 472 LNT. S-104 - LA VICTORIA</h3>
                 <h3 style="margin: 0px;font-size: 12px;">Telf. 324-1224 Cel. 996342002</h3> 
            </div>
            <div class="contenedor-columna" style="width: 20%;text-align: center;">
                
            </div>
        </div>
        <!--<div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;text-align: center;">
                
             </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;">
                
            </div>
            <div class="contenedor-columna" style="width: 20%;text-align: center;">
                
            </div>
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;text-align: center;">
                
             </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;">
                           
            </div>
            <div class="contenedor-columna" style="width: 20%;text-align: center;">
                
            </div>
        </div> -->
    </div>    
    

    <div class="contenedor-tabla" style="margin-top: 5px; margin-bottom: 5px;">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 5%;">
                TIENDA:
             </div>
            <div class="contenedor-columna" style="width: 50%;">
                {{strtoupper($vfacturas->almacen->cAlmNom)}}
            </div>
            <div class="contenedor-columna" style="width: 35%;">
                PROFORMA: {{$vfacturas->nVtaCod}}
            </div>
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna">
                CLIENTE:
             </div>
            <div class="contenedor-columna">
                {{strtoupper($vfacturas->cliente->cClieDesc)}}
            </div>
            <div class="contenedor-columna">
                FECHA: {{substr($vfacturas->dVFacFemi, 0, strpos($vfacturas->dVFacFemi, ' '))}}
            </div>
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna">
                DIRECCION:
             </div>
            <div class="contenedor-columna">
                {{strtoupper($vfacturas->cliente->cClieDirec)}}
            </div>
            <div class="contenedor-columna">
                
            </div>
        </div>
    </div>

<!--div style="clear:both;"></div-->

    
    <div class="row">                                          
        <div class="col_x">
            <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover cuerpo">
                <tr>
                    <th width="10px;">
                        ITEM
                    </th>
                    <th width="140px;">
                        DESCRIPCION
                    </th>
                    <th width="40px">   
                        CANT.
                    </th>                                                                
                    <th colspan="2" width="20px">
                        P.UNIT.
                    </th>
                    <th colspan="2" width="20px">
                        IMPORTE
                    </th>
                </tr>
                <?php $i=0; ?>
                @foreach($vfacturas->ventadetalle as $dfac)                                                                             
                    <tr>                                                      
                        <td>{{++$i}}</td>
                        @if(is_null($dfac->nProAlmCod)==0)
                            <td style="text-align: left;">
                                {{ $dfac->productoAlmacen->producto->nombre_generico }} {{ $dfac->productoAlmacen->color->nombre }}
                            </td>
                            <td style="text-align: center;">
                                {{ (($dfac->productoAlmacen->producto->nombre_especifico=='TELAS')?number_format($dfac->nVtaPeso,2):number_format($dfac->nVtaCant,2)) }}
                            </td>
                        @else
                            @if(is_null($dfac->nCodTC)==0)
                                <td style="text-align: left;">
                                   {{ $dfac->resStocktelas->producto->nombre_generico }} (TC)
                                </td>
                                <td style="text-align: center;">
                                    {{ (($dfac->resStocktelas->producto->nombre_especifico=='TELAS')?number_format($dfac->nVtaPeso,2):number_format($dfac->nVtaCant,2)) }}
                                </td>
                            @else
                                @if($dfac->resStockMP->insumo_id==0)
                                    <td style="text-align: left;">
                                       {{ $dfac->resStockMP->accesorio->nombre }} (MP)
                                    </td>
                                    <td style="text-align: center;">
                                        {{ (('TELAS'=='TELAS')?number_format($dfac->nVtaPeso,2):number_format($dfac->nVtaCant,2)) }}
                                    </td>
                                @else
                                    <td style="text-align: left;">
                                       {{ $dfac->resStockMP->insumo->nombre_generico }} (MP)
                                    </td>
                                    <td style="text-align: center;">
                                        {{ (('TELAS'=='TELAS')?number_format($dfac->nVtaPeso,2):number_format($dfac->nVtaCant,2)) }}
                                    </td>
                                @endif
                            @endif
                        @endif                                                       
                        <td  style="border-right: 6px solid white;">S/. </td> 
                                                
                        <td style="text-align: right;">{{ number_format($dfac->nVtaPrecioU,2) }}</td> 
                                                   
                        <td  style="border-right: 6px solid white;">S/. </td> 
                                                
                        <td style="text-align: right;">{{ number_format($dfac->nVtaPreST,2) }}</td> 
                    </tr>
                @endforeach
                @for ($j = ++$i; $j <=18; $j++)
                    <tr>                                                            
                        <td style="color:white;">{{$j}}</td>
                        <td></td>
                        <td></td>                        
                        <td width="50px"></td>
                        <td  style="border-right: 1px solid white;text-align: right;"></td>
                    </tr>
                @endfor
                    <tr>                                                            
                        <td colspan="3" style="border-bottom: 1px solid white;border-left: 1px solid white;text-align: left;">
                            <?php
                                $v=new NumberToLetterConverter();
                                //echo $v->convertNumber(1302587.25,"PEN","decimal");
                            ?>  
                            <label style="margin: 0px;font-size: 11px;">Son: {{$v->convertNumber($vfacturas->dVFacVTot,"PEN","decimal")}}</label>
                        </td>                        
                        <!--td>{{ $dfac->nVtaCant }}</td-->                        
                        <th colspan="2" style="border: none;">TOTAL</th> 
                        <td style="border-right: 1px solid white;">S/. </td> 
                        <td style="text-align: right;">{{ number_format($vfacturas->dVFacVTot,2) }}</td> 
                    </tr>                                              
            </table>
        </div>
    </div>
 

    <!--div class="row">
        <div class="col_x">
            <table >
              <tr>
                <th>SUB TOTAL   </th>
                <td>S/. {{$vfacturas->dVFacSTot}}</td>
              </tr>
              <tr>
                <th>IGV</th>
                <td>S/. {{$vfacturas->dVFacIgv}}</td>
              </tr> 
              <tr>
                <th>TOTAL   </th>                                                        
                <td>S/. {{$vfacturas->dVFacVTot}}</td>
              </tr>                                                 
            </table>
        </div>
    </div-->
</body>
