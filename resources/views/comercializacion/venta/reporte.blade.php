
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                
                <div class="panel-body">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">                                    
                                    <div class="print">
                                        <div class="row" style="text-align: right;">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        FACTURA
                                                    </div>

                                                    <div class="col-md-12">
                                                        {{$vfacturas->cFacNumFac}}
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                        <br>
                                        <br>

                                        <div class="row">                                          
                                            <div class="col-md-7">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        Señor(es): {{$vfacturas->cliente->cClieDesc}}
                                                    </div>

                                                    <div class="col-md-12">
                                                        Dirección: {{$vfacturas->cliente->cClieDirec}}
                                                    </div>

                                                    <div class="col-md-12">
                                                        R.U.C: {{$vfacturas->cliente->cClieNdoc}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
                                                            <tr>
                                                                <th>
                                                                    Dia
                                                                </th>                                            
                                                                <th>
                                                                    Mes
                                                                </th>
                                                                <th>
                                                                    Año
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    {{$dia}}
                                                                </td>
                                                                <td>
                                                                    {{$mes}}
                                                                </td>
                                                                <td>
                                                                    {{$anio}}
                                                                </td>
                                                            </tr>                                                        
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                        
                                        <div class="row">                                          
                                            <div class="col-md-12">
                                                <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
                                                    <tr>
                                                        <th>
                                                            CANT.
                                                        </th>                                            
                                                        <th wid>
                                                            DESCRIPCION
                                                        </th>
                                                        <th>
                                                            P.UNIT.
                                                        </th>
                                                        <th>
                                                            IMPORTE
                                                        </th>
                                                    </tr>
                                                    @foreach($vfacturas->ventadetalle as $dfac)
                                                        <tr>                                                            
                                                            <td>{{ $dfac->nVtaPeso }}</td>
                                                            <td>{{ $dfac->nVtaCant }}</td>
                                                            <td>{{ $dfac->productoAlmacen->producto->nombre_generico }} {{ $dfac->productoAlmacen->color->nombre }}</td>
                                                            <td>{{ $dfac->nVtaPrecioU }}</td> 
                                                            <td>{{ $dfac->nVtaPreST }}</td> 
                                                        </tr>
                                                    @endforeach                                                  
                                                </table>
                                            </div>
                                        </div>

                                        <div class="row">                                          
                                            <div class="col-md-12">
                                                SON: <p id="cantle" name="cantle"></p> SOLES
                                            </div>
                                        </div>

                                        <div class="row" style="text-align: right;">                                            
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <table >
                                                      <tr>
                                                        <th>SUB TOTAL   </th>
                                                        <td>S/. {{$vfacturas->dVFacSTot}}</td>
                                                      </tr>
                                                      <tr>
                                                        <th>IGV</th>
                                                        <td>S/. {{$vfacturas->dVFacIgv}}</td>
                                                      </tr> 
                                                      <tr>
                                                        <th>TOTAL   </th>                                                        
                                                        <td>S/. {{$vfacturas->dVFacVTot}}</td>
                                                      </tr>                                                 
                                                    </table>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                   
                </div>
            </div>
        </div>
    </div>

