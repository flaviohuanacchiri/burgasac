@extends('backend.layouts.appv2')

@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<style>
  .ui-autocomplete-loading {
    background: white url("../../img/ui-anim_basic_16x16.gif") right center no-repeat;
  }  
    .modal-dialog,
    .modal-content {
    /* 80% of window height */
    height: 80%;
    }

    .modal-body {
    /* 100% = dialog height, 120px = header + footer */
    max-height: calc(100% - 120px);
    overflow-y: scroll;
    }
</style>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">CIERRE DE INVENTARIO</div>
                <div class="panel-body">
                @include('comercializacion.venta.fragment.info')                

                    <div class="panel panel-default">
                        <div class="panel-body" id="body_req">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Tienda</label>                            
                                    <select class="form-control" name="tienda" id="tienda" autofocus="autofocus" tabindex="1">        

                                        <option value="">Seleccione tienda...</option>                                                                      
                                        @foreach ($tienda as $t)
                                            <option value="{{$t->areasalmacen->almacen->nAlmCod}}">{{$t->areasalmacen->almacen->cAlmNom}}</option>
                                        @endforeach
                                        
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Productos</label>                                                                
                                    <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" id="produc" name="produc" tabindex="2">                               
                                    </select>
                                </div>
                                <div class="col-md-4" style="text-align:center;    padding: 15px;">                                    
                                    <div  id="add" class="btn btn-primary" tabindex="3" onclick="agredardetalle()">Listar</div>
                                </div>                                
                            </div>                            
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-body" id="dina_control" name="dina_control">
                            <div class="row">
                                <div class="col-md-2" style="padding-top: 10px;">
                                    <label>Código del Producto</label>
                                    <input id="codpro" type="text" class="form-control" name="codpro" value=""  placeholder="Código" maxlength="20" tabindex="4">
                                </div>                                
                                
                                <div class="col-md-4" style="padding-top: 10px;">
                                    <label>Producto</label>
                                    <input id="prodes" type="text" class="form-control" name="prodes" value=""  placeholder="Eligir producto.." maxlength="25" tabindex="5">
                                    <input type="hidden" name="subtext" id="subtext" value="">
                                    <input type="hidden" name="esp" id="esp" value="">
                                    <input type="hidden" name="espxx" id="espxx" value="">
                                    <input type="hidden" name="valu" id="valu" value="">

                                </div> 

                                <div class="col-md-2" style="padding-top: 10px;">
                                    <label>Peso</label>
                                    <input id="pes" type="text" class="form-control" name="pes" placeholder="0" maxlength="6" tabindex="6" style="font-size: 22px;font-weight: bold;background-color: #a4d4e4;">
                                </div> 
                                <div class="col-md-2" style="padding-top: 10px;">
                                    <label style="color:red;">Contador de Item Ingresados</label>
                                </div>   
                                <div class="col-md-2" style="padding-top: 10px;">
                                    <label style="color:blue; font-size: 30px;" id="cont_itemsx">0</label>
                                </div>                                                                                             
                            </div>

                        </div>
                    </div>
                    <label id="rpt" style="color:red;"></label>

                    <div style="padding:7px; text-align: right;">
                        <button class="btn btn-warning" name="btn_ejecutar" id="btn_ejecutar"  data-toggle="modal" data-target="#cobro" title="Detalle de Ingresos">Detalle de Ingresos</button> 
                        <button class="btn btn-default" name="btn_vista" id="btn_vista" onclick="vistaprevia();" title="Vista Previa">VISTA - PDF</button> 
                    </div>
                    <form action="{{ route('inventario.store') }}" method="POST" onkeypress="return anular(event)">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="codint" id="codint" value="{{ $id }}">
                    <input type="hidden" name="conta" id="conta" value="">
                    <input type="hidden" name="conta2" id="conta2" value="">
                    <input type="hidden" name="eliminados" id="eliminados" value="">
                    <input type="hidden" name="actualizar" id="actualizar" value="">
                    <input type="hidden" name="prod_agre" id="prod_agre" value="">
                    <input type="hidden" name="prod_codpro" id="prod_codpro" value="">
                    <input type="hidden" name="prod_agredet" id="prod_agredet" value="">
                    <input type="hidden" name="page" id="page" value="create">
                    <input type="hidden" name="tienda_fija" id="tienda_fija" value="">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <br>
                                    <div id="capa" style="position: absolute;width: 97%;height: 92%;background-color: rgba(19, 19, 19, 0.35);display: none;top: 1px;"></div>
                                    <table id="bandeja" name="bandeja" class="table table-striped table-bordered table-hover">
                                        <thead>                                           
                                            <th>
                                                Item
                                            </th>
                                            <th>
                                                Tienda
                                            </th>
                                            <!--th>
                                                Código del Producto
                                            </th-->
                                            <th>
                                                Descripción
                                            </th>
                                            <th>
                                                Stock (Kg./Unid.)
                                            </th>
                                            <th>
                                                Stock Fisico
                                            </th>
                                            <th>
                                                Diferencias
                                            </th>     
                                            <th>
                                                Detalles
                                            </th>                                       
                                        </thead>
                                        <tbody style="text-align: center;">
                                            
                                            
                                        </tbody>
                                        <tr>
                                            <td colspan="3" style="text-align: center;">TOTALES</td>
                                            <td id="tot_stock_1" name="tot_stock_1" style="text-align: center;">0</td>
                                            <td id="tot_stockF_1" name="tot_stockF_1" style="text-align: center;">0</td>
                                            <td id="tot_dif_1" name="tot_dif_1" style="text-align: center;background-color: #eaeaea;"></td>
                                        </tr>                                     
                                    </table>

                                </div>
                            </div>
                            <!-- Modal -->
                                <div class="modal fade" id="cobro" name="cobro" role="dialog" aria-hidden="true">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Detalle de Ingreso</h4>
                                      </div>
                                      <div id="nuevaAventura" class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">                       
                                                    <table id="bandeja_detalle" name="bandeja_detalle" class="table table-striped table-bordered table-hover">
                                                        <thead>                                           
                                                            <th>
                                                                Item
                                                            </th>                                
                                                            <th>
                                                                Código del Producto
                                                            </th>
                                                            <th>
                                                                Descripción
                                                            </th>
                                                            <th>
                                                                Cantidad
                                                            </th>
                                                            <th>
                                                                Peso(Kg./Unid.)
                                                            </th>
                                                            <th>
                                                                Acción
                                                            </th>                                     
                                                        </thead>
                                                        <tbody style="text-align: center;">                                
                                                        </tbody>
                                                        <tr>
                                                            <td colspan="3" style="text-align: center;">TOTAL</td>
                                                            <td id="tot_cant_di" style="text-align: center;">0.0</td>
                                                            <td id="tot_pes_di" style="text-align: center;">0.0</td>
                                                            <td style="background-color: #ececec;"></td>
                                                        </tr>                                                        
                                                    </table>
                                                   
                                                    <!--ul class="pagination">
                                                    </ul-->

                                                </div>  
                                            </div>
                                        </div>
                                      </div>


                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                        <!--button type="button" class="btn btn-success" id="botonAventura" onClick="cobrar()">Confirmar</button-->        
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            <!-- Fin modal -->

                            <!-- Modal -->
                                <div class="modal fade" id="previsualizacion" name="previsualizacion" role="dialog" aria-hidden="true">
                                  <div class="modal-dialog">
                                    <div class="modal-content" style="width: 110%; height: 110%;">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                                        <h4 class="modal-title" id="myModalLabel_1">Reporte: Vista Previa de Cierre de Inventario</h4>
                                      </div>
                                      <div id="nuevaAventura_1" class="modal-body">
                                        <div class="row"  style="text-align: center;">
                                            <div class="col-md-12">
                                                <div class="form-group">  
                                                    <img src="../../img/wait.gif" id="carg" name="carg" style="height: 70px;width: 70px;display: none;">
                                                    <embed src="pdf/reportevistaprevia" width="100%" height="700">                     
                                                    <table id="bandeja_detalle_1" name="bandeja_detalle_1" class="table table-striped table-bordered table-hover" style="display:none;">
                                                                                                              
                                                    </table>
                                                   
                                                    <!--ul class="pagination">
                                                    </ul-->

                                                </div>  
                                            </div>
                                        </div>
                                      </div>


                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                        <!--button type="button" class="btn btn-success" id="botonAventura" onClick="cobrar()">Confirmar</button-->        
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            <!-- Fin modal -->
                           

                             <!-- Modal -->
                                <div class="modal fade" id="item_det" name="item_det" role="dialog" aria-hidden="true">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                                        <h4 class="modal-title" id="myModalLabel_item">Detalle de Item</h4>
                                      </div>
                                      <div id="itemdetallado" class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">                       
                                                    <table id="bandeja_detalle_item" name="bandeja_detalle_item" class="table table-striped table-bordered table-hover">
                                                        <thead>                                           
                                                            <th>
                                                                Item
                                                            </th>                                
                                                            <th>
                                                                Código del Producto
                                                            </th>
                                                            <th>
                                                                Descripción
                                                            </th>
                                                            <th>
                                                                Cantidad
                                                            </th>
                                                            <th>
                                                                Peso(Kg./Unid.)
                                                            </th>                                                                                                
                                                        </thead>
                                                        <tbody style="text-align: center;">                                
                                                        </tbody>                                                        
                                                    </table>
                                                   
                                                    <!--ul class="pagination">
                                                    </ul-->

                                                </div>  
                                            </div>
                                        </div>
                                      </div>


                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                        <!--button type="button" class="btn btn-success" id="botonAventura" onClick="cobrar()">Confirmar</button-->        
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            <!-- Fin modal -->
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-success">Ejecutar Cierre</button>                                      
                                    <a href="{{ route('inventario.index')}}" class="btn btn-danger">Bandeja</a>                                        
                                </div>
                            </div>
                        </div>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>


<div id="loaderDiv" style="display:none;width:169px;height:189px;position:absolute;top:50%;left:50%;padding:2px;z-index: 2;"><img src='../../img/wait.gif' width="64" height="64" /><br>Loading..</div>


@endsection

@push('scripts')
<script type="text/javascript">
    var indice_tabla=0;
    var indice_tabla_2=0;
    var key_enter=true;
    var cant_items_real=0; 
    var indice_act=1;
    var ultima_pag=1;
    var cantxpag=100000;
    var tabla=[];
    var exigecli=0;    
    var cont_item_bd=0;

    var ctrl=false;
 
    /*$("body").on("keydown",function(event){
      //console.log(event.keyCode);
     
      if(event.keyCode==17) 
      {    
        ctrl=true;
      }
      else if(event.keyCode==117) 
      {
        if(ctrl==true)
        {
            location.reload(true);
        }
      }
      else if(event.keyCode==113) 
      {
        if(ctrl==true)
        {     
            limpiarcontroles();       
            totales();                     
        }
      }      
    });*/

    $(document).ready(function() {
        /*@if(Session::has('cod'))
            
            window.open("{{ route('venta.reporte',Session::get('cod')) }}");            
        @endif*/

        $("form").keypress(function(e) {
            if (e.which == 13) {
                return false;
            }
        });

        $('#pes').numeric(",").numeric({decimalPlaces: 2,negative: false});
        
        /********************** PAGINACIÓN **************************/
        cant_items_real=cont_item_bd;    
        /*** cargar lista de p/ginas***/
        controlpaginacion(cantxpag,cant_items_real);        
        /*** cargar paginación inicial ***/
        paginacion(1,cantxpag);

    });

    // tu elemento que quieres activar.
    /*var cargando = $("#carg");

    // evento ajax start
    $(document).ajaxStart(function() {
        alert("mostrar");
        cargando.show();
    });

    // evento ajax stop
    $(document).ajaxStop(function() {
        alert("cerar");
        cargando.hide();
    });
    $('#previsualizacion').on('shown.bs.modal', function () {
      alert("termino");
    })*/
    function vistaprevia()
    {
             
        $("#bandeja_detalle_1").empty();
        $("#bandeja_detalle_1").html($("#bandeja").html()); 
        //$("tr").each(function() {
        
        //var colnum = $(this).prevAll("td").length;

        $("#bandeja_detalle_1").closest('table').find('thead tr th:eq(6)').remove();        
        $("#bandeja_detalle_1 tbody tr").each(function() {
            $(this).find("td:eq(6)").remove();
        });
        
        $.post("vistaprevia_store",{codigo:$.trim($('#bandeja_detalle_1').html())}, function(result)
        {            
            
            if(result==1)
                $('#previsualizacion').modal('toggle');  
            else
                alert("Ocurrió un problema, vuelva a intentarlo");           
            
        });
        
        //$(this).closest("table").find('tbody tr td:eq('+colnum+')').remove();
     
//});
    }

    /************************** Codigodebarras *************************************/
    $("#codpro").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            codpro_xx=$.trim($("#codpro").val());

            if(codpro_xx!="")
            {
                $.ajax({
                  type: "GET",
                  url: "veri_codbarra/"+codpro_xx,
                  //data: "5454",
                  dataType: "json",
                  error: function(){
                    alert("error petición ajax");
                    //agredardetalle();
                  },
                  beforeSend: function(){
                       $("#loaderDiv").show();
                   },
                  success: function(data){     
                    if(data!="")//data[0].ning_id
                    {   
                        //prod 
                        var key=false;  
          
                        $("#prodes").val(data[0].nombre_generico+" "+data[0].nombre);
                        $("#subtext").val(data[0].nombre);
                        $("#esp").val(data[0].nombre_especifico);
                        $("#valu").val(data[0].id+"_"+data[0].codcolor);

                        if(data[0].nombre_especifico =="TELAS")
                        {
                            $("#pes").val(data[0].peso_cant);
                        }
                        else
                        {                            
                            $("#pes").val(data[0].rollo);                            
                        }    
                        
                        agredarsubdetalle();
                        //$("#pes").focus();
                    }
                    else
                    {
                        alert("No existe este código de barras: "+codpro_xx); 
                        limpiarcontroles();                  
                    }
                    $("#loaderDiv").hide();
                  }
                });
            }
            else
            {
                $("#prodes").focus();                
            }
        }
        else if ( e.which == 27 ) 
        {
            limpiarcontroles();
        }
    });

    $("#prodes").on('keyup', function(){
        var value = $(this).val();   

        $( "#prodes" ).autocomplete({
          source: "autocomplete_producto/"+value+"/"+$("#tienda_fija").val(),
          minLength: 1,
          select: function( event, ui ) {
            var msn=ui.item.id ;
            $( "<div>" ).text(msn).prependTo( "#log" );
            $( "#log" ).scrollTop( 0 );              
            $("#valu").val(ui.item.codipro);
            $("#espxx").val(ui.item.nombre_especifico);
            //traer_producto(ui.item.nProAlmCod,ui.item.nombre_especifico);
            
          }
        });
    }).keyup();

    $("#prodes").bind('keydown',function(e){
        /*if(""!=$.trim($("#prodes").val()))
        {*/
            if ( e.which == 13 ) 
            {                
                $("#pes").focus();
            }
            else if ( e.which == 27 ) 
            {
                $("#codpro").focus();
            }           
        //}

    });

    $("#pes").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            agredarsubdetalle();
            //calcularsubtotal(num);
        }
        else if ( e.which == 27 ) 
        {            
            $("#prodes").focus();
        }
    });


    /********************************** INICIO: MANEJO DE TABLA *************************/

    /***********************combo anidado almacén producto ***********************/
    function buscar_array(array,buscar)
    {
        for (var i = 0; i <array.length; i++) 
        {
            if(buscar==array[i])
                return true;
        }
        return false;
    }



    $("#tienda").change(function(event){
        $("#produc").empty();
        $('#produc').selectpicker('refresh');
        var res = ($("#prod_agre").val()).split(",");        
        //alert(event.target.value);
        $.get("productoalm/"+event.target.value,function(response){            
        $("#produc").append("<option data-subtext='' value='0' >Todos los Productos...</option>"); 
        for(i=0;i<response.length;i++)
        {                
            if(buscar_array(res,response[i].nProAlmCod))
            {
                $("#produc").append("<option dato-esp='"+response[i].nombre_especifico+"' dato-stock='"+response[i].nProdAlmStock+"' dato-cod='"+response[i].id+"' value='"+response[i].nProAlmCod+"' disabled> "+response[i].value+"(¡Ya se agregó!)</option>");                          
            }
            else
            {
                $("#produc").append("<option dato-esp='"+response[i].nombre_especifico+"' dato-stock='"+response[i].nProdAlmStock+"' dato-cod='"+response[i].id+"' value='"+response[i].nProAlmCod+"' > "+response[i].value+"</option>");                          
            }
        }

        $("#stock").val("0");
        $('#produc').selectpicker('refresh');                 
        });
    });
    /********************************************************************************/
    $di_tiemsx=[];
    acum_itemtotal=0;
    acum_itemtotal_peso=0;
    function agredarsubdetalle()
    {          
        if(validaciondet())
        {
            $("#rpt").empty();
            if(validarAddDetalle())
            {
                // Obtenemos el total de columnas (tr) del id "tabla"  
                indice_tabla_2++;
                $("#conta2").val(indice_tabla_2);
                var nuevaFila="<tr id=filadet_"+indice_tabla_2+">";           

                nuevaFila+="<td>"+'<input type="hidden" name="cod_ndidet_'+indice_tabla_2+'" id="cod_ndidet_'+indice_tabla_2+'" value="0">'+'<i class="fa fa-hand-o-right" aria-hidden="true"></i> '+indice_tabla_2+"</td>";

                nuevaFila+="<td>"+'<input type="hidden" name="codb_'+indice_tabla_2+'" id="codb_'+indice_tabla_2+'" value="'+$("#codpro").val()+'">'+$("#codpro").val()+"</td>";

                nuevaFila+="<td style='font-size: 14px;width: 150px;'>"+'<input type="hidden" name="produdeta_'+indice_tabla_2+'" id="produdeta_'+indice_tabla_2+'" value="'+$("#prodes").val()+'">'+$("#prodes").val()+"</td>";
                
                    var aux_itemtotal=(($("#codpro").val()!="")?1:(($("#espxx").val()=="TELAS")?parseFloat($("#pes").val())/20:1)).toFixed(2);

                nuevaFila+="<td><input type='hidden'  name='cantixxx_"+indice_tabla_2+"' id='cantixxx_"+indice_tabla_2+"' value='"+aux_itemtotal+"'>"+aux_itemtotal+"</td>";

                    acum_itemtotal+=aux_itemtotal;
                    var caluxyz=parseFloat($("#tot_cant_di").html())+parseFloat(aux_itemtotal);
                    $("#tot_cant_di").html("");
                    $("#tot_cant_di").html(caluxyz.toFixed(2));
                    
                nuevaFila+="<td><input type='hidden'  name='cantipes_"+indice_tabla_2+"' id='cantipes_"+indice_tabla_2+"' value='"+$("#pes").val()+"'><input type='hidden'  name='valu_"+indice_tabla_2+"' id='valu_"+indice_tabla_2+"' value='"+$("#valu").val()+"'>"+$("#pes").val()+"</td>";
                    acum_itemtotal_peso+=parseFloat($("#pes").val());
                    $("#tot_pes_di").html("");
                    $("#tot_pes_di").html(acum_itemtotal_peso.toFixed(2));
         
                nuevaFila+='<td><div class="btn btn-link" onclick="delTabla('+indice_tabla_2+')" ><a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quitar"></i></a>'+'</div>';
                
                nuevaFila+="</tr>";
                
                $("#bandeja_detalle").append(nuevaFila);

                $di_tiemsx.push($("#valu").val()+","+$("#codpro").val()+","+$("#prodes").val()+","+$("#pes").val()+","+indice_tabla_2+","+$("#espxx").val());

                $("#prod_agredet").val($("#codb_"+indice_tabla_2).val()+","+$("#prod_agredet").val());
                var valore=parseFloat($("#codx_"+$("#valu").val()).text());
                $("#codx_"+$("#valu").val()).html(valore+parseFloat($("#pes").val()));
                $("#codx_fisi_"+$("#valu").val()).val(valore+parseFloat($("#pes").val()));                
                $("#difer_inve_"+$("#valu").val()).html(Math.abs(parseFloat($("#canti_x"+$("#valu").val()).val()) - (valore+parseFloat($("#pes").val()))));

                $("#tot_stockF_1").html(parseFloat($("#tot_stockF_1").html())+parseFloat($("#pes").val()));
                //$("#tot_dif_1").html(parseFloat($("#tot_dif_1").html()));//+parseFloat($("#difer_inve_"+$("#valu").val()).html()));

                var v_1=parseFloat($("#cont_itemsx").text());
                $("#cont_itemsx").html(++v_1);
                //$('#puni_'+indice_tabla).numeric(",").numeric({decimalPlaces: 3,negative: false});
                
                //calcular_totales(parseFloat($("#subt_x").val()),"inc");
                   
                limpiarcontroles();   

                
                // paginación   
                cant_items_real++;            
                // cargar lista de p/ginas
                controlpaginacion(cantxpag,cant_items_real);        
                // cargar paginación inicial            
                paginacion(ultima_pag,cantxpag);
            }
            
        }
        else
        {
            $("#rpt").html("Error: Debe rellenar los cuadro(s) marcados.");  
            //$("#codpro").focus();
        }
    }
    /*******************************************************************************/

    function agredardetalle_masivo(valx,datocod,textx,datostock)
    {          
        //if(validacion())
        //{
            // Obtenemos el total de columnas (tr) del id "tabla"  
            indice_tabla++;
            $("#conta").val(indice_tabla);
            var nuevaFila="<tr id=fila_"+indice_tabla+">";           

            nuevaFila+="<td>"+'<input type="hidden" name="cod_ndi_'+indice_tabla+'" id="cod_ndi_'+indice_tabla+'" value="0">'+'<i class="fa fa-hand-o-right" aria-hidden="true"></i> '+indice_tabla+"</td>";

            nuevaFila+="<td>"+'<input type="hidden" name="tie_'+indice_tabla+'" id="tie_'+indice_tabla+'" value="'+$("#tienda").val()+'">'+$("#tienda option:selected").text()+"</td>";

            nuevaFila+="<td style='font-size: 14px;width: 150px;'><input type='hidden'  name='valumaster_"+indice_tabla+"' id='valumaster_"+indice_tabla+"' value='"+datocod+"'>"+'<input type="hidden" name="produ_'+indice_tabla+'" id="produ_'+indice_tabla+'" value="'+valx+'">'+textx+"</td>";


            nuevaFila+="<td><input type='hidden'  name='canti_"+indice_tabla+"' id='canti_"+indice_tabla+"' value='"+datostock+"'><input type='hidden'  name='canti_x"+datocod+"' id='canti_x"+datocod+"' value='"+datostock+"'>"+datostock+"</td>";

            nuevaFila+="<input type='hidden'  name='codx_fisi_"+datocod+"' id='codx_fisi_"+datocod+"' value='0'><td id='codx_"+datocod+"'>0</td>";

            nuevaFila+="<td id='difer_inve_"+datocod+"' name='difer_inve_"+datocod+"'>"+datostock+"</td>";

            nuevaFila+="<td><div class='btn btn-success' name='btn_det_item_"+indice_tabla+"' id='btn_det_item_"+indice_tabla+"' onClick=di_mostrar('"+datocod+"');>Ver</div></td>";                 
            
            nuevaFila+="</tr>";
            
            $("#bandeja").append(nuevaFila);

            $("#prod_agre").val(valx+","+$("#prod_agre").val());
            $("#prod_codpro").val(datocod+","+$("#prod_codpro").val());

            $("#tot_stock_1").html(parseFloat($("#tot_stock_1").html())+parseFloat(datostock));
            $("#tot_stockF_1").html(parseFloat($("#tot_stockF_1").html())+parseFloat($("#codx_fisi_"+datocod).val()));
            //$("#tot_dif_1").html(parseFloat($("#tot_dif_1").html())+parseFloat($("#difer_inve_"+datocod).html()));
            
            $("#tienda_fija").val($("#tienda").val());
            $("#tienda").attr("disabled","disabled");
            $("#tienda").val("");
            $("#tienda").val($("#tienda_fija").val());
            //$("#tienda").change();
        //}
    }


    function agredardetalle()
    {          
        if(validacion())
        {
            // Obtenemos el total de columnas (tr) del id "tabla"  
            indice_tabla++;
            $("#conta").val(indice_tabla);
            var nuevaFila="<tr id=fila_"+indice_tabla+">";           

            nuevaFila+="<td>"+'<input type="hidden" name="cod_ndi_'+indice_tabla+'" id="cod_ndi_'+indice_tabla+'" value="0">'+'<i class="fa fa-hand-o-right" aria-hidden="true"></i> '+indice_tabla+"</td>";

            nuevaFila+="<td>"+'<input type="hidden" name="tie_'+indice_tabla+'" id="tie_'+indice_tabla+'" value="'+$("#tienda").val()+'">'+$("#tienda option:selected").text()+"</td>";

            /*nuevaFila+="<td>"+'<input type="hidden" name="codb_'+indice_tabla+'" id="codb_'+indice_tabla+'" value="'+$("#codpro").val()+'">'+$("#codpro").val()+"</td>";*/

            nuevaFila+="<td style='font-size: 14px;width: 150px;'><input type='hidden'  name='valumaster_"+indice_tabla+"' id='valumaster_"+indice_tabla+"' value='"+$("#produc option:selected").attr("dato-cod")+"'>"+'<input type="hidden" name="produ_'+indice_tabla+'" id="produ_'+indice_tabla+'" value="'+$("#produc").val()+'">'+$("#produc option:selected").text()+"</td>";


            nuevaFila+="<td><input type='hidden'  name='canti_"+indice_tabla+"' id='canti_"+indice_tabla+"' value='"+$("#produc option:selected").attr("dato-stock")+"'><input type='hidden'  name='canti_x"+$("#produc option:selected").attr("dato-cod")+"' id='canti_x"+$("#produc option:selected").attr("dato-cod")+"' value='"+$("#produc option:selected").attr("dato-stock")+"'>"+$("#produc option:selected").attr("dato-stock")+"</td>";

            nuevaFila+="<input type='hidden'  name='codx_fisi_"+$("#produc option:selected").attr("dato-cod")+"' id='codx_fisi_"+$("#produc option:selected").attr("dato-cod")+"' value='0'><td id='codx_"+$("#produc option:selected").attr("dato-cod")+"'>0</td>";

            nuevaFila+="<td id='difer_inve_"+$("#produc option:selected").attr("dato-cod")+"' name='difer_inve_"+$("#produc option:selected").attr("dato-cod")+"'>"+$("#produc option:selected").attr("dato-stock")+"</td>";
            
            nuevaFila+="<td><div class='btn btn-success' name='btn_det_item_"+indice_tabla+"' id='btn_det_item_"+indice_tabla+"' onClick=di_mostrar('"+$("#produc option:selected").attr("dato-cod")+"');>Ver</div></td>";
                 
            //nuevaFila+='<td><div class="btn btn-link" onclick="delTabla('+indice_tabla+')" >'+'<a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quitar"></i></a>'+'</div>';
            
            nuevaFila+="</tr>";
            
            $("#bandeja").append(nuevaFila);

            $("#prod_agre").val($("#produc").val()+","+$("#prod_agre").val());
            $("#prod_codpro").val($("#produc option:selected").attr("dato-cod")+","+$("#prod_codpro").val());

            $("#tot_stock_1").html(parseFloat($("#tot_stock_1").html())+parseFloat($("#produc option:selected").attr("dato-stock")));
            $("#tot_stockF_1").html(parseFloat($("#tot_stockF_1").html())+parseFloat($("#codx_fisi_"+$("#produc option:selected").attr("dato-cod")).val()));
            //$("#tot_dif_1").html(parseFloat($("#tot_dif_1").html())+parseFloat($("#difer_inve_"+$("#produc option:selected").attr("dato-cod")).html()));
            //$('#puni_'+indice_tabla).numeric(",").numeric({decimalPlaces: 3,negative: false});
            
            //calcular_totales(parseFloat($("#subt_x").val()),"inc");
               
            //limpiarcontroles();   

            
            // paginación   
            /*cant_items_real++;            
            // cargar lista de p/ginas
            controlpaginacion(cantxpag,cant_items_real);        
            // cargar paginación inicial            
            paginacion(ultima_pag,cantxpag);*/
            $("#tienda_fija").val($("#tienda").val());
            $("#tienda").attr("disabled","disabled");
            $("#tienda").val("");
            $("#tienda").val($("#tienda_fija").val());
            $("#tienda").change();
        }
        else if($("#produc").val()=='0')
        {
            var res = ($("#prod_agre").val()).split(",");
        
            $.get("productoalm/"+$("#tienda").val(),function(response)
            {            
                for(i=0;i<response.length;i++)
                {                
                    if(!buscar_array(res,response[i].nProAlmCod))
                    {          
                        agredardetalle_masivo(response[i].nProAlmCod,response[i].id,response[i].value,response[i].nProdAlmStock);
                    }
                }  
                $("#tienda").change();
            });
            
        }
    }

    function recalsubtotal(indice,prod)
    {        
        var auxsal=0;
        if(prod=="TELAS"){
            auxsal=(parseFloat($("#canti_"+indice).val())*parseFloat($("#puni_"+indice).val()));
        }
        else
            auxsal=(parseFloat($("#rollo_"+indice).val())*parseFloat($("#puni_"+indice).val()));   
        auxsal=(isNaN(auxsal)?0:auxsal);
        var ant=parseFloat($("#stotal_"+indice).val());
        var act=auxsal;
        $("#stotal_"+indice).val(auxsal.toFixed(2));
        //alert(act-ant);
        calcular_totales((act-ant),"inc");

        $("#stotalx_"+indice).empty();
        $("#stotalx_"+indice).html(auxsal.toFixed(2));
        //altualizar el subtotal
        //actualizar totales
    }

    cont_di=0;
    function di_mostrar(id)
    {
        /*$("#cobro").on('hidden.bs.modal', function () {                            
            $('#codpro').focus();
        }); */
        //recorrido de items ingresados
        
        for($iii=0;$iii<=500;$iii++)
        {
            $("#filadet_di_"+$iii).remove();     
        }
        var cont_di=0;
        var acum=0;
        var acum_canttt=0;
        $.each( $di_tiemsx, function( key, value ) 
        {
            var array_di=value.split(',');

            if(id==array_di[0])
            {
                //agregar a la tabla
                cont_di++;
                
                var nuevaFila="<tr id=filadet_di_"+cont_di+">";           

                nuevaFila+="<td>"+cont_di+"</td>";

                nuevaFila+="<td>"+array_di[1]+"</td>";

                nuevaFila+="<td>"+array_di[2]+"</td>";

                var aux_itemtotal_id=((array_di[1]!="")?1:((array_di[5]=="TELAS")?parseFloat(array_di[3])/20:1)).toFixed(2);
                nuevaFila+="<td>"+aux_itemtotal_id+"</td>";

                nuevaFila+="<td>"+parseFloat(array_di[3]).toFixed(2)+"</td>";

                nuevaFila+="</tr>";
                
                $("#bandeja_detalle_item").append(nuevaFila);
                acum_canttt+=parseFloat(aux_itemtotal_id);
                acum+=parseFloat(array_di[3]);
            }
          //alert( key + ": " + value );
        });
        
        var nuevaFila="<tr id=filadet_di_"+(++cont_di)+">";           

        nuevaFila+="<td colspan='3' style='text-align:center;'>TOTAL</td>";

        nuevaFila+="<td style='text-align:center;'>"+acum_canttt.toFixed(2)+"</td>";

        nuevaFila+="<td style='text-align:center;'>"+acum.toFixed(2)+"</td>";

        nuevaFila+="</tr>";
        
        $("#bandeja_detalle_item").append(nuevaFila);


        $("#item_det").modal("show");
    }


    function limpiarcontroles()
    {
        $("#prodes").val("");
        $("#subtext").val("");
        $("#esp").val("");
        $("#valu").val("");
                
        //$("#pes").prop( "readonly", true );        
        $("#pes").val("");
        $("#codpro").val("");
        $("#codpro").focus(); 
        $("#rpt").empty();
        $("#codpro").css("border","1px solid #d2d6de");
        $("#body_req").css("border","1px solid #d2d6de");        
    }

    function delTabla(id)
    {     
        
        if(confirm('Nota: El registro se retirará momentaneamente, pero el cambio no se hará efecto hasta que usted guarde los cambios.'))
        {
            var datos= $("#eliminados").val()+","+$("#cod_ndidet_"+id).val();
            $("#eliminados").val(datos);
            
            var valore=parseFloat($("#codx_"+$("#valu_"+id).val()).text());
            $("#codx_"+$("#valu_"+id).val()).html(valore-parseFloat($("#cantipes_"+id).val()));
            $("#codx_fisi_"+$("#valu_"+id).val()).val(valore-parseFloat($("#cantipes_"+id).val()));
            $("#difer_inve_"+$("#valu_"+id).val()).html(Math.abs(parseFloat($("#canti_x"+$("#valu_"+id).val()).val()) - (valore-parseFloat($("#cantipes_"+id).val()))));
            $("#tot_stockF_1").html(parseFloat($("#tot_stockF_1").html())-parseFloat($("#cantipes_"+id).val()));
            
            //acum_itemtotal-=parseFloat($("#cantipes_"+id).val());
            caluxyz=parseFloat($("#tot_cant_di").html())-parseFloat($("#cantixxx_"+id).val());
            $("#tot_cant_di").html("");
            $("#tot_cant_di").html(caluxyz.toFixed(2));

            caluyyy=parseFloat($("#tot_pes_di").html())-parseFloat($("#cantipes_"+id).val());
            $("#tot_pes_di").html("");
            $("#tot_pes_di").html(caluyyy.toFixed(2));

            var v_1=parseFloat($("#cont_itemsx").text());
            $("#cont_itemsx").html(--v_1);            

            var cb_escgdo=$.trim($("#codb_"+id).val());
            if(cb_escgdo!="")
            {
                var cadena_tra=quitar($("#prod_agredet").val(),cb_escgdo);
                $("#prod_agredet").val(cadena_tra);    
            }
            
            //------- para actualizar valores en el control ---
           
            //calcular_totales(parseFloat($("#stotal_"+id).val()),"dec");

            //limpiarcontroles();
            // fin controles-----------------------------------
            
            $("#filadet_"+id).remove();            

            cant_items_real--;
            // cargar lista de p/ginas
            controlpaginacion(cantxpag,cant_items_real);        
            // cargar paginación inicial 
            paginacion(indice_act,cantxpag);
            
            $.each( $di_tiemsx, function( key, value ) 
            {
                var array_di=value.split(',');

                if(id==array_di[4])
                {
                    $di_tiemsx.splice(key,1);                    
                }
              //alert( key + ": " + value );
            });
        }            
    }

    function quitar(cadena,elemento)
    {
        var array_auxi = cadena.split(",");    
        var cade="";
        for (var i = 0; i <array_auxi.length; i++) 
        {
            /*alert("->"+elemento+"="+array_auxi[i]);
            alert(elemento==array_auxi[i]); */
            if(elemento!=$.trim(array_auxi[i]))
                cade+=array_auxi[i]+",";
        }    
        //alert(cade);
        return cade;
    }

    function validacion()
    {
        var tienda_=$("#tienda").val(); 
        var producto_=$("#produc").val();     
        var key_error=true;
        
        if(tienda_=="")
        {
            $("#tienda").css("border","2px solid #f00");                
            key_error=false;
        }
        else
            $("#tienda").css("border","1px solid #d2d6de");

        
        if(producto_=="" || producto_=="0")
        {
            $("#produc").css("border","2px solid #f00");                
            key_error=false;
        }
        else
            $("#produc").css("border","1px solid #d2d6de");


        return key_error;
    }

    function validaciondet()
    {    
        var key_error=true;

        /*if($.trim($("#codpro").val())=="")
        {
            $("#codpro").css("border","2px solid #f00");                
            key_error=false;
        }
        else
            $("#codpro").css("border","1px solid #d2d6de");*/

        
        if($.trim($("#prodes").val())=="")
        {
            $("#prodes").css("border","2px solid #f00");                          
            key_error=false;
        }
        else
            $("#prodes").css("border","1px solid #d2d6de");

        if($.trim($("#pes").val())=="" || parseFloat($("#pes").val())<=0)
        {
            $("#pes").css("border","2px solid #f00");                
            key_error=false;
        }
        else
            $("#pes").css("border","1px solid #d2d6de");


        return key_error;
    }

    function validarAddDetalle()
    {
        var key_error=true;

        //verificar si existe producto para comparar
        var arra = ($("#prod_codpro").val()).split(",");
        if(!buscar_array(arra,$("#valu").val()))
        {
            $("#body_req").css("border","2px solid #f00");  
            $("#rpt").html("Error: No se puede agregar porque no está listado el producto correspondiente.");
            key_error=false;
        }
        else
        {
            //verificar codigo de barras
            if($("#valu").val()!="" && $.trim($("#esp").val())=="")
            {
                $("#rpt").empty();
                $("#codpro").css("border","1px solid #d2d6de");
                $("#body_req").css("border","1px solid #d2d6de");
            }          
            else
            {
                var arracodb = ($("#prod_agredet").val()).split(",");
                if(buscar_array(arracodb,$("#codpro").val()))
                {
                    limpiarcontroles();
                    $("#codpro").css("border","2px solid #f00");  
                    $("#rpt").html("Error: "+$("#codpro").val()+" no se puede agregar porque anteriormente ya se agrego este código.");
                    key_error=false;
                }
                else
                {
                    $("#rpt").empty();
                    $("#codpro").css("border","1px solid #d2d6de");
                    $("#body_req").css("border","1px solid #d2d6de");
                }       
            }
        }
        return key_error;
    }


    function controlpaginacion(pag,total)
    {
        var cad='';
        var act=true;
        var i;
        for(i=1;i<=total/pag;i++)
        {

            if(act)
                cad='<li onclick="paginacion('+i+','+pag+')" style="cursor: pointer;"><span>«</span></li>';

            cad+='<li '+((act)?'class="active"':'')+' onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            act=false;
        }
        if(total%pag>0)
        {
            cad+='<li onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            cad+='<li onclick="paginacion('+(i)+','+pag+');" style="cursor: pointer;"><span>»</span></li>';
        }
        else
            cad+='<li onclick="paginacion('+(--i)+','+pag+')" style="cursor: pointer;"><span>»</span></li>';

        ultima_pag=i;

        $(".pagination").empty();
        $(".pagination").html(cad);
    }

    function paginacion(pag,de)
    {   
        // ocultar registros de 10 en 10 

        var fin=pag*de;;
        var ini=(pag-1)*de;


        //meter todo en un array, con indice ordenado                        
        var array_items=[];
        var contador=0;
        for (var i = 1; i <= parseInt($("#conta2").val()); i++) 
        {                
            var nomfila="filadet_"+i;
            if ( $("#"+nomfila).length > 0 ) 
            {
                array_items[contador]=nomfila;
                contador++;
            }                
        }
        //recorrer segun los limites mostrar lo necesario

        for(var i=0;i<contador;i++)
        {
            if(i>=ini && i<fin)
                $("#"+array_items[i]).show();        
            else
                $("#"+array_items[i]).hide();
        }

        indice_act=pag;//guarada el indice de pág

        for(i=1;i<=ultima_pag;i++)
            $("#pgitem_"+i).removeAttr('class');

        $("#pgitem_"+indice_act).attr('class','active');

        return 1;
    }
    /********************************* FIN: TABLA ***************************************/
</script>
@endpush('scripts')