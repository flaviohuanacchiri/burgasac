@extends('backend.layouts.appv2')

@section('title', 'BANDEJA DE CIERRE DE INVENTARIO')

@section('after-styles')
    <style>
    .dropdown{padding: 0;}
    .dropdown .dropdown-menu{border: 1px solid #999}
    .detallescompra{
        display: none;
        background-color: #ececec;
    }
    </style>
@stop

@section('content')

	<div class="row">
		<div class="col-sm-12">
			<section class="content">
			    <div class="box box-info">
			        <div class="box-header with-border">
			            <h3 class="box-title">@yield('title')</h3>
			            <div class="box-tools pull-right">
			                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			            </div><!-- /.box tools -->
			        </div><!-- /.box-header -->
			        <div class="box-body">
			            <div class="row">
			            	<form action="{{ route('inventario.index') }}" method="GET" >
								{{ csrf_field() }}
								<input type="hidden" name="_method" value="GET">

		                    	<div class="col-md-4"> 
		                    		<label>Fecha de Cierre</label>
				                    <input class="form-control" type="date" name="fech" id="fech" step="1" min="2000-01-01" value="<?php echo date("Y-m-d");?>" tabindex="1">
		                    	</div>
		                    	<div class="col-md-4"> 
		                    		<label>Tienda</label>
				                    <select class="form-control" name="tien" id="tien">
								      	<option value="">Todas las tiendas...</option>
								      	@foreach($tiendas as $t)
									        <option value="{{$t->nAlmCod}}">{{$t->cAlmNom}}({{$t->cAlmUbi}})</option>	
								        @endforeach
								    </select>	
		                    	</div>
		                    	<div class="col-md-4" style="text-align:center;">
	                                <label style="color:white;">.</label>
	                                <button type="submit" class="btn btn-primary form-control"><span class="fa fa-search" aria-hidden="true"></span> Buscar</button>
	                            </div>
				    		</form>
	                    </div>
	                    <br>
	                    <div class="row">
	                    	<div class="col-md-12"> 
								@include('comercializacion.inventario.fragment.info')
								<table class="table table-hover table-striped">
									<thead>
										<tr>
											<th>Item</th>
											<th>Fecha y Hora de Cierre</th>									
											<th>Código de Cierre de Inventario</th>
											<th>Tienda/Almacén</th>									
											<th colspan="2">Opc</th>
										</tr>
									</thead>
									<tbody style="text-align: center;">
										<?php  
				                    		$vari=(isset($_GET["page"]))?$_GET["page"]:1;
				                    		$cont=($vari-1)*10; 
				                    	?>
										@foreach($cajaap as $cap)
										<tr>
											<td>{{++$cont}}</td>
											<td>{{ $cap->dInvFecHor }}</td>									
											<td>{{ $cap->cInvCod }}</td>
											<td>{{ $cap->almacen->cAlmNom }}</td>
											<td>
												<a href="{{ route('inventario.reportecierre',$cap->cInvCod) }}" class="btn btn-xs btn-info" target="_blank">
													IC
												</a> 
											</td>
											<td>
												<a href="{{ route('inventario.detalleingreso',$cap->cInvCod) }}" class="btn btn-xs btn-info" target="_blank">
													Ver DI
												</a>
											</td>											
										</tr>
										@endforeach
									</tbody>
								</table>
								{!! $cajaap->render() !!}
							</div>
						</div>
			        </div><!-- /.box-body -->
			    </div><!--box box-success-->
			</section>
		</div>
	</div>

@endsection

@push('scripts')

<script type="text/javascript">	
    $(document).bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
        	$(location).attr('href',"{{ route('almacen.create') }}");
        };
    });
</script>

@endpush('scripts')
