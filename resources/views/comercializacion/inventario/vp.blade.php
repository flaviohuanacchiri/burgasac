<style type="text/css">
       .contenedor-tabla
        {
            display: table;
            width: 100%;
            font-size: 15px;
        }

	.contenedor-fila
        {
            display: table-row;            
        }

        .contenedor-columna
        {
            display: table-cell;
            padding-top: 1px;  
	}


        body{
            /**font-family: monospace;*/
            /*font-family: "Lucida Console", "Lucida Sans Typewriter", monaco, "Bitstream Vera Sans Mono", monospace; */
            font-family: "Times New Roman", Times, serif;
        }

        table, td, th {
            border: 1px solid black;
            font-size: 13px;
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th {
            height: 5px;
        }
    </style>

<title>Reporte Cierre de Inentario</title>

<body style="margin-left: 4px;margin-right: 55px;">     
    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;text-align: center;padding-top: 10px;">
                <!--img src="img/logo.jpg" alt="Smiley face" height="25" width="45"-->
             </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;padding-top: 20px;padding-left: 10px;">
                <h1 style="margin: 0px;font-size: 20px;">VISTA PREVIA DE CIERRE DE INVENTARIO</h1>
            </div>
            <div class="contenedor-columna" style="width: 20%;text-align: right;">
                
            </div>
        </div>      
    </div>    

<!--div style="clear:both;"></div-->
<br>
    
    <div class="row">                                          
        <div class="col_x">
            <table id="bandeja" name="bandeja" class="table table-striped table-bordered table-hover">
                <?php echo $datos->codigo;?>
            </table>
        </div>
    </div>
</body>
