<style type="text/css">
    .contenedor-tabla { display: table; width: 100%; font-size: 15px; }
    .contenedor-fila { display: table-row;}
    .contenedor-columna { display: table-cell; padding-top: 1px; }
    body{font-family: "Times New Roman", Times, serif;}

        table, td, th {
            border: 1px solid black;
            font-size: 13px;
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th {
            height: 5px;
        }
    </style>

<title>Reporte Cierre de Inentario</title>

<body style="margin-left: 4px;margin-right: 55px;">     
    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;text-align: center;padding-top: 10px;">
                <!--img src="img/logo.jpg" alt="Smiley face" height="25" width="45"-->
             </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;padding-top: 20px;padding-left: 10px;">
                <h1 style="margin: 0px;font-size: 20px;">CIERRE DE INVENTARIO: {{$datos->cInvCod}}</h1>
            </div>
            <div class="contenedor-columna" style="width: 20%;text-align: right;">
                
            </div>
        </div>      
    </div>    
    
<br>
    <div class="contenedor-tabla">                
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 5%;">
                TIENDA: 
             </div>
            <div class="contenedor-columna" style="width: 65%;">
                 {{$datos->almacen->cAlmNom}}
            </div>
            <div class="contenedor-columna" style="width: 30%;text-align: right;">
                FECHA: {{$fecha}}
            </div>
        </div>
    </div>

<!--div style="clear:both;"></div-->
<br>
    
    <div class="row">                                          
        <div class="col_x">
            <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
                <tr>
                    <th>
                        ITEM
                    </th>
                    <th>
                        DESCRIPCIÓN
                    </th>
                    
                    <th>
                        INICIAL ANTES
                    </th>
                    <th>
                        INGRESO
                    </th>
                    <th>
                        SALIDA
                    </th>
                    <th>
                        SALDO
                    </th> 
                                      
                    <th>
                        INICIAL NUEVO
                    </th>
                    <th>
                        DIFERENCIA
                    </th> 
                </tr>   
                <?php $i=0;
                    //echo "<pre>";
                    //print_r($datos->ingreso);
                    //echo "</pre>";
                    //dd();
                ?>
                @foreach($datos->ingreso as $iv)
                <tr>                                                            
                    <td>{{++$i}}</td> 
                    <?php $valinicio=0; ?>
                    @if(is_null($iv->nProAlmCod)==0)
                        @if(!is_null($iv->proalm))
                            <td>{{$iv->proalm->producto->nombre_generico.' - '.$iv->proalm->color->nombre}}</td>
                        @else
                            <td></td>
                        @endif
                        <td>{{$iv["inicialAntes"]}}</td> 
                        @php
                            if (is_object($iv)) {
                                if (!is_null($iv->proalm)) {
                                    $valinicio+=$iv->proalm->nProdAlmStockIni;
                                }
                            }
                        @endphp
                    @elseif(is_null($iv->nCodTC)==0) 
                        <td>{{$iv->resStocktelas->producto->nombre_generico.' - '.$iv->resStocktelas->producto->nombre_especifico}}</td>
                        <td>0.00</td>
                    @else
                        @if($iv->resStockMP->insumo_id==0)
                            <td>{{ $iv->resStockMP->accesorio->nombre }} (MP)</td>
                            <td>0.00</td>
                        @else
                            <td>{{ $iv->resStockMP->insumo->nombre_generico }} (MP)</td>
                            <td></td>
                        @endif
                        
                    @endif
                                              
                                               
                    <?php $cont_sum1=0;
                        $htmlingreso ="";
                        $codBarras = [];
                        try {
                            if (is_null($iv->nProAlmCod)==0) {
                                foreach ($trans_i as $key => $value_trans_i) //revizar trasferencia
                                {
                                    foreach ($value_trans_i->transferenciadetalle as $key => $value) 
                                    {                          
                                        if (!is_null($value)) {
                                            if ($value->cod_barras == "") {
                                                if($value->productoalmacens->id_color==$iv->proalm->id_color &&
                                                $value->productoalmacens->cProdCod==$iv->proalm->cProdCod)
                                                {
                                                    $cont_sum1+=$value->dTraCant;
                                                    $htmlingreso.="\nTransferencia Sin Codigo de Barras : ".$value->dTraCant;
                                                }
                                            }
                                            if ($value->cod_barras != "") {
                                                if (!is_null($value)) {
                                                    if (!is_null($value->productoalmacens)) {
                                                        
                                                        //try {
                                                            if ($value->productoalmacens->id_color==$iv->proalm->id_color &&
                                                                $value->productoalmacens->cProdCod==$iv->proalm->cProdCod) {
                                                                $cont_sum1+=$value->dTraCant;
                                                                $htmlingreso.="\nTransferencia Con Codigo de Barras : ".$value->dTraCant;
                                                                $codBarras[$value->cod_barras] = $value->cod_barras;
                                                            }
                                                                
                                                        //} catch (Exception $e) {
                                                          //  echo $e->getMessage();
                                                        //}
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }    
                            
                                foreach ($ntaing_i as $key => $nta_ii) 
                                {
                                    if($nta_ii->nProAlmCod==$iv->nProAlmCod)
                                    {
                                        $cont_sum1+=$nta_ii->peso_cant;
                                        $htmlingreso.="\nN.Ingreso : ".$nta_ii->peso_cant;
                                    }                         
                                } 

                                foreach ($ntainga_i as $key => $ntaa_ii) 
                                {
                                    if($ntaa_ii->nProAlmCod==$iv->nProAlmCod)
                                    {
                                        $cont_sum1+=$ntaa_ii->peso_cant;
                                        $htmlingreso.="\nN.Ingreso A: ".$ntaa_ii->peso_cant;
                                    }                         
                                }    

                                foreach ($prodev_i as $key => $prodv) 
                                {
                                    if($prodv->nProAlmCod==$iv->nProAlmCod)
                                    {
                                        $cont_sum1+=$prodv->nDevPeso;
                                        $htmlingreso.="\nDevolucion : ".$prodv->nDevPeso;
                                    }                         
                                }
                            }
                        } catch (Exception $e) {
                            $htmlingreso.="\nError aqui!!!!";
                        }

                    ?>
                    <td>{{$cont_sum1}}</td> 
                    <?php $cont_sum2=0;
                        $htmlsalida = "";
                        if(is_null($iv->nProAlmCod)==0)  
                        {
                            foreach ($trans_e as $key => $value_trans_e) 
                            {
                                foreach ($value_trans_e->transferenciadetalle as $key => $value) 
                                {
                                    if($value->productoalmacens->id_color==$iv->proalm->id_color &&
                                    $value->productoalmacens->cProdCod==$iv->proalm->cProdCod)                                    
                                    {
                                        $cont_sum2+=$value->dTraCant;
                                        $htmlsalida.="\nTransferencia : ".$value->dTraCant;
                                    }
                                }
                            }    

                            foreach ($venta_e as $key => $v_E) 
                            {
                                if($v_E->nProAlmCod==$iv->nProAlmCod)
                                {
                                    $cont_sum2+=$v_E->nVtaPeso;
                                    $htmlsalida.="\n>Venta : ".$v_E->nVtaPeso;
                                }
                             
                            }  

                            foreach($detVentDEv_e   as $key => $dvd) 
                            {
                                if($dvd->nProAlmCod==$iv->nProAlmCod)
                                {
                                    $cont_sum2+=$dvd->nVtaDevPeso;
                                    $htmlsalida.="\nDevolucion : ".$dvd->nVtaDevPeso;
                                }
                             
                            }
                        }                     
                    ?>                           
                    <td>{{$cont_sum2}}</td>  
                    <td>{{number_format((($iv->inicialAntes+$cont_sum1)-$cont_sum2),2)}}</td>               
                    <td>{{number_format($iv->nStockFisTotal,2)}}</td>
                    @php
                        $diferencia = $iv->inicialAntes + $cont_sum1 - $cont_sum2 - $iv->nStockFisTotal;
                    @endphp
                    @if ($diferencia < 0)
                        <td style="font-weight: 700; color: red">{{number_format($diferencia,2)}}</td>
                    @else
                        <td style="font-weight: 700">{{number_format($diferencia,2)}}</td>
                    @endif                        
                                                
                </tr>
                @endforeach   
                                                    
            </table>
        </div>
    </div>
</body>
