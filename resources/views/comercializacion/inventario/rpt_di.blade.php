<style type="text/css">
       .contenedor-tabla
        {
            display: table;
            width: 100%;
            font-size: 15px;
        }

	.contenedor-fila
        {
            display: table-row;            
        }

        .contenedor-columna
        {
            display: table-cell;
            padding-top: 1px;  
	}


        body{
            /**font-family: monospace;*/
            /*font-family: "Lucida Console", "Lucida Sans Typewriter", monaco, "Bitstream Vera Sans Mono", monospace; */
            font-family: "Times New Roman", Times, serif;
        }

        table, td, th {
            border: 1px solid black;
            font-size: 13px;
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th {
            height: 5px;
        }
    </style>

<title>Reporte Detallado de Ingreso</title>

<body style="margin-left: 4px;margin-right: 55px;">     
    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;text-align: center;padding-top: 10px;">
                <!--img src="img/logo.jpg" alt="Smiley face" height="25" width="45"-->
             </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;padding-top: 20px;padding-left: 10px;">
                <h1 style="margin: 0px;font-size: 20px;">REPORTE DETALLADO DE INGRESO DEL INVENTARIO: {{$datos0->cInvCod}}</h1>
            </div>
            <div class="contenedor-columna" style="width: 20%;text-align: right;">
                
            </div>
        </div>      
    </div>    
    
<br>
    <div class="contenedor-tabla">                
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 5%;">
                TIENDA: 
            </div>
            <div class="contenedor-columna" style="width: 55%;">
                 {{$datos0->almacen->cAlmNom}}
            </div>
            <div class="contenedor-columna" style="width: 30%;text-align: center;">
                FECHA: {{$fecha}}
            </div>
        </div>
    </div>

<!--div style="clear:both;"></div-->
<br>
    
    <div class="row">                                          
        <div class="col_x">
            <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
                <tr>
                    <th>
                        ITEM
                    </th>
                    <th>
                        CÓD.BARRAS
                    </th>
                    <th>
                        PRODUCTO
                    </th>
                    
                    <th>
                        CANTIDAD
                    </th>
                    <th>
                        PESO
                    </th>                    
                </tr>   
                <?php $i=0;$sum_1=0;$sum_2=0; ?>
                @foreach($datos as $iv)
                <tr>                                                            
                    <td>{{++$i}}</td> 
                    <td>{{$iv->cCodBarra}}</td>  
                    <td>{{$iv->cDetalle}}</td>
                    <td>{{number_format($iv->dCanUni,2)}}</td>
                    <td>{{number_format($iv->dPesoUni,2)}}</td>               
                                                                
                </tr>
                <?php 
                    $sum_1+=$iv->dPesoUni; 
                    $sum_2+=$iv->dCanUni; 
                ?>
                @endforeach   
                <tr>
                	<td colspan="3" style="text-align: center;">TOTAL</td>
                	<td>{{number_format($sum_2,2)}}</td>
                    <td>{{number_format($sum_1,2)}}</td>
                	<!--td> - </td-->
                </tr>                               
            </table>
        </div>
    </div>
</body>
