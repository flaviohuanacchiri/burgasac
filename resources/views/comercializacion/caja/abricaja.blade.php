@extends('backend.layouts.appv2')

@section('subtitulo', 'APERTURA DE CAJA')

@section('content')


<link href="{{ asset('css/form.css') }}" rel="stylesheet">

<div class="row">
	<div class="col-sm-12">
		<section class="content">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title">@yield('subtitulo')</h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->
		        <div class="box-body">		            
		             @include('comercializacion.caja.fragment.info')

		            <form action="{{ route('caja.abrircaja_store',$id) }}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="POST">
		                <div class="form-group">
		                	<div class="row">
			                	<div class="col-md-12">
			                		<label>Usuario Caja: {{$nameuser}}</label>
			                	</div>
			                </div>
			                <div class="row">
			                	<div class="col-md-3">
			                		<label>Tienda</label>
			                    	<input class="form-control" type="text" name="tie" id="tie" value="{{$almacen}}" maxlength="255" readonly>
			                	</div>
			                	<div class="col-md-2">
			                		<label>Caja</label>
			                    	<input class="form-control" type="text" name="caj" id="caj" value="{{$area}}" maxlength="255" readonly>
			                	</div>
			                	<div class="col-md-3">
			                		<label>Fecha Ape.</label>
			                    	<input class="form-control" type="text" name="fec" id="fec" value="{{$fecha}}" maxlength="255" readonly>			                	</div>
			                	<div class="col-md-2 {{ $errors->has('mon') ? 'has-error' :'' }}">
			                		<label>Monto Ape.</label>
			                    	<input class="form-control" type="text" name="mon" id="mon" placeholder="0.00" value="0.00" min="0.00" maxlength="17"  onkeypress="return tabular(event,this)" autofocus="autofocus">
			                    	{!! $errors->first('mon','<span class="help-block">:message</span>') !!}
			                	</div>
			                	<div class="col-md-2">
			                		<div class="row-fluid">
			                			<br>
			                			 <button class="btn btn-success">Aperturar</button>
								    </div>
			                	</div>			                	
			                </div>
			            </div>
		                
		            </form>

    			</div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<section class="content" style="    min-height: 188px;">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title"></h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->
		        <div class="box-body">
		        	<div class="col-md-12" style="text-align: right;"> 
			        	<form action="{{ route('caja.abrircaja') }}" method="GET" >
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="GET">
							<label>Fecha de Apertura</label>
							<input class="" type="date" name="fech" id="fech" step="1" min="2000-01-01" value="<?php echo date("Y-m-d");?>" tabindex="1">
				            <button type="submit" class="btn btn-success btn-xs" style="height: 26px;margin-left: -3px;margin-top: -4px;border-radius: 0px 3px 3px 0px;"><span class="fa fa-search" aria-hidden="true"></span></button>
			    		</form>       
			        </div>
			        <br>
			        <br>
			        
		        	<div class="row">
			            <div class="col-md-12"> 
				        	<table class="table table-striped table-bordered table-hover">
		                        <thead>		       
		                        	<th>Item</th>                     
		                            <th>Fecha y hora de apertura</th>		
		                            <th>Código apertura</th>		
		                            <th>Monto de apertura</th>		
		                            <!--th>Elimnar</th-->		                                                    
		                        </thead>
			                    <tbody style="text-align: center;">
			                    	<?php  
			                    		$vari=(isset($_GET["page"]))?$_GET["page"]:1;
			                    		$cont=($vari-1)*5; 
			                    	?>
									@foreach($apertura2 as $ap)
									<tr>
										<td>{{++$cont}}</td>
										<td>{{ $ap->fCajFecApe }}</td>
										<td>{{ $ap->cCajApeCod }}</td>
										<td>{{ $ap->dCajMonApe }}</td>	
										<!--td>
											<a data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Delete" data-trans-title="Are you sure?" class="btn btn-xs btn-danger" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
												<i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
												
												<form action="{{ route('area.destroy',$ap->cCajApeCod) }}" method="POST" name="delete_item" style="display:none">
												   {{ csrf_field() }}
													<input type="hidden" name="_method" value="DELETE">
											
												</form>
											</a>
										</td-->
									</tr>
									@endforeach
								</tbody>
							</table>
							{!! $apertura2->render() !!}
		                </div>
		        </div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">

    function tabular(e,obj) {
     tecla=(document.all) ? e.keyCode : e.which;
     if(tecla!=13) return;
     frm=obj.form;
     for(i=0;i<frm.elements.length;i++)
       if(frm.elements[i]==obj) {
         if (i==frm.elements.length-1) i=-1;
         break }
     frm.elements[i+1].focus();
     return false;
    }

    if(!("autofocus" in document.createElement("input")))
       document.getElementById("uno").focus();

	$('#mon').numeric(",").numeric({decimalPlaces: 2,negative: false});

</script>
@endpush