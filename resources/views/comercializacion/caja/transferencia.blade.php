@extends('backend.layouts.appv2')

@section('subtitulo', 'TRASFERIR DESDE CAJA')

@section('content')


<link href="{{ asset('css/form.css') }}" rel="stylesheet">

<div class="row">
	<div class="col-sm-12">
		<section class="content">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title">@yield('subtitulo')</h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->
		        <div class="box-body">
		             @include('comercializacion.caja.fragment.info')

		            <form action="{{ route('caja.transferencia_store',$id) }}" method="POST" id="fortrans">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="POST">
						
		                <div class="form-group">
		                	<div class="row">
			                	<div class="col-md-12">
			                		<label>Usuario: {{$nameuser}}</label>
			                	</div>
			                </div>
			                <div class="row">
			                	<div class="col-md-4">
			                		<label>Tienda</label>
			                    	<input class="form-control" type="text" name="tie" id="tie" value="{{$almacen}}" readonly>
			                	</div>
			                	<div class="col-md-4">
			                		<label>Caja</label>
			                    	<input class="form-control" type="text" name="caj" id="caj" value="{{$area}}" readonly>	
			                	</div>
			                	<div class="col-md-4">
			                		<label>Monto Disponible</label>
			                    	<input class="form-control" type="text" name="mondis" id="mondis" value="{{$montodef}}" readonly>	
			                	</div>
			                </div>
			                <div class="row">
			                	<div class="col-md-3">
			                		<label>Fecha</label>
			                    	<input class="form-control" type="text" name="fec" id="fec" value="{{$fecha}}" readonly>
			                	</div>
			                	<div class="col-md-3 {{ $errors->has('tien') ? 'has-error' :'' }}"> 
		                    		<label>Tienda</label>
				                    <select class="form-control" name="tien" id="tien" autofocus="autofocus">
								      	<option value="">Todas las tiendas...</option>
								      	@foreach($tiendas as $t)
									        <option value="{{$t->nAlmCod}}">{{$t->cAlmNom}}({{$t->cAlmUbi}})</option>	
								        @endforeach
								    </select>	
								    {!! $errors->first('tien','<span class="help-block">:message</span>') !!}	
		                    	</div>
		                    	<div class="col-md-2 {{ $errors->has('caja') ? 'has-error' :'' }}"> 
		                    		<label>Caja</label>
				                    <select class="form-control" name="caja" id="caja">							      	
								    </select>
								    {!! $errors->first('caja','<span class="help-block">:message</span>') !!}	
		                    	</div>
			                	<div class="col-md-2 {{ $errors->has('mon') ? 'has-error' :'' }}">
			                		<label>Monto</label>
			                    	<input class="form-control" type="text" name="mon" id="mon" placeholder="0.00" value="{{ old('mon') }}" min="0.00" maxlength="17"  onkeypress="return tabular(event,this)">
			                    	{!! $errors->first('mon','<span class="help-block">:message</span>') !!}	
			                    	<p id="obs_vuel" name="obs_vuel" style="color: red;"></p>
			                	</div>			                				                
			                	<div class="col-md-2">
			                		<div class="row-fluid">
			                			<br>
			                			<a class="btn btn-success" onclick="trans();">
											<i class="fa fa-print" data-toggle="tooltip" data-placement="top" title="Transferir" data-original-title="Transferir"></i> Transferir
										</a> 
								    </div>
			                	</div>			                	
			                </div>
			            </div>
		                
		            </form>

    			</div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<section class="content" style="    min-height: 188px;">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title"></h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->
		        <div class="box-body">
		        	<div class="col-md-12" style="text-align: right;"> 
			        	<form action="{{ route('caja.cerrarcaja',$id) }}" method="GET" >
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="GET">
							<label>Fecha de Transferencia: </label>
							<input class="" type="date" name="fech" id="fech" step="1" min="2000-01-01" value="<?php echo date("Y-m-d");?>" tabindex="1">
				            <button type="submit" class="btn btn-success btn-xs" style="height: 26px;margin-left: -3px;margin-top: -4px;border-radius: 0px 3px 3px 0px;"><span class="fa fa-search" aria-hidden="true"></span></button>
			    		</form>       
			        </div>
			        <br>
			        <br>
			        
		        	<div class="row">
			            <div class="col-md-12"> 
				        	<table class="table table-striped table-bordered table-hover">
		                        <thead>
		                        	<th>Item</th> 
		                            <th>Fecha y hora de transferencia</th>		
		                            <th>Código de transferencia</th>		
		                            <th>De Caja</th>		
		                            <th>A Caja</th>		
		                            <th>Monto</th>		
		                            <th>Eliminar</th>
		                        </thead>
			                    <tbody style="text-align: center;">
			                    		<?php  
			                    		$vari=(isset($_GET["page"]))?$_GET["page"]:1;
			                    		$cont=($vari-1)*5; 
			                    		?>
										@foreach($lista_trans as $lt)
										<tr>
											<td>{{++$cont}}</td>
											<td>{{ $lt->tTraCajFec }}</td>
											<td>{{ $lt->cTraCaj }}</td>
											<td>{{ $lt->cajaapertura1->rolarea->areasalmacen->areas->cAreNom }}</td>	
											<td>{{ $lt->cajaapertura2->rolarea->areasalmacen->areas->cAreNom }}</td>	
											<td>{{ $lt->dTraCajMon }}</td>
											<td>
												<a data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Delete" data-trans-title="Are you sure?" class="btn btn-xs btn-danger" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
													<i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
													
													<form action="{{ route('caja.transferencia_destroy',$lt->cTraCaj) }}" method="POST" name="delete_item" style="display:none">
													   {{ csrf_field() }}
														<input type="hidden" name="_method" value="DELETE">
													</form>
												</a>
											</td>
										</tr>
										@endforeach
								</tbody>
							</table>
							{!! $lista_trans->render() !!}
								
		                </div>
		        </div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">

	var key_error=false;

    function tabular(e,obj) {
     tecla=(document.all) ? e.keyCode : e.which;
     if(tecla!=13) return;
     frm=obj.form;
     for(i=0;i<frm.elements.length;i++)
       if(frm.elements[i]==obj) {
         if (i==frm.elements.length-1) i=-1;
         break }
     frm.elements[i+1].focus();
     return false;
    }

    if(!("autofocus" in document.createElement("input")))
       document.getElementById("uno").focus();

	$('#mon').numeric(",").numeric({decimalPlaces: 2,negative: false});

	/***********************combo anidado almacén1 a almacén2 ***********************/
    $("#tien").change(function(event){
   		$("#caja").empty();
   		//$('#caja').selectpicker('refresh');
        //$("#codpro").prop( "readonly", true );
   		$.get("tiendaare2/"+event.target.value,function(response,alm_id){   			
   			$("#caja").append("<option value='' >Seleccione un área...</option>");   
   			for(i=0;i<response.length;i++)
   			{
   				if(response[i].nAreAlmCod != {{$cod_almacen}})
   					$("#caja").append("<option value='"+response[i].cCajApeCod+"' > "+response[i].cAreNom+"</option>");       	
   			}  
   		//$('#caja').selectpicker('refresh');     			
   		})
    });

    $("#mon").on('keyup', function()
    {
        var value = $(this).val();           
       
        if(typeof(value) !== "undefined" && value!="")
        {
            if(parseFloat(value)>parseFloat($("#mondis").val()))
            {            
                $(this).css("border","2px solid #f00");   
                $(this).focus();  
                $("#obs_vuel").html("¡El monto debe ser menor al disponible..!");  
                key_error=false;            
            }
            else
            {
                $(this).css("border","1px solid #d2d6de");
            	$("#obs_vuel").empty();
            	key_error=true; 
            }
        }
        //else          
        //    $("#subt_x").val("0.00"); 
        
    }).keyup();
    /*******************************************************************************/
    function trans()
    {   
        if(key_error)
            $("#fortrans").submit();  
        else       
        {
        	$("#mon").css("border","2px solid #f00");   
            $("#mon").focus();  
            $("#obs_vuel").html("¡El monto debe ser menor al disponible..!");  
            key_error=false; 
        }
    }
</script>
@endpush