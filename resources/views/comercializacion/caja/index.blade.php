@extends('backend.layouts.appv2')

@section('title', 'BANDEJA DE OPERACIONES DE CAJA')

@section('after-styles')
    <style>
    .dropdown{padding: 0;}
    .dropdown .dropdown-menu{border: 1px solid #999}
    .detallescompra{
        display: none;
        background-color: #ececec;
    }
    </style>
@stop

@section('content')

	<div class="row">
		<div class="col-sm-12">
			<section class="content">
			    <div class="box box-info">
			        <div class="box-header with-border">
			            <h3 class="box-title">@yield('title')</h3>
			            <div class="box-tools pull-right">
			                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			            </div><!-- /.box tools -->
			        </div><!-- /.box-header -->
			        <div class="box-body">
			            <div class="row">
			            	<form action="{{ route('caja.index') }}" method="GET" >
								{{ csrf_field() }}
								<input type="hidden" name="_method" value="GET">

		                    	<div class="col-md-3"> 
		                    		<label>Fecha</label>
				                    <input class="form-control" type="date" name="fech" id="fech" step="1" min="2000-01-01" value="<?php echo date("Y-m-d");?>" tabindex="1">
		                    	</div>
		                    	<div class="col-md-3"> 
		                    		<label>Tienda</label>
				                    <select class="form-control" name="tien" id="tien">
								      	<option value="">Todas las tiendas...</option>
								      	@foreach($tiendas as $t)
									        <option value="{{$t->nAlmCod}}">{{$t->cAlmNom}}({{$t->cAlmUbi}})</option>	
								        @endforeach
								    </select>	
		                    	</div>
		                    	<div class="col-md-2"> 
		                    		<label>Caja</label>
				                    <select class="form-control" name="caja" id="caja">							      	
								    </select>
		                    	</div>
		                    	<div class="col-md-2"> 
		                    		<label>Vendedor</label>
				                    <select class="form-control" name="vend" id="vend">							      	
								    </select>
		                    	</div>
		                    	<div class="col-md-2" style="text-align:center;">
	                                <label style="color:white;">.</label>
	                                <button type="submit" class="btn btn-primary form-control"><span class="fa fa-search" aria-hidden="true"></span> Buscar</button>
	                            </div>
				    		</form>
	                    </div>
	                    <br>
	                    <div class="row">
	                    	<div class="col-md-12"> 
								@include('comercializacion.caja.fragment.info')
								<table class="table table-hover table-striped">
									<thead>
										<tr>
											<th>Item</th>																								
											<th>Fecha de Apertura</th>									
											<th>Fecha de Cierre</th>
											<th>Tienda</th>									
											<th>Caja</th>									
											<th>Estado</th>
											<th>Vendedor</th>									
											<th>Monto de Apertura</th>									
											<th>Monto de Cierre</th>
											<th colspan="3">Opc</th>
										</tr>
									</thead>
									<tbody style="text-align: center;">
										<?php  
				                    		$vari=(isset($_GET["page"]))?$_GET["page"]:1;
				                    		$cont=($vari-1)*10; 
				                    	?>
										@foreach($cajaap as $cap)
										<tr>
											<td>{{++$cont}}</td>
											<td>{{ $cap->fCajFecApe }}</td>									
											<td>{{ $cap->cajacierre[0]->fCajFecCie }}</td>
											<td>{{ $cap->rolarea->areasalmacen->almacen->cAlmNom }}</td>									
											<td>{{ $cap->rolarea->areasalmacen->areas->cAreNom }}</td>
											<td>{{ ($cap->bCajCieAbierto==0)?'Abierto':'Cerrado' }}</td>							
											<td>{{ $cap->rolarea->user->name }}</td>							
											<td>{{ number_format($cap->dCajMonApe,2) }}</td>		
											<td>{{ number_format($cap->cajacierre[0]->dCajMonCie,2) }}</td>							
											<td>
												<a href="{{ route('caja.reportecajacierre',$cap->cCajApeCod) }}" class="btn btn-xs btn-info" target="_blank">
													AQC
												</a> 
											</td>
											<td>
												<a href="{{ route('caja.cajaoperaeditar',$cap->cCajApeCod) }}" class="btn btn-xs btn-primary">
													<i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>
												</a>										
											</td>
											<td>
												<a data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Delete" data-trans-title="¿Está seguro?" class="btn btn-xs btn-danger" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
													<i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
													
													<form action="{{ route('caja.operacion_destroy',$cap->cCajApeCod) }}" method="POST" name="delete_item" style="display:none">
													   {{ csrf_field() }}
														<input type="hidden" name="_method" value="DELETE">
													</form>
												</a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								{!! $cajaap->render() !!}
							</div>
						</div>
			        </div><!-- /.box-body -->
			    </div><!--box box-success-->
			</section>
		</div>
	</div>

@endsection

@push('scripts')

<script type="text/javascript">	
    $(document).bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
        	$(location).attr('href',"{{ route('almacen.create') }}");
        };
    });

    /* show / hide order details */
    $(".detalle").click(function() {
      $(this).closest("tr").next().toggle('fast');
      if($("#ver").attr("class") == 'fa fa-eye fa-1x')
        $("#ver").attr("class", "fa fa-eye-slash fa-1x");
      else
        $("#ver").attr("class", "fa fa-eye fa-1x");
    });

    /***********************combo anidado almacén1 a almacén2 ***********************/
    $("#tien").change(function(event){
   		$("#caja").empty();
   		//$('#caja').selectpicker('refresh');
        //$("#codpro").prop( "readonly", true );
   		$.get("tiendaare/"+event.target.value,function(response,alm_id){   			
   			$("#caja").append("<option value='' >Seleccione un área...</option>");   
   			for(i=0;i<response.length;i++){
   				$("#caja").append("<option value='"+response[i].nAreAlmCod+"' > "+response[i].cAreNom+"</option>");       	
   			}  
   		//$('#caja').selectpicker('refresh');     			
   		})
    });

    $("#caja").change(function(event){
   		$("#vend").empty();
   		//$('#caja').selectpicker('refresh');
        //$("#codpro").prop( "readonly", true );
   		$.get("cajavendedor/"+event.target.value,function(response,alm_id){   			
   			for(i=0;i<response.length;i++){
   				$("#vend").append("<option value='"+response[i].nRolAreCod+"' > "+response[i].name+"</option>");       	
   			}  
   		//$('#caja').selectpicker('refresh');     			
   		})
    });
    /*******************************************************************************/
</script>

@endpush('scripts')
