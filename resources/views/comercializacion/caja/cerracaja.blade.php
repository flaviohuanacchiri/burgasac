@extends('backend.layouts.appv2')

@section('subtitulo', 'CIERRE DE CAJA')

@section('content')


<link href="{{ asset('css/form.css') }}" rel="stylesheet">

<div class="row">
	<div class="col-sm-12">
		<section class="content">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title">@yield('subtitulo')</h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->
		        <div class="box-body">
		            @include('comercializacion.caja.fragment.error')
		             @include('comercializacion.caja.fragment.info')

		            <form action="{{ route('caja.cerrarcaja_store',$id) }}" method="POST" id="forcerrar">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="POST">
						<input type="hidden" name="codapertura" value="{{$cajacierre->cCajApeCod}}">
		                <div class="form-group">
		                	<div class="row">
			                	<div class="col-md-12">
			                		<label>Usuario Caja: {{$nameuser}}</label>
			                	</div>
			                </div>
			                <div class="row">
			                	<div class="col-md-3">
			                		<label>Tienda</label>
			                    	<input class="form-control" type="text" name="tie" id="tie" value="{{$almacen}}" readonly>
			                	</div>
			                	<div class="col-md-3">
			                		<label>Caja</label>
			                    	<input class="form-control" type="text" name="caj" id="caj" value="{{$area}}" readonly>	
			                	</div>
			                	<div class="col-md-3">
			                		<label>Fecha Ape.</label>
			                    	<input class="form-control" type="text" name="fec" id="fec" value="{{$cajacierre->cajaapertura->fCajFecApe}}" readonly>
			                	</div>
			                	<div class="col-md-3">
			                		<label>Monto Ape.</label>
			                    	<input class="form-control" type="text" name="mon" id="mon" value="{{$cajacierre->cajaapertura->dCajMonApe}}" readonly>	
			                	</div>			                	
			                </div>
			                <div class="row">
			                	<div class="col-md-3">
			                		<label>Fecha de Cierre</label>
			                    	<input class="form-control" type="text" name="fcie" id="fcie" value="{{$fecha}}" maxlength="255" readonly>
			                	</div>
			                	<div class="col-md-3">
			                		<label>Monto Total de Recaudo</label>
			                    	<input class="form-control" type="text" name="montttrec" id="montttrec" value="{{$cajacierre->cajaapertura->dCajApeMonTotVen}}" maxlength="17" readonly>
			                	</div>
			                	<div class="col-md-3 {{ $errors->has('moncie') ? 'has-error' :'' }}">
			                		<label>Monto de Cierre</label>
			                    	<input class="form-control" type="text" name="moncie" id="moncie" placeholder="0.00" value="{{$montodef}}" min="0.00" maxlength="17"  onkeypress="return tabular(event,this)" autofocus="autofocus">
			                    	{!! $errors->first('moncie','<span class="help-block">:message</span>') !!}
			                    	<p id="obs_vuel" name="obs_vuel" style="color: red;"></p>
			                	</div>
			                	<div class="col-md-3">
			                		<div class="row-fluid">
			                			<br>			                			
			                			<a class="btn btn-success" onclick="cerrar();">
											<i class="fa fa-print" data-toggle="tooltip" data-placement="top" title="Cerrar" data-original-title="Cerrar"></i> Cerrar
										</a> 
								    </div>
			                	</div>			                	
			                </div>
			            </div>
		                
		            </form>

    			</div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<section class="content" style="    min-height: 188px;">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title"></h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->
		        <div class="box-body">
		        	<div class="col-md-12" style="text-align: right;"> 
			        	<form action="{{ route('caja.cerrarcaja',$id) }}" method="GET" >
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="GET">
							<label>Fecha de Cierre</label>
							<input class="" type="date" name="fech" id="fech" step="1" min="2000-01-01" value="<?php echo date("Y-m-d");?>" tabindex="1">
				            <button type="submit" class="btn btn-success btn-xs" style="height: 26px;margin-left: -3px;margin-top: -4px;border-radius: 0px 3px 3px 0px;"><span class="fa fa-search" aria-hidden="true"></span></button>
			    		</form>       
			        </div>
			        <br>
			        <br>
			        
		        	<div class="row">
			            <div class="col-md-12"> 
				        	<table class="table table-striped table-bordered table-hover">
		                        <thead>
		                        	<th>Item</th> 
		                            <th>Fecha y hora de cierre</th>		
		                            <th>Código cierre</th>		
		                            <th>Fecha y hora de apertura</th>		
		                            <th>Monto de apertura</th>		
		                            <!--th>Elimnar</th-->		                                                    
		                        </thead>
			                    <tbody style="text-align: center;">
			                    		<?php  
			                    		$vari=(isset($_GET["page"]))?$_GET["page"]:1;
			                    		$cont=($vari-1)*5; 
			                    		?>
										@foreach($cajacierre2 as $ap)
										<tr>
											<td>{{++$cont}}</td>
											<td>{{ $ap->fCajFecCie }}</td>
											<td>{{ $ap->cCajCieCod }}</td>
											<td>{{ $ap->cajaapertura->fCajFecApe }}</td>	
											<td>{{ $ap->cajaapertura->dCajMonApe }}</td>	
											<!--td>
												<a data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Delete" data-trans-title="Are you sure?" class="btn btn-xs btn-danger" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
													<i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
													
													<form action="{{ route('area.destroy',$ap->cCajCieCod) }}" method="POST" name="delete_item" style="display:none">
													   {{ csrf_field() }}
														<input type="hidden" name="_method" value="DELETE">
												
													</form>
												</a>
											</td-->
										</tr>
										@endforeach
									</tbody>
								</table>
								{!! $cajacierre2->render() !!}
		                </div>
		        </div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">

	var key_error=false;

    function tabular(e,obj) {
     tecla=(document.all) ? e.keyCode : e.which;
     if(tecla!=13) return;
     frm=obj.form;
     for(i=0;i<frm.elements.length;i++)
       if(frm.elements[i]==obj) {
         if (i==frm.elements.length-1) i=-1;
         break }
     frm.elements[i+1].focus();
     return false;
    }

    if(!("autofocus" in document.createElement("input")))
       document.getElementById("uno").focus();

	$('#moncie').numeric(",").numeric({decimalPlaces: 2,negative: false});


	$("#moncie").on('keyup', function()
    {
        var value = $(this).val();           
       
        if(typeof(value) !== "undefined" && value!="")
        {
            if(parseFloat(value)<parseFloat({{$montodef}}))
            {            
                $(this).css("border","2px solid #f00");   
                $(this).focus();  
                $("#obs_vuel").html("¡El monto debe ser mayor o igual al: monto de recaudo + monto de apertura={{$montodef}}..!");  
                key_error=false;            
            }
            else
            {
                $(this).css("border","1px solid #d2d6de");
            	$("#obs_vuel").empty();
            	key_error=true; 
            }
        }
        //else          
        //    $("#subt_x").val("0.00"); 
        
    }).keyup();
    
    function cerrar()
    {   
        if(key_error)
            $("#forcerrar").submit();  
        else       
        {
        	$("#moncie").css("border","2px solid #f00");   
            $("#moncie").focus();  
            $("#obs_vuel").html("¡El monto debe ser mayor o igual al: monto de recaudo + monto de apertura={{$montodef}}..!");  
            key_error=false; 
        }
    }
</script>
@endpush