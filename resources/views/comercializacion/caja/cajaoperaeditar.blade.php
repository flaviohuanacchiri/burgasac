@extends('backend.layouts.appv2')

@section('title', 'EDITAR OPERACION DE CAJA: ')

@section('after-styles')
    <style>
    .dropdown{padding: 0;}
    .dropdown .dropdown-menu{border: 1px solid #999}
    .detallescompra{
        display: none;
        background-color: #ececec;
    }
    </style>
@stop

@section('content')

	<div class="row">
		<div class="col-sm-12">
			<section class="content">
			    <div class="box box-info">
			        <div class="box-header with-border">
			            <h3 class="box-title">@yield('title'){{$id}}</h3>
			            <div class="box-tools pull-right">
			                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			            </div><!-- /.box tools -->
			        </div><!-- /.box-header -->
			        <div class="box-body">
	                    <div class="row">
	                    	<div class="col-md-12"> 
								@include('comercializacion.caja.fragment.info')
								<form action="{{ route('caja.cajaoperaeditar_put',$cap->cCajApeCod) }}" method="POST" name="delete_item" >
								<table class="table table-hover table-striped">
									<thead>
										<tr>																							
											<th>Fecha de Apertura</th>									
											<th>Fecha de Cierre</th>
											<th>Tienda</th>									
											<th>Caja</th>									
											<th>Estado</th>
											<th>Vendedor</th>									
											<th>Monto de Apertura</th>									
											<th>Monto de Cierre</th>
											<th colspan="2">Acción</th>
										</tr>
									</thead>
									<tbody style="text-align: center;">
										<tr>											
											<td>{{ $cap->fCajFecApe }}</td>									
											<td>{{ $cap->cajacierre[0]->fCajFecCie }}</td>
											<td>{{ $cap->rolarea->areasalmacen->almacen->cAlmNom }}</td>									
											<td>{{ $cap->rolarea->areasalmacen->areas->cAreNom }}</td>
											<td>{{ ($cap->bCajCieAbierto==0)?'Abierto':'Cerrado' }}</td>							
											<td>{{ $cap->rolarea->user->name }}</td>							
											<td class="{{ $errors->has('monape') ? 'has-error' :'' }}">
												<input class="form-control" type="text" name="monape" id="monape" placeholder="0.00" value="{{ $cap->dCajMonApe }}" min="0.00" maxlength="17"  onkeypress="return tabular(event,this)" autofocus="autofocus" tabindex="1">
						                    	{!! $errors->first('monape','<span class="help-block">:message</span>') !!}
											</td>	
											<td class="{{ $errors->has('moncie') ? 'has-error' :'' }}">
												<input class="form-control" type="text" name="moncie" id="moncie" placeholder="0.00" value="{{ $cap->cajacierre[0]->dCajMonCie }}" min="0.00" maxlength="17"  onkeypress="return tabular(event,this)" tabindex="2">
						                    	{!! $errors->first('moncie','<span class="help-block">:message</span>') !!}
											</td>												
											<td>
												<a data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Guardar" data-trans-title="¿Está seguro?" class="btn btn-xs btn-success" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
													<i class="fa fa-floppy-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i> Guardar
													   {{ csrf_field() }}
														<input type="hidden" name="_method" value="PUT">
												</a>
											</td>
											<td>
												<a href="{{ route('caja.index') }}" class="btn btn-xs btn-danger">
													<i class="fa fa-reply" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cancelar"></i>
												</a>
											</td>
										</tr>										
									</tbody>
								</table>
								</form>
							</div>
						</div>
			        </div><!-- /.box-body -->
			    </div><!--box box-success-->
			</section>
		</div>
	</div>

@endsection

@push('scripts')

<script type="text/javascript">

	var key_error=false;

    function tabular(e,obj) {
     tecla=(document.all) ? e.keyCode : e.which;
     if(tecla!=13) return;
     frm=obj.form;
     for(i=0;i<frm.elements.length;i++)
       if(frm.elements[i]==obj) {
         if (i==frm.elements.length-1) i=-1;
         break }
     frm.elements[i+1].focus();
     return false;
    }

    if(!("autofocus" in document.createElement("input")))
       document.getElementById("uno").focus();

	$('#monape').numeric(",").numeric({decimalPlaces: 2,negative: false});
	$('#moncie').numeric(",").numeric({decimalPlaces: 2,negative: false});
</script>

@endpush('scripts')
