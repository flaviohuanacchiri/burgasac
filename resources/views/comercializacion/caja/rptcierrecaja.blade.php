<style type="text/css">
    html {
      margin: 0;
    }
    body {
      font-family: "Times New Roman", serif;
      margin: 15mm 15mm 2mm 15mm;
    }
    hr {
      page-break-after: always;
      border: 0;
      margin: 0;
      padding: 0;
    }
</style>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
    <title>AQC(Arqueo de Caja)</title>
<style>
table, td, th {
    border: 1px solid black;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th {
    text-align: center;
}
td {
    display: table-cell;
    vertical-align: inherit;
    font-size: 12px;
}
.alineados{
    float:left;
    width: 20%;
    font-size: 14px;
}
.alineados-tit{
    float:left;
    width: 20%;
    font-size: 14px;
    font-weight: bold;
}

</style>
</head>
<body>
<div>
    <div class="alineados-tit">TIENDA</div>
    <div class="alineados">{{($datos->rolarea->areasalmacen->almacen->cAlmNom)}}</div>    
</div>
<div style="clear:both"></div>
<div>
    <div class="alineados-tit">CAJA</div>
    <div class="alineados">{{$datos->rolarea->areasalmacen->areas->cAreNom}}</div>    
</div>
<div style="clear:both"></div>
<div>
    <div class="alineados-tit">FECHA DESDE</div>
    <div class="alineados">{{$datos->fCajFecApe}}</div>    
</div>
<div style="clear:both"></div>
<div>
    <div class="alineados-tit">FECHA HASTA</div>
    <div class="alineados">{{$datos->cajacierre[0]->fCajFecCie}}</div>    
</div>
<div style="clear:both"></div>
<div>
    <div class="alineados-tit">APERTURA</div>
    <div class="alineados">{{number_format($datos->dCajMonApe,2)}}</div>    
</div>
<div style="clear:both"></div>
<br>

<table>
  <tr>
    <th colspan="8">INGRESOS</th>
  </tr>
  <tr>
    <th colspan="2" style="font-size: 14px;">EFECTIVO</th>
    <th colspan="2" style="font-size: 14px;">DEPOSITO</th>
    <th colspan="2" style="font-size: 14px;">CHEQUE</th>
    <th colspan="2" style="font-size: 14px;">CRÉDITO</th>
  </tr>
  <tr>
    <td>GUIA DE VENTA</td>
    <td style="text-align: right;">S/. {{number_format($efec_guia,2)}}</td>
    <td>GUIA DE VENTA</td>
    <td style="text-align: right;">S/. {{number_format($dep_guia,2)}}</td>
    <td>GUIA DE VENTA</td>
    <td style="text-align: right;">S/. {{number_format($che_guia,2)}}</td>
    <td>GUIA DE VENTA</td>
    <td style="text-align: right;">S/. {{number_format($cre_guia,2)}}</td>
  </tr>
  <tr>
    <td>BOLETA</td>
    <td style="text-align: right;">S/. {{number_format($efec_bol,2)}}</td>
    <td>BOLETA</td>
    <td style="text-align: right;">S/. {{number_format($dep_bol,2)}}</td>
    <td>BOLETA</td>
    <td style="text-align: right;">S/. {{number_format($che_bol,2)}}</td>
    <td>BOLETA</td>
    <td style="text-align: right;">S/. {{number_format($cre_bol,2)}}</td>
  </tr>
  <tr>
    <td>FACTURA</td>
    <td style="text-align: right;">S/. {{number_format($efec_fac,2)}}</td>
    <td>FACTURA</td>
    <td style="text-align: right;">S/. {{number_format($dep_fac,2)}}</td>
    <td>FACTURA</td>
    <td style="text-align: right;">S/. {{number_format($che_fac,2)}}</td>
    <td>FACTURA</td>
    <td style="text-align: right;">S/. {{number_format($cre_fac,2)}}</td>
  </tr>
  <tr>
    <td>TOTAL</td>
    <td style="text-align: right;">S/. {{number_format($efec_guia+$efec_bol+$efec_fac,2)}}</td>
    <td>TOTAL</td>
    <td style="text-align: right;">S/. {{number_format($dep_guia+$dep_bol+$dep_fac,2)}}</td>
    <td>TOTAL</td>
    <td style="text-align: right;">S/. {{number_format($che_guia+$che_bol+$che_fac,2)}}</td>
    <td>TOTAL</td>
    <td style="text-align: right;">S/. {{number_format($cre_guia+$cre_bol+$cre_fac,2)}}</td>
  </tr>
  <tr>
    <td colspan="8"><br></td>
  </tr>
  <tr>
    <th colspan="8">EGRESO</th>
  </tr>
  <tr>
    <th colspan="2" style="font-size: 14px;">EFECTIVO</th>
    <th colspan="2" style="font-size: 14px;"></th>
    <th colspan="2" style="font-size: 14px;"></th>
    <th colspan="2" style="font-size: 14px;"></th>
  </tr>
  <tr>
    <td>GUIA DE VENTA</td>
    <td style="text-align: right;">S/. {{number_format($efec_guia_eg,2)}}</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>BOLETA</td>
    <td style="text-align: right;">S/. {{number_format($efec_bol_eg,2)}}</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>FACTURA</td>
    <td style="text-align: right;">S/. {{number_format($efec_fac_eg,2)}}</td>
    <!--td>S/. {{number_format($efec_fac*0.18,2)}}</td-->
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>TOTAL</td>
    <td style="text-align: right;">S/. {{number_format($efec_guia_eg+$efec_bol_eg+$efec_fac_eg,2)}}</td>
    <!--td>S/. {{number_format($efec_fac*0.18,2)}}</td-->
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
<br>
<div class="centrar" style="width: 550px;position: relative;left: 77px;">
    <table>
      <tr>
        <th colspan="4">RESUMEN</th>
      </tr>
      <tr>
        <th colspan="2" style="font-size: 14px;">INGRESOS</th>
        <th colspan="2" style="font-size: 14px;">EGRESOS</th>    
      </tr>
      <tr>
        <td>TOTAL EFECTIVO</td>
        <td style="text-align: right;">S/. {{number_format($efec_guia+$efec_bol+$efec_fac,2)}}</td>
        <td>TOTAL DEPOSITO</td>
        <td style="text-align: right;">S/. {{number_format($efec_guia_eg+$efec_bol_eg+$efec_fac_eg,2)}}</td>
        <!--td>S/. {{number_format($efec_fac*0.18,2)}}</td-->
      </tr>
      <tr>
        <td>TOTAL DEPOSITO</td>
        <td style="text-align: right;">S/. {{number_format($dep_guia+$dep_bol+$dep_fac,2)}}</td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>TOTAL CHEQUE</td>
        <td style="text-align: right;">S/. {{number_format($che_guia+$che_bol+$che_fac,2)}}</td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>TOTAL CRÉDITO</td>
        <td style="text-align: right;">S/. {{number_format($cre_guia+$cre_bol+$cre_fac,2)}}</td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td><br></td>
        <td><br></td>
        <td><br></td>
        <td><br></td>
      </tr>
      <tr>
        <td>TOTAL INGRESOS</td>
        <td style="text-align: right;">S/. {{number_format($efec_guia+$efec_bol+$efec_fac+$dep_guia+$dep_bol+$dep_fac+$che_guia+$che_bol+$che_fac+$cre_guia+$cre_bol+$cre_fac,2)}}</td>
        <td>TOTAL EGRESOS</td>
        <td style="text-align: right;">S/. {{number_format($efec_guia_eg+$efec_bol_eg+$efec_fac_eg,2)}}</td>
        <!--td>S/. {{number_format($efec_fac*0.18,2)}}</td-->
      </tr>
    </table>
</div>

<br>
<div class="centrar" style="width: 350px;position: relative;left: 170px;">
    <table>
      <tr>
        <th colspan="4">CAJA</th>
      </tr>
      <tr>
        <td colspan="2" style="font-size: 14px;">APERTURA</td>
        <td colspan="2" style="font-size: 14px;text-align: right;">S/. {{number_format($datos->dCajMonApe,2)}}</td>    
      </tr>
      <tr>
        <td colspan="2" style="font-size: 14px;">TOTAL EFECTIVO(INGRESO)</td>
        <td colspan="2" style="font-size: 14px;text-align: right;">S/. {{number_format($efec_guia+$efec_bol+$efec_fac+$dep_guia+$dep_bol+$dep_fac+$che_guia+$che_bol+$che_fac+$cre_guia+$cre_bol+$cre_fac,2)}}</td>    
      </tr>
      <tr>
        <td colspan="2" style="font-size: 14px;">TOTAL EFECTIVO(EGRESOS)</td>
        <td colspan="2" style="font-size: 14px;text-align: right;">S/. {{number_format($efec_guia_eg+$efec_bol_eg+$efec_fac_eg,2)}}</td>    
        <!--td colspan="2" style="font-size: 14px;">S/. {{number_format($efec_fac*0.18,2)}}</td-->    
      </tr>  
      <tr>
        <td colspan="2"><br></td>
        <td colspan="2"><br></td>
        
      </tr>
      <tr>
        <td colspan="2" style="font-size: 14px;">TOTAL CAJA</td>
        <td colspan="2" style="font-size: 14px;text-align: right;">S/. {{number_format($efec_guia+$efec_bol+$efec_fac+$dep_guia+$dep_bol+$dep_fac+$che_guia+$che_bol+$che_fac+$cre_guia+$cre_bol+$cre_fac+$datos->dCajMonApe-($efec_guia_eg+$efec_bol_eg+$efec_fac_eg),2)}}</td>    
        <!--td colspan="2" style="font-size: 14px;">S/. {{$efec_guia+$efec_bol+$efec_fac+$dep_guia+$dep_bol+$dep_fac+$che_guia+$che_bol+$che_fac+$cre_guia+$cre_bol+$cre_fac+$datos->dCajMonApe-$efec_fac*0.18}}</td-->
      </tr>
    </table>
</div>
</body>
</html>

<script type="text/php">
  if(isset($pdf))
    {
      $font = Font_Metrics::get_font("Arial", "bold");
      $pdf->page_text(765, 550, "Pagina {PAGE_NUM} de {PAGE_COUNT}", $font, 9, array(0, 0, 0));
    }
</script>