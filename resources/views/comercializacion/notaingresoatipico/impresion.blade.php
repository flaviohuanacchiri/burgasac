<title>Código Barras</title> 
<style type="text/css">
  
    body{
        width:1000px;
        /*margin-top: -40;      */  
    }  
    .conte{
        /*width: 892px;*/
        /*margin: 0;*/

    }
    .g{
        text-align: center;
        /*border-right: 1px solid #eeeeee;*/
        /*float: left;*/
        padding: 10px 0px 5px 15px;
        margin: 3px 3px 20px 3px;
        /*border: 1px solid #7d7d7d;*/
        /*border-radius: 13px;*/
        /*height: 165px;*/
        width:300px;
    }
    
    .noborder {
        border: 0 !important
    }
     
    #p01cb {
        font-family: Arial,sans-serif; 
        font-size: 8px;
        font-weight: bold;

    }
    
    #p02cb {
        font-family: Arial,sans-serif; 
        font-size: 14px;
        font-weight: bold;
    }
    
    #p03cb {
        font-family: Arial,sans-serif; 
        font-size: 12px;        
        
    }

   .contenedor-tabla
    {
        display: table;
        width: 100%;
        font-size: 13px;
        height: 50px;
        text-align: center;
    }

    .contenedor-fila
    {
        display: table-row;
        text-align: center;          
    }

    .contenedor-columna
    {
        display: table-cell;        
        text-align: center;
        /*padding-right: 15px;*/
    }

    .contenedor-tabla-p
    {
        display: table;
        width: 100%;
        text-align: center;
    }

    .contenedor-fila-p
    {
        display: table-row;
        text-align: center;          
    }

    .contenedor-columna-p
    {
        display: table-cell;        
        text-align: center;
        /*padding-right: 15px;*/
    }
   
    
</style>
    <?php 
        $cabecera=true;
        $pie=0;
        $cont_filas=0;
    ?>  
<body>
           
        @for ($i = 0; $i < sizeof($noimpresos); $i++)                 
           <div class="contenedor-tabla-p">
            <div class="contenedor-fila-p">                                
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>  
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div> 
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>

                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
                <div class="contenedor-columna-p" style="width: 5%;text-align: center;">
                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    <?php $i++; ?>
                    @if($i<sizeof($noimpresos))
                    <div class="g" >
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 15%;text-align: right;">
                                    <label id="p02cb" style="text-align: center;"><u> </u></label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p02cb"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>                                    
                                </div>                    
                            </div>
                            <div class="contenedor-fila">                                
                                 <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Artículo </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo4}}</label>                                    
                                 </div>                    
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Color </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo3}}</label>                                    
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 15%;text-align: left;">
                                    <label id="p03cb">Código </label>                                    
                                 </div>     
                                 <div class="contenedor-columna" style="width: 55%;text-align: left;">
                                    <label id="p03cb" style="margin-left: -15px;">: {{$noimpresos[$i]->campo2}}</label>                                                                        
                                 </div>                                
                            </div>
                        </div>
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                               
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla" style="margin-top: -15px;">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                 </div>          
                                <div class="contenedor-columna" style="width: 75%;text-align: center;">
                                    <?php echo DNS1D::getBarcodeHTML(str_replace("LOTE", "", (str_replace("-", "", $noimpresos[$i]->campo1))), "C128",1.50,40);?>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 10%;text-align: center;color:white;">
                                    dd
                                </div>          
                                <div class="contenedor-columna" style="width: 75%;padding-left: -60px;">
                                    <label id="p03cb">{{$noimpresos[$i]->campo1}}</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @else
                    <div class="g" >
                        
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">                                
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p02cb" style="text-align: center;color:white;"><u>{{env("APP_TITULO_IMPRESION", "")}}</u></label>
                                </div>
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: left;padding-top: 10px;">
                                    <label id="p03cb" style="color:white;">Art      : dddddddddddd</label>
                                 </div>                                
                            </div>
                        </div>

                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Cod: </label>
                                 </div>     
                                 <div class="contenedor-columna" style="width: 40%;text-align: left;">
                                    <label id="p03cb" style="color:white;">Color: </label>
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Principal</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">Sucursal</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_DOS", "")}}</label>
                                </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 30%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                                <div class="contenedor-columna" style="width: 40%;text-align: center;">
                                    <label id="p01cb" style="color:white;">{{env("APP_DIRECCION_TELEFONO", "")}}</label>
                                </div>                                
                            </div>
                        </div>
                       
                        <div class="contenedor-tabla">
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;color:white;">
                                    dddddddddddddddddddddddddddd
                                 </div>                                
                            </div>
                            <div class="contenedor-fila">
                                <div class="contenedor-columna" style="width: 100%;text-align: center;">
                                    <label id="p03cb" style="color:white;">ddddd</label>
                                 </div>                                
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>

            
               
            </div>
        </div>
        @endfor 
  
</body>     
