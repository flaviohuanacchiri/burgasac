@extends('backend.layouts.appv2')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">EDITAR TRANSFERENCIA: {{$id}}</div>
                <div class="panel-body">
                    <input type="hidden" name="codint" id="codint" value="{{$id}}">
                    <input type="hidden" name="conta" id="conta" value="">
                    <input type="hidden" name="eliminados" id="eliminados" value="">
                    <input type="hidden" name="actualizar" id="actualizar" value="">
                    <input type="hidden" name="cad_actt" id="cad_actt" value="">

                    <div class="panel panel-default">
                        <div class="panel-body">
                        	<div class="row">
    							<div class="col-md-6">
		                            <div class="row">
		                                <div class="col-md-12">
		                                    <label for="">Fecha</label>
		                                    <input id="fecha" type="text" class="form-control" name="fecha" value="{{ $tra->tTraFechReg }}" readonly="readonly">
		                                </div>
		                                <div class="col-md-12">
			                                <label for="">Almacén Actual</label>
			                                <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" id="alm1" name="alm1" tabindex="1" readonly="readonly">     
			                                    <option data-subtext="({{$tra->almacen1->cAlmUbi}})" value="{{$tra->almacen1->nAlmCod}}">{{$tra->almacen1->cAlmNom}}</option>
			                                    
			                                </select> 
			                            </div>

			                            <div class="col-md-12">
			                                <label for="">Almacén de Destino</label>
			                                <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" id="alm2" name="alm2" tabindex="2" readonly="readonly">  
			                                    <option data-subtext="({{$tra->almacen2->cAlmUbi}})" value="{{$tra->almacen2->nAlmCod}}">{{$tra->almacen2->cAlmNom}}</option>
			                                </select>
			                            </div>

		                                <div class="col-md-12">
		                                    <label for="">Cod. Transferencia</label>
		                                    <input id="ctrans" type="text" class="form-control" name="ctrans" readonly="readonly" value="{{$id}}">                                    
		                                </div>
		                            </div>
		                        </div>

		                        <div class="col-md-6">
		                        	<textarea class="form-control" rows="11" id="obs" name="obs" placeholder="OBSERVACION" maxlength="1000"  tabindex="3" readonly="readonly">{{ $tra->cTraObs }}</textarea>
		                        </div>
		                    </div>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <br>
                                    <div id="capa" style="position: absolute;width: 97%;height: 92%;background-color: rgba(19, 19, 19, 0.35);display: none;top: 1px;"></div>
                                    <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <th>
                                                Item
                                            </th>
                                            <th>
                                                Código
                                            </th>
                                            <th>
                                                Producto
                                            </th>
                                            <th>
                                                Color
                                            </th>
                                            <th>
                                                Peso
                                            </th>
                                            <th>
                                                Rollos
                                            </th>                                          
                                        </thead>
                                        <tbody style="text-align: center;">
                                            
                                            
                                        </tbody>
                                    </table>
                                   
                                    <ul class="pagination">
                                      
                                    </ul>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">                                    
                                    <a href="{{ route('transferencia.index')}}" class="btn btn-primary">Bandeja</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
<div id="loaderDiv" style="display:none;width:169px;height:189px;position:absolute;top:50%;left:50%;padding:2px;z-index: 2;"><img src='../../img/wait.gif' width="64" height="64" /><br>Loading..</div>
@endsection

@push('scripts')
<script type="text/javascript">
    var indice_tabla=0;
    var key_enter=true;
    var cant_items_real=0;
    var indice_act=1;
    var ultima_pag=1;
    var cantxpag=5;
    var tabla=[];

	$(document).ready(function() {
      $("form").keypress(function(e) {
            if (e.which == 13) {
                return false;
            }
        });

    var cont_item_bd=0;
    @foreach($tra_det as $det) 
        addtabla("{{ $det->nTraDetCod }}","{{ $det->cod_barras }}","{{ $det->nProAlmCod }}","{{ $det->productoalmacens->producto->nombre_generico}}","{{ $det->productoalmacens->color->nombre }}","{{ $det->productoalmacens->producto->nombre_especifico }}","{{ $det->dTraCant }}");
        cont_item_bd++;
    @endforeach
    
    /********************** PAGINACIÓN **************************/
    cant_items_real=cont_item_bd;    
    /*** cargar lista de p/ginas***/
    controlpaginacion(cantxpag,cant_items_real);        
    /*** cargar paginación inicial ***/
    paginacion(1,cantxpag);

    });

    function doscerosdecimal(num)
    {
        array=num.split('.');
        if(array.length>1)
        {
            if(array[1].length==1)
                num=num+"0";            
            else if(array[1].length==0)
                num=num+".00";  
        }
        else
            num=num+".00";  

        return num; 

    }    

    function addtabla(cod,cb,cproalm,prod,col,pesp,cant)
    {        

        var pes=0;
        var roll=0;

        if(pesp=="TELAS")
        {
            pes=cant;
            roll=0;
        }
        else
        {
            pes=0;
            roll=cant;   
        }

        indice_tabla++;
        $("#conta").val(indice_tabla);
        var nuevaFila="<tr id=fila_"+indice_tabla+">";
        nuevaFila+="<td>"+'<input type="hidden" name="cod_ndi_'+indice_tabla+'" id="cod_ndi_'+indice_tabla+'" value="'+cod+'">'+indice_tabla+"</td>";
        
        nuevaFila+="<td>"+'<input type="hidden" name="codb_'+indice_tabla+'" id="codb_'+indice_tabla+'" value="'+cb+'">'+cb+"</td>";

        nuevaFila+="<td>"+'<input type="hidden" name="prod_'+indice_tabla+'" id="prod_'+indice_tabla+'" value="'+cproalm+'"><p id="Lpes_'+indice_tabla+'">'+prod+"</p></td>";

        nuevaFila+="<td>"+col+"</td>";

        nuevaFila+="<td>"+'<input type="hidden" name="peso_'+indice_tabla+'" id="peso_'+indice_tabla+'" value="'+doscerosdecimal(pes)+'"><p id="Ltie_'+indice_tabla+'">'+doscerosdecimal(pes)+"</p></td>";

        nuevaFila+="<td>"+'<input type="hidden" name="rollo_'+indice_tabla+'" id="rollo_'+indice_tabla+'" value="'+roll+'"><p id="Lpar_'+indice_tabla+'">'+roll+"</p></td>";

        
        
        nuevaFila+="</tr>";
        $("#bandeja-transfer").append(nuevaFila);
        
        //------- para actualizar valores en el control ---
        //incdesstock(cproalm,((parseFloat(pes)!=0)?parseFloat(pes):parseFloat(roll)) ,"inc");

        return 1;            
    
    }

    function controlpaginacion(pag,total)
    {
        var cad='';
        var act=true;
        var i;
        for(i=1;i<=total/pag;i++)
        {

            if(act)
                cad='<li onclick="paginacion('+i+','+pag+')" style="cursor: pointer;"><span>«</span></li>';

            cad+='<li '+((act)?'class="active"':'')+' onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            act=false;
        }
        if(total%pag>0)
        {
            cad+='<li onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            cad+='<li onclick="paginacion('+(i)+','+pag+');" style="cursor: pointer;"><span>»</span></li>';
        }
        else
            cad+='<li onclick="paginacion('+(--i)+','+pag+')" style="cursor: pointer;"><span>»</span></li>';

        ultima_pag=i;

        $(".pagination").empty();
        $(".pagination").html(cad);
    }

    function paginacion(pag,de)
    {   
        // ocultar registros de 10 en 10 

        var fin=pag*de;;
        var ini=(pag-1)*de;


        //meter todo en un array, con indice ordenado                        
        var array_items=[];
        var contador=0;
        for (var i = 1; i <= parseInt($("#conta").val()); i++) 
        {                
            var nomfila="fila_"+i;
            if ( $("#"+nomfila).length > 0 ) 
            {
                array_items[contador]=nomfila;
                contador++;
            }                
        }
        //recorrer segun los limites mostrar lo necesario

        for(var i=0;i<contador;i++)
        {
            if(i>=ini && i<fin)
                $("#"+array_items[i]).show();        
            else
                $("#"+array_items[i]).hide();
        }

        indice_act=pag;//guarada el indice de pág

        for(i=1;i<=ultima_pag;i++)
            $("#pgitem_"+i).removeAttr('class');

        $("#pgitem_"+indice_act).attr('class','active');

        return 1;
    }
    /********************************* FIN: TABLA ***************************************/


</script>
@endpush('scripts')