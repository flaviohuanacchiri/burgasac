@extends('backend.layouts.appv2')

@section('after-styles')
    <style>
        .dropdown{padding: 0;}
        .dropdown .dropdown-menu{border: 1px solid #999}
        .detallescompra{
            display: none;
            background-color: #ececec;
        }
    </style>
@stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">BANDEJA DE TRANSFERENCIA ENTRE ALMACENES</div>
                <div class="panel-body">
                    <div class="row">                        
                        <form action="{{ route('transferencia.index') }}" method="GET">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="GET">

                            <div class="col-md-3">
                                <label for="">Fecha</label>
                                <input class="form-control" type="date" name="fech" id="fech" step="1" min="<?php echo date("Y-m-d");?>" value="<?php echo date("Y-m-d");?>"  tabindex="1">
                            </div>                            
                            <div class="col-md-3">
                                <label for="">Almacén Actual</label>
                                <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" id="alm1" name="alm1" tabindex="2">                                    
                                    @foreach($almacen as $alm)
                                        @if($request_->alm1==$alm->nAlmCod)
                                            <option data-subtext="({{$alm->cAlmUbi}})" value="{{$alm->nAlmCod}}" selected>{{$alm->cAlmNom}}</option>
                                        @else
                                            <option data-subtext="({{$alm->cAlmUbi}})" value="{{$alm->nAlmCod}}">{{$alm->cAlmNom}}</option>
                                        @endif
                                    @endforeach
                                </select> 
                            </div>

                            <div class="col-md-3">
                                <label for="">Almacén de Destino</label>
                                <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" id="alm2" name="alm2" tabindex="3">  
                                    @foreach($almacen as $alm)
                                        @if($request_->alm2==$alm->nAlmCod)
                                            <option data-subtext="({{$alm->cAlmUbi}})" value="{{$alm->nAlmCod}}" selected>{{$alm->cAlmNom}}</option>
                                        @else
                                            <option data-subtext="({{$alm->cAlmUbi}})" value="{{$alm->nAlmCod}}">{{$alm->cAlmNom}}</option>
                                        @endif
                                        
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="col-md-1" style="text-align:center;">
                                <br>
                                <button type="submit" class=" signbuttons btn btn-primary">Buscar</button>                            
                            </div>
                            <div class="col-md-2" style="text-align:center;">
                                <br>
                                <a href="{{ route('transferencia.create') }}" id="buscar-tabla" class="btn btn-primary">Crear</a>
                            </div>

                        </form>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <br>
                            <table id="bandeja-almacenes" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <th>
                                        Detalles
                                    </th>
                                    <th>
                                        Fecha
                                    </th>
                                    <th>
                                        Código Transferencia
                                    </th>
                                    <th>
                                        Almacén Actual
                                    </th>
                                    <th>
                                        Almacén Destino
                                    </th>                                   
                                    <th colspan="3">
                                        Opciones
                                    </th>
                                </thead>
                                <tbody style="text-align: center;">
                                @foreach($bandeja_tabla as $band)
                                    <tr>
                                        <th>
                                            <a class="btn btn-xs btn-success detalle">
                                                <i class="fa fa-eye fa-1x" id="ver" aria-hidden="true"></i>
                                            </a>
                                        </th>
                                        <td>
                                            {{ $band->tTraFechReg }}
                                        </td>
                                        <td>
                                            {{ $band->cTraCod }}
                                        </td>
                                        <td>
                                            {{ $band->almacen1->cAlmNom }}
                                        </td>
                                        <td>
                                            {{ $band->almacen2->cAlmNom }}
                                        </td>                                       
                                        <td>
                                            <a href="{{ route('transferencia.show',$band->cTraCod) }}" class="btn btn-xs btn-info">
                                                <i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver"></i>
                                            </a> | 
                                            <a href="{{ route('transferencia.edit',$band->cTraCod) }}" class="btn btn-xs btn-primary">
                                                <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar"></i>
                                            </a> |                                             
                                            <a data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Ok" data-trans-title="¿Está seguro?" class="btn btn-xs btn-danger" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
                                                <i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cerrar"></i>
                                                <form action="{{ route('transferencia.destroy',$band->cTraCod) }}" method="POST" name="delete_item" style="display:none">
                                                   {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="DELETE">   
                                                </form>
                                            </a>                                              
                                        </td>                                    
                                    </tr>
                                    <tr class="detallescompra">
                                        <td colspan="6">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <th>ITEM</th>
                                                    <th>CODIGO</th>
                                                    <th>PRODUCTO</th>
                                                    <th>COLOR</th>
                                                    <th>PESO</th> 
                                                    <th>ROLLO</th>                                                         
                                                </thead>
                                            <?php $conta=1; ?>
                                            @foreach($band->transferenciadetalle as $bdet)
                                                <tr colspan="6">                                                            
                                                    <td>{{ $conta }}</td>
                                                    <td>{{ $bdet->cod_barras }}</td>
                                                    <td>{{ $bdet->productoalmacens->producto->nombre_generico }}</td>
                                                    <td>{{ $bdet->productoalmacens->color->nombre }}</td>
                                                    @if($bdet->productoalmacens->producto->nombre_especifico == "TELAS")
                                                        <td>{{$bdet->dTraCant}}</td>
                                                        <td>{{number_format($bdet->dTraCant/20,2)}}
                                                        </td>
                                                    @else
                                                        <td>0.00</td>  
                                                        <td>{{ $bdet->dTraCant }}</td>                                           
                                                    @endif
                                                </tr>
                                            <?php $conta++; ?>
                                            @endforeach
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $bandeja_tabla->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

<script type="text/javascript"> 
    /* show / hide order details */
    $(".detalle").click(function() {
      $(this).closest("tr").next().toggle('fast');
      if($("#ver").attr("class") == 'fa fa-eye fa-1x')
        $("#ver").attr("class", "fa fa-eye-slash fa-1x");
      else
        $("#ver").attr("class", "fa fa-eye fa-1x");
    });

    function calcularrollo(num)
    {        
        var num_ceros=num/20;        
        return num_ceros.toFixed(2);
    }  
    
</script>

@endpush('scripts')