@extends('backend.layouts.appv2')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">EDITAR TRANSFERENCIA: {{$id}}</div>
                <div class="panel-body">

                <form action="{{ route('transferencia.update',$id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="codint" id="codint" value="{{$id}}">
                    <input type="hidden" name="conta" id="conta" value="">
                    <input type="hidden" name="eliminados" id="eliminados" value="">
                    <input type="hidden" name="actualizar" id="actualizar" value="">
                    <input type="hidden" name="cad_actt" id="cad_actt" value="">
                     <input type="hidden" name="prod_agredet" id="prod_agredet" value="">

                    <div class="panel panel-default">
                        <div class="panel-body">
                        	<div class="row">
    							<div class="col-md-6">
		                            <div class="row">
		                                <div class="col-md-12">
		                                    <label for="">Fecha</label>
		                                    <input id="fecha" type="text" class="form-control" name="fecha" value="{{ $tra->tTraFechReg }}" readonly>
		                                </div>
		                                <div class="col-md-12">
			                                <label for="">Almacén Actual</label>
			                                <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" id="alm1" name="alm1" tabindex="1">     
			                                    <option data-subtext="({{$tra->almacen1->cAlmUbi}})" value="{{$tra->almacen1->nAlmCod}}">{{$tra->almacen1->cAlmNom}}</option>
			                                    
			                                </select> 
			                            </div>

			                            <div class="col-md-12">
			                                <label for="">Almacén de Destino</label>
			                                <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" id="alm2" name="alm2" tabindex="2">  
			                                    <option data-subtext="({{$tra->almacen2->cAlmUbi}})" value="{{$tra->almacen2->nAlmCod}}">{{$tra->almacen2->cAlmNom}}</option>
			                                </select>
			                            </div>

		                                <div class="col-md-12">
		                                    <label for="">Cod. Transferencia</label>
		                                    <input id="ctrans" type="text" class="form-control" name="ctrans" readonly value="{{$id}}">                                    
		                                </div>
		                            </div>
		                        </div>

		                        <div class="col-md-6">
		                        	<textarea class="form-control" rows="11" id="obs" name="obs" placeholder="OBSERVACION" maxlength="1000"  tabindex="3" readonly="readonly">{{ $tra->cTraObs }}</textarea>
		                        </div>
		                    </div>

                        </div>
                    </div>

                    <label id="rpt" style="color:red;"></label>

                    <div class="panel panel-default">
                        <div class="panel-body" id="dina_control" name="dina_control">
                            <div class="row">                                
                                <div class="col-md-4">
                                    @if($manauto==0)        
                                        <input type="radio" id="radaut" name="codigo" value="0" checked="checked" disabled>
                                    @else
                                        <input type="radio" id="radaut" name="codigo" value="0" disabled>
                                    @endif
                                    <label for="contactChoice1">Aut.</label>

                                    @if($manauto==1)        
                                        <input type="radio" id="radman" name="codigo" value="1" checked="checked" disabled>
                                    @else
                                        <input type="radio" id="radman" name="codigo" value="1" disabled>
                                    @endif
                                    
                                    <label for="contactChoice2">Man.</label>
                                    <input id="codpro" type="text" class="form-control" name="codpro" placeholder="Ingrese código" maxlength="50" tabindex="4">
                                </div>
                                <div class="col-md-8">
                                    <label for="">Producto</label>                            
                                    <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" id="prod" name="prod" tabindex="5">  
                                        <option data-subtext="" value="">Seleccione Producto...</option>
    	                                @foreach($productos as $prod_acep)
                                            <option data-subtext='{{$prod_acep->nombre}}' dato-esp='{{$prod_acep->nombre_especifico}}' value='{{$prod_acep->nProAlmCod}}'>{{$prod_acep->nombre_generico}}</option>
                                        @endforeach 
	                                </select>
                                </div>
                            </div>
                            <div class="row">                                                                
                            	<div class="col-md-3">
                                    <label for="">Stock</label>
                                    <input id="stock" type="text" class="form-control" name="stock" maxlength="6" value="0.00" readonly="readonly">
                                </div>
                                <div class="col-md-3">
                                    <label for="">Peso</label>
                                    <input id="pes" type="text" class="form-control" name="pes" placeholder="00.00" maxlength="17" tabindex="6" value="0.00" readonly="readonly">
                                </div>
                                <div class="col-md-3">
                                    <label for="">Rollo</label>
                                    <input id="roll" type="text" class="form-control" name="roll" placeholder="0" maxlength="11" tabindex="7" value="0" readonly="readonly">
                                </div>                               
                                <div class="col-md-3" style="text-align:center;">
                                	<br>
                                	<div  id="add" class="btn btn-primary" tabindex="8">agregar</div>
                            	</div>
                            </div>
                        </div>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <br>
                                    <div id="capa" style="position: absolute;width: 97%;height: 92%;background-color: rgba(19, 19, 19, 0.35);display: none;top: 1px;"></div>
                                    <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <th>
                                                Item
                                            </th>
                                            <th>
                                                Código
                                            </th>
                                            <th>
                                                Producto
                                            </th>
                                            <th>
                                                Color
                                            </th>
                                            <th>
                                                Peso
                                            </th>
                                            <th>
                                                Rollos
                                            </th>
                                            <th>
                                                Eliminar
                                            </th>
                                        </thead>
                                        <tbody style="text-align: center;">
                                            
                                            
                                        </tbody>
                                    </table>
                                   
                                    <ul class="pagination">
                                      
                                    </ul>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-success">Actualizar Transferencia</button>
                                    <a href="{{ route('transferencia.index')}}" class="btn btn-primary">Bandeja</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
<div id="loaderDiv" style="display:none;width:169px;height:189px;position:absolute;top:50%;left:50%;padding:2px;z-index: 2;"><img src='../../../img/wait.gif' width="64" height="64" /><br>Loading..</div>
@endsection

@push('scripts')
<script type="text/javascript">
    var indice_tabla=0;
    var key_enter=true;
    var cant_items_real=0;
    var indice_act=1;
    var ultima_pag=1;
    var cantxpag=5;
    var tabla=[];
    var codbarra_active=0;
	$(document).ready(function() {
      $("form").keypress(function(e) {
            if (e.which == 13) {
                return false;
            }
        });

    $('#roll').keyup(function (){
      this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    $('#roll').numeric(false).numeric({negative: false});

    $('#pes').numeric(",").numeric({decimalPlaces: 2,negative: false});

    var cont_item_bd=0;
    @foreach($tra_det as $det) 
        addtabla("{{ $det->nTraDetCod }}","{{ $det->cod_barras }}","{{ $det->nProAlmCod }}","{{ $det->productoalmacens->producto->nombre_generico}}","{{ $det->productoalmacens->color->nombre }}","{{ $det->productoalmacens->producto->nombre_especifico }}","{{ $det->dTraCant }}");
        cont_item_bd++;
    @endforeach
    
    /********************** PAGINACIÓN **************************/
    cant_items_real=cont_item_bd;    
    /*** cargar lista de p/ginas***/
    controlpaginacion(cantxpag,cant_items_real);        
    /*** cargar paginación inicial ***/
    paginacion(1,cantxpag);

    });

    /***********************combo anidado almacén producto ***********************/
    $("#alm2").change(function(event){
   		$("#prod").empty();
   		$('#prod').selectpicker('refresh');
        $("#codpro").prop( "readonly", true );
        
   		$.get("../productoalm/"+$("#alm1").val()+"/"+event.target.value,function(response,alm_id){
   			$("#prod").append("<option data-subtext='' value='0' >Seleccione producto...</option>");  
   			for(i=0;i<response.length;i++){
   				$("#prod").append("<option data-subtext='"+response[i].nombre+"' dato-esp='"+response[i].nombre_especifico+"' value='"+response[i].nProAlmCod+"' > "+response[i].nombre_generico+"</option>");       	
   			}  
        $("#codpro").prop( "readonly", false );
        $("#pes").val("0.00");
        $("#pes").prop( "readonly", true );
        $("#roll").val("0");
        $("#roll").prop( "readonly", true );
        $("#stock").val("0");
        $("#codpro").focus();  
        $('#prod').selectpicker('refresh');       			
   		})
    });
    /*******************************************************************************/

    /***********************combo anidado almacén1 a almacén2 ***********************/
    /*$("#alm1").change(function(event){
   		$("#alm2").empty();
   		$('#alm2').selectpicker('refresh');
        $("#codpro").prop( "readonly", true );
   		$.get("almacenalm/"+event.target.value+"",function(response,alm_id){
   			$("#alm2").append("<option data-subtext='' value='' >Selecciona Alm. destino</option>");           
   			for(i=0;i<response.length;i++){
   				$("#alm2").append("<option data-subtext='("+response[i].cAlmUbi+")' value='"+response[i].nAlmCod+"' > "+response[i].cAlmNom+"</option>");       	
   			}  

   		$('#alm2').selectpicker('refresh');     			
   		})
    });*/
    /*******************************************************************************/
	/************************** sacando stock del combo productos ******************/
    $('#prod').change(function(){
        
        codbarra_active=0;
        var valor = $(this).val();
        $("#stock").val("0"); 
        if(valor=="0")
        {
            $("#codpro").prop( "readonly", false );    
            limpiarcontroles();

        }
        else
            $("#codpro").prop( "readonly", true );    
        

        $.get("../almacenProStock/"+valor+"",function(response,pstock)
        {            
            //alert(valor);
            //alert(tabla[valor]);
            $("#stock").val(((typeof(tabla[valor]) === "undefined")?response.nProdAlmStock:response.nProdAlmStock-tabla[valor]).toFixed(2) );
            //$("#stock").val(response.nProdAlmStock);
            $("#codpro").val("");
            if(response.nombre_especifico=="TELAS")
            {   
                if(response.nProdAlmStock>0)             
                    $("#pes").prop( "readonly", false );
                else
                    $("#pes").prop( "readonly", true );

                $("#roll").prop( "readonly", true );
                $("#roll").val("0");
                $("#pes").val("");
                $("#pes").focus();
            }
            else
            {
                if(response.nProdAlmStock>0)             
                    $("#roll").prop( "readonly", false );
                else
                    $("#roll").prop( "readonly", true );

                $("#pes").prop( "readonly", true );
                $("#pes").val("0.00");
                $("#roll").val("");
                $("#roll").focus();
            }

            
            
        })
    });
    /************************** fin: sacando stock del combo productos ******************/

    /************************** obtener producto con código de barras *******************/
	$("#codpro").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            //codpro_xx=$.trim($("#codpro").val());
            codpro_xx=$.trim($("#codpro").val()).split("'").join("-");

            if(codpro_xx!="")
            {
    			$.ajax({
    			  type: "GET",
    			  url: "../veri_codbarra/"+codpro_xx+"/"+$("#alm1").val(),
    			  //data: "5454",
    			  dataType: "json",
    			  error: function(){
    			    alert("error petición ajax");
    			    //agredardetalle();
    			  },
                  beforeSend: function(){
                       $("#loaderDiv").show();
                   },
    			  success: function(data){     
    			    if(data!="")//data[0].ning_id
    			    {   
    			    	//prod 
                        codbarra_active=1;
                        var key=false;   
                        $("#prod option").each(function(){
                           //alert('opcion '+$(this).text()+' valor '+ $(this).attr('value'))
                           if(data[0].nProAlmCod==$(this).attr('value'))
                                key=true;
                        });

                       if(key)
                       {
                            $("#prod option[value="+ data[0].nProAlmCod +"]").attr("selected",true);
                            $("#stock").val(data[0].nProdAlmStock);
                            //evaluar si es pero o rollo y boquearlo
                            if(data[0].nombre_especifico =="TELAS")
                            {
                                $("#pes").val(data[0].peso_cant);
                                $("#roll").val("0");
                                //$("#pes").prop( "readonly", false );
                                $("#pes").prop( "readonly", true );
                                $("#roll").prop( "readonly", true );
                            }
                            else
                            {
                                $("#pes").val("0");
                                $("#roll").val(data[0].rollo);
                                //$("#roll").prop( "readonly", false );
                                $("#roll").prop( "readonly", true );
                                $("#pes").prop( "readonly", true );
                            }    
                            $("#pes").focus();
                         
                            @if($manauto==0) 
                                agredardetalle();
                            @endif
                       }
                       else
                       {
                        var cad="";
                            if(data[0].nAlmCod==$("#alm1 option:selected").val())
                            {
                                cad="El código de barras: "+codpro_xx+", si existe.\n- Propducto: "+data[0].nombre_generico+"("+data[0].nombre_especifico+")\n- Color: "+data[0].nombre+"\n- Almacén: "+data[0].cAlmNom+"("+data[0].cAlmUbi+")"+" | stock: "+data[0].nProdAlmStock+"\n- ¡Adv.! El Almacén Actual("+ $("#alm2 option:selected").text()+") no soporta este producto/color, por eso no podemos traer la información.";
                            }
                            else
                            {
                                cad="El código de barras: "+codpro_xx+", si existe.\n- Propducto: "+data[0].nombre_generico+"("+data[0].nombre_especifico+")\n- Color: "+data[0].nombre+"\n- Almacén: "+data[0].cAlmNom+"("+data[0].cAlmUbi+")"+" | stock: "+data[0].nProdAlmStock+"\n- ¡Adv.! El almacén de Destino("+ $("#alm1 option:selected").text()+") no tiene registrado este producto/color, por eso no podemos traer la información.";
                            }
                        $("#rpt").html(cad);                          
                       } 
    			    	//alert(data[0].nProAlmCod);
                        //alert(data[0].nAlmCod);//debe coincidir copn el almacen 1
    			    	
    			    	$('#prod').selectpicker('refresh');
    			       
    			    }
    			    else
    			    {
    			        $("#rpt").html("Este código de barras ("+codpro_xx+"), no está registrado en el almacén actual.");   			        
    			    }
                    $("#loaderDiv").hide();
    			  }
    			});
            }
        };
    });
    /************************** FIN: obtener producto con código de barras *******************/

    $("#pes").bind('keydown',function(e){
        if ( e.which == 13 ) 
            agredardetalle();        
    });

    $("#roll").bind('keydown',function(e){
        if ( e.which == 13 ) 
            agredardetalle();        
    });

    /********************************** INICIO: MANEJO DE TABLA *************************/

    $("#add").click(function(){                         
        agredardetalle();    
    });

    function agredardetalle()
    {
        if(validacion()==0)
        {
            // Obtenemos el total de columnas (tr) del id "tabla"            
            indice_tabla++;
            $("#conta").val(indice_tabla);
            var nuevaFila="<tr id=fila_"+indice_tabla+">";
            nuevaFila+="<td>"+'<input type="hidden" name="cod_ndi_'+indice_tabla+'" id="cod_ndi_'+indice_tabla+'" value="0">'+'<i class="fa fa-hand-o-right" aria-hidden="true"></i> '+indice_tabla+"</td>";
            
            nuevaFila+="<td>"+'<input type="hidden" name="codb_'+indice_tabla+'" id="codb_'+indice_tabla+'" value="'+$("#codpro").val()+'">'+$("#codpro").val()+"</td>";
            nuevaFila+="<td>"+'<input type="hidden" name="prod_'+indice_tabla+'" id="prod_'+indice_tabla+'" value="'+$("#prod").val()+'"><p id="Lpes_'+indice_tabla+'">'+$("#prod option:selected").html()+"</p></td>";

            nuevaFila+="<td>"+$("#prod option:selected").attr("data-subtext")+"</td>";

            nuevaFila+="<td>"+'<input type="hidden" name="peso_'+indice_tabla+'" id="peso_'+indice_tabla+'" value="'+doscerosdecimal($("#pes").val())+'"><p id="Ltie_'+indice_tabla+'">'+doscerosdecimal($("#pes").val())+"</p></td>";

            nuevaFila+="<td>"+'<input type="hidden" name="rollo_'+indice_tabla+'" id="rollo_'+indice_tabla+'" value="'+$("#roll").val()+'"><p id="Lpar_'+indice_tabla+'">'+ (($("#prod option:selected").attr("dato-esp")=="TELAS")?calcularrollo(parseFloat($("#pes").val())):$("#roll").val()) +"</p></td>";
            
            nuevaFila+='<td><div class="btn btn-link" onclick="delTabla('+indice_tabla+')" >'+'<a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quitar"></i></a>'+'</div>';
            
            nuevaFila+="</tr>";
            $("#bandeja-transfer").append(nuevaFila);
            
            //------- para actualizar valores en el control ---
            incdesstock($("#prod").val(),((parseFloat($("#pes").val())!=0)?parseFloat($("#pes").val()):parseFloat($("#roll").val())) ,"inc");
            $("#prod_agredet").val($("#codb_"+indice_tabla).val()+","+$("#prod_agredet").val());
            limpiarcontroles();   
                   
            //fin-----------------------------------------------

            // paginación   
            cant_items_real++;            
            // cargar lista de p/ginas
            controlpaginacion(cantxpag,cant_items_real);        
            // cargar paginación inicial            
            paginacion(ultima_pag,cantxpag);
            
        }
    }

    function incdesstock(indice,valor,inc_dec)
    {
        
        if(typeof(tabla[indice]) === "undefined"){//si no existe agregar
            if(inc_dec=="inc")
                tabla[indice]=valor;
            else
                tabla[indice]=-valor;
        }
        else//si existe (incrementar/decrementar)
        {
            if(inc_dec=="inc")
                tabla[indice]+=valor;
            else
                tabla[indice]-=valor;
        }        
    }

    function limpiarcontroles()
    {
        $("#codpro").val("");

        $("#codpro").prop( "readonly", false );   
        $("#prod option[value=0]").attr("selected",true); 
        //$('#prod').selectpicker('refresh');
        $('#alm2').change();
        $("#pes").val("0.00");
        $("#pes").prop( "readonly", true );
        $("#roll").val("0");
        $("#roll").prop( "readonly", true );
        $("#stock").val("0");
        $("#codpro").focus();  
        $("#rpt").empty();

    }

    function doscerosdecimal(num)
    {
        array=num.split('.');
        if(array.length>1)
        {
            if(array[1].length==1)
                num=num+"0";            
            else if(array[1].length==0)
                num=num+".00";  
        }
        else
            num=num+".00";  

        return num; 

    }  

    function calcularrollo(num)
    {        
        var num_ceros=num/20;        
        return num_ceros.toFixed(2);
    }  

    function addtabla(cod,cb,cproalm,prod,col,pesp,cant)
    {        

        var pes=0;
        var roll=0;
        var roll_cal=0;

        if(pesp=="TELAS")
        {
            pes=cant;
            roll=0;
            roll_cal=calcularrollo(parseFloat(cant));
        }
        else
        {
            pes=0;
            roll=cant;   
            roll_cal=cant;
        }

        indice_tabla++;
        $("#conta").val(indice_tabla);
        var nuevaFila="<tr id=fila_"+indice_tabla+">";
        nuevaFila+="<td>"+'<input type="hidden" name="cod_ndi_'+indice_tabla+'" id="cod_ndi_'+indice_tabla+'" value="'+cod+'">'+indice_tabla+"</td>";
        
        nuevaFila+="<td>"+'<input type="hidden" name="codb_'+indice_tabla+'" id="codb_'+indice_tabla+'" value="'+cb+'">'+cb+"</td>";

        nuevaFila+="<td>"+'<input type="hidden" name="prod_'+indice_tabla+'" id="prod_'+indice_tabla+'" value="'+cproalm+'"><p id="Lpes_'+indice_tabla+'">'+prod+"</p></td>";

        nuevaFila+="<td>"+col+"</td>";

        nuevaFila+="<td>"+'<input type="hidden" name="peso_'+indice_tabla+'" id="peso_'+indice_tabla+'" value="'+doscerosdecimal(pes)+'"><p id="Ltie_'+indice_tabla+'">'+doscerosdecimal(pes)+"</p></td>";

        nuevaFila+="<td>"+'<input type="hidden" name="rollo_'+indice_tabla+'" id="rollo_'+indice_tabla+'" value="'+roll+'"><p id="Lpar_'+indice_tabla+'">'+roll_cal+"</p></td>";
        
        nuevaFila+='<td>Transferido...</div>';
        
        //nuevaFila+='<td><div class="btn btn-link" onclick="delTabla('+indice_tabla+')" >'+'<a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quitar"></i></a>'+'</div>';
        
        nuevaFila+="</tr>";
        $("#bandeja-transfer").append(nuevaFila);

        $("#prod_agredet").val($("#codb_"+indice_tabla).val()+","+$("#prod_agredet").val());
        
        //------- para actualizar valores en el control ---
        //incdesstock(cproalm,((parseFloat(pes)!=0)?parseFloat(pes):parseFloat(roll)) ,"inc");

        return 1;            
    
    }

    function delTabla(id)
    {     
        
        if(confirm('Nota: El registro se retirará momentaneamente, pero el cambio no se hará efecto hasta que usted guarde los cambios.'))
        {
            var datos= $("#eliminados").val()+","+$("#cod_ndi_"+id).val();
            $("#eliminados").val(datos);

            //------- para actualizar valores en el control ---
            incdesstock($("#prod_"+id).val(),((parseFloat($("#peso_"+id).val())!=0)?parseFloat($("#peso_"+id).val()):parseFloat($("#rollo_"+id).val())) ,"des");
            limpiarcontroles();
            // fin controles-----------------------------------
            var cb_escgdo=$.trim($("#codb_"+id).val());
            if(cb_escgdo!="")
            {
                var cadena_tra=quitar($("#prod_agredet").val(),cb_escgdo);
                $("#prod_agredet").val(cadena_tra);    
            }

            $("#fila_"+id).remove();            

            cant_items_real--;
            // cargar lista de p/ginas
            controlpaginacion(cantxpag,cant_items_real);        
            // cargar paginación inicial 
            paginacion(indice_act,cantxpag);
        }            
    }

/*  function editar(id)
    {
        $("#dina_control").css( "box-shadow","rgba(0, 0, 0, 0.75) 0px 4px 47px -5px"); 
        
        $('#tienda > option[value="'+$("#tie_"+id).val()+'"]').attr('selected', 'selected');
        $("#partida").val($("#par_"+id).val());
        $("#peso").val($("#pes_"+id).val());
        $("#rollo").val($("#roll_"+id).val());

        $("#partida").attr('disabled','disabled');
        $("#cdel_"+id).hide();
        $("#cedi_"+id).hide();

        $("#actualizar").val(id);

        $("#act").show();
        $("#add").hide();   

        $( "#peso" ).focus();
        $("#capa").show();
        key_enter=false;
    }

    function actualiza()
    {
        if(validacion()==0)
        {
            var id=$("#actualizar").val();
            
            $("#tie_"+id).val($("#tienda").val());
            $("#par_"+id).val($("#partida").val());
            $("#pes_"+id).val($("#peso").val());
            $("#roll_"+id).val($("#rollo").val());            
            
            $("#Ltie_"+id).text($("#tienda option:selected").html());
            $("#Lpar_"+id).text($("#partida").val());
            $("#Lpes_"+id).text($("#peso").val());
            $("#Lroll_"+id).text($("#rollo").val());

            $("#partida").removeAttr('disabled');
            $("#cdel_"+id).show();
            $("#cedi_"+id).show();

            $("#dina_control").css( "box-shadow","none"); 

            //$("#tienda").val("1");
            $("#partida").val("");
            $("#peso").val("");
            $("#rollo").val("");

            $("#act").hide();
            $("#add").show();

            //verificar si hay  cambios 
            if($("#tie_"+id).val()!=$("#tienda").val() || $("#pes_"+id).val()!=$("#peso").val() || $("#roll_"+id).val()!=$("#rollo").val())
            {                
                var datos= $("#cad_actt").val()+","+$("#cod_ndi_"+id).val();
                $("#cad_actt").val(datos);
            }

            $( "#partida" ).focus();
            $("#capa").hide();
            key_enter=true;
            //si hay agregar al input .... el código
        }

    }
    function cancelar()
    {
        var id=$("#actualizar").val();
        $("#partida").removeAttr('disabled');
        $("#cdel_"+id).show();
        $("#cedi_"+id).show();

        $("#dina_control").css( "box-shadow","none"); 

        //$("#tienda").val("");
        $("#partida").val("");
        $("#peso").val("");
        $("#rollo").val("");

        $("#act").hide();
        $("#add").show();

        $( "#partida" ).focus();
        $("#capa").hide();
        key_enter=true;
    }

     function anular(e) {
          tecla = (document.all) ? e.keyCode : e.which;
          return (tecla != 13);
     }
*/
    
    function quitar(cadena,elemento)
    {
        var array_auxi = cadena.split(",");    
        var cade="";
        for (var i = 0; i <array_auxi.length; i++) 
        {
            /*alert("->"+elemento+"="+array_auxi[i]);
            alert(elemento==array_auxi[i]); */
            if(elemento!=$.trim(array_auxi[i]))
                cade+=array_auxi[i]+",";
        }    
        //alert(cade);
        return cade;
    }

    function validacion()
    {
        var stock_=parseFloat($.trim($("#stock").val()));
        var pes_=parseFloat($.trim($("#pes").val()));
        var rol_=parseFloat($.trim($("#roll").val()));
        var key_error=false;
        
        if(stock_<=0)
        {
            $("#stock").css("border","2px solid #f00");                
            key_error=true;
        }
        else
            $("#stock").css("border","1px solid #d2d6de");

        if(pes_<0 || pes_>stock_ || isNaN(pes_))
        {
            alert("No tiene suficiente stock");
            $("#pes").css("border","2px solid #f00");   
            $("#stock").focus();             
            key_error=true;
        }
        else
            $("#pes").css("border","1px solid #d2d6de");

        if(rol_<0 || rol_>stock_ || isNaN(rol_))
        {
            alert("No tiene suficiente stock");
            $("#roll").css("border","2px solid #f00");
            $("#stock").focus();             
            key_error=true;
        }
        else
            $("#roll").css("border","1px solid #d2d6de");

        if( codbarra_active==1)
        {
            var arracodb = ($("#prod_agredet").val()).split(",");
            if(buscar_array(arracodb,$("#codpro").val()))
            {
                limpiarcontroles();
                $("#codpro").css("border","2px solid #f00");  
                $("#rpt").html("Error: "+$("#codpro").val()+" no se puede agregar porque anteriormente ya se agrego este código.");
                key_error=true;
            }
            else
            {
                $("#rpt").empty();
                $("#codpro").css("border","1px solid #d2d6de");            
            }    
        }    

        return (key_error)?1:0;
    }


    function buscar_array(array,buscar)
    {
        for (var i = 0; i <array.length; i++) 
        {
            if(buscar==array[i])
                return true;
        }
        return false;
    }

    function controlpaginacion(pag,total)
    {
        var cad='';
        var act=true;
        var i;
        for(i=1;i<=total/pag;i++)
        {

            if(act)
                cad='<li onclick="paginacion('+i+','+pag+')" style="cursor: pointer;"><span>«</span></li>';

            cad+='<li '+((act)?'class="active"':'')+' onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            act=false;
        }
        if(total%pag>0)
        {
            cad+='<li onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            cad+='<li onclick="paginacion('+(i)+','+pag+');" style="cursor: pointer;"><span>»</span></li>';
        }
        else
            cad+='<li onclick="paginacion('+(--i)+','+pag+')" style="cursor: pointer;"><span>»</span></li>';

        ultima_pag=i;

        $(".pagination").empty();
        $(".pagination").html(cad);
    }

    function paginacion(pag,de)
    {   
        // ocultar registros de 10 en 10 

        var fin=pag*de;;
        var ini=(pag-1)*de;


        //meter todo en un array, con indice ordenado                        
        var array_items=[];
        var contador=0;
        for (var i = 1; i <= parseInt($("#conta").val()); i++) 
        {                
            var nomfila="fila_"+i;
            if ( $("#"+nomfila).length > 0 ) 
            {
                array_items[contador]=nomfila;
                contador++;
            }                
        }
        //recorrer segun los limites mostrar lo necesario

        for(var i=0;i<contador;i++)
        {
            if(i>=ini && i<fin)
                $("#"+array_items[i]).show();        
            else
                $("#"+array_items[i]).hide();
        }

        indice_act=pag;//guarada el indice de pág

        for(i=1;i<=ultima_pag;i++)
            $("#pgitem_"+i).removeAttr('class');

        $("#pgitem_"+indice_act).attr('class','active');

        return 1;
    }
    /********************************* FIN: TABLA ***************************************/


</script>
@endpush('scripts')