@extends('backend.layouts.appv2')

@section('subtitulo', 'COMPRA DE TELA TEÑIDA')

@section('content')


<link href="{{ asset('css/form.css') }}" rel="stylesheet">

<div class="row"  style="line-height: 2;">
	<div class="col-sm-12">
		<section class="content">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title">@yield('subtitulo')</h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->
		        <div class="box-body">
		            @include('comercializacion.compraTT.fragment.info')

		            <form action="{{ route('compraTT.update',$comtel->nCodComTel) }}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="PUT">
	                    <input type="hidden" name="id" id="id" value="{{$comtel->nCodComTel}}">	
	                    <input type="hidden" name="conta" id="conta" value="">	                    
	                    <input type="hidden" name="eliminados" id="eliminados" value="">	                    
	                    <!--input type="hidden" name="prod_agre" id="prod_agre" value="">
	                    <input type="hidden" name="prod_codpro" id="prod_codpro" value=""-->
	                   
		                <div class="form-group">
			                <div class="row">
			                	<div class="col-md-3 {{ $errors->has('fecha') ? 'has-error' :'' }}">
			                		<label>Fecha</label>
			                    	  <input class="form-control" type="date" name="fecha" id="fecha" min="2000-01-01" value="{{substr($comtel->tFecha, 0, 10)}}" tabindex="1" readonly>
			                    	{!! $errors->first('fecha','<span class="help-block">:message</span>') !!}
			                	</div>
			                	<div class="col-md-3 {{ $errors->has('proveedor') ? 'has-error' :'' }}">
			                		<label>Proveedor</label>
			                    	<select id="proveedor" class="form-control" name="proveedor" readonly>
	                                    <option value="">Seleccione...</option>
	                                    @foreach ($proveedores as $key => $proveedor)
	                                    	@if($comtel->nCodProv==$proveedor->id)
	                                    		<option value="{{$proveedor->id}}" selected>{{$proveedor->nombre_comercial}}</option>
	                                    	@else
	                                        	<option value="{{$proveedor->id}}">{{$proveedor->nombre_comercial}}</option>
	                                        @endif
	                                    @endforeach
	                                </select>
			                    	{!! $errors->first('proveedor','<span class="help-block">:message</span>') !!}
			                    </div>
			                    <div class="col-md-3 {{ $errors->has('tipdoc') ? 'has-error' :'' }}">
			                    	<label>Tipo Doc.</label>
			                    	<select id="tipdoc" class="form-control" name="tipdoc" readonly>
	                                    <option value="">Seleccione...</option>
	                                    @foreach ($tipdocpag as $key => $tdp)
	                                    	@if($comtel->nCodTdoc==$tdp->nTipPagCod)
	                                    		<option value="{{$tdp->nTipPagCod}}" selected>{{$tdp->cDescTipPago}}</option>
	                                    	@else
	                                        	<option value="{{$tdp->nTipPagCod}}">{{$tdp->cDescTipPago}}</option>
	                                        @endif	                                        
	                                    @endforeach
	                                </select>
			                    	{!! $errors->first('tipdoc','<span class="help-block">:message</span>') !!}
			                	</div>
			                	<div class="col-md-3 {{ $errors->has('forpag') ? 'has-error' :'' }}">
			                		<label>Formas de Pago</label>
			                    	<select id="forpag" class="form-control" name="forpag" readonly>
	                                    <option value="">Seleccione...</option>
	                                    @foreach ($forpago as $key => $fp)
	                                        @if($comtel->nCodForP==$fp->nForPagCod)
	                                    		<option value="{{$fp->nForPagCod}}" selected>{{$fp->cDescForPag}}</option>
	                                    	@else
	                                        	<option value="{{$fp->nForPagCod}}">{{$fp->cDescForPag}}</option>
	                                        @endif	 	                                        
	                                    @endforeach
	                                </select>
			                    	{!! $errors->first('forpag','<span class="help-block">:message</span>') !!}
			                	</div>
			                </div>

			                <div class="row">
			                	<div class="col-md-3">
			                		<label>Producto</label>
			                    	<select id="prod" class="form-control" name="prod">
	                                    <option value='' >Seleccione un producto...</option>
                                        <?php
                                        	for ($i=0; $i < sizeof($results) ; $i++) 
                                        	{
                                        ?> 
                                        	<option dato-esp='{{$results[$i]["nombre_especifico"]}}' value='{{$results[$i]["nProAlmCod"]}}'>{{$results[$i]["value"]}}</option>		                                        		
                                        <?php
                                        	}
                                        ?>
	                                </select>
			                	</div>
			                	<div class="col-md-3">
			                		<label>Peso</label>
			                    	<input class="form-control" type="text" name="pes" id="pes" placeholder="0.00" value="">                                
			                    </div>
			                    <div class="col-md-3">
			                    	<label>Moneda</label>
			                    	<select id="mon" class="form-control" name="mon" readonly>	                
			                    		@if($comtel->cMoneda=='S')
                                    		<option value="S" selected>S/. Soles</option>
	                                    	<option value="$">$/. Dorales</option>
                                    	@else
                                        	<option value="S">S/. Soles</option>
	                                    	<option value="$" selected>$/. Dorales</option>
                                        @endif
	                                </select>
			                	</div>
			                	<div class="col-md-3">
			                		<label>Precio</label>
			                    	<input class="form-control" type="text" name="prec" id="prec" placeholder="0.00" value="">  
			                	</div>
			                </div>

			                <div class="row">			                
			                	<div class="col-md-3">
			                		<label>N° Documento(Factura/guía)</label>
			                    	<input class="form-control" type="text" name="dat" id="dat" placeholder="" value="{{$comtel->cDatDocFac}}">                                
			                    </div>

			                	<div class="col-md-7">
			                		<label>Observación</label>
			                    	<input class="form-control" type="text" name="obs" id="obs" placeholder="" value="{{$comtel->cObs}}">                                
			                    </div>
			                
			                	<div class="col-md-2">
			                		<label style="color:white;">jjjj</label>
			                    	<a class="signbuttons btn btn-primary form-control" onclick="agredardetalle()"><i class="fa fa-plus" aria-hidden="true" ></i> Agregar</a> 
			                	</div>
			                </div>
			            </div>

			            <table id="bandeja" name="bandeja" class="table table-striped table-bordered table-hover">
                            <thead>                                           
                                <th>                                    
                                    Quitar
                                </th> 
                                <th>
                                    Producto
                                </th>                              
                                <th>
                                    Peso
                                </th>
                                <th>
                                    Moneda
                                </th>
                                <th>
                                    Precio
                                </th>
                                <th>                                    
                                    Subtotal
                                </th>
                                                                           
                            </thead>
                            <tbody style="text-align: center;">
                                
                                
                            </tbody>
                            	<tr>
                            		<td colspan="4"></td>
                            		<input type='hidden'  name='tot_g_i' id='tot_g_i'>
                            		<td style="text-align: center;">Total</td>
                            		<td id="tot_g" name="tot_g" style="text-align:right;">0.00</td>
                            	</tr>
                        </table>
                        <ul class="pagination">
                                      
                        </ul>
                        <div>
		                <button type="submit" class=" signbuttons btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button> <a href="{{ route('compraTT.index') }}" class=" signbuttons btn btn-primary">Bandeja</a>
		                </div>
		            </form>

    			</div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
	var indice_tabla=0;        
    var cant_items_real=0; 
    var indice_act=1;
    var ultima_pag=1;
    var cantxpag=7;
    var cont_item_bd=0;
    var total=0;
      

    $(document).ready(function() 
    {
        /*@if(Session::has('cod'))            
            window.open("{{ route('venta.reporte',Session::get('cod')) }}");            
        @endif*/
        $("form").keypress(function(e) {
            if (e.which == 13) {
                return false;
            }
        });
        $('#pes').numeric(",").numeric({decimalPlaces: 2,negative: false});
        $('#prec').numeric(",").numeric({decimalPlaces: 2,negative: false});        
        /********************** PAGINACIÓN **************************/
        cant_items_real=cont_item_bd;    
        /*** cargar lista de p/ginas***/
        controlpaginacion(cantxpag,cant_items_real);        
        /*** cargar paginación inicial ***/
        paginacion(1,cantxpag);

        @foreach($comtel->DetComTel as $det)
        	agredardetalle_edit("{{$det->nDetCodComTel}}","{{$det->nProAlmCod}}","{{$det->ProductoAlmacen->producto->nombre_generico.' '.$det->ProductoAlmacen->color->nombre}}","{{$det->dPeso}}","{{$det->compratela->cMoneda}}","{{$det->dPrecio}}","{{$det->dSubtotal}}");
        @endforeach
    });

    $("#prod").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#pes").focus();
        }
        else if ( e.which == 27 ) 
        {
            //cancelarCli();
        }
    });
    $("#pes").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#mon").focus();
        }
        else if ( e.which == 27 ) 
        {
            $("#prod").focus();
        }
    });
    $("#mon").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#prec").focus();
        }
        else if ( e.which == 27 ) 
        {
            $("#pes").focus();
        }
    });

    function agredardetalle_edit(coddet,codpro,nompro,peso_x,mone_x,prec_x,st)
    {              	
        //if(validacion())
        //{
            // Obtenemos el total de columnas (tr) del id "tabla"  
            indice_tabla++;
            $("#conta").val(indice_tabla);
            var nuevaFila="<tr id=fila_"+indice_tabla+">";           

            nuevaFila+='<td><input type="hidden" name="cod_ndi_'+indice_tabla+'" id="cod_ndi_'+indice_tabla+'" value="'+coddet+'"><div class="btn btn-link" onclick="delTabla('+indice_tabla+')" ><a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quitar"></i></a></div>';

            nuevaFila+="<td>"+'<input type="hidden" name="prod_'+indice_tabla+'" id="prod_'+indice_tabla+'" value="'+codpro+'">'+nompro+"</td>";

            nuevaFila+="<td>"+'<input type="hidden" name="peso_'+indice_tabla+'" id="peso_'+indice_tabla+'" value="'+peso_x+'">'+peso_x+"</td>";

            nuevaFila+="<td><input type='hidden'  name='mon_"+indice_tabla+"' id='mon_"+indice_tabla+"' value='"+mone_x+"'>"+((mone_x=='S')?'S/.Soles':'$/.Dolares')+"</td>";

            nuevaFila+="<td>"+'<input type="hidden" name="prec_'+indice_tabla+'" id="prec_'+indice_tabla+'" value="'+prec_x+'">'+prec_x+"</td>";

            //var cal_stot=(parseFloat($("#prec").val())*parseFloat($("#pes").val())).toFixed(2);

            nuevaFila+="<td style='text-align:right;'>"+'<input type="hidden" name="stot_'+indice_tabla+'" id="stot_'+indice_tabla+'" value="'+st+'">'+st+"</td>";
                 
            nuevaFila+="</tr>";
            
            $("#bandeja").append(nuevaFila);

            total+=parseFloat(st);            
            totales(total.toFixed(2));
            //$("#prod_agre").val($("#produc").val()+","+$("#prod_agre").val());
            //$("#prod_codpro").val($("#produc option:selected").attr("dato-cod")+","+$("#prod_codpro").val());
            
            // paginación   
            cant_items_real++;            
            // cargar lista de p/ginas
            controlpaginacion(cantxpag,cant_items_real);        
            // cargar paginación inicial            
            paginacion(ultima_pag,cantxpag);

            limpiarcontroles();       	
        //}
    }

    function agredardetalle()
    {          
    	
        if(validacion())
        {
            // Obtenemos el total de columnas (tr) del id "tabla"  
            indice_tabla++;
            $("#conta").val(indice_tabla);
            var nuevaFila="<tr id=fila_"+indice_tabla+">";           

            nuevaFila+='<td><input type="hidden" name="cod_ndi_'+indice_tabla+'" id="cod_ndi_'+indice_tabla+'" value="0"><div class="btn btn-link" onclick="delTabla('+indice_tabla+')" >'+'<a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quitar"></i></a>'+'</div>';

            nuevaFila+="<td>"+'<input type="hidden" name="prod_'+indice_tabla+'" id="prod_'+indice_tabla+'" value="'+$("#prod").val()+'">'+$("#prod option:selected").text()+"</td>";

            nuevaFila+="<td>"+'<input type="hidden" name="peso_'+indice_tabla+'" id="peso_'+indice_tabla+'" value="'+$("#pes").val()+'">'+$("#pes").val()+"</td>";

            nuevaFila+="<td><input type='hidden'  name='mon_"+indice_tabla+"' id='mon_"+indice_tabla+"' value='"+$("#mon").val()+"'>"+$("#mon option:selected").text()+"</td>";

            nuevaFila+="<td>"+'<input type="hidden" name="prec_'+indice_tabla+'" id="prec_'+indice_tabla+'" value="'+$("#prec").val()+'">'+$("#prec").val()+"</td>";

            var cal_stot=(parseFloat($("#prec").val())*parseFloat($("#pes").val())).toFixed(2);

            nuevaFila+="<td style='text-align:right;'>"+'<input type="hidden" name="stot_'+indice_tabla+'" id="stot_'+indice_tabla+'" value="'+cal_stot+'">'+cal_stot+"</td>";
                 
            nuevaFila+="</tr>";
            
            $("#bandeja").append(nuevaFila);

            total+=parseFloat(cal_stot);            
            totales(total.toFixed(2));
            //$("#prod_agre").val($("#produc").val()+","+$("#prod_agre").val());
            //$("#prod_codpro").val($("#produc option:selected").attr("dato-cod")+","+$("#prod_codpro").val());
            
            // paginación   
            cant_items_real++;            
            // cargar lista de p/ginas
            controlpaginacion(cantxpag,cant_items_real);        
            // cargar paginación inicial            
            paginacion(ultima_pag,cantxpag);

            limpiarcontroles();   
    	
        }
    }

    function totales(dato_x)
    {    	
        $("#tot_g_i").val(dato_x);
        $("#tot_g").empty();
        $("#tot_g").html(dato_x);
    }

    function delTabla(id)
    {     
        
        if(confirm('Nota: El registro se retirará momentaneamente, pero el cambio no se hará efecto hasta que usted guarde los cambios.'))
        {
            var datos= $("#eliminados").val()+","+$("#cod_ndi_"+id).val();
            $("#eliminados").val(datos);

            //------- para actualizar valores en el control ---
            //incdesstock($("#prod_"+id).val(),((parseFloat($("#peso_"+id).val())!=0)?parseFloat($("#peso_"+id).val()):parseFloat($("#rollo_"+id).val())) ,"des");
            //calcular_totales(parseFloat($("#stotal_"+id).val()),"dec");
            //alert($("#canti_"+id).val());
            //incdesstock($("#proalm_"+id).val(),(($("#canti_"+id).val()!="")?parseFloat($("#canti_"+id).val()):parseFloat($("#rollo_"+id).val())),"dec");
                         

            //limpiarcontroles();
            // fin controles-----------------------------------
            total-=parseFloat($("#stot_"+id).val()).toFixed(2);            
            totales(total.toFixed(2));
            limpiarcontroles()

            $("#fila_"+id).remove();            

            cant_items_real--;
            // cargar lista de p/ginas
            controlpaginacion(cantxpag,cant_items_real);        
            // cargar paginación inicial 
            paginacion(indice_act,cantxpag);
        }            
    }

    function limpiarcontroles()
	{	
		//alert(parseFloat($("#tot_g_i").val()));
		if(parseFloat($("#tot_g_i").val())==0)
        {
        	$("#fecha").attr("readonly", false);
		    $("#proveedor").attr("readonly", false);
		    $("#tipdoc").attr("readonly", false);
		    $("#forpag").attr("readonly", false);
		    $("#mon").attr("readonly", false);
		    $("#dat").attr("readonly", false);
		    $("#obs").attr("readonly", false);	
        }
        else
        {
		    $("#fecha").attr("readonly", true);
		    $("#proveedor").attr("readonly", true);
		    $("#tipdoc").attr("readonly", true);
		    $("#forpag").attr("readonly", true);
		    $("#mon").attr("readonly", true);
		    $("#dat").attr("readonly", true);
		    $("#obs").attr("readonly", true);
		}
	    $("#prod").val("");
	    $("#pes").val("");
	    $("#prec").val("");
	    $("#fecha").css("border","1px solid #d2d6de");
		$("#proveedor").css("border","1px solid #d2d6de");
		$("#tipdoc").css("border","1px solid #d2d6de");
		$("#forpag").css("border","1px solid #d2d6de");
		$("#prod").css("border","1px solid #d2d6de");
		$("#pes").css("border","1px solid #d2d6de");
		$("#mon").css("border","1px solid #d2d6de");		
		$("#prec").css("border","1px solid #d2d6de");
    }

    function validacion()
    {

        var fecha_=$("#fecha").val(); 
        var proveedor_=$("#proveedor").val();  

        var tdoc_=$("#tipdoc").val(); 
        var forpag_=$("#forpag").val();  
        var prod_=$("#prod").val(); 
        var pes_=$("#pes").val();  
        var mon_=$("#mon").val(); 
        var prec_=$("#prec").val();  

        var key_error=true;
        
        if(fecha_=="")
        {
            $("#fecha").css("border","2px solid #f00");                
            key_error=false;
        }
        else
            $("#fecha").css("border","1px solid #d2d6de");

        
        if(proveedor_=="")
        {
            $("#proveedor").css("border","2px solid #f00");                
            key_error=false;
        }
        else
            $("#proveedor").css("border","1px solid #d2d6de");


        if(tdoc_=="")
        {
            $("#tipdoc").css("border","2px solid #f00");                
            key_error=false;
        }
        else
            $("#tipdoc").css("border","1px solid #d2d6de");

        
        if(forpag_=="")
        {
            $("#forpag").css("border","2px solid #f00");                
            key_error=false;
        }
        else
            $("#forpag").css("border","1px solid #d2d6de");

        if(prod_=="")
        {
            $("#prod").css("border","2px solid #f00");                
            key_error=false;
        }
        else
            $("#prod").css("border","1px solid #d2d6de");

        
        if(pes_=="" || parseFloat(pes_)<=0)
        {
            $("#pes").css("border","2px solid #f00");                
            key_error=false;
        }
        else
            $("#pes").css("border","1px solid #d2d6de");

        if(mon_=="")
        {
            $("#mon").css("border","2px solid #f00");                
            key_error=false;
        }
        else
            $("#mon").css("border","1px solid #d2d6de");

        
        if(prec_=="" || parseFloat(prec_)<=0)
        {
            $("#prec").css("border","2px solid #f00");                
            key_error=false;
        }
        else
            $("#prec").css("border","1px solid #d2d6de");

        return key_error;
    }

    function controlpaginacion(pag,total)
    {
        var cad='';
        var act=true;
        var i;
        for(i=1;i<=total/pag;i++)
        {

            if(act)
                cad='<li onclick="paginacion('+i+','+pag+')" style="cursor: pointer;"><span>«</span></li>';

            cad+='<li '+((act)?'class="active"':'')+' onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            act=false;
        }
        if(total%pag>0)
        {
            cad+='<li onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            cad+='<li onclick="paginacion('+(i)+','+pag+');" style="cursor: pointer;"><span>»</span></li>';
        }
        else
            cad+='<li onclick="paginacion('+(--i)+','+pag+')" style="cursor: pointer;"><span>»</span></li>';

        ultima_pag=i;

        $(".pagination").empty();
        $(".pagination").html(cad);
    }

    function paginacion(pag,de)
    {   
        // ocultar registros de 10 en 10 

        var fin=pag*de;;
        var ini=(pag-1)*de;


        //meter todo en un array, con indice ordenado                        
        var array_items=[];
        var contador=0;
        for (var i = 1; i <= parseInt($("#conta").val()); i++) 
        {                
            var nomfila="fila_"+i;
            if ( $("#"+nomfila).length > 0 ) 
            {
                array_items[contador]=nomfila;
                contador++;
            }                
        }
        //recorrer segun los limites mostrar lo necesario

        for(var i=0;i<contador;i++)
        {
            if(i>=ini && i<fin)
                $("#"+array_items[i]).show();        
            else
                $("#"+array_items[i]).hide();
        }

        indice_act=pag;//guarada el indice de pág

        for(i=1;i<=ultima_pag;i++)
            $("#pgitem_"+i).removeAttr('class');

        $("#pgitem_"+indice_act).attr('class','active');

        return 1;
    }


</script>
@endpush('scripts')