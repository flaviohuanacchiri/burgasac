@extends('backend.layouts.appv2')

@section('title', 'BANDEJA DE COMPRAS DE TELA TEÑIDA')

@section('after-styles')
    <style>
    .dropdown{padding: 0;}
    .dropdown .dropdown-menu{border: 1px solid #999}
    .detallescompra{
        display: none;
        background-color: #ececec;
    }
    </style>
@stop

@section('content')

	<div class="row">
		<div class="col-sm-12">
			<section class="content">
			    <div class="box box-info">
			        <div class="box-header with-border">
			            <h3 class="box-title">@yield('title')</h3>
			            <div class="box-tools pull-right">
			                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			            </div><!-- /.box tools -->
			        </div><!-- /.box-header -->
			        <div class="box-body">
			        	@include('comercializacion.compraTT.fragment.info')
			            <div class="row">                        	
                        	<form action="{{ route('compraTT.index') }}" method="GET">
	                        {{ csrf_field() }}
	                        <input type="hidden" name="_method" value="GET">

                            <div class="col-md-3">
                                <label for="">Fecha</label>
                                <input class="form-control" type="date" name="fecha" id="fecha" step="1" min="2000-01-01" value="{{ $datosant->fecha }}" tabindex="1">                                
                            </div>
                            <div class="col-md-3">
                                <label>Proveedor</label>
		                    	<select id="proveedor" class="form-control" name="proveedor" readonly>
                                    <option value="">Seleccione...</option>
                                    @foreach ($proveedores as $key => $proveedor)
                                    	@if($datosant->proveedor==$proveedor->id)
                                    		<option value="{{$proveedor->id}}" selected>{{$proveedor->nombre_comercial}}</option>
                                    	@else
                                        	<option value="{{$proveedor->id}}">{{$proveedor->nombre_comercial}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>Tipo Doc.</label>
		                    	<select id="tipdoc" class="form-control" name="tipdoc" readonly>
                                    <option value="">Seleccione...</option>
                                    @foreach ($tipdocpag as $key => $tdp)
                                    	@if($datosant->tipdoc==$tdp->nTipPagCod)
                                    		<option value="{{$tdp->nTipPagCod}}" selected>{{$tdp->cDescTipPago}}</option>
                                    	@else
                                        	<option value="{{$tdp->nTipPagCod}}">{{$tdp->cDescTipPago}}</option>
                                        @endif	                                        
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label>Formas de Pago</label>
		                    	<select id="forpag" class="form-control" name="forpag" readonly>
                                    <option value="">Seleccione...</option>
                                    @foreach ($forpago as $key => $fp)
                                        @if($datosant->forpag==$fp->nForPagCod)
                                    		<option value="{{$fp->nForPagCod}}" selected>{{$fp->cDescForPag}}</option>
                                    	@else
                                        	<option value="{{$fp->nForPagCod}}">{{$fp->cDescForPag}}</option>
                                        @endif	 	                                        
                                    @endforeach
                                </select>
                            </div>	                          
                            <div class="col-md-2" style="text-align:center;">
                                <br>
                                <button type="submit" class=" signbuttons btn btn-primary">Buscar</button>                            
                            </div>
                        	</form>	                        
	                    </div>
	                    <br>

			  			<div class="row">							
							<div class="col-md-12">
								<table class="table table-hover table-striped">
									<thead>
										<tr>
											<th>Fecha</th>									
											<th>Proveedor</th>									
											<th>T.Documento</th>
											<th>Forma de Pago</th>									
											<th>N° Documento</th>									
											<th>Moneda</th>									
											<th>Monto</th>									
											<th colspan="2">Acciones</th>
										</tr>
									</thead>
									<tbody style="text-align: center;">
										@foreach($ct as $c)
										<tr>							
											<td>{{ $c->tFecha }}</td>									
											<td>{{ $c->proveedor->nombre_comercial }}</td>
											<td>{{ $c->tipodoc->cDescTipPago }}</td>									
											<td>{{ $c->formapago->cDescForPag }}</td>

											<td>{{ $c->cDatDocFac }}</td>
											<td>{{ $c->cMoneda }}</td>
											<td style="text-align: right;color:green;">{{ number_format($c->dMonTotal,2) }}</td>
											<td>
												
												<a href="{{ route('compraTT.edit',$c->nCodComTel) }}" class="btn btn-xs btn-primary">
													<i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i>
												</a>
												
												<a data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Delete" data-trans-title="¿Está seguro, que desea eliminar este registro de compra?" class="btn btn-xs btn-danger" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
													<i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
													
													<form action="{{ route('compraTT.destroy',$c->nCodComTel) }}" method="POST" name="delete_item" style="display:none">
													   {{ csrf_field() }}
														<input type="hidden" name="_method" value="DELETE">
													    <!--input type="hidden" name="_token" value="MAkHFJSuu2TYPiebIcKDFTy53mpZB3g3TGN9svAi"-->
													</form>
												</a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								{!! $ct->render() !!}
							</div>	
						</div>
			        </div>
			    </div>
			</section>
		</div>
	</div>

@endsection

@push('scripts')

<script type="text/javascript">	
    $(document).bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
        	$(location).attr('href',"{{ route('almacen.create') }}");
        };
    });

    /* show / hide order details */
        $(".detalle").click(function() {
          $(this).closest("tr").next().toggle('fast');
          if($("#ver").attr("class") == 'fa fa-eye fa-1x')
            $("#ver").attr("class", "fa fa-eye-slash fa-1x");
          else
            $("#ver").attr("class", "fa fa-eye fa-1x");
        });
</script>

@endpush('scripts')
