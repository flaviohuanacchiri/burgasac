<style type="text/css">
       .contenedor-tabla
        {
            display: table;
            width: 100%;
            font-size: 13px;
        }

	.contenedor-fila
        {
            display: table-row;            
        }

        .contenedor-columna
        {
            display: table-cell;
            padding-top: 1px;  
	}


        body{
            /**font-family: monospace;*/
            /*font-family: "Lucida Console", "Lucida Sans Typewriter", monaco, "Bitstream Vera Sans Mono", monospace; */
            font-family: "Times New Roman", Times, serif;
        }

        table, td, th {
            border: 1px solid black;
            font-size: 12px;
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th {
            height: 5px;
        }
    </style>

<title>Resumen de Ventas por Cliente</title>

<body style="margin-left: 4px;margin-right: 55px;">     
    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;text-align: center;padding-top: 10px;">
                <!--img src="img/logo.jpg" alt="Smiley face" height="25" width="45"-->
             </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;padding-top: 20px;padding-left: 10px;">
                <h1 style="margin: 0px;font-size: 20px;">RESUMEN DE VENTAS POR CLIENTE</h1>
            </div>
            <div class="contenedor-columna" style="width: 20%;text-align: right;">
                
            </div>
        </div>      
    </div>    
    
<br>
    <div class="contenedor-tabla">                
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 5%;">
                FILTROS:  
             </div>
            <div class="contenedor-columna" style="width: 65%;">
                 Cliente: {{$nomclie}} | Año: {{$request->anio}}
            </div>
            <div class="contenedor-columna" style="width: 30%;text-align: right;">
                FECHA: {{$fecha}}
            </div>
        </div>
    </div>

<!--div style="clear:both;"></div-->
<br>
    
    <div class="row">                                          
        <div class="col_x">
            <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
                <tr>
                    <th>
                        Cliente/Mes
                    </th>
                    <th>
                        Enero
                    </th>
                    <th>
                        Febrero
                    </th>
                    <th>
                        Marzo
                    </th>
                    <th>
                        Abril
                    </th>
                    <th>
                        Mayo
                    </th>
                    <th>
                        Junio
                    </th>
                    <th>
                        Julio
                    </th>
                    <th>
                        Agosto
                    </th> 
                    <th>
                        Setiembre
                    </th>
                    <th>
                        Octubre
                    </th> 
                    <th>
                        Noviembre
                    </th>
                    <th>
                        Diciembre
                    </th> 
                    <th>
                        Total
                    </th> 
                </tr>   
                <?php
                $ene=0;$feb=0;$mar=0;$abr=0;$may=0;$jun=0;$jul=0;$ago=0;$set=0;$oct=0;$nov=0;$dic=0;
                ?>
                @foreach($array as $a)            
                <?php $ene+=$a[1];?>
                <?php $feb+=$a[2];?>
                <?php $mar+=$a[3];?>
                <?php $abr+=$a[4];?>
                <?php $may+=$a[5];?>
                <?php $jun+=$a[6];?>
                <?php $jul+=$a[7];?>
                <?php $ago+=$a[8];?>
                <?php $set+=$a[9];?>
                <?php $oct+=$a[10];?>
                <?php $nov+=$a[11];?>
                <?php $dic+=$a[12];?>
                <tr>        
                    <td style="text-align: left;">{{$a[0]}}</td>                            
                    <td style="text-align: right;">{{number_format($a[1],3)}}</td>  
                    <td style="text-align: right;">{{number_format($a[2],3)}}</td>  
                    <td style="text-align: right;">{{number_format($a[3],3)}}</td>  
                    <td style="text-align: right;">{{number_format($a[4],3)}}</td>  
                    <td style="text-align: right;">{{number_format($a[5],3)}}</td>  
                    <td style="text-align: right;">{{number_format($a[6],3)}}</td>  
                    <td style="text-align: right;">{{number_format($a[7],3)}}</td>  
                    <td style="text-align: right;">{{number_format($a[8],3)}}</td>  
                    <td style="text-align: right;">{{number_format($a[9],3)}}</td>  
                    <td style="text-align: right;">{{number_format($a[10],3)}}</td>  
                    <td style="text-align: right;">{{number_format($a[11],3)}}</td>  
                    <td style="text-align: right;">{{number_format($a[12],3)}}</td> 
                    <td style="text-align: right;">{{number_format($a[1]+$a[2]+$a[3]+$a[4]+$a[5]+$a[6]+$a[7]+$a[8]+$a[9]+$a[10]+$a[11]+$a[12],3)}}</td>  
                </tr>                
                @endforeach
                <tr>        
                    <td style="text-align: center;">TOTALES</td>                            
                    <td style="text-align: right;">{{number_format($ene,3)}}</td>  
                    <td style="text-align: right;">{{number_format($feb,3)}}</td>  
                    <td style="text-align: right;">{{number_format($mar,3)}}</td>  
                    <td style="text-align: right;">{{number_format($abr,3)}}</td>  
                    <td style="text-align: right;">{{number_format($may,3)}}</td>  
                    <td style="text-align: right;">{{number_format($jun,3)}}</td>  
                    <td style="text-align: right;">{{number_format($jul,3)}}</td>  
                    <td style="text-align: right;">{{number_format($ago,3)}}</td>  
                    <td style="text-align: right;">{{number_format($set,3)}}</td>  
                    <td style="text-align: right;">{{number_format($oct,3)}}</td>  
                    <td style="text-align: right;">{{number_format($nov,3)}}</td>  
                    <td style="text-align: right;">{{number_format($dic,3)}}</td>  
                    <td style="text-align: right;">{{number_format($ene+$feb+$mar+$abr+$may+$jun+$jul+$ago+$set+$oct+$nov+$dic,3)}}</td>  
                </tr> 
            </table>
        </div>
    </div>
</body>
