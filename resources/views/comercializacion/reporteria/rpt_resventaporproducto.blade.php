<style type="text/css">
       .contenedor-tabla
        {
            display: table;
            width: 100%;
            font-size: 13px;
        }

	.contenedor-fila
        {
            display: table-row;            
        }

        .contenedor-columna
        {
            display: table-cell;
            padding-top: 1px;  
	}


        body{
            /**font-family: monospace;*/
            /*font-family: "Lucida Console", "Lucida Sans Typewriter", monaco, "Bitstream Vera Sans Mono", monospace; */
            font-family: "Times New Roman", Times, serif;
        }

        table, td, th {
            border: 1px solid black;
            font-size: 12px;
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th {
            height: 5px;
        }
    </style>

<title>Resumen de ventas por producto</title>

<body style="margin-left: 4px;margin-right: 55px;">     
    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;text-align: center;padding-top: 10px;">
                <!--img src="img/logo.jpg" alt="Smiley face" height="25" width="45"-->
             </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;padding-top: 20px;padding-left: 10px;">
                <h1 style="margin: 0px;font-size: 20px;">RESUMEN DE VENTAS POR PRODUCTO</h1>
            </div>
            <div class="contenedor-columna" style="width: 20%;text-align: right;">
                
            </div>
        </div>      
    </div>    
    
<br>
    <div class="contenedor-tabla">                
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 5%;">
                FILTROS:  
             </div>
            <div class="contenedor-columna" style="width: 65%;">
                 Producto: {{$nompro}} | Año: {{$request->anio}}
            </div>
            <div class="contenedor-columna" style="width: 30%;text-align: right;">
                FECHA: {{$fecha}}
            </div>
        </div>
    </div>

<!--div style="clear:both;"></div-->
<br>
    
    <div class="row">                                          
        <div class="col_x">
            <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
                <tr>
                    <th>
                        Producto/Mes
                    </th>
                    <th>
                        Und. Medida
                    </th>
                    <th>
                        Enero
                    </th>
                    <th>
                        Febrero
                    </th>
                    <th>
                        Marzo
                    </th>
                    <th>
                        Abril
                    </th>
                    <th>
                        Mayo
                    </th>
                    <th>
                        Junio
                    </th>
                    <th>
                        Julio
                    </th>
                    <th>
                        Agosto
                    </th> 
                    <th>
                        Setiembre
                    </th>
                    <th>
                        Octubre
                    </th> 
                    <th>
                        Noviembre
                    </th>
                    <th>
                        Diciembre
                    </th> 
                    <th>
                        Total
                    </th> 
                </tr>   
                <?php
                $ene=0;$feb=0;$mar=0;$abr=0;$may=0;$jun=0;$jul=0;$ago=0;$set=0;$oct=0;$nov=0;$dic=0;
                $ene_p=0;$feb_p=0;$mar_p=0;$abr_p=0;$may_p=0;$jun_p=0;$jul_p=0;$ago_p=0;$set_p=0;$oct_p=0;$nov_p=0;$dic_p=0;
                ?>
                @foreach($array as $a)            
                <?php $ene+=substr($a[1], 0, strpos($a[1],"|")); $ene_p+=substr($a[1],strpos($a[1],"|")+1); ?>
                <?php $feb+=substr($a[2], 0, strpos($a[2],"|")); $feb_p+=substr($a[2],strpos($a[2],"|")+1); ?>
                <?php $mar+=substr($a[3], 0, strpos($a[3],"|")); $mar_p+=substr($a[3],strpos($a[3],"|")+1); ?>
                <?php $abr+=substr($a[4], 0, strpos($a[4],"|")); $abr_p+=substr($a[4],strpos($a[4],"|")+1); ?>
                <?php $may+=substr($a[5], 0, strpos($a[5],"|")); $may_p+=substr($a[5],strpos($a[5],"|")+1); ?>
                <?php $jun+=substr($a[6], 0, strpos($a[6],"|")); $jun_p+=substr($a[6],strpos($a[6],"|")+1); ?>
                <?php $jul+=substr($a[7], 0, strpos($a[7],"|")); $jul_p+=substr($a[7],strpos($a[7],"|")+1); ?>
                <?php $ago+=substr($a[8], 0, strpos($a[8],"|")); $ago_p+=substr($a[8],strpos($a[8],"|")+1); ?>
                <?php $set+=substr($a[9], 0, strpos($a[9],"|")); $set_p+=substr($a[9],strpos($a[9],"|")+1); ?>
                <?php $oct+=substr($a[10], 0, strpos($a[10],"|")); $oct_p+=substr($a[10],strpos($a[10],"|")+1); ?>                
                <?php $nov+=substr($a[11], 0, strpos($a[11],"|")); $nov_p+=substr($a[11],strpos($a[11],"|")+1); ?>
                <?php $dic+=substr($a[12], 0, strpos($a[12],"|")); $dic_p+=substr($a[12],strpos($a[12],"|")+1); ?>
                <tr>        
                    <td style="text-align: left;" rowspan="2">{{$a[0]}}</td>
                    <td style="text-align: center;">Monto S/.</td>                            
                    <td style="text-align: right;">{{number_format(substr($a[1], 0, strpos($a[1],"|")),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[2], 0, strpos($a[2],"|")),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[3], 0, strpos($a[3],"|")),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[4], 0, strpos($a[4],"|")),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[5], 0, strpos($a[5],"|")),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[6], 0, strpos($a[6],"|")),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[7], 0, strpos($a[7],"|")),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[8], 0, strpos($a[8],"|")),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[9], 0, strpos($a[9],"|")),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[10], 0, strpos($a[10],"|")),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[11], 0, strpos($a[11],"|")),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[12], 0, strpos($a[12],"|")),3)}}</td> 
                    <td style="text-align: right;">{{number_format(substr($a[1], 0, strpos($a[1],"|"))+substr($a[2], 0, strpos($a[2],"|"))+substr($a[3], 0, strpos($a[3],"|"))+substr($a[4], 0, strpos($a[4],"|"))+substr($a[5], 0, strpos($a[5],"|"))+substr($a[6], 0, strpos($a[6],"|"))+substr($a[7], 0, strpos($a[7],"|"))+substr($a[8], 0, strpos($a[8],"|"))+substr($a[9], 0, strpos($a[9],"|"))+substr($a[10], 0, strpos($a[10],"|"))+substr($a[11], 0, strpos($a[11],"|"))+substr($a[12], 0, strpos($a[12],"|")),3)}}</td>  
                </tr>  
                <tr>
                    <td style="text-align: center;">Peso KG</td> 
                    <td style="text-align: right;">{{number_format(substr($a[1],strpos($a[1],"|")+1),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[2],strpos($a[2],"|")+1),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[3],strpos($a[3],"|")+1),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[4],strpos($a[4],"|")+1),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[5],strpos($a[5],"|")+1),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[6],strpos($a[6],"|")+1),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[7],strpos($a[7],"|")+1),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[8],strpos($a[8],"|")+1),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[9],strpos($a[9],"|")+1),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[10],strpos($a[10],"|")+1),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[11],strpos($a[11],"|")+1),3)}}</td>  
                    <td style="text-align: right;">{{number_format(substr($a[12],strpos($a[12],"|")+1),3)}}</td> 
                    <td style="text-align: right;">{{number_format(substr($a[1],strpos($a[1],"|")+1)+substr($a[2],strpos($a[2],"|")+1)+substr($a[3],strpos($a[3],"|")+1)+substr($a[4],strpos($a[4],"|")+1)+substr($a[5],strpos($a[5],"|")+1)+substr($a[6],strpos($a[6],"|")+1)+substr($a[7],strpos($a[7],"|")+1)+substr($a[8],strpos($a[8],"|")+1)+substr($a[9],strpos($a[9],"|")+1)+substr($a[10],strpos($a[10],"|")+1)+substr($a[11],strpos($a[11],"|")+1)+substr($a[12],strpos($a[12],"|")+1),3)}}</td>  
                </tr>               
                @endforeach
                <tr>        
                    <td style="text-align: center;" rowspan="2">TOTALES</td> 
                    <td style="text-align: center;">Monto S/.</td>                            
                    <td style="text-align: right;">{{number_format($ene,3)}}</td>  
                    <td style="text-align: right;">{{number_format($feb,3)}}</td>  
                    <td style="text-align: right;">{{number_format($mar,3)}}</td>  
                    <td style="text-align: right;">{{number_format($abr,3)}}</td>  
                    <td style="text-align: right;">{{number_format($may,3)}}</td>  
                    <td style="text-align: right;">{{number_format($jun,3)}}</td>  
                    <td style="text-align: right;">{{number_format($jul,3)}}</td>  
                    <td style="text-align: right;">{{number_format($ago,3)}}</td>  
                    <td style="text-align: right;">{{number_format($set,3)}}</td>  
                    <td style="text-align: right;">{{number_format($oct,3)}}</td>  
                    <td style="text-align: right;">{{number_format($nov,3)}}</td>  
                    <td style="text-align: right;">{{number_format($dic,3)}}</td>  
                    <td style="text-align: right;">{{number_format($ene+$feb+$mar+$abr+$may+$jun+$jul+$ago+$set+$oct+$nov+$dic,3)}}</td>  
                </tr> 
                <tr>
                    <td style="text-align: center;">Peso KG</td>                            
                    <td style="text-align: right;">{{number_format($ene_p,3)}}</td>  
                    <td style="text-align: right;">{{number_format($feb_p,3)}}</td>  
                    <td style="text-align: right;">{{number_format($mar_p,3)}}</td>  
                    <td style="text-align: right;">{{number_format($abr_p,3)}}</td>  
                    <td style="text-align: right;">{{number_format($may_p,3)}}</td>  
                    <td style="text-align: right;">{{number_format($jun_p,3)}}</td>  
                    <td style="text-align: right;">{{number_format($jul_p,3)}}</td>  
                    <td style="text-align: right;">{{number_format($ago_p,3)}}</td>  
                    <td style="text-align: right;">{{number_format($set_p,3)}}</td>  
                    <td style="text-align: right;">{{number_format($oct_p,3)}}</td>  
                    <td style="text-align: right;">{{number_format($nov_p,3)}}</td>  
                    <td style="text-align: right;">{{number_format($dic_p,3)}}</td>  
                    <td style="text-align: right;">{{number_format($ene_p+$feb_p+$mar_p+$abr_p+$may_p+$jun_p+$jul_p+$ago_p+$set_p+$oct_p+$nov_p+$dic_p,3)}}</td>  
                </tr> 
            </table>
        </div>
    </div>
</body>
