<style type="text/css">
       .contenedor-tabla
        {
            display: table;
            width: 100%;
            font-size: 15px;
        }

	.contenedor-fila
        {
            display: table-row;            
        }

        .contenedor-columna
        {
            display: table-cell;
            padding-top: 1px;  
	}


        body{
            /**font-family: monospace;*/
            /*font-family: "Lucida Console", "Lucida Sans Typewriter", monaco, "Bitstream Vera Sans Mono", monospace; */
            font-family: "Times New Roman", Times, serif;
        }

        table, td, th {
            border: 1px solid black;
            font-size: 15px;
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 100%;    }

        th {
            height: 5px;     }
    </style>

<title>Stock de colores por tienda</title>

<body style="margin-left: 4px;margin-right: 55px;">     
    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;text-align: center;padding-top: 10px;">
                <!--img src="img/logo.jpg" alt="Smiley face" height="25" width="45"-->
             </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;padding-top: 20px;padding-left: 10px;">
                <h1 style="margin: 0px;font-size: 20px;">STOCK DE COLORES POR TIENDA</h1>
            </div>
            <div class="contenedor-columna" style="width: 20%;text-align: center;">
                
            </div>
        </div>      
    </div>    
    
<br>
    <div class="contenedor-tabla">                
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 5%;">
                PRODUCTO: 
             </div>
            <div class="contenedor-columna" style="width: 50%;">
                {{strtoupper($nompro->nombre_especifico.' - '.$nompro->nombre_generico)}}
            </div>
            <div class="contenedor-columna" style="width: 45%;text-align: right;">
                FECHA: {{$fecha}}
            </div>
        </div>
    </div>

<!--div style="clear:both;"></div-->
<br>
    
    <div class="row">                                          
        <div class="col_x">
            <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
                <tr>
                    <th width="10px;" rowspan="2">
                        COLORES
                    </th>
                    @foreach($tienda as $t)
                    <th width="100px;" colspan="2">
                        {{$t->cAlmNom}}
                    </th>
                    @endforeach                                                                                    
                    <th width="10px" rowspan="2">
                        COLORES
                    </th>                   
                    <th colspan="2" width="20px" rowspan="2">
                        TOTAL GENERAL
                    </th>                   
                </tr>      
                <tr>
                    
                    @foreach($tienda as $t)
                    <th width="50px;">
                        Kilos
                    </th>
                    <th width="50px;">
                        Rollos
                    </th>
                    @endforeach                                                                                    
                    
                </tr>           
                @foreach($color_Bus as $c)
                    <tr>                                                            
                        <td>{{$c->nombre}}</td>
                            <?php $kilo=0; ?>
                            <?php $rollo=0; ?>
                            @foreach($tienda as $t)
                            <?php $reg=0; ?>
                                @foreach($prodalm_Bus as $pab) 
                                    @if($c->id == $pab->id && $t->nAlmCod == $pab->nAlmCod)
                                        @if(trim($c->nombre_especifico)=="TELAS")  
                                        <td style="text-align: center;">{{ number_format($pab->nProdAlmStock,3) }}</td>
                                        <?php $kilo+=$pab->nProdAlmStock; ?>
                                        <td style="text-align: center;">{{ number_format($pab->nProdAlmStock/20,3) }}</td>
                                        <?php $rollo+=$pab->nProdAlmStock/20; ?>
                                        @else
                                        <td style="text-align: center;">{{ 0.00 }}</td>
                                        <?php $kilo+=0; ?>
                                        <td style="text-align: center;">{{ number_format($pab->nProdAlmStock,3) }}</td>
                                        <?php $rollo+=$pab->nProdAlmStock; ?>
                                        @endif
                                    <?php $reg++; ?>
                                    @endif
                                @endforeach
                             @if($reg==0)
                                <td style="text-align: center;">0.000</td>
                                <td style="text-align: center;">0.000</td>
                            @endif 
                            @endforeach
                        
                        <td> {{$c->nombre}}</td> 

                        <td>{{number_format($kilo,3)}}</td> 
                        <td>{{number_format($rollo,3)}}</td> 
                    </tr>
                @endforeach                                     
            </table>
        </div>
    </div>
</body>
