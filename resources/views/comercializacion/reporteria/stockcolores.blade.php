@extends('backend.layouts.appv2')

@section('subtitulo', 'Reporte: Stock de colores por tienda')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">@yield('subtitulo')</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div><!-- /.box tools -->
                </div><!-- /.box-header -->
                <div class="box-body">      
                    <form method="GET" action="{{ route('reporteria.stockcolores') }}" accept-charset="UTF-8" target="_blank">
                        <input name="_token" type="hidden" >
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="GET">             
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2" style="text-align: center;">
                                    <label>Producto</label>
                                </div>
                                <div class="col-md-8">                                                 
                                    <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" autofocus="autofocus" id="selprod" name="selprod">                                        
                                        @foreach($productos as $prod)
                                            <option data-subtext="({{$prod->nombre_especifico}})" value="{{$prod->id}}">{{$prod->nombre_generico}}</option>
                                        @endforeach
                                    </select>             
                                </div>
                                <button type="submit" class=" signbuttons btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Visualizar</button>                
                            </div>
                        </div>  
                    </form>                      
                </div><!-- /.box-body -->
            </div><!--box box-success-->
        </section>
    </div>
</div>

@endsection

@push('scripts')
    <script>
       $(document).ready(function() 
       {
          $("form").keypress(function(e) {
                if (e.which == 13) {
                    return false;
                }
            });
        });
   </script>
@endpush('scripts')