<style type="text/css">
       .contenedor-tabla
        {
            display: table;
            width: 100%;
            font-size: 12px;
        }

	.contenedor-fila
        {
            display: table-row;            
        }

        .contenedor-columna
        {
            display: table-cell;
            padding-top: 1px;  
	}


        body{
            /**font-family: monospace;*/
            /*font-family: "Lucida Console", "Lucida Sans Typewriter", monaco, "Bitstream Vera Sans Mono", monospace; */
            font-family: "Times New Roman", Times, serif;
        }

        table, td, th {
            border: 1px solid black;
            font-size: 11px;
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th {
            height: 5px;
        }
    </style>

<title>Detalle de Ventas por Producto</title>

<body style="margin-left: 4px;margin-right: 55px;">     
    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;text-align: center;padding-top: 10px;">
                <!--img src="img/logo.jpg" alt="Smiley face" height="25" width="45"-->
             </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;padding-top: 20px;padding-left: 10px;">
                <h1 style="margin: 0px;font-size: 20px;">DETALLE DE VENTA POR PRODUCTO</h1>
            </div>
            <div class="contenedor-columna" style="width: 20%;text-align: right;">
                
            </div>
        </div>      
    </div>    
    
<br>
    <div class="contenedor-tabla">                
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 5%;">
                FILTROS: 
             </div>
            <div class="contenedor-columna" style="width: 65%;">
                 Fecha: [{{$request->fech_i.' - '.$request->fech_f}}] | Color:{{$v_color}} | Tienda: {{$v_tienda}} | Producto: {{$v_prod}}
            </div>
            <div class="contenedor-columna" style="width: 30%;text-align: right;">
                FECHA: {{$fecha}}
            </div>
        </div>
    </div>

<!--div style="clear:both;"></div-->
<br>
    
    <div class="row">                                          
        <div class="col_x">
            <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
                <tr>
                    <th>
                        Fecha
                    </th>
                    <!--th>
                        Operación
                    </th>
                    <th>
                        Cliente
                    </th-->
                    <th>
                        Tienda
                    </th>
                    <!--th>
                        Tipo Doc.
                    </th>
                    <th>
                        Forma Pago
                    </th-->
                    <th>
                        Producto
                    </th>
                    <th>
                        Hilo
                    </th>
                    <th>
                        Color
                    </th> 
                    <th>
                        Peso
                    </th>
                    <th>
                        Rollos
                    </th> 
                    <!--th>
                        Precio
                    </th-->
                    <th>
                        Monto
                    </th> 
                </tr>      
                @foreach($res_venta as $v)
                    <tr>                                                            
                        <td>{{substr($v->dVFacFemi, 0, strpos($v->dVFacFemi,' '))}}</td>                            
                                           
                        <td>{{$v->cAlmNom}}</td>                            
                                              
                        <td>{{$v->nombre_generico.' - '.$v->nombre_especifico}}</td>                            
                        <td>{{$v->material}}</td>                            
                        <td>{{$v->nombre}}</td>                            
                        <td style="text-align: right;">{{number_format($v->nVtaPeso,2)}}</td>
                        <td style="text-align: right;">{{number_format($v->nVtaCant,2)}}</td>
                        <td style="text-align: right;">{{number_format($v->nVtaPreST,2)}}</td>                            
                    </tr>
                @endforeach                                     
            </table>
        </div>
    </div>
</body>
