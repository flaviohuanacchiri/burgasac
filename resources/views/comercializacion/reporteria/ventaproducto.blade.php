@extends('backend.layouts.appv2')

@section('subtitulo', 'Reporte: Detalle de venta del producto')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">@yield('subtitulo')</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div><!-- /.box tools -->
                </div><!-- /.box-header -->
                <div class="box-body">      
                    <form method="GET" action="{{ route('reporteria.ventaproducto') }}" accept-charset="UTF-8" target="_blank">
                        <input name="_token" type="hidden" >
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="GET">             
                        <div class="form-group">
                            <div class="row">                                
                                <div class="col-md-2">                                              
                                    <label>Fecha Inicial</label>
                                    <input class="form-control" type="date" name="fech_i" id="fech_i" step="1" min="2000-01-01" value="<?php echo date("Y-m-d");?>" tabindex="1" style="font-size: 12px;">
                                </div>
                                <div class="col-md-2">                        
                                    <label>Fecha Final</label>
                                    <input class="form-control" type="date" name="fech_f" id="fech_f" step="1" min="2000-01-01" value="<?php echo date("Y-m-d");?>" tabindex="2" style="font-size: 12px;">                      
                                </div>
                                <div class="col-md-2">   
                                    <label>Cliente</label> 
                                    <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" id="rdni" name="rdni" style="font-size: 12px;">    
                                        <option data-subtext="" value="">...</option>                       
                                        @foreach($cli as $u)
                                            <option data-subtext="({{$u->cClieNdoc}})" value="{{$u->nClieCod}}">{{$u->cClieDesc}}</option>
                                        @endforeach
                                    </select>                                           
                                </div>
                                <div class="col-md-2">                                                 
                                    <label>Tienda</label>
                                    <select class="form-control" name="tien" id="tien" style="font-size: 12px;">
                                        <option value="">Todas las tiendas...</option>
                                        @foreach($tiendas as $t)
                                            <option value="{{$t->nAlmCod}}">{{$t->cAlmNom}}({{$t->cAlmUbi}})</option>   
                                        @endforeach
                                    </select>               
                                </div>
                                <div class="col-md-2 {{ $errors->has('prod') ? 'has-error' :'' }}">                                                 
                                    <label>Producto</label>                                               
                                    <select class="form-control" data-show-subtext="true" data-live-search="true" autofocus="autofocus" id="prod" name="prod" style="font-size: 12px;">
                                       <option value="">Todos los Productos...</option>
                                        @foreach($productos as $p)
                                            <option value="{{$p->id}}">{{$p->nombre_generico}}({{$p->nombre_especifico}})</option>   
                                        @endforeach
                                    </select>            
                                    {!! $errors->first('prod','<span class="help-block">:message</span>') !!}   
                                </div>
                                <div class="col-md-2"> 
                                    <label style="color:white;">ff</label>                                                
                                    <button type="submit" class=" signbuttons btn btn-primary form-control"><i class="fa fa-floppy-o" aria-hidden="true"></i> Visualizar</button>                
                                </div>                                
                            </div>
                        </div>  
                    </form>                      
                </div><!-- /.box-body -->
            </div><!--box box-success-->
        </section>
    </div>
</div>

@endsection

@push('scripts')
    <script>
       $(document).ready(function() 
       {
          $("form").keypress(function(e) {
                if (e.which == 13) {
                    return false;
                }
            });
        });
       /*$("#tien").change(function(event){
        $("#prod").empty();
        //$('#prod').selectpicker('refresh');

        $.get("productoalm/"+event.target.value,function(response,alm_id){
            $("#prod").append("<option data-subtext='' value='' >Seleccione producto...</option>");  
            for(i=0;i<response.length;i++){
                $("#prod").append("<option data-subtext='"+response[i].nombre+"' dato-esp='"+response[i].nombre_especifico+"' value='"+response[i].nProAlmCod+"' > "+response[i].nombre_generico+"</option>");                          
            }  
        //$('#prod').selectpicker('refresh');                 
        })
        });*/
   </script>
@endpush('scripts')