<style type="text/css">
       .contenedor-tabla
        {
            display: table;
            width: 100%;
            font-size: 14px;
        }

	.contenedor-fila
        {
            display: table-row;            
        }

        .contenedor-columna
        {
            display: table-cell;
            padding-top: 1px;  
	}


        body{
            /**font-family: monospace;*/
            /*font-family: "Lucida Console", "Lucida Sans Typewriter", monaco, "Bitstream Vera Sans Mono", monospace; */
            font-family: "Times New Roman", Times, serif;
        }

        table, td, th {
            border: 1px solid black;
            font-size: 12px;
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th {
            height: 5px;
        }
    </style>

<title>Cuentas por cobrar</title>

<body style="margin-left: 4px;margin-right: 55px;">     
    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;text-align: center;padding-top: 10px;">
                <!--img src="img/logo.jpg" alt="Smiley face" height="25" width="45"-->
             </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;padding-top: 20px;padding-left: 10px;">
                <h1 style="margin: 0px;font-size: 20px;">CUENTAS POR COBRAR</h1>
            </div>
            <div class="contenedor-columna" style="width: 20%;text-align: right;">
                
            </div>
        </div>      
    </div>    
    
<br>
    <div class="contenedor-tabla">                
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 5%;">
                FILTROS:  
             </div>
            <div class="contenedor-columna" style="width: 65%;">
                 Cliente: {{$nomclie}} | T.Documento: {{$nomtdoc}}
            </div>
            <div class="contenedor-columna" style="width: 30%;text-align: right;">
                FECHA: {{$fecha}}
            </div>
        </div>
    </div>

<!--div style="clear:both;"></div-->
<br>
    
    <div class="row">                                          
        <div class="col_x">
            <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
                <tr>
                    <th>
                        Cliente
                    </th>
                    <th>
                        T.Documento
                    </th>
                    <th>
                        N°Documento
                    </th>
                    <th>
                        Forma de Pago
                    </th>
                    <th>
                        Fecha de Venta
                    </th>
                    <th>
                        Dias
                    </th>
                    <th>
                        Importe
                    </th>
                    <th>
                        Monto Abonado
                    </th>
                    <th>
                        Saldo
                    </th>
                </tr>                   
                @foreach($cont_cli as $cc)
                    <?php 
                        $v_1=0;
                        $v_2=0;
                        $v_3=0;
                    ?>
                    @foreach($datos as $dt)                     
                        @if($dt->nClieCod == $cc->nClieCod)
                        <?php
                            $v_1+=$dt->dVFacVTot;
                            $v_2+=$dt->pago;
                            $v_3+=$dt->saldo;
                        ?>
                            <tr>        
                                <td>{{$dt->cliente->cClieDesc}}</td>
                                <td>{{$dt->tipodocv->cDescTipPago}}</td>
                                <td>{{$dt->nVtaCod}}</td>
                                <td>{{$dt->forpago->cDescForPag}}</td>
                                <td>{{substr($dt->dVFacFemi, 0, strpos($dt->dVFacFemi," "))}}</td>
                                <td></td>
                                <td style="text-align: right;">{{number_format($dt->dVFacVTot,2)}}</td>
                                <td style="text-align: right;">{{number_format($dt->pago,2)}}</td>
                                <td style="text-align: right;">{{number_format($dt->saldo,2)}}</td>
                            </tr>  
                        @endif
                    @endforeach
                            <tr>        
                                <td colspan="5"></td>
                                

                                <td style="text-align: center;font-weight:bold;font-size: 13px;">TOTAL</td>
                                <td style="text-align: right;font-weight:bold;font-size: 13px;">{{number_format($v_1,2)}}</td>
                                <td style="text-align: right;font-weight:bold;font-size: 13px;">{{number_format($v_2,2)}}</td>
                                <td style="text-align: right;font-weight:bold;font-size: 13px;">{{number_format($v_3,2)}}</td>
                            </tr> 
                            <tr>
                                <td colspan="9"></td>
                            </tr> 
                @endforeach
               
            </table>
        </div>
    </div>
</body>
