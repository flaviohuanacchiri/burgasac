@extends('backend.layouts.appv2')

@section('subtitulo', 'NOTA DE DEVOLUCIÓN')

@section('content')

<form action="{{ route('notadevolucion.store') }}" method="POST">
	{{ csrf_field() }}
	<input type="hidden" name="_method" value="POST">	
    <input type="hidden" name="codint" id="codint" value="{{$id}}">
    <input type="hidden" name="conta" id="conta" value="">
    <input type="hidden" name="conta2" id="conta2" value="">
    <input type="hidden" name="eliminados" id="eliminados" value="">
    <input type="hidden" name="actualizar" id="actualizar" value="">
    <input type="hidden" name="cad_actt" id="cad_actt" value="">
    <input type="hidden" name="key_stock" id="key_stock" value="0">

     <input type="hidden" name="dTotDev" id="dTotDev" value="0">
    <input type="hidden" name="dTotRep" id="dTotRep" value="0">

	<div class="row">
		<div class="col-sm-12">
			<section class="content">
			    <div class="box box-info">
			        <div class="box-header with-border">
			            <h3 class="box-title">@yield('subtitulo')</h3>
			            <div class="box-tools pull-right">
			                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			            </div><!-- /.box tools -->
			        </div><!-- /.box-header -->
				        <div class="box-body">
				            @include('comercializacion.almacen.fragment.info')

				            <div class="row">
			                    <div class="col-md-12">
			                        <div class="panel panel-default">                            
			                            <div class="panel-body">		                                
								                <div class="form-group">
									                <div class="row" style="margin-top: 15px;">
									                	<div class="col-md-3">
									                		<label>Fecha</label>									                    	
									                    	<input class="form-control" type="text" name="fech" id="fech" value="{{ $fac->dVFacFemi }}" disabled>
									                	</div>
									                	<div class="col-md-2">
									                		<label>T. Documento</label>
									                    	<input class="form-control" type="text" name="tdoc_d" id="tdoc_d" value="{{ $fac->tipodocv->cDescTipPago }}" disabled>
									                    </div>
									                    <div class="col-md-2">
									                    	<label>Forma de Pago</label>
									                    	<input class="form-control" type="text" name="fpago_d" id="fpago_d" value="{{ $fac->forpago->cDescForPag }}" disabled>
									                	</div>
									                	<div class="col-md-2">
									                		<label>RUC</label>
									                    	<input class="form-control" type="text" name="ruc_d" id="ruc_d" value="{{ (strlen($fac->cliente->cClieNdoc) == 11)?$fac->cliente->cClieNdoc:'' }}" disabled>
									                	</div>
									                	<div class="col-md-3">
									                		<label>Guía/Factura</label>
									                    	<input class="form-control" type="text" name="gfac_d" id="gfac_d" value="{{ $fac->nVtaCod }}" maxlength="6" disabled>
									                	</div>
									                </div>
									                <div class="row" style="margin-top: 15px;">
									                	<div class="col-md-5">
									                		<label>Nombres</label>
									                    	<input class="form-control" type="text" name="nombre_d" id="nombre_d" placeholder="Nombre" value="{{ $fac->cliente->cClieDesc}}" disabled>			                    	
									                	</div>
									                	<div class="col-md-2">
									                		<label>DNI</label>
									                    	<input class="form-control" type="text" name="dni_d" id="dni_d" placeholder="Dirección" value="{{ (strlen($fac->cliente->cClieNdoc) == 8)?$fac->cliente->cClieNdoc:'' }}" disabled>
									                    </div>
									                    <div class="col-md-5" style="    padding: 6px 0px 0px 20px;">
									                    	
									                	</div>
									                </div>
									            </div>

								                
			                            </div>
			                        </div>
			                    </div>
			                </div>

			                <div class="panel panel-default">
		                        <div class="panel-body">

		                            <div class="row">

		                                <div class="col-md-12">
		                                	<!--div id="cap_ini" name="cap_ini" style="background-color: #00000030; width: 100%; height: 100%; position: absolute; z-index: 10;display: none;"></div-->
		                                    <table id="bandeja-transfer_dev" name="bandeja-transfer_dev" class="table table-striped table-bordered table-hover">
		                                        <thead>
		                                            <th>
		                                                Id
		                                            </th>   
		                                            <th>
		                                                Nota de Devolución
		                                            </th> 
		                                            <th>
		                                                Código
		                                            </th>                                         
		                                            <th>
		                                                Descripción
		                                            </th>
		                                            <th>
		                                                Peso
		                                            </th>
		                                            <th>
		                                                Rollo
		                                            </th>
		                                            <th>
		                                                P.Unit.
		                                            </th>
		                                            <th>
		                                                Subtotal
		                                            </th>
		                                            <th>
		                                                Estado del Producto
		                                            </th>
		                                            <th>
		                                                Acción
		                                            </th>                                       
		                                        
		                                        </thead>
		                                        <tbody style="text-align: center;">
		                                        	<?php  
							                    		/*$vari=(isset($_GET["page"]))?$_GET["page"]:1;
							                    		$cont=($vari-1)*5; */
							                    		$cont=0;
							                    	?>				
		                                            @foreach($dventa as $dv)
		                                            <tr>
		                                            	<td>
		                                            		{{++$cont}}
		                                            	</td>
			                                            <td id="nd_{{$cont}}" name="nd_{{$cont}}">			                                                
			                                                @if($dv->bDevuelto==1)
			                                                	{{$dv->prodevolver->cTipNDev}}
			                                                @endif
			                                            </td> 
			                                            <td>
			                                                 {{$dv->nDvtaCB}}
			                                                <input type="hidden" name="nDvtaCB_{{$cont}}" id="nDvtaCB_{{$cont}}" value="{{$dv->nDvtaCB}}">
			                                                <input type="hidden" name="cdetalle_{{$cont}}" id="cdetalle_{{$cont}}" value="{{$dv->nDetVtaCod}}">
			                                                @if(is_null($dv->nProAlmCod)==0) 
				                                                <input type="hidden" name="nProAlmCod_{{$cont}}" id="nProAlmCod_{{$cont}}" value="{{$dv->nProAlmCod}}">
				                                                <!--input type="hidden" name="tcid_{{$cont}}" id="tcid_{{$cont}}" value="0">
				                                                <input type="hidden" name="mpid_{{$cont}}" id="mpid_{{$cont}}" value="0"-->
			                                                @else
			                                                	@if(is_null($dv->nCodTC)==0) 
					                                                <input type="hidden" name="nProAlmCod_{{$cont}}" id="nProAlmCod_{{$cont}}" value="TC{{$dv->nCodTC}}">
					                                                <!--input type="hidden" name="tcid_{{$cont}}" id="tcid_{{$cont}}" value="1">
					                                                <input type="hidden" name="mpid_{{$cont}}" id="mpid_{{$cont}}" value="0"-->
				                                                @else
				                                                	<input type="hidden" name="nProAlmCod_{{$cont}}" id="nProAlmCod_{{$cont}}" value="MP{{$dv->nCodMP}}">
					                                                <!--input type="hidden" name="tcid_{{$cont}}" id="tcid_{{$cont}}" value="0">
					                                                <input type="hidden" name="mpid_{{$cont}}" id="mpid_{{$cont}}" value="1"-->
				                                                @endif
			                                                @endif
			                                            </td>                                       
			                                            @if(is_null($dv->nProAlmCod)==0)        
			                                            <td>
			                                                {{$dv->productoAlmacen->producto->nombre_generico.' '.$dv->productoAlmacen->color->nombre}} ({{$dv->productoAlmacen->producto->nombre_especifico}})
			                                                <input type="hidden" name="prodgen_{{$cont}}" id="prodgen_{{$cont}}" value="{{$dv->productoAlmacen->producto->nombre_generico}}">
			                                                <input type="hidden" name="prodesp_{{$cont}}" id="prodesp_{{$cont}}" value="{{$dv->productoAlmacen->producto->nombre_especifico}}">
			                                            </td>
			                                            @else
			                                            	@if(is_null($dv->nCodTC)==0) 
				                                                <td>
					                                                {{$dv->resStocktelas->producto->nombre_generico}} (TC)
					                                                <input type="hidden" name="prodgen_{{$cont}}" id="prodgen_{{$cont}}" value="{{$dv->resStocktelas->producto->nombre_generico}}">
					                                                <input type="hidden" name="prodesp_{{$cont}}" id="prodesp_{{$cont}}" value="{{$dv->resStocktelas->producto->nombre_especifico}}">
					                                            </td>
			                                                @else
			                                                	<td>
				                                                	@if($dv->resStockMP->insumo_id==0)
				                                                		{{$dv->resStockMP->accesorio->nombre }} (MP)
						                                                <input type="hidden" name="prodgen_{{$cont}}" id="prodgen_{{$cont}}" value="{{$dv->resStockMP->accesorio->nombre}}">
						                                                <input type="hidden" name="prodesp_{{$cont}}" id="prodesp_{{$cont}}" value="TELAS">
									                                @else
									                                	{{$dv->resStockMP->insumo->nombre_generico }} (MP)
						                                                <input type="hidden" name="prodgen_{{$cont}}" id="prodgen_{{$cont}}" value="{{$dv->resStockMP->insumo->nombre_generico}}">
						                                                <input type="hidden" name="prodesp_{{$cont}}" id="prodesp_{{$cont}}" value="TELAS">									                                    
									                                @endif
					                                            </td>
			                                                @endif			                                            
			                                            @endif
			                                            <td>
			                                                {{$dv->nVtaPeso}}
			                                                <input type="hidden" name="peso_{{$cont}}" id="peso_{{$cont}}" value="{{$dv->nVtaPeso}}">
			                                            </td>
			                                            <td>
			                                                {{$dv->nVtaCant}}
			                                                <input type="hidden" name="cantx_{{$cont}}" id="cantx_{{$cont}}" value="{{$dv->nVtaCant}}">
			                                            </td>
			                                            <td>
			                                            	<input type="hidden" name="ppunit_{{$cont}}" id="ppunit_{{$cont}}" value="{{$dv->nVtaPrecioU}}">
			                                                {{$dv->nVtaPrecioU}}
			                                            </td>
			                                            <td>
			                                                {{$dv->nVtaPreST}}
			                                            </td>
			                                            <td>
			                                            	@if($dv->bDevuelto==1)
			                                                	@if($dv->prodevolver->cEstPro =='NDPM')
			                                                		<label><input type="radio" id="NDPM_{{$cont}}" name="estpro_d_{{$cont}}" value="NDPM" checked="checked" disabled>NDPM</label>
						                                    		<label><input type="radio" id="NDPB_{{$cont}}" name="estpro_d_{{$cont}}" value="NDPB" disabled>NDPB</label>
						                                    	@else
						                                    		<label><input type="radio" id="NDPM_{{$cont}}" name="estpro_d_{{$cont}}" value="NDPM" disabled>NDPM</label>
						                                    		<label><input type="radio" id="NDPB_{{$cont}}" name="estpro_d_{{$cont}}" value="NDPB" checked="checked" disabled>NDPB</label>
			                                                	@endif
			                                                @else
			                                                	<label><input type="radio" id="NDPM_{{$cont}}" name="estpro_d_{{$cont}}" value="NDPM" checked="checked" disabled>NDPM</label>
						                                    	<label><input type="radio" id="NDPB_{{$cont}}" name="estpro_d_{{$cont}}" value="NDPB" disabled>NDPB</label>
			                                                @endif
			                                                
			                                            </td>
			                                            <td>
			                                            	@if($dv->bDevuelto!=1)
			                                                	<a onclick="pasar({{$cont}})" name="btneje_{{$cont}}" id="btneje_{{$cont}}" class="btn btn-xs btn-success" disabled>
																<i class="fa fa-external-link-square" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ejecutar"></i>
															</a> 
			                                                @endif			                                                
			                                            </td>  
			                                        </tr>
		                                            @endforeach		                                            
		                                        </tbody>
		                                    </table>
		                                   
		                                </div>
		                            </div>
		                             <div class="row">
		                                <div class="col-md-4">
		                                    <label for="">Importe</label>
		                                    <input id="impo_d" type="text" class="form-control" name="impo_d" value="{{$fac->dVFacSTot}}" readonly>
		                                </div>
		                                <div class="col-md-4">
		                                    <label for="">Igv</label>
		                                    <input id="igv_d" type="text" class="form-control" name="igv_d" value="{{$fac->dVFacIgv}}" readonly>
		                                </div>
		                                <div class="col-md-4">
		                                    <label for="">Total</label>
		                                    <input id="total_d" type="text" class="form-control" name="total_d" value="{{$fac->dVFacVTot}}" readonly>
		                                </div>
		                            </div>
		                            <br>
		                            
		                        </div>
		                    </div>
		    			</div><!-- /.box-body -->
			    </div><!--box box-success-->
			</section>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<section class="content">
			    <div class="box box-info">
			        <div class="box-header with-border">
			            <h3 class="box-title">ESPECIFICACIONES DEL PRODUCTO A CAMBIAR</h3>
			            <div class="box-tools pull-right">
			                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			            </div><!-- /.box tools -->
			        </div><!-- /.box-header -->
				        <div class="box-body">				     
			                <div class="panel panel-default">
		                        <div class="panel-body">		                            
		                            <div class="row">
		                                <div class="col-md-6">
		                                    <label for="">Monto A favor</label>
		                                    <input id="mfavor" type="text" class="form-control" name="mfavor" value="{{$devv->dMonFavor}}" readonly>
		                                </div>
		                                <div class="col-md-6">
		                                    <label for="">Monto en Contra</label>
		                                    <input id="mcontra" type="text" class="form-control" name="mcontra" value="{{$devv->dMonContra}}" readonly>
		                                </div>
		                            </div>
		                            <br>
		                            <div class="row" style="text-align: center;font-size: 15px;">
		                                <div class="col-md-6">
		                                    <label style="text-align:center; ">Producto a Devolver</label>
		                                    
		                                </div>
		                                <div class="col-md-6">
		                                    <label style="text-align:center; ">Producto a Cambiar</label>
		                                   
		                                </div>
		                            </div>
		                            
		                            <div class="row">
		                                <div class="col-md-6">
		                                    <table id="bandeja-pd" name="bandeja-pd" class="table table-striped table-bordered table-hover">
		                                        <thead>
		                                            <th>
		                                                Item
		                                            </th>   
		                                            <th>
		                                                Código
		                                            </th>                                         
		                                            <th>
		                                                Producto
		                                            </th>
		                                            <th>
		                                                Peso
		                                            </th>
		                                            <th>
		                                                Cantidad
		                                            </th>		                                            
		                                        </thead>
		                                        <tbody style="text-align: center;">
		                                        	<?php $i_pd=0;?>
		                                            @foreach($pd as $recpd)
		                                            <tr>
		                                            	<td>{{++$i_pd}}</td>
		                                            	<td>{{$recpd->nDvtaCB}}</td>               	

		                                            	@if(is_null($recpd->nProAlmCod)==0) 
		                                            		<td>{{$recpd->productoalmacen->producto->nombre_generico.' - '.$recpd->productoalmacen->color->nombre.' ('.$recpd->productoalmacen->producto->nombre_especifico.')'}}</td>
		                                            	@else
		                                            		@if(is_null($recpd->nCodTC)==0) 
				                                                <td>{{$recpd->resStocktelas->producto->nombre_generico.' (TC) '}}</td>
			                                                @else
			                                                	@if($recpd->resStockMP->insumo_id==0)
			                                                		<td>{{$recpd->resStockMP->accesorio->nombre }} (MP)</td>
								                                @else
								                                	<td>{{$recpd->resStockMP->insumo->nombre_generico }} (MP)</td>                
								                                @endif                                      
			                                                @endif	
		                                            	@endif		                                            	
		                                            	<td>{{$recpd->nDevPeso}}</td>
		                                            	<td>{{$recpd->nDevCant}}</td>
		                                            </tr>
		                                            @endforeach		                                            
		                                        </tbody>
		                                    </table>

		                                </div>
		                                <div class="col-md-6">

		                                    <table id="bandeja-pc" name="bandeja-pc" class="table table-striped table-bordered table-hover">
		                                        <thead>
		                                            <th>
		                                                Item
		                                            </th>   
		                                            <th>
		                                                Código
		                                            </th>                                         
		                                            <th>
		                                                Producto
		                                            </th>
		                                            <th>
		                                                Peso
		                                            </th>
		                                            <th>
		                                                Cantidad
		                                            </th>
		                                        </thead>
		                                        <tbody style="text-align: center;">
		                                            <?php $i_pc=0;?>
		                                            @foreach($pc as $recpc)
		                                            <tr>
		                                            	<td>{{++$i_pc}}</td>
		                                            	<td>{{$recpc->nDvtaCB}}</td>
		                                            	@if(is_null($recpc->nProAlmCod)==0) 
		                                            		<td>{{$recpc->productoalmacen->producto->nombre_generico.' - '.$recpc->productoalmacen->color->nombre.' ('.$recpc->productoalmacen->producto->nombre_especifico.')'}}</td>
		                                            	@else
		                                            		@if(is_null($recpc->nCodTC)==0) 
				                                                <td>{{$recpc->resStocktelas->producto->nombre_generico.' (TC) '}}</td>
			                                                @else
			                                                	@if($recpc->resStockMP->insumo_id==0)
			                                                		<td>{{$recpc->resStockMP->accesorio->nombre }} (MP)</td>
								                                @else
								                                	<td>{{$recpc->resStockMP->insumo->nombre_generico }} (MP)</td>                
								                                @endif
			                                                @endif	
		                                            	@endif	                               	
		                                            	<td>{{$recpc->nVtaDevPeso}}</td>
		                                            	<td>{{$recpc->nVtaDevCant}}</td>
		                                            </tr>
		                                            @endforeach			                                            
		                                        </tbody>
		                                    </table>
		                                </div>
		                            </div>
		                            
		                        </div>
		                    </div>
		    			</div><!-- /.box-body -->
			    </div><!--box box-success-->
			<a href="{{ route('notadevolucion.index') }}" class=" signbuttons btn btn-primary">Bandeja</a>
			</section>

		</div>
	</div>
</form>

@endsection

@push('scripts')
<script type="text/javascript">
	/***************************** resumen ************************/

	//producto a devolver

	var indice_tabla3=0;
	function agre_res_proddev(cod,des,peso,cant,codidoenlace,selection)
    { 
        indice_tabla3++;
        $("#conta3").val(indice_tabla3);
        var nuevaFila="<tr id=filadevres_"+indice_tabla3+">";

        nuevaFila+='<td><i class="fa fa-hand-o-right" aria-hidden="true"></i> '+indice_tabla3+"</td>";
        nuevaFila+="<td>"+cod+"</td>";
        nuevaFila+="<td>"+des+"</td>";
        nuevaFila+="<td>"+peso+"</td>";
        nuevaFila+="<td>"+cant+"</td>";

        //nuevaFila+='<td><div class="btn btn-link" onclick="delTabla2('+codidoenlace+','+selection+')" >'+'<a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quitar"></i></a>'+'</div>';
        
        nuevaFila+="</tr>";
        
        $("#bandeja-pd").append(nuevaFila);      
    }

    //producto a cambiar

	var indice_tabla4=0;
	function agre_res_prodcamb(cod,des,peso,cant)
    { 
        indice_tabla4++;
        $("#conta4").val(indice_tabla4);
        var nuevaFila="<tr id=filacanres_"+indice_tabla4+">";

        nuevaFila+='<td><i class="fa fa-hand-o-right" aria-hidden="true"></i> '+indice_tabla4+"</td>";
        nuevaFila+="<td>"+cod+"</td>";
        nuevaFila+="<td>"+des+"</td>";
        nuevaFila+="<td>"+peso+"</td>";
        nuevaFila+="<td>"+cant+"</td>";

        //nuevaFila+='<td><div class="btn btn-link" onclick="delTabla2('+codidoenlace+','+selection+')" >'+'<a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quitar"></i></a>'+'</div>';
        
        nuevaFila+="</tr>";
        
        $("#bandeja-pc").append(nuevaFila);      
    }

/*******************************************************************************/
</script>
@endpush('scripts')