@extends('backend.layouts.appv2')

@section('title', 'BANDEJA DE NOTA DE DEVOLUCIONES')

@section('after-styles')
    <style>
    .dropdown{padding: 0;}
    .dropdown .dropdown-menu{border: 1px solid #999}
    .detallescompra{
        display: none;
        background-color: #ececec;
    }
    </style>
@stop

@section('content')
	
	<div class="row">
		<div class="col-sm-12">
			<section class="content">
			    <div class="box box-info">
			        <div class="box-header with-border">
			            <h3 class="box-title">@yield('title')</h3>
			            <div class="box-tools pull-right">
			                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			            </div><!-- /.box tools -->
			        </div><!-- /.box-header -->
			        <div class="box-body">
			        	<div class="row">
			        		<form action="{{ route('notadevolucion.index') }}" method="GET">
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="GET">
					        	
					        	<div class="col-md-3">
					        		<label>Fecha</label> 
							        <input class="form-control" type="date" name="fech_b" id="fech_b" step="1" min="2000-01-01" value="{{$rq->fech_b}}" tabindex="1">
							    </div>
							    <div class="col-md-2">
							    	<label>T. Documento</label> 
							        <select class="form-control" name="tdoc_b" id="tdoc_b">
								      	<option value="">...</option>
								      	@foreach($tdoc as $t)
								      		@if($t->nTipPagCod==$rq->tdoc_b)
									        	<option value="{{$t->nTipPagCod}}" selected>{{$t->cDescTipPago}}</option>	
									        @else
									        	<option value="{{$t->nTipPagCod}}">{{$t->cDescTipPago}}</option>	
									        @endif
								        @endforeach
								    </select>							     
							    </div>
							    <div class="col-md-2">
							    	<label>Forma de Pago</label> 
							        <select class="form-control" name="fpago_b" id="fpago_b">
								      	<option value="">...</option>
								      	@foreach($fpago as $t)
								      		@if($t->nForPagCod==$rq->fpago_b)
									        	<option value="{{$t->nForPagCod}}" selected>{{$t->cDescForPag}}</option>	
									        @else
									        	<option value="{{$t->nForPagCod}}">{{$t->cDescForPag}}</option>	
									        @endif									        
								        @endforeach
								    </select>						     
							    </div>
							    <div class="col-md-2">
							    	<label>N°G.Venta/Factura</label> 
							        <input type="text" class="form-control" name="ngven_b" id="ngven_b" maxlength="250" tabindex="1" value="{{$rq->ngven_b}}">						     
							    </div>
							    <div class="col-md-2">
							    	<label>RUC/DNI</label> 
							        <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" id="rdni" name="rdni">    
							        	<option data-subtext="" value="">...</option>                       
	                                    @foreach($cli as $u)
	                                    	@if($rq->rdni == $u->nClieCod)
	                                    		<option data-subtext="({{$u->cClieNdoc}})" value="{{$u->nClieCod}}" selected>{{$u->cClieDesc}}</option>
	                                    	@else
	                                    		<option data-subtext="({{$u->cClieNdoc}})" value="{{$u->nClieCod}}">{{$u->cClieDesc}}</option>
	                                    	@endif	  
	                                        
	                                    @endforeach
	                                </select>
							    </div>
							    <div class="col-md-1"> 
							    	<label style="color:white;">dd</label>                                	
                                	<button type="submit" class="btn btn-primary"><span class="fa fa-search" aria-hidden="true"></span></button>
	                            </div>
							</form>       
			            </div>
			            
			            <br>
	                    <br>

						@include('comercializacion.venta.fragment.info')
						@include('comercializacion.venta.fragment.error')
						<table class="table table-hover table-striped">
							<thead>
								<tr>
									<!--th>DETALLES</th-->
									
									<th>FECHA</th>	
									<th>T.DOCUMENTO</th>	
									<th>F.PAGO</th>	
									<th>G.VENTA/FACTURA</th>									
									<th>DNI/RUC</th>
									<th>NOMBRE/RAZÓN SOCIAL</th>
									<!--th>SUB-TOTAL</th-->
									<!--th>IGV</th-->									
									<th>TOTAL VENTA</th>
									<th>MONTO A FAVOR</th>
									<th>MONTO EN CONTRA</th>
									<th>TOTAL</th>
									<!--th>ESTADO</th-->
									<th>Nota de Dev.</th>
									<th colspan="3">ACCIONES</th>
								</tr>
							</thead>
							<tbody style="text-align: center;">
								@foreach($vfacturas as $fac)
								<tr>
									<!--td>
										<a class="btn btn-xs btn-success detalle">
											<i class="fa fa-eye fa-1x" id="ver" aria-hidden="true"></i>
										</a>
									</td-->										
									<td>{{ $fac->dVFacFemi }}</td>	
									<td>{{ $fac->tipodocv->cDescTipPago }}</td>									
									<td>{{ $fac->forpago->cDescForPag }}</td>	
									<td>{{ ($fac->nTipPagCod==3)?$fac->cFacNumFac:$fac->nVtaCod }}</td>								
									<td>{{ $fac->cliente->cClieNdoc }}</td>										
									<td>{{ $fac->cliente->cClieDesc}}</td>
									<!--td>{{ $fac->dVFacSTot }}</td>									
									<td>{{ $fac->dVFacIgv }}</td-->
									<td>{{ number_format($fac->dVFacVTot,2) }}</td>
									<td>{{ number_format($fac->devolucion['dMonFavor'],2) }}</td>
									<td>{{ number_format($fac->devolucion['dMonContra'],2) }}</td>
									<td style="font-weight: bold;">{{ number_format(($fac->dVFacVTot-(($fac->devolucion['dMonFavor']=='')?0:$fac->devolucion['dMonFavor'])+(($fac->devolucion['dMonContra']=='')?0:$fac->devolucion['dMonContra'])),2) }}</td>
									<!--td>{{ ($fac->anulado==1)?'Anulado':'Activo'}}</td-->
									<td  style="font-weight: bold;">{{ $fac->devolucion['cDevCod'] }}</td>									
								
									<td>
										@if($fac->anulado==0)												
											@if($fac->devolucion['cDevCod']=='')
												<a href="{{ route('notadevolucion.create',$fac->nVtaCod) }}" class="btn btn-xs btn-success">
													<i class="fa fa-pencil-square-o" data-toggle="tooltip" data-placement="top" title="" data-original-title="Nota de Dev."></i>
												</a>							
											@else
												<a href="{{ route('notadevolucion.show',$fac->nVtaCod) }}" class="btn btn-xs btn-info">
													<i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver"></i>
												</a>
												<a href="{{ route('notadevolucion.reporte',$fac->nVtaCod) }}" class="btn btn-xs btn-default" target="_blank">
													<i class="fa fa-print" data-toggle="tooltip" data-placement="top" title="" data-original-title="Imprimir"></i>
												</a> 
											@endif											
											
											<!--a data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Anular" data-trans-title="¿Está seguro?" class="btn btn-xs btn-danger" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
												<i class="fa fa-ban" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
												
												<form action="{{ route('venta.update',$fac->nVtaCod) }}" method="POST" name="delete_item" style="display:none">
												   {{ csrf_field() }}
													<input type="hidden" name="_method" value="PUT">
												</form>
											</a-->											
										@else 
											Anulado
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						{!! $vfacturas->render() !!}
						
			        </div><!-- /.box-body -->
			    </div><!--box box-success-->
			</section>
		</div>
	</div>

@endsection

@push('scripts')

<script type="text/javascript">	
    $(document).bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
        	$(location).attr('href',"{{ route('almacen.create') }}");
        };
    });

    /* show / hide order details */
        $(".detalle").click(function() {
          $(this).closest("tr").next().toggle('fast');
          if($("#ver").attr("class") == 'fa fa-eye fa-1x')
            $("#ver").attr("class", "fa fa-eye-slash fa-1x");
          else
            $("#ver").attr("class", "fa fa-eye fa-1x");
        });
</script>

@endpush('scripts')