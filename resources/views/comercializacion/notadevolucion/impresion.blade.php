<style type="text/css">
   .contenedor-tabla
    {
        display: table;
        width: 70%;
        font-size: 10px;
    }

.contenedor-fila
    {
        display: table-row;            
    }

    .contenedor-columna
    {
        display: table-cell;
        padding-top: 1px;  
}


    body{
        /**font-family: monospace;*/
        font-family: "Lucida Console", "Lucida Sans Typewriter", monaco, "Bitstream Vera Sans Mono", monospace; 
    }

    table, td, th {
        border: 1px solid black;
        font-size: 10px;
        text-align: center;
    }

    table {
        border-collapse: collapse;
        width: 83%;    }

    th {
        height: 5px;     }
</style>

<title>Nota de Devolución</title>

<body style="margin-left: 4px;margin-right: 55px;">     
    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;text-align: center;padding-top: 10px;">
                <img src="img/logo.jpg" alt="Smiley face" height="25" width="45">
             </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;padding-top: 20px;padding-left: 10px;">
                <h1 style="margin: 0px;font-size: 10px;">TEXTILES BURGA S.A.C.</h1>
            </div>
            <div class="contenedor-columna" style="width: 20%;text-align: center;">
                
            </div>
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;text-align: center;">
                
             </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;">
                <h3 style="margin: 0px;font-size: 8px;">JR.AMERICA N° 472 LNT. S-104 - LA VICTORIA</h3>
            </div>
            <div class="contenedor-columna" style="width: 20%;text-align: center;">
                
            </div>
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;text-align: center;">
                
             </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;">
                <h3 style="margin: 0px;font-size: 8px;">Telf. 324-1224 Cel. 996342002</h3>             
            </div>
            <div class="contenedor-columna" style="width: 20%;text-align: center;">
                
            </div>
        </div>
    </div>    
    

    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 5%;">
                TIENDA:
             </div>
            <div class="contenedor-columna" style="width: 50%;">
                {{strtoupper($vfacturas->almacen->cAlmNom)}}
            </div>
            <div class="contenedor-columna" style="width: 35%;">
                PROFORMA: {{$vfacturas->nVtaCod}}
            </div>
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna">
                CLIENTE:
             </div>
            <div class="contenedor-columna">
                {{strtoupper($vfacturas->cliente->cClieDesc)}}
            </div>
            <div class="contenedor-columna">
                FECHA: {{substr($vfacturas->dVFacFemi, 0, strpos($vfacturas->dVFacFemi, ' '))}}
            </div>
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna">
                DIRECCION:
             </div>
            <div class="contenedor-columna">
                {{strtoupper($vfacturas->cliente->cClieDirec)}}
            </div>
            <div class="contenedor-columna">
                
            </div>
        </div>
    </div>

<!--div style="clear:both;"></div-->

    
    <div class="row">                                          
        <div class="col_x">
            <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
                <tr>
                    <th width="10px;">
                        ITEM
                    </th>
                    <th width="100px;">
                        DESCRIPCION
                    </th>
                    <th width="40px">   
                        CANT.
                    </th>                                                                
                    <th colspan="2" width="20px">
                        P.UNIT.
                    </th>
                    <th colspan="2" width="20px">
                        IMPORTE
                    </th>
                </tr>
                <?php $i=0; ?>
                @foreach($vfacturas->ventadetalle as $dfac)                                                                             
                    <tr>                                                            
                        <td>{{++$i}}</td>
                        <td style="text-align: left;">{{ $dfac->productoAlmacen->producto->nombre_generico }} {{ $dfac->productoAlmacen->color->nombre }}</td>
                        <td style="text-align: center;">{{ number_format($dfac->nVtaPeso,2) }}</td>
                                                                               
                        <td  style="border-right: 6px solid white;">S/. </td> 
                                                
                        <td style="text-align: right;">{{ number_format($dfac->nVtaPrecioU,2) }}</td> 
                                                   
                        <td  style="border-right: 6px solid white;">S/. </td> 
                                                
                        <td style="text-align: right;">{{ number_format($dfac->nVtaPreST,2) }}</td> 
                    </tr>
                @endforeach 
                <!--
                @for ($j = ++$i; $j <=14; $j++)                    
                    <tr>                      
                        <td>{{$j}}</td>
                        <td></td>
                        <td></td>                        
                                                        
                        <td  style="border-right: 1px solid white;">S/. </td> 
                        
                        <td></td> 
                                                                              
                        <td  style="border-right: 1px solid white;">S/. </td> 
                        
                        <td></td> 
                    </tr>
                @endfor   
                -->
                    <tr>                                                            
                        <td colspan="3" style="border-bottom: 1px solid white;border-left: 1px solid white;text-align: left;">
                            <?php
                                $v=new NumberToLetterConverter();
                                //echo $v->convertNumber(1302587.25,"PEN","decimal");
                            ?>  
                            <label style="margin: 0px;font-size: 9px;">Son: {{$v->convertNumber($vfacturas->dVFacVTot,"PEN","decimal")}}</label>
                        </td>                        
                        <!--td>{{ $dfac->nVtaCant }}</td-->                        
                        <th colspan="2">TOTAL</th> 
                        <td style="border-right: 1px solid white;">S/. </td> 
                        <td style="text-align: right;">{{ number_format($vfacturas->dVFacVTot,2) }}</td> 
                    </tr>                                              
            </table>
        </div>
    </div>
 

    <!--div class="row">
        <div class="col_x">
            <table >
              <tr>
                <th>SUB TOTAL   </th>
                <td>S/. {{$vfacturas->dVFacSTot}}</td>
              </tr>
              <tr>
                <th>IGV</th>
                <td>S/. {{$vfacturas->dVFacIgv}}</td>
              </tr> 
              <tr>
                <th>TOTAL   </th>                                                        
                <td>S/. {{$vfacturas->dVFacVTot}}</td>
              </tr>                                                 
            </table>
        </div>
    </div-->
</body>
