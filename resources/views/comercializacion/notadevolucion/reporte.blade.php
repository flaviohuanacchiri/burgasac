<!--
sI HAY UN PAGO ESTE FIGURARA AQUÍ, HACIENDO EL CALCULO SE LE COBRAR O DEVOLVERÁ LO CORRESPONDIENTE

EJEMPLO
VENTA 50
EVUELVE 35
SALDO 15
PAGO ANTES 20
PAGO>=SALGO 
	SI ES SI 
		-> PAGO=SALDO = 15
		-> DEVUELVE A FAVOR 5
	SI ES NO
		-> SE DEJA EL CALCULO NORMAL
-->
<style type="text/css">
   .contenedor-tabla
    {
        display: table;
        width: 70%;
        font-size: 10px;
    }

.contenedor-fila
    {
        display: table-row;            
    }

    .contenedor-columna
    {
        display: table-cell;
        padding-top: 1px;  
}


    body{
        /**font-family: monospace;*/
        font-family: "Lucida Console", "Lucida Sans Typewriter", monaco, "Bitstream Vera Sans Mono", monospace; 
    }

    table, td, th {
        border: 1px solid black;
        font-size: 10px;
        text-align: center;
    }

    table {
        border-collapse: collapse;
        width: 83%;    }

    th {
        height: 5px;     }
</style>

<title>Nota de Devolución</title>

<body style="margin-left: 4px;margin-right: 55px;">     
    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;text-align: center;padding-top: 10px;">
                <img src="img/logo.jpg" alt="Smiley face" height="25" width="45">
             </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;padding-top: 20px;padding-left: 10px;">
                <h1 style="margin: 0px;font-size: 10px;">TEXTILES BURGA S.A.C.</h1>
            </div>
            <div class="contenedor-columna" style="width: 20%;text-align: center;">
                
            </div>
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;text-align: center;">
                
             </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;">
                <h3 style="margin: 0px;font-size: 8px;">JR.AMERICA N° 472 LNT. S-104 - LA VICTORIA</h3>
            </div>
            <div class="contenedor-columna" style="width: 20%;text-align: center;">
                
            </div>
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;text-align: center;">
                
             </div>
            <div class="contenedor-columna" style="width: 60%;text-align: center;">
                <h3 style="margin: 0px;font-size: 8px;">Telf. 324-1224 Cel. 996342002</h3>             
            </div>
            <div class="contenedor-columna" style="width: 20%;text-align: center;">
                
            </div>
        </div>
    </div>    
    

    <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 5%;">
                TIENDA:
             </div>
            <div class="contenedor-columna" style="width: 50%;">
                {{strtoupper($vfacturas->almacen->cAlmNom)}}
            </div>
            <div class="contenedor-columna" style="width: 35%;">
                TRANSACCIÓN: {{$vfacturas->nVtaCod}}
            </div>
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna">
                CLIENTE:
             </div>
            <div class="contenedor-columna">
                {{strtoupper($vfacturas->cliente->cClieDesc)}}
            </div>
            <div class="contenedor-columna">
                FECHA: {{substr($vfacturas->dVFacFemi, 0, strpos($vfacturas->dVFacFemi, ' '))}}
            </div>
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna">
                DIRECCION:
             </div>
            <div class="contenedor-columna">
                {{strtoupper($vfacturas->cliente->cClieDirec)}}
            </div>
            <div class="contenedor-columna">
                
            </div>
        </div>
    </div>
    <br>


     <div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 50%;">
                <table id="bandeja-pd" name="bandeja-pd" class="table table-striped table-bordered table-hover">
		            <tr>
		                <th colspan="5">
		                    PRODUCTO A DEVOLVER
		                </th>  
		            </tr>
		            <tr>
		                <th>
		                    Item
		                </th>   
		                <th>
		                    Código
		                </th>                                         
		                <th>
		                    Producto
		                </th>
		                <th>
		                    Peso
		                </th>
		                <th>
		                    Cantidad
		                </th>		                                            
		            </tr>
		            	<?php $i_pd=0;?>
		                @foreach($pd as $recpd)
		            <tr>
		                	<td>{{++$i_pd}}</td>
		                	<td>{{$recpd->nDvtaCB}}</td>
                            @if(is_null($recpd->nProAlmCod)==0) 
                                <td>{{$recpd->productoalmacen->producto->nombre_generico.' - '.$recpd->productoalmacen->color->nombre.' ('.$recpd->productoalmacen->producto->nombre_especifico.')'}}</td>
                            @else
                                @if(is_null($recpd->nCodTC)==0) 
                                    <td>{{$recpd->resStocktelas->producto->nombre_generico.' (TC) '}}</td>
                                @else
                                    @if($recpd->resStockMP->insumo_id==0)
                                        <td>{{$recpd->resStockMP->accesorio->nombre }} (MP)</td>
                                    @else
                                        <td>{{$recpd->resStockMP->insumo->nombre_generico }} (MP)</td>                
                                    @endif                                      
                                @endif  
                            @endif  

		                	<td>{{$recpd->nDevPeso}}</td>
		                	<td>{{$recpd->nDevCant}}</td>
		            </tr>
		                @endforeach		                                            
		      
		        </table>
            </div>
            <div class="contenedor-columna" style="width: 50%;">
                <table id="bandeja-pc" name="bandeja-pc" class="table table-striped table-bordered table-hover">
		            <tr>
		                <th colspan="5">
		                    PRODUCTO A CAMBIAR
		                </th>  
		            </tr>
		            <tr>
		                <th>
		                    Item
		                </th>   
		                <th>
		                    Código
		                </th>                                         
		                <th>
		                    Producto
		                </th>
		                <th>
		                    Peso
		                </th>
		                <th>
		                    Cantidad
		                </th>
		            </tr>
		            
		                <?php $i_pc=0;?>
		                @foreach($pc as $recpc)
		            <tr>
		                	<td>{{++$i_pc}}</td>
		                	<td>{{$recpc->nDvtaCB}}</td>		                	
                            @if(is_null($recpc->nProAlmCod)==0) 
                                <td>{{$recpc->productoalmacen->producto->nombre_generico.' - '.$recpc->productoalmacen->color->nombre.' ('.$recpc->productoalmacen->producto->nombre_especifico.')'}}</td>
                            @else
                                @if(is_null($recpc->nCodTC)==0) 
                                    <td>{{$recpc->resStocktelas->producto->nombre_generico.' (TC) '}}</td>
                                @else
                                    @if($recpc->resStockMP->insumo_id==0)
                                        <td>{{$recpc->resStockMP->accesorio->nombre }} (MP)</td>
                                    @else
                                        <td>{{$recpc->resStockMP->insumo->nombre_generico }} (MP)</td>                
                                    @endif
                                @endif  
                            @endif    
		                	<td>{{$recpc->nVtaDevPeso}}</td>
		                	<td>{{$recpc->nVtaDevCant}}</td>
		            </tr>
		                @endforeach			                                            
		            
		        </table>
            </div>            
        </div>
    </div>

    <br>

  	<div class="contenedor-tabla">
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;">
                MONTO A FAVOR:
             </div>
            <div class="contenedor-columna" style="width: 45%;">
                {{$devv->dMonFavor}}
            </div>
            <div class="contenedor-columna" style="width: 35%;">
                
            </div>
        </div>
        <div class="contenedor-fila">
            <div class="contenedor-columna" style="width: 20%;">
                MONTO EN CONTRA:
             </div>
            <div class="contenedor-columna" style="width: 45%;">
                {{$devv->dMonContra}}
            </div>
            <div class="contenedor-columna" style="width: 35%;">
                
            </div>
        </div>
    </div>
    

</body>
