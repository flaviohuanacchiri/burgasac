@extends('backend.layouts.appv2')

@section('subtitulo', 'NOTA DE DEVOLUCIÓN')

@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<style>
  .ui-autocomplete-loading {
    background: white url("../../img/ui-anim_basic_16x16.gif") right center no-repeat;
  }  
</style>
<form action="{{ route('notadevolucion.store') }}" method="POST">
	{{ csrf_field() }}
	<input type="hidden" name="_method" value="POST">	
    <input type="hidden" name="codint" id="codint" value="{{$id}}">
    <input type="hidden" name="conta" id="conta" value="">
    <input type="hidden" name="conta2" id="conta2" value="">
    <input type="hidden" name="conta3" id="conta3" value="">
    <input type="hidden" name="conta4" id="conta4" value="">
    <input type="hidden" name="eliminados" id="eliminados" value="">
    <input type="hidden" name="actualizar" id="actualizar" value="">
    <input type="hidden" name="cad_actt" id="cad_actt" value="">
    <input type="hidden" name="key_stock" id="key_stock" value="0">

    <input type="hidden" name="cod_cliente" id="cod_cliente" value="{{$fac->nClieCod}}">

     <input type="hidden" name="dTotDev" id="dTotDev" value="0">
    <input type="hidden" name="dTotRep" id="dTotRep" value="0">

	<div class="row">
		<div class="col-sm-12">
			<section class="content">
			    <div class="box box-info">
			        <div class="box-header with-border">
			            <h3 class="box-title">@yield('subtitulo')</h3>
			            <div class="box-tools pull-right">
			                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			            </div><!-- /.box tools -->
			        </div><!-- /.box-header -->
				        <div class="box-body">
				            @include('comercializacion.almacen.fragment.info')

				            <div class="row">
			                    <div class="col-md-12">
			                        <div class="panel panel-default">                            
			                            <div class="panel-body">		                                
								                <div class="form-group">
									                <div class="row" style="margin-top: 15px;">
									                	<div class="col-md-3">
									                		<label>Fecha</label>									                    	
									                    	<input class="form-control" type="text" name="fech" id="fech" value="{{ $fac->dVFacFemi }}" disabled>
									                	</div>
									                	<div class="col-md-2">
									                		<label>T. Documento</label>
									                    	<input class="form-control" type="text" name="tdoc_d" id="tdoc_d" value="{{ $fac->tipodocv->cDescTipPago }}" disabled>
									                    </div>
									                    <div class="col-md-2">
									                    	<label>Forma de Pago</label>
									                    	<input class="form-control" type="text" name="fpago_d" id="fpago_d" value="{{ $fac->forpago->cDescForPag }}" disabled>
									                	</div>
									                	<div class="col-md-2">
									                		<label>RUC</label>
									                    	<input class="form-control" type="text" name="ruc_d" id="ruc_d" value="{{ (strlen($fac->cliente->cClieNdoc) == 11)?$fac->cliente->cClieNdoc:'' }}" disabled>
									                	</div>
									                	<div class="col-md-3">
									                		<label>Guía/Factura</label>
									                    	<input class="form-control" type="text" name="gfac_d" id="gfac_d" value="{{ $fac->nVtaCod }}" maxlength="6" disabled>
									                	</div>
									                </div>
									                <div class="row" style="margin-top: 15px;">
									                	<div class="col-md-5">
									                		<label>Nombres</label>
									                    	<input class="form-control" type="text" name="nombre_d" id="nombre_d" placeholder="Nombre" value="{{ $fac->cliente->cClieDesc}}" disabled>			                    	
									                	</div>
									                	<div class="col-md-2">
									                		<label>DNI</label>
									                    	<input class="form-control" type="text" name="dni_d" id="dni_d" placeholder="Dirección" value="{{ (strlen($fac->cliente->cClieNdoc) == 8)?$fac->cliente->cClieNdoc:'' }}" disabled>
									                    </div>
									                    <div class="col-md-5" style="    padding: 6px 0px 0px 20px;">
									                    	<label><input type="radio" id="NDXP" name="notas_d" value="NDXP" checked="checked" > Nota de devolución por cambio</label>
						                                    <label><input type="radio" id="NDXD" name="notas_d" value="NDXD"> Nota de devolución sin cambio</label>
									                	</div>
									                </div>
									            </div>

								                
			                            </div>
			                        </div>
			                    </div>
			                </div>

			                <div class="panel panel-default">
		                        <div class="panel-body">

		                            <div class="row">

		                                <div class="col-md-12">
		                                	<!--div id="cap_ini" name="cap_ini" style="background-color: #00000030; width: 100%; height: 100%; position: absolute; z-index: 10;display: none;"></div-->
		                                    <table id="bandeja-transfer_dev" name="bandeja-transfer_dev" class="table table-striped table-bordered table-hover">
		                                        <thead>
		                                            <th>
		                                                Id
		                                            </th>   
		                                            <th>
		                                                Nota de Devolución
		                                            </th> 
		                                            <th>
		                                                Código
		                                            </th>                                         
		                                            <th>
		                                                Descripción
		                                            </th>
		                                            <th>
		                                                Peso
		                                            </th>
		                                            <th>
		                                                Rollo
		                                            </th>
		                                            <th>
		                                                P.Unit.
		                                            </th>
		                                            <th>
		                                                Subtotal
		                                            </th>
		                                            <th>
		                                                Estado del Producto
		                                            </th>
		                                            <th>
		                                                Acción
		                                            </th>                                       
		                                        
		                                        </thead>
		                                        <tbody style="text-align: center;">
		                                        	<?php  
							                    		/*$vari=(isset($_GET["page"]))?$_GET["page"]:1;
							                    		$cont=($vari-1)*5; */
							                    		$cont=0;
							                    	?>				
		                                            @foreach($dventa as $dv)
		                                            <tr>
		                                            	<td>
		                                            		{{++$cont}}
		                                            	</td>
			                                            <td id="nd_{{$cont}}" name="nd_{{$cont}}">			                                                
			                                                @if($dv->bDevuelto==1)
			                                                	{{$dv->prodevolver->cTipNDev}}
			                                                @endif
			                                            </td> 
			                                            <td>
			                                                {{$dv->nDvtaCB}}
			                                                <input type="hidden" name="nDvtaCB_{{$cont}}" id="nDvtaCB_{{$cont}}" value="{{$dv->nDvtaCB}}">
			                                                <input type="hidden" name="cdetalle_{{$cont}}" id="cdetalle_{{$cont}}" value="{{$dv->nDetVtaCod}}">
			                                                @if(is_null($dv->nProAlmCod)==0) 
				                                                <input type="hidden" name="nProAlmCod_{{$cont}}" id="nProAlmCod_{{$cont}}" value="{{$dv->nProAlmCod}}">
				                                                <!--input type="hidden" name="tcid_{{$cont}}" id="tcid_{{$cont}}" value="0">
				                                                <input type="hidden" name="mpid_{{$cont}}" id="mpid_{{$cont}}" value="0"-->
			                                                @else
			                                                	@if(is_null($dv->nCodTC)==0) 
					                                                <input type="hidden" name="nProAlmCod_{{$cont}}" id="nProAlmCod_{{$cont}}" value="TC{{$dv->nCodTC}}">
					                                                <!--input type="hidden" name="tcid_{{$cont}}" id="tcid_{{$cont}}" value="1">
					                                                <input type="hidden" name="mpid_{{$cont}}" id="mpid_{{$cont}}" value="0"-->
				                                                @else
				                                                	<input type="hidden" name="nProAlmCod_{{$cont}}" id="nProAlmCod_{{$cont}}" value="MP{{$dv->nCodMP}}">
					                                                <!--input type="hidden" name="tcid_{{$cont}}" id="tcid_{{$cont}}" value="0">
					                                                <input type="hidden" name="mpid_{{$cont}}" id="mpid_{{$cont}}" value="1"-->
				                                                @endif
			                                                @endif
			                                            </td>                                 
			                                            @if(is_null($dv->nProAlmCod)==0)        
			                                            <td>
			                                                {{$dv->productoAlmacen->producto->nombre_generico.' '.$dv->productoAlmacen->color->nombre}} ({{$dv->productoAlmacen->producto->nombre_especifico}})
			                                                <input type="hidden" name="prodgen_{{$cont}}" id="prodgen_{{$cont}}" value="{{$dv->productoAlmacen->producto->nombre_generico}}">
			                                                <input type="hidden" name="prodesp_{{$cont}}" id="prodesp_{{$cont}}" value="{{$dv->productoAlmacen->producto->nombre_especifico}}">
			                                            </td>
			                                            @else
			                                            	@if(is_null($dv->nCodTC)==0) 
				                                                <td>
					                                                {{$dv->resStocktelas->producto->nombre_generico}} (TC)
					                                                <input type="hidden" name="prodgen_{{$cont}}" id="prodgen_{{$cont}}" value="{{$dv->resStocktelas->producto->nombre_generico}}">
					                                                <input type="hidden" name="prodesp_{{$cont}}" id="prodesp_{{$cont}}" value="TELAS">
					                                            </td>
			                                                @else
			                                                	<td>
				                                                	@if($dv->resStockMP->insumo_id==0)
				                                                		{{$dv->resStockMP->accesorio->nombre }} (MP)
						                                                <input type="hidden" name="prodgen_{{$cont}}" id="prodgen_{{$cont}}" value="{{$dv->resStockMP->accesorio->nombre}}">
						                                                <input type="hidden" name="prodesp_{{$cont}}" id="prodesp_{{$cont}}" value="TELAS">
									                                @else
									                                	{{$dv->resStockMP->insumo->nombre_generico }} (MP)
						                                                <input type="hidden" name="prodgen_{{$cont}}" id="prodgen_{{$cont}}" value="{{$dv->resStockMP->insumo->nombre_generico}}">
						                                                <input type="hidden" name="prodesp_{{$cont}}" id="prodesp_{{$cont}}" value="TELAS">									                                    
									                                @endif
					                                            </td>
			                                                @endif			                                            
			                                            @endif
			                                            <td>
			                                                {{$dv->nVtaPeso}}
			                                                <input type="hidden" name="peso_{{$cont}}" id="peso_{{$cont}}" value="{{$dv->nVtaPeso}}">
			                                            </td>
			                                            <td>
			                                                {{$dv->nVtaCant}}
			                                                <input type="hidden" name="cantx_{{$cont}}" id="cantx_{{$cont}}" value="{{$dv->nVtaCant}}">
			                                            </td>
			                                            <td>
			                                            	<input type="hidden" name="ppunit_{{$cont}}" id="ppunit_{{$cont}}" value="{{$dv->nVtaPrecioU}}">
			                                                {{$dv->nVtaPrecioU}}
			                                            </td>
			                                            <td>
			                                                {{$dv->nVtaPreST}}
			                                            </td>
			                                            <td>
			                                            	@if($dv->bDevuelto==1)
			                                                	@if($dv->prodevolver->cEstPro =='NDPM')
			                                                		<label><input type="radio" id="NDPM_{{$cont}}" name="estpro_d_{{$cont}}" value="NDPM" checked="checked" disabled>NDPM</label>
						                                    		<label><input type="radio" id="NDPB_{{$cont}}" name="estpro_d_{{$cont}}" value="NDPB" disabled>NDPB</label>
						                                    	@else
						                                    		<label><input type="radio" id="NDPM_{{$cont}}" name="estpro_d_{{$cont}}" value="NDPM" disabled>NDPM</label>
						                                    		<label><input type="radio" id="NDPB_{{$cont}}" name="estpro_d_{{$cont}}" value="NDPB" checked="checked" disabled>NDPB</label>
			                                                	@endif
			                                                @else
			                                                	<label><input type="radio" id="NDPM_{{$cont}}" name="estpro_d_{{$cont}}" value="NDPM" checked="checked">NDPM</label>
						                                    	<label><input type="radio" id="NDPB_{{$cont}}" name="estpro_d_{{$cont}}" value="NDPB">NDPB</label>
			                                                @endif
			                                                
			                                            </td>
			                                            <td>
			                                            	@if($dv->bDevuelto!=1)
			                                                	<a onclick="pasar({{$cont}})" name="btneje_{{$cont}}" id="btneje_{{$cont}}" class="btn btn-xs btn-success">
																	<i class="fa fa-external-link-square"></i>
																</a> 
			                                                @endif			                                                
			                                            </td>  
			                                        </tr>
		                                            @endforeach		                                            
		                                        </tbody>
		                                    </table>
		                                   
		                                </div>
		                            </div>
		                             <div class="row">
		                                <div class="col-md-4">
		                                    <label for="">Importe</label>
		                                    <input id="impo_d" type="text" class="form-control" name="impo_d" value="{{$fac->dVFacSTot}}" readonly>
		                                </div>
		                                <div class="col-md-4">
		                                    <label for="">Igv</label>
		                                    <input id="igv_d" type="text" class="form-control" name="igv_d" value="{{$fac->dVFacIgv}}" readonly>
		                                </div>
		                                <div class="col-md-4">
		                                    <label for="">Total</label>
		                                    <input id="total_d" type="text" class="form-control" name="total_d" value="{{$fac->dVFacVTot}}" readonly>
		                                </div>
		                            </div>
		                            <br>
		                            
		                        </div>
		                    </div>
		    			</div><!-- /.box-body -->
			    </div><!--box box-success-->
			</section>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<section class="content">
			    <div class="box box-info">
			        <div class="box-header with-border">
			            <h3 class="box-title">ESPECIFICACIONES DEL PRODUCTO A DEVOLVER</h3>
			            <div class="box-tools pull-right">
			                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			            </div><!-- /.box tools -->
			        </div><!-- /.box-header -->
			        	<div class="box-body">
				        	<div class="row">
				        		<div class="col-md-6">
				        			<div class="row">
					                    <div class="col-md-12">
					                        <div class="panel panel-default">                            
					                            <div class="panel-body" style="padding: 19px 31px 15px 35px;">
					                                <div class="row">
					                                    <div class="row">
									        				<div class="col-md-6">
									        					Código
									        					<input class="form-control" type="text" name="cod_esppro" id="cod_esppro" value="{{ old('tdoc') }}" maxlength="255" disabled="disabled">
									        				</div>
									        				<div class="col-md-6" style="padding: 15px;">
							                                    @if($manauto==0)        
							                                        <input type="radio" id="radaut0_s" name="codigo0_s" value="0" checked="checked" disabled>
							                                    @else
							                                        <input type="radio" id="radaut0_s" name="codigo0_s" value="0" disabled>
							                                    @endif
							                                    <label for="contactChoice1">Aut.</label>

							                                    @if($manauto==1)        
							                                        <input type="radio" id="radman0_s" name="codigo0_s" value="1" checked="checked" disabled>
							                                    @else
							                                        <input type="radio" id="radman0_s" name="codigo0_s" value="1" disabled>
							                                    @endif
							                                    
							                                    <label for="contactChoice2">Man.</label>
									        				</div>				 
									        			</div>
									        			<div class="row">
									        				<div class="col-md-12">
									        					Producto
									        					<input class="form-control" type="text" name="pro_esppro" id="pro_esppro" value="{{ old('tdoc') }}" maxlength="255" disabled="disabled">
									        				</div>				 
									        			</div>
									        			<div class="row">
									        				<div class="col-md-4">
									        					Peso o Cantidad
									        					<input class="form-control" type="text" name="peso_esppro" id="peso_esppro" value="{{ old('tdoc') }}" maxlength="17" readonly="readonly">
									        				</div>				 
									        				<div class="col-md-4">
									        					Rollos
									        					<input class="form-control" type="text" name="cantz_esppro" id="cantz_esppro" value="{{ old('tdoc') }}" maxlength="17" readonly="readonly">
									        				</div>				 
									        				<div class="col-md-4" style="padding: 15px;">
									        					
									        					<input type="hidden" name="tiponota" id="tiponota" value="">
									        					<input type="hidden" name="estpro_x" id="estpro_x" value="">
									        					<input type="hidden" name="codigoimp" id="codigoimp" value="">
									        					<input type="hidden" name="ppunit" id="ppunit" value="">
									        					<input type="hidden" name="pro_espproesp" id="pro_espproesp" value="">

									        					<input type="hidden" name="comp_peso" id="comp_peso" value="">
									        					<input type="hidden" name="comp_roll" id="comp_roll" value="">

									        					<input type="hidden" name="comp_id" id="comp_id" value="">
									        					<input type="hidden" name="proalmacen_x" id="proalmacen_x" value="">
									        					<input type="hidden" name="subtext" id="subtext" value="">
							                                    <input type="hidden" name="esp" id="esp" value="">
							                                    <input type="hidden" name="valu" id="valu" value="">
									        					

																<div  id="addespprodev" class="btn btn-primary" tabindex="4" onclick="agredardetalle_espprodev()" style="display: none;">agregar</div>
									        				</div>	
									        				<label id="sms_epd" name="sms_epd" style="color:red;"></label>			 
									        			</div>
					                                </div>
					                            </div>
					                        </div>
					                    </div>
					                </div>
				        		</div>
				        		<div class="col-md-6">
				        			<div class="row">
					                    <div class="col-md-12">
					                        <div class="panel panel-default">                            
					                            <div class="panel-body" style="padding: 33px;">
						                            <div class="row">
						                                <div class="col-md-12">
						                                    <br>
						                                    <div id="capa" style="position: absolute;width: 97%;height: 92%;background-color: rgba(19, 19, 19, 0.35);display: none;top: 1px;"></div>
						                                    <table id="bandeja-transfer_epd" name="bandeja-transfer_epd" class="table table-striped table-bordered table-hover">
						                                        <thead>
						                                            <th>
						                                                Item
						                                            </th>   
						                                            <th>
						                                                Código
						                                            </th>                                         
						                                            <th>
						                                                Descripción
						                                            </th>
						                                            <th>
						                                                Peso
						                                            </th>
						                                            <th>
						                                                Cant.
						                                            </th>	
						                                            <th>
						                                                Sub-Tot.
						                                            </th>								                                            
						                                            <th>
						                                                D
						                                            </th>                                            
						                                        
						                                        </thead>
						                                        <tbody style="text-align: center;">
						                                            
						                                            
						                                        </tbody>
						                                    </table>
						                                   
						                                    <ul class="pagination">
						                                      
						                                    </ul>

						                                </div>
						                            </div>
					                            </div>
				                            </div>
				                        </div>
				                    </div>
				                </div>

				        	</div>		    				
		    			</div><!-- /.box-body -->					
			    </div><!--box box-success-->
			</section>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<section class="content">
			    <div class="box box-info">
			        <div class="box-header with-border">
			            <h3 class="box-title">ESPECIFICACIONES DEL PRODUCTO A CAMBIAR</h3>
			            <div class="box-tools pull-right">
			                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			            </div><!-- /.box tools -->
			        </div><!-- /.box-header -->
				        <div class="box-body">
				        	<div id="cap_fin" name="cap_fin" style="background-color: #00000030; width: 100%; height: 80%; position: absolute; z-index: 10;"></div>
				            @include('comercializacion.almacen.fragment.info')

				            <div class="panel panel-default">
		                        <div class="panel-body" id="dina_control" name="dina_control">
		                            <div class="row">
		                                <div class="col-md-2" style="padding-top: 10px;">
		                                    <label for="">Tienda</label>                                    
		                                    <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" id="almprodes" name="almprodes" tabindex="2"> 
		                                            <option data-subtext='(Ubi. {{$almacen->cAlmUbi}})' value='{{$almacen->nAlmCod}}' >{{$almacen->cAlmNom}}</option>
		                                    </select>
		                                </div>

		                                <div class="col-md-3" style="padding-top: 10px;">
		                                    @if($manauto==0)        
		                                        <input type="radio" id="radaut" name="codigo" value="0" checked="checked" disabled>
		                                    @else
		                                        <input type="radio" id="radaut" name="codigo" value="0" disabled>
		                                    @endif
		                                    <label for="contactChoice1">Aut.</label>

		                                    @if($manauto==1)        
		                                        <input type="radio" id="radman" name="codigo" value="1" checked="checked" disabled>
		                                    @else
		                                        <input type="radio" id="radman" name="codigo" value="1" disabled>
		                                    @endif
		                                    
		                                    <label for="contactChoice2">Man.</label>

		                                    <input id="codpro" type="text" class="form-control" name="codpro" value=""  placeholder="Código" maxlength="50" tabindex="3">
		                                </div>                                
		                                
		                                <div class="col-md-3" style="padding-top: 10px;">
		                                    <label for="">Producto</label>                                    
		                                    <!--select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" id="prodes" name="prodes" tabindex="3">
		                                        <option value='' >Seleccione un producto...</option>
		                                        <?php
		                                        	for ($i=0; $i < sizeof($results) ; $i++) 
		                                        	{
		                                        ?> 
		                                        	<option dato-esp='{{$results[$i]["nombre_especifico"]}}' dato-tc='{{$results[$i]["tc"]}}' dato-mp='{{$results[$i]["mp"]}}' dato-cantidad='{{$results[$i]["cantidad"]}}' value='{{$results[$i]["nProAlmCod"]}}'>{{$results[$i]["value"]}}</option>		                                        		
		                                        <?php
		                                        	}
		                                        ?>
		                                    </select-->

                                            <input id="prodes" type="text" class="form-control" name="prodes" value=""  placeholder="Busque el producto.." maxlength="25" tabindex="5">

                                            <input type="hidden" name="subtext" id="subtext" value="">
		                                    <input type="hidden" name="esp" id="esp" value="">
		                                    <input type="hidden" name="valu" id="valu" value="">
                                           
		                                </div>    
		                                <div class="col-md-2" style="padding-top: 10px;border-left: 1px solid #9c9c9c;border-top: 1px solid #9c9c9c;border-bottom: 1px solid #9c9c9c;background-color: #dadada;">
		                                    <label for="">Peso</label>
		                                    <input id="stock" type="text" class="form-control" name="stock" placeholder="0" maxlength="6" disabled style="font-size: 22px;font-weight: bold;background-color: #a4d4e4;">
		                                </div> 
		                                 <div class="col-md-2" style="padding-top: 10px;border-right: 1px solid #9c9c9c;border-top: 1px solid #9c9c9c;border-bottom: 1px solid #9c9c9c;background-color: #dadada;">
		                                    <label for="">Rollos</label>
		                                    <input id="cant_rollo" type="text" class="form-control" name="cant_rollo" placeholder="0" maxlength="6" disabled style="font-size: 22px;font-weight: bold;background-color: #a4d4e4;">
		                                </div>                                                              
		                            </div>

		                            <div class="row">                                
		                                <div class="col-md-2" style="padding-top: 10px;">
		                                    <label for="">Peso  o Cantidad</label>
		                                    <input id="canti_x" type="number" class="form-control" name="canti_x" min="0.00" placeholder="00.00" maxlength="17" tabindex="4" value="" readonly="readonly">
		                                </div>
		                                <div class="col-md-2" style="padding-top: 10px;">
		                                    <label for="">Rollos</label>
		                                    <input id="canti_rollo_i" type="number" class="form-control" name="canti_rollo_i" min="0.00" placeholder="00.00" maxlength="17" tabindex="4" value="" readonly="readonly">
		                                </div>
		                                <div class="col-md-3" style="padding-top: 10px;">
		                                    <label for="">P.Unidad</label>
		                                    <input id="puni_x" type="number" class="form-control" name="puni_x" min="0.00" placeholder="00.00" maxlength="17" tabindex="4" value="" style="font-size: 22px;font-weight: bold;" readonly>
		                                </div>
		                                <div class="col-md-3" style="padding-top: 10px;">
		                                    <label for="">Subtotal</label>
		                                    <input id="subt_x" type="number" class="form-control" name="subt_x" min="0.00" placeholder="00.00" maxlength="17" tabindex="4" value="0.00" style="font-size: 25px;font-weight: bold;background-color: #a4d4e4;" readonly>
		                                </div>

		                                <div class="col-md-2" style="text-align:center;padding-top: 10px;">
		                                    <br>
		                                    <div  id="add" class="btn btn-primary" tabindex="4" onclick="agredardetalle()">Agregar</div>                                    
		                                </div>
		                                                                
		                            </div>
		                        </div>
		                    </div>

			                <div class="panel panel-default">
		                        <div class="panel-body">
		                            <div class="row">
		                                <div class="col-md-12">
		                                    <br>
		                                    <div id="capa" style="position: absolute;width: 97%;height: 92%;background-color: rgba(19, 19, 19, 0.35);display: none;top: 1px;"></div>
		                                    <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
		                                        <thead>
		                                            <th>
		                                                Id
		                                            </th>   
		                                            <th>
		                                                Código
		                                            </th>                                         
		                                            <th>
		                                                Descripción
		                                            </th>
		                                            <th>
		                                                Peso
		                                            </th>
		                                            <th>
		                                                Rollo
		                                            </th>
		                                            <th>
		                                                P.Unit.
		                                            </th>
		                                            <th>
		                                                Subtotal
		                                            </th>
		                                            <th>
		                                                Quitar
		                                            </th>                                            
		                                        
		                                        </thead>
		                                        <tbody style="text-align: center;">
		                                            
		                                            
		                                        </tbody>
		                                    </table>
		                                   
		                                    <ul class="pagination">
		                                      
		                                    </ul>

		                                </div>
		                            </div>
		                        
		                            <div class="row" style="text-align: center;font-size: 15px;">
		                                <div class="col-md-6">
		                                    <label style="color:red;text-align: ">Monto A favor</label>
		                                    <input id="mfavor" type="text" class="form-control" name="mfavor" value="0.00" readonly>
		                                </div>
		                                <div class="col-md-6">
		                                    <label style="color:red;text-align: ">Monto en Contra</label>
		                                    <input id="mcontra" type="text" class="form-control" name="mcontra" value="0.00" readonly>
		                                </div>
		                            </div>
		                            <br>
		                            
		                        </div>
		                    </div>

		                    <div class="panel panel-default">
		                        <div class="panel-body">
		                        	<div class="row" style="text-align: center;font-size: 15px;">
		                                <div class="col-md-6">
		                                    <label style="text-align:center; ">Producto a Devolver</label>
		                                    
		                                </div>
		                                <div class="col-md-6">
		                                    <label style="text-align:center; ">Producto a Cambiar</label>
		                                   
		                                </div>
		                            </div>
		                            <br>
		                            <div class="row">
		                                <div class="col-md-6">
		                                    <table id="bandeja-pd" name="bandeja-pd" class="table table-striped table-bordered table-hover">
		                                        <thead>
		                                            <th>
		                                                Item
		                                            </th>   
		                                            <th>
		                                                Código
		                                            </th>                                         
		                                            <th>
		                                                Producto
		                                            </th>
		                                            <th>
		                                                Peso
		                                            </th>
		                                            <th>
		                                                Cantidad
		                                            </th>		                                            
		                                        </thead>
		                                        <tbody style="text-align: center;">
		                                            
		                                            
		                                        </tbody>
		                                    </table>

		                                </div>
		                                <div class="col-md-6">

		                                    <table id="bandeja-pc" name="bandeja-pc" class="table table-striped table-bordered table-hover">
		                                        <thead>
		                                            <th>
		                                                Item
		                                            </th>   
		                                            <th>
		                                                Código
		                                            </th>                                         
		                                            <th>
		                                                Producto
		                                            </th>
		                                            <th>
		                                                Peso
		                                            </th>
		                                            <th>
		                                                Cantidad
		                                            </th>
		                                        </thead>
		                                        <tbody style="text-align: center;">
		                                            
		                                            
		                                        </tbody>
		                                    </table>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		    			</div><!-- /.box-body -->
			    </div><!--box box-success-->
			    <button type="submit" class=" signbuttons btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button> <a href="{{ route('notadevolucion.index') }}" class=" signbuttons btn btn-primary">Bandeja</a>
			</section>

		</div>
	</div>
</form>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!--h5 class="modal-title" id="exampleModalLabel">Lotes</h5-->
        <button type="button" class="close" data-dismiss="modal" onclick="Limpiar();" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">        
        <select class="form-control" id="lote_TC" name="lote_TC"> 
            <option value='0' >Elegir el Lote...</option>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="Limpiar();" data-dismiss="modal">Close</button>
        <!--button type="button" class="btn btn-primary" onclick="Limpiar()" data-dismiss="modal">Save changes</button-->
      </div>
    </div>
  </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
     var indice_tabla=0;
    var key_enter=true;
    var cant_items_real=0;
    var indice_act=1;
    var ultima_pag=1;
    var cantxpag=7;
    var tabla=[];
    var exigecli=0;
    var cont_item_bd =0;

    $(document).ready(function() {
        $("form").keypress(function(e) {
            if (e.which == 13) {
                return false;
            }
        });

        doTheClock();

        $('#dniruc').keyup(function (){      
          this.value = (this.value + '').replace(/[^0-9]/g, '');
        });
        
        $('#canti').numeric(",").numeric({decimalPlaces: 2,negative: false});

        $('#peso_esppro').numeric(",").numeric({decimalPlaces: 2,negative: false});
        $('#cantz_esppro').numeric(",").numeric({decimalPlaces: 2,negative: false});

        /********************** PAGINACIÓN **************************/
        cant_items_real=cont_item_bd;    
        /*** cargar lista de p/ginas***/
        controlpaginacion(cantxpag,cant_items_real);        
        /*** cargar paginación inicial ***/
        paginacion(1,cantxpag);
    });

    function doTheClock() 
    {
       window.setTimeout( "doTheClock()", 1000 );
       /*var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
       var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");*/

       t = new Date();
       
       if(document.all || document.getElementById)
       {
          /*var cad = diasSemana[t.getDay()] + ", " + t.getDate() + " de " + meses[t.getMonth()] + " del " + t.getFullYear() + ' ' + t.getHours()+':'+t.getMinutes()+':'+t.getSeconds();*/
          var dia=(t.getDate())+"";
          var mes=(t.getMonth()+1)+"";
          //alert(mes+"");
          $("#fecp").val(t.getFullYear()+"-"+((mes.length==1)?"0"+mes:mes)+"-"+((dia.length==1)?"0"+dia:dia));
          $("#horp").val(t.getHours()+':'+t.getMinutes()+':'+t.getSeconds());
       }else
       {   
          self.status = diasSemana[t.getDay()] + ", " + t.getDate() + " de " + meses[t.getMonth()] + " del " + t.getFullYear() + ' ' + t.getHours()+':'+t.getMinutes()+':'+t.getSeconds();
       }
    }

    function doscerosdecimal(num)
    {
        array=num.split('.');
        if(array.length>1)
        {
            if(array[1].length==1)
                num=num+"0";            
            else if(array[1].length==0)
                num=num+".00";  
        }
        else
            num=num+".00";  

        return num; 

    }  

    $('#sv').on( 'click', function() {
        if( $(this).is(':checked') ){
            // Hacer algo si el checkbox ha sido seleccionado
            $("#key_stock").val("1");
        } else {
            // Hacer algo si el checkbox ha sido deseleccionado
            //alert("El checkbox con valor " + $(this).val() + " ha sido deseleccionado");
            $("#key_stock").val("0");
        }
    });
    /************************** Codigodebarras *************************************/
    $("#codpro").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            //codpro_xx=$.trim($("#codpro").val());
            codpro_xx=$.trim($("#codpro").val()).split("'").join("-");

            if(codpro_xx!="")
            {
                $.ajax({
                  type: "GET",
                  url: "../veri_codbarra/"+codpro_xx+"/"+$("#almprodes").val(),
                  //data: "5454",
                  dataType: "json",
                  error: function(){
                    alert("error petición ajax");
                    //agredardetalle();
                  },
                  beforeSend: function(){
                       $("#loaderDiv").show();
                   },
                  success: function(data){     
                    if(data!="")//data[0].ning_id
                    {   
                        //prod 
                        var key=false;   

                            //$("#prodes option[value="+ data[0].nProAlmCod +"]").attr("selected",true);

                            $("#prodes").val(data[0].nombre_generico+" | "+data[0].nombre);
                            var auxi=((typeof(tabla[data[0].nProAlmCod]) === "undefined")?data[0].nProdAlmStock:data[0].nProdAlmStock-tabla[data[0].nProAlmCod]).toFixed(2);
                         
		                    $("#stock").val(auxi);                            
		                    $("#cant_rollo").val(isNaN(parseFloat(auxi)/20)?0:parseFloat(auxi)/20);

                            //$("#stock").val(data[0].nProdAlmStock);                            
                            //$("#cant_rollo").val(parseFloat(data[0].nProdAlmStock)/20);                            
                            $("#puni_x").val(data[0].nProdAlmPuni);

	                        /*$("#subtext").val(data[0].nombre);
	                        $("#esp").val(data[0].nombre_especifico);*/
	                        $("#valu").val(data[0].nProAlmCod);

                            //evaluar si es pero o rollo y boquearlo
                            var puni_xx=parseFloat(data[0].nProdAlmPuni);
                            if(data[0].nombre_especifico =="TELAS")
                            {
                                $("#canti_x").val(data[0].peso_cant);
                                $("#canti_rollo_i").val(parseFloat(data[0].peso_cant)/20);
                                //$("#canti_x").prop( "readonly", false );
                                $("#canti_x").prop( "readonly", true );
                                $("#canti_rollo_i").prop( "readonly", true );
                                $("#canti_x").focus();
                                $("#subt_x").val((puni_xx*parseFloat(data[0].peso_cant)).toFixed(2));
                            }
                            else
                            {
                                $("#canti_x").val("0");
                                $("#canti_rollo_i").val(data[0].rollo);
                                //$("#canti_rollo_i").prop( "readonly", false );
                                $("#canti_rollo_i").prop( "readonly", true );
                                $("#canti_x").prop( "readonly", true );
                                $("#canti_rollo_i").focus();
                                $("#subt_x").val((puni_xx*parseFloat(data[0].rollo)).toFixed(2));
                            }    
                            
                            //agredardetalle();

                            if(auxi>0)
                            {                            	
                                @if($manauto==0) 
                                    agredardetalle();
                                @endif
                                
                            }
                        
                        //$('#prodes').selectpicker('refresh');
                       
                    }
                    else
                    {
                        alert("No existe este código de barras: "+codpro_xx); 
                        limpiarcontroles();                  
                    }
                    $("#loaderDiv").hide();
                  }
                });
            }
            else
            {
                $("#prodes").val("");  
                $("#prodes").focus();                
            }
        }
        else if( e.which == 27 ) 
        {
        	limpiarcontroles();  
        }
    });
    /************************** Tipo documento *************************************/
    $("input[name=tipdoc]").click(function () {    
        //$('input:radio[name=tipdoc]:checked').val();
        var tdoc=$(this).val();   

        switch(tdoc)
        {
            case "2": $("#radcre").prop("disabled", true);exigecli=8; break;
            case "3": $("#radcre").prop("disabled", true);exigecli=11; break;
            default: $("#radcre").prop("disabled", false);exigecli=0;
        }     
    });
    /************************** Tipo documento *************************************/
    $("input[name=pago]").click(function () {    
        //$('input:radio[name=tipdoc]:checked').val();
        //alert($(this).val());
        var tdoc=$(this).val();   

        switch(tdoc)
        {
            case "1": 
            $("#banco").val("");
            $("#ncheq").val("");
            $("#nope").val("");

            $("#banco").prop("readonly", true);
            $("#ncheq").prop("readonly", true);
            $("#nope").prop("readonly", true);
            break;
            case "2":             
            $("#banco").val("");
            $("#ncheq").val("");
            $("#nope").val("");
            $("#banco").prop("readonly", false);
            $("#ncheq").prop("readonly", true);
            $("#nope").prop("readonly", false);
            break;
            case "3": 
            $("#radguia").prop( "checked", true );
            $("#banco").val("");
            $("#ncheq").val("");
            $("#nope").val("");

            $("#banco").prop("readonly", true);
            $("#ncheq").prop("readonly", true);
            $("#nope").prop("readonly", true); 
            break;
            case "4":             
            $("#banco").val("");
            $("#ncheq").val("");
            $("#nope").val("");

            $("#banco").prop("readonly", false);
            $("#ncheq").prop("readonly", false);
            $("#nope").prop("readonly", false); 
            break;
            //default: $("#radcre").prop("disabled", false);exigecli=0;
        } 
    });   
    /************************** fin: sacando stock del combo productos ******************/
    $tc_venta=true;
    $("#prodes").on('keyup', function(){
        var value = $(this).val();   
        $( "#prodes" ).autocomplete(
        {        	
          source: "../autocomplete_producto/"+$("#almprodes option:selected").val()+"/"+value,
          minLength: 1,
          select: function( event, ui ) 
          {

            $( "#log" ).scrollTop( 0 );  
            var cad_xyz=ui.item.nProAlmCod+"";
            
            if(cad_xyz.substring(0, 2)=="TC")
            {         
                $("#lote_TC").empty();

                $.get("../combo_lotes/"+ui.item.nProAlmCod.substring(2),function(response)
                {
                    $("#lote_TC").append("<option value='0' >Elegir el Lote...</option>");  
                    for(i=0;i<response.length;i++)
                    {
                        $("#lote_TC").append("<option value='"+response[i].id+"' cant='"+response[i].cantidad+"' roll='"+response[i].rollos+"' esp='TELAS'  > "+response[i].nro_lote+"</option>");
                    }
                }); 

                $('#exampleModal').modal('show');                    
                $tc_venta=false;
            }
            else if(cad_xyz.substring(0, 2)=="MP")
            { 
                traer_producto_mp(ui.item.nProAlmCod,"TELAS",ui.item.nombre,ui.item.cantidad,ui.item.rollos);
                $tc_venta=false;
            }
            else
            {                    
                traer_producto(ui.item.nProAlmCod,ui.item.nombre_especifico,ui.item.nombre);
                $tc_venta=true;
            }
            
          }
        });    
       
    }).keyup();

    function traer_producto(valu,esp,color){
        //alert(valu);
        //alert(esp);
        //alert(color);
        $("#stock").val("0");        
        $("#canti_x").val("");
        $("#cant_rollo").val("");
        //$("#unit_x").val("0");
        $("#puni_x").val("0");            
        $("#subt_x").val("0.00"); 
        $("#canti_x").prop( "readonly", true );
        $("#codpro").val();

        $("#subtext").val(color);
        $("#esp").val(esp);
        $("#valu").val(valu);
           
        //Verifica si ingreso el usuario para jalar precios relacionados anteriormente
        if($.trim($("#cod_cliente").val())=="")
        {
            $("#dniruc").css("border","2px solid #f00");     
            $("#resultado").html("- Primero ingrese DNI|RUC\n");   
            $("#dniruc").focus(); 
        }

        $.get("../prostock/"+valu+"/"+$("#cod_cliente").val(),function(response)
        {  
        	//alert("stock: "+response.nProdAlmStock);
            var auxi=((typeof(tabla[valu]) === "undefined")?response.nProdAlmStock:response.nProdAlmStock-tabla[valu]).toFixed(2);

            if(esp=="TELAS")
            {
                $("#stock").val(auxi);
                $("#stock").css("border","1px solid #d2d6de");    
                //calcular rollo
                $("#cant_rollo").val((parseFloat(auxi)/20).toFixed(2));
                $("#cant_rollo").css("border","1px solid #d2d6de"); 
            }
            else
            {
                //alert(auxi);
                //peso =0
                $("#stock").val("1");
                $("#stock").css("border","1px solid #d2d6de");  
                //asignar rollo
                $("#cant_rollo").val(auxi);
                $("#cant_rollo").css("border","1px solid #d2d6de"); 
            }

            $("#puni_x").val(response.nProdAlmPuni);
            

            if(auxi==0){
                $("#canti_x").prop( "readonly", true );      
            }
            else
            {
                if(esp=="TELAS")
                {
                    $("#canti_x").prop( "readonly", false );  
                    $("#canti_x").focus();
                }
                else
                {
                    $("#canti_rollo_i").prop( "readonly", false );  
                    $("#canti_rollo_i").focus();   
                }
            }

        });
    }

    function Limpiar()
    {
        $("#prodes").val("");
    }
/*
    function traer_producto_TC(valu,esp,color,cant_x,roll_x){
      
        $("#stock").val("0");        
        $("#canti_x").val("");
        $("#cant_rollo").val("");
        //$("#unit_x").val("0");
        $("#puni_x").val("0");            
        $("#subt_x").val("0.00"); 
        $("#canti_x").prop( "readonly", true );
        $("#codpro").val();

        $("#subtext").val(color);
        $("#esp").val(esp);
        $("#valu").val(valu);
           
        var cant_w=0;
        var roll_w=0;
        
            var auxi=0;

            if(typeof(tabla[valu]) === "undefined")
            {
                if(esp=="TELAS")
                {
                    auxi=cant_x.toFixed(2);
                }
                else
                {
                    auxi=roll_x.toFixed(2);
                }                
            }
            else
            {
                if(esp=="TELAS")
                {
                    auxi=(cant_x-tabla[valu]).toFixed(2);
                }
                else
                {
                    auxi=(roll_x-tabla[valu]).toFixed(2)
                }
            }
            
            if(esp=="TELAS")
            {
                $("#stock").val(auxi);
                $("#stock").css("border","1px solid #d2d6de");    
                //calcular rollo
                $("#cant_rollo").val(roll_x);
                $("#cant_rollo").css("border","1px solid #d2d6de"); 
                
            }
            else
            {
                //alert(auxi);
                //peso =0
                $("#stock").val("1");
                $("#stock").css("border","1px solid #d2d6de");  
                //asignar rollo
                $("#cant_rollo").val(auxi);
                $("#cant_rollo").css("border","1px solid #d2d6de"); 
                
            }

            //$("#puni_x").val(puni_x);
            $("#exampleModal").on('hidden.bs.modal', function () {                            
                $("#puni_x").prop( "readonly", false );  
            }); 
            
            if(auxi==0)
                $("#canti_x").prop( "readonly", true );      
            else
            {
                if(esp=="TELAS")
                {
                    $("#canti_x").prop( "readonly", false );  
                    $("#exampleModal").on('hidden.bs.modal', function () {                            
                        $("#canti_x").focus();
                    }); 
                    
                }
                else
                {
                    $("#canti_rollo_i").prop( "readonly", false );  
                    $("#exampleModal").on('hidden.bs.modal', function () {                            
                        $("#canti_rollo_i").focus();
                    }); 
                       
                }
            }

        //});
    }
*/
    function traer_producto_mp(valu,esp,color,cant_x,roll_x){
      
        $("#stock").val("0");        
        $("#canti_x").val("");
        $("#cant_rollo").val("");
        //$("#unit_x").val("0");
        $("#puni_x").val("0");            
        $("#subt_x").val("0.00"); 
        $("#canti_x").prop( "readonly", true );
        $("#codpro").val();

        $("#subtext").val(color);
        $("#esp").val(esp);
        $("#valu").val(valu);
           
        var cant_w=0;
        var roll_w=0;        
        var auxi=0;

        if(typeof(tabla[valu]) === "undefined")
        {
            if(esp=="TELAS")
            {
                auxi=cant_x.toFixed(2);
            }
            else
            {
                auxi=roll_x.toFixed(2);
            }                
        }
        else
        {
            if(esp=="TELAS")
            {
                auxi=(cant_x-tabla[valu]).toFixed(2);
            }
            else
            {
                auxi=(roll_x-tabla[valu]).toFixed(2)
            }
        }
        
        if(esp=="TELAS")
        {
            $("#stock").val(auxi);
            $("#stock").css("border","1px solid #d2d6de");    
            //calcular rollo
            $("#cant_rollo").val((parseFloat(auxi)/20).toFixed(2));
            //$("#cant_rollo").val(roll_x);
            $("#cant_rollo").css("border","1px solid #d2d6de"); 
            
        }
        else
        {
            //alert(auxi);
            //peso =0
            $("#stock").val("1");
            $("#stock").css("border","1px solid #d2d6de");  
            //asignar rollo
            $("#cant_rollo").val(auxi);
            $("#cant_rollo").css("border","1px solid #d2d6de"); 
            
        }

        //$("#puni_x").val(puni_x);
        //$("#exampleModal").on('hidden.bs.modal', function () {                            
            $("#puni_x").prop( "readonly", false );  
        //}); 
        
        if(auxi==0)
            $("#canti_x").prop( "readonly", true );      
        else
        {
            if(esp=="TELAS")
            {
                $("#canti_x").prop( "readonly", false );  
                //$("#exampleModal").on('hidden.bs.modal', function () {                            
                    $("#canti_x").focus();
                //}); 
                
            }
            else
            {
                $("#canti_rollo_i").prop( "readonly", false );  
                //$("#exampleModal").on('hidden.bs.modal', function () {                            
                    $("#canti_rollo_i").focus();
                //}); 
                   
            }
        }

        //});
    }


/*
    $("#prodes").change(function(event){
        
        $("#stock").val("0");        
        $("#canti_x").val("");
        $("#cant_rollo").val("");
        //$("#unit_x").val("0");
        $("#puni_x").val("0");            
        $("#subt_x").val("0.00"); 
        $("#canti_x").prop( "readonly", true );
        $("#codpro").val();
        
        var TC=$("#prodes option:selected").attr("dato-tc");
        var MP=$("#prodes option:selected").attr("dato-mp");
        if(MP=="1")
        {
        	//alert(event.target.value);
        	$tc_venta=false;
        	traer_producto_MP(event.target.value,"TELAS","",parseFloat($("#prodes option:selected").attr("dato-cantidad")),0);        	
        }
        else
        {
	        if(TC=="0")
	        {
		        //$("#subtext").val(color);
		        $("#esp").val($("#prodes option:selected").attr("dato-esp"));
		        $("#valu").val($("#prodes option:selected").val());
	        	$.get("../prostock/"+event.target.value+"/"+$("#cod_cliente").val(),function(response,alm_id){            
		            //alert(response.nProdAlmStock);
		            var auxi=((typeof(tabla[event.target.value]) === "undefined")?response.nProdAlmStock:response.nProdAlmStock-tabla[event.target.value]).toFixed(2);

		            if($("#prodes option:selected").attr("dato-esp")=="TELAS")
		            {
		                $("#stock").val(auxi);
		                $("#stock").css("border","1px solid #d2d6de");    
		                //calcular rollo
		                $("#cant_rollo").val((parseFloat(auxi)/20).toFixed(2));
		                $("#cant_rollo").css("border","1px solid #d2d6de"); 
		            }
		            else
		            {
		                //peso =0
		                $("#stock").val("0");
		                $("#stock").css("border","1px solid #d2d6de");  
		                //asignar rollo
		                $("#cant_rollo").val(auxi);
		                $("#cant_rollo").css("border","1px solid #d2d6de"); 
		            }

		            $("#puni_x").val(response.nProdAlmPuni);
		            

		            if(auxi==0)
		                $("#canti_x").prop( "readonly", true );      
		            else
		            {
		                if($("#prodes option:selected").attr("dato-esp")=="TELAS")
		                {
		                    $("#canti_x").prop( "readonly", false );  
		                    $("#canti_x").focus();
		                }
		                else
		                {
		                    $("#canti_rollo_i").prop( "readonly", false );  
		                    $("#canti_rollo_i").focus();   
		                }
		            }
		            $tc_venta=true;
		        });
		        
			}      
	        else if(TC=="1")
	        { 
	        	$("#lote_TC").empty();
	            $.get("../combo_lotes/"+event.target.value,function(response)
	            {
		            $("#lote_TC").append("<option value='0' >Elegir el Lote...</option>");  
		            for(i=0;i<response.length;i++)
		            {
		                $("#lote_TC").append("<option value='"+response[i].id+"' cant='"+response[i].cantidad+"' roll='"+response[i].rollos+"' esp='"+$("#prodes option:selected").attr("dato-esp")+"'  > "+response[i].nro_lote+"</option>");
		            }
		            $('#exampleModal').modal('show');
		            $tc_venta=false;
		        } );
		        
	 		}
	 	}  
       
    });
*/
    $("#lote_TC").change(function()
    {
        if($(this).val()!="0")
        {            
            traer_producto_TC("TC"+$(this).val(),
                $("#lote_TC option:selected").attr("esp"),
                "sin color",
                parseFloat($("#lote_TC option:selected").attr("cant")),
                parseFloat($("#lote_TC option:selected").attr("roll")));
                //parseFloat($("#lote_TC option:selected").attr("puni"))); 

            $('#exampleModal').modal('hide');
        }
        
    }); 

    function Limpiar()
    {
        $("#prodes").val("");
    }

    function traer_producto_TC(valu,esp,color,cant_x,roll_x){
      	/*
        $("#stock").val("0");        
        $("#canti_x").val("");
        $("#cant_rollo").val("");
        //$("#unit_x").val("0");
        $("#puni_x").val("0");            
        $("#subt_x").val("0.00"); 
        $("#canti_x").prop( "readonly", true );
        $("#codpro").val();*/

        //$("#subtext").val(color);
        $("#esp").val(esp);
        $("#valu").val(valu);
           
        //Verifica si ingreso el usuario para jalar precios relacionados anteriormente
        /*if($.trim($("#cod_cliente").val())=="")
        {
            $("#dniruc").css("border","2px solid #f00");     
            $("#resultado").html("- Primero ingrese DNI|RUC\n");   
            $("#dniruc").focus(); 
        }*/

        //$.get("prostock/"+valu+"/"+$("#cod_cliente").val(),function(response,alm_id){   
       // alert("arriba: "+$("#esp").val());
        var cant_w=0;
        var roll_w=0;
        
            var auxi=0;
            /*alert(valu);
            alert(tabla[valu]);*/
            if(typeof(tabla[valu]) === "undefined")
            {
                if(esp=="TELAS")
                {
                    auxi=cant_x.toFixed(2);
                }
                else
                {
                    auxi=roll_x.toFixed(2);
                }                
            }
            else
            {
                if(esp=="TELAS")
                {
                    auxi=(cant_x-tabla[valu]).toFixed(2);
                }
                else
                {
                    auxi=(roll_x-tabla[valu]).toFixed(2)
                }
            }
            
            if(esp=="TELAS")
            {
                $("#stock").val(auxi);
                $("#stock").css("border","1px solid #d2d6de");    
                //calcular rollo
                $("#cant_rollo").val(roll_x);
                $("#cant_rollo").css("border","1px solid #d2d6de"); 
                
            }
            else
            {
                //alert(auxi);
                //peso =0
                $("#stock").val("1");
                $("#stock").css("border","1px solid #d2d6de");  
                //asignar rollo
                $("#cant_rollo").val(auxi);
                $("#cant_rollo").css("border","1px solid #d2d6de"); 
                
            }

            //$("#puni_x").val(puni_x);
            $("#exampleModal").on('hidden.bs.modal', function () {                            
                $("#puni_x").prop( "readonly", false );  
            }); 
            
            if(auxi==0)
                $("#canti_x").prop( "readonly", true );      
            else
            {
                if(esp=="TELAS")
                {
                    $("#canti_x").prop( "readonly", false );  
                    $("#exampleModal").on('hidden.bs.modal', function () {                            
                        $("#canti_x").focus();
                    }); 
                    
                }
                else
                {
                    $("#canti_rollo_i").prop( "readonly", false );  
                    $("#exampleModal").on('hidden.bs.modal', function () {                            
                        $("#canti_rollo_i").focus();
                    }); 
                       
                }
            }

        //});

    }  
/*
    function traer_producto_MP(valu,esp,color,cant_x,roll_x){
      
        //$("#subtext").val(color);
        $("#esp").val(esp);
        $("#valu").val(valu);
           
        //Verifica si ingreso el usuario para jalar precios relacionados anteriormente
      

        //$.get("prostock/"+valu+"/"+$("#cod_cliente").val(),function(response,alm_id){   
       // alert("arriba: "+$("#esp").val());
        var cant_w=0;
        var roll_w=0;
        
            var auxi=0;
          
            if(typeof(tabla[valu]) === "undefined")
            {
                if(esp=="TELAS")
                {
                    auxi=cant_x.toFixed(2);
                }
                else
                {
                    auxi=roll_x.toFixed(2);
                }                
            }
            else
            {
                if(esp=="TELAS")
                {
                    auxi=(cant_x-tabla[valu]).toFixed(2);
                }
                else
                {
                    auxi=(roll_x-tabla[valu]).toFixed(2)
                }
            }
            
            if(esp=="TELAS")
            {
                $("#stock").val(auxi);
                $("#stock").css("border","1px solid #d2d6de");    
                //calcular rollo
                $("#cant_rollo").val((parseFloat(auxi)/20).toFixed(2));
                $("#cant_rollo").css("border","1px solid #d2d6de"); 
                
            }
            else
            {
                //alert(auxi);
                //peso =0
                $("#stock").val("1");
                $("#stock").css("border","1px solid #d2d6de");  
                //asignar rollo
                $("#cant_rollo").val(auxi);
                $("#cant_rollo").css("border","1px solid #d2d6de"); 
                
            }

            //$("#puni_x").val(puni_x);
            //$("#exampleModal").on('hidden.bs.modal', function () {                            
                $("#puni_x").prop( "readonly", false );  
            //}); 
            
            if(auxi==0)
                $("#canti_x").prop( "readonly", true );      
            else
            {
                if(esp=="TELAS")
                {
                    $("#canti_x").prop( "readonly", false );  
                    //$("#exampleModal").on('hidden.bs.modal', function () {                            
                        $("#canti_x").focus();
                    //}); 
                    
                }
                else
                {
                    $("#canti_rollo_i").prop( "readonly", false );  
                    //$("#exampleModal").on('hidden.bs.modal', function () {                            
                        $("#canti_rollo_i").focus();
                    //}); 
                       
                }
            }

        //});

    }  */

     $("#puni_x").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            agredardetalle(); 
            //$("#puni_x").attr("readonly", true);
            //$('body,html').animate({scrollTop : 500}, 0);
            //calcularsubtotal(num);
        }
        else if ( e.which == 27 ) 
        {
            //limpiarcontroles();
            if ($("#esp").val()=="TELAS")
                $("#canti_x").focus();
            else
                $("#canti_rollo_i").focus();
        }
    });

    $("#puni_x").on('keyup', function(){
        var value = $(this).val(); 

        var valuenum = ($("#esp").val()=="TELAS")?$("#canti_x").val():$("#canti_rollo_i").val();    

        if(typeof(value) !== "undefined" && value!="" && typeof(valuenum) !== "undefined" && valuenum!="")
        {            
            var unimed=parseFloat(value);                   
            var can=parseFloat(valuenum);       
            var sub=(can*unimed);
            $("#subt_x").val(sub.toFixed(2)); 
        }
        else{         
            $("#subt_x").val("0.00"); 
        }     
    }).keyup();


    $("#canti_x").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            if($tc_venta)            
            {
                agredardetalle(); 
                //$('body,html').animate({scrollTop : 500}, 0);
                //calcularsubtotal(num);
            }
            else
            {
                $("#puni_x").focus();
                $("#puni_x").val("");
            }            
        }
        else if ( e.which == 27 ) 
        {
            limpiarcontroles();
            $("#codpro").focus();
        }
    });

    $("#canti_x").on('keyup', function(){
        var value = $(this).val();    
        var pun=$("#puni_x").val();        

        if(typeof(value) !== "undefined" && value!="" && typeof(pun) !== "undefined" && pun!="" )
        {
            calcularsubtotal(value);
            $("#canti_rollo_i").val(calcularrollo(parseFloat(value)));
        }
        else{         
            $("#subt_x").val("0.00"); 
        }     
    }).keyup();

    $("#canti_rollo_i").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            if($tc_venta)            
            {
                agredardetalle(); 
                //$('body,html').animate({scrollTop : 500}, 0);
                //calcularsubtotal(num);
            }
            else
            {
                $("#puni_x").focus();
                $("#puni_x").val("");
            }              
        }
        else if ( e.which == 27 ) 
        {
            limpiarcontroles();
            $("#codpro").focus();
        }
    });

    $("#canti_rollo_i").on('keyup', function(){
        var value = $(this).val();            
        var pun=$("#puni_x").val();        

        if(typeof(value) !== "undefined" && value!="" && typeof(pun) !== "undefined" && pun!="" )
            calcularsubtotal(value);
        else{         
            $("#subt_x").val("0.00"); 
        }
    }).keyup();

    function calcularsubtotal(num)
    {

        //var cp=$("#medida_general").val();   
        //var uni=parseFloat($("#puni_x").val()); 
        //$("#canti_x").val(uni);

        var unimed=parseFloat($("#puni_x").val());        
        var can=parseFloat(num);
        //var desc=parseFloat($("#descu_"+id).val());
        //if(uni!=0)
        //{
            //if(can%uni==0)
            //{
                /*$.get("ofermedida/"+cp,function(response,alm_id)
                {  
                    //calcular_totales(parseFloat($("#stotal_"+id).val()),"dec");
                    var unican=unimed*can;
                    $("#unit_x").val(unican);

                    var p_uni_aux=((100-parseFloat(response.dMedProPor))/100)*parseFloat(response.dProdPpubl);
                    $("#puni_x").val(p_uni_aux.toFixed(2));
                    */
                    var sub=(can*unimed);
                    $("#subt_x").val(sub.toFixed(2));                     
                    //calcular_totales(sub,"inc");
                //})
            /*}
            else{
                if(typeof(uni) !== "undefined")
                {
                    alert("Adv!: La cantidad "+can+", debe ser multiplo de "+uni+" unidades, según la medida.");
                    //$("#resultado").html("Procesando, espere por favor...");
                    $("#canti_x").val("");
                    $("#unit_x").val("0");
                    $("#puni_x").val("0");            
                    $("#subt_x").val("0.00"); 
                }
            }*/
        /*}
         else
        {
            //calcular_totales(parseFloat($("#stotal_"+id).val()),"dec");
            $("#canti_x").val("0");
            $("#puni_x").val("0");            
            $("#subt_x").val("0.00"); 
            //incdesstock($("#proalm_"+id).val(),sub,"dec");
        }*/
    }   
    /***********************combo anidado almacén producto ***********************/
    $("#almprodes").change(function(event){
        $("#prodes").empty();
        $('#prodes').selectpicker('refresh');
        //alert(event.target.value);
        $.get("../productoalm/"+event.target.value,function(response,alm_id){
            $("#prodes").append("<option value='0' >Seleccione producto...</option>");  
            for(i=0;i<response.length;i++){
                $("#prodes").append("<option dato-esp='"+response[i].nombre_especifico+"' dato-tc='"+response[i].tc+"' dato-mp='"+response[i].mp+"' dato-cantidad='"+response[i].cantidad+"' value='"+response[i].nProAlmCod+"' > "+response[i].value+"</option>");                      
            }

        $("#stock").val("0");
        $('#prodes').selectpicker('refresh');                 
        })
    });
    /*******************************************************************************/

    $("#canti").bind('keydown',function(e){
        if ( e.which == 13 ) 
            agredardetalle();        
    });


    var sum_epk=0;

    function agredardetalle()
    {
        if(validacion()==0)
        {
            // Obtenemos el total de columnas (tr) del id "tabla"  
            indice_tabla++;
            $("#conta").val(indice_tabla);
            var nuevaFila="<tr id=fila_"+indice_tabla+">";
            nuevaFila+="<td>"+'<input type="hidden" name="cod_ndi_'+indice_tabla+'" id="cod_ndi_'+indice_tabla+'" value="0">'+'<i class="fa fa-hand-o-right" aria-hidden="true"></i> '+indice_tabla+"</td>";

            nuevaFila+="<td>"+'<input type="hidden" name="codb_'+indice_tabla+'" id="codb_'+indice_tabla+'" value="'+$("#codpro").val()+'">'+$("#codpro").val()+"</td>";

            nuevaFila+="<td style='font-size: 14px;width: 150px;'>"+'<input type="hidden" name="proalm_'+indice_tabla+'" id="proalm_'+indice_tabla+'" value="'+$("#valu").val()+'">'+$("#valu").val()+"</td>";

            nuevaFila+="<td><input type='hidden'  name='canti_"+indice_tabla+"' id='canti_"+indice_tabla+"' value='"+$("#canti_x").val()+"'>"+$("#canti_x").val()+"</td>";

             nuevaFila+="<td>"+'<input type="hidden" name="rollo_'+indice_tabla+'" id="rollo_'+indice_tabla+'" value="'+$("#canti_rollo_i").val()+'"><p id="Lpar_'+indice_tabla+'">'+(($("#prodes option:selected").attr("dato-esp")=="TELAS")?calcularrollo(parseFloat($("#canti_x").val())):$("#canti_rollo_i").val())+"</p></td>";

            /*nuevaFila+="<td><input type='hidden' name='puni_"+indice_tabla+"' id='puni_"+indice_tabla+"' value='"+$("#puni_x").val()+"'  style='width: 70px;'' maxlength='11' readonly>"+$("#puni_x").val()+"</td></td>";*/

             nuevaFila+="<td><input type='text' name='puni_"+indice_tabla+"' id='puni_"+indice_tabla+"' value='"+parseFloat($("#puni_x").val()).toFixed(2)+"'  style='width: 70px;'' maxlength='11' onkeyup=recalsubtotal("+indice_tabla+",'"+$("#prodes option:selected").attr("dato-esp")+"')></td>";
            
            //nuevaFila+="<td><input type='hidden' name='stotal_"+indice_tabla+"' id='stotal_"+indice_tabla+"' value='"+$("#subt_x").val()+"' readonly>"+$("#subt_x").val()+"</td>";

            nuevaFila+="<td><input type='hidden' name='stotal_"+indice_tabla+"' id='stotal_"+indice_tabla+"' value='"+parseFloat($("#subt_x").val()).toFixed(2)+"' readonly><label id='stotalx_"+indice_tabla+"'>"+parseFloat($("#subt_x").val()).toFixed(2)+"</label></td>";

            nuevaFila+='<td><div class="btn btn-link" onclick="delTabla('+indice_tabla+')" >'+'<a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quitar"></i></a>'+'</div>';
            
            nuevaFila+="</tr>";
            
            $("#bandeja-transfer").append(nuevaFila);
            //calcular_totales(parseFloat($("#subt_x").val()),"inc");

            $('#puni_'+indice_tabla).numeric(",").numeric({decimalPlaces: 3,negative: false});           

            incdesstock($("#valu").val(),((parseFloat($("#canti_x").val())!=0)?parseFloat($("#canti_x").val()):parseFloat($("#canti_rollo_i").val())),"inc");
            
            sum_epk+=parseFloat($("#subt_x").val());
            $("#dTotRep").val(sum_epk);

            agre_res_prodcamb($("#codpro").val(),$("#prodes").val(),$("#canti_x").val(),(($("#prodes option:selected").attr("dato-esp")=="TELAS")?calcularrollo(parseFloat($("#canti_x").val())):$("#canti_rollo_i").val()));

            limpiarcontroles();   
                   
			calcularfavor();            

            // paginación   
            cant_items_real++;            
            // cargar lista de p/ginas
            controlpaginacion(cantxpag,cant_items_real);        
            // cargar paginación inicial            
            paginacion(ultima_pag,cantxpag);
            
        }
    }

    function recalsubtotal(indice,prod)
    {       
    //alert(prod); 
        var auxsal=0;
        if(prod=="TELAS"){
            auxsal=(parseFloat($("#canti_"+indice).val())*parseFloat($("#puni_"+indice).val()));
        }
        else
            auxsal=(parseFloat($("#rollo_"+indice).val())*parseFloat($("#puni_"+indice).val()));   
        auxsal=(isNaN(auxsal)?0:auxsal);
        var ant=parseFloat($("#stotal_"+indice).val());
        var act=auxsal;
        $("#stotal_"+indice).val(auxsal.toFixed(2));
        //alert(act-ant);

        sum_epk+=(act-ant);
        $("#dTotRep").val(sum_epk)
        calcularfavor();            

        //calcular_totales((act-ant),"inc");

        $("#stotalx_"+indice).empty();
        $("#stotalx_"+indice).html(auxsal.toFixed(2));
        //altualizar el subtotal
        //actualizar totales
    }


    function calcularfavor()
    {
    	var prueba=sum_epk-sum_epd;
        if(prueba>=0){
        	$("#mfavor").val("0");
        	$("#mcontra").val(prueba.toFixed(2));
        }
        else{
        	$("#mcontra").val("0");
        	$("#mfavor").val((prueba*-1).toFixed(2));
        }
    }

    function incdesstock(indice,valor,inc_dec)
    {
        //alert("valor: "+indice);
        if(typeof(tabla[indice]) === "undefined"){//si no existe agregar
            if(inc_dec=="inc")
                tabla[indice]=valor;
            else
                tabla[indice]=-valor;
        }
        else//si existe (incrementar/decrementar)
        {
            if(inc_dec=="inc")
                tabla[indice]+=valor;
            else
                tabla[indice]-=valor;
        }
    }

    function calcular_totales(importe_bruto_ax,inc_dec)
    {
        var importe_bruto=parseFloat($("#impo").val());
        var timpuesto=parseFloat($("#igv").val());
        var ipagar=parseFloat($("#total").val());

        if(inc_dec=="inc")
        { 
            importe_bruto+=importe_bruto_ax;            
            $("#impo").val(importe_bruto.toFixed(2));

            timpuesto+=importe_bruto_ax*0.18;
            $("#igv").val(timpuesto.toFixed(2));
            
            ipagar+=(importe_bruto_ax+(importe_bruto_ax*0.18));
            $("#total").val(ipagar.toFixed(2));
            $("#soles_x").val("S/."+ipagar.toFixed(2));
            $("#dolar_x").val("$/."+(ipagar/parseFloat($("#cambio").val())).toFixed(2));
        }
        else
        {
            importe_bruto-=importe_bruto_ax;            
            $("#impo").val(importe_bruto.toFixed(2));

            timpuesto-=importe_bruto_ax*0.18;
            $("#igv").val(timpuesto.toFixed(2));
            
            ipagar-=(importe_bruto_ax+(importe_bruto_ax*0.18));
            $("#total").val(ipagar.toFixed(2));
            $("#soles_x").val("S/."+ipagar.toFixed(2));
            $("#dolar_x").val("$/."+(ipagar*parseFloat($("#cambio").val())).toFixed(2));
        }
    }  

    function puni_calculo(id)
    {
        var cp=$("#medi_"+id).val();        
        var uni=parseFloat($("#medi_"+id+" option:selected").attr("data-uni")); 
        var unimed=parseFloat($("#medi_"+id+" option:selected").attr("data-unimed"));        
        var can=parseFloat($("#canti_"+id).val());
        var desc=parseFloat($("#descu_"+id).val());
        
        if(uni!=0)
        {
            if(can/uni>=1)
            {
                $.get("../ofermedida/"+cp,function(response,alm_id)
                {  
                    calcular_totales(parseFloat($("#stotal_"+id).val()),"dec");

                    var p_uni_aux=((100-parseFloat(response.dMedProPor))/100)*parseFloat(response.dProdPpubl);
                    $("#puni_"+id).val(p_uni_aux.toFixed(2));
                    //var sub=(p_uni_aux*can)-desc;
                    var sub=(p_uni_aux*can*unimed);
                    $("#stotal_"+id).val(sub.toFixed(2));                     
                    calcular_totales(sub,"inc");
                })
            }
            else
                alert("Adv!: La cantidad "+can+", es menor que "+uni+" unidades.");
        }
        else
        {
            calcular_totales(parseFloat($("#stotal_"+id).val()),"dec");

            $("#puni_"+id).val("0");            
            $("#stotal_"+id).val("0.00"); 
            //incdesstock($("#proalm_"+id).val(),sub,"dec");
        }   
    }

    function limpiarcontroles()
    {
        $("#prodes option[value=0]").attr("selected",true); 
        $("#almprodes").change();
        $("#prodes").change();
        
        $("#canti_x").prop( "readonly", true );
        $("#canti_rollo_i").prop( "readonly", true );
        $("#puni_x").prop( "readonly", true );
        $("#canti_rollo_i").val("0");
        $("#puni_x").val("0");
        $("#prodes").focus(); 

        $("#stock").val("0.00"); 
        $("#cant_rollo").val("0.00");                
        $("#canti_x").val("0");
        //$("#unit_x").val("0");
        $("#puni_x").val("0");            
        $("#subt_x").val("0.00");
        $("#codpro").val("");
        $("#prodes").val("");
        $("#codpro").focus();  

        $("#subtext").val("");
        $("#esp").val("");
        $("#valu").val("");

    }

    function calcularrollo(num)
    {        
        var num_ceros=num/20;        
        return num_ceros.toFixed(2);
    } 

    function delTabla(id)
    {     
        
        //if(confirm('Nota: El registro se retirará momentaneamente, pero el cambio no se hará efecto hasta que usted guarde los cambios.'))
        //{
            var datos= $("#eliminados").val()+","+$("#cod_ndi_"+id).val();
            $("#eliminados").val(datos);

            //------- para actualizar valores en el control ---
            //incdesstock($("#prod_"+id).val(),((parseFloat($("#peso_"+id).val())!=0)?parseFloat($("#peso_"+id).val()):parseFloat($("#rollo_"+id).val())) ,"des");
            //calcular_totales(parseFloat($("#stotal_"+id).val()),"dec");

            incdesstock($("#proalm_"+id).val(),((parseFloat($("#canti_"+id).val())!=0)?parseFloat($("#canti_"+id).val()):parseFloat($("#rollo_"+id).val())),"dec");

            sum_epk-=parseFloat($("#stotal_"+id).val());
            $("#dTotRep").val(sum_epk);

            limpiarcontroles();
            // fin controles-----------------------------------

            calcularfavor();
            
            $("#fila_"+id).remove();  

            $("#filacanres_"+id).remove();  
		    indice_tabla4--;
		    $("#conta4").val(indice_tabla4);

            cant_items_real--;
            // cargar lista de p/ginas
            controlpaginacion(cantxpag,cant_items_real);        
            // cargar paginación inicial 
            paginacion(indice_act,cantxpag);
        //}            
    }

    function validacion()
    {
        var stock_=parseFloat($.trim($("#stock").val())); 
        var stock_rollo=parseFloat($.trim($("#cant_rollo").val()));     
        var sb_=parseFloat($.trim($("#subt_x").val()));
        var canti_=parseFloat($.trim($("#canti_x").val()));
        var rollo_=parseFloat($.trim($("#canti_rollo_i").val()));
        var key_error=false;
        
        if(sb_<=0)
        {
            $("#subt_x").css("border","2px solid #f00");                
            key_error=true;
        }
        else
            $("#subt_x").css("border","1px solid #d2d6de");

        
        if(stock_<=0)
        {
            $("#stock").css("border","2px solid #f00");                
            key_error=true;
        }
        else
            $("#stock").css("border","1px solid #d2d6de");


        if($("#prodes option:selected").attr("dato-esp")=="TELAS")
        {
            if(isNaN(canti_))
            {
                $("#canti_x").css("border","2px solid #f00");   
                $("#canti_x").focus();             
                key_error=true;
            }
            else
            {
                if(canti_<=0)
                {
                    $("#canti_x").css("border","2px solid #f00");   
                    $("#canti_x").focus();  
                    alert("Peso no puede ser menor o igual a 0");           
                    key_error=true;
                }
                else
                {
                    if(canti_>stock_)
                    {
                        $("#canti_x").css("border","2px solid #f00"); 
                        //$("#unit_x").css("border","2px solid #f00"); 
                          
                        $("#canti_x").focus();             
                        alert("cantidad no puede ser mayor que el stock");
                        key_error=true;
                    }
                    else{
                        $("#canti_x").css("border","1px solid #d2d6de");
                        //$("#unit_x").css("border","1px solid #d2d6de"); 
                    }
                }
            }
        }
        else
        {
            if(isNaN(rollo_))
            {
                $("#canti_rollo_i").css("border","2px solid #f00");   
                $("#canti_rollo_i").focus();             
                key_error=true;
            }
            else
            {
                if(rollo_<=0)
                {
                    $("#canti_rollo_i").css("border","2px solid #f00");   
                    $("#canti_rollo_i").focus();  
                    alert("Rollos no puede ser menor o igual a 0");           
                    key_error=true;
                }
                else
                {
                    if(rollo_>stock_rollo)
                    {
                        $("#canti_rollo_i").css("border","2px solid #f00"); 
                        //$("#unit_x").css("border","2px solid #f00"); 
                          
                        $("#canti_rollo_i").focus();             
                        alert("cantidad no puede ser mayor que el stock");
                        key_error=true;
                    }
                    else{
                        $("#canti_rollo_i").css("border","1px solid #d2d6de");
                        //$("#unit_x").css("border","1px solid #d2d6de"); 
                    }
                }
            }
        }

        return (key_error)?1:0;
    }

    function controlpaginacion(pag,total)
    {
        var cad='';
        var act=true;
        var i;
        for(i=1;i<=total/pag;i++)
        {

            if(act)
                cad='<li onclick="paginacion('+i+','+pag+')" style="cursor: pointer;"><span>«</span></li>';

            cad+='<li '+((act)?'class="active"':'')+' onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            act=false;
        }
        if(total%pag>0)
        {
            cad+='<li onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            cad+='<li onclick="paginacion('+(i)+','+pag+');" style="cursor: pointer;"><span>»</span></li>';
        }
        else
            cad+='<li onclick="paginacion('+(--i)+','+pag+')" style="cursor: pointer;"><span>»</span></li>';

        ultima_pag=i;

        $(".pagination").empty();
        $(".pagination").html(cad);
    }

    function paginacion(pag,de)
    {   
        // ocultar registros de 10 en 10 

        var fin=pag*de;;
        var ini=(pag-1)*de;


        //meter todo en un array, con indice ordenado                        
        var array_items=[];
        var contador=0;
        for (var i = 1; i <= parseInt($("#conta").val()); i++) 
        {                
            var nomfila="fila_"+i;
            if ( $("#"+nomfila).length > 0 ) 
            {
                array_items[contador]=nomfila;
                contador++;
            }                
        }
        //recorrer segun los limites mostrar lo necesario

        for(var i=0;i<contador;i++)
        {
            if(i>=ini && i<fin)
                $("#"+array_items[i]).show();        
            else
                $("#"+array_items[i]).hide();
        }

        indice_act=pag;//guarada el indice de pág

        for(i=1;i<=ultima_pag;i++)
            $("#pgitem_"+i).removeAttr('class');

        $("#pgitem_"+indice_act).attr('class','active');

        return 1;
    }
    /********************************* FIN: TABLA ***************************************/

    $("#efec").bind('keydown',function(e){
        if( e.which == 13 ) 
        {
            cobrar(); 
            //calcularsubtotal(num);
        }
    });

    function cobrar()
    {   
        if(parseFloat($("#vuel").val())>=0 && parseFloat($("#efec").val())>=parseFloat($("#p_tot").val()))
        {
            $("#venta").submit(); 
            //$("#obs_vuel").empty();
        }
        else
        {
            $("#vuel").css("border","2px solid #f00");                 
            $("#obs_vuel").html("¡El pago es menor al costo..!");
        }
    }

    function totales()
    {
        var dr_=$.trim($("#cod_cliente").val());
        var forpago=$('input:radio[name=pago]:checked').val();
         key_error=false;
         $("#obs_pro2").empty();

        if(forpago=="2")
        {
            if($("#banco").val()=="")
            {
                $("#banco").css("border","2px solid #f00");   
                $("#banco").focus();    
                key_error=true;
            }
            else
                $("#banco").css("border","1px solid #d2d6de");

            if($("#nope").val()=="")
            {
                $("#nope").css("border","2px solid #f00");                
                $("#nope").focus();
                key_error=true;
            }
            else
                $("#nope").css("border","1px solid #d2d6de");
        }
        else if(forpago=="4")
        {
            if($("#banco").val()=="")
            {
                $("#banco").css("border","2px solid #f00");   
                $("#banco").focus();    
                key_error=true;
            }
            else
                $("#banco").css("border","1px solid #d2d6de");

            if($("#nope").val()=="")
            {
                $("#nope").css("border","2px solid #f00");                
                $("#nope").focus();
                key_error=true;
            }
            else
                $("#nope").css("border","1px solid #d2d6de");

            if($("#ncheq").val()=="")
            {
                $("#ncheq").css("border","2px solid #f00");                
                $("#ncheq").focus();
                key_error=true;
            }
            else
                $("#ncheq").css("border","1px solid #d2d6de");

        }

        if(!key_error)
        {
            if(dr_!="")
            {
                if($.trim($("#dniruc_fijo").val()).length==exigecli || exigecli==0)
                {
                    $("#obs_pro").empty();
                    $("#dniruc").css("border","1px solid #d2d6de");                   
                    //$("#dniruc_fijo").css("border","1px solid #d2d6de");                   

                    $("#p_imp").val($("#impo").val());
                    $("#p_igv").val($("#igv").val());
                    $("#p_tot").val($("#total").val());

                    if(parseFloat($("#p_tot").val())>0)
                    {                                       
                        $('#cobro').modal("show");
                        $("#cobro").on('shown.bs.modal', function () {                            
                            $('#efec').focus();
                        });                       
                    }
                    else
                        alert("No ha seleccionado ningún producto!");    
                }
                else
                {
                    
                    switch($('input:radio[name=tipdoc]:checked').val())
                    {
                        case "2":$("#obs_pro").html("El tipo de documento BOLETA exige un cliente con DNI (8 dígitos)");break;
                        case "3":$("#obs_pro").html("El tipo de documento FACTURA exige un cliente con RUC (11 dígitos)");break;
                    }
                    $('body,html').animate({scrollTop : 0}, 500);
                    $("#dniruc").css("border","2px solid #f00"); 

                }                
            }
            else{
                $("#obs_pro").html("Debe buscar el cliente.");
                //alert("Falta ingresar el cliente");
                $("#dniruc").css("border","2px solid #f00"); 
                //$("#dniruc_fijo").css("border","2px solid #f00");   
                $('body,html').animate({scrollTop : 0}, 500);                        
                $("#dniruc").focus();            
            }
        }
        else
        {
            $('body,html').animate({scrollTop : 0}, 500);
            $("#obs_pro2").html("Debe ingresar los campos marcados...");
        }
    }

    function limpiarcobro()
    {
        $("#obs_pro").empty();
        $("#dniruc").css("border","1px solid #d2d6de");                   
        $("#dniruc_fijo").css("border","1px solid #d2d6de");                   

        $("#p_imp").val("");
        $("#p_igv").val("");
        $("#p_tot").val("");
        $("#efec").val("");
        $("#vuel").val("0.00");
        $("#subtext").val("");
        $("#esp").val("");
        $("#valu").val("");
           
    }

    $("#efec").on('keyup', function(){
            var value = $(this).val().length;            
            var resto=parseFloat($(this).val())-parseFloat($("#p_tot").val());
            $("#vuel").val(resto.toFixed(1));

            //$("obs_vuel").html(value);
        }).keyup();

    /****************************** hora ****************/
    var selec_actual=0;
    function pasar(id)
    {
    	selec_actual=id;
    	//cod_esppro
    	//prodesp_
    	//$("#btneje_"+id).attr("class","btn btn-xs btn-warning");
    	$("#nd_"+id).html($('input:radio[name=notas_d]:checked').val());
    	$("#btneje_"+id).hide();
    	//$("#cap_ini").show();

    	$("#cod_esppro").val($("#nDvtaCB_"+id).val());

		$("#pro_esppro").val($("#prodgen_"+id).val()+" ("+$("#prodesp_"+id).val()+")");
		$("#pro_espproesp").val($("#prodesp_"+id).val());

		$("#peso_esppro").val($("#peso_"+id).val());
		$("#cantz_esppro").val($("#cantx_"+id).val());

		$("#comp_peso").val($("#peso_"+id).val());
		$("#comp_roll").val($("#cantx_"+id).val());

		$("#comp_id").val(id);
		$("#proalmacen_x").val($("#nProAlmCod_"+id).val());

		$("#NDPM_"+id).prop("disabled",true);
		$("#NDPB_"+id).prop("disabled",true);

		if($("#prodesp_"+id).val()=="TELAS"){
			$("#cantz_esppro").prop( "readonly",true);
			$("#peso_esppro").prop( "readonly",false);		
			$("#peso_esppro").focus();
		}
		else{
			$("#peso_esppro").val("0");
			$("#peso_esppro").prop( "readonly",true);		
			$("#cantz_esppro").prop( "readonly",false);		
			$("#cantz_esppro").focus();
		}

		$("#tiponota").val($('input:radio[name=notas_d]:checked').val());
		$("#estpro_x").val($('input:radio[name=estpro_d_'+id+']:checked').val());
		$("#codigoimp").val($("#cdetalle_"+id).val());
		$("#ppunit").val($("#ppunit_"+id).val());

		$("#addespprodev").show();
		$('body,html').animate({scrollTop : 500}, 0);
    }

    /*$("#peso_esppro").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
        	agredardetalle_espprodev();
        }});

    $("#cantz_esppro").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
        	agredardetalle_espprodev();
        }});*/

    var indice_tabla2=0;
    var sum_epd=0;

    function agredardetalle_espprodev()
    {
        if(validacion_epd())
        {
        	$("#sms_epd").empty();
            // Obtenemos el total de columnas (tr) del id "tabla"  
            indice_tabla2++;
            $("#conta2").val(indice_tabla2);
            var nuevaFila="<tr id=filadev_"+indice_tabla2+">";
            nuevaFila+='<td><i class="fa fa-hand-o-right" aria-hidden="true"></i> '+indice_tabla2+"</td>";

            nuevaFila+="<td>"+'<input type="hidden" name="cod_ndid_'+indice_tabla2+'" id="cod_ndid_'+indice_tabla2+'" value="0">'
            +'<input type="hidden" name="cod_epd_'+indice_tabla2+'" id="cod_epd_'+indice_tabla2+'" value="'+$("#codigoimp").val()+'">'
            +'<input type="hidden" name="tiponota_'+indice_tabla2+'" id="tiponota_'+indice_tabla2+'" value="'+$("#tiponota").val()+'">'
            +'<input type="hidden" name="estpro_x_'+indice_tabla2+'" id="estpro_x_'+indice_tabla2+'" value="'+$("#estpro_x").val()+'">'
            +'<input type="hidden" name="ppunit_x_'+indice_tabla2+'" id="ppunit_x_'+indice_tabla2+'" value="'+$("#ppunit").val()+'">'
            +'<input type="hidden" name="proalmacen_x_'+indice_tabla2+'" id="proalmacen_x_'+indice_tabla2+'" value="'+$("#proalmacen_x").val()+'">'
            +'<input type="hidden" name="id_x_'+indice_tabla2+'" id="id_x_'+indice_tabla2+'" value="'+$("#comp_id").val()+'">'
            +'<input type="hidden" name="codespro_'+indice_tabla2+'" id="codespro_'+indice_tabla2+'" value="'+$("#cod_esppro").val()+'">'
            //+'<input type="hidden" name="codigoimp_'+indice_tabla2+'" id="codigoimp_'+indice_tabla2+'" value="'+$("#codigoimp").val()+'">'
            +$("#cod_esppro").val()+"</td>";

            nuevaFila+="<td>"+'<input type="hidden" name="nameespe_'+indice_tabla2+'" id="nameespe_'+indice_tabla2+'" value="'+$("#pro_espproesp").val()+'">'+$("#pro_esppro").val()+"</td>";

            nuevaFila+="<td>"+'<input type="hidden" name="peso2_'+indice_tabla2+'" id="peso2_'+indice_tabla2+'" value="'+parseFloat($("#peso_esppro").val()).toFixed(2)+'">'+parseFloat($("#peso_esppro").val()).toFixed(2)+"</td>";
            nuevaFila+="<td>"+'<input type="hidden" name="roll2_'+indice_tabla2+'" id="roll2_'+indice_tabla2+'" value="'+parseFloat($("#cantz_esppro").val()).toFixed(2)+'">'+parseFloat($("#cantz_esppro").val()).toFixed(2)+"</td>";

            var stotal_xx=parseFloat(parseFloat($("#ppunit").val())*parseFloat(($("#pro_espproesp").val() == 'TELAS')?$("#peso_esppro").val():$("#cantz_esppro").val())).toFixed(2);

            nuevaFila+="<td>"+'<input type="hidden" name="st_'+indice_tabla2+'" id="st_'+indice_tabla2+'" value="'+stotal_xx+'">'+stotal_xx+"</td>";

            nuevaFila+='<td><div class="btn btn-link" onclick="delTabla2('+indice_tabla2+','+selec_actual+')" >'+'<a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quitar"></i></a>'+'</div>';
            
            nuevaFila+="</tr>";
            
            $("#bandeja-transfer_epd").append(nuevaFila);

            sum_epd+=parseFloat(stotal_xx);
            //alert(sum_epd);
            $("#dTotDev").val(sum_epd);
                        
            agre_res_proddev($("#cod_esppro").val(),$("#pro_esppro").val(),parseFloat($("#peso_esppro").val()).toFixed(2),parseFloat($("#cantz_esppro").val()).toFixed(2),indice_tabla2,selec_actual);             

            limpiar_epd();   

            calcularfavor(); 
        }
        else
        	$("#sms_epd").html("El peso o la cantidad es mayor a lo vendido.");
    }

    function delTabla2(id,check_id)
    {   
        $superid=$("#id_x_"+id).val();
        $("#btneje_"+$superid).show();
        $("#nd_"+$superid).empty();
        $("#NDPM_"+check_id).prop("disabled",false);
		$("#NDPB_"+check_id).prop("disabled",false);


        sum_epd-=parseFloat($("#st_"+id).val());

        $("#dTotDev").val(sum_epd);

        $("#filadev_"+id).remove(); 

        $("#filadevres_"+id).remove();  
        indice_tabla3--;
        $("#conta3").val(indice_tabla3);

        calcularfavor();
    }

    function validacion_epd()
    {
        var peso_esppro=parseFloat($.trim($("#peso_esppro").val())); 
        var cantz_esppro=parseFloat($.trim($("#cantz_esppro").val()));     

        var peso_esppro_aux=parseFloat($.trim($("#comp_peso").val())); 
        var cantz_esppro_aux=parseFloat($.trim($("#comp_roll").val()));     
      
        var key_error=true;
        
        if(peso_esppro<0 || peso_esppro>peso_esppro_aux)
        {
            $("#peso_esppro").css("border","2px solid #f00");                            
            key_error=false;
        }
        else
            $("#peso_esppro").css("border","1px solid #d2d6de");

        
        if(cantz_esppro<0 || cantz_esppro>cantz_esppro_aux)
        {
            $("#cantz_esppro").css("border","2px solid #f00");                
            key_error=false;
        }
        else
            $("#cantz_esppro").css("border","1px solid #d2d6de");

      

        return key_error;
    }

    function limpiar_epd()
    {
    	$("#cod_esppro").val("").prop("readonly",true);
    	$("#pro_esppro").val("").prop("readonly",true);
		$("#peso_esppro").val("").prop("readonly",true);
		$("#cantz_esppro").val("").prop("readonly",true);
		
		$("#ppunit").val("");
		$("#pro_espproesp").val("");
						
		$("#addespprodev").hide();
		if($("#tiponota").val()=="NDXD"){
			//$('body,html').animate({scrollTop : 0}, 500);
			//$('body,html').animate({scrollTop : 1000}, 0);
			//$("#cap_ini").hide();
		}
		else{
			$("#cap_fin").hide();
			//('body,html').animate({scrollTop : 1000}, 0);
			//$("#cap_ini").show();
		}

		$("#tiponota").val("");
		$("#estpro_x").val("");
		$("#codigoimp").val("");
    }

	/***************************** resumen ************************/

	//producto a devolver

	var indice_tabla3=0;
	function agre_res_proddev(cod,des,peso,cant,codidoenlace,selection)
    { 
        indice_tabla3++;
        $("#conta3").val(indice_tabla3);
        var nuevaFila="<tr id=filadevres_"+indice_tabla3+">";

        nuevaFila+='<td><i class="fa fa-hand-o-right" aria-hidden="true"></i> '+indice_tabla3+"</td>";
        nuevaFila+="<td>"+cod+"</td>";
        nuevaFila+="<td>"+des+"</td>";
        nuevaFila+="<td>"+peso+"</td>";
        nuevaFila+="<td>"+cant+"</td>";

        //nuevaFila+='<td><div class="btn btn-link" onclick="delTabla2('+codidoenlace+','+selection+')" >'+'<a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quitar"></i></a>'+'</div>';
        
        nuevaFila+="</tr>";
        
        $("#bandeja-pd").append(nuevaFila);      
    }

    //producto a cambiar

	var indice_tabla4=0;
	function agre_res_prodcamb(cod,des,peso,cant)
    { 
        indice_tabla4++;
        $("#conta4").val(indice_tabla4);
        var nuevaFila="<tr id=filacanres_"+indice_tabla4+">";

        nuevaFila+='<td><i class="fa fa-hand-o-right" aria-hidden="true"></i> '+indice_tabla4+"</td>";
        nuevaFila+="<td>"+cod+"</td>";
        nuevaFila+="<td>"+des+"</td>";
        nuevaFila+="<td>"+peso+"</td>";
        nuevaFila+="<td>"+cant+"</td>";

        //nuevaFila+='<td><div class="btn btn-link" onclick="delTabla2('+codidoenlace+','+selection+')" >'+'<a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quitar"></i></a>'+'</div>';
        
        nuevaFila+="</tr>";
        
        $("#bandeja-pc").append(nuevaFila);      
    }

/*******************************************************************************/

    $("#peso_esppro").on('keyup', function(){
        var value = $(this).val();            
             
        if(typeof(value) !== "undefined" && value!="")
          $("#cantz_esppro").val(calcularrollo(parseFloat(value)));        
    }).keyup();

</script>
@endpush('scripts')