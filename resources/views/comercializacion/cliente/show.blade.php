@extends('backend.layouts.appv2')
@section('subtitulo', 'Cliente')
@section('content')

<link href="{{ asset('css/form.css') }}" rel="stylesheet">

<div class="row">
    <div class="col-sm-12">
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">@yield('subtitulo') | {{ $cliente->cClieDesc }}</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div><!-- /.box tools -->
                </div><!-- /.box-header -->
                <div class="box-body">           

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4 {{ $errors->has('tipodoc_id') ? 'has-error' :'' }}">                                                 
                                <select id="tipodoc_id" name="tipodoc_id" class="form-control" autofocus="autofocus" tabindex="1" disabled="disabled">
                                    @foreach($tipodoc as $tb)
                                        @if($tb->nTdocCod == $cliente->nTdocCod)
                                            <option value= "{{$tb->nTdocCod}}" selected="selected">{{$tb->cTdocSigla}}</option>
                                        @else
                                            <option value= "{{$tb->nTdocCod}}" >{{$tb->cTdocSigla}}</option>
                                        @endif
                                    @endforeach
                                </select>   
                                {!! $errors->first('tipodoc_id','<span class="help-block">:message</span>') !!}                                
                            </div>
                            <div class="col-md-8 {{ $errors->has('cClieNdoc') ? 'has-error' :'' }}">
                                 <input class="form-control" placeholder="Número de Doc." name="cClieNdoc" type="text" maxlength="11" value="{{ $cliente->cClieNdoc }}" tabindex="2" disabled="disabled">   
                                 {!! $errors->first('cClieNdoc','<span class="help-block">:message</span>') !!}                                  
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">    
                            <div class="col-md-6 {{ $errors->has('cClieDesc') ? 'has-error' :'' }}">
                                <input class="form-control" placeholder="Descripción (Nombre/Razón Social)" name="cClieDesc" type="text" maxlength="255" value="{{ $cliente->cClieDesc }}" tabindex="3" disabled="disabled">
                                {!! $errors->first('cClieDesc','<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="col-md-6 {{ $errors->has('cClieDirec') ? 'has-error' :'' }}">
                                <input class="form-control" placeholder="Dirección" name="cClieDirec" type="text" maxlength="255" value="{{ $cliente->cClieDirec }}" tabindex="4" disabled="disabled">
                                {!! $errors->first('cClieDirec','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 {{ $errors->has('cClieObs') ? 'has-error' :'' }}">
                                <textarea class="form-control" placeholder="Observación" rows="5" name="cClieObs" cols="50" tabindex="5" maxlength="255" disabled="disabled">{{$cliente->cClieObs}}</textarea>
                                {!! $errors->first('cClieObs','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    

                    <a href="{{ route('cliente.edit',$cliente->nClieCod) }}" class=" signbuttons btn btn-primary">Editar</a> <a href="{{ route('cliente.index') }}" class=" signbuttons btn btn-primary">Listado</a>
                  
                </div><!-- /.box-body -->
            </div><!--box box-success-->
        </section>
    </div>
</div>

@endsection