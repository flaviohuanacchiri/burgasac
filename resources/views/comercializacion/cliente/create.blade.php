@extends('backend.layouts.appv2')

@section('subtitulo', 'Nuevo Cliente')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">@yield('subtitulo')</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div><!-- /.box tools -->
                </div><!-- /.box-header -->
                <div class="box-body">                   
                @include('comercializacion.cliente.fragmento.info')
                    <form method="POST" action="{{ route('cliente.store') }}" accept-charset="UTF-8"><input name="_token" type="hidden" >
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="POST">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 {{ $errors->has('tipodoc_id') ? 'has-error' :'' }}">                                                 
                                    <select id="tipodoc_id" name="tipodoc_id" class="form-control" autofocus="autofocus" tabindex="1">
                                        @foreach($tipodoc as $tb)
                                            @if($tb->nTdocCod == old('tipodoc_id'))
                                                <option value="{{$tb->nTdocCod}}" selected="selected">{{$tb->cTdocSigla}}</option>
                                            @else
                                                <option value="{{$tb->nTdocCod}}">{{$tb->cTdocSigla}}</option>
                                            @endif
                                        @endforeach
                                    </select>   
                                    {!! $errors->first('tipodoc_id','<span class="help-block">:message</span>') !!}                                
                                </div>
                                <div class="col-md-8 {{ $errors->has('cClieNdoc') ? 'has-error' :'' }}">
                                     <input class="form-control" placeholder="Número de Doc." name="cClieNdoc" id="cClieNdoc" type="text" maxlength="11" value="{{ old('cClieNdoc') }}" tabindex="2">   
                                     {!! $errors->first('cClieNdoc','<span class="help-block">:message</span>') !!}                                  
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">    
                                <div class="col-md-6 {{ $errors->has('cClieDesc') ? 'has-error' :'' }}">
                                    <input class="form-control" placeholder="Nombre/Razón Social" name="cClieDesc" type="text" maxlength="255" value="{{ old('cClieDesc') }}" tabindex="3">
                                    {!! $errors->first('cClieDesc','<span class="help-block">:message</span>') !!}
                                </div>

                                <div class="col-md-6 {{ $errors->has('cClieDirec') ? 'has-error' :'' }}">
                                    <input class="form-control" placeholder="Dirección" name="cClieDirec" type="text" maxlength="255" value="{{ old('cClieDirec') }}" tabindex="4">
                                    {!! $errors->first('cClieDirec','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 {{ $errors->has('cClieObs') ? 'has-error' :'' }}">
                                    <textarea class="form-control" placeholder="Observación" rows="5" name="cClieObs" cols="50" tabindex="5" maxlength="255">{{ old('cClieObs') }}</textarea>
                                    {!! $errors->first('cClieObs','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        
                        <button type="submit" class=" signbuttons btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button> 
                        <a href="{{ redirect()->getUrlGenerator()->previous() }}" class=" signbuttons btn btn-primary">Cancelar</a>
                    </form>
                </div><!-- /.box-body -->
            </div><!--box box-success-->
        </section>
    </div>
</div>

@endsection

@push('scripts')
    <script>
       $(document).ready(function() 
       {
          $("form").keypress(function(e) {
                if (e.which == 13) {
                    return false;
                }
            });

            $("#tipodoc_id").change(function () 
            {   
                $("#cClieNdoc").val("");
                $("#cClieNdoc").focus();
                if($(this).val()==1)
                    $("#cClieNdoc").attr("maxlength","11");
                else
                    $("#cClieNdoc").attr("maxlength","8");

            });

        });
   </script>
@endpush('scripts')