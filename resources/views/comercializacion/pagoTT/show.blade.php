@extends('backend.layouts.appv2')

@section('subtitulo', 'REGISTROS DE PAGOS')

@section('content')


<link href="{{ asset('css/form.css') }}" rel="stylesheet">

<div class="row"  style="line-height: 2;">
	<div class="col-sm-12">
		<section class="content">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title">@yield('subtitulo')</h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->
		        <div class="box-body">
		            @include('comercializacion.pagoTT.fragment.info')
	
	                   
		                <div class="form-group">
			                <div class="row">
			                	<div class="col-md-4">
			                		<label>Fecha</label>
			                    	  <input class="form-control" type="text" name="fecha" id="fecha" value="{{substr($pc->CompraTelat->tFecha,0,strpos($pc->CompraTelat->tFecha, ' '))}}" readonly> 
			                	</div>
			                	<div class="col-md-4">
			                		<label>Proveedor</label>
			                    	<input class="form-control" type="text" name="prov" id="prov" value="{{$pc->CompraTelat->proveedor->nombre_comercial}}" readonly> 
			                    </div>
			                    <div class="col-md-4">
			                    	<label>Factura/Guía</label>
                                    <input class="form-control" type="text" name="dat" id="dat" value="{{$pc->CompraTelat->cDatDocFac}}" readonly>
			                	</div>
			                
			                </div>

			                <div class="row">
			                	<div class="col-md-4">
			                		<label>Monto de Compra</label>
			                    	<input class="form-control" type="text" name="moncom" id="moncom" value="{{number_format($pc->CompraTelat->dMonTotal,2)}}" readonly> 
			                	</div>
			                	<div class="col-md-4">
			                		<label>Saldo</label>
			                    	<input class="form-control" type="text" name="sald" id="sald" value="{{number_format($pc->dPagoSaldo,2)}}" readonly>
			                    </div>
			                    <div class="col-md-4">
			                    	<label>Monto Total Pagado</label>
			                    	<input class="form-control" type="text" name="monpag" id="monpag" value="{{number_format($pc->dPagoTotal,2)}}" readonly> 
			                	</div>
		
			                </div>			                
			            </div>

			            <table id="bandeja" name="bandeja" class="table table-striped table-bordered table-hover">
                            <thead>
                                <th>                                    
                                    Impresión
                                </th>
                                <th>
                                    Fecha
                                </th>                              
                                <th>
                                    Monto Pagado
                                </th>                              
                                <th>
                                    Origen
                                </th>     
                            </thead>
                            <tbody style="text-align: center;">
                            	@foreach($pc->detalles as $p)
                            		<tr>	                            		
	                            		<td>
	                            			<div class="btn btn-link" onclick="alert('imprimir');" >
	                            				<a class="btn btn-xs btn-default">
	                            					<i class="fa fa-print" data-toggle="tooltip" data-placement="top" title="" data-original-title="Imprimir"></i>
	                            				</a>
	                            			</div>
	                            		</td>
	                            		<td style="text-align: center;">{{$p->tFecha}}</td>
	                            		<td style="text-align: center;">{{number_format($p->dMontoPag,2)}}</td>
	                            		<td style="text-align: center;">{{($p->bCaja=="1")?'Caja: '.$p->cCaja:'Otros'}}</td>
	                            	</tr>
                            	@endforeach
                            </tbody>
                        </table>
                    	<br>
                        <div>
		                	<a href="{{ route('pagoTT.index') }}" class=" signbuttons btn btn-primary">Bandeja</a>
		                </div>
		            <!--/form-->

    			</div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>

@endsection

@push('scripts')

@endpush('scripts')