@extends('backend.layouts.appv2')

@section('title', 'BANDEJA DE COMPRAS AL CRÉDITO')

@section('after-styles')
    <style>
    .dropdown{padding: 0;}
    .dropdown .dropdown-menu{border: 1px solid #999}
    .detallescompra{
        display: none;
        background-color: #ececec;
    }
    </style>
@stop

@section('content')

	<div class="row">
		<div class="col-sm-12">
			<section class="content">
			    <div class="box box-info">
			        <div class="box-header with-border">
			            <h3 class="box-title">@yield('title')</h3>
			            <div class="box-tools pull-right">
			                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			            </div><!-- /.box tools -->
			        </div><!-- /.box-header -->
			        <div class="box-body">
			        	@include('comercializacion.pagoTT.fragment.info')
			        	<div class="row">                        	
                        	<form action="{{ route('pagoTT.index') }}" method="GET">
	                        {{ csrf_field() }}
	                        <input type="hidden" name="_method" value="GET">
                            
                            <div class="col-md-6">
                                <label>Proveedor</label>
		                    	<select id="proveedor" class="form-control" name="proveedor" tabindex="1">
                                    <option value="">Todos...</option>
                                    @foreach ($proveedores as $key => $proveedor)
                                    	@if($datosant->proveedor==$proveedor->id)
                                    		<option value="{{$proveedor->id}}" selected>{{$proveedor->nombre_comercial}}</option>
                                    	@else
                                        	<option value="{{$proveedor->id}}">{{$proveedor->nombre_comercial}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label for="">Saldo</label>
                                <input class="form-control" type="text" name="compa" id="compa" value="{{ $datosant->compa }}" placeholder=">X, <X, =X, >=X, <=X">                                
                            </div>
                                   
                            <div class="col-md-2" style="text-align:center;">
                                <br>
                                <button type="submit" class=" signbuttons btn btn-primary">Buscar</button>                            
                            </div>
                        	</form>	                        
	                    </div>
	                    <br>
						<div class="row">							
							<div class="col-md-12">
								<table class="table table-hover table-striped">
									<thead>
										<tr>
											<th>Fecha</th>									
											<th>Proveedor</th>									
											<th>T.Documento</th>
											<th>Forma de Pago</th>									
											<th>N° Documento</th>									
											<th>Moneda</th>									
											<th>Monto</th>		
											<th>Pago</th>									
											<th>Saldo</th>									
											<th colspan="2">Acciones</th>
										</tr>
									</thead>
									<tbody style="text-align: center;">
										@foreach($pc as $c)
											@if(evaluarSaldo(trim($datosant->compa),$c->PagoCompra[0]->dPagoSaldo)=="1")
												<tr>							
													<td>{{ $c->tFecha }}</td>
													<td>{{ $c->proveedor->nombre_comercial }}</td>
													<td>{{ $c->tipodoc->cDescTipPago }}</td>
													<td>{{ $c->formapago->cDescForPag }}</td>
													<td>{{ $c->cDatDocFac }}</td>
													<td>{{ $c->cMoneda }}</td>
													<td style="text-align: right;color:green;">{{ number_format($c->dMonTotal,2) }}</td>
													<td style="text-align: right;color:blue;">{{ number_format($c->PagoCompra[0]->dPagoTotal,2) }}</td>
													<td style="text-align: right;color:red;">{{ number_format($c->PagoCompra[0]->dPagoSaldo,2) }}</td>
													<td>
														<a href="{{ route('pagoTT.show',$c->PagoCompra[0]->nCodPagComp) }}" class="btn btn-xs btn-info">
				                                            <i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver"></i>
				                                        </a>
														@if($c->PagoCompra[0]->dPagoSaldo > 0)		

					                                        <a href="{{ route('pagoTT.create',$c->PagoCompra[0]->nCodPagComp) }}" class="btn btn-xs btn-success">
					                                            <i class="fa fa-paypal" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver"></i>
					                                        </a>
				                                        @endif
													</td>
												</tr>
											@endif
										@endforeach
									</tbody>
								</table>
								{!! $pc->render() !!}
							</div>	
						</div>
			        </div><!-- /.box-body -->
			    </div><!--box box-success-->
			</section>
		</div>
	</div>
<?php 
	function evaluarSaldo($condicion,$saldo)
	{
		if(trim($condicion)=="")
			return "1";

		$signo="";
		$numero="";		
		if(is_numeric(substr($condicion,1,2)))
		{
			$signo=substr($condicion,0,1);
			$numero=trim(substr($condicion,1));
		}
		else
		{
			$signo=substr($condicion,0,2);	
			$numero=trim(substr($condicion,2));
		}	
		/*echo $signo;
		echo $numero;
		echo $saldo.'|';*/

		if($signo=='>' && $numero<$saldo)
		{
			return 	"1";	
		}
		else if($signo=='<' && $numero>$saldo)
		{
			return 	"1";	
		}
		else if($signo=='=' && $numero==$saldo)
		{
			return 	"1";	
		}
		else if($signo=='>=' && $numero<=$saldo)
		{
			return 	"1";	
		}
		else if($signo=='<=' && $numero>=$saldo)
		{
			return 	"1";	
		}
	
		return "0";
	}
 ?>
@endsection

@push('scripts')

<script type="text/javascript">	
    $(document).bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
        	$(location).attr('href',"{{ route('almacen.create') }}");
        };
    });

    /* show / hide order details */
        $(".detalle").click(function() {
          $(this).closest("tr").next().toggle('fast');
          if($("#ver").attr("class") == 'fa fa-eye fa-1x')
            $("#ver").attr("class", "fa fa-eye-slash fa-1x");
          else
            $("#ver").attr("class", "fa fa-eye fa-1x");
        });
</script>

@endpush('scripts')
