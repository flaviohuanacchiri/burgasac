@extends('backend.layouts.appv2')

@section('subtitulo', 'REGISTRO DE PAGOS')

@section('content')


<link href="{{ asset('css/form.css') }}" rel="stylesheet">

<div class="row"  style="line-height: 2;">
	<div class="col-sm-12">
		<section class="content">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title">@yield('subtitulo')</h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->
		        <div class="box-body">
		            @include('comercializacion.pagoTT.fragment.info')

		            <form action="{{ route('pagoTT.store') }}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="POST">						
	                    <input type="hidden" name="conta" id="conta" value="">	                    
	                    <input type="hidden" name="eliminados" id="eliminados" value="">
                        <input type="hidden" name="id" id="id" value="{{$id}}">	
	                    <!--input type="hidden" name="prod_agre" id="prod_agre" value="">
	                    <input type="hidden" name="prod_codpro" id="prod_codpro" value=""-->
	                   
		                <div class="form-group">
			                <div class="row">
			                	<div class="col-md-2">
			                		<label>Fecha</label>
			                    	  <input class="form-control" type="text" name="fecha" id="fecha" value="{{substr($pc->CompraTelat->tFecha,0,strpos($pc->CompraTelat->tFecha, ' '))}}" readonly> 
			                	</div>
			                	<div class="col-md-5">
			                		<label>Proveedor</label>
			                    	<input class="form-control" type="text" name="prov" id="prov" value="{{$pc->CompraTelat->proveedor->nombre_comercial}}" readonly> 
			                    </div>
			                    <div class="col-md-3">
			                    	<label>Factura/Guía</label>
                                    <input class="form-control" type="text" name="dat" id="dat" value="{{$pc->CompraTelat->cDatDocFac}}" readonly>
			                	</div>
			                	<div class="col-md-2" style="padding: 16px;text-align: center;">
			                    	<input  type="checkbox" name="caja_i" value="1">CAJA
			                	</div>
			                </div>

			                <div class="row">
			                	<div class="col-md-3">
			                		<label>Monto de Compra</label>
			                    	<input class="form-control" type="text" name="moncom" id="moncom" value="{{number_format($pc->CompraTelat->dMonTotal,2)}}" readonly> 
			                	</div>
			                	<div class="col-md-3">
			                		<label>Saldo</label>
			                    	<input class="form-control" type="text" name="sald" id="sald" value="{{number_format($pc->dPagoSaldo,2)}}" readonly>
			                    </div>
			                    <div class="col-md-3">
			                    	<label>Monto a Pagar</label>
			                    	<input class="form-control" type="text" name="monpag" id="monpag" placeholder="0.00" value="" autofocus> 
			                	</div>
			                	<div class="col-md-3">
			                		<label style="color:white;">jjjj</label>
                                    <a class="signbuttons btn btn-primary form-control" onclick="agredardetalle()"><i class="fa fa-plus" aria-hidden="true" ></i> Agregar</a> 
			                	</div>
			                </div>			                
			            </div>

			            <table id="bandeja" name="bandeja" class="table table-striped table-bordered table-hover">
                            <thead>                                                                     
                                <th>                                    
                                    Quitar
                                </th>
                                <th>                                    
                                    Impresión
                                </th>
                                <th>
                                    Fecha
                                </th>                              
                                <th>
                                    Proveedor
                                </th>                              
                                <th>
                                    Factura/Guía
                                </th>                              
                                <th>
                                    Monto
                                </th>
                                                                          
                            </thead>
                            <tbody style="text-align: center;">
                                
                                
                            </tbody>
                            	<tr>
                            		<td colspan="4" style="font-weight: bold;">
                                        Saldo disponible en caja: S/. {{$saldo}}  | Considerar si el dinero sale de caja.    
                                    </td>
                            		<input type='hidden'  name='tot_g_i' id='tot_g_i'>
                            		<td style="text-align: center;">Total</td>
                            		<td id="tot_g" name="tot_g" style="text-align:right;font-size: 15px;font-weight: bold;">0.00</td>
                            	</tr>
                        </table>
                        <ul class="pagination">
                                      
                        </ul>
                        <div>
		                <button type="submit" class=" signbuttons btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button> <a href="{{ route('pagoTT.index') }}" class=" signbuttons btn btn-primary">Cancelar</a>
		                </div>
		            </form>

    			</div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
	var indice_tabla=0;        
    var cant_items_real=0; 
    var indice_act=1;
    var ultima_pag=1;
    var cantxpag=7;
    var cont_item_bd=0;
    var total=0;
      

    $(document).ready(function() 
    {
        /*@if(Session::has('cod'))            
            window.open("{{ route('venta.reporte',Session::get('cod')) }}");            
        @endif*/
        $("form").keypress(function(e) {
            if (e.which == 13) {
                return false;
            }
        });
        $('#monpag').numeric(",").numeric({decimalPlaces: 2,negative: false});
        /********************** PAGINACIÓN **************************/
        cant_items_real=cont_item_bd;    
        /*** cargar lista de p/ginas***/
        controlpaginacion(cantxpag,cant_items_real);        
        /*** cargar paginación inicial ***/
        paginacion(1,cantxpag);
    });

    $("#monpag").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {

            agredardetalle();
        }
    });

    function agredardetalle()
    {          
    	
        if(validacion())
        {
            // Obtenemos el total de columnas (tr) del id "tabla"  
            indice_tabla++;
            $("#conta").val(indice_tabla);
            var nuevaFila="<tr id=fila_"+indice_tabla+">";           

            nuevaFila+='<td><input type="hidden" name="cod_ndi_'+indice_tabla+'" id="cod_ndi_'+indice_tabla+'" value="0"><div class="btn btn-link" onclick="delTabla('+indice_tabla+')" >'+'<a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quitar"></i></a>'+'</div>';

            nuevaFila+='<td><div class="btn btn-link" onclick="alert();" >'+'<a class="btn btn-xs btn-default"><i class="fa fa-print" data-toggle="tooltip" data-placement="top" title="" data-original-title="Imprimir"></i></a>'+'</div>';

            nuevaFila+="<td>"+$("#fecha").val()+"</td>";

            nuevaFila+="<td>"+$("#prov").val()+"</td>";

            nuevaFila+="<td>"+$("#dat").val()+"</td>";

            nuevaFila+="<td style='text-align:right;'>"+'<input type="hidden" name="monP_'+indice_tabla+'" id="monP_'+indice_tabla+'" value="'+parseFloat($("#monpag").val()).toFixed(2)+'">'+parseFloat($("#monpag").val()).toFixed(2)+"</td>";
            
            nuevaFila+="</tr>";
            
            $("#bandeja").append(nuevaFila);

            total+=parseFloat($("#monpag").val());            
            totales(total.toFixed(2));
            //$("#prod_agre").val($("#produc").val()+","+$("#prod_agre").val());
            //$("#prod_codpro").val($("#produc option:selected").attr("dato-cod")+","+$("#prod_codpro").val());
            
            // paginación   
            cant_items_real++;            
            // cargar lista de p/ginas
            controlpaginacion(cantxpag,cant_items_real);        
            // cargar paginación inicial            
            paginacion(ultima_pag,cantxpag);

            limpiarcontroles();   
    	
        }
    }

    function totales(dato_x)
    {    	
        $("#tot_g_i").val(dato_x);
        $("#tot_g").empty();
        $("#tot_g").html(dato_x);
        sal_x=parseFloat("{{$saldo}}");
        if(dato_x > sal_x)
        {
            $("#tot_g").css("color","red");
        }
        else
        {
            $("#tot_g").css("color","blue");
        }
    }

    function delTabla(id)
    {     
        
        var datos= $("#eliminados").val()+","+$("#cod_ndi_"+id).val();
        $("#eliminados").val(datos);

        //------- para actualizar valores en el control ---
        total-=parseFloat($("#monP_"+id).val()).toFixed(2);            
        totales(total.toFixed(2));
        limpiarcontroles()

        $("#fila_"+id).remove();            

        cant_items_real--;
        // cargar lista de p/ginas
        controlpaginacion(cantxpag,cant_items_real);        
        // cargar paginación inicial 
        paginacion(indice_act,cantxpag);
            
    }

    function limpiarcontroles()
	{	
	    $("#monpag").val("");
	    $("#monpag").css("border","1px solid #d2d6de");

    }

    function validacion()
    {

        var mon_=$("#monpag").val(); 
        var sald_=parseFloat($("#sald").val());       
        var key_error=true;
        
        if(mon_=="")
        {
            $("#monpag").css("border","2px solid #f00");                
            key_error=false;
        }
        else
            $("#monpag").css("border","1px solid #d2d6de"); 

        if(sald_ <total+parseFloat(mon_))
        {
            $("#sald").css("border","2px solid #f00");                
            key_error=false;
        }
        else
            $("#sald").css("border","1px solid #d2d6de"); 

        
        return key_error;
    }

    function controlpaginacion(pag,total)
    {
        var cad='';
        var act=true;
        var i;
        for(i=1;i<=total/pag;i++)
        {

            if(act)
                cad='<li onclick="paginacion('+i+','+pag+')" style="cursor: pointer;"><span>«</span></li>';

            cad+='<li '+((act)?'class="active"':'')+' onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            act=false;
        }
        if(total%pag>0)
        {
            cad+='<li onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            cad+='<li onclick="paginacion('+(i)+','+pag+');" style="cursor: pointer;"><span>»</span></li>';
        }
        else
            cad+='<li onclick="paginacion('+(--i)+','+pag+')" style="cursor: pointer;"><span>»</span></li>';

        ultima_pag=i;

        $(".pagination").empty();
        $(".pagination").html(cad);
    }

    function paginacion(pag,de)
    {   
        // ocultar registros de 10 en 10 

        var fin=pag*de;;
        var ini=(pag-1)*de;


        //meter todo en un array, con indice ordenado                        
        var array_items=[];
        var contador=0;
        for (var i = 1; i <= parseInt($("#conta").val()); i++) 
        {                
            var nomfila="fila_"+i;
            if ( $("#"+nomfila).length > 0 ) 
            {
                array_items[contador]=nomfila;
                contador++;
            }                
        }
        //recorrer segun los limites mostrar lo necesario

        for(var i=0;i<contador;i++)
        {
            if(i>=ini && i<fin)
                $("#"+array_items[i]).show();        
            else
                $("#"+array_items[i]).hide();
        }

        indice_act=pag;//guarada el indice de pág

        for(i=1;i<=ultima_pag;i++)
            $("#pgitem_"+i).removeAttr('class');

        $("#pgitem_"+indice_act).attr('class','active');

        return 1;
    }


</script>
@endpush('scripts')