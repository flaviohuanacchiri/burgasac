@extends('backend.layouts.appv2')

@section('subtitulo', 'Nuevo Módulo')

@section('content')


<link href="{{ asset('css/form.css') }}" rel="stylesheet">

<div class="row">
	<div class="col-sm-12">
		<section class="content">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title">@yield('subtitulo')</h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->
		        <div class="box-body">
		            @include('comercializacion.modulos.fragment.info')

		            <form action="{{ route('modulos.store') }}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="POST">
		                <div class="form-group">
			                <div class="row">
			                	<div class="col-md-3 {{ $errors->has('nombre') ? 'has-error' :'' }}">
			                		<label>Nombre</label>
			                    	<input class="form-control" type="text" name="nombre" id="nombre" placeholder="Nombre" value="{{ old('nombre') }}" value="" maxlength="249"  onkeypress="return tabular(event,this)" autofocus="autofocus">
			                    	{!! $errors->first('nombre','<span class="help-block">:message</span>') !!}
			                	</div>
			                	<div class="col-md-5 {{ $errors->has('urlx') ? 'has-error' :'' }}">
			                		<label>Url</label>
			                    	<input class="form-control" type="text" name="urlx" id="urlx" placeholder="Ingrese la url para realizar los filtros necesario..." value="{{ old('urlx') }}" maxlength="499">			                    
			                    	{!! $errors->first('urlx','<span class="help-block">:message</span>') !!}	
			                	</div>
			                	<div class="col-md-4">
			                		<label>Descripción</label>
			                    	<input class="form-control" type="text" name="desc" id="desc" placeholder="Si lo desea haga una descricpión..." value="{{ old('desc') }}" maxlength="499">			                    	
			                	</div>
			                </div>
			            </div>

		                <button type="submit" class=" signbuttons btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button> <a href="{{ route('modulos.index') }}" class=" signbuttons btn btn-primary">Cancelar</a>
		            </form>

    			</div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>

@endsection

@push('scripts')
	<script>
       function tabular(e,obj) {
         tecla=(document.all) ? e.keyCode : e.which;
         if(tecla!=13) return;
         frm=obj.form;
         for(i=0;i<frm.elements.length;i++)
           if(frm.elements[i]==obj) {
             if (i==frm.elements.length-1) i=-1;
             break }
         frm.elements[i+1].focus();
         return false;
       }

       if(!("autofocus" in document.createElement("input")))
           document.getElementById("uno").focus();
   </script>
@endpush('scripts')