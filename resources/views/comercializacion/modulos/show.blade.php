@extends('backend.layouts.appv2')
@section('subtitulo', 'Área')
@section('content')

<link href="{{ asset('css/form.css') }}" rel="stylesheet">

<div class="row">
	<div class="col-sm-12">
		<section class="content">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title">@yield('subtitulo') | {{ $ar->cNomMod }}</h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->
		        <div class="box-body">
	                <div class="form-group">
		                <div class="row">
		                	<div class="col-md-3">
		                		<label>Nombre</label>
		                    	<input class="form-control" type="text" name="nombre" id="nombre" placeholder="Nombre" value="{{ $ar->cNomMod }}" value="" maxlength="255"  onkeypress="return tabular(event,this)" disabled>
		                	</div>
		                	<div class="col-md-5 {{ $errors->has('urlx') ? 'has-error' :'' }}">
			                		<label>Url</label>
			                    	<input class="form-control" type="text" name="urlx" id="urlx" placeholder="Ingrese la url para realizar los filtros necesario..." value="{{ $ar->url }}" maxlength="499" disabled>			                    
			                    	{!! $errors->first('urlx','<span class="help-block">:message</span>') !!}	
			                	</div>
		                	<div class="col-md-4">
		                		<label>Descripción</label>
		                    	<input class="form-control" type="text" name="desc" id="desc" placeholder="Si lo desea haga una descricpión..." value="{{ $ar->cDesMod }}" maxlength="499" disabled>			                    	
		                	</div>
		                </div>
		            </div>

	                <a href="{{ route('modulos.edit',$ar->nCodMod) }}" class=" signbuttons btn btn-primary">Editar</a> <a href="{{ route('modulos.index') }}" class=" signbuttons btn btn-primary">Listado</a>
		          
		        </div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>

@endsection