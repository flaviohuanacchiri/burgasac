@extends('backend.layouts.appv2')
@section('subtitulo', 'Área')
@section('content')

<link href="{{ asset('css/form.css') }}" rel="stylesheet">

<div class="row">
	<div class="col-sm-12">
		<section class="content">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title">@yield('subtitulo') | {{ $ar->nAreCod }}</h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->
		        <div class="box-body">
		            <div class="form-group">
			                <div class="row">
			                	<div class="col-md-6">
			                		<div class="row" style="margin: 3px;">
			                			<div class="col-md-12 {{ $errors->has('nombre') ? 'has-error' :'' }}">
					                		<label>Nombre</label>
					                    	<input class="form-control" type="text" name="nombre" id="nombre" placeholder="Nombre" value="{{ $ar->cAreNom }}" disabled>
					                    	{!! $errors->first('nombre','<span class="help-block">:message</span>') !!}
					                	</div>
					                </div>
					                <div class="row" style="margin: 3px;">
				                		<div class="col-md-12">
					                		<label>Forma de Pago</label>
					                    	<div class="form-control">
					                    		@if($ar->bForPag == 0)
	                                            <input type="radio" class="" id="radbol" name="tipdoc" value="0" checked disabled>
	                                            @else
	                                            <input type="radio" class="" id="radbol" name="tipdoc" value="0" disabled>
	                                            @endif
	                                            <label>Automático</label>     

	                                            @if($ar->bForPag == 1)
	                                            <input type="radio" class="" id="radguia" name="tipdoc" value="1" checked disabled>
	                                            @else
	                                            <input type="radio" class="" id="radguia" name="tipdoc" value="1"  disabled>
	                                            @endif						                    	
	                                            <label>Manual</label>
                                            </div>
					                    	{!! $errors->first('tipdoc','<span class="help-block">:message</span>') !!}
					                	</div>
					                </div>					                
			                	</div>
			                	<div class="col-md-6">
			                		<label>Listado de Módulos</label>
			                    	<div class="row">
		                                <div class="col-md-12">		                             
		                                    <div id="capa" style="position: absolute;width: 97%;height: 92%;background-color: rgba(19, 19, 19, 0.35);display: none;top: 1px;"></div>
		                                    <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
		                                        <thead>
		                                            <th>
		                                                Item
		                                            </th>
		                                            <th>
		                                                Módulo
		                                            </th>		     
		                                        </thead>
		                                        <tbody style="text-align: center;">
		                                        	<?php $i=0; ?>
		                                        	@foreach($ar->rolareamod as $mx)
		                                        		<tr>
		                                        			<td>
		                                        				{{++$i}}
		                                        			</td>
		                                        			<td>
		                                        				{{$mx->modulos->cNomMod}}
		                                        			</td>
		                                        		</tr>
		                                        	@endforeach
		                                        </tbody>
		                                    </table>
		                                   
		                                    <ul class="pagination">
		                                      
		                                    </ul>

		                                </div>
		                            </div>
			                	</div>
			                </div>
			            </div>

	                <a href="{{ route('area.edit',$ar->nAreCod) }}" class=" signbuttons btn btn-primary">Editar</a> <a href="{{ route('area.index') }}" class=" signbuttons btn btn-primary">Bandeja</a>
		          
		        </div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>

@endsection

@push('scripts')
    <script>
    var indice_tabla=0;
    var key_enter=true;
    var cant_items_real=0;
    var indice_act=1;
    var ultima_pag=1;
    var cantxpag=5;
    var tabla=[];

	$(document).ready(function() {
      $("form").keypress(function(e) {
            if (e.which == 13) {
                return false;
            }
        });
    
    /********************** PAGINACIÓN **************************/
    cant_items_real=cont_item_bd;    
    /*** cargar lista de p/ginas***/
    controlpaginacion(cantxpag,cant_items_real);        
    /*** cargar paginación inicial ***/
    paginacion(1,cantxpag);

    });


    function controlpaginacion(pag,total)
    {
        var cad='';
        var act=true;
        var i;
        for(i=1;i<=total/pag;i++)
        {

            if(act)
                cad='<li onclick="paginacion('+i+','+pag+')" style="cursor: pointer;"><span>«</span></li>';

            cad+='<li '+((act)?'class="active"':'')+' onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            act=false;
        }
        if(total%pag>0)
        {
            cad+='<li onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            cad+='<li onclick="paginacion('+(i)+','+pag+');" style="cursor: pointer;"><span>»</span></li>';
        }
        else
            cad+='<li onclick="paginacion('+(--i)+','+pag+')" style="cursor: pointer;"><span>»</span></li>';

        ultima_pag=i;

        $(".pagination").empty();
        $(".pagination").html(cad);
    }

    function paginacion(pag,de)
    {   
        // ocultar registros de 10 en 10 

        var fin=pag*de;;
        var ini=(pag-1)*de;


        //meter todo en un array, con indice ordenado                        
        var array_items=[];
        var contador=0;
        for (var i = 1; i <= parseInt($("#conta").val()); i++) 
        {                
            var nomfila="fila_"+i;
            if ( $("#"+nomfila).length > 0 ) 
            {
                array_items[contador]=nomfila;
                contador++;
            }                
        }
        //recorrer segun los limites mostrar lo necesario

        for(var i=0;i<contador;i++)
        {
            if(i>=ini && i<fin)
                $("#"+array_items[i]).show();        
            else
                $("#"+array_items[i]).hide();
        }

        indice_act=pag;//guarada el indice de pág

        for(i=1;i<=ultima_pag;i++)
            $("#pgitem_"+i).removeAttr('class');

        $("#pgitem_"+indice_act).attr('class','active');

        return 1;
    }
   </script>
@endpush('scripts')