@extends('backend.layouts.appv2')

@section('subtitulo', 'Edición del Área')

@section('content')

<link href="{{ asset('css/form.css') }}" rel="stylesheet">

<div class="row">
	<div class="col-sm-12">
		<section class="content">
		    <div class="box box-info">
		        <div class="box-header with-border">
		            <h3 class="box-title">@yield('subtitulo') | {{ $ar->nAreCod }}</h3>
		            <div class="box-tools pull-right">
		                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            </div><!-- /.box tools -->
		        </div><!-- /.box-header -->		     
		        <div class="box-body">
		            @include('comercializacion.area.fragment.info')

		            <form action="{{ route('area.update',$ar->nAreCod) }}" method="POST">
							{{ csrf_field() }}
						<input type="hidden" name="_method" value="PUT">

	                    <input type="hidden" name="codint" id="codint" value="">
	                    <input type="hidden" name="conta" id="conta" value="">
	                    <input type="hidden" name="eliminados" id="eliminados" value="">
	                    <input type="hidden" name="actualizar" id="actualizar" value="">
	                    <input type="hidden" name="cad_actt" id="cad_actt" value="">

		                <div class="form-group">
			                <div class="row">
			                	<div class="col-md-6">
			                		<div class="row" style="margin: 3px;">
			                			<div class="col-md-12 {{ $errors->has('nombre') ? 'has-error' :'' }}">
					                		<label>Nombre</label>
					                    	<input class="form-control" type="text" name="nombre" id="nombre" placeholder="Nombre" value="{{ $ar->cAreNom }}" maxlength="255" autofocus="autofocus">
					                    	{!! $errors->first('nombre','<span class="help-block">:message</span>') !!}
					                	</div>
					                </div>
					                <div class="row" style="margin: 3px;">
				                		<div class="col-md-12">
					                		<label>Forma de Pago</label>
					                    	<div class="form-control">
	                                            @if($ar->bForPag == 0)
	                                            <input type="radio" class="" id="radbol" name="tipdoc" value="0" checked>
	                                            @else
	                                            <input type="radio" class="" id="radbol" name="tipdoc" value="0">
	                                            @endif
	                                            <label>Automático</label>     

	                                            @if($ar->bForPag == 1)
	                                            <input type="radio" class="" id="radguia" name="tipdoc" value="1" checked>
	                                            @else
	                                            <input type="radio" class="" id="radguia" name="tipdoc" value="1" >
	                                            @endif						                    	
	                                            <label>Manual</label>
                                            </div>
					                    	{!! $errors->first('tipdoc','<span class="help-block">:message</span>') !!}
					                	</div>
					                </div>
					                <div class="row" style="margin: 3px;">
					                	<div class="col-md-9">
					                		<label>Módulo</label>
					                    	 <select class="selectpicker form-control" data-show-subtext="true" data-live-search="true" id="modins" name="modins" tabindex="1">                          
				                                  	@foreach($mod as $m)
				                                        <option data-subtext="" value="{{$m->nCodMod}}">{{$m->cNomMod}}</option>
				                                    @endforeach
			                                </select>
					                	</div>
					                	<div class="col-md-3">
					                		<label style="color: white;">Módulo</label>
					  						<div  id="add" class="btn btn-primary form-control" tabindex="8" onclick="agredardetalle()">agregar</div>
					                	</div>
					                </div>
			                	</div>
			                	<div class="col-md-6">
			                		<label>Listado de Módulos</label>
			                    	<div class="row">
		                                <div class="col-md-12">		                             
		                                    <div id="capa" style="position: absolute;width: 97%;height: 92%;background-color: rgba(19, 19, 19, 0.35);display: none;top: 1px;"></div>
		                                    <table id="bandeja-transfer" name="bandeja-transfer" class="table table-striped table-bordered table-hover">
		                                        <thead>
		                                            <th>
		                                                Item
		                                            </th>
		                                            <th>
		                                                Módulo
		                                            </th>		                                            
		                                            <th>
		                                                Eliminar
		                                            </th>
		                                        </thead>
		                                        <tbody style="text-align: center;">
		                                            	                                            
		                                        </tbody>
		                                    </table>
		                                   
		                                    <ul class="pagination">
		                                      
		                                    </ul>

		                                </div>
		                            </div>
			                	</div>
			                </div>
			            </div>

		               <button type="submit" class=" signbuttons btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button> <a href="{{ route('area.index') }}" class=" signbuttons btn btn-primary">Bandeja</a>
		            </form>

    			</div><!-- /.box-body -->
		    </div><!--box box-success-->
		</section>
	</div>
</div>
    
@endsection

@push('scripts')
    <script>
    var indice_tabla=0;
    var key_enter=true;
    var cant_items_real=0;
    var indice_act=1;
    var ultima_pag=1;
    var cantxpag=5;
    var tabla=[];

	$(document).ready(function() {
      $("form").keypress(function(e) {
            if (e.which == 13) {
                return false;
            }
        });
    

    @foreach($det as $d)
		agredardetalle_ini("{{$d->nRolAreMod}}","{{$d->nCodMod}}","{{$d->modulos->cNomMod}}");
    @endforeach
    

    });

    function agredardetalle_ini(cod,codmod,mod)
    {   
        // Obtenemos el total de columnas (tr) del id "tabla"            
        indice_tabla++;
        $("#conta").val(indice_tabla);
        var nuevaFila="<tr id=fila_"+indice_tabla+">";
        nuevaFila+='<td><input type="hidden" name="cod_ndi_'+indice_tabla+'" id="cod_ndi_'+indice_tabla+'" value="'+cod+'"><i class="fa fa-check" aria-hidden="true"></i></td>';
        
        nuevaFila+='<td><input type="hidden" name="mod_'+indice_tabla+'" id="mod_'+indice_tabla+'" value="'+codmod+'">'+mod+'</td>';
        
        nuevaFila+='<td><div class="btn btn-link" onclick="delTabla('+indice_tabla+')" >'+'<a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quitar"></i></a>'+'</div>';
        
        nuevaFila+="</tr>";
        $("#bandeja-transfer").append(nuevaFila);
        
        tabla[indice_tabla]=codmod;        
        //limpiarcontroles();                  
        //fin-----------------------------------------------		
		recargar_combo();        

        // paginación   
        cant_items_real++;            
        // cargar lista de p/ginas
        controlpaginacion(cantxpag,cant_items_real);        
        // cargar paginación inicial            
        paginacion(ultima_pag,cantxpag);
    }

    function agredardetalle()
    {  
        //alert($("#modins").val()!=null); 
        if($("#modins").val()!=null)
        {
            // Obtenemos el total de columnas (tr) del id "tabla"            
            indice_tabla++;
            $("#conta").val(indice_tabla);
            var nuevaFila="<tr id=fila_"+indice_tabla+">";
            nuevaFila+='<td><input type="hidden" name="cod_ndi_'+indice_tabla+'" id="cod_ndi_'+indice_tabla+'" value="0"><i class="fa fa-hand-o-right" aria-hidden="true"></i></td>';
            
            nuevaFila+='<td><input type="hidden" name="mod_'+indice_tabla+'" id="mod_'+indice_tabla+'" value="'+$("#modins").val()+'">'+$("#modins option:selected").html()+'</td>';
            
            nuevaFila+='<td><div class="btn btn-link" onclick="delTabla('+indice_tabla+')" >'+'<a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Quitar"></i></a>'+'</div>';
            
            nuevaFila+="</tr>";
            $("#bandeja-transfer").append(nuevaFila);
            
            tabla[indice_tabla]=$("#modins").val();
            
            //limpiarcontroles();   
                   
            //fin-----------------------------------------------
    		
    		recargar_combo();        

            // paginación   
            cant_items_real++;            
            // cargar lista de p/ginas
            controlpaginacion(cantxpag,cant_items_real);        
            // cargar paginación inicial            
            paginacion(ultima_pag,cantxpag);
        }
        
    
    }
	
	function buscar_array(array,buscar)
    {
        for (var i = 0; i <array.length; i++) 
        {
            if(buscar==array[i])
                return true;
        }
        return false;
    }

    function recargar_combo()
    {
    	$("#modins").empty();
        $('#modins').selectpicker('refresh');

    	@foreach($mod as $m)
    		if(!buscar_array(tabla,'{{$m->nCodMod}}'))
    			$("#modins").append("<option data-subtext='' value='{{$m->nCodMod}}' >{{$m->cNomMod}}</option>");                  		
        @endforeach

        $('#modins').selectpicker('refresh');
    	
    }

	function delTabla(id)
    {     
        
        if(confirm('Nota: El registro se retirará momentaneamente, pero el cambio no se hará efecto hasta que usted guarde los cambios.'))
        {
            var datos= $("#eliminados").val()+","+$("#cod_ndi_"+id).val();
            $("#eliminados").val(datos);

            //------- para actualizar valores en el control ---
            delete tabla[id];
            recargar_combo();     
            // fin controles-----------------------------------
            
            $("#fila_"+id).remove();            

            cant_items_real--;
            // cargar lista de p/ginas
            controlpaginacion(cantxpag,cant_items_real);        
            // cargar paginación inicial 
            paginacion(indice_act,cantxpag);
        }            
    }


    function controlpaginacion(pag,total)
    {
        var cad='';
        var act=true;
        var i;
        for(i=1;i<=total/pag;i++)
        {

            if(act)
                cad='<li onclick="paginacion('+i+','+pag+')" style="cursor: pointer;"><span>«</span></li>';

            cad+='<li '+((act)?'class="active"':'')+' onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            act=false;
        }
        if(total%pag>0)
        {
            cad+='<li onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            cad+='<li onclick="paginacion('+(i)+','+pag+');" style="cursor: pointer;"><span>»</span></li>';
        }
        else
            cad+='<li onclick="paginacion('+(--i)+','+pag+')" style="cursor: pointer;"><span>»</span></li>';

        ultima_pag=i;

        $(".pagination").empty();
        $(".pagination").html(cad);
    }

    function paginacion(pag,de)
    {   
        // ocultar registros de 10 en 10 

        var fin=pag*de;;
        var ini=(pag-1)*de;


        //meter todo en un array, con indice ordenado                        
        var array_items=[];
        var contador=0;
        for (var i = 1; i <= parseInt($("#conta").val()); i++) 
        {                
            var nomfila="fila_"+i;
            if ( $("#"+nomfila).length > 0 ) 
            {
                array_items[contador]=nomfila;
                contador++;
            }                
        }
        //recorrer segun los limites mostrar lo necesario

        for(var i=0;i<contador;i++)
        {
            if(i>=ini && i<fin)
                $("#"+array_items[i]).show();        
            else
                $("#"+array_items[i]).hide();
        }

        indice_act=pag;//guarada el indice de pág

        for(i=1;i<=ultima_pag;i++)
            $("#pgitem_"+i).removeAttr('class');

        $("#pgitem_"+indice_act).attr('class','active');

        return 1;
    }
   </script>
@endpush('scripts')