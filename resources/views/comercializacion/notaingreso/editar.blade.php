@extends('backend.layouts.appv2')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">NOTA DE INGRESO</div>
                <div class="panel-body">
                @include('comercializacion.notaingreso.fragment.info')
                <form action="{{ route('notaingreso.store') }}" method="POST" onkeypress="return anular(event)">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="codint" id="codint" value="{{ $id }}">
                    <input type="hidden" name="conta" id="conta" value="">
                    <input type="hidden" name="eliminados" id="eliminados" value="">
                    <input type="hidden" name="actualizar" id="actualizar" value="">
                    <input type="hidden" name="cad_actt" id="cad_actt" value="">
                    <input type="hidden" name="page" id="page" value="edition">
                    <input type="hidden" name="codproid" id="codproid" value="{{$bandeja->producto_id}}">
                    <input type="hidden" name="codprovid" id="codprovid" value="{{$bandeja->proveedor_id}}">


                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="">Fecha</label>
                                    <input id="fecha_t" type="text" class="form-control" name="fecha_t" value="{{ $fecha }}" readonly>
                                </div>
                                <div class="col-md-2">
                                    <label for="">Control Interno</label>
                                    <input id="control_t" type="text" class="form-control" name="control_t" readonly value="{{ $bandeja->id }}">
                                </div>
                                <div class="col-md-3">
                                    <label for="">Proveedor</label>
                                    <input id="proveedor_t" type="text" class="form-control" name="proveedor_t" readonly value="{{ $bandeja->razon_social }}">
                                </div>

                                <div class="col-md-5">
                                    <label for="">Producto</label>
                                    <input id="producto_t" type="text" class="form-control" name="producto_t" readonly value="{{ $bandeja->nombre_generico }}">
                                    <input id="producto_t_e" type="hidden" class="form-control" name="producto_t_e" readonly value="{{ $bandeja->nombre_especifico }}">
                                </div>
                             
                            </div>
                            <div class="row">   
                                <div class="col-md-2">
                                    <label for="">Lote</label>
                                    <input id="lote_t" type="text" class="form-control" name="lote_t" readonly value="{{ $bandeja->nro_lote }}" maxlength="8">
                                </div>

                                <div class="col-md-2">
                                    <label for="">Guía</label>
                                    <input id="mp_t" type="text" class="form-control" name="mp_t" readonly value="{{ $bandeja->nroguia}}">
                                </div>

                                <div class="col-md-2">
                                    <label for="">Peso</label>
                                    <input id="peso_t" type="text" class="form-control" name="peso_t" readonly value="{{ $bandeja->cantidad }}">
                                </div>
                                <div class="col-md-3">
                                    <label for="">Rollos</label>
                                    <input id="rollos_t" type="text" class="form-control" name="rollos_t" readonly value="{{ $bandeja->rollos }}">
                                </div>

                                <div class="col-md-3">
                                    <label for="">Color</label>
                                    <input id="color_t" type="text" class="form-control" name="color_t" readonly value="{{ $bandeja->nombre }}">
                                </div>                        
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body" id="dina_control" name="dina_control">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="">Tienda</label>                            
                                    <select class="form-control" name="tienda" id="tienda" autofocus="autofocus" tabindex="1">               
                                        @foreach ($tienda as $tiendaalm)
                                            @if($tiendaalm->almacen->nAlmCod == $codalmusu)
                                                <option value="{{$tiendaalm->nProAlmCod}}" selected>{{$tiendaalm->almacen->cAlmNom}}</option>
                                            @else
                                                <!--option value="{{$tiendaalm->nProAlmCod}}">{{$tiendaalm->almacen->cAlmNom}}</option-->
                                            @endif
                                            
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="">Guía</label>                                    
                                    <select class="form-control" name="guiatt" id="guiatt" autofocus="autofocus" tabindex="2">    
                                        <option value="0">Seleccionar...</option>                                                       
                                        @foreach ($guiaz as $par)
                                            <option value="{{$par->guia}}">{{$par->guia}}</option>
                                        @endforeach
                                        <option value="-1">Nuevo...</option> 
                                    </select>
                                    <input class="form-control" type="text" name="guiattX" id="guiattX" style="display: none;">
                                    <input class="form-control" type="text" name="guiattY" id="guiattY" style="display: none;">
                                </div>
                                <div class="col-md-2">
                                    <label for="">Partida</label>
                                    <!--input id="partida" type="text" class="form-control" name="partida" placeholder="# partida" maxlength="15" tabindex="2"-->
                                    <select class="form-control" name="partida" id="partida" autofocus="autofocus" tabindex="3">   
                                        <option value="0">Seleccionar...</option>    
                                    </select>
                                    <input class="form-control" type="text" name="partidaY" id="partidaY" style="display: none;">
                                </div>
                                <div class="col-md-2">
                                    <label for="">Peso</label>
                                    <input id="peso" type="text" class="form-control" name="peso" placeholder="peso o cantidad" maxlength="6" tabindex="4">
                                </div>

                                <div class="col-md-1">
                                    <label for="">Rollos</label>
                                    <input id="rollo" type="text" class="form-control" name="rollo" placeholder="rollos" maxlength="9" tabindex="5" value="1" style="width: 88px;">
                                </div>
                                <div class="col-md-2" style="text-align:center;">
                                    <br>                                    
                                    <div  id="add" class="btn btn-primary" tabindex="6">agregar</div>
                                    <div  id="act" class="btn btn-primary" style="display:none;" onclick="actualiza()">actualizar</div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <br>
                                    <div id="capa" style="position: absolute;width: 97%;height: 92%;background-color: rgba(19, 19, 19, 0.35);display: none;top: 1px;"></div>
                                    <table id="bandeja-produccion" name="bandeja-produccion" class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <th>
                                                Id
                                            </th>
                                            <th>
                                                Fecha
                                            </th>
                                            <th>
                                                Producto
                                            </th>
                                            <th>
                                                Tienda
                                            </th>
                                            <th>
                                                Guía
                                            </th>
                                            <th>
                                                Partida
                                            </th>
                                            <th>
                                                Color
                                            </th>
                                            <th>
                                                Peso
                                            </th>
                                            <th>
                                                Rollos
                                            </th>
                                            <th>
                                                Print
                                            </th>                                       
                                            <th colspan="2">
                                                Acción
                                            </th>
                                        
                                        </thead>
                                        <tbody style="text-align: center;">
                                            
                                            
                                        </tbody>
                                    </table>
                                    <ul class="pagination">
                                      
                                    </ul>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                        <button class="btn btn-success">Guardar</button>
                                        <a href="{{ route('comercializacion.index')}}" class="btn btn-primary">Bandeja</a>
                                </div>
                            </div>



                        </div>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts')
<script type="text/javascript">
var indice_tabla=0;
var key_enter=true;
var cant_items_real=0;
var indice_act=1;
var ultima_pag=1;
    $(document).ready(function(){
        
        /*$('#rollo').keyup(function (){
          this.value = (this.value + '').replace(/[^0-9]/g, '');
        });*/
        $('#rollo').numeric(",").numeric({decimalPlaces: 3,negative: false});

        $('#peso').numeric(",").numeric({decimalPlaces: 2,negative: false});
        //$("#act").hide();

       /* $("#tienda").attr('disabled','disabled');
        $("#partida").attr('disabled','disabled');
        $("#peso").attr('disabled','disabled');
        $("#rollo").attr('disabled','disabled');
        */

        /*$("#peso").on('keyup', function(){
            var value = $(this).val();            
            
            if(typeof(value) !== "undefined" && value!="")
            {            
                $("#rollo").val(parseFloat(value)/20);
            }
            else{         
                $("#rollo").val("0"); 
            }     
        }).keyup();*/
        
       
        
        var cont_item_bd=0;
        @foreach($bandejatabla as $tab_bandeja) 
            addtabla("{{ $tab_bandeja->dNotIng_id }}","{{ $tab_bandeja->fecha }}","{{ $tab_bandeja->nombre_generico }}","{{ $tab_bandeja->nProAlmCod}}","{{ $tab_bandeja->cAlmNom }}","{{ $tab_bandeja->partida }}","{{ $tab_bandeja->nombre }}","{{ $tab_bandeja->peso_cant }}","{{ $tab_bandeja->rollo }}","{{ $tab_bandeja->impreso }}","{{ $tab_bandeja->cod_barras }}","{{ $tab_bandeja->guia }}");
            cont_item_bd++;
        @endforeach

        cant_items_real=cont_item_bd;
        
        /*** cargar lista de p/ginas***/
        controlpaginacion(10,cant_items_real);        
        /*** cargar paginación inicial ***/
        paginacion(1,10);

        if("{{ $impresion }}"=="1")
            window.open("{{ route('notaingreso.impresion',$id) }}");

    });

     $("#add").click(function()
        {
            if(validacion()==0)
            {                
                if($("#guiatt").val()=="-1")
                {
                    // VERIFICAR QUE ESTa partida no exista en la bd
                    partida_xx=$.trim($("#partida").val());
                    var cod_val=("{{$bandeja->desptint_id}}"=="")?"0":"{{$bandeja->desptint_id}}";
                    
                    $.ajax({
                          type: "GET",
                          url: "../veri_partida/"+cod_val+"/"+partida_xx+"/"+$("#codprovid").val()+"/"+$("#guiattX").val()+"/"+$("#codproid").val(),                          
                          dataType: "json",
                          error: function(){
                            alert("error petición ajax");
                          },
                          success: function(data)
                          {
                            if(data=="1")
                            {                            
                               add_fx();
                            }
                            else
                            {
                                alert("ya existe la combinación de guía y partida...");
                                $("#partida").css("border","2px solid #f00");
                                $("#guiattX").css("border","2px solid #f00");
                            }
                          }
                    });
                }
                else
                {
                    add_fx();
                }
            }

        });

    function add_fx()
    {
        //$("#guiatt").show();
        //$("#guiattX").hide();
        // Obtenemos el numero de filas (td) que tiene la primera columna

        // Obtenemos el total de columnas (tr) del id "tabla"            
        indice_tabla++;
        $("#conta").val(indice_tabla);
        var nuevaFila="<tr id=fila_"+indice_tabla+">";
        nuevaFila+="<td>"+'<input type="hidden" name="cod_ndi_'+indice_tabla+'" id="cod_ndi_'+indice_tabla+'" value="0">'+'<i class="fa fa-hand-o-right" aria-hidden="true"></i> '+indice_tabla+"</td>";
        
        nuevaFila+="<td>"+'<input type="hidden" name="fec_'+indice_tabla+'" id="fec_'+indice_tabla+'" value="'+$("#fecha_t").val()+'">'+$("#fecha_t").val()+"</td>";
        nuevaFila+="<td>"+$("#producto_t").val()+"</td>";
        nuevaFila+="<td>"+'<input type="hidden" name="tie_'+indice_tabla+'" id="tie_'+indice_tabla+'" value="'+$("#tienda").val()+'"><p id="Ltie_'+indice_tabla+'">'+$("#tienda option:selected").html()+"</p></td>";

        if($("#guiatt").val()=="-1")
        {
            nuevaFila+="<td>"+'<input type="hidden" name="guiatt_'+indice_tabla+'" id="guiatt_'+indice_tabla+'" value="'+$.trim($("#guiattX").val())+'"><p id="Lgui_'+indice_tabla+'">'+$.trim($("#guiattX").val())+"</p></td>";    
        }
        else
        {
            nuevaFila+="<td>"+'<input type="hidden" name="guiatt_'+indice_tabla+'" id="guiatt_'+indice_tabla+'" value="'+$.trim($("#guiatt").val())+'"><p id="Lgui_'+indice_tabla+'">'+$.trim($("#guiatt").val())+"</p></td>";    
        }            

        nuevaFila+="<td>"+'<input type="hidden" name="par_'+indice_tabla+'" id="par_'+indice_tabla+'" value="'+$("#partida").val()+'"><p id="Lpar_'+indice_tabla+'">'+$("#partida").val()+"</p></td>";
        nuevaFila+="<td>"+$("#color_t").val()+"</td>";
        nuevaFila+="<td>"+'<input type="hidden" name="pes_'+indice_tabla+'" id="pes_'+indice_tabla+'" value="'+doscerosdecimal($("#peso").val())+'"><p id="Lpes_'+indice_tabla+'">'+doscerosdecimal($("#peso").val())+"</p></td>";
        nuevaFila+="<td>"+'<input type="hidden" name="roll_'+indice_tabla+'" id="roll_'+indice_tabla+'" value="'+$("#rollo").val()+'"><p id="Lroll_'+indice_tabla+'">'+$("#rollo").val()+"</p></td>";

        nuevaFila+="<td>"+'<input type="hidden" name="cb_'+indice_tabla+'" id="cb_'+indice_tabla+'" value=""><input type="checkbox" id="cbox_'+indice_tabla+'" name="cbox_'+indice_tabla+'">'+"</td>";
        // Añadimos una columna con el numero total de columnas.
        // Añadimos uno al total, ya que cuando cargamos los valores para la
        // columna, todavia no esta añadida
        nuevaFila+='<td><div class="btn btn-link" onclick="delTabla('+indice_tabla+')" >'+'<a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver"></i></a>'+'</div>';

        nuevaFila+='<td><div class="btn btn-link" onclick="editar('+indice_tabla+')" >'+'<a class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver"></i></a>'+'</div>';

        nuevaFila+="</tr>";
        $("#bandeja-produccion").append(nuevaFila);
        
        $("#peso").val("");
        $("#rollo").val("1");
        $( "#peso" ).focus();

        //$("#partida").attr('disabled','disabled');

        /*** paginación ***/        
        cant_items_real++;
        //var ult_pag=Math.floor(cant_items_real/10);
        /*** cargar lista de p/ginas***/
        controlpaginacion(10,cant_items_real);        
        /*** cargar paginación inicial ***/
        //paginacion(((cant_items_real%10 > 0)?++ult_pag:ult_pag),10);
        paginacion(ultima_pag,10);
        //$("#partida").attr('disabled','disabled');
        
    }

    $("#guiatt").change(function(event)
    {        
        $("#partida").empty();

        switch(event.target.value)
        {
            case "-1":
                $("#guiatt").hide();
                $("#guiattX").show();
                $.get("../partidasall/{{$id}}",function(response)
                {            

                    $("#partida").append("<option data-subtext='' value='0' >Seleccionar...</option>"); 
                    for(i=0;i<response.length;i++)
                    {   
                        $("#partida").append("<option value='"+response[i].partida+"' > "+response[i].partida+"</option>");
                    }        
                });
            break;
            default:
                $.get("../partidas/{{$id}}/"+event.target.value,function(response)
                {            

                    $("#partida").append("<option data-subtext='' value='0' >Seleccionar...</option>"); 
                    for(i=0;i<response.length;i++)
                    {   
                        $("#partida").append("<option value='"+response[i].partida+"' > "+response[i].partida+"</option>");
                    }        
                });
            break;
        }

        
    });

    $(document).bind('keydown',function(e){
        if ( e.which == 27 ) 
        {
            cancelar();
        };
    });


    /*$("#rollo").bind('keydown',function(e){
        if ( e.which == 13 && key_enter ) 
        {
            $('#add').click();
        };
    });      */      

    $("#tienda").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#partida").focus();
        };
    });

    $("#guiatt").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#partida").focus();
        };
    });

    $("#guiattX").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#partida").focus();
        }
        else if ( e.which == 27 ) 
        {
            $("#guiatt").show();
            $("#guiattX").hide();
        }
    });


    $("#partida").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#peso").focus();
        };
    });

    $("#peso").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            $("#rollo").focus();
        };
    });


    $("#rollo").bind('keydown',function(e){
        if ( e.which == 13 ) 
        {
            if(key_enter)
                $('#add').click();
            else
                actualiza();
        };
    });
    
    function doscerosdecimal(num)
    {
        array=num.split('.');
        if(array.length>1)
        {
            if(array[1].length==1)
                num=num+"0";            
            else if(array[1].length==0)
                num=num+".00";  
        }
        else
            num=num+".00";  

        return num; 

    }      

    function addtabla(cod,fec,prod,tiecod,tie,par,col,pes,roll,imp,cb,guiatt)
    {        
        indice_tabla++;
        $("#conta").val(indice_tabla);
        var nuevaFila="<tr id=fila_"+indice_tabla+">";
        nuevaFila+="<td>"+'<input type="hidden" name="cod_ndi_'+indice_tabla+'" id="cod_ndi_'+indice_tabla+'" value="'+cod+'">'+indice_tabla+"</td>";
        
        nuevaFila+="<td>"+'<input type="hidden" name="fec_'+indice_tabla+'" id="fec_'+indice_tabla+'" value="'+fec+'">'+fec+"</td>";
        nuevaFila+="<td>"+prod+"</td>";
        nuevaFila+="<td>"+'<input type="hidden" name="tie_'+indice_tabla+'" id="tie_'+indice_tabla+'" value="'+tiecod+'"><p id="Ltie_'+indice_tabla+'">'+tie+"</p></td>";
        nuevaFila+="<td>"+'<input type="hidden" name="guiatt_'+indice_tabla+'" id="guiatt_'+indice_tabla+'" value="'+guiatt+'"><p id="Lgui_'+indice_tabla+'">'+guiatt+"</p></td>";
        nuevaFila+="<td>"+'<input type="hidden" name="par_'+indice_tabla+'" id="par_'+indice_tabla+'" value="'+par+'"><p id="Lpar_'+indice_tabla+'">'+par+"</p></td>";
        nuevaFila+="<td>"+col+"</td>";
        nuevaFila+="<td>"+'<input type="hidden" name="pes_'+indice_tabla+'" id="pes_'+indice_tabla+'" value="'+doscerosdecimal(pes)+'"><p id="Lpes_'+indice_tabla+'">'+doscerosdecimal(pes)+"</p></td>";
        nuevaFila+="<td>"+'<input type="hidden" name="roll_'+indice_tabla+'" id="roll_'+indice_tabla+'" value="'+roll+'"><p id="Lroll_'+indice_tabla+'">'+roll+"</p></td>";

        nuevaFila+="<td>"+'<input type="hidden" name="cb_'+indice_tabla+'" id="cb_'+indice_tabla+'" value="'+cb+'"><input type="checkbox" id="cbox_'+indice_tabla+'" name="cbox_'+indice_tabla+'" '+"checked"+'>'+"</td>";
        // Añadimos una columna con el numero total de columnas.
        // Añadimos uno al total, ya que cuando cargamos los valores para la
        // columna, todavia no esta añadida
        nuevaFila+='<td><div class="btn btn-link" id="cdel_'+indice_tabla+'"  onclick="delTabla('+indice_tabla+')" >'+'<a class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver"></i></a>'+'</div>';
        nuevaFila+='<td><div class="btn btn-link" id="cedi_'+indice_tabla+'" onclick="editar('+indice_tabla+')" >'+'<a class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver"></i></a>'+'</div>';
        nuevaFila+="</tr>";
        $("#bandeja-produccion").append(nuevaFila);
        return 1;            
    
    }

    function delTabla(id)
    {     
        
        if(confirm('Nota: El registro se retirará momentaneamente, pero el cambio no se hará efecto hasta que usted guarde los cambios.'))
        {
            var datos= $("#eliminados").val()+","+$("#cod_ndi_"+id).val();
            $("#eliminados").val(datos);
            $("#fila_"+id).remove();
            cant_items_real--;
            /*** cargar lista de p/ginas***/
            controlpaginacion(10,cant_items_real);        
            /*** cargar paginación inicial ***/
            paginacion(indice_act,10);
        }            
    }

    function editar(id)
    {

        $("#tienda").removeAttr("disabled");
        //$("#partida").attr('disabled','disabled');        
        $("#peso").removeAttr("disabled");
        $("#rollo").removeAttr("disabled");

        $("#dina_control").css( "box-shadow","rgba(0, 0, 0, 0.75) 0px 4px 47px -5px"); 

        $("#tienda").val($("#tie_"+id).val());
        //$('#partida > option[value="'+$("#par_"+id).val()+'"]').attr('selected', 'selected');
        //$("#partida").val($("#par_"+id).val());

        $("#guiatt").hide(); 
        $("#guiattX").hide();
        $("#guiattY").val($("#guiatt_"+id).val());
        $("#guiattY").show(); 

        $("#partida").hide();
        $("#partidaY").val($("#par_"+id).val());
        $("#partidaY").show();

        $("#peso").val($("#pes_"+id).val());
        $("#rollo").val($("#roll_"+id).val());
        
        $("#tienda").attr('disabled','disabled');
        $("#partidaY").attr('disabled','disabled');
        $("#guiattY").attr('disabled','disabled');
        //$("#guiattX").attr('disabled','disabled');


        $("#cdel_"+id).hide();
        $("#cedi_"+id).hide();

        $("#actualizar").val(id);

        $("#act").show();
        $("#add").hide();   

        $( "#peso" ).focus();
        $("#capa").show();
        key_enter=false;
    }

    function actualiza()
    {
        if(validacionAct()==0)
        {
            var id=$("#actualizar").val();
            $("#tie_"+id).val($("#tienda").val());

            $("#guiatt_"+id).val($("#guiattY").val());
            $("#par_"+id).val($("#partidaY").val());
            $("#pes_"+id).val($("#peso").val());
            $("#roll_"+id).val($("#rollo").val());            
            
            $("#Ltie_"+id).text($("#tienda option:selected").html());
            $("#Lgui_"+id).text($("#guiattY").val());
            $("#Lpar_"+id).text($("#partidaY").val());
            $("#Lpes_"+id).text($("#peso").val());
            $("#Lroll_"+id).text($("#rollo").val());

            $("#guiattY").hide();
            $("#guiatt").show();

            $("#partidaY").hide();
            $("#partida").show();

            $("#tienda").removeAttr('disabled');

            $("#cdel_"+id).show();
            $("#cedi_"+id).show();

            $("#dina_control").css( "box-shadow","none"); 

            $("#tienda").val("1");
            //$("#partida").val("");
            $("#peso").val("");
            $("#rollo").val("");

            $("#act").hide();
            $("#add").show();

            //verificar si hay  cambios 
            if($("#tie_"+id).val()!=$("#tienda").val() || $("#pes_"+id).val()!=$("#peso").val() || $("#roll_"+id).val()!=$("#rollo").val())
            {                
                var datos= $("#cad_actt").val()+","+$("#cod_ndi_"+id).val();
                $("#cad_actt").val(datos);
            }

            $("#partida" ).focus();
            $("#capa").hide();
            key_enter=true;
            //si hay agregar al input .... el código
            /*$("#tienda").attr('disabled','disabled');
            $("#partida").attr('disabled','disabled');
            $("#peso").attr('disabled','disabled');
            $("#rollo").attr('disabled','disabled');*/
        }

    }
    function cancelar()
    {
        var id=$("#actualizar").val();
        //$("#partida").removeAttr('disabled');
        $("#cdel_"+id).show();
        $("#cedi_"+id).show();

        $("#dina_control").css( "box-shadow","none"); 

        $("#tienda").val("1");
        //$("#partida").val("");
        $("#peso").val("");
        $("#rollo").val("");

        $("#act").hide();
        $("#add").show();

        $( "#partida" ).focus();
        $("#capa").hide();
        key_enter=true;
        /*$("#tienda").attr('disabled','disabled');
        $("#partida").attr('disabled','disabled');
        $("#peso").attr('disabled','disabled');
        $("#rollo").attr('disabled','disabled');*/
    }

     function anular(e) {
          tecla = (document.all) ? e.keyCode : e.which;
          return (tecla != 13);
     }

     function validacionAct()
     {
        
        var pes_=$.trim($("#peso").val());
        var rol_=$.trim($("#rollo").val());
        var key_error=false;
        
        if(rol_=="")
        {
            $("#rollo").css("border","2px solid #f00");
            $("#rollo").focus();  
            key_error=true;
        }
        else
            $("#rollo").css("border","1px solid #d2d6de");
        
        if(pes_=="")
        {
            $("#peso").css("border","2px solid #f00");   
            $("#peso").focus();                  
            key_error=true;
        }
        else
            $("#peso").css("border","1px solid #d2d6de");

        return (key_error)?1:0;
     }
     
     function validacion()
     {
        var tie_=$.trim($("#tienda").val());
        var gui_=$.trim($("#guiatt").val());
        var gui_2=$.trim($("#guiattX").val());

        var par_=$.trim($("#partida").val());
        var pes_=$.trim($("#peso").val());
        var rol_=$.trim($("#rollo").val());
        var key_error=false;
        
        if(rol_=="")
        {
            $("#rollo").css("border","2px solid #f00");
            $("#rollo").focus();  
            key_error=true;
        }
        else
            $("#rollo").css("border","1px solid #d2d6de");
        
        if(pes_=="")
        {
            $("#peso").css("border","2px solid #f00");   
            $("#peso").focus();                  
            key_error=true;
        }
        else
            $("#peso").css("border","1px solid #d2d6de");

        if(par_=="0")
        {
            $("#partida").css("border","2px solid #f00");                
            $("#partida").focus();
            key_error=true;
        }
        else
            $("#partida").css("border","1px solid #d2d6de");

        if(gui_=="0")
        {
            $("#guiatt").css("border","2px solid #f00"); 
            $("#guiatt").focus();       
            key_error=true;
        }
        else if(gui_=="-1")
        {
            if(gui_2=="")
            {
                $("#peso").css("border","2px solid #f00");   
                $("#peso").focus();                  
                key_error=true;
            }
            else
                $("#guiattX").css("border","1px solid #d2d6de");            
        }
        else
            $("#guiatt").css("border","1px solid #d2d6de");
        
        if(tie_=="")
        {
            $("#tienda").css("border","2px solid #f00");     
            $("#tienda").focus();            
            key_error=true;
        }
        else
            $("#tienda").css("border","1px solid #d2d6de");

        return (key_error)?1:0;
     }

     function controlpaginacion(pag,total)
    {
        var cad='';
        var act=true;
        var i;
        for(i=1;i<=total/pag;i++)
        {

            if(act)
                cad='<li onclick="paginacion('+i+','+pag+')" style="cursor: pointer;"><span>«</span></li>';

            cad+='<li '+((act)?'class="active"':'')+' onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            act=false;
        }
        if(total%pag>0)
        {
            cad+='<li onclick="paginacion('+i+','+pag+');" id="pgitem_'+i+'" style="cursor: pointer;"><span>'+i+'</span></li>';
            cad+='<li onclick="paginacion('+(i)+','+pag+');" style="cursor: pointer;"><span>»</span></li>';
        }
        else
            cad+='<li onclick="paginacion('+(--i)+','+pag+')" style="cursor: pointer;"><span>»</span></li>';

        ultima_pag=i;

        $(".pagination").empty();
        $(".pagination").html(cad);
    }

    function paginacion(pag,de)
    {   
        /** ocultar registros de 10 en 10 **/         

        var fin=pag*de;;
        var ini=(pag-1)*de;


        //meter todo en un array, con indice ordenado                        
        var array_items=[];
        var contador=0;
        for (var i = 1; i <= parseInt($("#conta").val()); i++) 
        {                
            var nomfila="fila_"+i;
            if ( $("#"+nomfila).length > 0 ) 
            {
                array_items[contador]=nomfila;
                contador++;
            }                
        }
        //recorrer segun los limites mostrar lo necesario

        for(var i=0;i<contador;i++)
        {
            if(i>=ini && i<fin)
                $("#"+array_items[i]).show();        
            else
                $("#"+array_items[i]).hide();
        }

        indice_act=pag;//guarada el indice de pág

        for(i=1;i<=ultima_pag;i++)
            $("#pgitem_"+i).removeAttr('class');

        $("#pgitem_"+indice_act).attr('class','active');

        return 1;
        }

    </script>    
@endpush('scripts')