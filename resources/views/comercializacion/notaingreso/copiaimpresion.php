<title>Código Barras</title> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../cb/css/stylesheet.css" rel="stylesheet">

<script src="../../cb/jquery-1.10.2.js" type="text/JavaScript" language="javascript"></script>
<script src="../../cb/jquery-ui-1.10.4.custom.js"></script>
<script src="../../cb/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<br>
<button type="button" class="btn btn-success" id="impri">Imprimir</button>
<a href="{{ route('comercializacion.index') }}" class="btn btn-primary">
	Bandeja
</a>
<br>
<br>
<body style="background-color: #c7c7c7;text-align: center;">
<div class="row" style="width: 820px;height: 842px;background-color: white;left:15%;position: relative;" id="esto" >
    
        <div id="glyphs">
            
            <?php 
                $xcont = 1; $xcb= "";
                $arr=(array) json_decode(stripslashes($noimpresos));
            ?>  
            @foreach($arr as $var)
            
                <div class="g">
                    <table>
                        <tr><td id="p02cb" colspan="3" style="text-align: center;"><u><strong>TEXTILES BURGA SAC</strong></u><br></td></tr>
                        <tr><td id="p03cb" colspan="3">Art: {{$var[3]}}</td></tr>
                        <tr><td id="p03cb" colspan="3">Cod: {{$var[1]}} Color: {{$var[2]}}</td></tr>
                        <tr>
                        <td id="p01cb">Principal</td>
                        <td id="p01cb">&nbsp;&nbsp;</td>
                        <td id="p01cb">Sucursal</td>
                        </tr>
                        <tr>
                        <td id="p01cb">Jr. Sebastian Barranca 1448</td>
                        <td id="p01cb">&nbsp;&nbsp;</td>
                        <td id="p01cb">Sebastian Barranca 1441 Int 32-33</td>
                        </tr>
                        <tr>
                        <td id="p01cb">Fno. 324-1224 Cel. 998306592</td>
                        <td id="p01cb">&nbsp;&nbsp;</td>
                        <td id="p01cb">Fno. 324-1224 Cel. 998306592</td>
                        </tr>                                                       
                        <tr>
                            <td colspan="3" style="text-align: center;padding: 8px;">
                                <p style='font-family:"Code 128"'>{{$var[0]}}</p>
                            </td>
                        </tr>
                        <tr><td id="p03cb" colspan="3" style="text-align: center;">{{$var[0]}}</td></tr>
                    </table>
                </div>        
            @endforeach                     

        </div>        
        
    
</div>
</body>


<script>
    $(document).ready(function(){

        var dialog = $("div.testDialog").dialog({position : { my : "left top", at : "left bottom+50", of : ".SettingsBox" }, width: "600", title: "Print Dialog Box Contents"});

        //$("#impri").click(function(){

            var options = { mode : "popup", popClose : close};

            $("#esto").printArea( options);
        //});

    });

  </script>