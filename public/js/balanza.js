timer = false;
Balanza = {
    pesar : function(checked, input, disabled) {
        //GUARDAMOS EN UNA VARIABLE EL RESULTADO DE LA CONSULTA AJAX  
        let contentType ="application/x-www-form-urlencoded; charset=utf-8";
     
        if(window.XDomainRequest)
            contentType = "text/plain";
        if (checked == 1) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                },
                 url:"http://127.0.0.1/balanza/peso.php",
                 data:"name=Ravi&age=12",
                 type:"POST",
                 dataType:"json",   
                 contentType:contentType,    
                 success:function(data)
                 {
                    if (disabled) {
                        input.removeAttr("disabled");
                    } else {
                        input.removeAttr("readonly");
                    }
                    input.val(parseFloat(data.peso).toFixed(2));
                    if (disabled) {
                        input.prop("disabled", true);
                    } else {
                        input.prop("readonly", true);
                    }
                 },
                error:function(jqXHR,textStatus,errorThrown)
                {
                    console.log("You can not send Cross Domain AJAX requests: "+errorThrown);
                    input.val(0.00);
                }
            });
        } else {
            timer = false;
        }
    }, 
    limpiar : function(checked, input, disabled) {
        let contentType ="application/x-www-form-urlencoded; charset=utf-8";
             
        if(window.XDomainRequest) {
            contentType = "text/plain";
        }
        if (checked == 1) {
            $.ajax({
                url:"http://127.0.0.1/balanza/limpiar.php",
                data:"name=Ravi&age=12",
                type:"POST",
                dataType:"json",   
                contentType:contentType,    
                success:function(data)
                {
                    input.val(data.peso);
                    timer = false;
                },
                error:function(jqXHR,textStatus,errorThrown)
                {
                    console.log("You can not send Cross Domain AJAX requests: "+errorThrown);
                    input.val(0.00);
                    timer = false;
                }
            }); 
        }
    }
};