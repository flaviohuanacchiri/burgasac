let item = 0;
let tipo = "";
let moneda = 0;
Pago = {
	buscar : function() {
		$('#table-reporte').DataTable().destroy();
		return $('#table-reporte').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            ajax: 'pago?fechafiltroinicio='+$("#fechafiltroinicio").val()+'&fechafiltrofin='+$("#fechafiltrofin").val()+'&proveedorfiltro='+$("#proveedorfiltro").val()+'&guiafiltro='+$("#guiafiltro").val(),
            columns: [
            	{data: 'fecha', name: 'fecha'},
				{data: 'proveedor', name: 'proveedor'},
				{data: 'color', name: 'color'},
                {data: 'guia', name: 'guia'},
                {data: 'tipo', name: 'tipo'},
                {data: 'color', name: 'color'},
                {data: 'moneda', name: 'moneda'},
                {"data": function ( row, type, val, meta ) {
                    return row.deuda;
				}, name: 'deuda'},
                {"data": function ( row, type, val, meta ) {
                    return row.saldo;
				}, name: 'saldo'},
                {"data": function ( row, type, val, meta ) {
                    let boton = '<a class="btn btn-xs btn-primary" data-url="pago" ';
                    boton+=' data-guia="'+row.guia+'" data-proveedor="'+row.proveedor+'" ';
                    boton+='data-id="'+row.iddeuda+'" data-saldo="'+row.saldo+'"  ';
                    boton+='data-monto="'+row.deuda+'" data-tipo="'+row.tipo+'" ';
                    boton+='data-moneda="'+row.moneda_id+'" ';
                    boton+='data-toggle="modal" data-target="#modal-detalle">';
                    boton+='<i class="fa fa-eye"></i></a>';
                    return boton;
				}, name: 'action'},
            ]
        });
	},
	ejecutar : function() {
		form = {
			"tipo" : tipo,
			"monto": $("#montopagar").val(),
			"tipocomprobante" : $("#comprobantepago").val(),
			"nrocomprobante" : $("#nrocomprobante").val(),
            "moneda" : moneda,
            "item" : item
		};
		$.ajax({
			url : "pagar",
			type: "POST",
			data: form,
			success: function(obj) {
				if (obj.rst == 2 || obj.rst == "2") {
                    Mensaje.alerta(obj.msj);
                }
                if (obj.rst == 1 || obj.rst == "1") {
                    Mensaje.confirmacion(obj.msj);
                    Pago.listarDetalle();
                    saldo = parseFloat(obj.obj.total) - parseFloat(obj.obj-totalpagado);
                    $("#saldopago").removeAttr("disabled");
                    $("#saldopago").val(saldo.toFixed(2));
                    $("#saldopago").prop("disabled", true);

                }
			}
		});
		return false;
	},
	listarDetalle : function() {
		$('#modal-detalle-table').DataTable().destroy();
		return $('#modal-detalle-table').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            ajax: 'pagodetalle?id='+item+'&tipo='+tipo,
            columns: [
            	{data: 'updated_at', name: 'updated_at'},
            	{data: 'comprobante', name: 'comprobante'},
                {data: 'nro_comprobante', name: 'nro_comprobante'},
            	{data: 'moneda', name: 'moneda'},
				{"data": function ( row, type, val, meta ) {
                    let total = parseFloat(row.total).toFixed(2);
                    return total;
				}, name: 'total'},
                {"data": function ( row, type, val, meta ) {
                    let boton = '<a class="btn btn-xs btn-danger eliminar" data-url="pago" data-id="'+item+'">';
                    boton+='<i class="fa fa-trash"></i></a>';
                    return boton;
				}, name: 'action'},
            ]
        });
	},
    eliminar: function (id, url) {
        $.ajax({
            type: "DELETE",
            headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').val()
            },
            url: url + '/' + id,
            success: function (data) {
                console.log(data);
                    
                    if (data.rst == "2" || data.rst == 2){
                        Mensaje.alerta(data.msj);
                        return false;
                    }
                    timer = false;

                swal(
                    'Eliminado!',
                    'El registro ha sido eliminado.',
                    'success'
                  );
                  switch(url){
                    case 'pago':
                        Pago.listarDetalle();
                        break;
                    default:
                        break;
                }
                
            },
            error: function (data) {
                Mensaje.alerta("Error!!");
            }
        });
    }, 
}

$(document).ready(function(e) {
	$(".fa-search").click(function(){
		Pago.buscar();
	});
	$(".btn-pagar").click(function(){
		Pago.ejecutar();
		return false;
	});
	$('.bs-example-modal-lg').on('show.bs.modal', function (event) {
		$("#modal-detalle-table").DataTable().destroy();
		$("#modal-detalle-table").DataTable();
		var button = $(event.relatedTarget); 
		let saldo = button.data("saldo");
		let monto = button.data("monto");
		let proveedor = button.data("proveedor");
		let guia = button.data("guia");
		item = button.data("id");
        moneda = button.data("moneda");
        tipo = button.data("tipo");
		$("#montopago, #saldopago, #proveedorpago, #guiapago").removeAttr("disabled");
		$("#montopago").val(parseFloat(monto).toFixed(2));
		$("#saldopago").val(parseFloat(saldo).toFixed(2));
        $("#monedapago").val(moneda);
        $("#monedapago").selectpicker("refresh");
		$("#proveedorpago").val(proveedor);
		$("#guiapago").val(guia);
		$("#montopago, #saldopago, #proveedorpago, #guiapago").prop("disabled", true);
		Pago.listarDetalle();
		return;
	});
	$('.bs-example-modal-lg').on('hide.bs.modal', function (event) {
	    item = 0;
        moneda = 0;
	    tipo = "";
	});
    $(document).delegate(".eliminar", "click", function(e){
        id = $(this).data("id");
        url = $(this).data("url");
        

        swal.queue([{
                      title: '¿Estás seguro de eliminarlo?',
                      text: "Este cambio no es reversible!",
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonText: 'Si deseo, eliminarlo!',
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      showLoaderOnConfirm: true
                    }]).then(function () {
                        Pago.eliminar(id, url);
                    });
        
    });
});
Pago.buscar();