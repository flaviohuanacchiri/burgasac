$(".solo-enteros").numeric(false, 
  function() { 
    this.notify("Solo Enteros", "danger");this.value = ""; this.focus(); 
});
$(".decimales").numeric();
$('input').keypress(function(e) {
  if (e.which == 13) {
    $(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();
     e.preventDefault();
  }
});
            /* VALIDATIONS */
$(".onlynumbers").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl/cmd+A
                    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                     // Allow: Ctrl/cmd+C
                    (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                     // Allow: Ctrl/cmd+X
                    (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                     // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

Web = {
  showloading: function(){
    $(".contenedor-spinner").css("display", "block");
  },
  hideloading: function(){
    $(".contenedor-spinner").css("display", "none");
  }

};
Mensaje = {
  alerta : function(mensaje) {
    swal(
      'Oops...',
      mensaje,
      'error'
    )
  },
  confirmacion: function(titulo, mensaje) {
  	swal(
		titulo,
		mensaje,
		'success'
	)
  },
  info: function (titulo, mensaje) {
    swal({
      title: titulo,
      type: 'info',
      html: mensaje
    })
  }
}