var i = 0;
var lotes_in_details = [];
var currentStock;
objdetalleplaneamiento = {};
tabmateriaprima = false;
$(document).ready(function(){
    $("#select_titulo_accesorio").change(function(){
        let titulo_id = $(this).val();
        let proveedor_id = $("[name=proveedor] option:selected").val();
        if (proveedor_id == "") {
            Mensaje.alerta("Elija primero un proveedor");
            return false;
        }
        if (titulo_id!="") {
            $("#select_accesorio").removeAttr("disabled");
            let option = $("#select_accesorio option[data-titulo='"+titulo_id+"']");
            if (option.val()!="undefined" && option.val()!=undefined) {
                option.prop("selected", true);
                $("#cantidad_accesorio").removeAttr("disabled");
                Planeamiento.getStockInsumos(option.val(), proveedor_id);
            } else {
                $("#select_accesorio").val("");
                $("#select_accesorio").prop("disabled", true);
                $("#cantidad_accesorio").val("");
                $("#cantidad_accesorio").prop("disabled", true);
                Mensaje.alerta("No hay un accesorio para el titulo elegido!!!");
                return false;
            }
            $("#select_accesorio").prop("disabled", true);
        } else {
            $("#select_accesorio").val("");
            $("#select_accesorio").prop("disabled", true);
            $("#cantidad_accesorio").val("");
            $("#cantidad_accesorio").prop("disabled", true);
        }
    })
    $(".nav.nav-tabs a").on("shown.bs.tab", function(e){
        tabseleccionadoactive = $(this)[0].hash;
        if (tabseleccionadoactive == "#tab_insumo") {
            tabmateriaprima = true;
        } else {
            tabmateriaprima = false;
        }
    });
	$('#select_insumo').change(function () {
        $('#select_accesorio').val('');
    });
    /* On selected value, update var insumo */
    $('#select_accesorio').change(function () {
        $('#select_insumo').val('');
    });
	$("#planeamiento-form").submit(function(){
		contador = 0;
    	$("#compras_grid_accesorio tbody tr").each(function(){
    	     contador++;
    	});
    	$("#compras_grid_insumo tbody tr").each(function(){
    		contador++;
    	});

    	if (contador < 2) {
    		Mensaje.alerta("Debe elegir Accesorio y Materia Prima!!");
    		return false;
    	}
        let producto_id = $("[name=producto]").val();
        let parseproductos = JSON.parse(productosagrupados);
        let indicadores = parseproductos[producto_id];
        let cantidadinsumos = indicadores.length;
        let contadorinsumos = 0;
        $(".hidden_detalle").each(function() {
            let name = $(this).attr("name");
            match = name.match(/insumo_id/g)
            if (match!=null) {
               if (match.length > 0) {
                    contadorinsumos ++;
                } 
            }
            
        });
        if (cantidadinsumos != contadorinsumos) {
            Mensaje.alerta("Debe ingresar "+cantidadinsumos+" insumos para el Producto Elegido!!!");
            return false;
        }
	});

	$("[name=proveedor], [name=producto]").change(function(){
		proveedor_id = $(this).val();
        producto_id = $("#select_producto").val();
        if (producto_id == "") {
            Mensaje.alerta("Debe escoger un producto!!!!");
            $("[name=proveedor]").val("")
            return false;
        }
		Planeamiento.getLotesporProveedor(proveedor_id, producto_id);
	});


            $("[name='lote_insumo']").change(function () {
               var id = $(this).val();
               var producto = $("#select_producto").val();
               if (producto == "") {
                Mensaje.alerta("Debe escoger un producto a producir");
                $("[name='lote_insumo']").val("");
                return false;
               }
               $.ajax({
                url: '/compras/lote' + '/detalles-insumo',
                data: {proveedor_id : $("[name=proveedor] option:selected").val(), 
                      lote : id,
                      producto_id : producto
                      },

                success:function (detalles) {
                  $("#select_titulo_insumo").empty();
                  $("[name='insumo']").empty();
                  var insumosproducto = [];
                  var indicadores = JSON.parse(listIndicadores);
                  for (var i in indicadores) {
                    if (parseInt(indicadores[i].producto_id) == parseInt(producto)) {
                        insumosproducto.push(parseInt(indicadores[i].insumo_id));
                    }
                  }
                  if (insumosproducto.length > 0) {
                    for (var i = 0; i < detalles.length; i++) {
                        $("#select_titulo_insumo").append('<option value="' + detalles[i].titulo_id   +'" >' + detalles[i].nombre_titulo +'</option>');
                        $("[name='insumo']").append('<option value="' + detalles[i].insumo_id   +'" >' + detalles[i].nombre_insumo +'</option>');
                      }

                  } else {
                    Mensaje.alerta("No existen Materia Prima para este Lote");
                    $("[name='lote_insumo']").val("");
                  }

                  $("[name='insumo']").selectpicker("refresh");
                  
                }
                });
            })

	$(".detalle").click(function() {
          $(this).closest("tr").next().toggle('fast');
          if($(this).text() == '[ + ]')
            $(this).text('[ - ]');
          else
            $(this).text('[ + ]');
        });

	$("#buscar-tabla").click(function () {
        $("#mp-planeamiento").DataTable().destroy();
        Planeamiento.listar();
        return false;
    });
	$('body').on('click', 'a.eliminar', function () {
        let tr  = $(this).parent();
        let detallehidden = tr.find("input");
        if (typeof detallehidden[0] !="undefined" && typeof detallehidden[0] !=undefined) {
            let posicion = parseInt(detallehidden[0].value);
            if (tabmateriaprima) {
                objdetalleplaneamiento[posicion] = undefined;
            }
        }
        $(this).parent().parent().remove();
    });
	/* make enter key act like tab key */
    $('input').keypress(function(e) {
        if (e.which == 13) {
            $(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();
            e.preventDefault();
        }
    });

    $('input#cantidad_paquetes').keypress(function (e) {
               if (e.which == 13) {
                    $('#add_to_grid').click();
                    $('#select_materia_prima').focus();
                    e.preventDefault();
                }
    });

    /* On selected value: insumo or accesorio */
    $('#select_materia_prima').change(function () {
                if ( $(this).val() == 'materia_prima' ){
                    $('#select_accesorio').hide();
                    $('#select_insumo').show();
                }
                else{
                    $('#select_accesorio').show();
                    $('#select_insumo').hide();
                }
    });

                $("[name='cantidad']").keydown(function () {
               var lastVal = $(this).val();
               if(Number($(this).val())>Number(currentStock)) return $(this).val(currentStock);
            }).keyup(function () {
              var lastVal = $(this).val();
              if(Number($(this).val())>Number(currentStock)) return $(this).val(currentStock);
            });

            $("[name='accesorio']").change(function () {
              var id = $(this).val();
              Planeamiento.getStockAccesorios(id);
            });

            $("[name='insumo']").change(function () {
              var id = $(this).val();
              $.ajax({
                url: '/compras/insumos' + '/'+id+'/lotes',
                success:function (lotes) {
                  $("[name='lote_insumo']").empty();
                  for (var i = 0; i < lotes.length; i++) {
                    $("[name='lote_insumo']").append('<option >' + lotes[i].nro_lote   +'</option>');
                  }
                }
              })
            });
$('#add_accesorio_to_grid').click(function () {
                        // titulo_accesorio = $('#select_titulo_accesorio').val();
    nombre_titulo = $('#select_titulo_accesorio option:selected').text();
                        accesorio = $('#select_accesorio').val();
                        cantidad = $('[name="cantidad"]').val();
                        titulo = $("#select_titulo_accesorio").val();
                        nombre_accesorio = $('#select_accesorio option:selected').text();


                if (accesorio!='' &&  cantidad!='')
                {
                    $('#compras_grid_accesorio tbody tr:last').after('<tr>\
                        <td>' + Planeamiento.add_hidden_button(i, 'titulo_id', titulo) + nombre_titulo + '</td>\
                        <td>' + nombre_accesorio + '</td>\
                        <td>' + Planeamiento.add_hidden_button(i, 'cantidad', cantidad) + cantidad + '</td>\
                        <td><a class="eliminar" style="cursor:pointer"><i class="fa fa-remove"></i></a>'
                        + Planeamiento.add_hidden_button(i, 'accesorio_id', accesorio)
                        + '</td></tr>'
                    );
                    i++;

                    $('#planeamiento-form select#proveedor option:not(:selected)').attr('disabled', true);
                    $('#planeamiento-form input#nro_guia').prop('readonly', true).css('cursor', 'not-allowed');
                    $('#planeamiento-form input#nro_comprobante').prop('readonly', true).css('cursor', 'not-allowed');

                    $('#planeamiento-form input.fillable').val('');
                    $("#planeamiento-form select#select_accesorio").val($("#planeamiento-form select#select_accesorio option:first").val());
                    $("#planeamiento-form select#select_titulo_accesorio").val($("#planeamiento-form select#select_titulo_accesorio option:first").val());
                }
                else
                {
                    alert('Para agregar al detalle complete los campos requeridos:\
                        \n- Fecha\
                        \n- Procedencia\
                        \n- Proveedor\
                        \n- Accesorio\
                        \n- Titulo\
                        \n- Cantidad')
                }

                return false;

            });

            $('#add_to_grid').click(function () {
                var fecha_registro = $("[name='fecha']").val(),
                    proveedor = $("[name='proveedor']").val(),
                    empleado = $("[name='empleado']").val(),
                    turno = $("[name='turno']").val(),
                    maquina = $("[name='maquina']").val(),
                    producto = $("[name='producto']").val(),
                    lote_accesorio = $("[name='lote_accesorio']").val(),
                    accesorio = $("[name='accesorio']").val(),
                    cantidad = $("[name='cantidad']").val(),
                    lote_insumo = $("[name='lote_insumo']").val(),
                    insumo  = $("[name='insumo']").val(),
                    titulo  = $("[name='titulo']").val();
                    debugger;


                if (fecha_registro!='' && proveedor != '' && empleado != '' && turno != '' && maquina != '' && producto != '' && lote_accesorio != '' && accesorio != '' && cantidad != '' && lote_insumo != '' && insumo != '' && titulo != '')
                {
                    var cod = turno+maquina+lote_insumo+lote_accesorio;
                    console.log(cod);
                    if(plan().indexOf(cod)>=0) return alert('Ya existe un trabajador con esa maquina y turno');

                    $('#compras_grid tbody tr:last').after(
                      '<tr>\
                        <td>' + Planeamiento.add_hidden_button(i, 'fecha', fecha_registro) + fecha_registro + '</td>\
                        <td>' + Planeamiento.add_hidden_button(i, 'empleado', empleado) + $("[name='empleado']").find("[value='"+ empleado +"']").html()  + '</td>\
                        <td class="turno">' + Planeamiento.add_hidden_button(i, 'turno', turno) + turno + '</td>\
                        <td class="maquina">' + Planeamiento.add_hidden_button(i, 'maquina', maquina) + $("[name='maquina']").find("[value='"+ maquina +"']").html()   + '</td>\
                        <td>' +  Planeamiento.add_hidden_button(i, 'cantidad', cantidad) + Planeamiento.add_hidden_button(i, 'producto', producto) + $("[name='producto']").find("[value='"+ producto +"']").html()  + '</td>\
                        <td class="accesorio">' + Planeamiento.add_hidden_button(i,'accesorio',accesorio)  + add_hidden_button(i, 'lote_accesorio', lote_accesorio) + lote_insumo + '</td>\
                        <td class="insumo">' +  Planeamiento.add_hidden_button(i, 'lote_insumo', lote_insumo) + Planeamiento.add_hidden_button(i, 'insumo', insumo) + $("[name='insumo']").find("[value='"+ insumo +"']").html() + '</td>\
                        <td>' + Planeamiento.add_hidden_button(i, 'titulo', titulo) + $("[name='titulo']").find("[value='"+ titulo +"']").html() + '</td>\
                        <td><a class="eliminar" style="cursor:pointer"><i class="fa fa-remove"></i></a>'
                        + (insumo != ''? Planeamiento.add_hidden_button(i, 'insumo_id', insumo, insumo) : Planeamiento.add_hidden_button(i, 'accesorio_id', accesorio))
                        + '</td></tr>'
                    );
                    i++;
                }
                else
                {
                    alert('Para agregar al detalle complete los campos requeridos:\
                        \n- Fecha Registro\
                        \n- Proveedor\
                        \n- Tejedor\
                        \n- Turno\
                        \n- Maquina\
                        \n- Producto a prod\
                        \n- Accesorios\
                        \n- Materia Prima')
                }

                return false;
            });

            $('#add_insumo_to_grid').click(function () {
                        /* campos de compra */
                        nro_lote = $('#nro_lote').val();
                        insumo = $('#select_insumo_t').val();
                        titulo_insumo = $('#select_titulo_insumo').val();
                        nombre_insumo = $('#select_insumo_t option:selected').text();
                        nombre_titulo = $('#select_titulo_insumo option:selected').text();
                        if (typeof objdetalleplaneamiento[insumo]!="undefined" && typeof objdetalleplaneamiento[insumo] != undefined) {
                            alert("Ya agrego el Insumo Seleccionado!!!");
                            return;
                        }
                        objdetalleplaneamiento[insumo] = insumo;
                if (nro_lote!='' &&
                    insumo!='' && titulo_insumo!='')
                {


    $('#compras_grid_insumo tbody tr:last').after('<tr>\
        <td>' + Planeamiento.add_hidden_button(i, 'nro_lote', nro_lote) + '<span class="lotes">'+ nro_lote + '</span></td>\
        <td>' + Planeamiento.add_hidden_button(i, 'insumo', insumo, insumo) + nombre_insumo + '</td>\
        <td>' + Planeamiento.add_hidden_button(i, 'titulo', titulo_insumo) + nombre_titulo + '</td>\
        <td><a class="eliminar" style="cursor:pointer"><i class="fa fa-remove"></i></a>'
        + Planeamiento.add_hidden_button(i, 'insumo_id', insumo, insumo)
        + '</td></tr>'
    );
                    i++;

                    $('#planeamiento-form select#proveedor option:not(:selected)').attr('disabled', true);
                    $('#planeamiento-form input#nro_guia').prop('readonly', true).css('cursor', 'not-allowed');
                    $('#planeamiento-form input#nro_comprobante').prop('readonly', true).css('cursor', 'not-allowed');

                    $('#planeamiento-form input.fillable').val('');
                    $("#planeamiento-form select#select_insumo").val($("#planeamiento-form select#select_insumo option:first").val());
                    $("#planeamiento-form select#select_titulo_insumo").val($("#planeamiento-form select#select_titulo_insumo option:first").val());

                }
                else
                {
                    alert('Para agregar al detalle complete los campos requeridos:\
                        \n- Fecha\
                        \n- Procedencia\
                        \n- Proveedor\
                        \n- Lote\
                        \n- Materia Prima\
                        \n- Titulo\
                        \n- Peso Bruto\
                        \n- Peso Tara\
                        \n- Cantidad')
                }

                return false;
            });
    $('#compras_grid tbody tr td.show_peso_tara input').on('keyup', function() {
        show_peso_tara = $(this).val();
        show_peso_bruto = $(this).parent().parent().find('td.show_peso_bruto input').val();
        show_peso_neto = show_peso_bruto - show_peso_tara;
        show_peso_neto = parseFloat(Math.round( show_peso_neto * 100) / 100).toFixed(2);
        $(this).parent().parent().find('td.show_peso_neto').html(show_peso_neto);
    });


            /* actualizar peso_neto */
            $('#compras_grid tbody tr td.show_peso_bruto input').on('keyup', function() {
                show_peso_bruto = $(this).val();
                show_peso_tara = $(this).parent().parent().find('td.show_peso_tara input').val();
                show_peso_neto = show_peso_bruto - show_peso_tara;
                $(this).parent().parent().find('td.show_peso_neto').html(show_peso_neto);
            });

});

var Planeamiento = {
	listar : function() {
		$("#mp-planeamiento").DataTable({
          processing: true,
          serverSide: true,
          "ajax":{
            url:'planeamientos',
            data:function(d){
              return $.extend( {}, d, {
                "proveedor": $('#proveedor_table').val(),
                "empleado":$("#empleado_table").val(),
                "fecha_inicio" : $("#fecha_table").val(),
                "fecha_fin" : $("#fecha_fin_table").val(),
                "turno" : $("#turno_table").val(),
                "estado" : $("#estado_table").val(),
                "producto": $("#producto_table").val(),
                "maquina":$("#maquina_table").val()
              });
            },
            dataSrc: function (json) {
              var planeamientos = json.data,
              return_data = [];
              for (var i = 0,planeamiento; planeamiento = planeamientos[i]; i++) {
                var data = {};
                data.fecha      = planeamiento.fecha;
                data.proveedor  = planeamiento.proveedor.nombre_comercial;
                data.empleado   = planeamiento.empleado.nombres + " " + planeamiento.empleado.apellidos;
                data.turno      = planeamiento.turno;
                data.maquina    = planeamiento.maquina.nombre;
                data.producto   = planeamiento.producto.nombre_generico;

                data.rollos     = planeamiento.rollos;
                data.kg_producidos = planeamiento.kg_producidos;
                data.kg_falla     = planeamiento.kg_falla;

                for (var j = 0,detalle; detalle = planeamiento.detalles[j]; j++) {
                  data.lote       = detalle.lote_insumo;
                  data.mp         = detalle.insumo? detalle.insumo.nombre_generico:detalle.accesorio.nombre;
                  data.titulo     = detalle.titulo.nombre;
                  data.cajas      = detalle.cajas;
                  data.Kg         = detalle.kg;

                  return_data.push(jQuery.extend(true, {}, data));
                }
              }
              console.log(return_data);
              return return_data;
            }
          },
          "columns": [
            { "data": "fecha", name:"fecha" },
            { "data": "proveedor", name:"proveedor.nombre_comercial" },
            { "data": "empleado",name:"detalle_planeamientos.empleado.nombre_comercial" },
            { "data": "turno", name:"detalle_planeamientos.turno" },
            { "data": "maquina", name:"detalle_planeamientos.maquina.nombre" },
            { "data": "producto",name:"producto.nombre_generico"},

            { "data": "lote",name:"detalle_planeamientos.lote_insumo"},
            { "data": "mp",name:"insumo.nombre_generico"},
            { "data": "titulo",name:"titulos.nombre"},
            { "data": "cajas",name:"detalle_planeamientos.cajas"},
            { "data": "Kg",name:"detalle_planeamientos.Kg"},
            { "data": "rollos",name:"rollos"},
            { "data": "kg_producidos",name:"kg_producidos"},
            { "data": "kg_falla",name:"kg_falla"}

          ],
          "fnDrawCallback":function (oSettings) {
              $(".delete-detalle-planeamientos").click(function(){
                var detalle_id = $(this).attr('data-detalle-id');
                $.ajax({
                  url:'planeamientos'+detalle_id,
                  type:'DELETE',
                  success:function () {
                    bandeja.ajax.reload();
                  }
                });
                return false;
              });

          },
        });
	},
	getCurrentStock : function() {
      var accesorio = $("[name='accesorio']").val();
      var proveedor = $("[name='proveedor']").val();
      if (accesorio !="" && proveedor!="") {
        Planeamiento.getStockInsumos(accesorio, proveedor);
    }

    },

    getStockAccesorios :function(id) {
    	$("[name='lote_accesorio']").trigger("change");
              $.ajax({
                  url: '/compras/accesorios' + '/' + id + '/lotes',
                  success:function (lotes) {
                    $("[name='lote_accesorio']").empty();
                    for (var i = 0; i < lotes.length; i++) {
                      $("[name='lote_accesorio']").append('<option >' + lotes[i].nro_lote   +'</option>');
                    }
                    console.log('holaaa');
                    Planeamiento.getCurrentStock();
                  }
              });
    },

    getStockInsumos : function(accesorio, proveedor) {
    	$.ajax({
        url: '/insumo' + '/' + accesorio + '/proveedor/' + proveedor + '/stock',
        success:function (stock) {
          currentStock = stock;
          $("[name='cantidad']").val(currentStock);
          if(Number($("[name='cantidad']").val())>Number(currentStock)) return $("[name='cantidad']").val(currentStock);
          if (currentStock <=0) {
            $("#select_accesorio").val("");
            $("#select_accesorio").prop("disabled", true);
          }
        }
      });
    },

    add_hidden_button: function(j, fieldname, value) {
        return '<input class="hidden_detalle" type="hidden" name="detalles[' + j + '][' + fieldname + ']" value="' + value + '">';
    },

    getLotesporProveedor : function (proveedor_id, producto_id) {
        let fecplaneamiento = $("input[name=fecha]").val();
    	let lotes = $.ajax({
			type: "POST",
			url : "/lotesporproveedor",
			data : {proveedor_id: proveedor_id, producto_id : producto_id, fecha : fecplaneamiento},
			headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').val()
            },
			success: function(obj){
			html = "<option value=''>Seleccione</option>";
			for(var i in obj) {
				html+="<option value='"+obj[i].nro_lote+"'>"+obj[i].nro_lote+"</option>";
			}
			$("#nro_lote").html(html);
			}
		});
    }
}
