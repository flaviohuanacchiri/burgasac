planeamientoOpt = 0;
let cantidad_max = 0, rollos_max = 0;
Despacho = {
  getStockTela : function(producto_id, proveedor_id, nro_lote) {
    $.ajax({
         url: "/stockporlote",
         data : {producto_id : producto_id, proveedor_id : proveedor_id, nro_lote : nro_lote, primero: 0},
        success:function (obj) {
            if (obj.rst == 1) {
              stock = obj.stock;
              cantidad_max = stock.cantidad;
              rollos_max = stock.rollos;
              $("[name=kg], [name=rollos]").removeAttr("disabled");
              $("[name='kg']").val(stock.cantidad);
              $("[name='rollos']").val(stock.rollos);
            }
            
       }
     });
    return false;
  },
  getLotesporProducto : function(producto_id, proveedor_id) {
      $.get({
        url : "/lotesconstock",
        data: {producto_id : producto_id, proveedor_id : proveedor_id},
        success: function(obj) {
          var html = "";
          if (obj.rst == 1) {
            html+="<option value=''>Seleccione</option>";
            for(var i in obj.lotes) {
              html+="<option value='"+obj.lotes[i].nro_lote+"' data-planeamiento='"+obj.lotes[i].idplanea+"'>"+obj.lotes[i].nro_lote+"</option>";
            }
            
            $("[name='nrolote']").html(html);
            $("[name='nrolote']").removeAttr("disabled");
            $("[name='nrolote']").selectpicker("refresh");
          }
          if (obj.rst == 2) {
            Mensaje.alerta("No existen lotes disponibles!!!!");
            $("[name='nrolote']").html(html);
            $("[name='nrolote']").prop("disabled", true);
          }
        }
    });
  },
  getProductosProducidos : function(proveedor_id) {
      $.ajax({
         url: "/proveedor/"+proveedor_id+"/productos",
        success:function (productos) {
          if(productos.length > 0) {
            let htmlproducto = "<option value=''>Seleccione</option>";
            for(let i in productos) {
              let producto = productos[i];
              htmlproducto+="<option value='"+producto.producto.id+"'>"+producto.producto.nombre_generico+"</option>";
            }
            $("[name='producto']").html(htmlproducto);
            $("[name='producto']").removeAttr("disabled");
            $("[name='producto']").selectpicker("refresh");
          } else {
            Mensaje.alerta("No hay Productos Producidos para el Proveedor Seleccionado");
          }
       }
     });
    return false;
  },
  limpiarInpus : function() {
    $("[name=kg], [name=rollos]").removeAttr("disabled");
    $("[name=kg], [name=rollos]").val("");
    $("[name=kg], [name=rollos]").prop("disabled", true);
  }
}
var productos_in_details = function(){
            let lotes = [];
            $("#despachos_grid tbody tr").map(function () {
              var producto = $(this).find(".producto").find("input").val();
              var proveedor =  $(this).find(".proveedor").find("input").val();
              lotes.push(producto+proveedor);
            })
            return lotes;
          };

$(document).ready(function(){
  $("#planeamiento-form").submit(function(e){
    $("#planeamiento-form").append("<input type='hidden' name='planeamiento_id'  value='"+planeamientoOpt+"' />");
  });
	$(".btn-boleta").click(function(e){
        var despachoid = $(this).data("despacho");
        var url = "/boletadespachotercero?id="+despachoid;
        window.open(url, ",mywin", "left=20, top=20, width=900, height = 900, toolbar=no, directories=no, menubar=no, status=no, resizable=1");
    });
    $('input[type=radio]').change(function() {
        $('input[type=radio]:checked').not(this).prop('checked', false);
    });
    $("[name='proveedor']").change(function(){
      Despacho.limpiarInpus();
      let proveedor_id = $(this).find("option:selected").val();
      $("[name='producto']").removeAttr("disabled");
      $("[name='producto']").html("");
      $("[name='producto']").selectpicker("refresh");

      $("[name='nrolote']").removeAttr("disabled");
      $("[name='nrolote']").html("");
      $("[name='nrolote']").selectpicker("refresh");

      if(proveedor_id!="") {
        Despacho.getProductosProducidos(proveedor_id);
        let direccion = "";
        if (typeof listproveedores[proveedor_id]!="undefined" && typeof listproveedores[proveedor_id]!=undefined ) {
          direccion = listproveedores[proveedor_id].direccion;
          $("#direccionllegada").val(direccion);
        }
      } else {
        $("[name='producto']").prop("disabled", true);
      }
    });
    $("[name='producto']").change(function () {
      Despacho.limpiarInpus();
      $("[name='nrolote']").removeAttr("disabled");
      $("[name='nrolote']").html("");
      $("[name='nrolote']").selectpicker("refresh");

      
        let id = $(this).find("option:selected").val();
        let proveedor_id = $("[name='proveedor']").val();
        if (proveedor_id == "") {
          Mensaje.alerta("Elija un Proveedor!!!");
          return false;
        }
        if (id == "") {
          Mensaje.alerta("Elija un Producto!!!");
          return false;
        }
        if (id!="" && proveedor_id!="") {
          Despacho.getLotesporProducto(id, proveedor_id);
        }
        
        return false;

     });
    $("[name='nrolote']").change(function(){
      Despacho.limpiarInpus();
      let select = $(this).find("option:selected");
      let nro_lote = $("[name='nrolote']").val();
      let proveedor_id = $("[name='proveedor']").val();
      let producto_id = $("[name='producto']").val();
      if (nro_lote!="") {
        Despacho.getStockTela(producto_id, proveedor_id, nro_lote);
      }

        if (select.val()!="") {
          let planeamientoid = select.data("planeamiento");
          planeamientoOpt = select.data("planeamiento");
        } else {
          planeamientoOpt = 0;
        }
    })

    $("#agregar-materia").click(function () {
            var id = $(".select-details:checked").val();
            var tr = $(".select-details:checked").parent().parent();
            if(tr.length){
              var cajas = Number($("[name='cajas']").val());
              var materia_prima = Number($("[name='materia_prima']").val());
              if(cajas&&materia_prima){
                var data ={
                  caja: cajas,
                  materia: materia_prima
                }
                tr.find(".cajas").find("input").val(cajas);
                tr.find(".materia").find("input").val(materia_prima);
                tr.find(".cajas").find("span").html(cajas)
                tr.find(".materia").find("span").html(materia_prima)


              }else {
                Mensaje.alerta('Complete los campos por favor')
              }
            }else {
              Mensaje.alerta('Seleccione un campo')
            }
          });
    $("[name='accesorio']").change(function () {
              var id = $(this).val();
              $.ajax({
                url: "{{url('compras/accesorios')}}" + '/'+id + '/lotes',
                success:function (lotes) {
                  $("[name='lote_accesorio']").empty();
                  for (var i = 0; i < lotes.length; i++) {
                    $("[name='lote_accesorio']").append('<option >' + lotes[i].nro_lote   +'</option>');
                  }
                }
              })
            });
    $("[name='insumo']").change(function () {
              var id = $(this).val();
              $.ajax({
                url: "{{url('compras/insumos')}}" + '/'+id+'/lotes',
                success:function (lotes) {
                  $("[name='lote_insumo']").empty();
                  for (var i = 0; i < lotes.length; i++) {
                    $("[name='lote_insumo']").append('<option >' + lotes[i].nro_lote   +'</option>');
                  }
                }
              })
            });

               /* FUNCIONALITY */
            /* On selected value: insumo or accesorio */
            $('#select_materia_prima').change(function () {
                if ( $(this).val() == 'materia_prima' ){
                    $('#select_accesorio').hide();
                    $('#select_insumo').show();
                }
                else{
                    $('#select_accesorio').show();
                    $('#select_insumo').hide();
                }
            });

            /* On selected value, update var accesorio */
            $('#select_insumo').change(function () {
                $('#select_accesorio').val('');
            });
            /* On selected value, update var insumo */
            $('#select_accesorio').change(function () {
                $('#select_insumo').val('');
            });
                /* make enter key act like tab key */
});
$(document).ready(function(){
              $('input').keypress(function(e) {
                if (e.which == 13) {
                    $(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();
                    e.preventDefault();
                    switch(e.target.id) {
                      case "licenciatransportista":
                        $("#nroguia").focus();
                        break;
                      default:
                        break;
                    }
                }
            });

});