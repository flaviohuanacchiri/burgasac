var i = 0;
tipo_balanza = 2;
listDetalle = {};
planeamientoOpt = 0;
var productos_in_details = function(){
            let lotes = [];
            $("#despachos_grid tbody tr").map(function () {
              var producto = $(this).find(".producto").find("input").val();
              var proveedor =  $(this).find(".proveedor").find("input").val();
              lotes.push(producto+proveedor);
            })
            return lotes;
          };

          $('input[type=radio]').change(function() {
              $('input[type=radio]:checked').not(this).prop('checked', false);
          });

          var cantidad_max = 0,
              rollos_max = 0;


          function checkStockKG() {
              var kg = $(this).val();
              //if(kg>cantidad_max) return $(this).val(cantidad_max);
          }

          function checkStockRollos(){
            var rollos = $(this).val();
            //if(rollos>rollos_max) return $(this).val(rollos_max);
          }
$(document).ready(function(){
  $("#lote").change(function(){
    let select = $(this).find("option:selected");
      if (select.val()!="") {
          let planeamientoid = select.data("planeamiento");
          planeamientoOpt = select.data("planeamiento");
        } else {
          planeamientoOpt = 0;
        }
    
  });

  $("[name=kg]").prop("disabled", true);
	$("#proveedor").on('change', function(e){
    var proveedorseleccionado = $("[name=proveedor] option:selected").val();
    DespachoTintoreria.colorPorProveedor(proveedorseleccionado);
    var direccion = "";
    if (typeof listproveedores[proveedorseleccionado]!="undefined" && typeof listproveedores[proveedorseleccionado]!=undefined ) {
      direccion = listproveedores[proveedorseleccionado].direccion;
      $("#direccionllegada").val(direccion);
    }
		
	});
  $("[name=peso_balanza]").change(function(){
    tipo_balanza = parseInt($(this).val());
    if (tipo_balanza == 1) {
      timer = false;
        $("[name=kg]").val("0.00");
        $("[name=kg]").prop("disabled", true);
    }
    if (tipo_balanza == 2) {
      timer = true;
      $("[name=kg]").removeAttr("disabled");
      $("[name=kg]").val("");
    }
  });
  $("[name=producto]").change(function(){
    //DespachoTintoreria.stockProveedor();
  });
  $("[name=lote]").change(function(){
    DespachoTintoreria.stockPorLote();
  });

	$(".btn-boleta").click(function(e){
        var despachoid = $(this).data("despacho");
        var url = "/boletadespachotintoreria?id="+despachoid;
        window.open(url, "_blank");
    });

	$("#agregar-materia").click(function () {
            var id = $(".select-details:checked").val();
            var tr = $(".select-details:checked").parent().parent();
            if(tr.length){
              var cajas = Number($("[name='cajas']").val());
              var materia_prima = Number($("[name='materia_prima']").val());
              if(cajas&&materia_prima){
                var data ={
                  caja: cajas,
                  materia: materia_prima
                }
                tr.find(".cajas").find("input").val(cajas);
                tr.find(".materia").find("input").val(materia_prima);
                tr.find(".cajas").find("span").html(cajas)
                tr.find(".materia").find("span").html(materia_prima);
              }else {
                Mensaje.alerta('Complete los campos por favor')
              }
            }else {
              Mensaje.alerta('Seleccione un campo')
            }
          });
    $("#planeamiento-form").submit(function(e){
      var numproveedores  = 0;
      var proveedores = {};
      var productos = {};
      var nro_lotes = {};
      $('td.proveedor').each(function(e) {
        var columna = $(this).find("input[type=hidden]");
        proveedores[columna.val()] = true;
      });
      $('td.producto').each(function(e) {
        var columna = $(this).find("input[type=hidden]");
        productos[columna.val()] = true;
      });
      $('td.nro_lote').each(function(e) {
        var columna = $(this).find("input[type=hidden]");
        nro_lotes[columna.val()] = true;
      });
      if (Object.keys(proveedores).length > 1) {
        Mensaje.alerta("Solo puede elegir un proveedor!!!");
        return false;
      }
      if (Object.keys(productos).length > 1) {
        Mensaje.alerta("Solo puede elegir un producto!!!");
        return false;
      }
      if (Object.keys(nro_lotes).length > 1) {
        Mensaje.alerta("Solo puede elegir un lote!!!");
        return false;
      }
      $("#planeamiento-form").append("<input type='hidden' name='planeamiento_id'  value='"+planeamientoOpt+"' />");
    });
	/* TAB FOCUS */
    $('#select_materia_prima').focus();

            /* make enter key act like tab key */
            $('input').keypress(function(e) {
                if (e.which == 13) {
                    $(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();
                    e.preventDefault();
                    switch(e.target.id) {
                      case "licenciatransportista":
                        $("#nroguia").focus();
                        break;
                      default:
                        break;
                    }
                }
            });

            $('input#cantidad_paquetes').keypress(function (e) {
               if (e.which == 13) {
                    $('#add_to_grid').click();
                    $('#select_materia_prima').focus();
                    e.preventDefault();
                }
            });
	$("[name='kg']").keydown(checkStockKG).keyup(checkStockKG);
          $("[name='rollos']").keydown(checkStockRollos).keyup(checkStockRollos);

    $('#select_materia_prima').change(function () {
                if ( $(this).val() == 'materia_prima' ){
                    $('#select_accesorio').hide();
                    $('#select_insumo').show();
                }
                else{
                    $('#select_accesorio').show();
                    $('#select_insumo').hide();
                }
            });

            /* On selected value, update var accesorio */
            $('#select_insumo').change(function () {
                $('#select_accesorio').val('');
            });
            /* On selected value, update var insumo */
            $('#select_accesorio').change(function () {
                $('#select_insumo').val('');
            });
   $("[name='producto']").change(function () {
        var id = $(this).val();
       // DespachoTintoreria.proveedoresPorProducto(id);
       DespachoTintoreria.loteProducto();
    });

    $('#add_to_grid').click(function () {
		var fecha_registro = $("[name='fecha']").val(),
        producto = $("[name='producto']").val(),
        color = $("[name='color']").val(),
        proveedor = $("[name='proveedor']").val(),
        rollos = $("[name='rollos']").val(),
        kg = $("[name='kg']").val(),
        nro_lote = $("[name=lote]").val();
		var cod = producto + proveedor;
        if (fecha_registro!='' && producto != '' && color != '' && rollos != '' && kg != '' && proveedor != '' && nro_lote!='')
        {
            //if(productos_in_details().indexOf(cod)>=0) return Mensaje.alerta('Ya existe un trabajador con esa maquina y turno');
            $('#despachos_grid tbody tr:last').after('<tr>\
                <td>' + DespachoTintoreria.add_hidden_button(i, 'fecha', fecha_registro) + fecha_registro + '</td>\
                <td class="proveedor">' +  DespachoTintoreria.add_hidden_button(i, 'proveedor', proveedor) + $("[name='proveedor'] option:selected").text() + '</td>\
                <td class="producto">' +  DespachoTintoreria.add_hidden_button(i, 'producto', producto) + $("[name='producto'] option:selected").text() + '</td>\
                <td class="nro_lote">' +  DespachoTintoreria.add_hidden_button(i, 'nro_lote', nro_lote) + $("[name='lote'] option:selected").text() + '</td>\
                <td>' +  DespachoTintoreria.add_hidden_button(i, 'color', color) + $("[name='color'] option:selected").text() + '</td>\
                <td>' +  DespachoTintoreria.add_hidden_button(i, 'kg', kg) + $("[name='kg']").val() + '</td>\
                <td>' +  DespachoTintoreria.add_hidden_button(i, 'rollos', rollos) + $("[name='rollos']").val() + '</td>\
                <td><a class="eliminar" style="cursor:pointer" data-position="'+i+'"><i class="fa fa-remove"></i></a>'
                + '</td></tr>'
            );
            if (typeof detalleTintoreria[producto] == "undefined" || typeof detalleTintoreria[producto] == undefined) {
              detalleTintoreria[producto] = {};
            }
            if (typeof detalleTintoreria[producto][nro_lote] == "undefined" || typeof detalleTintoreria[producto][nro_lote] == undefined) {
              detalleTintoreria[producto][nro_lote] = {peso : cantidad_max, rollos : rollos_max};
            }
            detalleTintoreria[producto][nro_lote].peso -= $("[name=kg]").val();
            detalleTintoreria[producto][nro_lote].rollos -=$("[name=rollos]").val();

            $("[name=kg]").val(detalleTintoreria[producto][nro_lote].peso);
            $("[name=rollos]").val(detalleTintoreria[producto][nro_lote].rollos);
            i++;
        } else {
            Mensaje.alerta('Para agregar al detalle complete los campos requeridos:\
                        \n- Fecha Registro\
                        \n- Número de Lote\
                        \n- Producto\
                        \n- Marca\
                        \n- Titulo\
                        \n- Peso Bruto\
                        \n- Peso Tara\
                        \n- Cantidad de Caja/Bolsas')
        }
        if (tipo_balanza == 1) {
          timer = true;
          let inputpeso = $("input[name=kg]");
          let radio = parseInt($("input[name='peso_balanza']:checked").val());
          Balanza.limpiar(radio, inputpeso, true);
          timer = false;
        } else {
          timer = false;
        }

        return false;
    });

            /* actualizar peso_neto */
            $('#compras_grid tbody tr td.show_peso_bruto input').on('keyup', function() {
                show_peso_bruto = $(this).val();
                show_peso_tara = $(this).parent().parent().find('td.show_peso_tara input').val();
                show_peso_neto = show_peso_bruto - show_peso_tara;
                $(this).parent().parent().find('td.show_peso_neto').html(show_peso_neto);
            });

            $('#compras_grid tbody tr td.show_peso_tara input').on('keyup', function() {
                show_peso_tara = $(this).val();
                show_peso_bruto = $(this).parent().parent().find('td.show_peso_bruto input').val();
                show_peso_neto = show_peso_bruto - show_peso_tara;
                show_peso_neto = parseFloat(Math.round( show_peso_neto * 100) / 100).toFixed(2);
                $(this).parent().parent().find('td.show_peso_neto').html(show_peso_neto);
            });

            /* eliminar tr */
            $('body').on('click', 'a.eliminar', function () {
                let indicegenerado = $(this).data("position");
                let productofila = "";
                let lotefila = "";
                let pesofila = 0;
                let rollofila = 0;
                $(this).closest('tr').find("input[type=hidden]").each(function() {
                    let name = $(this).attr("name");
                    switch(name) {
                      case "detalles["+indicegenerado+"][producto]":
                        productofila = $(this).val();
                        break;

                      case "detalles["+indicegenerado+"][nro_lote]":
                        lotefila = $(this).val();
                        break;
                      case "detalles["+indicegenerado+"][kg]":
                        pesofila = $(this).val();
                        break;
                      case "detalles["+indicegenerado+"][rollos]":
                        rollofila = $(this).val();
                        break;
                      default:
                        break;
                    }
                });

                detalleTintoreria[productofila][lotefila].peso = parseFloat(pesofila) + parseFloat(detalleTintoreria[productofila][lotefila].peso);
                detalleTintoreria[productofila][lotefila].rollos = parseFloat(rollofila) + parseFloat(detalleTintoreria[productofila][lotefila].rollos);
                //detalleTintoreria[producto][nro_lote].peso -= $("[name=kg]").val();
                //detalleTintoreria[producto][nro_lote].rollos -=$("[name=rollos]").val();

                $("[name=kg]").val(detalleTintoreria[productofila][lotefila].peso);
                $("[name=rollos]").val(detalleTintoreria[productofila][lotefila].rollos);
                $(this).parent().parent().remove();
                i--;
            });
        $("[name='accesorio']").change(function () {
              var id = $(this).val();
              $.ajax({
                url:'/compras/accesorios/'+id + '/lotes',
                success:function (lotes) {
                  $("[name='lote_accesorio']").empty();
                  for (var i = 0; i < lotes.length; i++) {
                    $("[name='lote_accesorio']").append('<option >' + lotes[i].nro_lote   +'</option>');
                  }
                }
              })
            });
        $("[name='insumo']").change(function () {
              var id = $(this).val();
              $.ajax({
                url:'compras/insumos/'+id+'/lotes',
                success:function (lotes) {
                  $("[name='lote_insumo']").empty();
                  for (var i = 0; i < lotes.length; i++) {
                    $("[name='lote_insumo']").append('<option >' + lotes[i].nro_lote   +'</option>');
                  }
                }
              })
            });
});

var DespachoTintoreria = {
	colorPorProveedor : function(proveedor_id, color_id) {
		var colores = $.ajax({
			type: "POST",
			url: "/coloresporproveedorproducto", 
			data: {proveedor_id: proveedor_id, color_id: color_id, producto_id : $("[name=producto]").val()}, 
			headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').val()
            },
            success : function(data){
            	DespachoTintoreria.pintarComboColor(data);
            } 
        });
	},
	pintarComboColor : function(data) {
		if (data.rst == 1) {
			html="<option value=''>Seleccione</option>";
			var colores = data.data;
			for(var i in colores) {
				html+="<option value='"+colores[i].color_id+"'>"+colores[i].codigocolor+"</option>";
			}
			$("[name=color]").removeAttr("disabled");
			$("[name=color]").html(html);
			$("[name=color]").selectpicker();
		    $("[name=color]").selectpicker("refresh");
		}

		if (data.rst == 2 || data.rst == "2") {
			html="<option value=''>Seleccione</option>";
			Mensaje.alerta(data.msj);
			$("[name=color]").html(html);
			$("[name=color]").prop("disabled", true);
		}
	},
	proveedoresPorProducto: function(producto_id) {
		$.ajax({
            url:'/producto/'+ producto_id + '/proveedores',
                success:function (obj) {
                var html = "<option value=''>Seleccione</option>";
                console.log(obj.length);
	                if (obj.length > 0) {
	                	for (var i = 0; i < obj.length; i++) {
		                    html+='<option value="'+ obj[i].proveedor_id + '" >' + obj[i].nombre_comercial   +'</option>';
		                }
		                
		                $("[name='proveedor']").removeAttr("disabled");
		                $("[name='proveedor']").html(html);
		            	$("[name='proveedor']").selectpicker();
		            	$("[name='proveedor']").selectpicker("refresh");
	                  	//
	                } else {
	                	$("[name='proveedor']").html(html);
	                	$("[name='proveedor']").prop("disabled", true);
	                }
                }
        });
	},
	stockProveedor : function() {
		var id = $("[name='producto']").val()
        var proveedor_id = $("[name='proveedor']").val();
        $.ajax({
              url: "/telas/"+id+'/proveedor/3/stock',
              success:function (stock) {
                /*cantidad_max = stock.cantidad;
                rollos_max = stock.rollos;
                $("[name='kg']").val(stock.cantidad);
                $("[name='rollos']").val(stock.rollos);*/
              }
        });
	},
  loteProducto : function() {
    var producto_id = $("[name='producto']").val();
    let proveedor_id = null;
    if (proveedorburga!="null" && proveedorburga!=null) {
      proveedor_id = proveedorburga.id;
    }
    $.get({
      url : "/lotesconstock",
      data: {producto_id : producto_id, proveedor_id : proveedor_id},
      success: function(obj) {
        var html = "";
        if (obj.rst == 1) {
          html+="<option>Seleccione</option>";
          for(var i in obj.lotes) {
            html+="<option value='"+obj.lotes[i].nro_lote+"' data-planeamiento='"+obj.lotes[i].idplanea+"'>"+obj.lotes[i].nro_lote+"</option>";
          }
          $("#lote").removeAttr("disabled");
          $("#lote").html(html);
          $("#lote").selectpicker();
          $("#lote").selectpicker("refresh");
        }
        if (obj.rst == 2) {
          Mensaje.alerta("No existen lotes disponibles!!!!");
          $("#lote").html(html);
          $("#lote").prop("disabled", true);
        }
      }
    });
  },
  stockPorLote : function() {
    var nro_lote = $("[name='lote']").val();
    var producto_id = $("[name='producto']").val();
    let proveedor_id = null;
    if (proveedorburga!="null" && proveedorburga!=null) {
      proveedor_id = proveedorburga.id;
    }
    if (typeof detalleTintoreria[producto_id] !="undefined" && typeof detalleTintoreria[producto_id] !=undefined) {
      if (typeof detalleTintoreria[producto_id][nro_lote] !="undefined" && typeof detalleTintoreria[producto_id][nro_lote] !=undefined) {
        cantidad_max = detalleTintoreria[producto_id][nro_lote].peso;
        rollos_max = detalleTintoreria[producto_id][nro_lote].rollos;
        $("[name='kg'], [name='rollos']").removeAttr("disabled");
        $("[name='kg']").val(cantidad_max);
        $("[name='rollos']").val(rollos_max);
      } else {
  
      }
    } else {
                $.get({
              url : "/stockporlote",
              data: {producto_id : producto_id, proveedor_id : proveedor_id, nro_lote: nro_lote},
              success: function(obj) {
                var html = "";
                if (obj.rst == 1) {
                  cantidad_max = obj.stock.cantidad;
                  rollos_max = obj.stock.rollos;

                  $("[name='kg'], [name='rollos']").removeAttr("disabled");
                  $("[name='kg']").val(obj.stock.cantidad);
                  $("[name='rollos']").val(obj.stock.rollos);
                  
                }
                if (obj.rst == 2) {
                  Mensaje.alerta("No existen lotes disponibles!!!!");
                  $("[name='kg']").val(0);
                  $("[name='rollos']").val(0);
                  $("[name='kg'], [name='rollos']").prop("disabled", true);
                  return false;
                }
              }
            });
    }


  },
	add_hidden_button : function(j, fieldname, value) {
        return '<input type="hidden" name="detalles[' + j + '][' + fieldname + ']" value="' + value + '">';
    }
}