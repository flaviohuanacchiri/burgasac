var Reporte = {
	buscar : function() {
		$('#table-reporte').DataTable().destroy();
		return $('#table-reporte').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            ajax: url+'?fechafiltroinicio='+$("#fechafiltroinicio").val()+'&fechafiltrofin='+$("#fechafiltrofin").val()+'&proveedorfiltro='+$("#proveedorfiltro").val()+'&productofiltro='+$("#productofiltro").val()+'&colorfiltro='+$("#colorfiltro").val(),
            columns: [
            	{data: 'created_at', name: 'created_at'},
				{"data": function ( row, type, val, meta ) {
                    if (row.proveedor !=null) {
                        return row.producto.nombre_generico;
                    }
                    return "";
                }, name: 'producto'},
				{"data": function ( row, type, val, meta ) {
                    if (row.proveedor !=null) {
                        return row.proveedor.nombre_comercial;
                    }
                    return "";
                }, name: 'proveedor'},
                {"data": function ( row, type, val, meta ) {
                    let kg = row.kg_producidos - row.kg_falla;
                    return kg;
                }, name: 'kg_producidos'}
            ]
        });
	}
}