var Reporte = {
	buscar : function() {
		$('#table-reporte').DataTable().destroy();
		return $('#table-reporte').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            ajax: 'merma_tintoreria?fechafiltroinicio='+$("#fechafiltroinicio").val()+'&fechafiltrofin='+$("#fechafiltrofin").val()+'&proveedorfiltro='+$("#proveedorfiltro").val()+'&productofiltro='+$("#productofiltro").val()+'&colorfiltro='+$("#colorfiltro").val(),
            columns: [
            	{data: 'fecha', name: 'fecha'},
                {data: 'planeamiento_id', name: 'planeamiento_id'},
                {data: 'lotes', name: 'lotes'},
                {data: 'producto', name: 'producto'},
                {data: 'pesoneto', name: 'pesoneto'},
                {data: 'cantidadneta', name: 'cantidadneta'},
            ]
        });
	}
}