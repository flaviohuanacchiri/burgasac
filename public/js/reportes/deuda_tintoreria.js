var Reporte = {
	buscar : function() {
		$('#table-reporte').DataTable().destroy();
		return $('#table-reporte').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            ajax: 'tela_deuda?fechafiltroinicio='+$("#fechafiltroinicio").val()+'&fechafiltrofin='+$("#fechafiltrofin").val()+'&proveedorfiltro='+$("#proveedorfiltro").val()+'&productofiltro='+$("#productofiltro").val()+'&colorfiltro='+$("#colorfiltro").val(),
            columns: [
            	{data: 'created_at', name: 'created_at'},
				{data: 'producto', name: 'producto'},
				{data: 'proveedor', name: 'proveedor'},
                {data: 'color', name: 'color'},
                {data: 'cantidad', name: 'color'},
            ]
        });
	}
}