$(document).ready(function(){
	$("#producto").change(function(e){
		let producto_id = $(this).val();
		CostoProveedorProducto.getIndicadoresPorProducto(producto_id);
	});
});

var CostoProveedorProducto = {
	getIndicadoresPorProducto : function(producto_id) {
		let indicador = listIndicadores[producto_id];
		if (typeof indicador!="undefined" && typeof indicador!=undefined) {
			if (producto_id!="") {
				let htmltitulo = "";
				let htmlinsumo = "";
				let insumo = indicador.insumo;
				let titulo = indicador.titulo;
				if (typeof insumo !="undefined" && typeof insumo !=undefined) {
					htmlinsumo+=insumo.nombre_generico;
				}
				if (typeof titulo !="undefined" && typeof titulo !=undefined) {
					htmltitulo+=titulo.nombre;
				}
				if (htmltitulo!="" && htmlinsumo!="") {
					let htmlselect = "<option value='"+titulo.id+"'>"+htmlinsumo+" "+htmltitulo+"</option>";
					$("#titulo").removeAttr("disabled");
					$("#tituloo").html("");
					$("#titulo").html(htmlselect);
					$("#titulo").selectpicker("refresh");

				} else {
					Mensaje.alerta("No hay titulo disponible para el producto!!!");
				}

			} else {
				$("#titulo").selectpicker();
				$("#titulo").val("");
				$("#titulo").selectpicker("refresh");
				$("#titulo").prop("disabled", true);
			}
		}
	},
	colorPorProveedor : function(proveedor_id) {
		var colores = $.ajax({
			type: "POST",
			url: "/coloresporproveedor", 
			data: {proveedor_id: proveedor_id}, 
			headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').val()
            },
            success : function(data){
            	ProveedorColorProducto.pintarComboColor(data);
            } 
        });
	},
}