<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tempVista extends Model
{
    protected $table = 'temp_prevista';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'codTempP';

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'codigo',
        'user',
        'fechaCreacion'
    ];

}
