<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CajaCierre extends Model
{
    protected $table = 'caja_cierre';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'cCajCieCod';
    public $incrementing = false;
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fCajFecCie',
        'cCajApeCod',
        'dCajMonCie'
    ];

    public function cajaapertura()
    {
        return $this->belongsTo('App\CajaApertura', 'cCajApeCod');
    }
}
