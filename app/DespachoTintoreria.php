<?php
namespace App;

use App\Planeamiento;
use App\DespachoTintoreriaPlaneamiento;

class DespachoTintoreria extends BaseModel
{
    protected $table = "despacho_tintoreria";
    protected $primaryKey = 'id';

    protected $fillable=[
        'id',
        'proveedor_id',
        'planeamiento_id',
        'fecha',
        'nroguia',
        'direccion_partida',
        'direccion_llegada',
        'created_at',
        'updated_at',
        'deleted_at',
        'user_created_at',
        'user_updated_at',
        'user_deleted_at',
        'userid_created_at',
        'userid_updated_at',
        'userid_deleted_at'
    ];

    public function detalledespachotintoreria()
    {
        return $this->hasMany('App\DetalleDespachoTintoreria', 'despacho_id', 'id');
    }

    public function proveedor()
    {
        return $this->belongsTo('App\Proveedor');
    }

    public function planeamiento()
    {
        return $this->belongsTo('App\Planeamiento');
    }
    public function transportista()
    {
        return $this->hasOne('App\TransportistaDespacho', 'despacho_id', 'id');
    }
    public function consumirPlaneamientos($nroLote, $totaldespachado = 0, $detalleDespachoId = 0)
    {
        $planeamientos = Planeamiento::select("planeamientos.*")->where("estado", "=", 1)->where("completadodespacho", "=", 0)
            ->leftJoin("detalle_planeamientos AS dp", "dp.planeamiento_id", "=", "planeamientos.id")
            ->where("dp.lote_insumo", "=", $nroLote)
            ->groupBy("dp.planeamiento_id")
            ->orderBy("planeamientos.fecha", "ASC")
            ->get();

        if (count($planeamientos) > 0 && $totaldespachado > 0) {
            foreach ($planeamientos as $key => $value) {
                $kgTotal = $value->kg_producidos - $value->kg_falla;
                if ($kgTotal > 0 && $value->completadodespacho == 0) {
                    if ($kgTotal > $value->kg_despachado) {
                        $diferencia = $kgTotal - $value->kg_despachado;
                        if ($totaldespachado >= $diferencia) {
                            $totalconsumido = $diferencia;
                            $totaldespachado-=$diferencia;
                            $value->kg_despachado = $kgTotal;
                            $value->completadodespacho = 1;
                        } else {
                            $totalconsumido = $totaldespachado;
                            $totaldespachado  = 0;
                            $value->kg_despachado+=$totalconsumido;
                            $value->completadodespacho = 0;
                        }
                        $update = [
                            "kg_despachado" => $value->kg_despachado,
                            "completadodespacho" => $value->completadodespacho
                        ];
                        Planeamiento::find($value->id)->update($update);
                        DespachoTintoreriaPlaneamiento::insert([
                            "despacho_tintoreria_id" => $this->id,
                            "detalles_despacho_tintoreria_id" => $detalleDespachoId,
                            "planeamiento_id" => $value->id,
                            "peso" => $totalconsumido
                        ]);
                    }
                }
            }
        }
    }
}
