<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DespachoTerceroPlaneamiento extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
    protected $table = 'despacho_tercero_planeamiento';

  /**
  * The database primary key value.
  *
  * @var string
  */
    protected $primaryKey = 'id';

  /**
   * Attributes that should be mass-assignable.
   *
   * @var array
   */
    protected $fillable = [
      'despacho_tercero_id',
      'detalles_despacho_tercero_id',
      'planeamiento_id',
      'peso'
    ];

    public function planeamiento()
    {
        return $this->hasOne("App\Planeamiento", "id", "planeamiento_id");
    }

    public function despachoTercero()
    {
        return $this->hasOne("App\DespachoTerceros", "id", "despacho_tercero_id");
    }
    public function despachoTerceroDetalle()
    {
        return $this->hasOne("App\DetallesDespachoTerceros", "id", "detalles_despacho_tercero_id");
    }
}
