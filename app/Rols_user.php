<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rols_user extends Model
{
    protected $table = 'role_user';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'role_id'
    ];

    /*public function rolsareas()
    {
        return $this->hasMany('App\Rols_areas', 'id');
    }*/

    public function rol()
    {
        return $this->belongsTo('App\Roles', 'role_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Users', 'user_id');
    }
}
