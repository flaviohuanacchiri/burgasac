<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Res_stock_mp extends Model
{
    protected $table = 'resumen_stock_materiaprima';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';    

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $fillable = [  
    	'lote',
		'proveedor_id',
		'titulo_id',
        'cantidad',                
        'estado',
        'created_at',
        'updated_at',
        'insumo_id',
        'accesorio_id',
        'peso_neto'
    ];

    public function insumo(){
      return $this->belongsTo('App\Insumo','insumo_id');
    }

    public function proveedor(){
      return $this->belongsTo('App\Proveedor','proveedor_id');
    }

    public function accesorio(){
      return $this->belongsTo('App\Accesorio','accesorio_id');
    }

    public function ventadetalle()
    {
        return $this->hasOne('App\ventadetalle','id');
    }

    public function prodevolver()
    {
        return $this->hasOne('App\Prodevolver','id');
    }

    public function Detvendev()
    {
        return $this->hasOne('App\DetalleVentaDev','id');
    }

    public function ingreso()
    {
        return $this->hasOne('App\Ingreso','id');
    }
}
