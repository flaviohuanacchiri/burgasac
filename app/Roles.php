<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = 'roles';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    //public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'all',
        'sort',
        'created_at',
        'updated_at'
    ];

    public function roluser()
    {
        return $this->hasMany('App\Rols_user', 'id');
    }

    /*public function almacen()
    {
        return $this->belongsTo('App\Almacen', 'nAlmCod');
    }*/
}
