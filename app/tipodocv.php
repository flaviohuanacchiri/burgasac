<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipodocv extends Model
{
    protected $table = 'tipodocpago';
    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nTipPagCod';
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

	protected $fillable = [
        'cDescTipPago'
    ];

    public function venta()
    {
        return $this->hasMany('App\venta','nTipPagCod');
    }
}
