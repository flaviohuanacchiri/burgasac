<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prodevolver extends Model
{
    protected $table = 'productodevolver';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'cProdDevCod';
    
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $fillable = [      	
        'nProAlmCod',
        'nDetVtaCod',
        'cDevCod',
        'nDvtaCB',
		'cTipNDev',
		'cEstPro',
        'nDevPeso',
        'nDevCant',
        'nCodTC',
        'nCodMP'
    ];

    public function devolucion()
    {
        return $this->belongsTo('App\Devoluciones', 'cDevCod');//de muchos a uno
    }

    public function vdetalle()
    {
        return $this->hasOne('App\ventadetalle','nDetVtaCod');
    }

    public function productoalmacen()
    {
        return $this->belongsTo('App\ProductoAlmacen', 'nProAlmCod');//de muchos a uno   
    }
    public function resStocktelas()
    {
        return $this->belongsTo('App\Res_Stock_Telas', 'nCodTC');//de muchos a uno
    }

    public function resStockMP()
    {
        return $this->belongsTo('App\Res_stock_mp', 'nCodMP');//de muchos a uno
    }

}
