<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferenciaCaja extends Model
{
    protected $table = 'transfercaja';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'cTraCaj';
    public $incrementing = false;
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cCajApeCod1',
        'cCajApeCod2',
        'tTraCajFec',
        'dTraCajMon',
        'nameUserLog',
        'nameRolLog'
    ];

    public function cajaapertura1()
    {
        return $this->belongsTo('App\CajaApertura','cCajApeCod1');
    }

    public function cajaapertura2()
    {
        return $this->belongsTo('App\CajaApertura','cCajApeCod2');
    }
}
