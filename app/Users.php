<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'users';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';    

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $fillable = [  
    	'name',
		'email',
		'password',
        'status',
        'confirmation_code',          
        'confirmed',
        'remember_token',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function Rolsuser()
    {
        return $this->hasMany('App\Rols_user', 'id');//de muchos a uno
    }

    public function rolsareas()
    {
        return $this->hasMany('App\Rols_areas', 'id');
    }
}
