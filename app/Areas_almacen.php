<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Areas_almacen extends Model
{
    protected $table = 'areas_almacen';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nAreAlmCod';

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nAlmCod',
        'nAreCod'
    ];

    public function areas()
    {
        return $this->belongsTo('App\Areas', 'nAreCod');
    }

    public function almacen()
    {
        return $this->belongsTo('App\Almacen', 'nAlmCod');
    }

    public function rolsareas()
    {
        return $this->hasMany('App\Rols_areas', 'nAreAlmCod');
    }

    public function scopeNamearea($query,$namearea)
    {
        if(trim($namearea) != "")
            $query->where('cProdCod',$namearea);
    }
}
