<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{
    protected $table = 'areas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nAreCod';

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cAreNom',
        'bForPag'
    ];

    public function areasalmacen()
    {
        return $this->hasMany('App\Areas_almacen', 'nAreCod');
    }

    public function rolareamod()
    {
        return $this->hasMany('App\RolAreaModulo', 'nAreCod');
    }
}
