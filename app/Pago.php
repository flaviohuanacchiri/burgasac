<?php

namespace App;

class Pago extends BaseModel
{
    protected $table = 'pago';
    protected $fillable=[
        'id',
        'compra_id',
        'despacho_tercero_id',
        'despacho_tintoreria_id',
        'detalle_despacho_id',
        'moneda_id',
        'tipo_comprobante',
        'total',
        'user_created_at',
        'user_updated_at',
        'user_deleted_at',
        'userid_created_at',
        'userid_updated_at',
        'userid_deleted_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    
    public function productoAlmacen()
    {
        return $this->hasMany('App\productoAlmacen', 'id');
    }
    public function notaA()
    {
        return $this->hasMany('App\NotaIngresoA', 'id');
    }
    public function compra()
    {
        return $this->hasOne('App\Compra', 'id', 'compra_id');
    }
    public function deudaTintoreria()
    {
        return $this->hasOne('App\ProveedorDespachoTintoreriaDeuda', 'id', 'proveedor_despacho_tintoreria_deuda_id');
    }
    public function deudaServicio()
    {
        return $this->hasOne('App\ProveedorDespachoTerceroDeuda', 'id', 'proveedor_despacho_tercero_deuda_id');
    }
}
