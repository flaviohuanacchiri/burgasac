<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProveedorDespachoTerceroDeuda;

class DetallesDespachoTerceros extends Model
{
    protected $table = "detalles_despacho_terceros";
    
    protected $fillable = [
        "color_id",
        "producto_id",
        "cantidad",
        "proveedor_id",
        "descripcion",
        'despacho_id',
        'rollos',
        'nrolote'
    ];

    public function producto()
    {
        return $this->belongsTo('App\Producto');
    }
    public function proveedor()
    {
        return $this->belongsTo('App\Proveedor');
    }

    public function color()
    {
        return $this->belongsTo('App\Color');
    }
    public function detallePlanemiento()
    {
        return $this->hasMany("App\DespachoTerceroPlaneamiento", "detalles_despacho_tercero_id", "id");
    }
    public function getPrecio(
        $proveedorId = 0,
        $despachoId = 0,
        $productoId = 0,
        $id = 0
    ) {
        return ProveedorDespachoTerceroDeuda::where("proveedor_id", "=", $proveedorId)
            ->where("despacho_id", "=", $despachoId)
            ->where("producto_id", "=", $productoId)
            ->where("detalle_despacho_id", "=", $id)
            ->first();
    }
}
