<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trans_det_elim extends Model
{
	protected $table = 'transferencias_detalle_eliminados';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nTraDetCod';
    // public $incrementing = false;
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'nProAlmCod',
    	'cod_barras',
    	'dTraCant',
    	'bTraDev',
    	'cTraCod'
    ];
	/*
    public function transferencia()
    {
        return $this->belongsTo('App\Transferencias', 'cTraCod');
    }    

    public function productoalmacens()
    {
        return $this->belongsTo('App\ProductoAlmacen', 'nProAlmCod');
    } */   
}
