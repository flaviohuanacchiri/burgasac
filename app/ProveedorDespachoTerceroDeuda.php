<?php

namespace App;

class ProveedorDespachoTerceroDeuda extends BaseModel
{
    protected $table = 'proveedor_despacho_tercero_deuda';
    protected $fillable=[
        'id',
        'proveedor_id',
        'producto_id',
        'despacho_id',
        'detalle_despacho_id',
        'moneda_id',
        'preciounitario',
        'total',
        'totalpagado',
        'user_created_at',
        'user_updated_at',
        'user_deleted_at',
        'userid_created_at',
        'userid_updated_at',
        'userid_deleted_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function proveedor()
    {
        return $this->belongsTo('App\Proveedor');
    }

    public function producto()
    {
        return $this->belongsTo('App\Producto');
    }
    public function despacho()
    {
        return $this->belongsTo('App\DespachoTintoreria');
    }
    public function getDeuda()
    {
        return $this->total - $this->totalpagado;
    }
}
