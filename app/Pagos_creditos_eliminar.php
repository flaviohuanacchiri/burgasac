<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pagos_creditos_eliminar extends Model
{ 
    protected $table = 'pagos_creditos_eliminados';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nPagCreLog';
    //public $incrementing = false;
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cPagCre',
        'nVtaCod',
        'dPagCreMonto',
        'tPagCreFecReg',
        'nCodUsu',
        'nNomUsu',
        'tFecElim'
    ];
  
    /*public function ventas()
    {
        return $this->belongsTo('App\venta', 'nVtaCod');
    }*/
}
