<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransportistaDespacho extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transportista_despacho';
}
