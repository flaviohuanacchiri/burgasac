<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Indicador;

class Planeamiento extends Model
{
    use SoftDeletes;
  /**
   * The database table used by the model.
   *
   * @var string
   */
    protected $table = 'planeamientos';

  /**
  * The database primary key value.
  *
  * @var string
  */
    protected $primaryKey = 'id';

  /**
   * Attributes that should be mass-assignable.
   *
   * @var array
   */
    protected $fillable = [
      'fecha',
      'proveedor_id',
      'empleado_id',
      'maquina_id',
      'producto_id',
      'estado',
      'turno',
      'estado',
      'rollos',
      'kg_falla',
      'kg_producidos',
      'kg_despachado',
      'mp_producida',
      'rollos_falla',
      'completadodespacho'
    ];

  /**
   * The attributes that should be mutated to dates.
   *
   * @var array
   */
    protected $dates = ['deleted_at'];

    public function proveedor()
    {
        return $this->belongsTo('App\Proveedor');
    }
    public function tintoreria()
    {
        return $this->hasMany('App\DespachoTintoreria');
    }
    public function despachoTintoreria()
    {
        return $this->hasMany('App\DespachoTintoreriaPlaneamiento', 'planeamiento_id', 'id');
    }

    public function despachoTercero()
    {
        return $this->hasMany('App\DespachoTerceroPlaneamiento', 'planeamiento_id', 'id');
    }

    public function detalles()
    {
        return $this->hasMany('App\DetallePlaneamiento');
    }
    public function getIndicador()
    {
        $indicador = DetallePlaneamiento::from("detalle_planeamientos as dt")
          ->select("dt.insumo_id", "dt.titulo_id", "dt.planeamiento_id", "dt.lote_insumo as lote", "i.valor")
          ->leftJoin("indicador as i", "i.insumo_id", "=", "dt.insumo_id")
          ->where("dt.planeamiento_id", $this->id)
          ->orderBy("i.valor", "DESC")
          ->first();
        return $indicador;
    }

    public function getIndicadores()
    {
        $indicador = DetallePlaneamiento::from("detalle_planeamientos as dt")
          ->select("dt.insumo_id", "dt.titulo_id", "dt.planeamiento_id", "dt.lote_insumo as lote", "i.valor")
          ->leftJoin("indicador as i", "i.insumo_id", "=", "dt.insumo_id")
          ->where("dt.planeamiento_id", $this->id)
          ->orderBy("i.valor", "DESC")
          ->get();
        return $indicador;
    }
    public function producto()
    {
        return $this->belongsTo('App\Producto');
    }

    public function empleado()
    {
        return $this->belongsTo('App\Empleado');
    }

    public function maquina()
    {
        return $this->belongsTo('App\Maquina');
    }

    public function getTotalProducido()
    {
        return $this->kg_producidos - $this->kg_falla;
    }
}
