<?php

namespace App;

class Devoluciones extends BaseModel
{
     protected $table = 'devolucion';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'cDevCod';
    public $incrementing = false;
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nVtaCod',
        /*'cTipNDev',
        'cEstPro',
        'dTotDetVen',*/
        'dTotDev',
        'dTotRep',
        'dMonFavor',
        'dMonContra',
        'created_at',
        'updated_at',
        'deleted_at',
        'userid_created_at',
        'userid_updated_at',
        'userid_deleted_at',
        'user_created_at',
        'user_updated_at',
        'user_deleted_at'
    ];

    public function detalleventadev()
    {
        return $this->hasMany('App\DetalleVentaDev', 'cDevCod');
    }
    public function productodevolver()
    {
        return $this->hasMany('App\Prodevolver', 'cDevCod');
    }

    public function ventas()
    {
        return $this->hasOne('App\venta', 'nVtaCod');
    }
}
