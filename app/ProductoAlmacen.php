<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoAlmacen extends Model
{
    protected $table = 'productoalmacens';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nProAlmCod';
    //public $incrementing = false;

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $fillable = [                
    	'cProdCod',
        'id_color',
    	'nAlmCod',
		'nProdAlmMin',
		'nProdAlmMax',
		'nProdAlmStock',
        'nProdAlmPuni',
        'nProdAlmStockFalla',
        'nProdAlmStockIni'
    ];

    public function producto()
    {
        return $this->belongsTo('App\Producto', 'cProdCod');//de muchos a uno
    }

    public function almacen()
    {
        return $this->belongsTo('App\Almacen', 'nAlmCod');//de muchos a uno
    }    

    public function color()
    {
        return $this->belongsTo('App\Color', 'id_color');//de muchos a uno
    }

    public function detComTel()
    {
        return $this->hasMany('App\DetCompraTelat', 'nProAlmCod');//de muchos a uno
    }

    public function venta()
    {
        return $this->hasMany('App\venta', 'nProAlmCod');//de muchos a uno
    }

    public function transferenciadetalle()
    {
        return $this->hasMany('App\Transferencias_detalle', 'nProAlmCod');
    }

    public function productodevolver()
    {
        return $this->hasMany('App\Prodevolver', 'nProAlmCod');//de muchos a uno
    }

    public function detalleventadev()
    {
        return $this->hasMany('App\DetalleVentaDev', 'nProAlmCod');//de muchos a uno
    }

    public function scopeNamecodigo($query,$namecodigo,$color)
    {
        if(trim($namecodigo) != "")
            $query->where('cProdCod',$namecodigo)->where("id_color",$color);
    }

    public function ingreso()
    {
        return $this->hasMany('App\Ingreso', 'nProAlmCod');
    }

}
