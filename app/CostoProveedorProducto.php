<?php

namespace App;

class CostoProveedorProducto extends BaseModel
{
    //

    protected $table = "costo_proveedor_producto";
    protected $primaryKey = 'id';

    protected $fillable=[
        'id',
        'producto_id',
        'proveedor_id',
        'titulo_id',
        'moneda_id',
        'precio',
        'user_created_at',
        'user_updated_at',
        'user_deleted_at',
        'userid_created_at',
        'userid_updated_at',
        'userid_deleted_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function producto()
    {
        return $this->belongsTo('App\Producto');
    }
    public function titulo()
    {
        return $this->belongsTo('App\Titulo');
    }
    public function proveedor()
    {
        return $this->belongsTo('App\Proveedor');
    }
}
