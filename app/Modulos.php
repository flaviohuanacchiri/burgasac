<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulos extends Model
{
    protected $table = 'modulos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nCodMod';

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cNomMod',        
        'cDesMod',
        'url'
    ];

    public function rolareamod()
    {
        return $this->hasMany('App\RolAreaModulo', 'nCodMod');
    }
}
