<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferCajaEliminados extends Model
{
    protected $table = 'transfercaja_eliminados';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nTraCajLog';
    //public $incrementing = false;
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cTraCaj',
        'cCajApeCod1',
        'cCajApeCod2',
        'tTraCajFec',
        'dTraCajMon',
        'nameUserLog',
        'nameRolLog',
        'tFechElim'
    ];

    /*public function cajaapertura1()
    {
        return $this->belongsTo('App\CajaApertura','cCajApeCod1');
    }

    public function cajaapertura2()
    {
        return $this->belongsTo('App\CajaApertura','cCajApeCod2');
    }*/
}
