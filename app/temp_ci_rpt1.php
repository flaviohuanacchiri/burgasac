<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class temp_ci_rpt1 extends Model
{
    protected $table = 'temp_ci_rpt1';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'codtempci';

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contenidohtml',
        'cInvCod'
    ];
}
