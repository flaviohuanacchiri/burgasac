<?php

namespace App\Http\Controllers\Backend\Access\User;

use App\Models\Access\User\User;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Access\User\UserRepository;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Http\Requests\Backend\Access\User\StoreUserRequest;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;
use App\Http\Requests\Backend\Access\User\UpdateUserRequest;
use Illuminate\Http\Request;
use App\Almacen;
use App\Areas_almacen;
use App\Rols_areas;
use App\Users;
use DB;
/**
 * Class UserController
 */
class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * @var RoleRepository
     */
    protected $roles;

    /**
     * @param UserRepository $users
     * @param RoleRepository $roles
     */
    public function __construct(UserRepository $users, RoleRepository $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
    }

	/**
     * @param ManageUserRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageUserRequest $request)
    {
        return view('backend.access.index');
    }

	/**
     * @param ManageUserRequest $request
     * @return mixed
     */
    public function create(ManageUserRequest $request)
    {
        $alm=Almacen::all();
        return view('backend.access.create',compact("alm"))->withRoles($this->roles->getAll());
    }

    public function almarea(Request $request,$id)
    {       
        if($request->ajax()){
            $datos_x=DB::table("areas_almacen")
            ->Join("areas","areas_almacen.nAreCod","=","areas.nAreCod")                
            ->where('areas_almacen.nAlmCod','=',$id)
            ->select("areas_almacen.nAreAlmCod","areas.cAreNom","areas.nAreCod")
            ->distinct()
            ->get();
            
            return response()->json($datos_x);
        }
    }

    public function detallearea(Request $request,$id)
    {
        if($request->ajax()){
            $prod_x=DB::table("rols_areas_modulos")
            ->Join("modulos","rols_areas_modulos.nCodMod","=","modulos.nCodMod") 
            ->where('rols_areas_modulos.nAreCod','=',$id)            
            ->select("modulos.cNomMod","modulos.cDesMod","modulos.url")
            ->distinct()
            ->get();

            return response()->json($prod_x);           
        }
    }

	/**
     * @param StoreUserRequest $request
     * @return mixed
     */
    public function store(StoreUserRequest $request)
    {        
        $this->validate($request, [
            'areas' => 'required'
        ]);
        
        DB::beginTransaction();

        try 
        {
            $this->users->create(['data' => $request->except('assignees_roles'), 'roles' => $request->only('assignees_roles')]);

            $cod=Areas_almacen::where("nAlmCod",$request->alm)->where("nAreCod",$request->areas)->first();
            $coduser=Users::orderby("id","desc")->first();

            $rolarea=new Rols_areas;
            $rolarea->nAreAlmCod=$cod->nAreAlmCod; 
            //$rolarea->bRolAreAct=$request->area2;  
            $rolarea->id=$coduser->id;
            
            $rolarea->save();        
        } catch (Exception $e) {

            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();
        return redirect()->route('admin.access.user.index')->withFlashSuccess(trans('alerts.backend.users.created'));
    }

	/**
	 * @param User $user
	 * @param ManageUserRequest $request
	 * @return mixed
	 */
	public function show(User $user, ManageUserRequest $request) {
		return view('backend.access.show')
			->withUser($user);
	}

	/**
     * @param User $user
     * @param ManageUserRequest $request
     * @return mixed
     */
    public function edit(User $user, ManageUserRequest $request)
    {
        $ra=Rols_areas::where("id",$user->id)->where("bRolAreAct","1")->first();
        
        $nRolAreCod=$ra->nRolAreCod;

        $codalm=$ra->areasalmacen->nAlmCod;   

        $areasX=Areas_almacen::where("nAlmCod",$codalm)->get();   

        $codare=$ra->areasalmacen->nAreCod;

        $darea_x=DB::table("rols_areas_modulos")
            ->Join("modulos","rols_areas_modulos.nCodMod","=","modulos.nCodMod") 
            ->where('rols_areas_modulos.nAreCod','=',$codare)            
            ->select("modulos.cNomMod","modulos.cDesMod")
            ->distinct()
            ->get();

        $alm=Almacen::all();
        return view('backend.access.edit',compact("alm","codalm","codare","areasX","nRolAreCod","darea_x"))
            ->withUser($user)
            ->withUserRoles($user->roles->pluck('id')->all())
            ->withRoles($this->roles->getAll());
    }

	/**
     * @param User $user
     * @param UpdateUserRequest $request
     * @return mixed
     */
    public function update(User $user, UpdateUserRequest $request)
    {
        $this->validate($request, [
            'areas' => 'required'
        ]);
        
        DB::beginTransaction();

        try 
        {
            $this->users->update($user, ['data' => $request->except('assignees_roles'), 'roles' => $request->only('assignees_roles')]);

            $cod=Areas_almacen::where("nAlmCod",$request->alm)->where("nAreCod",$request->areas)->first();
            //$coduser=Users::orderby("id","desc")->first();

            Rols_areas::where('nRolAreCod',"=",$request->codarerol)
            ->update([
                'nAreAlmCod' => $cod->nAreAlmCod,
                'id' => $user->id           
                ]); 
       
        } catch (Exception $e) {

            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return redirect()->route('admin.access.user.index')->withFlashSuccess(trans('alerts.backend.users.updated'));
    }

	/**
     * @param User $user
     * @param ManageUserRequest $request
     * @return mixed
     */
    public function destroy(User $user, ManageUserRequest $request)
    {
        $this->users->delete($user);
        
        $areaalm=Rols_areas::where('id',"=",$user->id);
        $areaalm->delete();

        return redirect()->route('admin.access.user.deleted')->withFlashSuccess(trans('alerts.backend.users.deleted'));
    }
}