<?php

namespace App\Http\Controllers\Master;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

use App\Color;
use App\Proveedor;
use App\Producto;
use App\ProveedorColor;
use App\ProveedorColorProducto;
use App\CostoProveedorProducto;
use App\Indicador;
use DB;

class CostoProveedorProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data = CostoProveedorProducto::with("producto", "titulo", "proveedor")
        ->paginate(10);
        return view('master.costo_proveedor_producto.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tipo_id = \Config::get("sistema.tipo_proveedor_planeamiento_id");
        $obj = new ProveedorColorProducto;
        $dataproveedores = Proveedor::select("proveedores.*")
          ->leftJoin("proveedor_tipo as pt", "pt.proveedor_id", "=", "proveedores.id")
          ->where("pt.tipo_proveedor_id", "=", $tipo_id)
          ->get();
        $proveedores = [];
        $proveedores[""] = "Selecciona";
        foreach ($dataproveedores as $key => $value) {
            $proveedores[$value->id] = $value->nombre_comercial;
        }
        $dataproductos = Producto::all();
        $productos = [];
        $productos[""] = "Selecciona";
        foreach ($dataproductos as $key => $value) {
            $productos[$value->id] = $value->nombre_generico;
        }
        $dataindicadores = Indicador::with("titulo", "insumo")->get();
        $indicadores = [];
        foreach ($dataindicadores as $key => $value) {
            $indicadores[$value->producto_id] = $value;
        }
        return view('master.costo_proveedor_producto.create', compact('obj', 'proveedores', 'productos', 'indicadores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        $this->validate($request, [
            'producto_id'       => 'required',
            'proveedor_id'       => 'required',
            'titulo_id'         => 'required',
            'moneda_id'         => 'required'
        ]);
        CostoProveedorProducto::create($requestData);

        Session::flash('flash_message', 'Registro added!');

        return redirect('costo_proveedor_producto');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $color = CostoProveedorProducto::findOrFail($id);

        return view('master.color.show', compact('color'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $tipo_id = \Config::get("sistema.tipo_proveedor_planeamiento_id");
        $obj = CostoProveedorProducto::find($id);
        $dataproveedores = Proveedor::select("proveedores.*")
          ->leftJoin("proveedor_tipo as pt", "pt.proveedor_id", "=", "proveedores.id")
          ->where("pt.tipo_proveedor_id", "=", $tipo_id)
          ->get();
        $proveedores = [];
        $proveedores[""] = "Selecciona";
        foreach ($dataproveedores as $key => $value) {
            $proveedores[$value->id] = $value->nombre_comercial;
        }
        $dataproductos = Producto::all();
        $productos = [];
        $productos[""] = "Selecciona";
        foreach ($dataproductos as $key => $value) {
            $productos[$value->id] = $value->nombre_generico;
        }
        $dataindicadores = Indicador::with("titulo", "insumo")->get();
        $indicadores = [];
        foreach ($dataindicadores as $key => $value) {
            $indicadores[$value->producto_id] = $value;
        }

        return view('master.costo_proveedor_producto.edit', compact('obj', 'proveedores', 'productos', 'indicadores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $requestData = $request->all();
        
        $obj = CostoProveedorProducto::findOrFail($id);
        $obj->update($requestData);

        Session::flash('flash_message', 'Registro actualizado!');

        return redirect('costo_proveedor_producto');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $obj = CostoProveedorProducto::findOrFail($id);
        if (!is_null($obj)) {
            $obj->userid_deleted_at = Auth::user()->id;
            $obj->user_deleted_at = Auth::user()->name;
            $obj->save();
            $obj->delete();
        }

        Session::flash('flash_message', 'Costo eliminado!');

        return redirect('costo_proveedor_producto');
    }
}
