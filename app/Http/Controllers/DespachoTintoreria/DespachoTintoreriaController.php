<?php

namespace App\Http\Controllers\DespachoTintoreria;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Producto;
use App\Proveedor;
use App\Color;
use App\DespachoTintoreria;
use App\Planeamiento;
use App\DetalleDespachoTintoreria;
use App\Movimiento_Tela;
use App\Resumen_Stock_Tela;
use App\ResumenDespachoTintoreria;
use App\ProveedorColorProducto;
use App\ProveedorDespachoTintoreriaDeuda;
use App\TransportistaDespacho;
use App\DespachoTintoreriaPlaneamiento;
use App\ProveedorTipo;
use App\NotaIngreso;
use Carbon\Carbon;
use Session;
use Validator;
use DB;

use PDF;
use App;

class DespachoTintoreriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $despachoTintorerias = DespachoTintoreria::with("proveedor", "planeamiento")->orderBy("id", "DESC")->paginate(10);

        foreach ($despachoTintorerias as $key => $value) {
            $despachoTintorerias[$key]->detalles = DetalleDespachoTintoreria::select("detalles_despacho_tintoreria.*",
              "c.nombre as color", "p.nombre_generico as producto")
            ->leftJoin("productos as p", "p.id", "=", "detalles_despacho_tintoreria.producto_id")
            ->leftJoin("color as c", "c.id", "=", "detalles_despacho_tintoreria.color_id")
            ->where('despacho_id', $value->id)
            ->get();
            $notasingreso = [];
            foreach ($despachoTintorerias[$key]->detalles as $key2 => $value2) {
              $notasingreso[] = NotaIngreso::where("despTint_id", "=", $value2->id)->get();
            }
            $despachoTintorerias[$key]->notasingreso = $notasingreso;
        }

        return view('tintoreria.index',compact('despachoTintorerias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipoburga = \Config::get("sistema.tipo_proveedor_burgasac_id");
        $proveedorburga = Proveedor::select("proveedores.*")
        ->leftJoin("proveedor_tipo as pt", "pt.proveedor_id", "=", "proveedores.id")
        ->where("pt.tipo_proveedor_id", "=", $tipoburga)
        ->first();

        $tipo_id = \Config::get("sistema.tipo_proveedor_tintoreria_id");
        $productos = array();
        $proveedores = Proveedor::select("proveedores.id as proveedor_id", "nombre_comercial", "direccion")
        ->leftJoin("proveedor_tipo as pt", "pt.proveedor_id", "=", "proveedores.id")
        ->where("pt.tipo_proveedor_id", "=", $tipo_id)
        ->get();
        $colores = Color::get();
        $planeamientos = DB::select(DB::raw("SELECT DISTINCT producto_id,proveedor_id FROM planeamientos"));

        foreach ($planeamientos as $key => $planeamiento) {
          $productos[$planeamiento->producto_id] = Producto::find($planeamiento->producto_id);
        }
        $tipo_id = \Config::get("sistema.tipo_proveedor_burgasac_id");
        $objproveedortipo = ProveedorTipo::where(["tipo_proveedor_id" => $tipo_id])->first();
        $direccion = "";
        if (!is_null($objproveedortipo)) {
            $objburgasac = Proveedor::find($objproveedortipo->proveedor_id);
            if (is_null($objburgasac)) {
              
            } else {
              $direccion = $objburgasac->direccion;
            }
        } else {
            $direccion = "";
        }

        return view('tintoreria.create',compact('productos','proveedores', 'colores', 'direccion', 'proveedorburga'));
    }

    public function obtenerProveedores($producto_id){
      $tipo_id = \Config::get("sistema.tipo_proveedor_tintoreria_id");
      /*$planeamientos = Planeamiento::select("planeamientos.proveedor_id", "pr.nombre_comercial")
        ->leftJoin("proveedor_tipo as pt", "pt.proveedor_id", "=", "planeamientos.proveedor_id")
        ->leftJoin("proveedores as pr", "pr.id", "=", "planeamientos.proveedor_id")
        ->where("planeamientos.producto_id",$producto_id)
        //->where("pt.tipo_proveedor_id", "=", $tipo_id)
        ->get();
      /*$otrosproveedores = Proveedor::select("id", "nombre_comercial")
        ->leftJoin("proveedor_tipo as pt", "pt.proveedor_id", "=", "proveedores.id")
        ->where("pt.tipo_proveedor_id", "=", $tipo_id)
        ->get();*/
      $proveedores = Proveedor::select("proveedores.id as proveedor_id", "nombre_comercial")
        ->leftJoin("proveedor_tipo as pt", "pt.proveedor_id", "=", "proveedores.id")
        ->where("pt.tipo_proveedor_id", "=", $tipo_id)
        ->get();
        
      $resultado = [];
      $data = [];
      foreach ($proveedores as $key => $value) {
        $resultado[$value->proveedor_id] = $value;
      }
      /*foreach ($otrosproveedores as $key => $value) {
        $resultado[$value->proveedor_id] = $value;
      }*/
      foreach ($resultado as $key => $value) {
        $data[] = $value;
      }
      return $data;
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $requestData = $request->all();
      /*
       * validate($request, $rules, $messages)
       */
      $this->validate($request, [
          'fecha'             => 'date',
          'detalles'          => 'required',
      ],
      [
          'detalles.required' => 'Ingrese al menos un detalle para el Despacho.'
      ]);

      $detallesdespacho = $requestData['detalles'];
      $transportista = $requestData['transportista'];
      $detalles = [];
      foreach ($detallesdespacho as $key => $value) {
         if (!isset($detalles[$value["color"]])) {
            $detalles[$value["color"]] = $value;
            $detalles[$value["color"]]["kg"] = (double)$value["kg"];
         } else {
            $detalles[$value["color"]]["kg"]+=(double)$value["kg"];
            $detalles[$value["color"]]["rollos"]+=(int)$value["rollos"];
         }
      }
      $create  = [
        "proveedor_id" => $requestData["proveedor"],
        "direccion_partida" => $requestData["direccion_partida"],
        "direccion_llegada" => $requestData["direccion_llegada"],
        "nroguia" => $requestData["nroguia"]
      ];
      $despacho_id = DespachoTintoreria::create($create)->id;
      $objdespacho = DespachoTintoreria::find($despacho_id);
      $objplaneamiento = Planeamiento::find($requestData["planeamiento_id"]);
      $totalProducidoPlaneamiento = 0;
      $kgDespachado = 0;
      /*if (!is_null($objplaneamiento)) {
        $totalProducidoPlaneamiento = $objplaneamiento->getTotalProducido();
        $kgDespachado = $objplaneamiento->kg_despachado;
        $kgDespachado+=$requestData["kg"];
        if ($kgDespachado >= $totalProducidoPlaneamiento) {
          $objplaneamiento->completadodespacho = 1;
          
        }
        $objplaneamiento->kg_despachado = $kgDespachado;
        $objplaneamiento->save();
      }*/
      $tipo_id = \Config::get("sistema.tipo_proveedor_burgasac_id");
      $proveedorBurga = Proveedor::select("proveedores.*")
          ->leftJoin("proveedor_tipo as pt", "pt.proveedor_id", "=", "proveedores.id")
          ->where("pt.tipo_proveedor_id", "=", $tipo_id)
          ->first();
      
      if (isset($despacho_id)) {
          foreach ($detalles as $detalle) {
            $detalle["proveedor_id"] = $detalle["proveedor"];
            $detalle["despacho_id"] = $despacho_id;
            $detalle["producto_id"] = $detalle["producto"];
            $detalle["cantidad"] = $detalle["kg"];
            $detalle["rollos"] = $detalle["rollos"];
            $detalle["nro_lote"] = $detalle["nro_lote"];
            $detalle["estado"] = 0;

            $movimiento = array();
            $movimiento["planeacion_id"] = 0;
            $movimiento["proveedor_id"] = $detalle["proveedor_id"];
            $movimiento["producto_id"] = $detalle["producto_id"];
            $movimiento["rollos"] = - $detalle["rollos"];
            $movimiento["estado"] = 0;
            $movimiento["cantidad"] = - $detalle["kg"];
            $movimiento["descripcion"] = "Despacho a tintorerias";
            $movimiento["nro_lote"] = $detalle["nro_lote"];

            $totalPesoDespachado = $detalle["kg"];

            $movimiento_mp = Movimiento_Tela::create($movimiento);

            Resumen_Stock_Tela::calculateCurrentStock($detalle["producto_id"], $proveedorBurga->id,-($detalle["kg"]),-($detalle["rollos"]), $detalle["nro_lote"]);
            if (isset($detalle["color"])) {
              $detalle["color_id"] = $detalle["color"];
            }

            $iddetalledespacho = DetalleDespachoTintoreria::create($detalle);

            if (isset($detalle["color"])) {
              $detalle["color_id"] = $detalle["color"];

            } else {
              $detalle["color_id"] = 0;

            }
            $objdespacho->consumirPlaneamientos($detalle["nro_lote"], $totalPesoDespachado, $iddetalledespacho->id);
            unset($detalle["color"]);

            $resumen = ResumenDespachoTintoreria::where(["color_id" => $detalle["color_id"], "producto_id" => $detalle["producto"], "fecha" => $detalle["fecha"]])->whereRaw("deleted_at IS NULL")->first();

            //dd($resumen);
            if (!is_null($resumen)) {
              $objresumen = ResumenDespachoTintoreria::find($resumen->id);
            } else {
              $objresumen = new ResumenDespachoTintoreria;
              $objresumen->producto_id = $detalle["producto"];
              $objresumen->color_id = $detalle["color_id"];
              $objresumen->peso = 0;
              $objresumen->rollos = 0;
              $objresumen->fecha = $detalle["fecha"];
              $objresumen->save();
            }
            $objresumen->actualizar($detalle["cantidad"], $detalle["rollos"]);

            $proveedorcolorproducto = ProveedorColorProducto::where(["proveedor_id" => $detalle["proveedor_id"],
              "producto_id" => $detalle["producto_id"], "color_id" => $detalle["color_id"]])->whereRaw("deleted_at IS NULL")->orderBy("id", "DESC")->first();
            if (!is_null($proveedorcolorproducto)) {
              $objdeuda = new ProveedorDespachoTintoreriaDeuda;
              $objdeuda->proveedor_id = $detalle["proveedor_id"];
              $objdeuda->despacho_id = $detalle["despacho_id"];
              $objdeuda->color_id = $detalle["color_id"];
              $objdeuda->producto_id = $detalle["producto_id"];
              $objdeuda->preciounitario = $proveedorcolorproducto->precio;
              $objdeuda->total = $proveedorcolorproducto->precio*$detalle["kg"];
              $objdeuda->moneda_id = $proveedorcolorproducto->moneda_id;
              $objdeuda->detalle_despacho_id = $iddetalledespacho->id;
              $objdeuda->save();
            }
            
          }
      }

      if (!is_null($transportista)) {
        $objtransportista = new TransportistaDespacho;
        $objtransportista->despacho_id = $despacho_id;
        $objtransportista->nombre = $transportista["nombre"];
        $objtransportista->direccion = $transportista["direccion"];
        $objtransportista->ruc = $transportista["ruc"];
        $objtransportista->marca = $transportista["marca"];
        $objtransportista->placa = $transportista["placa"];
        $objtransportista->licencia = $transportista["licencia"];
        $objtransportista->save();
      }

      Session::flash('flash_message', 'Datos guardados!');
      return redirect('despacho-tintoreria/despacho-tintoreria');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $despacho = DespachoTintoreria::find($id);
        $detalles = DetalleDespachoTintoreria::where("despacho_id",$id)->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        //$despacho = DespachoTintoreria::find($id);
        $detalles = DetalleDespachoTintoreria::where("despacho_id",$id)->get();
        $pesoDespachado = 0;
        $tipo_id = \Config::get("sistema.tipo_proveedor_burgasac_id");
      $proveedorBurga = Proveedor::select("proveedores.*")
          ->leftJoin("proveedor_tipo as pt", "pt.proveedor_id", "=", "proveedores.id")
          ->where("pt.tipo_proveedor_id", "=", $tipo_id)
          ->first();
        foreach ($detalles as $key => $detalle) {
          $movimiento = array();
          $movimiento["planeacion_id"] = 0;
          $movimiento["proveedor_id"] = $detalle["proveedor_id"];
          $movimiento["producto_id"] = $detalle["producto_id"];
          $movimiento["rollos"] = $detalle["rollos"];
          $movimiento["estado"] = 0;
          $movimiento["cantidad"] = $detalle["cantidad"];
          $movimiento["descripcion"] = "Eliminacion de espacho a tintorerias";
          $movimiento["nro_lote"] = $detalle["nro_lote"];
          $movimiento_mp = Movimiento_Tela::create($movimiento);

          $pesoDespachado+=$detalle["cantidad"];
          $despachoplaneamiento = DespachoTintoreriaPlaneamiento::where("despacho_tintoreria_id", "=", $id)
          ->where("detalles_despacho_tintoreria_id", $detalle->id)->first();
          if (!is_null($despachoplaneamiento)) {
            $despachoplaneamiento->delete();
          }
          Resumen_Stock_Tela::calculateCurrentStock($detalle["producto_id"], $proveedorBurga->id,($detalle["cantidad"]),$detalle["rollos"], $detalle["nro_lote"]);
          $fecha = date("Y-m-d", strtotime($detalle["created_at"]));
         // dd($detalle);
         $resumen = ResumenDespachoTintoreria::where(["color_id" => $detalle["color_id"], "producto_id" => $detalle["producto_id"], "fecha" => $fecha])->whereRaw("deleted_at IS NULL")->first();
         //dd($resumen);

            if (!is_null($resumen)) {
              $objresumen = ResumenDespachoTintoreria::find($resumen->id);
              $objresumen->actualizar(-$detalle["cantidad"], -$detalle["rollos"]);
            }
            ProveedorDespachoTintoreriaDeuda::where(["proveedor_id" => $detalle["proveedor_id"], "producto_id" => $detalle["producto_id"], "despacho_id" => $id])->delete();

        }
        $despacho = DespachoTintoreria::find($id);
        /*$objplaneamiento = Planeamiento::find($despacho->planeamiento_id);
        $objplaneamiento->kg_despachado-=$pesoDespachado;
        $objplaneamiento->completadodespacho = 0;
        $objplaneamiento->save();*/
        DespachoTintoreria::find($id)->delete();


        Session::flash('flash_message', 'Datos guardados!');
        return redirect('despacho-tintoreria/despacho-tintoreria');


    }

    public function getBoleta(Request $request)
    {
        $despacho = DespachoTintoreria::with("detalledespachotintoreria", "detalledespachotintoreria.producto", "detalledespachotintoreria.color", "proveedor", "transportista")->find($request->id);

        $view =  \View::make('tintoreria.print.guiasimple', compact("despacho"))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper([5, -33, 375.465, 715.276], 'landscape');//->setPaper('a4', 'potrait');
        return $pdf->stream('invoice');
    }

    public function getLotesConStock(Request $request)
    {
        $producto_id = $request->producto_id;
        $proveedor_id = isset($request->proveedor_id)? $request->proveedor_id : 3;
        $primero = isset($request->primero)? $primero : null;
        $where = "";
        if (isset($request->producto_id) && $request->producto_id!="") {
            $where.=" AND rst.producto_id = {$producto_id}";
        }
        $where.=" AND rst.proveedor_id = {$proveedor_id} ";
        $query = "SELECT a.* FROM (
          SELECT rst.*, ind.valor AS indicador FROM resumen_stock_telas AS rst
          LEFT JOIN resumen_stock_materiaprima AS rsmp ON rsmp.lote = rst.nro_lote
          LEFT JOIN insumos AS i ON i.id = rsmp.insumo_id
          LEFT JOIN indicador AS ind ON (ind.insumo_id = i.id AND ind.producto_id = rst.producto_id AND i.titulo_id = ind.titulo_id)
          WHERE rst.deleted_at IS NULL
          {$where}
          ORDER BY ind.valor DESC) AS a
          GROUP BY a.nro_lote";
        $lotes = DB::select($query);

        if (count($lotes) > 0) {
          if (is_null($primero)) {
            return response(["rst" => 1,"lotes" => $lotes]);
          }
            return response(["rst" => 1,"lotes" => $lotes[0]]);
        } else {
          return response(["rst" => 2,"msj" => "No existen lotes disponibles!!!", "lotes" => []]);
        }
    }

    public function getStockporLote(Request $request)
    {
      $nro_lote = $request->nro_lote;
      $producto_id = $request->producto_id;
      $proveedor_id = isset($request->proveedor_id)? $request->proveedor_id : 3;
      $stock = Resumen_Stock_Tela::where("producto_id", "=", $producto_id)
          ->where("proveedor_id", "=", $proveedor_id)
          ->where("nro_lote", "=", $nro_lote)
          ->first();

      if (!is_null($stock)) {
          return response(["rst" => 1,"stock" => $stock]);
        } else {
          return response(["rst" => 2,"msj" => "No existen lotes disponibles!!!"]);
        }
    }
    public function imprimirGuia(Request $request)
    {
      $id = $request->id;
      $despacho = DespachoTintoreria::with("detalledespachotintoreria", "detalledespachotintoreria.producto", "detalledespachotintoreria.color", "proveedor", "planeamiento", "planeamiento.detalles", "transportista")->find($id);
        $dia=Carbon::now()->format('d');  
        $mes=Carbon::now()->format('m');  
        $anio=Carbon::now()->format('Y');  
        $mes_n="";
        $resultado = [
          "proveedor" => null,
          "direccion_partida" => "",
          "direccion_llegada" => "",
          "transportista" => null,
          "detalle" => []
        ];
        switch ($mes) {
            case '01': $mes_n="Enero";break;
            case '02': $mes_n="Febrero";break;
            case '03': $mes_n="Marzo";break;
            case '04': $mes_n="Abril";break;
            case '05': $mes_n="Mayo";break;
            case '06': $mes_n="Junio";break;
            case '07': $mes_n="Julio";break;
            case '08': $mes_n="Agosto";break;
            case '09': $mes_n="Setiembre";break;
            case '10': $mes_n="Octubre";break;
            case '11': $mes_n="Noviembre";break;
            case '12': $mes_n="Diciembre";break;
        }
        $lote = "";
        if (!is_null($despacho)) {
          $resultado["proveedor"] = $despacho["proveedor"];
          $resultado["transportista"] = $despacho["transportista"];
          $resultado["direccion_llegada"] = $despacho["direccion_llegada"];
          $resultado["direccion_partida"] = $despacho["direccion_partida"];
          foreach ($despacho["detalledespachotintoreria"] as $key => $value) {
            if(!isset($resultado["detalle"][$value->nro_lote])) {
              $resultado["detalle"][$value->nro_lote] = [];
            }
            if (!isset($resultado["detalle"][$value->nro_lote][$value->producto_id])) {
              $resultado["detalle"][$value->nro_lote][$value->producto_id] = $value;
            } else {
              $resultado["detalle"][$value->nro_lote][$value->producto_id]->rollos+= $value->rollos;
              $resultado["detalle"][$value->nro_lote][$value->producto_id]->cantidad+= $value->cantidad;
            }
          }
        }
        foreach ($despacho["detalledespachotintoreria"] as $key => $value) {
          # code...
        }
        if (!is_null($despacho["planeamiento"])) {
          if (count($despacho["planeamiento"]["detalles"]) > 0) {
            foreach ($despacho["planeamiento"]["detalles"] as $key => $value) {
              if ($value->lote_insumo!="0") {
                $lote = $value->lote_insumo;
              }
            }
          }
        }

        $view =  \View::make('tintoreria.print.guiaremision', compact("despacho","dia","mes","anio","mes_n", "lote", "resultado"))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper([5, 0, 630, 630], 'landscape');//->setPaper('a4', 'potrait');
        return $pdf->stream('invoice');   
    }
}
