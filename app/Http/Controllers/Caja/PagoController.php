<?php
namespace App\Http\Controllers\Caja;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ObjectViews\Filtro;

use DB;
use PDF;
use App;
use App\ObjectViews\Reporte;
use App\Compra;
use App\DespachoTerceros;
use App\ProveedorDespachoTintoreriaDeuda;
use App\ProveedorDespachoTerceroDeuda;
use App\Pago;

class PagoController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $queryGuia1 = "";
            $queryGuia2 = "";
            $queryGuia3 = "";

            $queryProveedor1 = "";
            $queryProveedor2 = "";
            $queryProveedor3 = "";

            $queryInicio1 = "";
            $queryInicio2 = "";
            $queryInicio3 = "";
            if ($request->fechafiltroinicio && $request->fechafiltroinicio != "") {
                $queryInicio1.=" AND DATE(pdtd.created_at) >= '{$request->fechafiltroinicio}' ";
                $queryInicio2.=" AND DATE(pdtid.created_at) >= '{$request->fechafiltroinicio}' ";
                $queryInicio3.=" AND DATE(c.fecha) >= '{$request->fechafiltroinicio}' ";
            }
            if ($request->fechafiltrofin && $request->fechafiltrofin!="") {
                    $queryInicio1.=" AND DATE(pdtd.created_at) <= '{$request->fechafiltrofin}' ";
                    $queryInicio2.=" AND DATE(pdtid.created_at) <= '{$request->fechafiltrofin}' ";
                    $queryInicio3.=" AND DATE(c.fecha) <= '{$request->fechafiltrofin}' ";
            }
            if ($request->proveedorfiltro && $request->proveedorfiltro!="") {
                    $queryProveedor1.= "AND pdtd.proveedor_id = {$request->proveedorfiltro} ";
                    $queryProveedor2.= "AND pdtid.proveedor_id = {$request->proveedorfiltro} ";
                    $queryProveedor3.= "AND c.proveedor_id = {$request->proveedorfiltro} ";
            }
            if ($request->guiafiltro && $request->guiafiltro!="") {
                    $queryGuia1.= "AND dt.nroguia LIKE '%{$request->guiafiltro}%' ";
                    $queryGuia2.= "AND dt.nroguia LIKE '%{$request->guiafiltro}%' ";
                    $queryGuia3.= "AND c.nro_guia LIKE '%{$request->guiafiltro}%' ";
            }
            $query = '(SELECT
				DATE(pdtd.created_at) AS fecha,
				pdtd.despacho_id AS iddeuda,
				pr.nombre_comercial AS proveedor,
                "" AS color,
				p.nombre_generico AS producto,
				IF(pdtd.moneda_id = 1, "Soles", IF(pdtd.moneda_id = 2, "USD", "")) AS moneda,
                pdtd.moneda_id,
				dt.nroguia AS guia,
				SUM(pdtd.total) AS deuda,
				SUM(pdtd.totalpagado) AS pagado,
				SUM(pdtd.total - pdtd.totalpagado) AS saldo,
				"Servicio" AS tipo 
				FROM proveedor_despacho_tercero_deuda AS pdtd
				LEFT JOIN productos AS p ON p.id = pdtd.producto_id
				LEFT JOIN proveedores AS pr ON pr.id = pdtd.proveedor_id
				LEFT JOIN despacho_terceros AS dt ON dt.id = pdtd.despacho_id
				WHERE 1 '.$queryInicio1.' '.$queryProveedor1.' '.$queryGuia1.'
                GROUP BY pdtd.despacho_id, pdtd.producto_id, pdtd.detalle_despacho_id
			)
			UNION
			(SELECT 
				DATE(pdtid.created_at) AS fecha,
				pdtid.despacho_id AS iddeuda,
				pr.nombre_comercial AS proveedor,
                c.nombre AS color,
				p.nombre_generico AS producto,
				IF(pdtid.moneda_id = 1, "Soles", IF(pdtid.moneda_id = 2, "USD", "")) AS moneda,
                pdtid.moneda_id,
				dt.nroguia AS guia,
				SUM(pdtid.total) AS deuda,
				SUM(pdtid.totalpagado) AS pagado,
				SUM(pdtid.total - pdtid.totalpagado) AS saldo,
				"Tintoreria" AS tipo
				FROM proveedor_despacho_tintoreria_deuda AS pdtid
				LEFT JOIN productos AS p ON p.id = pdtid.producto_id
				LEFT JOIN proveedores AS pr ON pr.id = pdtid.proveedor_id
				LEFT JOIN despacho_tintoreria AS dt ON dt.id = pdtid.despacho_id
                LEFT JOIN color as c ON c.id = color_id
				WHERE 1 '.$queryInicio2.' '.$queryProveedor2.' '.$queryGuia2.' 
				GROUP BY pdtid.despacho_id, pdtid.producto_id, pdtid.color_id, pdtid.detalle_despacho_id
			)
			UNION
			(SELECT 
				DATE(c.fecha) AS fecha,
				c.id AS iddeuda,
				pr.nombre_comercial AS proveedor,
                "" as color,
				IFNULL(p.nombre_generico, IFNULL(i.nombre_generico, a.nombre)) AS producto,
				IF(c.moneda = 1, "Soles", IF(c.moneda = 2, "USD", "")) AS moneda,
                c.moneda AS moneda_id,
				c.nro_guia AS guia,
				c.total AS deuda,
				c.totalpagado AS pagado,
				(c.total - c.totalpagado) AS saldo,
				"Compra" AS tipo
				FROM compras AS c
				LEFT JOIN detalle_compras AS dc ON dc.compra_id = c.id
				LEFT JOIN productos AS p ON p.id = dc.producto_id
				LEFT JOIN insumos AS i ON i.id = dc.insumo_id
				LEFT JOIN accesorios AS a ON a.id = dc.accesorio_id
				LEFT JOIN proveedores AS pr ON pr.id = c.proveedor_id
				WHERE c.total > 0 '.$queryInicio3.' '.$queryProveedor3.' '.$queryGuia3.'
			GROUP BY dc.compra_id)';
            $cantidad = DB::select('SELECT COUNT(*) AS cantidad FROM ('.$query.') AS a');
            $deudas = DB::select($query);

            return response([
                "draw" => 1,
                "recordsTotal" => $cantidad[0]->cantidad,
                "recordsFiltered" => $request->length,
                "data" => $deudas
            ]);
        }
        $objfiltro = new Filtro;
        $filtro = $objfiltro->filtroReporteBasico();
        return view("caja.pago.index", $filtro);
    }

    public function pagar(Request $request)
    {
        $tipo = $request->tipo;
        $fecha = $request->fecha;
        $monto = $request->monto;
        $tipoComprobante = $request->tipocomprobante;
        $nroComprobante = $request->nrocomprobante;
        $item = $request->item;
        $moneda = $request->moneda;

        if ($tipoComprobante == "") {
            return response(["rst" => 2, "msj" => "El Tipo de Comprobante es Obligatorio!!!"]);
        }
        if ($monto == "") {
            return response(["rst" => 2, "msj" => "Ingrese un Monto a Pagar!!!"]);
        }
        if ($monto <=0) {
            return response(["rst" => 2, "msj" => "El Monto a Pagar debe ser Mayor a 0!!!"]);
        }
        $obj = null;
        $deuda = 0;
        $objPago = new Pago;
        switch ($tipo) {
            case "Tintoreria":
                $obj = ProveedorDespachoTintoreriaDeuda::find($item);
                $deuda = $obj->getDeuda();
                if ($deuda < 0) {
                    return response(["rst" => 2, "msj" => "El Despacho de Tintoreria no Tiene Deuda"]);
                }
                $objPago->proveedor_despacho_tintoreria_deuda_id = $obj->id;
                break;
            case 'Servicio':
                $obj = ProveedorDespachoTerceroDeuda::find($item);
                $deuda = $obj->getDeuda();
                if ($deuda < 0) {
                    return response(["rst" => 2, "msj" => "El Despacho de Tintoreria no Tiene Deuda"]);
                }
                $objPago->proveedor_despacho_tercero_deuda_id = $obj->id;
                break;
            case 'Compra':
                $obj = Compra::find($item);
                $deuda = $obj->getDeuda();
                if ($deuda < 0) {
                    return response(["rst" => 2, "msj" => "La Compra no Tiene Deuda"]);
                }
                $objPago->compra_id = $obj->id;
                break;
            default:
                break;
        }
        if ($monto > $deuda) {
            return response(["rst" => 2, "msj" => "El monto a Pagar es Mayor a la Deuda!!!"]);
        }
        DB::beginTransaction();
        try {
            $obj->totalpagado+=$monto;
            if ($obj->totalpagado == $obj->total) {
                if ($tipo == "Compra") {
                    $obj->estado_pago = 2;
                } else {
                    $obj->estado = 2;
                }

            } else {
                if ($tipo == "Compra") {
                    $obj->estado_pago = 1;
                } else {
                    $obj->estado = 1;
                }
            }
            $obj->save();

            $objPago->total = $monto;
            $objPago->tipo_comprobante = $tipoComprobante;
            $objPago->moneda_id = $moneda;
            $objPago->nro_comprobante = $nroComprobante;
            $objPago->save();
            DB::commit();
            return response(["rst" => 1, "msj" => "Pago Realizado Correctamente", "obj" => $obj]);
        } catch (Exception $e) {
            DB::rollback();
            return response(["rst" => 2, "msj" => "Error en la BD"]);
        }
    }

    public function listarDetalle(Request $request)
    {
        $lista = [];
        $tipo = $request->tipo;
        $id = $request->id;
        switch ($tipo) {
            case "Tintoreria":
                $lista = Pago::select(
                    "pago.*",
                    DB::raw("IF(pago.moneda_id = 1, 'Soles', IF(pago.moneda_id = 2, 'USD', '')) AS moneda"),
                    DB::raw("IF(pago.tipo_comprobante = 1, 'Boleta', IF(pago.tipo_comprobante = 2, 'Factura', '')) AS comprobante")
                )
                    ->where(["despacho_tintoreria_id" => $id])
                    ->whereRaw("deleted_at IS NULL")
                    ->orderBy("id", "DESC")
                    ->get();
                break;
            case 'Servicio':
                $lista = Pago::select(
                    "pago.*",
                    DB::raw("IF(pago.moneda_id = 1, 'Soles', IF(pago.moneda_id = 2, 'USD', '')) AS moneda"),
                    DB::raw("IF(pago.tipo_comprobante = 1, 'Boleta', IF(pago.tipo_comprobante = 2, 'Factura', '')) AS comprobante")
                )
                    ->where(["despacho_tercero_id" => $id])
                    ->whereRaw("deleted_at IS NULL")
                    ->orderBy("id", "DESC")
                    ->get();
                break;
            case 'Compra':
                $lista = Pago::select(
                    "pago.*",
                    DB::raw("IF(pago.moneda_id = 1, 'Soles', IF(pago.moneda_id = 2, 'USD', '')) AS moneda"),
                    DB::raw("IF(pago.tipo_comprobante = 1, 'Boleta', IF(pago.tipo_comprobante = 2, 'Factura', '')) AS comprobante")
                )
                    ->where(["compra_id" => $id])
                    ->whereRaw("deleted_at IS NULL")
                    ->orderBy("id", "DESC")
                    ->get();
                break;
            default:
                break;
        }
        return response([
                "draw" => 1,
                "recordsTotal" => count($lista),
                "recordsFiltered" => $request->length,
                "data" => $lista
            ]);
    }

    public function destroy($id)
    {
        
        $pago = Pago::find($id);
        $obj = null;
        $iscompra = false;
        if (is_null($pago)) {
            return response(["rst" => 2, "msj" => "No existe Pago"]);
        }
        if (!is_null($pago->compra)) {
            $obj = $pago->compra;
            $iscompra = true;
        }
        if (!is_null($pago->deudaTintoreria)) {
            $obj = $pago->deudaTintoreria;
        }
        if (!is_null($pago->deudaServicio)) {
            $obj = $pago->deudaServicio;
        }
        if (is_null($obj)) {

        }
        $obj->totalpagado-=$pago->total;
        if ($iscompra) {
            $obj->estado_pago = 1;
            if ($obj->totalpagado <=0) {
                $obj->estado_pago = 0;
            }
        } else {
            $obj->estado = 1;
            if ($obj->totalpagado <=0) {
                $obj->estado = 0;
            }
        }
        try {
            $obj->save();
            $pago->delete();
            return response(["rst" => 1, "msj" => "Pago Eliminado"]);
        } catch (Exception $e) {
            return response(["rst" => 2, "msj" => "Error en BD"]);
        }
    }
}
