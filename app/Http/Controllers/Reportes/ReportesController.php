<?php

namespace App\Http\Controllers\Reportes;

use App\Http\Controllers\Controller;
use App\Proveedor;
use App\Producto;
use App\Insumo;
use App\Accesorio;
use App\Empleado;
use App\Compra;
use App\Maquina;
use App\Planeamiento;
use App\Resumen_Stock_MP;
use Yajra\Datatables\Facades\Datatables;
use DB;
use Excel;
use App\Resumen_Stock_Tela;
use App\ResumenDespachoTintoreria;
use App\ProveedorDespachoTintoreriaDeuda;
use Illuminate\Http\Request;

class ReportesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proveedores = Proveedor::all();
        $productos = Producto::all();
        $accesorios = Accesorio::all();
        $insumos = Insumo::all();
        $empleados = Empleado::all();
        $maquinas = Maquina::all();
        return view('reportes.index', compact('proveedores', 'productos', 'accesorios', 'insumos', 'empleados', 'maquinas'));
    }

    public function proveedorTintoreriaDeuda()
    {
        $deudas = ProveedorDespachoTintoreriaDeuda::with('proveedor', 'color', 'producto')->get();
        return view('reportes.proveedor_tela_deuda', compact('deudas'));
    }

    public function compras(Request $request)
    {
        $proveedores = Proveedor::all();
        $productos = Producto::all();
        $accesorios = Accesorio::all();
        $insumos = Insumo::all();
        $empleados = Empleado::all();
        $maquinas = Maquina::all();

        if ($request->ajax()) {
            $requestData = $request->all();
            $compras = Compra::with('proveedor','procedencia','detalles','detalles.titulo','detalles.insumo','detalles.accesorio');

          return Datatables::eloquent($compras)
              ->filter(function ($query) use ($request) {

                                if ($request->has('fechainicio')){
                                  $query->whereDate('fecha', '>=', $request->fechainicio);
                                }
                                 if ($request->has('fechafin')){
                                  $query->whereDate('fecha', '<=', $request->fechafin);
                                }

                                if ($request->has('proveedor')) {
                                  $query->where('proveedor_id', '=', $request->proveedor);
                                }
                                if ($request->has('accesorio')) {
                                  $query->whereHas('detalles', function($query) use ($request){
                                      $query->where('detalle_compras.accesorio_id', '=', $request->accesorio);
                                  });
                                }

                                if ($request->has('insumo')) {
                                  $query->whereHas('detalles', function($query) use ($request){
                                      $query->where('detalle_compras.insumo_id', '=', $request->insumo);
                                  });
                                }
                                if ($request->has('producto')) {
                                  $query->whereHas('detalles', function($query) use ($request){
                                      $query->where('detalle_compras.producto_id', '=', $request->producto);
                                  });
                                }
          })->make(true);        
        }

        return view('reportes.compras', compact('proveedores', 'productos', 'accesorios', 'insumos', 'empleados', 'maquinas'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function stockGeneral(Request $request){

      $proveedores = Proveedor::all();
      $productos = Producto::all();
      $accesorios = Accesorio::all();
      $insumos = Insumo::all();
      $empleados = Empleado::all();
      $maquinas = Maquina::all();

      if($request->ajax()){
        $resumenes = Resumen_Stock_MP::with('insumo','accesorio', 'proveedor', 'titulo');
        return Datatables::eloquent($resumenes)
                        ->filter(function ($query) use ($request) {

                              if ($request->has('accesorio')) {
                                $query->where('accesorio_id', '=', $request->accesorio);
                              }

                              if ($request->has('insumo')) {
                                $query->where('insumo_id', '=', $request->insumo);
                              }

                              if ($request->has('lote') && $request->lote!="") {
                                $query->where("lote", $request->lote);
                              }
                              if ($request->has('proveedor') && $request->proveedor!="") {
                                $query->where('proveedor_id', '=', $request->proveedor);
                              }

                        })
                        ->make(true);        
      }

      return view('reportes.stock_general.index',compact('proveedores','productos','accesorios','insumos','empleados','maquinas'));


    }

     public function despachoTintoreria(Request $request){
      $proveedores = Proveedor::all();
      $productos = Producto::all();
      $accesorios = Accesorio::all();
      $insumos = Insumo::all();
      $empleados = Empleado::all();
      $maquinas = Maquina::all();

      if($request->ajax()){
        $telas = ResumenDespachoTintoreria::select("resumen_despacho_tintoreria.*", "p.nombre_generico as producto", "c.nombre as color")
          ->leftJoin("productos as p", "p.id", "=", "resumen_despacho_tintoreria.producto_id")
          ->leftJoin("color as c", "c.id", "=", "resumen_despacho_tintoreria.color_id")
          ->orderBy("resumen_despacho_tintoreria.id", "DESC");
        return Datatables::eloquent($telas)
                    ->make(true);
      }

      return view('reportes.despacho_tintoreria',compact('proveedores','productos','accesorios','insumos','empleados','maquinas'));

    }

    public function telasResumen(Request $request)
    {
        $proveedores = Proveedor::all();
        $productos = Producto::all();
        $accesorios = Accesorio::all();
        $insumos = Insumo::all();
        $empleados = Empleado::all();
        $maquinas = Maquina::all();
        if ($request->ajax()) {
            $columns = $request->columns;
            $start = $request->start;
            $length = $request->length;
            $order = $request->order;
            $telas = Resumen_Stock_Tela::with('producto', 'proveedor')->where("cantidad", ">", 0);
            return Datatables::eloquent($telas)
                        ->filter(function ($query) use ($request) {
                            if ($request->has('producto')) {
                                if ($request->producto !="") {
                                    $query->where('producto_id', '=', $request->producto);
                                }
                            }
                            if ($request->has('proveedor')) {
                                $query->where('proveedor_id', '=', $request->proveedor);
                            }
                        })->make(true);
        }
        return view('reportes.telas', compact('proveedores', 'productos', 'accesorios', 'insumos', 'empleados', 'maquinas'));
    }

    public function telasDescargar(Request $request){

      $producto = $request->producto;
      $proveedor = $request->proveedor;

      $resumen_stock = DB::table("resumen_stock_telas")
                          ->join("productos","productos.id","=","resumen_stock_telas.producto_id")
                          ->join("proveedores", "proveedores.id", "=", "resumen_stock_telas.proveedor_id");

      if($producto) $resumen_stock->where("producto_id", $producto);
      if($proveedor) $resumen_stock->where("proveedor_id", $proveedor);

      $resumenes =  $resumen_stock->select(
        'proveedores.nombre_comercial as Proveedor',
        'productos.nombre_generico as Producto',
        'resumen_stock_telas.nro_lote as Lote',
        'rollos as Rollos',
        DB::raw("cantidad as 'Peso Neto(KG)'")
      )->orderBy("proveedores.nombre_comercial", "ASC")->whereRaw("resumen_stock_telas.deleted_at IS NULL")->get();
       $resumenes_array = array();
       foreach ($resumenes as $key => $resumen) {
         $resumenes_array[] = (array)$resumen;
       }
       Excel::create('Reporte_stock_telas_'.date('Ymdhis'), function($excel) use($resumenes_array) {
           $excel->sheet('Sheetname', function($sheet) use($resumenes_array) {

               $sheet->setAllBorders('thin');
               $sheet->mergeCells('A1:E1');
               $sheet->row(1, array(
                'Reporte de stock telas'
               ));

               $sheet->cell('A1', function($cell) {
                   $cell->setFont(array(
                       'family'     => 'Calibri',
                       'size'       => '16',
                       'bold'       =>  true
                   ));
                   $cell->setAlignment('center');

               });

               $sheet->cell('A3:E3', function($cells) {
                 $cells->setFont(array(
                     'family'     => 'Calibri',
                     'size'       => '12',
                     'bold'       =>  true
                 ));
               });

               $sheet->fromArray($resumenes_array, null, 'A3', false, true);
           });

       })->export('xls');

    }

    public function planeamientosResumen(Request $request){

      $proveedores = Proveedor::all();
      $productos = Producto::all();
      $accesorios = Accesorio::all();
      $insumos = Insumo::all();
      $empleados = Empleado::all();
      $maquinas = Maquina::all();

      if($request->ajax()){
      $planeamientos = Planeamiento::select(
        "planeamientos.*",
        DB::raw("(SELECT lote_insumo FROM detalle_planeamientos AS dp LEFT JOIN indicador AS ind ON dp.insumo_id = ind.insumo_id WHERE dp.planeamiento_id = planeamientos.id AND dp.lote_insumo <> 0 AND  ind.producto_id = planeamientos.producto_id AND dp.titulo_id = ind.titulo_id ORDER BY ind.valor DESC LIMIT 1) AS Lote")
      )
        ->with('empleado','maquina','proveedor','producto')->where("planeamientos.estado", 0);
      return Datatables::eloquent($planeamientos)
                      ->filter(function ($query) use ($request) {


                            $q = $request->search["value"];
                            if ($q) {
                              $query->whereHas('empleado', function($query) use ($request,$q){
                                  $query->where('empleados.nombres', 'LIKE', $q);
                              });

                              $query->whereHas('proveedor', function($query) use ($request,$q){
                                  $query->where('proveedores.nombre_comercial', 'LIKE', $q);
                              });

                            }

                            if ($request->has('fechainicio')){
                              $query->whereRaw("fecha >=  '{$request->fechainicio}' ");
                            } else {
                              $hoy = date("Y-m-d");
                              $query->whereRaw("fecha >=  '{$hoy}' ");
                            }

                            if ($request->has('fechafin')){
                              $query->whereRaw("fecha <=  '{$request->fechafin}' ");
                            } else {
                              $hoy = date("Y-m-d");
                              $query->whereRaw("fecha <=  '{$hoy}' ");
                            }

                            if($request->has('producto')){
                              $query->where("producto_id","=",$request->producto);
                            }

                            if($request->has('maquina')){
                              $query->where("maquina_id","=",$request->maquina);
                            }

                            if ($request->has('estado')){
                              $query->where('estado', '=', $request->estado);
                            }

                            if ($request->has('turno')) {
                                $query->where('turno', '=', $request->turno);
                            }

                            if ($request->has('proveedor')) {
                              $query->where('proveedor_id', '=', $request->proveedor);
                            }
                            if ($request->has('empleado')) {
                              $query->where('empleado_id', '=', $request->empleado);
                            }
                      })
                      ->make(true);
      }
      return view('reportes.planeamientos',compact('proveedores','productos','accesorios','insumos','empleados','maquinas'));


    }

    public function planeamientosDescargar(Request $request){
      // SELECT p.fecha 'Fecha', pr.nombre_comercial 'Proveedor', CONCAT(e.nombres,' ',e.apellidos) 'Colaborador', p.turno 'Turno',m.nombre 'Maquina',prd.nombre_generico 'Producto', dp.lote_insumo 'Lote',i.nombre_generico 'MP', t.nombre 'Titulo',dp.cajas 'Cajas', dp.Kg 'Kg', p.rollos 'Rollos',p.kg_producidos 'Kg Pr', p.kg_falla 'Falla Kg' FROM planeamientos p INNER JOIN detalle_planeamientos dp ON p.id = dp.planeamiento_id INNER JOIN empleados e ON e.id = p.empleado_id INNER JOIN maquinas m ON m.id = p.maquina_id LEFT JOIN accesorios a ON a.id = dp.accesorio_id INNER JOIN titulos t ON t.id = dp.titulo_id LEFT JOIN insumos i ON i.id = dp.insumo_id INNER JOIN proveedores pr ON pr.id = p.proveedor_id INNER JOIN productos prd ON prd.id = p.producto_id

      $producto = $request->producto;
      $maquina = $request->maquina;
      $proveedor = $request->proveedor;
      $empleado = $request->empleado;
      $fechainicio = $request->fecha_inicio;
      $fechafin = $request->fecha_fin;

      $planeamientos_query = DB::table("planeamientos")
          ->join("empleados","planeamientos.empleado_id","=","empleados.id")
          ->join("maquinas","planeamientos.maquina_id","=","maquinas.id")
          ->join("proveedores","planeamientos.proveedor_id","=","proveedores.id")
          ->join("productos","productos.id","=","planeamientos.producto_id");

      if($fechainicio) $planeamientos_query->whereRaw("fecha >= '{$fechainicio}' ");
      if($fechafin) $planeamientos_query->whereRaw("fecha <= '{$fechafin}' ");
      if($producto) $planeamientos_query->where("producto_id","=",$producto);
      if($maquina) $planeamientos_query->where("maquina_id","=",$maquina);
      if($proveedor) $planeamientos_query->where("proveedor_id","=",$proveedor);
      if($empleado) $planeamientos_query->where("empleado_id","=",$empleado);

      $planeamientos_query->select(
        'fecha as Fecha',
        'proveedores.nombre_comercial as Proveedor',
        DB::raw("CONCAT(empleados.nombres,' ',empleados.apellidos) as Tejedor"),
        'planeamientos.turno as Turno',
        'maquinas.nombre as Maquina',
        DB::raw("(SELECT lote_insumo FROM detalle_planeamientos AS dp LEFT JOIN indicador AS ind ON dp.insumo_id = ind.insumo_id WHERE dp.planeamiento_id = planeamientos.id AND dp.lote_insumo <> 0 AND  ind.producto_id = planeamientos.producto_id AND dp.titulo_id = ind.titulo_id ORDER BY ind.valor DESC LIMIT 1) AS Lote"),
        'productos.nombre_generico as Producto'
        
      );
      $planeamientos = $planeamientos_query->where("estado", 0)->orderBy("planeamientos.fecha", "ASC")->get();
      $objPlaneamientos = $planeamientos;
      //$planeamientos = DB::select(DB::raw("SELECT p.fecha 'Fecha', pr.nombre_comercial 'Proveedor', CONCAT(e.nombres,' ',e.apellidos) 'Tejedor', p.turno 'Turno',m.nombre 'Maquina',prd.nombre_generico 'Producto' FROM planeamientos p INNER JOIN empleados e ON e.id = p.empleado_id INNER JOIN maquinas m ON m.id = p.maquina_id INNER JOIN proveedores pr ON pr.id = p.proveedor_id INNER JOIN productos prd ON prd.id = p.producto_id "));
      $planeamientos = array();
      foreach ($objPlaneamientos as $objPlaneamiento) {
        $planeamientos[] = (array)$objPlaneamiento;
      }


      Excel::create('Reporte de planeamientos_'.date("Ymdhis"), function($excel) use($planeamientos) {
          $excel->sheet('Sheetname', function($sheet) use($planeamientos) {
              $sheet->setAllBorders('thin');
              $sheet->mergeCells('A1:G1');
              $sheet->row(1, array(
               'Planeamientos para la Produccion de Tela'
              ));

              $sheet->cell('A1', function($cell) {
                  $cell->setFont(array(
                      'family'     => 'Calibri',
                      'size'       => '16',
                      'bold'       =>  true
                  ));
                  $cell->setAlignment('center');

              });

              $sheet->cell('A3:G3', function($cells) {
                $cells->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '12',
                    'bold'       =>  true
                ));
              });

              $sheet->fromArray($planeamientos, null, 'A3', false, true);

          });

      })->export('xls');

    }

    public function resumenDescargar(Request $request){
      $insumo = $request->insumo;
      $accesorio = $request->accesorio;
      $proveedor = $request->proveedor;
      $lote = $request->lote;

      $resumen_stock = DB::table("resumen_stock_materiaprima")
                          ->join("proveedores","proveedores.id","=","resumen_stock_materiaprima.proveedor_id")
                          ->leftJoin("insumos","insumos.id","=","resumen_stock_materiaprima.insumo_id")
                          ->leftJoin("accesorios","accesorios.id","=","resumen_stock_materiaprima.accesorio_id")
                          ->leftJoin("titulos", "titulos.id", "=", "resumen_stock_materiaprima.titulo_id");

      if($insumo && $insumo!="") $resumen_stock->where("insumo_id",$insumo);
      if($accesorio && $accesorio!="") $resumen_stock->where("accesorio_id",$accesorio);
      if($proveedor) $resumen_stock->where("resumen_stock_materiaprima.proveedor_id",$proveedor);
      if($lote && $lote!="") $resumen_stock->where("lote", $lote);

      $resumenes =  $resumen_stock->select(
          'proveedores.razon_social as Proveedor',
          'lote as Lote',
          DB::raw('CASE WHEN accesorio_id = 0
                 THEN insumos.nombre_generico
              ELSE accesorios.nombre
          END as Producto'),
          "titulos.nombre as Titulo",
          DB::raw("CASE WHEN accesorio_id = 0
                 THEN peso_neto
              ELSE cantidad
          END as 'P.Neto/Cant.' "),
          DB::raw("CASE WHEN accesorio_id = 0
                 THEN cantidad
              ELSE ''
          END as 'Rollos' ")
        )
        ->whereRaw("resumen_stock_materiaprima.deleted_at IS NULL")
        ->orderBy("proveedores.nombre_comercial", "ASC")
        ->get();
       $resumenes_array = array();
       foreach ($resumenes as $key => $resumen) {
         $resumenes_array[] = (array)$resumen;
       }
       Excel::create('Reporte de Stock General de Materia Prima'.date("Ymdhis"), function($excel) use($resumenes_array) {
           $excel->sheet('Sheetname', function($sheet) use($resumenes_array) {
             $sheet->setAllBorders('thin');
             $sheet->mergeCells('A1:F1');
             $sheet->row(1, array(
              'Reporte de Stock General de Materia Prima'
             ));

             $sheet->cell('A1', function($cell) {
                 $cell->setFont(array(
                     'family'     => 'Calibri',
                     'size'       => '16',
                     'bold'       =>  true
                 ));
                 $cell->setAlignment('center');

             });

             $sheet->cell('A3:F3', function($cells) {
               $cells->setFont(array(
                   'family'     => 'Calibri',
                   'size'       => '12',
                   'bold'       =>  true
               ));
             });

             $sheet->fromArray($resumenes_array, null, 'A3', false, true);

           });

       })->export('xls');

    }

    public function comprasDescargar(Request $request){

      $fechainicio = $request->fechainicio;
      $fechafin = $request->fechafin;
      $proveedor = $request->proveedor;
      $accesorio = $request->accesorio;
      $insumo = $request->insumo;
      $producto = $request->producto;

      $compras_query = DB::table("compras")
          ->join("detalle_compras","detalle_compras.compra_id","=","compras.id")
          ->join("proveedores","proveedores.id","=","compras.proveedor_id")
          ->join("procedencias","procedencias.id","=","compras.procedencia_id")
          ->join("titulos","titulos.id","=","detalle_compras.titulo_id")
          ->leftJoin("insumos","insumos.id","=","detalle_compras.insumo_id")
          ->leftJoin("accesorios","accesorios.id","=","detalle_compras.accesorio_id")
          ->leftJoin("productos","productos.id","=","detalle_compras.producto_id");

      if($fechainicio) $compras_query->whereDate("fecha",">=", $fechainicio);
      if($fechafin) $compras_query->whereDate("fecha","<=", $fechafin);
      if($proveedor) $compras_query->where("compras.proveedor_id",$proveedor);
      if($accesorio) $compras_query->where("detalle_compras.accesorio_id",$accesorio);
      if($insumo) $compras_query->where("detalle_compras.insumo_id",$insumo);
      if($producto) $compras_query->where("detalle_compras.producto_id",$producto);

      $compras = $compras_query->select(
          'fecha as Fecha',
          'nombre_comercial as Proveedor',
          'procedencias.nombre as Procedencia',
          'compras.codigo as Cod.Compra',
          'compras.nro_guia as Guia',
          DB::raw("IF(compras.nro_comprobante='', 'NO', 'SI') AS Factura"),
          'compras.nro_comprobante as #Factura',
          'nro_lote as Lote',
          DB::raw("IF(detalle_compras.accesorio_id IS NOT NULL, accesorios.nombre,
            IF(detalle_compras.insumo_id IS NOT NULL, insumos.nombre_generico, 
              IF(detalle_compras.producto_id IS NOT NULL, productos.nombre_generico, '') 
            )
          ) as Producto"),
          'titulos.nombre as Titulo/Codigo',
          'detalle_compras.cantidad as Cantidad',
          DB::raw('(detalle_compras.peso_bruto - detalle_compras.peso_tara) as "Peso Neto"'),
          "compras.precio",
          DB::raw("IF(compras.moneda = 1, 's/.', 'USD') as Moneda "),
          "compras.total AS Monto",
          DB::raw("IF(detalle_compras.accesorio_id IS NOT NULL, 'A',
            IF(detalle_compras.insumo_id IS NOT NULL, 'I', 
              IF(detalle_compras.producto_id IS NOT NULL, 'T', 'N') 
            )
          ) as tipo_producto")
      )->orderBy("compras.fecha", "ASC")->get();
      $array_compras = array();
      $totalinsumo = ["cantidad" => 0, "peso" => 0, "monto" => 0, "moneda" => "USD"];
      $totalaccesorio = ["cantidad" => 0, "peso" => 0, "monto" => 0, "moneda" => "USD"];
      $totaltela = ["cantidad" => 0, "peso" => 0, "monto" => 0, "moneda" => "USD"];
      foreach ($compras as $key => &$compra) {
        $compra = (array)$compra;
        switch($compra["tipo_producto"]) {
          case "A":
            $totalaccesorio["cantidad"]+=$compra["Cantidad"];
            $totalaccesorio["peso"]+=$compra["Peso Neto"];
            $totalaccesorio["monto"]+=$compra["Monto"];
            $totalaccesorio["moneda"] =$compra["Moneda"];
            break;

          case "I":
            $totalinsumo["cantidad"]+=$compra["Cantidad"];
            $totalinsumo["peso"]+=$compra["Peso Neto"];
            $totalinsumo["monto"]+=$compra["Monto"];
            $totalinsumo["moneda"] = $compra["Moneda"];
            break;

          case "T":
            $totaltela["cantidad"]+=$compra["Cantidad"];
            $totaltela["peso"]+=$compra["Peso Neto"];
            $totaltela["monto"]+=$compra["Monto"];
            $totaltela["moneda"] = $compra["Moneda"];
            break;

          case "N":
            break;
        }
        unset($compra["tipo_producto"]);
        $array_compras[] = $compra;
      }
      $array_compras[] = [
        "", "", "", "", "", "", "", "", "",
        "Total Insumo", number_format($totalinsumo["cantidad"], 2), number_format($totalinsumo["peso"], 2),
        "0.00", $totalinsumo["moneda"], number_format($totalinsumo["monto"], 2)
      ];

      $array_compras[] = [
        "", "", "", "", "", "", "", "", "",
        "Total Accesorio", number_format($totalaccesorio["cantidad"], 2), number_format($totalaccesorio["peso"], 2),
        "0.00", $totalaccesorio["moneda"], number_format($totalaccesorio["monto"], 2)
      ];

      $array_compras[] = [
        "", "", "", "", "", "", "", "", "",
        "Total Tela", number_format($totaltela["cantidad"], 2), number_format($totaltela["peso"], 2),
        "0.00", $totaltela["moneda"], number_format($totaltela["monto"], 2)
      ];

      Excel::create('Reporte de compras'.date("Ymdhis"), function($excel) use($array_compras) {
          $excel->sheet('Sheetname', function($sheet) use($array_compras) {
            $sheet->setAllBorders('thin');
            $sheet->mergeCells('A1:O1');
            $sheet->row(1, array(
             'Reporte de compras'
            ));

            $sheet->cell('A1', function($cell) {
                $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '16',
                    'bold'       =>  true
                ));
                $cell->setAlignment('center');

            });

            $sheet->cell('A3:O3', function($cells) {
              $cells->setFont(array(
                  'family'     => 'Calibri',
                  'size'       => '12',
                  'bold'       =>  true
              ));
            });

            $sheet->fromArray($array_compras, null, 'A3', false, true);

          });

      })->export('xls');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
