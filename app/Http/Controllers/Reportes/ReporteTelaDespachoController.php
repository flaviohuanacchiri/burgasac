<?php

namespace App\Http\Controllers\Reportes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ObjectViews\Filtro;
use DB;
use PDF;
use App;
use App\ObjectViews\ReporteTela as Reporte;

class ReporteTelaDespachoController extends Controller implements ReporteInterfaceController
{
    public function index(Request $request)
    {
        $objreporte = new Reporte;
        $filtro = [];
        if ($request->fechafiltroinicio!="") {
            $from = $request->fechafiltroinicio." 00:00:00";
            $to = $request->fechafiltrofin." 23:59:59";
            $filtro["fecha"] = [$from, $to];
        }
        if ($request->proveedorfiltro) {
                $filtro["proveedor"] = $request->proveedorfiltro;
        }
        if ($request->productofiltro) {
            $filtro["producto"] = $request->productofiltro;
        }
        if ($request->flag) {
            return ReporteTelaDespachoController::getPdf($filtro);
        }
        if ($request->ajax()) {
            $deudas = $objreporte->telaDespacho($filtro);
            $deudas = $deudas->get();
            return response(["draw" => 1, "recordsTotal" => 0, "recordsFiltered" => 0, "data" => $deudas]);
        }
        $objfiltro = new Filtro;
        $filtro = $objfiltro->filtroReporteBasico();
        return view("reportes.tela_despacho.index", $filtro);
    }

    public static function getPdf($filtro)
    {
        $objreporte = new Reporte;
        $data = $objreporte->telaDespacho($filtro)->get();
        //print_r($data);
        //dd("aqui");
        foreach ($data as $key => $value) {
            @$value->indicador = $value->getIndicador();
        }
		$data = json_decode(json_encode($data), true);
		$view = \View::make('reportes.tela_despacho.pdf', compact("data"))->render();
       	$pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4', 'landscape');
        return $pdf->stream();
	}
}