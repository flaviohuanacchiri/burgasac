<?php namespace App\Http\Controllers\Reportes;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ObjectViews\Filtro;

use DB;
use PDF;
use App;
use App\ObjectViews\ReporteMerma as Reporte;

class ReporteMermaTintoreriaController extends Controller implements ReporteInterfaceController
{
	public function index(Request $request)
	{
		$objreporte = new Reporte;
		$filtro = [];
			if ($request->fechafiltroinicio!="") {
				$from = $request->fechafiltroinicio;
				$to = $request->fechafiltrofin;
				$filtro["fecha"] = [$from, $to];
			}

			if ($request->productofiltro) {
				$filtro["producto"] = $request->productofiltro;
			}
		if ($request->flag){
			return ReporteMermaTintoreriaController::getPdf($filtro);
		}
		if ($request->ajax()) {
	
			$deudas = $objreporte->tintoreria($filtro);
			//$deudas
			//$deudas = ProveedorDespachoTintoreriaDeuda::all();
			
			return response(["draw" => 1, "recordsTotal" => 0, "recordsFiltered" => 0, "data" => $deudas ]);
		}
		$objfiltro = new Filtro;
		$filtro = $objfiltro->filtroReporteBasico();
		return view("reportes.merma_tintoreria.index", $filtro);
	}

	public static function getPdf($filtro)
	{
		$objreporte = new Reporte;
		$data = $objreporte->tintoreria($filtro);
		$view = \View::make('reportes.merma_tintoreria.pdf', compact("data"))->render();
       	$pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4', 'landscape');
        return $pdf->stream();
	}
}