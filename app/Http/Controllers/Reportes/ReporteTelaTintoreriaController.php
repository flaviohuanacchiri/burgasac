<?php

namespace App\Http\Controllers\Reportes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ObjectViews\Filtro;
use DB;
use PDF;
use App;
use App\ObjectViews\ReporteTela as Reporte;

class ReporteTelaTintoreriaController extends Controller implements ReporteInterfaceController
{
    public function index(Request $request)
    {
        $objreporte = new Reporte;
        $filtro = [];
        if ($request->fechafiltroinicio!="") {
            $from = $request->fechafiltroinicio." 00:00:00";
            $to = $request->fechafiltrofin." 23:59:59";
            $filtro["fecha"] = [$from, $to];
        }
        if ($request->proveedorfiltro) {
            $filtro["proveedor"] = $request->proveedorfiltro;
        }
        if ($request->colorfiltro) {
            $filtro["color"] = $request->colorfiltro;
        }
        if ($request->productofiltro) {
            $filtro["producto"] = $request->productofiltro;
        }
        if ($request->flag) {
            return ReporteTelaTintoreriaController::getPdf($filtro);
        }
        if ($request->ajax()) {
            $deudas = $objreporte->telaTintoreria($filtro);
            $deudas = $deudas->get();
            return response(["draw" => 1, "recordsTotal" => 0, "recordsFiltered" => 0, "data" => $deudas]);
        }
        $objfiltro = new Filtro;
        $filtro = $objfiltro->filtroReporteBasico();
        return view("reportes.tela_tintoreria.index", $filtro);
    }

    public static function getPdf($filtro)
    {
        $objreporte = new Reporte;
		$data = $objreporte->telaTintoreria($filtro)->get();
		$agrupar = [];
		foreach ($data as $key => $value) {
			if (!isset($agrupar[$value->proveedor])) {
				$agrupar[$value->proveedor] = [];
			}
			if (!isset($agrupar[$value->proveedor][$value->nroguia])) {
				$agrupar[$value->proveedor][$value->nroguia] = [];
			}
			if (!isset($agrupar[$value->proveedor][$value->nroguia][$value->producto])) {
				$agrupar[$value->proveedor][$value->nroguia][$value->producto] = [];
			}
			if (!isset($agrupar[$value->proveedor][$value->nroguia][$value->producto][$value->color])) {
				$agrupar[$value->proveedor][$value->nroguia][$value->producto][$value->color] = [];
			}
			if (!isset($agrupar[$value->proveedor][$value->nroguia][$value->producto][$value->color][$value->lote])) {
				$agrupar[$value->proveedor][$value->nroguia][$value->producto][$value->color][$value->lote] = [];
			}
			$agrupar[$value->proveedor][$value->nroguia][$value->producto][$value->color][$value->lote][] = $value;
		}
        $view = \View::make('reportes.tela_tintoreria.pdf', compact("agrupar"))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4', 'landscape');
        return $pdf->stream();
    }
}
