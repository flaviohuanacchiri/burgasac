<?php namespace App\Http\Controllers\Reportes;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ObjectViews\Filtro;
use DB;
use Excel;
use PDF;
use App;
use App\Proveedor;
use App\Producto;
use App\Accesorio;
use App\Insumo;
use App\Empleado;
use App\Maquina;
use App\Planeamiento;
use App\ObjectViews\Reporte;
use Yajra\Datatables\Facades\Datatables;

class ReporteProduccionController extends Controller
{
    public function produccionDescargar(Request $request)
    {

        $producto = $request->producto;
        $maquina = $request->maquina;
        $proveedor = $request->proveedor;
        $empleado = $request->empleado;
        $fecha = $request->fecha;
        $planeamientos_query = DB::table("planeamientos")
              ->join("empleados", "planeamientos.empleado_id", "=", "empleados.id")
              ->join("maquinas", "planeamientos.maquina_id", "=", "maquinas.id")
              ->join("proveedores", "planeamientos.proveedor_id", "=", "proveedores.id")
              ->join("productos", "productos.id", "=", "planeamientos.producto_id");

        if ($fecha) {
            $planeamientos_query->whereRaw("DATE(planeamientos.fecha) = '{$fecha}' ");
        }
        if ($producto) {
            $planeamientos_query->where("producto_id", "=", $producto);
        }
        if ($maquina) {
            $planeamientos_query->where("maquina_id", "=", $maquina);
        }
            if($proveedor) $planeamientos_query->where("planeamientos.proveedor_id","=",$proveedor);
            if($empleado) $planeamientos_query->where("empleado_id","=",$empleado);

            $planeamientos_query->select(
              'planeamientos.fecha as Fecha',
              'proveedores.nombre_comercial as Proveedor',
              DB::raw("CONCAT(empleados.nombres,' ',empleados.apellidos) as Tejedor"),
              'planeamientos.turno as Turno',
              'maquinas.nombre as Maquina',
              'productos.nombre_generico as Producto',
               DB::raw("(SELECT lote_insumo FROM detalle_planeamientos AS dp LEFT JOIN indicador AS ind ON dp.insumo_id = ind.insumo_id WHERE dp.planeamiento_id = planeamientos.id AND dp.lote_insumo <> 0 AND  ind.producto_id = planeamientos.producto_id AND dp.titulo_id = ind.titulo_id ORDER BY ind.valor DESC LIMIT 1) AS Lote"),
              'planeamientos.rollos as Bolsas',
              'planeamientos.kg_producidos as Peso',
              'planeamientos.rollos_falla as BolsasFalla',
              'planeamientos.kg_falla as PesoFalla',
              DB::raw("(planeamientos.rollos - planeamientos.rollos_falla) AS BolsasProducido"),
              DB::raw("(planeamientos.kg_producidos - planeamientos.kg_falla) AS PesoProducido")
            );

            $planeamientos = $planeamientos_query->get();

            $objPlaneamientos = $planeamientos;
            $planeamientos = array();
            foreach ($objPlaneamientos as $objPlaneamiento) {
              $objPlaneamiento->Bolsas = number_format($objPlaneamiento->Bolsas, 2);
              $objPlaneamiento->Peso = number_format($objPlaneamiento->Peso, 2);
              $objPlaneamiento->BolsasFalla = number_format($objPlaneamiento->BolsasFalla, 2);
              $objPlaneamiento->PesoFalla = number_format($objPlaneamiento->PesoFalla, 2);
              $objPlaneamiento->BolsasProducido = number_format($objPlaneamiento->BolsasProducido, 2);
              $objPlaneamiento->PesoProducido = number_format($objPlaneamiento->PesoProducido, 2);
              $planeamientos[] = (array)$objPlaneamiento;
            }


            Excel::create('Reporte de Produccion_'.date("Ymdhis"), function($excel) use($planeamientos) {
                $excel->sheet('Sheetname', function($sheet) use($planeamientos) {
                    $sheet->setAllBorders('thin');
                    $sheet->mergeCells('A1:M1');
                    $sheet->row(1, array(
                     'Reporte de produccion'
                    ));

                    $sheet->cell('A1', function($cell) {
                        $cell->setFont(array(
                            'family'     => 'Calibri',
                            'size'       => '16',
                            'bold'       =>  true
                        ));
                        $cell->setAlignment('center');

                    });

                    $sheet->cell('A3:M3', function($cells) {
                      $cells->setFont(array(
                          'family'     => 'Calibri',
                          'size'       => '12',
                          'bold'       =>  true
                      ));
                    });

                    $sheet->fromArray($planeamientos, null, 'A3', false, true);

                });

            })->export('xls');
    }

    public function produccionResumen(Request $request)
    {
      
        $proveedores = Proveedor::all();
        $productos = Producto::all();
        $accesorios = Accesorio::all();
        $insumos = Insumo::all();
        $empleados = Empleado::all();
        $maquinas = Maquina::all();

        if ($request->ajax()) {
	        $planeamientos = Planeamiento::with('empleado','maquina','detalles.accesorio','detalles.titulo','detalles.insumo','proveedor','producto');
	      return Datatables::eloquent($planeamientos)
	                      ->filter(function ($query) use ($request) {


	                            $q = $request->search["value"];
	                            if ($q) {
	                              $query->whereHas('empleado', function($query) use ($request,$q){
	                                  $query->where('empleados.nombres', 'LIKE', $q);
	                              });

	                              $query->whereHas('proveedor', function($query) use ($request,$q){
	                                  $query->where('proveedores.nombre_comercial', 'LIKE', $q);
	                              });

	                            }

	                            if ($request->has('fecha')){
	                              $query->whereRaw("DATE(updated_at) = '{$request->fecha}' ");
	                            }

	                            if($request->has('producto')){
	                              $query->where("producto_id","=",$request->producto);
	                            }

	                            if($request->has('maquina')){
	                              $query->where("maquina_id","=",$request->maquina);
	                            }

	                            if ($request->has('estado')){
	                              $query->where('estado', '=', $request->estado);
	                            }

	                            if ($request->has('turno')) {
	                                $query->where('turno', '=', $request->turno);
	                            }

	                            if ($request->has('proveedor')) {
	                              $query->where('proveedor_id', '=', $request->proveedor);
	                            }
	                            if ($request->has('empleado')) {
	                              $query->where('empleado_id', '=', $request->empleado);
	                            }
	                      })
	                      ->make(true);

	      }

      return view('reportes.produccion',compact('proveedores','productos','accesorios','insumos','empleados','maquinas'));

    }
}