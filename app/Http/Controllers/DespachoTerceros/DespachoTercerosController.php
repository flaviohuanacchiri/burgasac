<?php

namespace App\Http\Controllers\DespachoTerceros;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DespachoTerceros;
use App\Producto;
use App\Proveedor;
use App\ProveedorTipo;
use App\Color;
use App\Planeamiento;
use App\Movimiento_Tela;
use App\Resumen_Stock_Tela;
use App\DetallesDespachoTerceros;
use App\TransportistaDespachoTercero;
use App\CostoProveedorProducto;
use App\ProveedorDespachoTerceroDeuda;
use App\DespachoTerceroPlaneamiento;
use Carbon\Carbon;
use App\venta;
use DB;
use Session;
use App;

class DespachoTercerosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $despachos = DespachoTerceros::paginate(10);
        foreach ($despachos as $key => $despacho) {
            $despacho->detalles = DetallesDespachoTerceros::where('despacho_id', $despacho->id)
            ->with("color", "producto")->get();
        }
        return view('despacho-terceros.index', compact('despachos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipo_id = \Config::get("sistema.tipo_proveedor_despacho_tercero_id");
        $planeamientos = DB::select(DB::raw("SELECT DISTINCT producto_id,proveedor_id FROM planeamientos WHERE estado = 1"));

      //dd($productos);
        $proveedores = Proveedor::select("proveedores.*")
          ->leftJoin("proveedor_tipo as pt", "pt.proveedor_id", "=", "proveedores.id")
          ->where("pt.tipo_proveedor_id", "=", $tipo_id)
          ->get();


        $tipo_id = \Config::get("sistema.tipo_proveedor_burgasac_id");
        $objproveedortipo = ProveedorTipo::where(["tipo_proveedor_id" => $tipo_id])->first();
        $direccion = "";
        if (!is_null($objproveedortipo)) {
            $objburgasac = Proveedor::find($objproveedortipo->proveedor_id);
            if (is_null($objburgasac)) {
            } else {
                $direccion = $objburgasac->direccion;
            }
        } else {
            $direccion = "";
        }

        return view('despacho-terceros.create', compact('proveedores', 'direccion'));
    }


    public function obtenerProductos($proveedor_id)
    {
        $planeamientos = Planeamiento::where("proveedor_id", $proveedor_id)
                                    ->where("estado", 1)
                                    ->with('producto')
                                    ->groupBy("producto_id")
                                    ->get();
        return $planeamientos;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        if (isset($requestData["proveedor"])) {
            $requestData["proveedor_id"] = $requestData["proveedor"];
            unset($requestData["proveedor"]);
        }
        $nroLote = $requestData["nrolote"];
        $transportista = $requestData["transportista"];
      /*
       * validate($request, $rules, $messages)
       */
        $this->validate(
            $request,
            [
            'fecha'             => 'date',
            'detalles'          => 'required',
            ],
            [
                'detalles.required' => 'Ingrese al menos un detalle a la compra.'
            ]
        );
        $despacho_id = DespachoTerceros::create($requestData)->id;
        $objdespacho = DespachoTerceros::find($despacho_id);

        if (isset($despacho_id)) {
            foreach ($requestData['detalles'] as $detalle) {
                $detalle["proveedor_id"] = $detalle["proveedor"];
                $detalle["despacho_id"] = $despacho_id;
                $detalle["producto_id"] = $detalle["producto"];
                $detalle["cantidad"] = $detalle["kg"];
                $detalle["rollos"] = $detalle["rollos"];
                $detalle["nrolote"] = $nroLote;

                $movimiento = array();
                $movimiento["planeacion_id"] = 0;
                $movimiento["proveedor_id"] = $detalle["proveedor_id"];
                $movimiento["producto_id"] = $detalle["producto_id"];
                $movimiento["rollos"] = - $detalle["rollos"];
                $movimiento["estado"] = 0;
                $movimiento["cantidad"] = - $detalle["kg"];
                $movimiento["descripcion"] = "Despacho a terceros";
                $movimiento["nro_lote"] = $nroLote;

                $totalPesoDespachado = $detalle["kg"];

                $movimiento_mp = Movimiento_Tela::create($movimiento);
                Resumen_Stock_Tela::calculateCurrentStock(
                    $detalle["producto_id"],
                    $detalle["proveedor_id"],
                    -($detalle["kg"]),
                    -($detalle["rollos"]),
                    $nroLote
                );
                $iddetalledespacho  = DetallesDespachoTerceros::create($detalle);
                $objdespacho->consumirPlaneamientos($nroLote, $totalPesoDespachado, $iddetalledespacho->id);

                $proveedorcolorproducto = CostoProveedorProducto::where(["proveedor_id" => $detalle["proveedor_id"],
                  "producto_id" => $detalle["producto_id"]])->whereRaw("deleted_at IS NULL")->orderBy("id", "DESC")->first();
                if (!is_null($proveedorcolorproducto)) {
                    $objdeuda = new ProveedorDespachoTerceroDeuda;
                    $objdeuda->proveedor_id = $detalle["proveedor_id"];
                    $objdeuda->despacho_id = $detalle["despacho_id"];
                    $objdeuda->producto_id = $detalle["producto_id"];
                    $objdeuda->preciounitario = $proveedorcolorproducto->precio;
                    $objdeuda->total = $proveedorcolorproducto->precio*$detalle["kg"];
                    $objdeuda->moneda_id = $proveedorcolorproducto->moneda_id;
                    $objdeuda->detalle_despacho_id = $iddetalledespacho->id;
                    $objdeuda->save();
                }
            }
            if (!is_null($transportista)) {
                $objtransportista = new TransportistaDespachoTercero;
                $objtransportista->despacho_id = $despacho_id;
                $objtransportista->nombre = $transportista["nombre"];
                $objtransportista->direccion = $transportista["direccion"];
                $objtransportista->ruc = $transportista["ruc"];
                $objtransportista->marca = $transportista["marca"];
                $objtransportista->placa = $transportista["placa"];
                $objtransportista->licencia = $transportista["licencia"];
                $objtransportista->save();
            }
        }
        Session::flash('flash_message', 'Datos guardados!');
        return redirect('despacho-terceros/despacho-terceros');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $objdespacho = DespachoTerceros::find($id);
        if (!is_null($objdespacho)) {
            $objplaneamiento = Planeamiento::with("detalles")->find($objdespacho->planeamiento_id);
            $lote = "";
            foreach ($objplaneamiento["detalles"] as $key => $value) {
                if ($value->lote_insumo!="0") {
                    $lote = $value->lote_insumo;
                }
            }
            $detalles = DetallesDespachoTerceros::where("despacho_id", $id)->get();
            foreach ($detalles as $key => $detalle) {
                $movimiento = array();
                $movimiento["planeacion_id"] = 0;
                $movimiento["proveedor_id"] = $detalle["proveedor_id"];
                $movimiento["producto_id"] = $detalle["producto_id"];
                $movimiento["rollos"] = $detalle["rollos"];
                $movimiento["estado"] = 0;
                $movimiento["cantidad"] = $detalle["cantidad"];
                $movimiento["descripcion"] = "Eliminacion de espacho a tintorerias";
                $movimiento_mp = Movimiento_Tela::create($movimiento);

                Resumen_Stock_Tela::calculateCurrentStock($detalle["producto_id"], $detalle["proveedor_id"], ($detalle["cantidad"]), $detalle["rollos"], $lote);
                $despachoplaneamiento = DespachoTerceroPlaneamiento::where("despacho_tercero_id", "=", $id)
                ->where("detalles_despacho_tercero_id", $detalle->id)->first();
                if (!is_null($despachoplaneamiento)) {
                    $despachoplaneamiento->delete();
                }
            }
            $objdespacho->delete();
        }
        Session::flash('flash_message', 'Datos guardados!');
        return redirect('despacho-terceros/despacho-terceros');
    }

    public function getBoleta(Request $request)
    {
        $despacho = DespachoTerceros::with("detalles", "detalles.producto", "proveedor", "transportista")->find($request->id);
        foreach ($despacho as $key => $value) {
            @$despacho[$key]->detalles->planeamientos = [];
        }

        $view =  \View::make('despacho-terceros.print.guiasimple', compact("despacho"))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper([5, 0, 630, 630], 'landscape');//->setPaper('a4', 'potrait');
        return $pdf->stream('invoice');
    }

    public function imprimirGuia(Request $request)
    {
      $id = $request->id;
      $despacho = DespachoTerceros::with("detalles", "detalles.producto", "proveedor", "planeamiento", "planeamiento.detalles", "transportista")->find($id);
        $dia=Carbon::now()->format('d');  
        $mes=Carbon::now()->format('m');  
        $anio=Carbon::now()->format('Y');  
        $mes_n="";
        switch ($mes) {
            case '01': $mes_n="Enero";break;
            case '02': $mes_n="Febrero";break;
            case '03': $mes_n="Marzo";break;
            case '04': $mes_n="Abril";break;
            case '05': $mes_n="Mayo";break;
            case '06': $mes_n="Junio";break;
            case '07': $mes_n="Julio";break;
            case '08': $mes_n="Agosto";break;
            case '09': $mes_n="Setiembre";break;
            case '10': $mes_n="Octubre";break;
            case '11': $mes_n="Noviembre";break;
            case '12': $mes_n="Diciembre";break;
        }
        $lote = "";
        if (!is_null($despacho["planeamiento"])) {
          if (count($despacho["planeamiento"]["detalles"]) > 0) {
            foreach ($despacho["planeamiento"]["detalles"] as $key => $value) {
              if ($value->lote_insumo!="0") {
                $lote = $value->lote_insumo;
              }
            }
          }
        }

        $view =  \View::make('despacho-terceros.print.guiaremision', compact("despacho","dia","mes","anio","mes_n", "lote"))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper([5, 0, 630, 630], 'landscape');//->setPaper('a4', 'potrait');
        return $pdf->stream('invoice');   
    }

    public function imprimirFactura (Request $request)
    {
      $id = $request->id;
      $vfacturas=DespachoTerceros::with("detalles", "detalles.producto", "proveedor")->find($id);

        $dia=Carbon::now()->format('d');  
        $mes=Carbon::now()->format('m');  
        $anio=Carbon::now()->format('Y');  

        $mes_n="";
        switch ($mes) {
            case '01': $mes_n="Enero";break;
            case '02': $mes_n="Febrero";break;
            case '03': $mes_n="Marzo";break;
            case '04': $mes_n="Abril";break;
            case '05': $mes_n="Mayo";break;
            case '06': $mes_n="Junio";break;
            case '07': $mes_n="Julio";break;
            case '08': $mes_n="Agosto";break;
            case '09': $mes_n="Setiembre";break;
            case '10': $mes_n="Octubre";break;
            case '11': $mes_n="Noviembre";break;
            case '12': $mes_n="Diciembre";break;
        }

        $view =  \View::make('despacho-terceros.print.factura', compact("vfacturas","dia","mes","anio","mes_n"))->render();
                    $pdf = \App::make('dompdf.wrapper');
                    $pdf->loadHTML($view)->setPaper('a4', 'potrait');
                    return $pdf->stream('invoice');
    }
}
