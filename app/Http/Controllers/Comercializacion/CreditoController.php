<?php

namespace App\Http\Controllers\Comercializacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\venta;
use App\tipodocv;
use App\forpago;
use App\Cliente;
use App\Pagos_creditos;
use Carbon\Carbon;
use App\Pagos_creditos_eliminar;
use DB;

class CreditoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
    	//$almacen=Almacen::all();
    	$rq=$request;
    	$eva="like";
    	$num="%";
    	$valor_x=str_replace(' ', '', trim($request->saldo_b));

    	if($valor_x!='')
    	{
    		$eva=substr($valor_x,1, 1);
    		
    		if(is_numeric($eva)){
    			$eva=substr($valor_x,0, 1);
    			$num=substr($valor_x,1);
    		}
    		else{
    			$eva=substr($valor_x,0, 2);
    			$num=substr($valor_x,2);	
    		}
    	}

    	
        if(substr($eva,0, 1)!=">" && substr($eva,0, 1)!="<" && substr($eva,0, 1)!="=")
        {
            $eva="like";
            $num="%";
        }
        if(!is_numeric($num))
        {
            $eva="like";
            $num="%";   
        }
        //return substr($eva,0, 1).'|'.$num;

	    
	    	if($request->rdni=='')
	    	{
	    		$vfacturas=venta::orderBy("nVtaCod","desc")
                ->credito($request->ngven_b)
                ->where("saldo",$eva,$num)
                ->where("nForPagCod","3")
                ->where("anulado","0")
                ->paginate(10);    
	    	}
	    	else
	    	{	
	    		$vfacturas=venta::orderBy("nVtaCod","desc")
                ->cliente($request->rdni)
                ->where("saldo",$eva,$num)
                ->where("nForPagCod","3")
                ->where("anulado","0")
                ->paginate(10);    
	    	}
    	

        $tdoc=tipodocv::all();
        $fpago=forpago::all();
        $cli=Cliente::all();
        $fecha=Carbon::now()->format('Y-m-d');

    	return view("comercializacion.credito.index",compact("vfacturas","rq","tdoc","fpago","cli","fecha"));
    }

    public function show($id)
    {
        $fac=venta::where("nVtaCod",$id)->first();       
    	$tabla=Pagos_creditos::where("nVtaCod",$id)->orderBy("cPagCre","desc")->paginate(5);
        return view("comercializacion.credito.show",compact("id","fac","tabla"));
    }

	public function edit($id)
    {  
    	$fac=venta::where("nVtaCod",$id)->first();       
    	$tabla=Pagos_creditos::where("nVtaCod",$id)->orderBy("cPagCre","desc")->paginate(5);
        return view("comercializacion.credito.edit",compact("id","fac","tabla"));
    }  

    public function store(Request $request)
    {
        $this->validate($request, [
            'montop'       => 'required'
        ]);

    	DB::beginTransaction();

        try 
        {   
      

	       	$pc=new Pagos_creditos;
	    	$pc->nVtaCod=$request->codint;
            $pc->dPagCreMonto=$request->montop;
	    	$pc->tPagCreFecReg=$request->fecp;	        	
            $pc->nCodUsu=access()->user()->id;
            $pc->nNomUsu=access()->user()->name;
            $pc->save();

            /*********** inicio actualizar stock alm1 ******************/	                    
            $pago=venta::where('nVtaCod',"=",$request->codint)->first();
            
            $total=0;        
          
            $total=$pago->pago + $request->montop;                                                           
                                                                    
            venta::where('nVtaCod',"=",$request->codint)
            ->update([
                'pago' => $total,
                'saldo' => number_format(($pago->dVFacVTot - $pago->devolucion['dMonFavor'] + $pago->devolucion['dMonContra'])-$total ,2)

            ]);
            /*********** fin actualizar stock ******************/

        } 
        catch (Exception $e) 
        {

            DB::rollback();
            return redirect()->route('credito.edit',$request->codint)->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        $d=Pagos_creditos::orderBy("cPagCre","desc")->first();

        return redirect()->route('credito.edit',$request->codint)->with('info','Se registró correctamente...');    
        //return redirect()->route('credito.edit',$request->codint)->with(['cod'=>$d->nVtaCod]); 
    }

    function reporte($id)
    {
        $datos=Pagos_creditos::where("cPagCre",$id)->first();

        $dia=Carbon::now()->format('d');  
        $mes=Carbon::now()->format('m');  
        $anio=Carbon::now()->format('Y');  

        $mes_n="";
        switch ($mes) {
            case '01': $mes_n="Enero";break;
            case '02': $mes_n="Febrero";break;
            case '03': $mes_n="Marzo";break;
            case '04': $mes_n="Abril";break;
            case '05': $mes_n="Mayo";break;
            case '06': $mes_n="Junio";break;
            case '07': $mes_n="Julio";break;
            case '08': $mes_n="Agosto";break;
            case '09': $mes_n="Setiembre";break;
            case '10': $mes_n="Octubre";break;
            case '11': $mes_n="Noviembre";break;
            case '12': $mes_n="Diciembre";break;
        }

      
        $view =  \View::make('comercializacion.credito.reporte', compact("datos"))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper([10, -25, 260, 180], 'potrait');//margen izq.- arriba, alto,ancho//-
        //$pdf->loadHTML($view)->setPaper('a6', 'potrait');
        return $pdf->stream('invoice');    
    }

    public function destroy($id)
    {
    	$datos=Pagos_creditos::where("cPagCre",$id)->first();

    	DB::beginTransaction();

        try
        {

            /*********** inicio actualizar stock alm1 ******************/	                    
            $pago=venta::where('nVtaCod',"=",$datos->nVtaCod)->first();
            
            $total=0;        
          
            $total=$pago->pago - $datos->dPagCreMonto;                                                           
                                                                    
            venta::where('nVtaCod',"=",$datos->nVtaCod)
            ->update([
                'pago' => $total,
                'saldo' => number_format(($pago->dVFacVTot - $pago->devolucion['dMonFavor'] + $pago->devolucion['dMonContra'])-$total ,2)
            ]);
            /*********** fin actualizar stock ******************/  
            $pcre_datos=Pagos_creditos::where("cPagCre",$id)->first();

            $fecha=Carbon::now()->format('Y-m-d h:i:s');            

            $pce=new Pagos_creditos_eliminar;
            $pce->cPagCre=$id;
            $pce->nVtaCod=$pcre_datos->nVtaCod;
            $pce->dPagCreMonto=$pcre_datos->dPagCreMonto;
            $pce->tPagCreFecReg=$pcre_datos->tPagCreFecReg;             
            $pce->nCodUsu=$pcre_datos->nCodUsu; 
            $pce->nNomUsu=$pcre_datos->nNomUsu;
            $pce->tFecElim=$fecha;
            $pce->save();

            $pcre=Pagos_creditos::where("cPagCre",$id);
            $pcre->delete();
        } 
        catch(Exception $e)
        {
        	DB::rollback();
            return redirect()->route('credito.edit',$datos->nVtaCod)->with('info','Error:No puedo eliminar el registro.');
        }

        DB::commit();

        return redirect()->route('credito.edit',$datos->nVtaCod)->with('info','El crédito fue eliminado.');
    }
 
}
