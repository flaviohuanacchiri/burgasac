<?php

namespace App\Http\Controllers\Comercializacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Almacen;
use App\ProductoAlmacen;
use App\Areas_almacen;
use App\Areas;
use App\Rols_user;
use App\Rols_areas;
use Carbon\Carbon;
use App\CajaApertura;
use App\CajaCierre;
use App\TransferenciaCaja;
use App\CajaVenta;
use App\Users;
use App\TransferCajaEliminados;
use Illuminate\Support\Facades\Auth;

use DB;

class CajaController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('perfiles',['except'=>'tiendaare','except'=>'tiendaare2','except'=>'cajavendedor']);
    }

    public function index(Request $request)
    {
    	$userLogueado = Auth::user();
        $userLogueado =Users::with("rolsareas", "rolsareas.areaAlmacen", "rolsareas.areaAlmacen.almacen")->find($userLogueado->id);
        $rolsAreas = $userLogueado->rolsareas;

        $rolArea = "";
        //print_r($rolsAreas);
        //dd();
        if (isset($rolsAreas[0])) {
            $rolArea = $rolsAreas[0]->nRolAreCod;
            $areaAlmacen = $rolsAreas[0]->areaAlmacen;
            if (!is_null($areaAlmacen)) {
                $almacen = $areaAlmacen->almacen;
                if (!is_null($almacen)) {
                    //$codAlmacen = $almacen->nAlmCod;
                }
            }
        }

        $cajaap=CajaApertura::where("nRolAreCod", $rolArea)
    	->where("fCajFecApe","like","%".$request->fech."%")
    	->orderBy("cCajApeCod","DESC")->paginate(10);
    	$tiendas=Almacen::all();
    	return view("comercializacion.caja.index",compact("cajaap","tiendas"));
    }

    public function tiendaare(Request $request,$id)
    {
    	if($request->ajax()){
    		$areas_x=DB::table("areas_almacen")
    		->leftJoin("areas","areas_almacen.nAreCod","=","areas.nAreCod")
    		->where("areas_almacen.nAlmCod",$id)
    		->select("areas_almacen.nAreAlmCod","areas.cAreNom")
    		->get();

    		return response()->json($areas_x);
    	}
    }

    public function tiendaare2(Request $request,$id)
    {
    	if($request->ajax()){
    		$areas_x=DB::table("areas_almacen")
    		->Join("areas","areas_almacen.nAreCod","=","areas.nAreCod")
    		->Join("rols_areas","areas_almacen.nAreAlmCod","=","rols_areas.nAreAlmCod")
			->Join("caja_apertura","rols_areas.nRolAreCod","=","caja_apertura.nRolAreCod")
    		->where("areas_almacen.nAlmCod",$id)
    		->where("caja_apertura.bCajCieAbierto","0")
    		->select("caja_apertura.cCajApeCod","caja_apertura.bCajCieAbierto","areas.cAreNom","areas_almacen.nAreAlmCod")
    		->distinct("areas_almacen.nAreAlmCod")
    		->get();

    		return response()->json($areas_x);
    	}
    }

    public function cajavendedor(Request $request,$id)
    {
    	if($request->ajax()){
    		$vende_x=DB::table("rols_areas")    		
    		->leftJoin("users","rols_areas.id","=","users.id")
    		->where("rols_areas.nRolAreCod","=",$id)
    		->select("rols_areas.nRolAreCod","users.name")
    		->get();
    		
    		return response()->json($vende_x);
    	}
    }

    public function abrircaja_store(Request $request,$id)
    {

        $this->validate($request, [
            'mon' => 'required'
        ]);
        
        DB::beginTransaction();

        try 
        {
        	$trans_ultimo = CajaApertura::select('cCajApeCod')->orderBy('cCajApeCod',"desc")->get()->first();

        	$conn=0;
	        $dat="";
	        if($trans_ultimo!="")
	        {
	            $dat=$trans_ultimo->cCajApeCod;       
	            $conn=substr($dat, 1);
	        }        
	        //return $conn;
	        $cad="A";
	        $cad2="C";
	        for($i=strlen(++$conn);$i<9;$i++)
	        {
	            $cad.="0";
	            $cad2.="0";
	        }
	       
	        $cad.= $conn;
	        $cad2.= $conn;
	  		
	  		$fecha=Carbon::now()->format('Y-m-d h:i:s');
	  		$name=access()->user()->name;
	  			$id_user=access()->user()->id;
        		$rol_x=Rols_user::where("user_id",$id_user)->get();
        	$rol=$rol_x[0]->rol->name;
        	

            $cajaape=new CajaApertura;
            $cajaape->cCajApeCod=$cad;
            $cajaape->fCajFecApe=$fecha; 
            $cajaape->dCajMonApe=$request->mon;
            $cajaape->nRolAreCod=$id; 

            $cajaape->nameUserLog=$name; 
            $cajaape->nameRolLog=$rol; 
            
            $cajaape->dCajApeMonTotVen=0;
            $cajaape->dCajApeTotTraSal=0; 
            $cajaape->dCajApeTotTraIng=0; 
            
            $cajaape->save();   

            $cajacie=new CajaCierre;
            $cajacie->cCajCieCod=$cad2;
            //$cajacie->fCajFecCie=null; 
            $cajacie->cCajApeCod=$cad;            
            $cajacie->dCajMonCie=0; 
            
            $cajacie->save();       

        } catch (Exception $e) {

            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return redirect()->route('caja.index')->with('info','Se aperturó la caja satisfactoriamente.');  
    }

    public function abrircaja(Request $request)
    {

        $id_user=access()->user()->id;
        $userX=Users::where("id",$id_user)->first();        
        $apertura=CajaApertura::where("nRolAreCod",$userX->rolsareas[0]->nRolAreCod)->where("bCajCieAbierto","0")->first();
        $apertura2=CajaApertura::where("nRolAreCod",$userX->rolsareas[0]->nRolAreCod)->where("fCajFecApe","like","%".$request->fech."%")->paginate(5);

        if($apertura=="")//evluar si ya aperturo una caja, si si redirigirlo a cierre
        {
        	$fecha=Carbon::now()->format('Y-m-d h:i:s');
	        $area=$userX->rolsareas[0]->areasalmacen->areas->cAreNom;
	        $almacen=$userX->rolsareas[0]->areasalmacen->almacen->cAlmNom;
	        $id=$userX->rolsareas[0]->nRolAreCod;	              
	        $nameuser=access()->user()->name;  

	        return view("comercializacion.caja.abricaja",compact("id","fecha","area","almacen","nameuser","apertura2"));
	    }
	    else
	    {
	    	$codcierre=$apertura->cajacierre[0]->cCajCieCod;	    	
	    	
	    	return redirect()->route('caja.cerrarcaja',$codcierre)->with('info','Ya se aperturó la caja, si desea puede cerrarlo.');
	    }
    }

    public function cerrarcaja(Request $request,$id)
    {       

        $nameuser=access()->user()->name;
        $id_user=access()->user()->id;
        $userX=Users::where("id",$id_user)->first();        
        $cajacierre=CajaCierre::where("cCajCieCod",$id)->first();


        if($cajacierre->cajaapertura->rolarea->id != access()->user()->id)
            return redirect()->route('caja.index')->with('info','Usted no puede CERRAR la caja de otro usuario.');

        $cajacierre2=CajaCierre::where("fCajFecCie","like","%".$request->fech."%")->paginate(5);

        /*if($apertura!="")//evluar si ya aperturo una caja, si si redirigirlo a cierre
        {*/
        	$fecha=Carbon::now()->format('Y-m-d h:i:s');
	        $area=$userX->rolsareas[0]->areasalmacen->areas->cAreNom;
	        $almacen=$userX->rolsareas[0]->areasalmacen->almacen->cAlmNom;
	 
	        $montodef=$cajacierre->cajaapertura->dCajApeMonTotVen+$cajacierre->cajaapertura->dCajMonApe;

	        return view("comercializacion.caja.cerracaja",compact("id","fecha","cajacierre","nameuser","area","almacen","montodef","cajacierre2"));
	    /*}
	    else
	    {
	    	return view("comercializacion.caja.abricaja");	
	    }*/
    }

    public function cerrarcaja_store(Request $request,$id)
    {
        $aux=CajaCierre::where('cCajCieCod',"=",$id)->first();

        if($aux->cajaapertura->rolarea->id != access()->user()->id)
            return redirect()->route('caja.index')->with('info','Usted no puede CERRAR la caja de otro usuario.');

        $this->validate($request, [
            'moncie' => 'required'
        ]);
        
        DB::beginTransaction();

        try 
        {	
	  		$fecha=Carbon::now()->format('Y-m-d h:i:s');
	  	
            CajaCierre::where('cCajCieCod',"=",$id)
	        ->update([
	            'fCajFecCie' => $fecha,
	            'dCajMonCie' => $request->moncie
	            ]);  

	        CajaApertura::where('cCajApeCod',"=",$request->codapertura)
	        ->update([
	            'bCajCieAbierto' => "1"
	            ]);    

        } catch (Exception $e) {


            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return redirect()->route('caja.index')->with('info','Se cerró la caja satisfactoriamente.');  
    }

    public function transferencia(Request $request)
    {
		$id_user=access()->user()->id;
        $userX=Users::where("id",$id_user)->first();        
        $apertura=CajaApertura::where("nRolAreCod",$userX->rolsareas[0]->nRolAreCod)->where("bCajCieAbierto","0")->first();

        if($apertura!="")//evluar si ya aperturo una caja, si si redirigirlo a cierre
        {
        	$fecha=Carbon::now()->format('Y-m-d h:i:s');
	        $area=$userX->rolsareas[0]->areasalmacen->areas->cAreNom;
	        $almacen=$userX->rolsareas[0]->areasalmacen->almacen->cAlmNom;
	        $cod_almacen=$userX->rolsareas[0]->areasalmacen->nAreAlmCod;
	     
	        $id=$apertura->cCajApeCod;

	        $tiendas=Almacen::all();

	        $nameuser=access()->user()->name;  

	        $montodef=$apertura->dCajApeMonTotVen - $apertura->dCajApeTotTraSal + $apertura->dCajApeTotTraIng - $apertura->dCajApeTotEgreso;// + $apertura->dCajMonApe;

	        $lista_trans=TransferenciaCaja::where("tTraCajFec","like","%".$request->fech."%")->paginate(5);

	        return view("comercializacion.caja.transferencia",compact("id","fecha","area","almacen","tiendas","cod_almacen","montodef","lista_trans","nameuser"));
	    }
	    else
	    {
	    	//$codcierre=$apertura->cajacierre[0]->cCajCieCod;	    		    	
	    	return redirect()->route('caja.abrircaja')->with('info','Para tranasferir primero debe aperturar caja.');
	    }
    }

    public function transferencia_store(Request $request,$id)
    {

        $this->validate($request, [
            'tien' => 'required',
            'caja' => 'required',
            'mon' => 'required'
        ]);
        
        DB::beginTransaction();

        try 
        {
        	$trans_ultimo = TransferenciaCaja::select('cTraCaj')->orderBy('cTraCaj',"desc")->get()->first();

        	$conn=0;
	        $dat="";
	        if($trans_ultimo!="")
	        {
	            $dat=$trans_ultimo->cTraCaj;       
	            $conn=substr($dat, 2);
	        }        
	        
	        $cad="TC";	        
	        for($i=strlen(++$conn);$i<8;$i++)
	            $cad.="0";
	       
	        $cad.= $conn;
	        
	  		
	  		$fecha=Carbon::now()->format('Y-m-d h:i:s');
	  		$name=access()->user()->name;
	  			$id_user=access()->user()->id;
        		$rol_x=Rols_user::where("user_id",$id_user)->get();
        	$rol=$rol_x[0]->rol->name;
        	

            $trans=new TransferenciaCaja;
            $trans->cTraCaj=$cad;
            $trans->cCajApeCod1=$id; 
            $trans->cCajApeCod2=$request->caja;
            $trans->tTraCajFec=$fecha;
            $trans->dTraCajMon=$request->mon; 

            $trans->nameUserLog=$name;             
            $trans->nameRolLog=$rol;            
            $trans->save();   

            CajaApertura::where('cCajApeCod',"=",$id)
	        ->update([
	            'dCajApeTotTraSal' => $request->mon
	            ]);     

	        CajaApertura::where('cCajApeCod',"=",$request->caja)
	        ->update([
	            'dCajApeTotTraIng' => $request->mon
	            ]);     

        } catch (Exception $e) {

            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return redirect()->back()->with('info','Se realizó la transferencia satisfactoriamente.');  
    }

    public function transferencia_destroy($id)
    {  

        DB::beginTransaction();

        try 
        {   
        	$trans=TransferenciaCaja::where('cTraCaj','=',$id)->first();   

        	$ca1=CajaApertura::where('cCajApeCod',"=",$trans->cCajApeCod1)->first();
        	$canto_Aux=$ca1->dCajApeTotTraSal-$trans->dTraCajMon;
        	CajaApertura::where('cCajApeCod',"=",$trans->cCajApeCod1)
            ->update([
                'dCajApeTotTraSal' => $canto_Aux
                ]);     

            $ca2=CajaApertura::where('cCajApeCod',"=",$trans->cCajApeCod2)->first();
        	$canto_Aux2=$ca2->dCajApeTotTraIng-$trans->dTraCajMon;
        	CajaApertura::where('cCajApeCod',"=",$trans->cCajApeCod2)
            ->update([
                'dCajApeTotTraIng' => $canto_Aux2
                ]);  

            //Guardar log ----------------------------------------
            $datos_tc=TransferenciaCaja::where('cTraCaj','=',$id)->first();

            $log=new TransferCajaEliminados;            
            $log->cTraCaj=$datos_tc->cTraCaj;
            $log->cCajApeCod1=$datos_tc->cCajApeCod1;
            $log->cCajApeCod2=$datos_tc->cCajApeCod2;
            $log->tTraCajFec=$datos_tc->tTraCajFec;
            $log->dTraCajMon=$datos_tc->dTraCajMon;
            $log->nameUserLog=$datos_tc->nameUserLog;
            $log->nameRolLog=$datos_tc->nameRolLog;

            $fecha=Carbon::now()->format('Y-m-d h:i:s');
            
            $log->tFechElim=$fecha;
                    
            $log->save(); 

            $ar=TransferenciaCaja::where('cTraCaj','=',$id);
            $ar->delete();
         } catch (Exception $e) {

            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();
                
        return back()->with('info','La transferencia fue eliminada.');
    }

    function reportecajacierre($id)
    {

        $datos=CajaApertura::where("cCajApeCod",$id)->first();
        $efec_guia=$this->totales($id,1,1);
        $dep_guia=$this->totales($id,2,1);
        $che_guia=$this->totales($id,4,1);
        $cre_guia=$this->totales($id,3,1);

        $efec_bol=$this->totales($id,1,2);
        $dep_bol=$this->totales($id,2,2);
        $che_bol=$this->totales($id,4,2);
        $cre_bol=$this->totales($id,3,2);

        $efec_fac=$this->totales($id,1,3);
        $dep_fac=$this->totales($id,2,3);
        $che_fac=$this->totales($id,4,3);
        $cre_fac=$this->totales($id,3,3);

        $efec_guia_eg=$this->totales_tt($id,1,1);
        $efec_bol_eg=$this->totales_tt($id,1,2);
        $efec_fac_eg=$this->totales_tt($id,1,3);        

        $view =  \View::make('comercializacion.caja.rptcierrecaja',compact("datos","efec_guia","dep_guia","che_guia","cre_guia","efec_bol","dep_bol","che_bol","cre_bol","efec_fac","dep_fac","che_fac","cre_fac","efec_guia_eg","efec_bol_eg","efec_fac_eg"))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4', 'potrait');
        return $pdf->stream('invoice');
    }

    public function totales($id,$forpag,$doc)
    {
        $datitos=DB::table("caja_venta")        
        ->leftJoin("ventas","caja_venta.nVtaCod","=","ventas.nVtaCod")        
        ->where("caja_venta.cCajApeCod",$id)
        ->where("ventas.nForPagCod",$forpag)
        ->where("ventas.nTipPagCod",$doc)
        ->select(DB::raw('SUM(ventas.dVFacVTot) as total'))
        ->first();

        return ($datitos->total=="")?0:$datitos->total;
    }

    public function totales_tt($id,$forpag,$doc)
    {
        $datitos=DB::table("compra_telat")        
        ->Join("pago_compra","compra_telat.nCodComTel","=","pago_compra.nCodComTel")        
        ->Join("detalle_pago_compra","pago_compra.nCodPagComp","=","detalle_pago_compra.nCodPagComp")       
        ->where("detalle_pago_compra.cCaja",$id)
        //->where("compra_telat.nCodForP",$forpag)
        ->where("compra_telat.nCodTdoc",$doc)
        ->where("detalle_pago_compra.bCaja","1")
        ->select(DB::raw('SUM(detalle_pago_compra.dMontoPag) as total'))
        ->first();

        return ($datitos->total=="")?0:$datitos->total;
    }


    function cajaoperaeditar(Request $request,$id)
    {
        $cap=CajaApertura::where("cCajApeCod",$id)->first();
        
        if($cap->rolarea->id == access()->user()->id)
            return view("comercializacion.caja.cajaoperaeditar",compact("cap","id"));
        else
            return redirect()->route('caja.index')->with('info','Usted no puede EDITAR la caja de otro usuario.');
     
    }

    function cajaoperaeditar_put(Request $request,$id)
    {
        $this->validate($request, [
            'monape' => 'required',
            'moncie' => 'required'
        ]);

        $aux=CajaApertura::where('cCajApeCod',"=",$id)->first();

        if($aux->rolarea->id != access()->user()->id)
            return redirect()->route('caja.index')->with('info','Usted no puede EDITAR la caja de otro usuario.');
        
        DB::beginTransaction();

        try 
        {
            CajaApertura::where('cCajApeCod',"=",$id)
            ->update([
                'dCajMonApe' => $request->monape
                ]);  

            CajaCierre::where('cCajCieCod',"=",$aux->cajacierre[0]->cCajCieCod)
            ->update([
                'dCajMonCie' => $request->moncie
                ]);  
        } catch (Exception $e) {

            DB::rollback();
            return redirect()->route('caja.index')->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();
        
        return redirect()->route('caja.index')->with('info','Se editó la operación correctamente...');
    }

    function operacion_destroy($id)
    {  
        $aux=CajaApertura::where('cCajApeCod',"=",$id)->first();

        if($aux->rolarea->id != access()->user()->id)
            return redirect()->route('caja.index')->with('info','Usted no puede ELIMINAR la caja de otro usuario.');

        DB::beginTransaction();

        try 
        {   
            $trans=CajaVenta::where('cCajApeCod','=',$id)->first();
            
            if($trans=="")
            {
                $ar=CajaCierre::where('cCajApeCod','=',$id);
                $ar->delete();

                $ar2=CajaApertura::where('cCajApeCod','=',$id);
                $ar2->delete();
            }
            else
                return redirect()->back()->with('info','No se puede eliminar porque existe registros de venta!...');       
            
         } catch (Exception $e) {

            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();
                
        return back()->with('info','La operación fue eliminada...');
    }

}
