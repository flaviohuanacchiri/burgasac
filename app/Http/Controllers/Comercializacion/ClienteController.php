<?php

namespace App\Http\Controllers\Comercializacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Cliente; /*Se agregra el nombre del modelo*/
use App\Tipodoc; /*Se agregra el nombre del modelo*/
use Carbon\Carbon;
use App\Http\Requests\ClienteRequest;

class ClienteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $tipodoc=Tipodoc::select('nTdocCod','cTdocSigla')->get();
        return view('comercializacion.cliente.create',compact("tipodoc"));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'tipodoc_id'       => 'required',
            'cClieNdoc'       => 'numeric|required|digits_between:8,15',
            'cClieDesc'       => 'required',
            'cClieDirec'       => 'required'

        ]);
        //dd($request);
        try{
            $cliente = new Cliente;
            $cliente->nTdocCod = $request->tipodoc_id;
            $cliente->cClieNdoc = $request->cClieNdoc;
            $cliente->cClieDesc = $request->cClieDesc;
            $cliente->cClieDirec = $request->cClieDirec;
            $cliente->cClieObs = $request->cClieObs;

            $cliente->save();

        } 
        catch(\Exception $e){
           return redirect()->route('cliente.create')->with('info','Vuelva a intentarlo.');  
        }
        
        return redirect()->route('cliente.index')->with('El cliente fue guardado');        
    }

   public function edit($id)
    {
        $cliente = Cliente::where('nClieCod','=',$id)->get()->first();
        $tipodoc=Tipodoc::select('nTdocCod','cTdocSigla')->get();
        return view('comercializacion.cliente.edit', compact('cliente','tipodoc')); 
    }

   public function update(Request $request,$id)
    {
        $this->validate($request, [
            'tipodoc_id'       => 'required',
            'cClieNdoc'       => 'numeric|required|digits_between:8,15',
            'cClieDesc'       => 'required',
            'cClieDirec'       => 'required'
        ]);
        //dd($request);
        try{
            $fecha_act=Carbon::now()->format('Y-m-d h:i:s');
            Cliente::where('nClieCod',"=",$id)
            ->update([
                'nTdocCod' => $request->tipodoc_id,
                'cClieNdoc' => $request->cClieNdoc,
                'cClieDesc' => $request->cClieDesc,
                'cClieDirec' => $request->cClieDirec,
                'cClieObs' => $request->cClieObs,
                'dFecAct' => $fecha_act
                ]); 
        } 
        catch(\Exception $e){
           return redirect()->route('cliente.edit',$id)->with('info','El cliente no fue actualizado, vuelva a intentarlo.');              
        }
        
        return redirect()->route('cliente.show',$id)->with('info','El cliente fue actualizado.');              
    }

    public function index()
    {
        $cliente = Cliente::where("anulado","0")->orderby('nClieCod', 'ASC')->paginate(5);
        return view('comercializacion.cliente.index', compact('cliente'));
    }

   public function show($id)
    {
        $cliente = Cliente::where('nClieCod','=',$id)->where("anulado","0")->get()->first();
        $tipodoc=Tipodoc::select('nTdocCod','cTdocSigla')->get();
    	return view('comercializacion.cliente.show', compact('cliente','tipodoc'));        
    }

     public function destroy($id)
    {
        try{
            $fecha_elim=Carbon::now()->format('Y-m-d h:i:s');
            Cliente::where('nClieCod',"=",$id)
            ->update([
                'dFecElim' => $fecha_elim,
                'anulado' => '1'
                ]); 

            /*$cliente=cliente::where('nClieCod','=',$id);
            $cliente->delete();*/
        } 
        catch(\Exception $e){
           return redirect()->route('cliente.index',$id)->with('info','Error:No puedo eliminar el registro porque está relacionado con punto de venta');              
        }

        return back()->with('info','El cliente fue eliminado.');
    }
}
