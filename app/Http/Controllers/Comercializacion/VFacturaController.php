<?php

namespace App\Http\Controllers\Comercializacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ventadetalle;
use App\venta;
use App\Cliente;
use App\Almacen;
use App\ProductoAlmacen;
use Carbon\Carbon;
use App\Rols_user;
use App\Rols_areas;
use App\CajaApertura;
use App\CajaCierre;
use App\CajaVenta;
use App\tipodocv;
use App\forpago;
use App\Configuracion;
use App\Banco;
use App\Devoluciones;
use App\Users;
use App\Res_Stock_Telas;
use App\Movimiento_Tela;
use App\Res_stock_mp;
use App\Movimiento_MP;
use Illuminate\Support\Facades\Auth;

use DB;

class VFacturaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function create()
    {
        $id_user=access()->user()->id;
        $userX=Users::where("id",$id_user)->first();   
        $apertura=CajaApertura::where("nRolAreCod",$userX->rolsareas[0]->nRolAreCod)->where("bCajCieAbierto","0")->first();

        if($apertura!="")//evluar si ya aperturo una caja, si si redirigirlo a cierre
        {
            //$provedor=provedor::all();
            $codalm=$userX->rolsareas[0]->areasalmacen->almacen->nAlmCod;
            $almacen=Almacen::where("nAlmCod",$codalm)->orderby("nAlmCod","asc")->first();//sólo para cerrar el sistema a un almacén]
            $manauto=$userX->rolsareas[0]->areasalmacen->areas->bForPag;

            $almacen1=Almacen::orderby("nAlmCod","asc")->first();

            if($almacen1=="")
                return redirect()->route('venta.index')->with('info','No se puede mostrar el Punto de Venta porque no han registrado los almacenes.');
            
            //$productos=ProductoAlmacen::where("nAlmCod",$almacen1->nAlmCod)->get();
            $productoAlmacen=DB::table("productoalmacens")
                ->Join("productos","productoalmacens.cProdCod","=","productos.id")            
                ->Join("color","productoalmacens.id_color","=","color.id")          
                ->where('productoalmacens.nAlmCod','=',$almacen1->nAlmCod)            
                ->select("color.nombre","productos.nombre_generico","productos.nombre_especifico","productoalmacens.nProAlmCod")
                ->distinct()
                ->get();
            
            $id="";

            $fecha=Carbon::now()->format('Y-m-d');
            $hora=Carbon::now()->format('h:i:s');

            /************* numeración factura *************/

            $num_fac=$almacen->nAlmNumAct;

            $cad2="";
            for($i=strlen(++$num_fac);$i<6;$i++)
            {
                $cad2.="0";
            }
            $cad2.=$num_fac;

            $conf=Configuracion::orderby("cConfCod","desc")->first();
            $clientes=Cliente::all();
            $banco_x=Banco::all();

            return view("comercializacion.venta.create",compact("fecha","hora","id","almacen","productoAlmacen","cad2","conf","clientes","banco_x","manauto"));
        }
        else
        {
            return redirect()->route('caja.abrircaja')->with('info','Debe aperturar la caja primero.');             
        }    
    }

    public function buscli(Request $request,$id)
    {
        if($request->ajax()){
            $cliente_x=Cliente::where('nClieCod',$id)->first();
            return response()->json($cliente_x);
        }
    }

    public function recusuarios(Request $request)
    {
        if($request->ajax()){
            $cliente_x=cliente::all();
            return response()->json($cliente_x);
        }
    }

    public function bbancos(Request $request)
    {
        if($request->ajax()){
            $banco_x=Banco::all();
            return response()->json($banco_x);
        }
    }

    public function prostock(Request $request,$id,$cli)
    {
        if($request->ajax()){
            
            $registro_x=DB::table("ventas")
            ->Join("detalleventa","detalleventa.nVtaCod","=","ventas.nVtaCod")                        
            ->where('ventas.nClieCod','=',$cli)            
            ->where('detalleventa.nProAlmCod','=',$id)            
            ->select("detalleventa.nVtaPrecioU")            
            ->first();
            //return $registro_x;
            if($registro_x=="")
            {
                $prod_x=ProductoAlmacen::select("nProdAlmStock","nProdAlmPuni")->where('nProAlmCod',$id)->first();    
            }
            else
            {
                $prod_x=DB::table("ventas")
                ->Join("detalleventa","detalleventa.nVtaCod","=","ventas.nVtaCod")                        
                ->Join("productoalmacens","productoalmacens.nProAlmCod","=","detalleventa.nProAlmCod")                        
                ->where('ventas.nClieCod','=',$cli)            
                ->where('detalleventa.nProAlmCod','=',$id)            
                ->select("detalleventa.nVtaPrecioU as nProdAlmPuni","productoalmacens.nProdAlmStock")
                ->orderby("nDetVtaCod","desc")            
                ->first();
            }

            return response()->json($prod_x);
        }
    }

    public function productoalm(Request $request,$id)
    {
        if($request->ajax()){
            $prod_x=DB::table("productoalmacens")
            ->Join("productos","productoalmacens.cProdCod","=","productos.id")            
            ->Join("color","productoalmacens.id_color","=","color.id")          
            ->where('productoalmacens.nAlmCod','=',$id)            
            ->select("color.nombre","productos.nombre_generico","productos.nombre_especifico","productoalmacens.nProAlmCod")
            ->distinct()
            ->get();

            return response()->json($prod_x);           
        }
    }

    public function combo_lotes(Request $request,$id)
    {
        if($request->ajax()){
            $prod_x=DB::table("resumen_stock_telas")  
            ->Join("productos","resumen_stock_telas.producto_id","=","productos.id")                  
            ->where('producto_id','=',$id) 
            ->where('cantidad','>','0')           
            ->select("resumen_stock_telas.id","resumen_stock_telas.cantidad","resumen_stock_telas.rollos","resumen_stock_telas.nro_lote")//,"productos.dPreUniPro")
            //->distinct()
            ->get();

            return response()->json($prod_x);           
        }
    }

    public function promedida(Request $request,$id)
    {
        if($request->ajax()){
            $prodmedida_x=DB::table('productos')
            ->leftJoin("medidaproductos","productos.cProdCod","=","medidaproductos.cProdCod")
            ->leftJoin("medidas","medidaproductos.nMedCod","=","medidas.nMedCod")
            ->select("medidas.nMedCod","medidaproductos.nMedProCod","medidas.cMedNom","medidaproductos.nMedProCanUni","medidaproductos.dMedProPor","medidas.cMedUnis")
            ->where('medidaproductos.cProdCod',$id)
            ->where('medidaproductos.bMedProIntExt',"1")
            ->where('medidaproductos.activo',"1")
            ->get();
            return response()->json($prodmedida_x);         
        }
    }

    public function ofermedida(Request $request,$id)
    {
        if($request->ajax()){
            $ofermedida_x=DB::table('medidaproductos')
            ->leftJoin("productos","productos.cProdCod","=","medidaproductos.cProdCod")
            //->leftJoin("medidas","medidaproductos.nMedCod","=","medidas.nMedCod")
            ->select("medidaproductos.dMedProPor","productos.dProdPpubl")
            ->where('medidaproductos.nMedProCod',$id)
            ->first();

            return response()->json($ofermedida_x);         
        }
    }

    public function veri_codbarra($id,$alm1)
    {
        $datos=DB::table('detalle_nota_ingreso')
        ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso.nProAlmCod")
        ->leftJoin("productos","productos.id","=","productoalmacens.cProdCod")
        ->leftJoin("color","color.id","=","productoalmacens.id_color")
        ->leftJoin("almacens","almacens.nAlmCod","=","productoalmacens.nAlmCod")       
        ->where(DB::raw("REPLACE(detalle_nota_ingreso.cod_barras,'-','')"),"=",$id)
        ->orWhere("detalle_nota_ingreso.cod_barras","=",$id)
        ->where("productoalmacens.nAlmCod","=",$alm1)   
        ->select("productoalmacens.nProAlmCod","productos.nombre_generico","productos.nombre_especifico","color.nombre","productoalmacens.nProdAlmStock","detalle_nota_ingreso.peso_cant","almacens.nAlmCod","almacens.cAlmNom","almacens.cAlmUbi","detalle_nota_ingreso.rollo","productoalmacens.nAlmCod","productoalmacens.nProdAlmPuni")
        ->limit(1)->get();        
        $dat=json_decode($datos, true);
        if(empty($dat))
        {           
            //$datos=detalle_nota_ingreso_a::where("cod_barras","=",$id)->get();
            $datos=DB::table('detalle_nota_ingreso_a')
            ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso_a.nProAlmCod")
            ->leftJoin("productos","productos.id","=","productoalmacens.cProdCod")
            ->leftJoin("color","color.id","=","productoalmacens.id_color")
            ->leftJoin("almacens","almacens.nAlmCod","=","productoalmacens.nAlmCod")
            ->where(DB::raw("REPLACE(detalle_nota_ingreso_a.cod_barras,'-','')"),"=",$id)
            ->orWhere("detalle_nota_ingreso_a.cod_barras","=",$id)
            ->where("productoalmacens.nAlmCod","=",$alm1)   
            ->select("productoalmacens.nProAlmCod","productos.nombre_generico","productos.nombre_especifico","color.nombre","productoalmacens.nProdAlmStock","detalle_nota_ingreso_a.peso_cant","almacens.nAlmCod","almacens.cAlmNom","almacens.cAlmUbi","detalle_nota_ingreso_a.rollo","productoalmacens.nAlmCod","productoalmacens.nProdAlmPuni")
            ->limit(1)->get(); 
        }

        return $datos;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'dniruc_fijo'       => 'required',
            //'alm2'       => 'required'
        ]);

        DB::beginTransaction();

        try 
        {   
            $trans_ultimo = venta::select('nVtaCod')
                ->orderBy('nVtaCod',"desc")                      
                ->get()->first();

            $conn=0;
            $dat="";
            if($trans_ultimo!="")
            {
                $dat=$trans_ultimo->nVtaCod;       
                $conn=substr($dat, 3);
            }        
            //return $conn;
            $cad="GV-";
            for($i=strlen(++$conn);$i<10;$i++)
            {
                $cad.="0";
            }
           
            $cad.= $conn;
            //$id=$cad;

            $id_user=access()->user()->id;
            $role_user=Rols_user::where("user_id",$id_user)->first();   
       
            $trans=new venta;
            $trans->nVtaCod=$cad;
            $trans->nClieCod=$request->cod_cliente;
            $trans->nTipPagCod=$request->tipdoc;
            $trans->nForPagCod=$request->pago;
            $trans->nAlmCod=$request->almprodes;            
            $trans->dDniContacto=$request->dniruc_fijo;
            $trans->cContacto=$request->nomcli;
            $trans->cBanco=$request->banco;
            $trans->cNCheque=$request->ncheq;
            $trans->cNoperacion=$request->nope;
            $trans->nTotal_sol=substr($request->soles_x,3);
            $trans->nTotal_dol=substr($request->dolar_x,3);
            $trans->nTipCam=$request->cambio;
            $trans->dVFacFemi=$request->fecp.' '.$request->horp;
            $trans->bSV=$request->key_stock;
            $trans->dVFacSTot=$request->impo;
            $trans->dVFacIgv=$request->igv;
            $trans->dVFacVTot=$request->total;

            $trans->nCodUsu=access()->user()->id;
            $trans->nNomUsu=access()->user()->name;
            $trans->nCarUsu=$role_user->rol->name;
            
            if($request->tipdoc=="3")
            {
                Almacen::where('nAlmCod',"=",$request->almprodes)
                ->update([                
                    'nAlmNumAct' => $request->dfactu2
                    ]);

                $trans->cFacNumFac=$request->dfactu1.'-'.$request->dfactu2;
            }
            else
                $trans->cFacNumFac="";

            if($request->pago=="3")
            {
                $trans->pago="0";
                $trans->saldo=$request->total;
            }
            else
            {
                $trans->pago=$request->total;
                $trans->saldo="0";
            }

            $trans->cNumGuia=$request->nguia_zz;  
            $fecha_act=Carbon::now()->format('Y-m-d H:i:s');
            $trans->dFecAct=$fecha_act;
            
            $trans->save();

            for($i=1;$i<=$request->conta;$i++)
            {
                if($request["cod_ndi_".$i]!="")
                {
                    if($request["cod_ndi_".$i]=="0")//si es diferente no se ingresa
                    {

                        $tradetalle=new ventadetalle;        
                        if(substr($request["proalm_".$i],0,2)=="TC")                                 
                            $tradetalle->nCodTC=substr($request["proalm_".$i],2);
                        else if(substr($request["proalm_".$i],0,2)=="MP")                                 
                            $tradetalle->nCodMP=substr($request["proalm_".$i],2);
                        else
                            $tradetalle->nProAlmCod=$request["proalm_".$i];

                        $tradetalle->nVtaCod=$cad;
                        $tradetalle->nDvtaCB=$request["codb_".$i];
                        $tradetalle->nVtaPeso=$request["canti_".$i];                        
                        $tradetalle->nVtaCant=$request["rollo_".$i];
                        $tradetalle->nVtaPrecioU=$request["puni_".$i];
                        $tradetalle->nVtaPreST=$request["stotal_".$i];
                                            
                        $tradetalle->save();

                        if($request->key_stock=="0")
                        {
                            if(substr($request["proalm_".$i],0,2)=="TC")
                            {
                                /*********** inicio actualizar stock alm1 ******************/            

                                $stock=Res_Stock_Telas::where('id',"=",substr($request["proalm_".$i],2))->first();

                                $save_mov=new Movimiento_Tela;
                                $save_mov->planeacion_id=1;
                                $save_mov->producto_id=$stock->producto_id;
                                $save_mov->proveedor_id=$stock->proveedor_id;
                                $save_mov->nro_lote=$stock->nro_lote;
                                $save_mov->rollos=$request["rollo_".$i];
                                $save_mov->cantidad=$request["canti_".$i];
                                $save_mov->descripcion="Venta";
                                $save_mov->estado=0;

                                $save_mov->save();


                                Res_Stock_Telas::where('id',"=",substr($request["proalm_".$i],2))
                                ->update([
                                    'cantidad' => ($stock->cantidad - $request["canti_".$i]),
                                    'rollos' => ($stock->rollos - $request["rollo_".$i])
                                ]);
                                /*********** fin actualizar stock ******************/
                            }
                            else if(substr($request["proalm_".$i],0,2)=="MP")
                            {
                                /*********** inicio actualizar stock alm1 ******************/            

                                $stock=Res_stock_mp::where('id',"=",substr($request["proalm_".$i],2))->first();
                                
                                $fecha_aux=Carbon::now()->format('Y-m-d');

                                $save_movMP=new Movimiento_MP;
                                $save_movMP->fecha=$fecha_aux;
                                //$save_movMP->compra_id=1;                                
                                $save_movMP->proveedor_id=$stock->proveedor_id;
                                $save_movMP->lote=$stock->lote;
                                $save_movMP->titulo_id=$stock->titulo_id;                                
                                $save_movMP->cantidad=$request["canti_".$i];
                                $save_movMP->estado=1;
                                $save_movMP->insumo_id=$stock->insumo_id; 
                                $save_movMP->accesorio_id=$stock->accesorio_id; 
                                $save_movMP->descripcion="Venta";
                                $save_movMP->peso_neto=$stock->peso_neto; 

                                $save_movMP->save();


                                Res_stock_mp::where('id',"=",substr($request["proalm_".$i],2))
                                ->update([
                                    'peso_neto' => ($stock->peso_neto - $request["canti_".$i]),
                                    'cantidad' => ($stock->cantidad - $request["rollo_".$i])
                                ]);
                                /*********** fin actualizar stock ******************/
                            }
                            else
                            {
                                /*********** inicio actualizar stock alm1 ******************/            

                                $stock=ProductoAlmacen::where('nProAlmCod',"=",$request["proalm_".$i])->first();
                                
                                $total=0;        
                                if($stock->producto->nombre_especifico=="TELAS")                            
                                    $total=$stock->nProdAlmStock - $request["canti_".$i];                                                           
                                else
                                    $total=$stock->nProdAlmStock - $request["rollo_".$i];                                                           
                                                                                        
                                ProductoAlmacen::where('nProAlmCod',"=",$request["proalm_".$i])->update(['nProdAlmStock' => $total]);
                                /*********** fin actualizar stock ******************/
                            }

                        }
                    }
                    else
                    {
                       
                    }
                }//dif empty
            } //end for

            //********************* registrar en caja_venta e incrementar el montototalventa en caja apertura *********************/

            $id_user=access()->user()->id;
            $userX=Users::where("id",$id_user)->first();   
            $apertura=CajaApertura::where("nRolAreCod",$userX->rolsareas[0]->nRolAreCod)->where("bCajCieAbierto","0")->first();

            $cv=new CajaVenta;
            $cv->cCajApeCod=$apertura->cCajApeCod;
            $cv->nVtaCod=$cad;

            $cv->save();

            
            $total_aux=$apertura->dCajApeMonTotVen;
            $total_aux+=$request->total;

            CajaApertura::where('cCajApeCod',"=",$apertura->cCajApeCod)
            ->update([                
                'dCajApeMonTotVen' => $total_aux
                ]);

        } 
        catch (Exception $e) 
        {

            DB::rollback();
            return redirect()->route('venta.create')->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        //if($request->pago==3){
            //return redirect()->route('venta.create')->with('info','Se registró correctamente la venta al Crédito.'); 
        //}
        //else  
        //{
            if($request->tipdoc==1)
                return redirect()->route('venta.create')->with(['cod'=>$cad]);  
            else                                
                return redirect()->route('venta.create')->with(['cod'=>$cad,'fp'=>$request->pago]);
        //}          
            

        //return redirect()->route('venta.reporte',$cad)->with('info','La venta se registró correctamente.'); 
    }

    public function index(Request $request)
    {
        //$almacen=Almacen::all();
        $rq=$request;
        $fecha=Carbon::now()->format('Y-m-d');
        $userLogueado = Auth::user();
        $userLogueado =Users::with("rolsareas", "rolsareas.areaAlmacen", "rolsareas.areaAlmacen.almacen")->find($userLogueado->id);
        $rolsAreas = $userLogueado->rolsareas;

        $codAlmacen = "";
        if (isset($rolsAreas[0])) {
            $areaAlmacen = $rolsAreas[0]->areaAlmacen;
            if (!is_null($areaAlmacen)) {
                $almacen = $areaAlmacen->almacen;
                if (!is_null($almacen)) {
                    $codAlmacen = $almacen->nAlmCod;
                }
            }
        }
        if(trim($request->ngven_b)==""){
            
            if(trim($request->fech_b)=="")
                $request->fech_b=$fecha;

            $vfacturas=venta::orderBy("nVtaCod","desc");
            if ($codAlmacen !="") {
                $vfacturas->where("nAlmCod", $codAlmacen);
            }

            $vfacturas = $vfacturas->name($request->fech_b,$request->tdoc_b,$request->fpago_b/*,$request->ngven_b*/,$request->rdni,$request->tie_b)
                ->paginate(10);      
        }
        else{
           $vfacturas=venta::orderBy("nVtaCod","desc");
           if ($codAlmacen !="") {
                $vfacturas->where("nAlmCod", $codAlmacen);
            }
           $vfacturas = $vfacturas->credito($request->ngven_b)->paginate(10);       
        }
        $tdoc=tipodocv::all();
        $fpago=forpago::all();
        $cli=Cliente::all();
        $alm=Almacen::all();
        
        return view("comercializacion.venta.index",compact("vfacturas","rq","tdoc","fpago","cli","fecha","alm"));
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();

        try 
        {   
              
            $cajave=CajaVenta::where("nVtaCod",$id)->first();
            
            if($cajave->cajaapertura->bCajCieAbierto=="0")
            {
           
                $venta=venta::where("nVtaCod",$id)->first();

                if (Devoluciones::where("nVtaCod",$id)->exists())
                {
                    return back()->with('info','No se puede anular porque tiene nota de devoluvión, puede fijarse en detalles...');
                }
                else
                {

                    if($venta->bSV=="0")
                    {
                        $tradetalle=ventadetalle::where("nVtaCod",$id)->get();
                       
                        foreach ($tradetalle as $key => $datos) 
                        {

                            if(is_null($datos->nProAlmCod)==1)                                
                            {
                                if(is_null($datos->nCodTC)==1)                                
                                {
                                    $stock=Res_stock_mp::where('id',"=",$datos->nCodMP)->first();
                                
                                    $fecha_aux=Carbon::now()->format('Y-m-d');

                                    $save_movMP=new Movimiento_MP;
                                    $save_movMP->fecha=$fecha_aux;
                                    //$save_movMP->compra_id=1;                                
                                    $save_movMP->proveedor_id=$stock->proveedor_id;
                                    $save_movMP->lote=$stock->lote;
                                    $save_movMP->titulo_id=$stock->titulo_id;
                                    $save_movMP->cantidad=$datos->nVtaPeso;
                                    $save_movMP->estado=1;
                                    $save_movMP->insumo_id=$stock->insumo_id; 
                                    $save_movMP->accesorio_id=$stock->accesorio_id; 
                                    $save_movMP->descripcion="Anulación";
                                    $save_movMP->peso_neto=$stock->peso_neto; 

                                    $save_movMP->save();


                                    Res_stock_mp::where('id',"=",$datos->nCodMP)
                                    ->update([
                                        'peso_neto' => ($stock->peso_neto + $datos->nVtaPeso),
                                        'cantidad' => ($stock->cantidad + $datos->nVtaCant)
                                    ]);
                                    /*********** fin actualizar stock ******************/   
                                }
                                else
                                {
                                    /*********** inicio actualizar stock alm1 ******************/            

                                    $stock=Res_Stock_Telas::where('id',"=",$datos->nCodTC)->first();

                                    $save_mov=new Movimiento_Tela;
                                    $save_mov->planeacion_id=1;
                                    $save_mov->producto_id=$stock->producto_id;
                                    $save_mov->proveedor_id=$stock->proveedor_id;
                                    $save_mov->nro_lote=$stock->nro_lote;
                                    $save_mov->rollos=$datos->nVtaCant;
                                    $save_mov->cantidad=$datos->nVtaPeso;
                                    $save_mov->descripcion="Anulación";
                                    $save_mov->estado=0;

                                    $save_mov->save();


                                    Res_Stock_Telas::where('id',"=",$datos->nCodTC)
                                    ->update([
                                        'cantidad' => ($stock->cantidad + $datos->nVtaPeso),
                                        'rollos' => ($stock->rollos + $datos->nVtaCant)
                                    ]);
                                    /*********** fin actualizar stock ******************/
                                }
                            }
                            else
                            {
                                /*********** inicio actualizar stock alm1 ******************/
                                $stock=ProductoAlmacen::where('nProAlmCod',"=",$datos->nProAlmCod)->first();
                                  
                                $total=0;
                                if($stock->producto->nombre_especifico=="TELAS")                            
                                    $total=$stock->nProdAlmStock + $datos->nVtaPeso;                                                           
                                else
                                    $total=$stock->nProdAlmStock + $datos->nVtaCant;        
                                                 
                                ProductoAlmacen::where('nProAlmCod',"=",$datos->nProAlmCod)->update(['nProdAlmStock' => $total]);
                                /*********** fin actualizar stock ******************/ 
                            }                               
                        }                
                    }           

                    //********************* Eliminar en caja_venta y disminuir el montototalventa en caja apertura *********************/

                    $id_user=access()->user()->id;
                    $userX=Users::where("id",$id_user)->first();   
                    $apertura=CajaApertura::where("nRolAreCod",$userX->rolsareas[0]->nRolAreCod)->where("bCajCieAbierto","0")->first();

                    $total_aux=$apertura->dCajApeMonTotVen;
                    $total_aux-=$venta->dVFacVTot;

                    CajaApertura::where('cCajApeCod',"=",$apertura->cCajApeCod)
                    ->update([                
                        'dCajApeMonTotVen' => $total_aux
                        ]);

                    /*if($apertura->bCajCieAbierto=="1")
                    {
                        $total_aux2=$apertura->cajacierre->dCajMonCie;
                        $total_aux2-=$venta->dVFacVTot;

                        CajaCierre::where('cCajCieCod',"=",$apertura->cajacierre->cCajCieCod)
                        ->update([                
                            'dCajMonCie' => $total_aux2
                            ]);
                    }      */      

                    $cv=CajaVenta::where('cCajApeCod','=',$apertura->cCajApeCod)->where("nVtaCod","=",$id);
                    $cv->delete();

                    $fecha_elim=Carbon::now()->format('Y-m-d H:i:s');

                    venta::where("nVtaCod",$id)
                    ->update([
                        'anulado' => "1",
                        'dFecElim' => $fecha_elim
                    ]);

                    //$tra->delete();
                }
            }
            else
                return back()->with('info','No se puede anular porque la caja esta cerrada.');                

        } 
        catch (Exception $e) 
        {

            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return back()->with('info','El registro fue anulado.');
    }

    function reporte($id)
    {
        $vfacturas=venta::where("anulado","0")->where("nVtaCod",$id)->first();

        $dia=Carbon::now()->format('d');  
        $mes=Carbon::now()->format('m');  
        $anio=Carbon::now()->format('Y');  

        $mes_n="";
        switch ($mes) {
            case '01': $mes_n="Enero";break;
            case '02': $mes_n="Febrero";break;
            case '03': $mes_n="Marzo";break;
            case '04': $mes_n="Abril";break;
            case '05': $mes_n="Mayo";break;
            case '06': $mes_n="Junio";break;
            case '07': $mes_n="Julio";break;
            case '08': $mes_n="Agosto";break;
            case '09': $mes_n="Setiembre";break;
            case '10': $mes_n="Octubre";break;
            case '11': $mes_n="Noviembre";break;
            case '12': $mes_n="Diciembre";break;
        }

        //return compact("vfacturas");
        //return view("comercializacion.venta.reporte",compact("vfacturas","dia","mes","anio","mes_n"));

        /*$date = date('Y-m-d');
        $invoice = "2222";*/
        //$view =  \View::make('pdf.invoice', compact('data', 'date', 'invoice'))->render();
        //if($vfacturas->nForPagCod!=3)
        //{
            switch ($vfacturas->nTipPagCod) 
            {
                case 1:
                    $view =  \View::make('comercializacion.venta.guia', compact("vfacturas","dia","mes","anio","mes_n"))->render();
                    $pdf = \App::make('dompdf.wrapper');
                    $pdf->loadHTML($view)->setPaper([5, -33, 375.465, 715.276], 'landscape');//margen izq.- arriba, alto,ancho//->setPaper('a4', 'landscape');
                    return $pdf->stream('invoice');
                case 2:
                    $view =  \View::make('comercializacion.venta.boleta', compact("vfacturas","dia","mes","anio","mes_n"))->render();
                    $pdf = \App::make('dompdf.wrapper');
                    $pdf->loadHTML($view)->setPaper('a4', 'potrait');
                    return $pdf->stream('invoice');
                case 3:
                    $view =  \View::make('comercializacion.venta.factura', compact("vfacturas","dia","mes","anio","mes_n"))->render();
                    $pdf = \App::make('dompdf.wrapper');
                    $pdf->loadHTML($view)->setPaper('a4', 'potrait');
                    return $pdf->stream('invoice');
                    //break;            
            }
        //}
        
    }

    function reportesologuia($id)
    {
        $vfacturas=venta::where("anulado","0")->where("nVtaCod",$id)->first();

        $dia=Carbon::now()->format('d');  
        $mes=Carbon::now()->format('m');  
        $anio=Carbon::now()->format('Y');  

        $mes_n="";
        switch ($mes) {
            case '01': $mes_n="Enero";break;
            case '02': $mes_n="Febrero";break;
            case '03': $mes_n="Marzo";break;
            case '04': $mes_n="Abril";break;
            case '05': $mes_n="Mayo";break;
            case '06': $mes_n="Junio";break;
            case '07': $mes_n="Julio";break;
            case '08': $mes_n="Agosto";break;
            case '09': $mes_n="Setiembre";break;
            case '10': $mes_n="Octubre";break;
            case '11': $mes_n="Noviembre";break;
            case '12': $mes_n="Diciembre";break;
        }

        //if($vfacturas->nForPagCod!=3)
        //{     
            $view =  \View::make('comercializacion.venta.guia_rem', compact("vfacturas","dia","mes","anio","mes_n"))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view)->setPaper([5, 0, 630, 630], 'landscape');//->setPaper('a4', 'potrait');
            return $pdf->stream('invoice');    
        //}
        
    }

    function regcliente($codigo,$nombre,$direc,$obs)
    {        

        //if($request->ajax()){
        if (!(Cliente::where('cClieNdoc', '=', trim($codigo))->exists()))
        {
            $saveCli=new Cliente;
            $saveCli->nTdocCod=(strlen($codigo)==8)?2:1;
            $saveCli->cClieNdoc=$codigo;
            $saveCli->cClieDesc=$nombre;
            $saveCli->cClieDirec=$direc;
            $saveCli->cClieObs=$obs;

            $saveCli->save();

            $dato=Cliente::select("nClieCod")->orderBy("nClieCod","desc")->first();

            return $dato->nClieCod;
        }
        else
        {
            return "0";
        }
        //}

    }

    function autocomplete($id)
    {    
        //$term = Input::get('term');
        
        $results = array();
        
        $queries = Cliente::where('cClieNdoc','like','%'.$id.'%')
            ->orWhere('cClieDesc', 'LIKE', '%'.$id.'%')
            ->where('anulado','0')
            ->take(5)->get();
        
        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->nClieCod, 'value' => $query->cClieNdoc.' | '.$query->cClieDesc ];
        }

        $results[] = [ 'id' => '0', 'value' => 'Cliente Nuevo...','cod'=>$id ];

        return response()->json($results);   
        //return Response::json($results);    
    }

    function autoTipoDoc($id)
    {    
        //$term = Input::get('term');
        
        $results = array();
        
        $queries = tipodocv::where('cDescTipPago','like','%'.$id.'%')->get();
        
        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->nTipPagCod, 'value' => $query->cDescTipPago ];
        }

        return response()->json($results);   
        //return Response::json($results);    
    }    

    function autofORpaG($id)
    {    
        //$term = Input::get('term');
        
        $results = array();
        
        $queries = forpago::where('cDescForPag','like','%'.$id.'%')->get();
        
        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->nForPagCod, 'value' => $query->cDescForPag ];
        }

        return response()->json($results);   
        //return Response::json($results);    
    }

    function autocomplete_producto($nAlmCod,$busca)
    {    
        //$term = Input::get('term');
        $array = explode(" ", trim($busca));
        $cad='%';
        foreach ($array as $key => $value) {
            $cad.=$value.'%';
        }


        $results = array();
        
        $queries = DB::table("productoalmacens")
        ->Join("productos","productoalmacens.cProdCod","=","productos.id")            
        ->Join("color","productoalmacens.id_color","=","color.id")          
        ->where('productoalmacens.nAlmCod','=',$nAlmCod)            
        ->select("color.nombre","productos.nombre_generico","productos.nombre_especifico","productoalmacens.nProAlmCod")
        //->orWhere("productos.nombre_generico","like","%".$busca."%")
        //->orWhere("color.nombre",$busca)
        ->where(DB::raw("concat(productos.nombre_generico,' ', color.nombre)"),'like',$cad)
        ->distinct()
        ->take(5)
        ->get();
        
        foreach ($queries as $query)
        {
            $results[] = [ 'nProAlmCod' => $query->nProAlmCod, 'value' => $query->nombre_generico.' '.$query->nombre, 'nombre' => $query->nombre, 'nombre_generico' =>  $query->nombre_generico, "nombre_especifico" => $query->nombre_especifico];
        }

        $queries2 = DB::table("resumen_stock_telas")
        ->Join("productos","resumen_stock_telas.producto_id","=","productos.id")            
        ->select("productos.nombre_generico","productos.nombre_especifico","resumen_stock_telas.producto_id")
        ->where("productos.nombre_generico",'like',$cad)
        ->whereNull('resumen_stock_telas.deleted_at')
        ->distinct()
        ->take(5)
        ->get();
        
        foreach ($queries2 as $query2)
        {
            $results[] = [ 'nProAlmCod' => 'TC'.$query2->producto_id, 'value' => $query2->nombre_generico.' (TC)','nombre' => '', 'nombre_generico' =>  $query2->nombre_generico, "nombre_especifico" => $query2->nombre_especifico];
        }

        $queries3 = DB::table("resumen_stock_materiaprima")
        ->Join("insumos","resumen_stock_materiaprima.insumo_id","=","insumos.id") 
        ->Join("titulos","resumen_stock_materiaprima.titulo_id","=","titulos.id")           
        ->select("insumos.nombre_generico","insumos.nombre_especifico","resumen_stock_materiaprima.id","resumen_stock_materiaprima.cantidad","resumen_stock_materiaprima.peso_neto","titulos.nombre")
        ->where("insumos.nombre_generico",'like',$cad)
        ->where("resumen_stock_materiaprima.insumo_id",'<>',"0")
        ->where("resumen_stock_materiaprima.cantidad",'>',"0")
        ->whereNull('resumen_stock_materiaprima.deleted_at')
        ->distinct()
        ->take(5)
        ->get();
        
        foreach ($queries3 as $query3)
        {
            $results[] = [ 'nProAlmCod' => 'MP'.$query3->id, 'value' => $query3->nombre_generico.' '.$query3->nombre.' (I)','nombre' => '', 'nombre_generico' =>  $query3->nombre_generico, "nombre_especifico" => $query3->nombre_especifico, "cantidad" => $query3->peso_neto, "rollos" => $query3->cantidad];
        }

        $queries4 = DB::table("resumen_stock_materiaprima")
        ->Join("accesorios","resumen_stock_materiaprima.accesorio_id","=","accesorios.id")            
        ->select("accesorios.nombre","resumen_stock_materiaprima.id","resumen_stock_materiaprima.cantidad")
        ->where("accesorios.nombre",'like',$cad)
        ->where("resumen_stock_materiaprima.accesorio_id",'<>',"0")
        ->where("resumen_stock_materiaprima.cantidad",'>',"0")
        ->whereNull('resumen_stock_materiaprima.deleted_at')
        ->distinct()
        ->take(5)
        ->get();
        
        foreach ($queries4 as $query4)
        {
            $results[] = [ 'nProAlmCod' => 'MP'.$query4->id, 'value' => $query4->nombre.' (A)','nombre' => '', 'nombre_generico' =>  $query4->nombre, "nombre_especifico" => '', "cantidad" => $query4->cantidad, "rollos" => "0"];
        }

        return response()->json($results);           
    }
/*
    function autocomplete_producto_TC($busca)
    {  
        $results = array();
        
        $queries = DB::table("resumen_stock_telas")
        ->Join("productos","resumen_stock_telas.producto_id","=","productos.id")            
        ->select("productos.nombre_generico","productos.nombre_especifico","resumen_stock_telas.producto_id")
        ->where("productos.nombre_generico",'like',"%".$busca."%")
        ->distinct()
        ->take(5)
        ->get();
        
        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->producto_id, 'value' => $query->nombre_generico.' (TC)', 'nombre_generico' =>  $query->nombre_generico, "nombre_especifico" => $query->nombre_especifico];
        }

        return response()->json($results);             
    }*/
}

