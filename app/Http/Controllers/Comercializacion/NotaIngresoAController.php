<?php

namespace App\Http\Controllers\Comercializacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Tienda;
use App\Color;
use App\Proveedor;
use App\Producto;
use App\NotaIngresoA;
use App\DetalleNotaIngresoA;
use App\ProductoAlmacen;
use App\Almacen;
use Carbon\Carbon;
use DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use \Milon\Barcode\DNS1D;
use \Milon\Barcode\DNS2D; 
use App\tmpimpresion2;

class NotaIngresoAController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        DB::beginTransaction();

        try 
        {
        /*DB::transaction(function()
        {*/
            /************************ ELMINACION *******************************/
            //eliminamos los que esten en codigos a eliminar
                //verificamos si el codigo esta en la bd,par eliminarlo, sino esta el codigo fue alterado por el navegador, sólo se puede eliminar uno de los registro que se han traido(dentro del filtro pasamos el codigo qeu jala todo)
            
            $arrayElim=explode(",", $request->eliminados);

            for($j=1;$j<count($arrayElim);$j++)
            {                  
                $datos=DetalleNotaIngresoA::where('dNotInga_id','=',$arrayElim[$j])->first();  

                /*********** inicio actualizar stock ******************/
                $stock=ProductoAlmacen::where('nProAlmCod',"=",$datos->nProAlmCod)->first();
                  
                $total=0;        
                if($stock->producto->nombre_especifico=="TELAS")                            
                    $total=$stock->nProdAlmStock - $datos->peso_cant;                                                           
                else
                    $total=$stock->nProdAlmStock - $datos->rollo;  

                if($total<0){
                    DB::rollback();
                    return redirect()->back()->with('info','Error: No se pudo eliminar los registros, porque El almacén tiene de stock: '.$stock->nProdAlmStock.", por lo tanto sólo se puede retirar esa cantidad como máximo, lo resto ya se vendió.");  
                }

                ProductoAlmacen::where('nProAlmCod',"=",$datos->nProAlmCod)->update(['nProdAlmStock' => $total]);

                DetalleNotaIngresoA::where('dNotInga_id','=',$arrayElim[$j])->delete();   
                /*********** fin actualizar stock ******************/ 
            }
            //que me vote las partidas segun codigo interno
            //evaluar si no tiene detalles
            //sino delete
            
            $arrayPart=explode(",", $request->parti_eli);

            $partidas = NotaIngresoA::select('partida','ninga_id')
            ->whereIn('partida',$arrayPart)                      
            ->get();

            foreach($partidas as $obj)
            {
                $verdetpar = DB::table('detalle_nota_ingreso_a')
                ->leftJoin('nota_ingreso_a', 'detalle_nota_ingreso_a.ninga_id', '=', 'nota_ingreso_a.ninga_id')
                ->select('detalle_nota_ingreso_a.ninga_id')
                //->where('nota_ingreso_a.desptint_id','=', $request->codint)
                ->where('nota_ingreso_a.partida','=',$obj->partida)            
                ->get()
                ->first();

                if($verdetpar=="")
                {
                    $deletedRows = NotaIngresoA::where('ninga_id','=',$obj->ninga_id)->delete();                                       
                }
            }
            /************************ FIN ELMINACION *******************************/

            for($i=1;$i<=$request->conta;$i++)
            {
                if($request["cod_ndi_".$i]!="")
                {
                    if($request["cod_ndi_".$i]==0)//si es diferente no se ingresa
                    {
                        $ninga_id=NotaIngresoA::select('ninga_id')
                        //->where('despTint_id','=',$request->codint)
                        ->where('partida','=',$request["par_".$i])
                        //->where('fecha','=',$request["fec_".$i])
                        ->orderBy('ninga_id','desc')->get()->first();

                        if($ninga_id=="")
                        {
                            $ninga_id=NotaIngresoA::select('ninga_id')->orderBy('ninga_id','desc')->get()->first();


                            $NotaIngresoa=new NotaIngresoA;
                            $NotaIngresoa->color_id=$request["col_".$i];
                            $NotaIngresoa->producto_id=$request["pro_".$i];
                            $NotaIngresoa->proveedor_id=$request["prov_".$i];
                            $NotaIngresoa->partida=$request["par_".$i];
                            $NotaIngresoa->fecha=$request["fec_".$i];
                            //$NotaIngreso->fecha=$request["fec_".$i];

                            $NotaIngresoa->save();
                        }
                        else
                        {
                            /**************************** ACTUALIZAR *******************************/
                                /*if(in_array($request["cod_ndi_".$i], $arraycad_actt, true))
                                {
                                    DetalleNotaIngreso::where('dNotIng_id',"=",$request["cod_ndi_".$i])                              
                                    ->update(['tienda_id' => $request["tie_".$i],'peso_cant' => $request["pes_".$i],'rollo' => $request["roll_".$i]]);    
                                }*/
                            /**************************** FIN ACTUALIZAR ***************************/
                        }
                    }
                }
            }

            $arraycad_actt=explode(",", $request->cad_actt);
            $noimpresos_ = array();

            //Recorrer, buscar el reg de la partida y fecha y registrar
            for($i=1;$i<=$request->conta;$i++)
            {
                if($request["cod_ndi_".$i]!="")
                {
                    $ninga_id=NotaIngresoA::where('partida','=',$request["par_".$i])
                        //->where('fecha','=',$request["fec_".$i])
                        ->orderBy('ninga_id','desc')->first();

                    if($request["cod_ndi_".$i]==0)//si es diferente no se ingresa
                    {                        
                        $DetalleNotaIngreso=new DetalleNotaIngresoA;
                        $DetalleNotaIngreso->ninga_id=$ninga_id->ninga_id;
                        $DetalleNotaIngreso->nProAlmCod=$request["tie_".$i];
                        $DetalleNotaIngreso->cod_barras="";
                        $DetalleNotaIngreso->peso_cant=$request["pes_".$i];
                        $DetalleNotaIngreso->rollo=$request["roll_".$i];
                        $DetalleNotaIngreso->impreso="1";
                        $DetalleNotaIngreso->estado="0";
                        $DetalleNotaIngreso->fecha=$request["fec_".$i];

                        $DetalleNotaIngreso->save();

                        /*********** inicio actualizar stock ******************/
                        $stock=ProductoAlmacen::where('nProAlmCod',"=",$request["tie_".$i])->first();
                        
                        $total=0;        
                        if($stock->producto->nombre_especifico=="TELAS")                            
                            $total=$stock->nProdAlmStock + $request["pes_".$i];                                                           
                        else
                            $total=$stock->nProdAlmStock + $request["roll_".$i];                                                           
                        ProductoAlmacen::where('nProAlmCod',"=",$request["tie_".$i])->update(['nProdAlmStock' => $total]);
                        /*********** fin actualizar stock ******************/


                        $codi=DetalleNotaIngresoA::select('dNotInga_id')
                        ->orderBy('dNotInga_id','Desc')                      
                        ->get()
                        ->first();          

                        /*$cb=$codi->dNotInga_id."-".$request["fec_".$i];          
                        

                        DetalleNotaIngresoA::where('dNotInga_id',"=",$codi->dNotInga_id)                              
                            ->update(['cod_barras' => $cb]); 

                        if(!isset($request["cbox_".$i]))//agregam los codigos de barra que estan sin marcar
                            array_push($noimpresos_, $cb);*/


                            /*CODIGO DE BARRAS*/
                        //$cb=substr($ninga_id->producto->nombre_especifico,0,1) . "-". $request["par_".$i]. "-" .$codi->dNotInga_id;
                        $cb=substr($ninga_id->producto->nombre_especifico,0,1) . "-". $request["par_".$i];//.$codi->dNotInga_id;

                        /* Estructura del código de barras
                         * T-LOTE-{PARTIDA-0001} (TELAS), ok
                         * C-LOTE-{PARTIDA-0001} (CUELLOS), ok
                         * P-LOTE-{PARTIDA-0001} (PUÑOS), ok

                         * Además del código de barras tendremos un campo código (telas) que
                         * es el peso ejemplo: 23.21 kg => 23C21H00
                         * Para el caso de cuellos y puños si estará la cantidad.
                         */
                         
                         $color_for= $ninga_id->color->nombre;
                         $producto_generico = $ninga_id->producto->nombre_generico;

                         //echo dd(strpos($request["pes_".$i],'.'));

                         if(substr($ninga_id->producto->nombre_especifico,0,1)=='T')
                         {  
                            if(strpos($request["pes_".$i],'.') == false) 
                            {
                                $peso_for= $request["pes_".$i]."T00B00";
                            }
                            else 
                            {
                                $peso_for= substr($request["pes_".$i], 0, strpos($request["pes_".$i],'.')) . "T" . substr($request["pes_".$i], strpos($request["pes_".$i],'.')+1). "B00";
                            }
                        }
                         else
                        {
                            $peso_for= $request["pes_".$i];
                        }

                         /* FIN CODIGO DE BARRAS*/
                         DetalleNotaIngresoA::where('dNotInga_id',"=",$codi->dNotInga_id)                              
                            ->update(['cod_barras' => $cb]); 


                        if(!isset($request["cbox_".$i])){//agregam los codigos de barra que estan sin marcar
                            array_push($noimpresos_,array($cb, $peso_for,$color_for,$producto_generico));
                        }
                    
                    }
                    else
                    {                        
                        $color_for= $ninga_id->color->nombre;
                        $producto_generico = $ninga_id->producto->nombre_generico;

                         //echo dd(strpos($request["pes_".$i],'.'));
                         
                         if(substr($ninga_id->producto->nombre_especifico,0,1)=='T')
                         {  
                            if(strpos($request["pes_".$i],'.') == false) 
                            {
                                $peso_for= $request["pes_".$i]."T00B00";
                            }
                            else 
                            {
                                $peso_for= substr($request["pes_".$i], 0, strpos($request["pes_".$i],'.')) . "T" . substr($request["pes_".$i], strpos($request["pes_".$i],'.')+1). "B00";
                            }
                        }
                         else
                        {
                            $peso_for= $request["pes_".$i];
                        }
                        /**************************** ACTUALIZAR *******************************/
                            /*if(in_array($request["cod_ndi_".$i], $arraycad_actt, true))
                            {
                                DetalleNotaIngreso::where('dNotIng_id',"=",$request["cod_ndi_".$i])                              
                                ->update(['tienda_id' => $request["tie_".$i],'peso_cant' => $request["pes_".$i],'rollo' => $request["roll_".$i]]);    
                            }*/
                        /**************************** FIN ACTUALIZAR ***************************/
                        if(!isset($request["cbox_".$i]))
                        {//agregam los codigos de barra que estan sin marcar
                            //array_push($noimpresos_, $request["cb_".$i]);
                            array_push($noimpresos_,array($request["cb_".$i],$peso_for,$color_for,$producto_generico));  
                        }
                    }
                }
            }
        } catch (Exception $e) {

            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        //});
/*  
            if(empty($noimpresos_))            
                return redirect()->route('notaingresoatipico.create',$request->codint)->with('info','Las notas atipico se crearon correctamente.');        
            else
            {
                $cod_ndi=$request->codint;                            
                $noimpresos=json_encode($noimpresos_,true);   */

                foreach ($noimpresos_ as $key => $value) {
                    $regimp=new tmpimpresion2;
                    $regimp->campo1=$value[0];
                    $regimp->campo2=$value[1];
                    $regimp->campo3=$value[2];
                    $regimp->campo4=$value[3];                  
                    $regimp->save();           
                }

                /* $view =  \View::make('comercializacion.notaingresoatipico.impresion',compact("noimpresos","cod_ndi"))->render();
                $pdf = \App::make('dompdf.wrapper');
                $pdf->loadHTML($view)->setPaper('a5', 'landscape');
                return $pdf->stream('invoice');                 
                
                return view("comercializacion.notaingresoatipico.impresion",compact("noimpresos","cod_ndi"));
            }    
*/
        return redirect()->route('notaingresoatipico.create')->with('info','Las guardó correctamente.');  
    }    

    public function create()
    {        
        $proveedor=Proveedor::all();
        //$color=Color::all();
        $producto=Producto::all();

        $bandejatabla = DB::table('detalle_nota_ingreso_a')
        ->leftJoin('productoalmacens', 'detalle_nota_ingreso_a.nProAlmCod', '=', 'productoalmacens.nProAlmCod')
        ->leftJoin('almacens', 'productoalmacens.nAlmCod', '=', 'almacens.nAlmCod')            
        ->leftJoin('nota_ingreso_a', 'detalle_nota_ingreso_a.ninga_id', '=', 'nota_ingreso_a.ninga_id')
        ->leftJoin('color', 'nota_ingreso_a.color_id', '=', 'color.id')
        ->leftJoin('productos', 'nota_ingreso_a.producto_id', '=', 'productos.id')            
        ->leftJoin('proveedores', 'nota_ingreso_a.proveedor_id', '=', 'proveedores.id')

        ->select('detalle_nota_ingreso_a.dNotInga_id',
            'detalle_nota_ingreso_a.fecha',                
            'productos.nombre_generico',
            'productos.nombre_especifico',
            'productos.id',
            'productoalmacens.nProAlmCod',
            'almacens.cAlmNom',
            'nota_ingreso_a.partida',
            'color.nombre',
            'color.id as id_color',
            'detalle_nota_ingreso_a.peso_cant',
            'detalle_nota_ingreso_a.rollo',
            'detalle_nota_ingreso_a.impreso',
            'detalle_nota_ingreso_a.cod_barras')
        //->where('nota_ingreso.desptint_id','=', $id)
        ->orderBy('detalle_nota_ingreso_a.dNotInga_id',"Asc")->get();
        //->paginate(10);

        $id=0;

        $partidas_ultima = NotaIngresoA::select('ninga_id')
            ->orderBy('ninga_id',"desc")                      
            ->get()->first();

        $conn=0;

        if($partidas_ultima!="")
            $conn=$partidas_ultima->ninga_id;       

        $cad="PA";
        for($i=strlen(++$conn);$i<11;$i++)
        {
            $cad.="0";
        }
        $cad.= $conn;


        $fecha=Carbon::now()->format('Y-m-d H:i:s');
        $impresion="0";
        if (tmpimpresion2::select("cod")->exists())
            $impresion="1";

        return view("comercializacion.notaingresoatipico.create",compact('proveedor','tienda','producto','fecha','id','bandejatabla',"cad","conn","impresion"));
        //return $bandejatabla;
    }

    public function colorAlm(Request $request,$id,$col)
    {
        if($request->ajax())
        {
            $id_user=access()->user()->id;

            $almacenes_x=DB::table("productoalmacens")
            ->select("productoalmacens.nProAlmCod","almacens.cAlmNom")
            ->Join("almacens","productoalmacens.nAlmCod","=","almacens.nAlmCod")
            ->Join("areas_almacen","areas_almacen.nAlmCod","=","almacens.nAlmCod")
            ->Join("rols_areas","rols_areas.nAreAlmCod","=","areas_almacen.nAreAlmCod") 
            ->where('productoalmacens.cProdCod','=',$id)
            ->where('productoalmacens.id_color','=',$col)
            ->where('rols_areas.id','=',$id_user)
            ->get();
            return response()->json($almacenes_x);
        }
    }

    public function productoCol(Request $request,$id)
    {
        if($request->ajax()){
            $colors_x=DB::table("productoalmacens")
            ->select("color.id","color.nombre")
            ->Join("color","productoalmacens.id_color","=","color.id")
            ->where('productoalmacens.cProdCod','=',$id)
            ->distinct("color.id")
            ->get();
            return response()->json($colors_x);
        }
    }

    function impresion()
    {
        $noimpresos=tmpimpresion2::all();
        
        $ar=tmpimpresion2::select("cod");
        $ar->delete();

        $cod_ndi="";
        $barra = new DNS1D();
        //$cod_ndi=$request->codint;                            
        //$noimpresos=json_encode($noimpresos_,true);     
        /*foreach ($noimpresos_ as $key => $value) {
            $regimp=new tmpimpresion1;
            $regimp->campo1=$value[0];
            $regimp->campo2=$value[1];
            $regimp->campo3=$value[2];
            $regimp->campo4=$value[3];
            $regimp->nt=$request->codint;
            $regimp->save();           
        }*/
      
      //return view("comercializacion.notaingresoatipico.impresion",compact("noimpresos","cod_ndi","barra"));  
        
        $view =  \View::make('comercializacion.notaingresoatipico.impresion',compact("noimpresos","cod_ndi","barra"))->render();
        $pdf = \App::make('dompdf.wrapper');
        //$pdf->loadHTML($view)->setPaper([5, -36, 450.465, 700.276], 'landscape');//margen izq.- arriba, alto,ancho
        //$pdf->loadHTML($view)->setPaper([30, -30, 490, 620.276], 'landscape');//margen izq.- arriba, alto,ancho
        //$pdf->loadHTML($view)->setPaper([40, -25, 360, 620], 'landscape');//margen izq.- arriba, alto,ancho
        $pdf->loadHTML($view)->setPaper([30, -5, 390, 5000], 'landscape');//margen izq.- arriba, alto,ancho
        return $pdf->stream('invoice');   
     
      
           
    }
    
  
}
