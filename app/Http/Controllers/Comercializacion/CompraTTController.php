<?php

namespace App\Http\Controllers\Comercializacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Proveedor;
use App\tipodocv;
use App\forpago;
use Illuminate\Support\Facades\Auth;
use App\Users;
use App\Almacen;
use App\CompraTelat;
use App\DetCompraTelat;
use App\pago_compra;
use App\detalle_pago_compra;
use App\CajaApertura;
use Carbon\Carbon;

use DB;

class CompraTTController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $proveedores = Proveedor::all();
        $tipdocpag = tipodocv::all();
        $forpago = forpago::all();

        $userX=Users::where("id",Auth::id())->first();
        $codalm=$userX->rolsareas[0]->areasalmacen->almacen->nAlmCod;
        $almacen=Almacen::where("nAlmCod",$codalm)->orderby("nAlmCod","asc")->first();

        //$apertura=CajaApertura::where("nRolAreCod",$userX->rolsareas[0]->nRolAreCod)->where("bCajCieAbierto","0")->first();

        $results = array();
        
        $productoAlmacen=DB::table("productoalmacens")
            ->Join("productos","productoalmacens.cProdCod","=","productos.id")            
            ->Join("color","productoalmacens.id_color","=","color.id")          
            ->where('productoalmacens.nAlmCod','=',$almacen->nAlmCod)            
            ->select("color.nombre","productos.nombre_generico","productos.nombre_especifico","productoalmacens.nProAlmCod")
            ->distinct()
            ->get();

        foreach ($productoAlmacen as $palm)
        {
            $results[] = [ 'nProAlmCod' => $palm->nProAlmCod, 'value' => $palm->nombre_generico.' '.$palm->nombre, 'nombre_generico' =>  $palm->nombre_generico, "nombre_especifico" => $palm->nombre_especifico/*, "tc" => "0", "mp" => "0", "cantidad" => ''*/];
        }
        
        /*$queries = DB::table("resumen_stock_telas")
        ->Join("productos","resumen_stock_telas.producto_id","=","productos.id")            
        ->select("productos.nombre_generico","productos.nombre_especifico","resumen_stock_telas.producto_id")
        //->where("productos.nombre_generico",'like',"%".$busca."%")
        ->distinct()
        //->take(5)
        ->get();
        
        foreach ($queries as $query)
        {
            $results[] = [ 'nProAlmCod' => $query->producto_id, 'value' => $query->nombre_generico.' (TC)', 'nombre_generico' =>  $query->nombre_generico, "nombre_especifico" => $query->nombre_especifico, "tc" => "1", "mp" => "0", "cantidad" => ''];
        }

        $queries3 = DB::table("resumen_stock_materiaprima")
        ->Join("insumos","resumen_stock_materiaprima.insumo_id","=","insumos.id")            
        ->select("insumos.nombre_generico","insumos.nombre_especifico","resumen_stock_materiaprima.id","resumen_stock_materiaprima.cantidad")
        //->where("insumos.nombre_generico",'like',"%".$busca."%")
        ->where("resumen_stock_materiaprima.insumo_id",'<>',"0")
        ->distinct()
        ->take(5)
        ->get();
        
        foreach ($queries3 as $query3)
        {
            $results[] = [ 'nProAlmCod' => 'MP'.$query3->id, 'value' => $query3->nombre_generico.' (I)', 'nombre_generico' =>  $query3->nombre_generico, "nombre_especifico" => $query3->nombre_especifico, "tc" => "0", "mp" => "1", "cantidad" => $query3->cantidad];
        }

        $queries4 = DB::table("resumen_stock_materiaprima")
        ->Join("accesorios","resumen_stock_materiaprima.accesorio_id","=","accesorios.id")            
        ->select("accesorios.nombre","resumen_stock_materiaprima.id","resumen_stock_materiaprima.cantidad")
        //->where("accesorios.nombre",'like',"%".$busca."%")
        ->where("resumen_stock_materiaprima.accesorio_id",'<>',"0")
        ->distinct()
        ->take(5)
        ->get();
        
        foreach ($queries4 as $query4)
        {
            $results[] = [ 'nProAlmCod' => 'MP'.$query4->id, 'value' => $query4->nombre.' (A)', 'nombre_generico' =>  $query4->nombre, "nombre_especifico" => '', "tc" => "0", "mp" => "1", "cantidad" => $query4->cantidad];
        }*/


        return view("comercializacion.compraTT.create",compact("proveedores","tipdocpag","forpago","results"));
    }

    public function index(Request $request)
    {        
        $fechahora=Carbon::now()->format('Y-m-d');
        if($request->fecha=="")
            $request->fecha=$fechahora;

    	$ct=CompraTelat::where("tFecha",$request->fecha)        
        ->Where("nCodProv","like","%".$request->proveedor."%")
        ->Where("nCodTdoc","like","%".$request->tipdoc."%")
        ->Where("nCodForP","like","%".$request->forpag."%")
        ->whereNull("deleted_at")
        ->paginate(10);
        
        $datosant=$request;

        $proveedores = Proveedor::all();
        $tipdocpag = tipodocv::all();
        $forpago = forpago::all();

    	return view("comercializacion.compraTT.index",compact("ct","datosant","proveedores","tipdocpag","forpago"));
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'fecha'       => 'required',
            'proveedor'       => 'required',
            'tipdoc'       => 'required',
            'forpag'       => 'required'//,
            //'prod'       => 'required',
            //'pes'       => 'required',
            //'prec'       => 'required',
            //'dat'       => 'required',
            //'obs'       => 'required'
        ]);

        DB::beginTransaction();

        try 
        {   
            if($request["tot_g_i"]<=0)
            {
                DB::rollback();
                return redirect()->back()->with('info','No se puede registrar si el total es 0.00');  
            }

            //$fechahora=Carbon::now()->format('Y-m-d h:i:s');
        
            $ct=new CompraTelat;
            $ct->tFecha=$request["fecha"];
            $ct->dMonTotal=$request["tot_g_i"];
            $ct->cMoneda=$request["mon"];
            $ct->nCodProv=$request["proveedor"];
            $ct->nCodTdoc=$request["tipdoc"];
            $ct->nCodForP=$request["forpag"];
            $ct->cDatDocFac=$request["dat"];
            $ct->cObs=$request["obs"];           
      
            $ct->save();

        
            $coding=CompraTelat::select("nCodComTel")->orderBy("nCodComTel","desc")->first();

            for($j=1;$j<=$request->conta;$j++)
            {
                if($request["cod_ndi_".$j]!="")
                {
                    if($request["cod_ndi_".$j]=="0")
                    {
                        $detcomtel=new DetCompraTelat;                                          
                        $detcomtel->nProAlmCod=$request["prod_".$j];
                        $detcomtel->dPeso=$request["peso_".$j];                        
                        $detcomtel->dPrecio=$request["prec_".$j];
                        $detcomtel->dSubtotal=$request["stot_".$j];
                        $detcomtel->nCodComTel=$coding->nCodComTel;
                                            
                        $detcomtel->save();   
                    }
                }
            }
            
            if($request["forpag"]==3)
            {
                $pc=new pago_compra;            
                $pc->dPagoTotal="0";
                $pc->dPagoSaldo=$request["tot_g_i"];
                $pc->nCodComTel=$coding->nCodComTel;
          
                $pc->save();
            }
            else
            {                
                //--- Registramos el pago ---
                $pc=new pago_compra;            
                $pc->dPagoTotal=$request["tot_g_i"];
                $pc->dPagoSaldo="0";
                $pc->nCodComTel=$coding->nCodComTel;
          
                $pc->save();

                $codpc=pago_compra::select("nCodPagComp")->orderBy("nCodPagComp","desc")->first();

                $dpc=new detalle_pago_compra;            
                $dpc->tFecha=$request["fecha"];
                $dpc->dMontoPag=$request["tot_g_i"];
                $dpc->bCaja=1;
                $dpc->nCodPagComp=$codpc->nCodPagComp;
          
                $dpc->save();

                //--- REgistramos la salida de caja ---

                $id_user=access()->user()->id;
                $userX=Users::where("id",$id_user)->first();   
                $apertura=CajaApertura::where("nRolAreCod",$userX->rolsareas[0]->nRolAreCod)->where("bCajCieAbierto","0")->first();

                if($apertura!="")//evluar si ya aperturo una caja, si si redirigirlo a cierre
                {     
                    $saldo=$apertura->dCajApeMonTotVen - $apertura->dCajApeTotTraSal + $apertura->dCajApeTotTraIng - $apertura->dCajApeTotEgreso;
                    if($saldo>=$request["tot_g_i"])
                    {
                        CajaApertura::where("nRolAreCod",$userX->rolsareas[0]->nRolAreCod)->where("bCajCieAbierto","0")->first()
                        ->update([
                            'dCajApeTotEgreso' => $request["tot_g_i"]
                            ]); 
                    }
                    else
                    {
                        DB::rollback();
                        return redirect()->back()->with('info','No tiene suficiente saldo en Caja.');  
                    }
                }
                else
                {
                    DB::rollback();
                    return redirect()->route('caja.abrircaja')->with('info','Debe aperturar la caja primero.');             
                }
            }
            
        } 
        catch (Exception $e) 
        {
            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return redirect()->route('compraTT.index')->with('info',"Se registró correctamente..."); 

    }
    
    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'fecha'       => 'required',
            'proveedor'       => 'required',
            'tipdoc'       => 'required',
            'forpag'       => 'required'//,
            //'prod'       => 'required',
            //'pes'       => 'required',
            //'prec'       => 'required',
            //'dat'       => 'required',
            //'obs'       => 'required'
        ]);

        DB::beginTransaction();

        try 
        {   
            if($request["tot_g_i"]<=0)
            {
                DB::rollback();
                return redirect()->back()->with('info','No se puede actualizar porque el total es 0.00');  
            }

            CompraTelat::where("nCodComTel","=",$id)
            ->update([
            'tFecha' => $request["fecha"],
            'dMonTotal' => $request["tot_g_i"],
            'cMoneda' => $request["mon"],
            'nCodProv' => $request["proveedor"],
            'nCodTdoc' => $request["tipdoc"],
            'nCodForP' => $request["forpag"],
            'cDatDocFac' => $request["dat"],
            'cObs' => $request["obs"]
            ]); 

            //eliminar los registro que se quitaron y actualizar stock
            $arrayElim=explode(",", $request->eliminados);

            for($j=1;$j<count($arrayElim);$j++)
            {
                DetCompraTelat::where('nDetCodComTel','=',$arrayElim[$j])->delete();  
            }            
        
            for($j=1;$j<=$request->conta;$j++)
            {
                if($request["cod_ndi_".$j]!="")
                {
                    if($request["cod_ndi_".$j]=="0")
                    {
                        $detcomtel=new DetCompraTelat;                                          
                        $detcomtel->nProAlmCod=$request["prod_".$j];
                        $detcomtel->dPeso=$request["peso_".$j];                        
                        $detcomtel->dPrecio=$request["prec_".$j];
                        $detcomtel->dSubtotal=$request["stot_".$j];
                        $detcomtel->nCodComTel=$id;
                                            
                        $detcomtel->save();   
                    }
                }
            }

            pago_compra::where('nCodComTel','=',$id)->delete(); 
            
            if($request["forpag"]==3)
            {
                $pc=new pago_compra;            
                $pc->dPagoTotal="0";
                $pc->dPagoSaldo=$request["tot_g_i"];
                $pc->nCodComTel=$id;
          
                $pc->save();
            }
            else
            {                
                //--- Registramos el pago ---
                $pc=new pago_compra;            
                $pc->dPagoTotal=$request["tot_g_i"];
                $pc->dPagoSaldo="0";
                $pc->nCodComTel=$id;
          
                $pc->save();

                $codpc=pago_compra::select("nCodPagComp")->orderBy("nCodPagComp","desc")->first();

                $dpc=new detalle_pago_compra;            
                $dpc->tFecha=$request["fecha"];
                $dpc->dMontoPag=$request["tot_g_i"];
                $dpc->bCaja=1;
                $dpc->nCodPagComp=$codpc->nCodPagComp;
          
                $dpc->save();

                //--- REgistramos la salida de caja ---

                $id_user=access()->user()->id;
                $userX=Users::where("id",$id_user)->first();   
                $apertura=CajaApertura::where("nRolAreCod",$userX->rolsareas[0]->nRolAreCod)->where("bCajCieAbierto","0")->first();

                if($apertura!="")//evluar si ya aperturo una caja, si si redirigirlo a cierre
                {     
                    $saldo=$apertura->dCajApeMonTotVen - $apertura->dCajApeTotTraSal + $apertura->dCajApeTotTraIng - $apertura->dCajApeTotEgreso;
                    if($saldo>=$request["tot_g_i"])
                    {
                        CajaApertura::where("nRolAreCod",$userX->rolsareas[0]->nRolAreCod)->where("bCajCieAbierto","0")->first()
                        ->update([
                            'dCajApeTotEgreso' => $apertura->dCajApeTotEgreso + $request["tot_g_i"]
                            ]); 
                    }
                    else
                    {
                        DB::rollback();
                        return redirect()->route('compraTT.index')->with('info','Su saldo disponible en Caja es: S/.'.$saldo.', No tiene es suficiente.');  
                    }
                }
                else
                {
                    DB::rollback();
                    return redirect()->route('caja.abrircaja')->with('info','Debe aperturar la caja primero.');             
                }
            }
            
        } 
        catch (Exception $e) 
        {
            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return redirect()->route('compraTT.index')->with('info',"Se actualizó correctamente...");  
    }
/*
    public function show($id)
    {
        $alm=almacen::where('nAlmCod','=',$id)->get()->first();
        return view("comercializacion.almacen.show",compact("alm"));
    }
*/
    public function edit($id)
    {
        $comtel=CompraTelat::where("nCodComTel","=",$id)->first();

        if($comtel->PagoCompra[0]["dPagoTotal"]==0)
        {
            $proveedores = Proveedor::all();
            $tipdocpag = tipodocv::all();
            $forpago = forpago::all();

            $userX=Users::where("id",Auth::id())->first();
            $codalm=$userX->rolsareas[0]->areasalmacen->almacen->nAlmCod;
            $almacen=Almacen::where("nAlmCod",$codalm)->orderby("nAlmCod","asc")->first();

            //$apertura=CajaApertura::where("nRolAreCod",$userX->rolsareas[0]->nRolAreCod)->where("bCajCieAbierto","0")->first();

            $results = array();
            
            $productoAlmacen=DB::table("productoalmacens")
                ->Join("productos","productoalmacens.cProdCod","=","productos.id")            
                ->Join("color","productoalmacens.id_color","=","color.id")          
                ->where('productoalmacens.nAlmCod','=',$almacen->nAlmCod)            
                ->select("color.nombre","productos.nombre_generico","productos.nombre_especifico","productoalmacens.nProAlmCod")
                ->distinct()
                ->get();

            foreach ($productoAlmacen as $palm)
            {
                $results[] = [ 'nProAlmCod' => $palm->nProAlmCod, 'value' => $palm->nombre_generico.' '.$palm->nombre, 'nombre_generico' =>  $palm->nombre_generico, "nombre_especifico" => $palm->nombre_especifico/*, "tc" => "0", "mp" => "0", "cantidad" => ''*/];
            }

            return view("comercializacion.compraTT.edit",compact("proveedores","tipdocpag","forpago","results","comtel"));    
        }
        else
        {
            return redirect()->route('compraTT.index')->with('info',"Imposible de editar porque ya tiene pagos relacionados.");
        }
        
    }

   public function destroy($id)
    {
    	
        $comtel=CompraTelat::where("nCodComTel","=",$id)->first();

        if($comtel->PagoCompra[0]["dPagoTotal"]==0)
        {
            DB::beginTransaction();

            try 
            {    
                $fecha=Carbon::now()->format('Y-m-d h:i:s');  
                CompraTelat::where("nCodComTel","=",$id)
                ->update([
                'deleted_at' => $fecha
                ]); 
                /*pago_compra::where('nCodComTel','=',$id)->delete(); 
                DetCompraTelat::where('nCodComTel','=',$id)->delete();
                CompraTelat::where("nCodComTel","=",$id)->delete(); */

            } catch (Exception $e) {

                DB::rollback();
                return redirect()->route('compraTT.index')->with('info','Ocurrió un error al intentar eliminar el registro vuelva a intentarlo.');    
            }

            DB::commit();

            return redirect()->route('compraTT.index')->with('info',"Se Eliminó correctamente...");  

        }
        else
        {
            return redirect()->route('compraTT.index')->with('info',"No se puede eliminar porque tiene pagos relacionados.");
        }

        
    }

    

}
