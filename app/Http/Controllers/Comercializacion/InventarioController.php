<?php

namespace App\Http\Controllers\Comercializacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Cliente;
use App\Almacen;
use App\ProductoAlmacen;
use Carbon\Carbon;
use App\Rols_user;
use App\Rols_areas;
use App\Configuracion;
use App\Inventario;
use App\Ingreso;
use App\Detalle_Ingreso;
use App\Res_Stock_Telas;
use App\Res_stock_mp;
use App\Users;
use App\Transferencias;
use App\ventadetalle;
use App\tempVista;
use Illuminate\Support\Facades\Auth;

use DB;

class InventarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {    
        $id_user=access()->user()->id;
        $userX=Users::where("id",$id_user)->first();  
        //$codalm=$userX->rolsareas[0]->areasalmacen->almacen->nAlmCod;
        $tienda=$userX->rolsareas;

        //$tienda=Almacen::all();
        
        $fecha=Carbon::now()->format('Y-m-d');
        $hora=Carbon::now()->format('H:i:s');

        $id="";

        return view("comercializacion.inventario.create",compact("fecha","hora","id","tienda"));       
    }

    public function index(Request $request)
    {    
        $userLogueado = Auth::user();
        $userLogueado =Users::with("rolsareas", "rolsareas.areaAlmacen", "rolsareas.areaAlmacen.almacen")->find($userLogueado->id);
        $rolsAreas = $userLogueado->rolsareas;

        $codAlmacen = "";
        if (isset($rolsAreas[0])) {
            $areaAlmacen = $rolsAreas[0]->areaAlmacen;
            if (!is_null($areaAlmacen)) {
                $almacen = $areaAlmacen->almacen;
                if (!is_null($almacen)) {
                    $codAlmacen = $almacen->nAlmCod;
                }
            }
        }
        $cajaap=Inventario::where("nAlmCod","like","%".$request->tien."%")
        ->where("dInvFecHor","like","%".$request->fech."%")->orderBy("cInvCod","DESC");

        if ($codAlmacen !="") {
            $cajaap = $cajaap->where("nAlmCod", $codAlmacen);
        }
        $cajaap = $cajaap->paginate(10);
        $tiendas=Almacen::all();
        return view("comercializacion.inventario.index",compact("cajaap","tiendas"));      
    }

    public function productoalm(Request $request,$id)
    {
        if($request->ajax())
        {           
            $results = array();

            //$productos=ProductoAlmacen::where("nAlmCod",$almacen1->nAlmCod)->get();
            $prod_x=DB::table("productoalmacens")
            ->Join("productos","productoalmacens.cProdCod","=","productos.id")            
            ->Join("color","productoalmacens.id_color","=","color.id")          
            ->where('productoalmacens.nAlmCod','=',$id)            
            ->select("color.nombre","productos.nombre_generico","productos.nombre_especifico","productoalmacens.nProAlmCod","productoalmacens.nProdAlmStock","productos.id",DB::raw("color.id as codcolor"))
            ->distinct()
            ->get();

            foreach ($prod_x as $palm)
            {
                $results[] = [ 
                    'nProAlmCod' => $palm->nProAlmCod, 
                    'value' => $palm->nombre_generico.' '.$palm->nombre, 
                    'nombre_generico' =>  $palm->nombre_generico, 
                    "nombre_especifico" => $palm->nombre_especifico, 
                    "nProdAlmStock" => $palm->nProdAlmStock, 
                    "id" => $palm->id."_".$palm->codcolor, 
                    "tc" => "0"];
            }
            
            /*$queries = DB::table("resumen_stock_telas")
            ->Join("productos","resumen_stock_telas.producto_id","=","productos.id")            
            ->select("productos.nombre_generico","productos.nombre_especifico","resumen_stock_telas.producto_id","resumen_stock_telas.id","resumen_stock_telas.cantidad","resumen_stock_telas.rollos","resumen_stock_telas.nro_lote","resumen_stock_telas.producto_id")
            //->where("productos.nombre_generico",'like',"%".$busca."%")
            ->whereNull('resumen_stock_telas.deleted_at')
            ->distinct()
            //->take(5)
            ->get();
            
            foreach ($queries as $query)
            {
                $results[] = [ 
                    'nProAlmCod' => "TC".$query->id, 
                    'value' => $query->nombre_generico.' - TC | '.strtoupper($query->nro_lote), 
                    'nombre_generico' =>  $query->nombre_generico, 
                    "nombre_especifico" => $query->nombre_especifico, 
                    "nProdAlmStock" => (("TELAS" == "TELAS")?$query->cantidad:$query->rollos), 
                    "id" => "TC".$query->producto_id."_".$query->nro_lote, 
                    "tc" => "1"];
            }

            $queries3 = DB::table("resumen_stock_materiaprima")
            ->Join("insumos","resumen_stock_materiaprima.insumo_id","=","insumos.id")            
            ->select("insumos.nombre_generico","insumos.nombre_especifico","resumen_stock_materiaprima.id","resumen_stock_materiaprima.cantidad","resumen_stock_materiaprima.insumo_id","resumen_stock_materiaprima.peso_neto")
            //->where("insumos.nombre_generico",'like',"%".$busca."%")
            ->where("resumen_stock_materiaprima.insumo_id",'<>',"0")
            ->whereNull('resumen_stock_materiaprima.deleted_at')
            ->distinct()
            //->take(5)
            ->get();
            
            foreach ($queries3 as $query3)
            {
                $results[] = [ 
                    'nProAlmCod' => 'MP'.$query3->id, 
                    'value' => $query3->nombre_generico.' (I)', 
                    'nombre_generico' =>  $query3->nombre_generico, 
                    "nombre_especifico" => $query3->nombre_especifico, 
                    "nProdAlmStock" => $query3->peso_neto,                    
                    "id" => "I".$query3->insumo_id, 
                    "tc" => "0"];
            }

            $queries4 = DB::table("resumen_stock_materiaprima")
            ->Join("accesorios","resumen_stock_materiaprima.accesorio_id","=","accesorios.id")            
            ->select("accesorios.nombre","resumen_stock_materiaprima.id","resumen_stock_materiaprima.cantidad","resumen_stock_materiaprima.accesorio_id")
            //->where("accesorios.nombre",'like',"%".$busca."%")
            ->where("resumen_stock_materiaprima.accesorio_id",'<>',"0")
            ->whereNull('resumen_stock_materiaprima.deleted_at')
            ->distinct()
            //->take(5)
            ->get();
            
            foreach ($queries4 as $query4)
            {
                $results[] = [ 
                    'nProAlmCod' => 'MP'.$query4->id, 
                    'value' => $query4->nombre.' (A)', 
                    'nombre_generico' =>  $query4->nombre, 
                    "nombre_especifico" => '', 
                    "nProdAlmStock" => $query4->cantidad,
                    "id" => "A".$query4->accesorio_id, 
                    "tc" => "0"];
            }*/

            return response()->json($results);          
        }
    }

    function autocomplete_producto($busca,$id)
    {    
        //$term = Input::get('term');
        
        $array = explode(" ", trim($busca));
        $cad='%';
        foreach ($array as $key => $value) {
            $cad.=$value.'%';
        }


        $results = array();
        
        $queries = DB::table("productoalmacens")
        ->Join("productos","productoalmacens.cProdCod","=","productos.id")            
        ->Join("color","productoalmacens.id_color","=","color.id")                             
        ->select("color.nombre","productos.nombre_generico","productos.nombre_especifico","productos.id","productoalmacens.nProAlmCod",DB::raw("color.id as codcolor"))
        ->where('productoalmacens.nAlmCod','=',$id) 
        ->where(DB::raw("concat(productos.nombre_generico,' ', color.nombre)"),'like',$cad)
        ->distinct()
        ->take(10)->get();
        
        foreach ($queries as $query)
        {
            $results[] = [/* 'nProAlmCod' => $query->nProAlmCod, */'value' => $query->nombre_generico.' '.$query->nombre, 'nombre' => $query->nombre, 'nombre_generico' =>  $query->nombre_generico, "nombre_especifico" => $query->nombre_especifico, "codipro" => $query->id."_".$query->codcolor];
        }

        /*$queries = DB::table("resumen_stock_telas")
        ->Join("productos","resumen_stock_telas.producto_id","=","productos.id")            
        ->select("productos.nombre_generico","productos.nombre_especifico","resumen_stock_telas.producto_id","resumen_stock_telas.nro_lote")
        ->where("productos.nombre_generico",'like',$cad)
        ->whereNull('resumen_stock_telas.deleted_at')
        ->distinct()
        ->take(10)
        ->get();
        
        foreach ($queries as $query)
        {
            $results[] = [ /*'id' => $query->producto_id,*/ /*
            'value' => $query->nombre_generico.' - TC | '.strtoupper($query->nro_lote), 
            'nombre' => '', 
            'nombre_generico' =>  $query->nombre_generico, 
            "nombre_especifico" => $query->nombre_especifico, 
            "codipro" => "TC".$query->producto_id."_".$query->nro_lote];
        }

        $queries3 = DB::table("resumen_stock_materiaprima")
        ->Join("insumos","resumen_stock_materiaprima.insumo_id","=","insumos.id")            
        ->select("insumos.nombre_generico","insumos.nombre_especifico","resumen_stock_materiaprima.id","resumen_stock_materiaprima.cantidad","resumen_stock_materiaprima.insumo_id","resumen_stock_materiaprima.peso_neto")
        ->where("insumos.nombre_generico",'like',$cad)
        ->where("resumen_stock_materiaprima.insumo_id",'<>',"0")
        ->whereNull('resumen_stock_materiaprima.deleted_at')
        ->distinct()
        ->take(10)
        ->get();
        
        foreach ($queries3 as $query3)
        {
            $results[] = [ 
            /*'nProAlmCod' => 'MP'.$query3->id,*//*
            'value' => $query3->nombre_generico.' (I)', 
            'nombre' => '', 
            'nombre_generico' =>  $query3->nombre_generico, 
            "nombre_especifico" => $query3->nombre_especifico,             
            "codipro" => "I".$query3->insumo_id];
        }

        $queries4 = DB::table("resumen_stock_materiaprima")
        ->Join("accesorios","resumen_stock_materiaprima.accesorio_id","=","accesorios.id")            
        ->select("accesorios.nombre","resumen_stock_materiaprima.id","resumen_stock_materiaprima.cantidad","resumen_stock_materiaprima.accesorio_id")
        ->where("accesorios.nombre",'like',$cad)
        ->where("resumen_stock_materiaprima.accesorio_id",'<>',"0")
        ->whereNull('resumen_stock_materiaprima.deleted_at')
        ->distinct()
        ->take(10)
        ->get();
        
        foreach ($queries4 as $query4)
        {
            $results[] = [ 
            /*'nProAlmCod' => 'MP'.$query4->id,*//*
            'value' => $query4->nombre.' (A)', 
            'nombre' => '', 
            'nombre_generico' =>  $query4->nombre, 
            "nombre_especifico" => '',             
            "codipro" => "A".$query4->accesorio_id];
        }*/

        return response()->json($results);           
    }

    public function veri_codbarra($id)
    {        

        $datos=DB::table('detalle_nota_ingreso')
        ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso.nProAlmCod")
        ->leftJoin("productos","productos.id","=","productoalmacens.cProdCod")
        ->leftJoin("color","color.id","=","productoalmacens.id_color")
        ->where(DB::raw("REPLACE(detalle_nota_ingreso.cod_barras,'-','')"),"=",$id)
        ->orWhere("detalle_nota_ingreso.cod_barras","=",$id)
        //->where("detalle_nota_ingreso.cod_barras","=",$id)          
        ->select("productoalmacens.nProAlmCod","productos.nombre_generico","productos.nombre_especifico","color.nombre","productoalmacens.nProdAlmStock","detalle_nota_ingreso.peso_cant","detalle_nota_ingreso.rollo","productos.id",DB::raw("color.id as codcolor"))
            //,"productoalmacens.nAlmCod","productoalmacens.nProdAlmPuni")
        ->limit(1)->get();        
        $dat=json_decode($datos, true);
        if(empty($dat))
        {   
            $datos=DB::table('detalle_nota_ingreso_a')
            ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso_a.nProAlmCod")
            ->leftJoin("productos","productos.id","=","productoalmacens.cProdCod")
            ->leftJoin("color","color.id","=","productoalmacens.id_color")         
            ->where(DB::raw("REPLACE(detalle_nota_ingreso_a.cod_barras,'-','')"),"=",$id)
            ->orWhere("detalle_nota_ingreso_a.cod_barras","=",$id)
            //->where("detalle_nota_ingreso_a.cod_barras","=",$id)            
            ->select("productoalmacens.nProAlmCod","productos.nombre_generico","productos.nombre_especifico","color.nombre","productoalmacens.nProdAlmStock","detalle_nota_ingreso_a.peso_cant","detalle_nota_ingreso_a.rollo","productos.id",DB::raw("color.id as codcolor"))
                //,"productoalmacens.nAlmCod","productoalmacens.nProdAlmPuni")
            ->limit(1)->get(); 
        }

        return $datos;
    }

    public function vistaprevia_store(Request $request)
    {        
        DB::beginTransaction();

        try 
        {               
            $fechahora=Carbon::now()->format('Y-m-d h:i:s');
            $id_user=access()->user()->id;

            $tvista=new tempVista;
            $tvista->codigo=$request->codigo;
            $tvista->user=$id_user;
            $tvista->fechaCreacion=$fechahora;
      
            $tvista->save();
        }
        catch (Exception $e) 
        {
            DB::rollback();
            return 0;
        }

        DB::commit();

        return 1;
    }

    public function reportevistaprevia()
    {
        $id_user=access()->user()->id;
        $datos=tempVista::where("user",$id_user)->orderBy("codTempP","desc")->first();        
        $view =  \View::make('comercializacion.inventario.vp',compact("datos"))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4', 'landscape');
        return $pdf->stream('invoice');
    }

    public function store(Request $request)
    {
        DB::beginTransaction();

        try 
        {   
            $trans_ultimo = Inventario::select('cInvCod')
                ->orderBy('cInvCod',"desc")                      
                ->get()->first();

            $conn=0;
            $dat="";
            if($trans_ultimo!="")
            {
                $dat=$trans_ultimo->cInvCod;       
                $conn=substr($dat, 2);
            }        
            //return $conn;
            $cad="I-";
            for($i=strlen(++$conn);$i<8;$i++)
            {
                $cad.="0";
            }
           
            $cad.= $conn;

            $fechahora=Carbon::now()->format('Y-m-d H:i:s');
        
            $inv=new Inventario;
            $inv->cInvCod=$cad;
            $inv->dInvFecHor=$fechahora;
            $inv->nAlmCod=$request["tienda_fija"];
      
            $inv->save();

            for($i=1;$i<=$request->conta;$i++)
            {
                if($request["cod_ndi_".$i]!="")
                {
                    if($request["cod_ndi_".$i]=="0")//si es diferente no se ingresa
                    {
                        $ing=new Ingreso;    

                        if(substr($request["produ_".$i],0,2)=="TC")
                        {
                            $ing->nCodTC=substr($request["produ_".$i],2);    
                        }
                        else if(substr($request["produ_".$i],0,2)=="MP")
                        {
                            $ing->nCodMP=substr($request["produ_".$i],2);    
                        }
                        else
                        {
                            $ing->nProAlmCod=$request["produ_".$i];                             
                        }
                        
                        $ing->cInvCod=$cad;
                        $ing->nStockLogTotal=$request["canti_".$i];                     
                        $ing->nStockFisTotal=$request["codx_fisi_".$request["valumaster_".$i]];
                        $valor_x=0;
                        /*if(substr($request["produ_".$i],0,2)=="TC")
                        {
                             $queries = DB::table("ventas")
                            ->Join("detalleventa","detalleventa.nVtaCod","=","ventas.nVtaCod")            
                            ->select(DB::raw("sum(detalleventa.nVtaPeso) as peso"),DB::raw("sum(detalleventa.nVtaCant) as rollo"))
                            ->where(DB::raw("DATE(dVFacFemi)"),'=',DB::raw("CURDATE()"))
                            ->where('detalleventa.nCodTC','=',substr($request["produ_".$i],2))
                            ->first();

                            $tipo=Res_Stock_Telas::where('id',"=",substr($request["produ_".$i],2))->first();
                            
                           
                                $valor_x=$queries->peso;           
                            
                        } 
                        else if(substr($request["produ_".$i],0,2)=="MP")
                        {
                             $queries = DB::table("ventas")
                            ->Join("detalleventa","detalleventa.nVtaCod","=","ventas.nVtaCod")            
                            ->select(DB::raw("sum(detalleventa.nVtaPeso) as peso"),DB::raw("sum(detalleventa.nVtaCant) as rollo"))
                            ->where(DB::raw("DATE(dVFacFemi)"),'=',DB::raw("CURDATE()"))
                            ->where('detalleventa.nCodMP','=',substr($request["produ_".$i],2))
                            ->first();

                            $tipo=Res_stock_mp::where('id',"=",substr($request["produ_".$i],2))->first();
                            
                            if("TELAS"=="TELAS")
                                $valor_x=$queries->peso;           
                            else
                                $valor_x=$queries->rollo;
                        }                               
                        else
                        {
                            $queries = DB::table("ventas")
                            ->Join("detalleventa","detalleventa.nVtaCod","=","ventas.nVtaCod")            
                            ->select(DB::raw("sum(detalleventa.nVtaPeso) as peso"),DB::raw("sum(detalleventa.nVtaCant) as rollo"))
                            ->where(DB::raw("DATE(dVFacFemi)"),'=',DB::raw("CURDATE()"))
                            ->where('detalleventa.nProAlmCod','=',$request["produ_".$i])
                            ->first();
                            
                            $tipo=ProductoAlmacen::where('nProAlmCod',"=",$request["produ_".$i])->first();
                            
                            if(strtoupper(trim($tipo->producto->nombre_especifico))=="TELAS")                            
                                $valor_x=$queries->peso;           
                            else
                                $valor_x=$queries->rollo;              
                        }
                        */
                        $ing->nVentaHoy=$valor_x;


                        $ing->save();   

                        $coding=Ingreso::select("nIngCod")->orderBy("nIngCod","desc")->first();

                        for($j=1;$j<=$request->conta2;$j++)
                        {
                            if($request["cod_ndidet_".$j]!="")
                            {
                                if($request["cod_ndidet_".$j]=="0")
                                {
                                    if($request["valu_".$j]==$request["valumaster_".$i])
                                    {
                                        $detingreso=new Detalle_Ingreso;                                          
                                        $detingreso->nIngCod=$coding->nIngCod;
                                        $detingreso->cCodBarra=$request["codb_".$j];                        
                                        $detingreso->cDetalle=$request["produdeta_".$j];
                                        $detingreso->dPesoUni=$request["cantipes_".$j];
                                        $detingreso->dCanUni=$request["cantixxx_".$j];
                                                            
                                        $detingreso->save();                      
                                    }
                                }
                                else
                                {
                                   
                                }
                            }//dif empty
                        } //end for  

                        if(substr($request["produ_".$i],0,2)=="TC")
                        {
                            $tipo_xx=Res_Stock_Telas::where('id',"=",substr($request["produ_".$i],2))->first();

                            //if(strtoupper(trim($tipo_xx->producto->nombre_especifico))=="TELAS")
                            //{
                                Res_Stock_Telas::where('id',"=",substr($request["produ_".$i],2))
                                ->update([
                                'cantidad' => $request["codx_fisi_".$request["valumaster_".$i]],
                                'rollos' => $request["codx_fisi_".$request["valumaster_".$i]]/20
                                ]);
                           /* }
                            else
                            {
                                Res_Stock_Telas::where('id',"=",substr($request["produ_".$i],2))
                                ->update([
                                'rollos' => $request["codx_fisi_".$request["valumaster_".$i]]
                                ]);
                            }  */
                        }
                        else if(substr($request["produ_".$i],0,2)=="MP")
                        {
                            //----- Actualizar stock --------------------
                            Res_stock_mp::where('id',"=",substr($request["produ_".$i],2))
                            ->update([
                            'peso_neto' => $request["codx_fisi_".$request["valumaster_".$i]],
                            'cantidad' => ($request["codx_fisi_".$request["valumaster_".$i]])/20
                            ]);                
                            //----- Fin de actualización de stock --------    
                        }
                        else
                        {
                            //----- Actualizar stock --------------------
                            ProductoAlmacen::where('nProAlmCod',"=",$request["produ_".$i])
                            ->update([
                                'nProdAlmStock' => $request["codx_fisi_".$request["valumaster_".$i]]
                                ]);                  
                            //----- Fin de actualización de stock --------    
                        }
                    }
                    else
                    {
                       
                    }
                }//dif empty
            } //end for
        } 
        catch (Exception $e) 
        {

            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return redirect()->route('inventario.index')->with('Inf: ', "Se registró correctamente...");

        //return redirect()->route('venta.reporte',$cad)->with('info','La venta se registró correctamente.');
    }

    public function reportecierre($id)
    {
        $datos=Inventario::where("cInvCod", $id)->first();
        $codAlmacen = $datos->nAlmCod;
        //obtenemos los dos ultimos inventarios
        $inventarios = Inventario::where("nAlmCod", $codAlmacen)
            ->where("dInvFecHor", "<=", $datos->dInvFecHor)
            ->orderBy("dInvFecHor", "DESC")
            ->limit(2)->get();
        $fechas = [];
        if (count($inventarios) == 2) {
            $fecha[0] = $inventarios[1]->dInvFecHor;
            $fecha[1] = $inventarios[0]->dInvFecHor;
            foreach ($datos->ingreso as $key => $value) {
                if (isset($inventarios[1]->ingreso[$key])) {
                    $datos->ingreso[$key]->inicialAntes = $inventarios[1]->ingreso[$key]->nStockFisTotal;
                } else {
                    $datos->ingreso[$key]->inicialAntes = 0;
                }
                
            }
        }

        if (count($inventarios) == 1) {
            $fecha[0] = $inventarios[0]->dInvFecHor;
            foreach ($datos->ingreso as $key => $value) {
                if (!is_null($datos->ingreso[$key])) {
                    if (isset($datos->ingreso[$key])) {
                        $datos->ingreso[$key]->inicialAntes = 0;
                    }
                }
            }
        }

        $trans_e=Transferencias::where("nAlmCod1", $datos->nAlmCod)->whereRaw("deleted_at IS NULL");
        if (count($fecha) == 1) {
            $trans_e->whereRaw("created_at <= '{$fecha[0]}' ");
        }
        if (count($fecha) == 2) {
            $trans_e->whereBetween("created_at", $fecha);
        }
        $trans_e = $trans_e->get();

        $venta_e=DB::table('detalleventa')
            ->leftJoin("productoalmacens", "productoalmacens.nProAlmCod", "=", "detalleventa.nProAlmCod")
            ->leftJoin("ventas", "ventas.nVtaCod", "=", "detalleventa.nVtaCod")
            ->where("productoalmacens.nAlmCod", "=", $datos->nAlmCod)
            ->whereRaw("dFecElim IS NULL")
            ->select("detalleventa.nDetVtaCod", "productoalmacens.nProAlmCod", "detalleventa.nVtaPeso");
        if (count($fecha) == 1) {
            $venta_e->where("ventas.dFecAct", "<=", $fecha[0]);
        }
        if (count($fecha) == 2) {
            $venta_e->whereBetween("ventas.dFecAct", $fecha);
        }
        $venta_e = $venta_e->get();
      //  echo "<pre>";
    //    print_r($fecha);
  //      echo "</pre>";
//dd();
        $detVentDEv_e=DB::table('detalleventadev')
        ->leftJoin("productoalmacens", "productoalmacens.nProAlmCod", "=", "detalleventadev.nProAlmCod")
        ->leftJoin("devolucion", "devolucion.cDevCod", "=", "detalleventadev.cDevCod")
        ->where("productoalmacens.nAlmCod", "=", $datos->nAlmCod)
        ->whereRaw("devolucion.deleted_at IS NULL")
        ->select("detalleventadev.nDetVtaDevCod", "productoalmacens.nProAlmCod", "detalleventadev.nVtaDevPeso");
        if (count($fecha) == 1) {
            $detVentDEv_e->whereRaw("created_at <= '{$fecha[0]}' ");
        }
        if (count($fecha) == 2) {
            $detVentDEv_e->whereBetween("created_at", $fecha);
        }
        $detVentDEv_e =  $detVentDEv_e->get();

        $trans_i=Transferencias::where("nAlmCod2", $datos->nAlmCod)
            ->whereRaw("deleted_at IS NULL");
        if (count($fecha) == 1) {
            $trans_i->whereRaw("created_at <= '{$fecha[0]}' ");
        }
        if (count($fecha) == 2) {
            $trans_i->whereBetween("created_at", $fecha);
        }
        $trans_i = $trans_i->get();
    
        $ntaing_i=DB::table('detalle_nota_ingreso')
        ->leftJoin("productoalmacens", "productoalmacens.nProAlmCod", "=", "detalle_nota_ingreso.nProAlmCod")
        ->where("productoalmacens.nAlmCod", "=", $datos->nAlmCod)
        ->select("detalle_nota_ingreso.ning_id", "productoalmacens.nProAlmCod", "detalle_nota_ingreso.peso_cant", "detalle_nota_ingreso.cod_barras");
        if (count($fecha) == 1) {
            $ntaing_i->whereRaw("fecha <= '{$fecha[0]}' ");
        }
        if (count($fecha) == 2) {
            $ntaing_i->whereBetween("fecha", $fecha);
        }
        $ntaing_i = $ntaing_i->get();
        $codBarras = [];
        foreach ($ntaing_i as $key => $value) {
            $codBarras[$key] = $value->cod_barras;
        }
        $transTemp = DB::table("transferencia_detalle")
            ->whereIn("cod_barras", $codBarras)
            ->pluck("cod_barras", "cod_barras");

        foreach ($ntaing_i as $key => $value) {
            if (isset($transTemp[$value->cod_barras])) {
                unset($ntaing_i[$key]);
            }
        }
        // recorremos las trasnferencias de egreso para sacar las notas de ingreso que han salido y se han perdido traza
        $almacenesDestino = [];
        $almacenesOrigen = [];
        foreach ($trans_i as $key => $value) {
            $almacenesDestino[$value->nAlmCod2] = $value->nAlmCod2;
        }
        foreach ($trans_e as $key => $value) {
            $almacenesOrigen[$value->nAlmCod1] = $value->nAlmCod1;
        }
        $notaIngresoTrasnferidas =DB::table('detalle_nota_ingreso')
        ->leftJoin("productoalmacens", "productoalmacens.nProAlmCod", "=", "detalle_nota_ingreso.nProAlmCod")
        ->leftJoin("transferencia_detalle", "transferencia_detalle.cod_barras", "=", "detalle_nota_ingreso.cod_barras")
        ->leftJoin("transferencia", "transferencia.cTraCod", "=", "transferencia_detalle.cTraCod")
        ->whereIn("productoalmacens.nAlmCod", $almacenesDestino)
        ->whereIn("transferencia.nAlmCod2", $almacenesDestino)
        ->where("detalle_nota_ingreso.cod_barras", "<>", "")
        ->where("detalle_nota_ingreso.nProAlmCod", "=", "transferencia_detalle.nProAlmCod")
        ->select("detalle_nota_ingreso.ning_id", "transferencia_detalle.nProAlmCod", "detalle_nota_ingreso.peso_cant", "detalle_nota_ingreso.cod_barras");
        if (count($fecha) == 1) {
            $notaIngresoTrasnferidas->whereRaw("fecha <= '{$fecha[0]}' ");
        }
        if (count($fecha) == 2) {
            $notaIngresoTrasnferidas->whereBetween("fecha", $fecha);
        }
        $notaIngresoTrasnferidas = $notaIngresoTrasnferidas->get();
        foreach ($notaIngresoTrasnferidas as $key => $value) {
            $ntaing_i[] = $value;
        }

        $notaIngresoTrasnferidas =DB::table('detalle_nota_ingreso')
        ->leftJoin("productoalmacens", "productoalmacens.nProAlmCod", "=", "detalle_nota_ingreso.nProAlmCod")
        ->leftJoin("transferencia_detalle", "transferencia_detalle.cod_barras", "=", "detalle_nota_ingreso.cod_barras")
        ->leftJoin("transferencia", "transferencia.cTraCod", "=", "transferencia_detalle.cTraCod")
        ->whereIn("transferencia.nAlmCod1", $almacenesOrigen)
        ->where("detalle_nota_ingreso.cod_barras", "<>", "")
        ->where("detalle_nota_ingreso.nProAlmCod", "<>", "transferencia_detalle.nProAlmCod")
        ->select("detalle_nota_ingreso.ning_id", "transferencia_detalle.nProAlmCod", "detalle_nota_ingreso.peso_cant", "detalle_nota_ingreso.cod_barras");
        if (count($fecha) == 1) {
            $notaIngresoTrasnferidas->whereRaw("fecha <= '{$fecha[0]}' ");
        }
        if (count($fecha) == 2) {
            $notaIngresoTrasnferidas->whereBetween("fecha", $fecha);
        }
        $notaIngresoTrasnferidas = $notaIngresoTrasnferidas->get();
        foreach ($notaIngresoTrasnferidas as $key => $value) {
            $ntaing_i[] = $value;
        }

        $ntainga_i=DB::table('detalle_nota_ingreso_a')
        ->leftJoin("productoalmacens", "productoalmacens.nProAlmCod", "=", "detalle_nota_ingreso_a.nProAlmCod")
        ->Where("productoalmacens.nAlmCod", "=", $datos->nAlmCod)
        ->select("detalle_nota_ingreso_a.dNotInga_id", "productoalmacens.nProAlmCod", "detalle_nota_ingreso_a.peso_cant", "detalle_nota_ingreso_a.cod_barras");
        if (count($fecha) == 1) {
            $ntainga_i->whereRaw("fecha <= '{$fecha[0]}' ");
        }
        if (count($fecha) == 2) {
            $ntainga_i->whereBetween("fecha", $fecha);
        }
        $ntainga_i = $ntainga_i->get();

        $notaIngresoTrasnferidasA =DB::table('detalle_nota_ingreso_a')
        ->leftJoin("productoalmacens", "productoalmacens.nProAlmCod", "=", "detalle_nota_ingreso_a.nProAlmCod")
        ->leftJoin("transferencia_detalle", "transferencia_detalle.cod_barras", "=", "detalle_nota_ingreso_a.cod_barras")
        ->leftJoin("transferencia", "transferencia.cTraCod", "=", "transferencia_detalle.cTraCod")
        ->whereIn("transferencia.nAlmCod1", $almacenesOrigen)
        ->where("detalle_nota_ingreso_a.cod_barras", "<>", "")
        ->where("detalle_nota_ingreso_a.nProAlmCod", "<>", "transferencia_detalle.nProAlmCod")
        ->select("detalle_nota_ingreso_a.ninga_id", "transferencia_detalle.nProAlmCod", "detalle_nota_ingreso_a.peso_cant", "detalle_nota_ingreso_a.cod_barras");
        if (count($fecha) == 1) {
            $notaIngresoTrasnferidasA->whereRaw("fecha <= '{$fecha[0]}' ");
        }
        if (count($fecha) == 2) {
            $notaIngresoTrasnferidasA->whereBetween("fecha", $fecha);
        }
        $notaIngresoTrasnferidasA = $notaIngresoTrasnferidasA->get();
        foreach ($notaIngresoTrasnferidasA as $key => $value) {
            $ntainga_i[] = $value;
        }

        $notaIngresoTrasnferidasA =DB::table('detalle_nota_ingreso_a')
        ->leftJoin("productoalmacens", "productoalmacens.nProAlmCod", "=", "detalle_nota_ingreso_a.nProAlmCod")
        ->leftJoin("transferencia_detalle", "transferencia_detalle.cod_barras", "=", "detalle_nota_ingreso_a.cod_barras")
        ->leftJoin("transferencia", "transferencia.cTraCod", "=", "transferencia_detalle.cTraCod")
        ->whereIn("productoalmacens.nAlmCod", $almacenesDestino)
        ->whereIn("transferencia.nAlmCod2", $almacenesDestino)
        ->where("detalle_nota_ingreso_a.cod_barras", "<>", "")
        ->select("detalle_nota_ingreso_a.ninga_id", "transferencia_detalle.nProAlmCod", "detalle_nota_ingreso_a.peso_cant", "detalle_nota_ingreso_a.cod_barras");
        if (count($fecha) == 1) {
            $notaIngresoTrasnferidasA->whereRaw("fecha <= '{$fecha[0]}' ");
        }
        if (count($fecha) == 2) {
            $notaIngresoTrasnferidasA->whereBetween("fecha", $fecha);
        }
        $notaIngresoTrasnferidasA = $notaIngresoTrasnferidasA->get();
        foreach ($notaIngresoTrasnferidasA as $key => $value) {
            $ntainga_i[] = $value;
        }
        $codBarras = [];
        foreach ($trans_i as $key => $value) {
            foreach ($value->transferenciadetalle as $key2 => $value2) {
            $codBarras[$value2->cod_barras] = $value2->cod_barras;
            }
        }
        /*$transTemp = DB::table("transferencia_detalle")
            ->whereIn("cod_barras", $codBarras)
            ->pluck("cod_barras", "cod_barras");*/
            
        foreach ($ntainga_i as $key => $value) {
            if (isset($codBarras[$value->cod_barras])) {
                unset($ntainga_i[$key]);
            }
        }

        $prodev_i=DB::table('productodevolver')
        ->leftJoin("productoalmacens", "productoalmacens.nProAlmCod", "=", "productodevolver.nProAlmCod")
        ->leftJoin("devolucion", "devolucion.cDevCod", "=", "productodevolver.cDevCod")
        ->Where("productoalmacens.nAlmCod", "=", $datos->nAlmCod)
        ->where("cEstPro", "NDPB")
        ->select("productodevolver.cProdDevCod", "productoalmacens.nProAlmCod", "productodevolver.nDevPeso");
        if (count($fecha) == 1) {
            $prodev_i->whereRaw("devolucion.created_at <= '{$fecha[0]}' ");
        }
        if (count($fecha) == 2) {
            $prodev_i->whereBetween("devolucion.created_at", $fecha);
        }
        $prodev_i = $prodev_i->get();
        //$trans_de=ventadetalle::where("nProAlmCod",$datos->nAlmCod)->get();

        $fecha=Carbon::now()->format('Y-m-d H:i:s');
        $view =  \View::make(
            'comercializacion.inventario.rpt_ci',
            compact("datos", "fecha", "trans_e", "trans_i", "ntaing_i", "ntainga_i", "venta_e", "detVentDEv_e", "prodev_i")
        )->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4', 'landscape');
        return $pdf->stream('invoice');
        /*
            SELECT DATE(`dVFacFemi`),sum(detalleventa.nVtaPeso  )  FROM `ventas` 
            inner join detalleventa on detalleventa.nVtaCod=ventas.nVtaCod
            WHERE DATE(`dVFacFemi`)= CURDATE()
        */
    }

    public function detalleingreso($id)
    {
        $datos0=Inventario::where("cInvCod", $id)->first();

        $datos=DB::table('ingreso')->select("detalle_ingreso.cCodBarra","detalle_ingreso.cDetalle","detalle_ingreso.dPesoUni","detalle_ingreso.dCanUni")
        ->join("detalle_ingreso","ingreso.nIngCod","=","detalle_ingreso.nIngCod")
        ->where("ingreso.cInvCod",$id)->get();
        //return compact("datos");

        $fecha=Carbon::now()->format('Y-m-d h:i:s');

        $view =  \View::make('comercializacion.inventario.rpt_di',compact("datos","datos0","fecha"))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4', 'potrait');
        return $pdf->stream('invoice');
    }

}
