<?php

namespace App\Http\Controllers\Comercializacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Almacen;
use App\ProductoAlmacen;
use App\Areas_almacen;
use App\Areas;
use App\Rols_areas;

use DB;

class Areaalmacen extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$almacens=Almacen::orderBy("nAlmCod","ASC")->paginate(10);
       
    	return view("comercializacion.areaalmacen.index",compact("almacens"));
    }

    public function proalm_store(Request $request,$id)
    {

        $this->validate($request, [
            'area2' => 'required'
        ]);
        
        DB::beginTransaction();

        try 
        {
            $areaalm=new Areas_almacen;
            $areaalm->nAlmCod=$id;
            $areaalm->nAreCod=$request->area2;  
            
            $areaalm->save();        
        } catch (Exception $e) {

            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return redirect()->route('areaalmacen.proalm_create',compact("id"))->with('info','El área fue agredado al almacén satisfactoriamente.');  
    }

    public function proalm_create(Request $request,$id)
    {
        //$productos=Producto::all();

    	$areas=Areas_almacen::where("nAlmCod","=",$id)->get();
    	$areas2=Areas::whereNotIn("nAreCod",Areas_almacen::select("nAreCod")->where("nAlmCod","=",$id)->get())->get();

        $areaalm=Areas_almacen::where("nAlmCod","=",$id)->Where("nAreCod","like","%".$request->codbus."%")->paginate(5);
        
        $nombrealmacen=Almacen::where("nAlmCod","=",$id)->first();       

        return view("comercializacion.areaalmacen.proalm_create",compact("areas","areas2","areaalm","id","nombrealmacen"));
    }

    public function proalm_destroy($id)
    {       
        $p1=Areas_almacen::where("nAreAlmCod","=",$id)->first();

        if(Rols_areas::where("nAreAlmCod","=",$id)->exists())                             
             return redirect()->route('areaalmacen.index',$id)->with('info','Error: No se pudo eliminar porque el AREA('.$p1->areas->cAreNom.') está relacionado con algún usuario.');
    
        $prodalm1=Areas_almacen::where("nAreAlmCod","=",$id);
        $prodalm1->delete();     
                
        return redirect()->route('areaalmacen.index',$id)->with('info','El área('.$p1->areas->cAreNom.') fue retirada del almacen.');
    }

    public function detallearea(Request $request,$id)
    {
        if($request->ajax()){
            $prod_x=DB::table("rols_areas_modulos")
            ->Join("modulos","rols_areas_modulos.nCodMod","=","modulos.nCodMod") 
            ->where('rols_areas_modulos.nAreCod','=',$id)            
            ->select("modulos.cNomMod","modulos.cDesMod","modulos.url")
            ->distinct()
            ->get();

            return response()->json($prod_x);           
        }
    }


}
