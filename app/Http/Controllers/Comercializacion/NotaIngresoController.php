<?php

namespace App\Http\Controllers\Comercializacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ProductoAlmacen;
use App\Almacen;
use App\NotaIngreso;
use App\DetalleNotaIngreso;
use Carbon\Carbon;
use DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use \Milon\Barcode\DNS1D;
use \Milon\Barcode\DNS2D;
use App\tmpimpresion1;
use App\Rols_areas;
use App\Users;

class NotaIngresoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
/*
    public function index()
    {
    	$provedors=provedor::orderBy("nProvCod","ASC")->paginate();
    	return view("provedor.index",compact("provedors"));
    }
*/
    public function store(Request $request)
    {
        
        DB::beginTransaction();

        try {
        /*DB::transaction(function()
        {*/
            /************************ ELMINACION *******************************/
            //eliminamos los que esten en codigos a eliminar
                //verificamos si el codigo esta en la bd,par eliminarlo, sino esta el codigo fue alterado por el navegador, sólo se puede eliminar uno de los registro que se han traido(dentro del filtro pasamos el codigo qeu jala todo)
                        //echo "<pre>";
                //print_r($request->all());
                //print_r($arrayElim);
            //echo "</pre>";
            //dd();
            $arrayElim=explode(",", $request->eliminados);
            for ($j=1; $j < count($arrayElim); $j++) {
                $datos=DetalleNotaIngreso::where('dNotIng_id','=',$arrayElim[$j])->first();  

                /*********** inicio actualizar stock ******************/
                if (!is_null($datos)) {
                    $stock=ProductoAlmacen::where('nProAlmCod',"=",$datos->nProAlmCod)->first();
                  
                    $total=0;        
                    if($request->producto_t_e=="TELAS")                            
                        $total=$stock->nProdAlmStock - $datos->peso_cant;                                                           
                    else
                        $total=$stock->nProdAlmStock - $datos->rollo;  

                    if($total<0){
                        DB::rollback();
                        return redirect()->back()->with('info','Error: No se pudo eliminar los registros, porque El almacén tiene de stock: '.$stock->nProdAlmStock.", por lo tanto sólo se puede retirar esa cantidad como máximo, lo resto ya se vendió.");  
                    }
                

                    ProductoAlmacen::where('nProAlmCod',"=",$datos->nProAlmCod)->update(['nProdAlmStock' => $total]);
                }


                DetalleNotaIngreso::where('dNotIng_id','=',$arrayElim[$j])->delete();  
                /*********** fin actualizar stock ******************/ 
            }
            //que me vote las partidas segun codigo interno
            //evaluar si no tiene detalles
            //sino delete
            
            $partidas = NotaIngreso::select('partida','nIng_id')
            ->where('despTint_id','=', $request->codint)                      
            ->get();

            foreach($partidas as $obj)
            {
                $verdetpar = DB::table('detalle_nota_ingreso')
                ->leftJoin('nota_ingreso', 'detalle_nota_ingreso.ning_id', '=', 'nota_ingreso.ning_id')
                ->select('desptint_id')
                ->where('nota_ingreso.desptint_id','=', $request->codint)
                ->where('nota_ingreso.partida','=',$obj->partida)            
                ->get()
                ->first();

                if($verdetpar=="")
                {
                    $deletedRows = NotaIngreso::where('nIng_id','=',$obj->nIng_id)->delete();                                       
                }
            }
            /************************ FIN ELMINACION *******************************/

            for($i=1;$i<=$request->conta;$i++)
            {
                if($request["cod_ndi_".$i]!="")
                {
                    if($request["cod_ndi_".$i]==0)//si es diferente no se ingresa
                    {
                        //Evalua si la partida ya existe 
                        //$nIng_id=NotaIngreso::select('nIng_id')
                        //->where('despTint_id','=',$request->codint)
                        //->where('partida','=',$request["par_".$i])                        
                        //->orderBy('nIng_id','desc')->get()->first();
                        
                        //if($auxi__==0)//si no existe, lo guardo | new requirement: que se agregue si guia, producto son iguales pero no el códgio de producto.
                        //{
                            $NotaIngreso=new NotaIngreso;
                            $NotaIngreso->despTint_id=$request->codint;
                            $NotaIngreso->partida=$request["par_".$i];
                            //$NotaIngreso->fecha=$request["fec_".$i];

                            $NotaIngreso->save();
                        //}
                       
                    }
                }
            }

            $arraycad_actt=explode(",", $request->cad_actt);
            $noimpresos_ = array();

            //Recorrer, buscar el reg de la partida y fecha y registrar
            for($i=1;$i<=$request->conta;$i++)
            {
                if($request["cod_ndi_".$i]!="")
                {
                    if($request["cod_ndi_".$i]==0)//si es diferente no se ingresa
                    {
                        $nIng_id=NotaIngreso::select('nIng_id')
                        ->where('despTint_id','=',$request->codint)
                        ->where('partida','=',$request["par_".$i])
                        //->where('fecha','=',$request["fec_".$i])
                        ->orderBy('nIng_id','desc')->get()->first();
                        
                            $DetalleNotaIngreso=new DetalleNotaIngreso;
                            $DetalleNotaIngreso->nIng_id=$nIng_id->nIng_id;
                            $DetalleNotaIngreso->nProAlmCod=$request["tie_".$i];
                            $DetalleNotaIngreso->cod_barras="";
                            $DetalleNotaIngreso->peso_cant=$request["pes_".$i];
                            $DetalleNotaIngreso->rollo=$request["roll_".$i];
                            $DetalleNotaIngreso->impreso="1";
                            $DetalleNotaIngreso->fecha=$request["fec_".$i];
                            $DetalleNotaIngreso->guia=$request["guiatt_".$i];

                            $DetalleNotaIngreso->save();


                            /*********** inicio actualizar stock ******************/
                            $stock=ProductoAlmacen::where('nProAlmCod',"=",$request["tie_".$i])->first();
                            
                            $total=0;        
                            if($request->producto_t_e=="TELAS")                            
                                $total=$stock->nProdAlmStock + $request["pes_".$i];                                                           
                            else
                                $total=$stock->nProdAlmStock + $request["roll_".$i];                                                           
                            ProductoAlmacen::where('nProAlmCod',"=",$request["tie_".$i])->update(['nProdAlmStock' => $total]);
                            /*********** fin actualizar stock ******************/
                         
                            $codi=DetalleNotaIngreso::select('dNotIng_id')
                            ->orderBy('dNotIng_id','Desc')                      
                            ->get()
                            ->first(); 

                            /**** enumeracion de partidas para codigo de barras */
                            //$num=DetalleNotaIngreso::where('ning_id',$nIng_id->nIng_id)->count(); 
                            $num=DB::table('nota_ingreso')
                                ->Join('detalle_nota_ingreso', 'detalle_nota_ingreso.ning_id', '=', 'nota_ingreso.ning_id')
                                //->where('nota_ingreso.ning_id','=',$nIng_id->nIng_id) 
                                ->where('nota_ingreso.partida','=',$request["par_".$i])            
                                ->where('detalle_nota_ingreso.guia','=',$request["guiatt_".$i])                            
                                ->count();         
                            //return $codi;
                            
                            /*CODIGO DE BARRAS*/

                            //$cb=substr($request["producto_t_e"],0,1) . "-". $request["lote_t"]. "-".$request["par_".$i]. "-" .$num;//$codi->dNotIng_id;
                          
                            //Código de barras
                            /*
                            $cb=substr($request["producto_t_e"],0,1) . "-".$stock->cProdCod. "-". $request["lote_t"]. "-".$request["par_".$i].str_pad($num, 4, "0", STR_PAD_LEFT);
                            */     
                            //return $nIng_id->nIng_id."   ".$request["par_".$i]."   ".$request["guiatt_".$i];
                            $codrandom_bd=DB::table('nota_ingreso')
                                ->Join('detalle_nota_ingreso', 'detalle_nota_ingreso.ning_id', '=', 'nota_ingreso.ning_id')
                                ->select('detalle_nota_ingreso.cod_barras')
                                //->where('nota_ingreso.ning_id','=',$nIng_id->nIng_id) 
                                ->where('nota_ingreso.partida','=',$request["par_".$i])            
                                ->where('detalle_nota_ingreso.guia','=',$request["guiatt_".$i])                            
                                ->first();

                            if($codrandom_bd->cod_barras!="") 
                            {
                                $codrandom=substr($codrandom_bd->cod_barras, 1,10);
                            }
                            else
                                $codrandom=rand(1000000000, 9999999999);
                            
                            $cb=substr($request["producto_t_e"],0,1).$codrandom.str_pad($num, 4, "0", STR_PAD_LEFT);
                            
                            //$codi->dNotIng_id;

                            /* Estructura del código de barras
                             * T-LOTE-{PARTIDA-0001} (TELAS), ok
                             * C-LOTE-{PARTIDA-0001} (CUELLOS), ok
                             * P-LOTE-{PARTIDA-0001} (PUÑOS), ok

                             * Además del código de barras tendremos un campo código (telas) que
                             * es el peso ejemplo: 23.21 kg => 23C21H00
                             * Para el caso de cuellos y puños si estará la cantidad.
                             */
                             
                             $color_for= $request["color_t"];
                             $producto_generico = $request["producto_t"];

                             //echo dd(strpos($request["pes_".$i],'.'));

                             if(substr($request["producto_t_e"],0,1)=='T')
                             {  
                                if(strpos($request["pes_".$i],'.') == false) 
                                {
                                    $peso_for= $request["pes_".$i]."T00B00";
                                }
                                else 
                                {
                                    $peso_for= substr($request["pes_".$i], 0, strpos($request["pes_".$i],'.')) . "T" . substr($request["pes_".$i], strpos($request["pes_".$i],'.')+1). "B00";
                                }
                            }
                            else
                            {
                                $peso_for= $request["pes_".$i];
                            }

                             /* FIN CODIGO DE BARRAS*/
                             DetalleNotaIngreso::where('dNotIng_id',"=",$codi->dNotIng_id)                              
                                ->update(['cod_barras' => $cb]); 

                            if(!isset($request["cbox_".$i])){//agregam los codigos de barra que estan sin marcar
                                array_push($noimpresos_,array($cb, $peso_for,$color_for,$producto_generico,$request["par_".$i]));
                            }
                    }
                    else
                    {
                        $color_for= $request["color_t"];
                        $producto_generico = $request["producto_t"];

                        if(substr($request["producto_t_e"],0,1)=='T')
                        {
                            if(strpos($request["pes_".$i],'.') == false) {$peso_for= $request["pes_".$i]."T00B00";}
                            else {$peso_for= substr($request["pes_".$i], 0, strpos($request["pes_".$i],'.')) . "T" . substr($request["pes_".$i], strpos($request["pes_".$i],'.')+1). "B00";}
                        }
                        else{$peso_for= $request["pes_".$i];}

                        /**************************** ACTUALIZAR *******************************/
                            if(in_array($request["cod_ndi_".$i], $arraycad_actt, true))
                            {
                                //sacas datos
                                $dato_act=DetalleNotaIngreso::select("peso_cant","rollo")->where('dNotIng_id',"=",$request["cod_ndi_".$i])->first();  
                                
                                //restas nuevo - actual
                                /*********** inicio actualizar stock ******************/
                                $stock=ProductoAlmacen::where('nProAlmCod',"=",$request["tie_".$i])->first();
                                
                                $total=0;
                                if($request->producto_t_e=="TELAS")                            
                                    $total=$stock->nProdAlmStock + ($request["pes_".$i]-$dato_act->peso_cant);                                                           
                                else
                                    $total=$stock->nProdAlmStock + ($request["roll_".$i]-$dato_act->rollo);                                                           
                                ProductoAlmacen::where('nProAlmCod',"=",$request["tie_".$i])->update(['nProdAlmStock' => $total]);
                                /*********** fin actualizar stock ******************/

                                //actualizas
                                DetalleNotaIngreso::where('dNotIng_id',"=",$request["cod_ndi_".$i])                              
                                ->update(['nProAlmCod' => $request["tie_".$i],'peso_cant' => $request["pes_".$i],'rollo' => $request["roll_".$i]]);   
                            }

                            if(!isset($request["cbox_".$i]))//agregam los codigos de barra que estan sin marcar
                                array_push($noimpresos_,array($request["cb_".$i],$peso_for,$color_for,$producto_generico,$request["par_".$i])); 
                        /**************************** FIN ACTUALIZAR ***************************/
                    }
                }//dif empty
            } //end for
                
        } catch (Exception $e) {

            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

            /*if(empty($noimpresos_))            
                return redirect()->route('comercializacion.index',$request->codint)->with('info','Las notas se crearon correctamente.');        
            else
            {
                $barra = new DNS1D();

*/
                $cod_ndi=$request->codint;                            
                //$noimpresos=json_encode($noimpresos_,true);     
                foreach ($noimpresos_ as $key => $value) {
                    $regimp=new tmpimpresion1;
                    $regimp->campo1=$value[0];
                    $regimp->campo2=$value[1];
                    $regimp->campo3=$value[2];
                    $regimp->campo4=$value[3];
                    $regimp->nt=$request->codint;
                    $regimp->campo5=$value[4];
                    $regimp->save();           
                }
                
                /*
                $view =  \View::make('comercializacion.notaingreso.impresion',compact("noimpresos","cod_ndi","barra"))->render();
                $pdf = \App::make('dompdf.wrapper');
                $pdf->loadHTML($view)->setPaper([20, -33, 283.465, 595.276], 'landscape');//margen izq.- arriba, alto,ancho
                return $pdf->stream('invoice');   
               */
            /*    return view("comercializacion.notaingreso.impresion",compact("noimpresos","cod_ndi","barra"));
            }   */  

            if($request->page=="create")
                return redirect()->route('notaingreso.create',$request->codint)->with('info','Guargado satosfactoriamente.');  
            else
                return redirect()->route('notaingreso.editar',$request->codint)->with('info','Guargado satosfactoriamente.');  
    }

    public function show($id)
    {        
        $bandeja = DB::table('despacho_tintoreria')
            ->leftJoin('detalles_despacho_tintoreria', 'detalles_despacho_tintoreria.despacho_id', '=', 'despacho_tintoreria.id')
            ->leftJoin('color', 'detalles_despacho_tintoreria.color_id', '=', 'color.id')
            ->leftJoin('productos', 'detalles_despacho_tintoreria.producto_id', '=', 'productos.id')
            ->leftJoin('proveedores', 'detalles_despacho_tintoreria.proveedor_id', '=', 'proveedores.id')
            ->leftJoin('nota_ingreso', 'nota_ingreso.desptint_id', '=', 'detalles_despacho_tintoreria.id')
            ->select('detalles_despacho_tintoreria.created_at',
                'detalles_despacho_tintoreria.id', 
                'proveedores.razon_social', 
                'productos.nombre_generico',
                'productos.nombre_especifico',
                'productos.id AS cProdCod',
                'detalles_despacho_tintoreria.cantidad',
                'detalles_despacho_tintoreria.rollos',
                'color.id as id_color',
                'color.nombre',
                'detalles_despacho_tintoreria.estado',
                'detalles_despacho_tintoreria.color_id',
                'detalles_despacho_tintoreria.producto_id',
                'detalles_despacho_tintoreria.proveedor_id',
                'detalles_despacho_tintoreria.nro_lote',
                'nota_ingreso.desptint_id',
                'despacho_tintoreria.nroguia')            
            ->where('detalles_despacho_tintoreria.id','=', $id)            
            ->first();

        $tienda=ProductoAlmacen::where('cProdCod','=',$bandeja->cProdCod)->where('id_color','=',$bandeja->id_color)->get();

        $bandejatabla = DB::table('detalle_nota_ingreso')
            ->leftJoin('nota_ingreso', 'detalle_nota_ingreso.ning_id', '=', 'nota_ingreso.ning_id')
            ->leftJoin('productoalmacens', 'detalle_nota_ingreso.nProAlmCod', '=', 'productoalmacens.nProAlmCod')
            ->leftJoin('almacens', 'productoalmacens.nAlmCod', '=', 'almacens.nAlmCod')
            ->leftJoin('detalles_despacho_tintoreria', 'nota_ingreso.desptint_id', '=', 'detalles_despacho_tintoreria.id')
            ->leftJoin('productos', 'detalles_despacho_tintoreria.producto_id', '=', 'productos.id')
            ->leftJoin('color', 'detalles_despacho_tintoreria.color_id', '=', 'color.id')

            ->select('detalle_nota_ingreso.dNotIng_id',
                'nota_ingreso.ning_id',
                'detalle_nota_ingreso.fecha',
                'productos.nombre_generico',
                'productos.nombre_especifico',
                'productoalmacens.nProAlmCod',
                'almacens.cAlmNom',
                'nota_ingreso.partida',
                'color.nombre',
                'detalle_nota_ingreso.peso_cant',
                'detalle_nota_ingreso.rollo',
                'detalle_nota_ingreso.impreso',
                'detalle_nota_ingreso.cod_barras',
                'detalle_nota_ingreso.guia')
            ->where('nota_ingreso.desptint_id','=', $id)
            ->orderBy('detalle_nota_ingreso.dNotIng_id',"Asc")->get();
            //->paginate(10);
        $fecha=Carbon::now()->format('Y-m-d H:i:s');
        return view("comercializacion.notaingreso.show",compact('bandeja','tienda','fecha','id','bandejatabla'));
    }

    public function create($id)
    {    	
   		$bandeja = DB::table('despacho_tintoreria')
            ->leftJoin('detalles_despacho_tintoreria', 'detalles_despacho_tintoreria.despacho_id', '=', 'despacho_tintoreria.id')
            ->leftJoin('color', 'detalles_despacho_tintoreria.color_id', '=', 'color.id')
            ->leftJoin('productos', 'detalles_despacho_tintoreria.producto_id', '=', 'productos.id')
            ->leftJoin('proveedores', 'detalles_despacho_tintoreria.proveedor_id', '=', 'proveedores.id')
            ->leftJoin('nota_ingreso', 'nota_ingreso.desptint_id', '=', 'detalles_despacho_tintoreria.id')
            ->select('detalles_despacho_tintoreria.created_at',
                'detalles_despacho_tintoreria.id', 
                'proveedores.razon_social', 
                'productos.nombre_generico',
                'productos.nombre_especifico',
                'productos.id AS cProdCod',
                'detalles_despacho_tintoreria.cantidad',
                'detalles_despacho_tintoreria.rollos',
                'color.id as id_color',
                'color.nombre',
                'detalles_despacho_tintoreria.estado',
                'detalles_despacho_tintoreria.color_id',
                'detalles_despacho_tintoreria.producto_id',
                'detalles_despacho_tintoreria.proveedor_id',
                'detalles_despacho_tintoreria.nro_lote',
                'nota_ingreso.desptint_id',
                'despacho_tintoreria.nroguia')            
            ->where('detalles_despacho_tintoreria.id','=', $id)            
            ->first();

        $tienda=ProductoAlmacen::where('cProdCod','=',$bandeja->cProdCod)->where('id_color','=',$bandeja->id_color)->get();

        $bandejatabla = DB::table('detalle_nota_ingreso')
            ->leftJoin('nota_ingreso', 'detalle_nota_ingreso.ning_id', '=', 'nota_ingreso.ning_id')
            ->leftJoin('productoalmacens', 'detalle_nota_ingreso.nProAlmCod', '=', 'productoalmacens.nProAlmCod')
            ->leftJoin('almacens', 'productoalmacens.nAlmCod', '=', 'almacens.nAlmCod')
            ->leftJoin('detalles_despacho_tintoreria', 'nota_ingreso.desptint_id', '=', 'detalles_despacho_tintoreria.id')
            ->leftJoin('productos', 'detalles_despacho_tintoreria.producto_id', '=', 'productos.id')
            ->leftJoin('color', 'detalles_despacho_tintoreria.color_id', '=', 'color.id')

            ->select('detalle_nota_ingreso.dNotIng_id',
                'nota_ingreso.ning_id',
                'detalle_nota_ingreso.fecha',
                'productos.nombre_generico',
                'productos.nombre_especifico',
                'productoalmacens.nProAlmCod',
                'almacens.cAlmNom',
                'nota_ingreso.partida',
                'color.nombre',
                'detalle_nota_ingreso.peso_cant',
                'detalle_nota_ingreso.rollo',
                'detalle_nota_ingreso.impreso',
                'detalle_nota_ingreso.cod_barras',
                'detalle_nota_ingreso.guia')
            ->where('nota_ingreso.desptint_id','=', $id)
            ->orderBy('detalle_nota_ingreso.dNotIng_id',"Asc")->get();
            //->paginate(10);

        $fecha=Carbon::now()->format('Y-m-d H:i:s');
        
        $impresion="0";
        if (tmpimpresion1::where('nt', '=', $id)->exists())
            $impresion="1";

        $id_user=access()->user()->id;
        $userX=Users::where("id",$id_user)->first();  

        $almAsig=Rols_areas::where("id",$userX->id)->first();
        
        $codalmusu=$almAsig->areasalmacen->almacen->nAlmCod;

        $tienda=ProductoAlmacen::where('cProdCod','=',$bandeja->cProdCod)->where('id_color','=',$bandeja->id_color)->get();


    	return view("comercializacion.notaingreso.create",compact('bandeja','tienda','fecha','id','bandejatabla','impresion','codalmusu'));
    }
    
    public function editar($id)
    {
        
        $bandeja = DB::table('despacho_tintoreria')
            ->leftJoin('detalles_despacho_tintoreria', 'detalles_despacho_tintoreria.despacho_id', '=', 'despacho_tintoreria.id')
            ->leftJoin('color', 'detalles_despacho_tintoreria.color_id', '=', 'color.id')
            ->leftJoin('productos', 'detalles_despacho_tintoreria.producto_id', '=', 'productos.id')
            ->leftJoin('proveedores', 'detalles_despacho_tintoreria.proveedor_id', '=', 'proveedores.id')
            ->leftJoin('nota_ingreso', 'nota_ingreso.desptint_id', '=', 'detalles_despacho_tintoreria.id')
            ->select('detalles_despacho_tintoreria.created_at',
                'detalles_despacho_tintoreria.id', 
                'proveedores.razon_social', 
                'productos.nombre_generico',
                'productos.nombre_especifico',
                'productos.id AS cProdCod',
                'detalles_despacho_tintoreria.cantidad',
                'detalles_despacho_tintoreria.rollos',
                'color.id as id_color',
                'color.nombre',
                'detalles_despacho_tintoreria.estado',
                'detalles_despacho_tintoreria.color_id',
                'detalles_despacho_tintoreria.producto_id',
                'detalles_despacho_tintoreria.proveedor_id',
                'detalles_despacho_tintoreria.nro_lote',
                'nota_ingreso.desptint_id',
                'despacho_tintoreria.nroguia')            
            ->where('detalles_despacho_tintoreria.id','=', $id)            
            ->first();

        $tienda=ProductoAlmacen::where('cProdCod','=',$bandeja->cProdCod)->where('id_color','=',$bandeja->id_color)->get();

        $bandejatabla = DB::table('detalle_nota_ingreso')
            ->leftJoin('nota_ingreso', 'detalle_nota_ingreso.ning_id', '=', 'nota_ingreso.ning_id')
            ->leftJoin('productoalmacens', 'detalle_nota_ingreso.nProAlmCod', '=', 'productoalmacens.nProAlmCod')
            ->leftJoin('almacens', 'productoalmacens.nAlmCod', '=', 'almacens.nAlmCod')
            ->leftJoin('detalles_despacho_tintoreria', 'nota_ingreso.desptint_id', '=', 'detalles_despacho_tintoreria.id')
            ->leftJoin('productos', 'detalles_despacho_tintoreria.producto_id', '=', 'productos.id')
            ->leftJoin('color', 'detalles_despacho_tintoreria.color_id', '=', 'color.id')

            ->select('detalle_nota_ingreso.dNotIng_id',
                'nota_ingreso.ning_id',
                'detalle_nota_ingreso.fecha',
                'productos.nombre_generico',
                'productos.nombre_especifico',
                'productoalmacens.nProAlmCod',
                'almacens.cAlmNom',
                'nota_ingreso.partida',
                'color.nombre',
                'detalle_nota_ingreso.peso_cant',
                'detalle_nota_ingreso.rollo',
                'detalle_nota_ingreso.impreso',
                'detalle_nota_ingreso.cod_barras',
                'detalle_nota_ingreso.guia')
            ->where('nota_ingreso.desptint_id','=', $id)

            ->orderBy('detalle_nota_ingreso.dNotIng_id',"Asc")->get();
            //->paginate(10);

        $guiaz = DB::table('nota_ingreso')
            ->leftJoin('detalle_nota_ingreso', 'detalle_nota_ingreso.ning_id', '=', 'nota_ingreso.ning_id')
            ->select('detalle_nota_ingreso.guia')
            ->where('nota_ingreso.desptint_id','=', $id)
            ->where('detalle_nota_ingreso.guia','<>', "")
            ->distinct()
            ->get();

        //$partidas=NotaIngreso::select("partida")->where("desptint_id","=",$id)->get();


        $fecha=Carbon::now()->format('Y-m-d H:i:s');
        $impresion="0";
        if (tmpimpresion1::where('nt', '=', $id)->exists())
            $impresion="1";

        $id_user=access()->user()->id;
        $userX=Users::where("id",$id_user)->first();  

        $almAsig=Rols_areas::where("id",$userX->id)->first();
        
        $codalmusu=$almAsig->areasalmacen->almacen->nAlmCod;

        return view("comercializacion.notaingreso.editar",compact('bandeja','tienda','fecha','id','bandejatabla','guiaz','impresion','codalmusu'));
    }

    public function partidas(Request $request,$id,$guia)
    {
        if($request->ajax()){
            $partidas = DB::table('nota_ingreso')
            ->leftJoin('detalle_nota_ingreso', 'detalle_nota_ingreso.ning_id', '=', 'nota_ingreso.ning_id')
            ->select('nota_ingreso.partida')
            ->where('nota_ingreso.desptint_id','=', $id)            
            ->where('detalle_nota_ingreso.guia','=', $guia)
            ->distinct()
            ->get();

            return response()->json($partidas);         
        }
    }

    public function partidasall(Request $request,$id)
    {
        if($request->ajax()){
            $partidas = DB::table('nota_ingreso')
            ->leftJoin('detalle_nota_ingreso', 'detalle_nota_ingreso.ning_id', '=', 'nota_ingreso.ning_id')
            ->select('nota_ingreso.partida')
            ->where('nota_ingreso.desptint_id','=', $id)            
            //->where('detalle_nota_ingreso.guia','=', $guia)
            ->distinct()
            ->get();

            return response()->json($partidas);         
        }
    }

    public function veri_partida($id,$par,$codprovid,$guia,$codproid)
    {
        if($id=="0")
        {
            return 1;
        }
/*
        $auxi__ = DB::table('nota_ingreso')
        ->join('detalles_despacho_tintoreria', 'detalles_despacho_tintoreria.id', '=', 'nota_ingreso.desptint_id')
        ->join('despacho_tintoreria', 'despacho_tintoreria.id', '=', 'detalles_despacho_tintoreria.despacho_id')
        ->select('detalles_despacho_tintoreria.created_at','despacho_tintoreria.nroguia')
        ->where('despacho_tintoreria.proveedor_id','=',$codprovid) 
        ->where('despacho_tintoreria.nroguia','=',$mp_t)
        ->where('nota_ingreso.partida','=',$par)                 
        ->count();
*/
        $auxi__ = DB::table('nota_ingreso')
        ->join('detalles_despacho_tintoreria', 'detalles_despacho_tintoreria.id', '=', 'nota_ingreso.desptint_id')
        ->join('despacho_tintoreria', 'despacho_tintoreria.id', '=', 'detalles_despacho_tintoreria.despacho_id')
        ->join('detalle_nota_ingreso', 'detalle_nota_ingreso.ning_id', '=', 'nota_ingreso.ning_id')
        ->select('detalles_despacho_tintoreria.created_at','despacho_tintoreria.nroguia')
        ->where('despacho_tintoreria.proveedor_id','=',$codprovid) 
        ->where('detalle_nota_ingreso.guia','=',$guia)
        ->where('nota_ingreso.partida','=',$par)                 
        ->count();



        if($auxi__==0)//si no existe, lo guardo | new requirement: que se agregue si guia, producto son iguales pero no el códgio de producto.
        {
            return 1;
        }
        else
        {
            
            $auxi__2 = DB::table('nota_ingreso')
            ->join('detalles_despacho_tintoreria', 'detalles_despacho_tintoreria.id', '=', 'nota_ingreso.desptint_id')
            ->join('despacho_tintoreria', 'despacho_tintoreria.id', '=', 'detalles_despacho_tintoreria.despacho_id')
            ->join('detalle_nota_ingreso', 'detalle_nota_ingreso.ning_id', '=', 'nota_ingreso.ning_id')
            ->select('detalles_despacho_tintoreria.producto_id')
            ->where('despacho_tintoreria.proveedor_id','=',$codprovid) 
            ->where('detalle_nota_ingreso.guia','=',$guia)
            ->where('nota_ingreso.partida','=',$par)                 
            ->get();

            $aux_conta=0;
            foreach ($auxi__2 as $key => $value) 
            {
                if($codproid==$value->producto_id)
                    $aux_conta++;
            }

            if($aux_conta==0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }

    function impresion($id)
    {
        $noimpresos=tmpimpresion1::where('nt', '=', $id)->get();
        
        $ar=tmpimpresion1::where('nt', '=', $id);
        $ar->delete();

        $cod_ndi="";
        $barra = new DNS1D();
               
        //return view("comercializacion.notaingreso.impresion",compact("noimpresos","cod_ndi","barra","valor"));
 
        $view =  \View::make('comercializacion.notaingreso.impresion',compact("noimpresos","cod_ndi","barra","valor"))->render();
        $pdf = \App::make('dompdf.wrapper');
        //$pdf->loadHTML($view)->setPaper([5, -36, 450.465, 700.276], 'landscape');//margen izq.- arriba, alto,ancho
        //$pdf->loadHTML($view)->setPaper([30, -30, 350, 620.276], 'landscape');//margen izq.- arriba, alto,ancho
        //$pdf->loadHTML($view)->setPaper([40, -25, 360, 620], 'landscape');//margen izq.- arriba, alto,ancho----
        $pdf->loadHTML($view)->setPaper([30, -5, 390, 5000], 'landscape');//margen izq.- arriba, alto,ancho
        return $pdf->stream('invoice');             
    }



}
