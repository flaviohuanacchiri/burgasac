<?php

namespace App\Http\Controllers\Comercializacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Almacen;
use App\Producto;
use App\Color;
use App\ProductoAlmacen;
use App\DetalleNotaIngreso;
use App\DetalleNotaIngresoA;
use App\Areas_almacen;
use DB;

class AlmacenController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$almacens=Almacen::orderBy("nAlmCod","ASC")->paginate(10);
    	return view("comercializacion.almacen.index",compact("almacens"));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre'       => 'required',
            'ubic'       => 'required',
            'serie'       => 'required|min:3',
            'numero'       => 'required|min:6'
        ]);

        $almacen=new almacen;
        $almacen->cAlmNom=$request->nombre;
        $almacen->cAlmUbi=$request->ubic; 
        $almacen->nAlmSerAct=$request->serie;
        $almacen->nAlmNumAct=$request->numero;       
        
        $almacen->save();

        return redirect()->route('almacen.index')->with('info','El almacen fue creado.');  
    }

    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'nombre'       => 'required',
            'ubic'       => 'required',
            'serie'       => 'required|min:3',
            'numero'       => 'required|min:6'
        ]);

        almacen::where('nAlmCod',"=",$id)
        ->update([
            'cAlmNom' => $request->nombre,
            'cAlmUbi' => $request->ubic,
            'nAlmSerAct' => $request->serie,
            'nAlmNumAct' => $request->numero
            ]); 

        return redirect()->route('almacen.show',$id)->with('info','El almacen fue actualizado.');  
    }

    public function show($id)
    {
        $alm=almacen::where('nAlmCod','=',$id)->get()->first();
        return view("comercializacion.almacen.show",compact("alm"));
    }

    public function edit($id)
    {
        $alm=almacen::where('nAlmCod','=',$id)->get()->first();
        return view("comercializacion.almacen.edit",compact("alm","id"));
    }

    public function destroy($id)
    {
    	DB::beginTransaction();

        try 
        {      
	        $prodalm=ProductoAlmacen::where('nAlmCod',"=",$id)->get();
	        
	        foreach ($prodalm as $value) 
	        {
	            if($value->nProdAlmStock != 0)            
	                return back()->with('info','Error: No se puede eliminar este almacén ('.$value->almacen->cAlmNom.') porque tiene stock.');

	            if(DetalleNotaIngreso::where("nProAlmCod",$value->nProAlmCod)->first()!= "")               
	                return back()->with('info','Error: No se puede eliminar este almacén ('.$value->almacen->cAlmNom.') porque tiene registros enlazados a nota de ingreso.');
         
               if(DetalleNotaIngresoA::where("nProAlmCod",$value->nProAlmCod)->first()!= "")            
               		return back()->with('info','Error: No se puede eliminar este almacén ('.$value->almacen->cAlmNom.') porque tiene registros enlazados a nota de ingreso atípico.');

                if(Areas_almacen::where("nAlmCod",$id)->first()!= "")            
                    return back()->with('info','Error: No se puede eliminar este almacén ('.$value->almacen->cAlmNom.') porque tiene registros enlazados a areas_almacen.');
	        }


	        $areaalm=Areas_almacen::where('nAlmCod',"=",$id);
	        $areaalm->delete();

            $prodalm=ProductoAlmacen::where('nAlmCod',"=",$id);
            $prodalm->delete();

	        $almacen=Almacen::where('nAlmCod','=',$id);
	        $almacen->delete();
        } catch (Exception $e) {

            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return back()->with('info','El almacen fue eliminado.');
    }

    public function create()
    {
    	return view("comercializacion.almacen.create");
    }

    public function proalm_store(Request $request,$id)
    {

        $this->validate($request, [
            'mini'       => 'required',
            'maxi'       => 'required',
            'colprod'       => 'required',
            'puni'       => 'required',
            'selprod'       => 'required',
            'stock'        => 'required'
        ]);
        
        DB::beginTransaction();

        try 
        {
            $proalm=new ProductoAlmacen;
            $proalm->cProdCod=$request->selprod;
            $proalm->id_color=$request->colprod;
            $proalm->nAlmCod=$id;  
            $proalm->nProdAlmMin=$request->mini;
            $proalm->nProdAlmMax=$request->maxi;  
            $proalm->nProdAlmPuni=$request->puni;  
            $proalm->nProdAlmStock=$request->stock;  
            $proalm->nProdAlmStockIni=$request->stock;  
            
            
            $proalm->save();        
        } catch (Exception $e) {

            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return redirect()->route('almacen.proalm_create',compact("id"))->with('info','El producto fue agredado al almacen satisfactoriamente.');  
    }

    public function proalm_create(Request $request,$id)
    {
        $productos=Producto::all();

        $prodalm=ProductoAlmacen::where("nAlmCod","=",$id)->namecodigo($request->nombre,$request->color_bus)->paginate(5);
        $prodalm_Bus=DB::table('productoalmacens')
        ->join("productos","productos.id","=","productoalmacens.cProdCod")
        ->select("productos.nombre_especifico","productos.id","productos.nombre_generico")
        ->where("nAlmCod","=",$id)
        ->distinct("productos.id")
        ->get();
        $nombrealmacen=Almacen::where("nAlmCod","=",$id)->first();

        return view("comercializacion.almacen.proalm_create",compact("productos","prodalm","prodalm_Bus","id","nombrealmacen"));
    }

    public function proalm_destroy($id)
    {        
        $prodalm=ProductoAlmacen::where("nProAlmCod","=",$id)->first();
        
        if($prodalm->nProdAlmStock == 0)
        {
            $prodalm1=ProductoAlmacen::where("nProAlmCod","=",$id);
            $prodalm1->delete();
        }
        else
            return back()->with('info','Error: No se puede retirar el producto porque aún queda stock.');
                
        return back()->with('info','El producto fue retirado del almacen.');
    }

    public function proalm_edit(Request $request,$id)
    {
        //$productos=Producto::whereNotIn("cProdCod",ProductoAlmacen::select("cProdCod")->where("nAlmCod","=",$id)->get())->get();
        $prodalm_esp=ProductoAlmacen::where("nProAlmCod","=",$id)->first();
        $prodalm=ProductoAlmacen::where("nAlmCod","=",$prodalm_esp->nAlmCod)->namecodigo($request->nombre,$request->color_bus)->paginate(5);
        $prodalm_Bus=DB::table('productoalmacens')
        ->join("productos","productos.id","=","productoalmacens.cProdCod")
        ->select("productos.nombre_especifico","productos.id","productos.nombre_generico")
        ->where("nAlmCod","=",$id)
        ->distinct("productos.id")
        ->get();
        $productos_total=Producto::all();
        return view("comercializacion.almacen.proalm_edit",compact("prodalm","prodalm_Bus","id","prodalm_esp","productos_total"));
    }

    public function proalm_update(Request $request,$id)
    {
     
        $this->validate($request, [
            'mini'       => 'required',
            'maxi'       => 'required',            
            'puni'       => 'required',           
            'stock'        => 'required'
        ]);
        
        ProductoAlmacen::where('nProAlmCod',"=",$id)
        ->update([
            'nProdAlmMin' => $request->mini,
            'nProdAlmMax' => $request->maxi,
            'nProdAlmPuni' => $request->puni,
            //'nProdAlmStock' => $request->stock            
            ]); 
        $prodalm_esp=ProductoAlmacen::where("nProAlmCod","=",$id)->first();

        $productos=Producto::whereNotIn("id",ProductoAlmacen::select("cProdCod")->where("nAlmCod","=",$prodalm_esp->nAlmCod)->get())->get();
        $prodalm=ProductoAlmacen::where("nAlmCod","=",$prodalm_esp->nAlmCod)->paginate(5);
        $id=$prodalm_esp->nAlmCod;
        return redirect()->route("almacen.proalm_create",compact("productos","prodalm","id"));
    }

    public function productocol($id,$pro)
    {
        $colores=Color::whereNotIn("id",ProductoAlmacen::select("id_color")->where("nAlmCod",$id)->where("cProdCod",$pro)->get())->where("estado","1")->get();

        return $colores;
    }

    public function productocolSi($id,$pro)
    {
        $colores=Color::whereIn("id",ProductoAlmacen::select("id_color")->where("nAlmCod",$id)->where("cProdCod",$pro)->get())->where("estado","1")->get();

        return $colores;
    }
}
