<?php

namespace App\Http\Controllers\Comercializacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ProductoAlmacen;
use App\Almacen;
use App\Producto;
use App\DetalleDespachoTintoreria;
use App\Transferencias;
use App\Transferencias_detalle;
use Carbon\Carbon;
use App\Trans_det_elim;
use App\Trans_elim;
use DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Users;
use App\Rols_areas;
use App\DetalleNotaIngreso;
use App\DetalleNotaIngresoA;

class TransferenciaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
    	$almacen=Almacen::all();
    	$bandeja_tabla=Transferencias::orderBy("cTraCod","desc")->where('tTraFechReg', 'like', "%".$request->fech."%")
    	->Where('nAlmCod1', '=', $request->alm1)
        ->where('nAlmCod2', '=', $request->alm2)
        ->paginate(50);    	
        $request_=$request;
    	return view("comercializacion.transferencia.index",compact("almacen","bandeja_tabla","request_"));
    }

    public function create()
    {
    	$id_user=access()->user()->id;

        $productoAlmacen=DB::table("almacens")
    	->join("productoalmacens","productoalmacens.nAlmCod","=","almacens.nAlmCod")        
        ->Join("areas_almacen","areas_almacen.nAlmCod","=","almacens.nAlmCod")
        ->Join("rols_areas","rols_areas.nAreAlmCod","=","areas_almacen.nAreAlmCod") 
    	->select("almacens.cAlmUbi","almacens.nAlmCod","almacens.cAlmNom")
        ->where('rols_areas.id','=',$id_user)
    	->distinct()
    	->get();

    	//$bandeja_tabla=transferencia::paginate(10);

    	$id=0;

        $trans_ultimo = Transferencias::select('cTraCod')
            ->orderBy('cTraCod',"desc")                      
            ->get()->first();

        $conn=0;
        $dat="";
        if($trans_ultimo!="")
        {
            $dat=$trans_ultimo->cTraCod;       
            $conn=substr($dat, 1);
        }        
        //return $conn;
        $cad="T";
        for($i=strlen(++$conn);$i<10;$i++)
        {
            $cad.="0";
        }
       
        $cad.= $conn;

        $id_user=access()->user()->id;
        $userX=Users::where("id",$id_user)->first(); 
        $manauto=$userX->rolsareas[0]->areasalmacen->areas->bForPag;

        $fecha=Carbon::now()->format('Y-m-d');
        $id_user=access()->user()->id;
        $userX=Users::where("id",$id_user)->first();  

        $almAsig=Rols_areas::where("id",$userX->id)->first();
        
        $codalmusu=$almAsig->areasalmacen->almacen->nAlmCod;
    	return view("comercializacion.transferencia.create",compact("fecha","productoAlmacen","id","cad","manauto","codalmusu"));
    }

    /*public function veri_siSeTraslado_CB($cb,$alm1,$alm2)
    {
        if (DB::table('transferencia')
        ->Join("transferencia_detalle","transferencia_detalle.cTraCod","=","transferencia.cTraCod")
        ->where('transferencia_detalle.cod_barras',"=",$cb)
        ->where('transferencia.nAlmCod1',"=",$cb)
        ->where('transferencia.nAlmCod2',"=",$cb)        
        ->get()) 
        {
            return 0;
        }       

        return 1;
    }*/

    public function veri_codbarra($id,$alm1)
    {
        $datos=DB::table('detalle_nota_ingreso')
        ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso.nProAlmCod")
        ->leftJoin("productos","productos.id","=","productoalmacens.cProdCod")
        ->leftJoin("color","color.id","=","productoalmacens.id_color")
        ->leftJoin("almacens","almacens.nAlmCod","=","productoalmacens.nAlmCod")
        ->where(DB::raw("REPLACE(detalle_nota_ingreso.cod_barras,'-','')"),"=",$id)
        ->orWhere("detalle_nota_ingreso.cod_barras","=",$id)
        //->where("detalle_nota_ingreso.cod_barras","=",$id)
        ->where("productoalmacens.nAlmCod","=",$alm1)   
        ->select("productoalmacens.nProAlmCod","productos.nombre_generico","productos.nombre_especifico","color.nombre","productoalmacens.nProdAlmStock","detalle_nota_ingreso.peso_cant","almacens.nAlmCod","almacens.cAlmNom","almacens.cAlmUbi","detalle_nota_ingreso.rollo","productoalmacens.nAlmCod")
        ->limit(1)->get();        
        
        $dat=json_decode($datos, true);
        
        if(empty($dat))
        {        	
        	//$datos=detalle_nota_ingreso_a::where("cod_barras","=",$id)->get();
        	$datos=DB::table('detalle_nota_ingreso_a')
	        ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso_a.nProAlmCod")
	        ->leftJoin("productos","productos.id","=","productoalmacens.cProdCod")
	        ->leftJoin("color","color.id","=","productoalmacens.id_color")
	        ->leftJoin("almacens","almacens.nAlmCod","=","productoalmacens.nAlmCod")
	        ->where(DB::raw("REPLACE(detalle_nota_ingreso_a.cod_barras,'-','')"),"=",$id)
            ->orWhere("detalle_nota_ingreso_a.cod_barras","=",$id)
            //->where("detalle_nota_ingreso_a.cod_barras","=",$id)
	        ->where("productoalmacens.nAlmCod","=",$alm1)   
	        ->select("productoalmacens.nProAlmCod","productos.nombre_generico","productos.nombre_especifico","color.nombre","productoalmacens.nProdAlmStock","detalle_nota_ingreso_a.peso_cant","almacens.nAlmCod","almacens.cAlmNom","almacens.cAlmUbi","detalle_nota_ingreso_a.rollo","productoalmacens.nAlmCod")
	        ->limit(1)->get();  
        }

        return $datos;
    }

    public function productoalm(Request $request,$id,$id2)
    {    	
    	if($request->ajax()){
    		$productos_x=DB::table("productoalmacens")
    		->Join("productos","productoalmacens.cProdCod","=","productos.id")
    		//->leftJoin("detalles_despacho_tintoreria","detalles_despacho_tintoreria.producto_id","=","productos.id")
    		->Join("color","productoalmacens.id_color","=","color.id")    		
    		->where('productoalmacens.nAlmCod','=',$id)
    		->whereIn(DB::raw("CONCAT(productoalmacens.cProdCod,'_',id_color)"),ProductoAlmacen::select(DB::raw("CONCAT(cProdCod,'_',id_color)"))->where('nAlmCod','=',$id2)->get())
    		->select("color.nombre","productos.nombre_generico","productos.nombre_especifico","productoalmacens.nProAlmCod")
    		->distinct()
    		->get();
    		return response()->json($productos_x);
    	}
    }

    public function almacenalm(Request $request,$id)
    {
    	if($request->ajax()){
    		$almacen_x=DB::table("almacens")
	    	->join("productoalmacens","productoalmacens.nAlmCod","=","almacens.nAlmCod")
	    	->select("almacens.cAlmUbi","almacens.nAlmCod","almacens.cAlmNom")
	    	->where('productoalmacens.nAlmCod','<>',$id)
	    	->distinct()
	    	->get();
    		return response()->json($almacen_x);
    	}
    }

    public function almacenProStock(Request $request,$id)
    {
    	if($request->ajax()){
    		$almacenprostock_x=ProductoAlmacen::select("productoalmacens.nProdAlmStock","productos.nombre_especifico")
    		->leftJoin("productos","productos.id","=","productoalmacens.cProdCod")
	    	->where('productoalmacens.nProAlmCod','=',$id)	    	
	    	->get()
	    	->first();

    		return response()->json($almacenprostock_x);
    	}
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'alm1'       => 'required',
            'alm2'       => 'required'
        ]);

    	DB::beginTransaction();

        try 
        {      
	       	$trans=new Transferencias;
	    	$trans->cTraCod=$request->ctrans;
	    	$trans->nAlmCod1=$request->alm1;
	    	$trans->nAlmCod2=$request->alm2;
	    	$trans->tTraFechReg=$request->fecha;
	    	$trans->tTraFecUlt=$request->fecha;
	    	$trans->cTraObs=$request->obs;
	    	$trans->save();
	    	
			for($i=1;$i<=$request->conta;$i++)
	        {
	            if($request["cod_ndi_".$i]!="")
	            {
	                if($request["cod_ndi_".$i]==0)//si es diferente no se ingresa
	                {                    
	                    $tradetalle=new Transferencias_detalle;
	                    $tradetalle->nProAlmCod=$request["prod_".$i];                   
	                    $tradetalle->cod_barras=$request["codb_".$i];
	                    $tradetalle->dTraCant=$request["peso_".$i];
	                    $tradetalle->bTraDev=$request["rollo_".$i];
	                    $tradetalle->cTraCod=$request->ctrans;
	                    
	                    $tradetalle->save();

	                    /*********** inicio actualizar stock alm1 ******************/
	                    $stock=ProductoAlmacen::where('nProAlmCod',"=",$request["prod_".$i])->first();
	                    
	                    $total=0;        
	                    if($stock->producto->nombre_especifico=="TELAS")                            
	                        $total=$stock->nProdAlmStock - $request["peso_".$i];                                                           
	                    else
	                        $total=$stock->nProdAlmStock - $request["rollo_".$i];     

	                    ProductoAlmacen::where('nProAlmCod',"=",$request["prod_".$i])
                        ->update(['nProdAlmStock' => $total]);
	                    /*********** fin actualizar stock ******************/

                        $total=0; 
                        /*********** inicio actualizar stock alm2 ******************/
                        $stock2=ProductoAlmacen::where('nAlmCod',$request->alm2)
                        ->where('cProdCod',$stock->cProdCod)
                        ->where('id_color',$stock->id_color)
                        ->first();
                        
                        $total=0;        
                        if($stock2->producto->nombre_especifico=="TELAS")                            
                            $total=$stock2->nProdAlmStock + $request["peso_".$i];          
                        else
                            $total=$stock2->nProdAlmStock + $request["rollo_".$i];                                                           
                        ProductoAlmacen::where('nProAlmCod',"=",$stock2->nProAlmCod)
                        ->update(['nProdAlmStock' => $total]);
                        /*********** fin actualizar stock ******************/

                       
                        /*********** Si tiene CB actualizamos ubicación**********/
                        if (DB::table('detalle_nota_ingreso')
                            ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso.nProAlmCod")
                            ->where(DB::raw("REPLACE(detalle_nota_ingreso.cod_barras,'-','')"),"=",$request["codb_".$i])
                            ->orWhere("detalle_nota_ingreso.cod_barras","=",$request["codb_".$i])
                            ->where("productoalmacens.nAlmCod","=",$request->alm1)   
                            ->select("detalle_nota_ingreso.dNotIng_id")
                            ->exists()) 
                        {
                            $datosCB=DB::table('detalle_nota_ingreso')
                            ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso.nProAlmCod")
                            ->where(DB::raw("REPLACE(detalle_nota_ingreso.cod_barras,'-','')"),"=",$request["codb_".$i])
                            ->orWhere("detalle_nota_ingreso.cod_barras","=",$request["codb_".$i])
                            ->where("productoalmacens.nAlmCod","=",$request->alm1)   
                            ->select("detalle_nota_ingreso.dNotIng_id")
                            ->first();        

                            DetalleNotaIngreso::where("dNotIng_id",$datosCB->dNotIng_id)
                            ->update(['nProAlmCod' => $stock2->nProAlmCod]);
                        }
                        else if(DB::table('detalle_nota_ingreso_a')
                            ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso_a.nProAlmCod")
                            ->where(DB::raw("REPLACE(detalle_nota_ingreso_a.cod_barras,'-','')"),"=",$request["codb_".$i])
                            ->orWhere("detalle_nota_ingreso_a.cod_barras","=",$request["codb_".$i])
                            ->where("productoalmacens.nAlmCod","=",$request->alm1)   
                            ->select("detalle_nota_ingreso_a.dNotInga_id")
                            ->exists()) 
                        {
                            $datosCB=DB::table('detalle_nota_ingreso_a')
                            ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso_a.nProAlmCod")
                            ->where(DB::raw("REPLACE(detalle_nota_ingreso_a.cod_barras,'-','')"),"=",$request["codb_".$i])
                            ->orWhere("detalle_nota_ingreso_a.cod_barras","=",$request["codb_".$i])
                            ->where("productoalmacens.nAlmCod","=",$request->alm1)   
                            ->select("detalle_nota_ingreso_a.dNotInga_id")
                            ->first();        

                            DetalleNotaIngresoA::where("dNotInga_id",$datosCB->dNotInga_id)
                            ->update(['nProAlmCod' => $stock2->nProAlmCod]);
                        }
                        /*********** FIn: CB actualizamos ubicación**********/
	             
	                }
	                else
	                {
	                   
	                }
	            }//dif empty
	        } //end for
        } 
        catch (Exception $e) 
        {

            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return redirect()->route('transferencia.index')->with('info','La transferencia se registró correctamente.'); 
    }

    public function edit($id)
    {
    	$tra=Transferencias::where("cTraCod",$id)->first();

    	$productos=DB::table("productoalmacens")
    		->Join("productos","productoalmacens.cProdCod","=","productos.id")
    		//->leftJoin("detalles_despacho_tintoreria","detalles_despacho_tintoreria.producto_id","=","productos.id")
    		->Join("color","productoalmacens.id_color","=","color.id")    		
    		->where('productoalmacens.nAlmCod','=',$tra->nAlmCod1)
    		->whereIn(DB::raw("CONCAT(productoalmacens.cProdCod,'_',id_color)"),ProductoAlmacen::select(DB::raw("CONCAT(cProdCod,'_',id_color)"))->where('nAlmCod','=',$tra->nAlmCod2)->get())
    		->select("color.nombre","productos.nombre_generico","productos.nombre_especifico","productoalmacens.nProAlmCod")
    		->distinct()
    		->get();

    	$tra_det=Transferencias_detalle::where("cTraCod",$id)->get();

        $id_user=access()->user()->id;
        $userX=Users::where("id",$id_user)->first(); 
        $manauto=$userX->rolsareas[0]->areasalmacen->areas->bForPag;

    	return view("comercializacion.transferencia.edit",compact("id","tra_det","tra","productos","manauto"));
    }

    public function update(Request $request,$id)
    {            
    	DB::beginTransaction();

        try 
        {      
	       	$fecha=Carbon::now()->format('Y-m-d');
        	Transferencias::where('cTraCod',"=",$id)->update(['tTraFecUlt' => $fecha]);

	       	//eliminar los registro que se quitaron y actualizar stock
	    	$arrayElim=explode(",", $request->eliminados);

	        for($j=1;$j<count($arrayElim);$j++)
	        {
	            $datos=Transferencias_detalle::where('nTraDetCod','=',$arrayElim[$j])->first();  

	            /*********** inicio actualizar stock ******************/
	            $stock=ProductoAlmacen::where('nProAlmCod',"=",$datos->nProAlmCod)->first();
	              
	            $total=0;        
	            //if($stock->producto->nombre_especifico=="TELAS")                             
	                $total=$stock->nProdAlmStock + $datos->dTraCant;                                                           
	            //else
	                //$total=$stock->nProdAlmStock + $datos->rollo;  

	            ProductoAlmacen::where('nProAlmCod',"=",$datos->nProAlmCod)->update(['nProdAlmStock' => $total]);

	            Transferencias_detalle::where('nTraDetCod','=',$arrayElim[$j])->delete();  
	            /*********** fin actualizar stock ******************/ 


                $total=0; 
                /*********** inicio actualizar stock alm2 ******************/
                $stock2=ProductoAlmacen::where('nAlmCod',$request->alm2)
                ->where('cProdCod',$stock->cProdCod)
                ->where('id_color',$stock->id_color)
                ->first();
                
                $total=0;        
                //if($stock->producto->nombre_especifico=="TELAS")                             
                    $total=$stock2->nProdAlmStock - $datos->dTraCant;                                                           
                //else
                    //$total=$stock->nProdAlmStock + $datos->rollo;                                                            
                ProductoAlmacen::where('nProAlmCod',"=",$stock2->nProAlmCod)->update(['nProdAlmStock' => $total]);
                /*********** fin actualizar stock ******************/
                if($datos->cod_barras!="")
                {
                    if (DB::table('detalle_nota_ingreso')
                        ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso.nProAlmCod")
                        ->where(DB::raw("REPLACE(detalle_nota_ingreso.cod_barras,'-','')"),"=",$request["codb_".$j])
                        ->orWhere("detalle_nota_ingreso.cod_barras","=",$datos->cod_barras)
                        ->where("productoalmacens.nAlmCod","=",$request->alm1)   
                        ->select("detalle_nota_ingreso.dNotIng_id")
                        ->exists()) 
                    {
                        $datosCB_x=DB::table('detalle_nota_ingreso')
                        ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso.nProAlmCod")
                        ->where(DB::raw("REPLACE(detalle_nota_ingreso.cod_barras,'-','')"),"=",$request["codb_".$j])
                        ->orWhere("detalle_nota_ingreso.cod_barras","=",$datos->cod_barras)
                        ->where("productoalmacens.nAlmCod","=",$request->alm1)   
                        ->select("detalle_nota_ingreso.dNotIng_id")
                        ->first();        

                        DetalleNotaIngreso::where("dNotIng_id",$datosCB->dNotIng_id)
                        ->update(['nProAlmCod' => $datos->nProAlmCod]);
                    }
                    else if(DB::table('detalle_nota_ingreso_a')
                        ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso_a.nProAlmCod")
                        ->where(DB::raw("REPLACE(detalle_nota_ingreso_a.cod_barras,'-','')"),"=",$request["codb_".$j])
                        ->orWhere("detalle_nota_ingreso_a.cod_barras","=",$datos->cod_barras)
                        ->where("productoalmacens.nAlmCod","=",$request->alm1)   
                        ->select("detalle_nota_ingreso_a.dNotInga_id")
                        ->exists()) 
                    {
                        $datosCB_x=DB::table('detalle_nota_ingreso_a')
                        ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso_a.nProAlmCod")
                        ->where(DB::raw("REPLACE(detalle_nota_ingreso_a.cod_barras,'-','')"),"=",$request["codb_".$j])
                        ->orWhere("detalle_nota_ingreso_a.cod_barras","=",$datos->cod_barras)
                        ->where("productoalmacens.nAlmCod","=",$request->alm1)   
                        ->select("detalle_nota_ingreso_a.dNotInga_id")
                        ->first();        

                        DetalleNotaIngresoA::where("dNotInga_id",$datosCB->dNotInga_id)
                        ->update(['nProAlmCod' => $datos->nProAlmCod]);
                    }

                }
                
	        }

	    	//agregar los nuevos
			for($i=1;$i<=$request->conta;$i++)
	        {
	            if($request["cod_ndi_".$i]!="")
	            {
	                if($request["cod_ndi_".$i]==0)//si es diferente no se ingresa
	                {
	                    
	                    $tradetalle=new Transferencias_detalle;
	                    $tradetalle->nProAlmCod=$request["prod_".$i];                   
	                    $tradetalle->cod_barras=$request["codb_".$i];
	                    $tradetalle->dTraCant=$request["peso_".$i];
	                    $tradetalle->bTraDev=$request["rollo_".$i];
	                    $tradetalle->cTraCod=$id;
	                    
	                    $tradetalle->save();

	                    /*********** inicio actualizar stock alm1 ******************/
	                    $stock=ProductoAlmacen::where('nProAlmCod',"=",$request["prod_".$i])->first();
	                    
	                    $total=0;        
	                    if($stock->producto->nombre_especifico=="TELAS")                            
	                        $total=$stock->nProdAlmStock - $request["peso_".$i];                                                          
	                    else
	                        $total=$stock->nProdAlmStock - $request["rollo_".$i];                                                           
	                    ProductoAlmacen::where('nProAlmCod',"=",$request["prod_".$i])->update(['nProdAlmStock' => $total]);
	                    /*********** fin actualizar stock ******************/

                        $total=0; 
                        /*********** inicio actualizar stock alm2 ******************/
                        $stock2=ProductoAlmacen::where('nAlmCod',$request->alm2)
                        ->where('cProdCod',$stock->cProdCod)
                        ->where('id_color',$stock->id_color)
                        ->first();
                        
                        $total=0;        
                        if($stock2->producto->nombre_especifico=="TELAS")                            
                            $total=$stock2->nProdAlmStock + $request["peso_".$i];          
                        else
                            $total=$stock2->nProdAlmStock + $request["rollo_".$i];                                                           
                        ProductoAlmacen::where('nProAlmCod',"=",$stock2->nProAlmCod)->update(['nProdAlmStock' => $total]);
                        /*********** fin actualizar stock ******************/

                        /*********** Si tiene CB actualizamos ubicación**********/
                        if (DB::table('detalle_nota_ingreso')
                            ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso.nProAlmCod")
                            ->where(DB::raw("REPLACE(detalle_nota_ingreso.cod_barras,'-','')"),"=",$request["codb_".$i])
                            ->orWhere("detalle_nota_ingreso.cod_barras","=",$request["codb_".$i])
                            ->where("productoalmacens.nAlmCod","=",$request->alm1)   
                            ->select("detalle_nota_ingreso.dNotIng_id")
                            ->exists()) 
                        {
                            $datosCB=DB::table('detalle_nota_ingreso')
                            ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso.nProAlmCod")
                            ->where(DB::raw("REPLACE(detalle_nota_ingreso.cod_barras,'-','')"),"=",$request["codb_".$i])
                            ->orWhere("detalle_nota_ingreso.cod_barras","=",$request["codb_".$i])
                            ->where("productoalmacens.nAlmCod","=",$request->alm1)   
                            ->select("detalle_nota_ingreso.dNotIng_id")
                            ->first();        

                            DetalleNotaIngreso::where("dNotIng_id",$datosCB->dNotIng_id)
                            ->update(['nProAlmCod' => $stock2->nProAlmCod]);
                        }
                        else if(DB::table('detalle_nota_ingreso_a')
                            ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso_a.nProAlmCod")
                            ->where(DB::raw("REPLACE(detalle_nota_ingreso_a.cod_barras,'-','')"),"=",$request["codb_".$i])
                            ->orWhere("detalle_nota_ingreso_a.cod_barras","=",$request["codb_".$i])
                            ->where("productoalmacens.nAlmCod","=",$request->alm1)   
                            ->select("detalle_nota_ingreso_a.dNotInga_id")
                            ->exists()) 
                        {
                            $datosCB=DB::table('detalle_nota_ingreso_a')
                            ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso_a.nProAlmCod")
                            ->where(DB::raw("REPLACE(detalle_nota_ingreso_a.cod_barras,'-','')"),"=",$request["codb_".$i])
                            ->orWhere("detalle_nota_ingreso_a.cod_barras","=",$request["codb_".$i])
                            ->where("productoalmacens.nAlmCod","=",$request->alm1)   
                            ->select("detalle_nota_ingreso_a.dNotInga_id")
                            ->first();        

                            DetalleNotaIngresoA::where("dNotInga_id",$datosCB->dNotInga_id)
                            ->update(['nProAlmCod' => $stock2->nProAlmCod]);
                        }
                        /*********** FIn: CB actualizamos ubicación**********/
	             
	                }
	                else
	                {
	                   
	                }
	            }//dif empty
	        } //end for

        } 
        catch (Exception $e) 
        {
            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return redirect()->route('transferencia.index')->with('info','LA transferencia se actualizó correctamente.'); 
    }

    public function show($id)
    {
    	$tra=Transferencias::where("cTraCod",$id)->first();

    	$productos=DB::table("productoalmacens")
    		->Join("productos","productoalmacens.cProdCod","=","productos.id")
    		//->leftJoin("detalles_despacho_tintoreria","detalles_despacho_tintoreria.producto_id","=","productos.id")
    		->Join("color","productoalmacens.id_color","=","color.id")    		
    		->where('productoalmacens.nAlmCod','=',$tra->nAlmCod1)
    		->whereIn(DB::raw("CONCAT(productoalmacens.cProdCod,'_',id_color)"),ProductoAlmacen::select(DB::raw("CONCAT(cProdCod,'_',id_color)"))->where('nAlmCod','=',$tra->nAlmCod2)->get())
    		->select("color.nombre","productos.nombre_generico","productoalmacens.nProAlmCod")
    		->distinct()
    		->get();

    	$tra_det=Transferencias_detalle::where("cTraCod",$id)->get();

    	return view("comercializacion.transferencia.show",compact("id","tra_det","tra","productos"));
    }

    public function destroy($id)
    {
    	DB::beginTransaction();

        try 
        {      
        	$tradetalle=Transferencias_detalle::where("cTraCod",$id)->get();
           
           	foreach ($tradetalle as $key => $datos) 
           	{
           		/*********** inicio actualizar stock alm1 ******************/
	            $stock=ProductoAlmacen::where('nProAlmCod',"=",$datos->nProAlmCod)->first();
	              
	            $total=0;        
	            //if($stock->producto->nombre_especifico=="TELAS")                             
	                $total=$stock->nProdAlmStock + $datos->dTraCant;                                                           
	            //else
	                //$total=$stock->nProdAlmStock + $datos->rollo;  

	            ProductoAlmacen::where('nProAlmCod',"=",$datos->nProAlmCod)->update(['nProdAlmStock' => $total]);
	            /*********** fin actualizar stock ******************/   

                $total=0; 
                /*********** inicio actualizar stock alm2 ******************/
                $stock2=ProductoAlmacen::where('nAlmCod',$datos->transferencia->nAlmCod2)
                ->where('cProdCod',$stock->cProdCod)
                ->where('id_color',$stock->id_color)
                ->first();
                
                $total=0;        
                //if($stock->producto->nombre_especifico=="TELAS")                             
                    $total=$stock2->nProdAlmStock - $datos->dTraCant;                                                           
                //else
                    //$total=$stock->nProdAlmStock + $datos->rollo;                                                            
                ProductoAlmacen::where('nProAlmCod',"=",$stock2->nProAlmCod)->update(['nProdAlmStock' => $total]);
                /*********** fin actualizar stock ******************/        	
           	}

            
            //Guardar log ----------------------------------------
            
            $tra_datos=Transferencias::where("cTraCod",$id)->first();

            $log=new Trans_elim;            
            $log->cTraCod=$tra_datos->cTraCod;
            $log->nAlmCod1=$tra_datos->nAlmCod1;
            $log->nAlmCod2=$tra_datos->nAlmCod2;
            $log->tTraFechReg=$tra_datos->tTraFechReg;
            $log->tTraFecUlt=$tra_datos->tTraFecUlt;
            $log->cTraObs=$tra_datos->cTraObs;
         
            $fecha=Carbon::now()->format('Y-m-d h:i:s');
            
            $log->tFechElim=$fecha;
                    
            $log->save(); 

            //Guardar log ----------------------------------------

            $tra_det_datos=Transferencias_detalle::where("cTraCod",$id)->get();
            
            foreach($tra_det_datos as $key => $value) {
                
                $log_det=new Trans_det_elim;
                $log_det->nTraDetCod=$value->nTraDetCod;                   
                $log_det->nProAlmCod=$value->nProAlmCod;
                $log_det->cod_barras=$value->cod_barras;
                $log_det->dTraCant=$value->dTraCant;
                $log_det->bTraDev=$value->bTraDev;
                $log_det->cTraCod=$value->cTraCod;
                
                $log_det->save();
            }

            //eliminar
            
	       	$tra_det=Transferencias_detalle::where("cTraCod",$id);
			$tra_det->delete();

			$tra=Transferencias::where("cTraCod",$id);
	       	$tra->delete();
        } 
        catch (Exception $e) 
        {

            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return back()->with('info','El registro fue eliminado.');
    }
    
}
