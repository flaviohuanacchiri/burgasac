<?php

namespace App\Http\Controllers\Comercializacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Almacen;
use App\Producto;
use App\Color;
use App\ProductoAlmacen;
use App\DetalleNotaIngreso;
use App\DetalleNotaIngresoA;
use App\Areas_almacen;
use DB;
use App\Cliente;
use Carbon\Carbon;
use App\Rols_user;
use App\Rols_areas;
use App\CajaApertura;
use App\CajaCierre;
use App\CajaVenta;
use App\venta;
use App\tipodocv;
use App\forpago;
use App\ventadetalle;
use App\Devoluciones;
use App\Prodevolver;
use App\DetalleVentaDev;
use App\Users;
use App\Res_Stock_Telas;
use App\Movimiento_Tela;
use App\Res_stock_mp;
use App\Movimiento_MP;
use Illuminate\Support\Facades\Auth;

class NotadevController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    function autocomplete_producto($nAlmCod,$busca)
    {    
        //$term = Input::get('term');
        $array = explode(" ", trim($busca));
        $cad='%';
        foreach ($array as $key => $value) {
            $cad.=$value.'%';
        }


        $results = array();
        
        $queries = DB::table("productoalmacens")
        ->Join("productos","productoalmacens.cProdCod","=","productos.id")            
        ->Join("color","productoalmacens.id_color","=","color.id")          
        ->where('productoalmacens.nAlmCod','=',$nAlmCod)            
        ->select("color.nombre","productos.nombre_generico","productos.nombre_especifico","productoalmacens.nProAlmCod")
        //->orWhere("productos.nombre_generico","like","%".$busca."%")
        //->orWhere("color.nombre",$busca)
        ->where(DB::raw("concat(productos.nombre_generico,' ', color.nombre)"),'like',$cad)
        ->distinct()
        ->take(5)
        ->get();
        
        foreach ($queries as $query)
        {
            $results[] = [ 'nProAlmCod' => $query->nProAlmCod, 'value' => $query->nombre_generico.' '.$query->nombre, 'nombre' => $query->nombre, 'nombre_generico' =>  $query->nombre_generico, "nombre_especifico" => $query->nombre_especifico];
        }

        $queries2 = DB::table("resumen_stock_telas")
        ->Join("productos","resumen_stock_telas.producto_id","=","productos.id")            
        ->select("productos.nombre_generico","productos.nombre_especifico","resumen_stock_telas.producto_id")
        ->where("productos.nombre_generico",'like',$cad)
        ->whereNull('resumen_stock_telas.deleted_at')
        ->distinct()
        ->take(5)
        ->get();
        
        foreach ($queries2 as $query2)
        {
            $results[] = [ 'nProAlmCod' => 'TC'.$query2->producto_id, 'value' => $query2->nombre_generico.' (TC)','nombre' => '', 'nombre_generico' =>  $query2->nombre_generico, "nombre_especifico" => $query2->nombre_especifico];
        }

        $queries3 = DB::table("resumen_stock_materiaprima")
        ->Join("insumos","resumen_stock_materiaprima.insumo_id","=","insumos.id")            
        ->select("insumos.nombre_generico","insumos.nombre_especifico","resumen_stock_materiaprima.id","resumen_stock_materiaprima.cantidad","resumen_stock_materiaprima.peso_neto")
        ->where("insumos.nombre_generico",'like',$cad)
        ->where("resumen_stock_materiaprima.insumo_id",'<>',"0")
        ->whereNull('resumen_stock_materiaprima.deleted_at')
        ->distinct()
        ->take(5)
        ->get();
        
        foreach ($queries3 as $query3)
        {
            $results[] = [ 'nProAlmCod' => 'MP'.$query3->id, 'value' => $query3->nombre_generico.' (I)','nombre' => '', 'nombre_generico' =>  $query3->nombre_generico, "nombre_especifico" => $query3->nombre_especifico, "cantidad" => $query3->peso_neto, "rollos" => $query3->cantidad];
        }

        $queries4 = DB::table("resumen_stock_materiaprima")
        ->Join("accesorios","resumen_stock_materiaprima.accesorio_id","=","accesorios.id")            
        ->select("accesorios.nombre","resumen_stock_materiaprima.id","resumen_stock_materiaprima.cantidad")
        ->where("accesorios.nombre",'like',$cad)
        ->where("resumen_stock_materiaprima.accesorio_id",'<>',"0")
        ->whereNull('resumen_stock_materiaprima.deleted_at')
        ->distinct()
        ->take(5)
        ->get();
        
        foreach ($queries4 as $query4)
        {
            $results[] = [ 'nProAlmCod' => 'MP'.$query4->id, 'value' => $query4->nombre.' (A)','nombre' => '', 'nombre_generico' =>  $query4->nombre, "nombre_especifico" => '', "cantidad" => $query4->cantidad, "rollos" => "0"];
        }

        return response()->json($results);           
    }


    public function store(Request $request)
    {
        /*$this->validate($request, [
            'nombre'       => 'required',
            'ubic'       => 'required',
            'serie'       => 'required|min:3',
            'numero'       => 'required|min:6'
        ]);*/

        DB::beginTransaction();

        try 
        {   
            $trans_ultimo = Devoluciones::select('cDevCod')
                ->orderBy('cDevCod',"desc")                      
                ->get()->first();

            $conn=0;
            $dat="";
            if($trans_ultimo!="")
            {
                $dat=$trans_ultimo->cDevCod;       
                $conn=substr($dat, 2);
            }        
            //return $conn;
            $cad="ND";
            for($i=strlen(++$conn);$i<8;$i++)
            {
                $cad.="0";
            }
           
            $cad.= $conn;
            //$id=$cad;

            $id_user=access()->user()->id;
            $role_user=Rols_user::where("user_id",$id_user)->first();   
      
            $trans=new Devoluciones;
            $trans->cDevCod=$cad;
            $trans->nVtaCod=$request->codint;  
            $trans->dTotDev=$request->dTotDev;
            $trans->dTotRep=$request->dTotRep;
            $trans->dMonFavor=$request->mfavor;
            $trans->dMonContra=$request->mcontra;
            $trans->nCodUsu=access()->user()->id;
            $trans->nNomUsu=access()->user()->name;
            $trans->nCarUsu=$role_user->rol->name;
          
            $trans->save();

            //actualizando monto de venta en caja apertura ***************

            $aux_venta=venta::where('nVtaCod',"=",$request->codint)->first();

            //EVALUO SI EL SALDO ES MENOR QUE EL PAGO
            /*VENTA 50
            EVUELVE 35
            SALDO 15
            PAGO ANTES 20
            PAGO>=SALGO 
                SI ES SI 
                    -> PAGO=SALDO = 15
                    -> DEVUELVE la diferencia A FAVOR 5 //esto debe figurar en el reporte
                    -> Saldo = 0
                SI ES NO
                    -> SE DEJA EL CALCULO NORMAL
            */
            $saldo_xx=$aux_venta->dVFacVTot - $request->mfavor + $request->mcontra;
            if($aux_venta->pago>=$saldo_xx)
            {
                venta::where('nVtaCod',"=",$request->codint)
                ->update([
                    'pago' => $saldo_xx,
                    'saldo' => "0",
                    'bDevuelto' => "1"
                ]);
            }
            else
            {
                venta::where('nVtaCod',"=",$request->codint)
                ->update([
                    //'dTotMonFavor' => ($aux_totalfavor+$request->mfavor),
                    'saldo' => $saldo_xx-$aux_venta->pago,
                    'bDevuelto' => "1"
                ]);
            }

            $ca_cod=CajaVenta::where("nVtaCod",$request->codint)->first();
            
            $totalcajape=CajaApertura::select("dCajApeMonTotVen")->where('cCajApeCod',"=",$ca_cod->cCajApeCod)->first();
            
            //$total_venta=(($aux_venta->dVFacVTot + $request->mcontra) - $request->mfavor)-$aux_venta->dVFacVTot;
            
            $totatotal=(($totalcajape->dCajApeMonTotVen + $request->mcontra) - $request->mfavor);
            
            CajaApertura::where('cCajApeCod',"=",$ca_cod->cCajApeCod)
            ->update([
                'dCajApeMonTotVen' => $totatotal
            ]);

            //**********************************************************
            for($i=1;$i<=$request->conta2;$i++)
            {
                if($request["cod_ndid_".$i]!="")
                {
                    if($request["cod_ndid_".$i]=="0")//si es diferente no se ingresa
                    {

                        $tradetalle=new Prodevolver;                                         
                        $tradetalle->cDevCod=$cad;
                        if(substr($request["proalmacen_x_".$i],0,2)=="MP")
                        //if($request["mpid_".$i]=="1")
                        {
                            $tradetalle->nCodMP=substr($request["proalmacen_x_".$i],2);   
                        }
                        else
                        {
                            if(substr($request["proalmacen_x_".$i],0,2)=="TC")
                            //if($request["tcid_".$i]=="1")                                
                                $tradetalle->nCodTC=substr($request["proalmacen_x_".$i],2);   
                            else
                                $tradetalle->nProAlmCod=$request["proalmacen_x_".$i];
                        }
                        
                        $tradetalle->nDvtaCB=$request["codespro_".$i];
                        $tradetalle->nDetVtaCod=$request["cod_epd_".$i];
                        $tradetalle->cTipNDev=$request["tiponota_".$i];
                        $tradetalle->cEstPro=$request["estpro_x_".$i];                        
                        $tradetalle->nDevPeso=$request["peso2_".$i];
                        $tradetalle->nDevCant=$request["roll2_".$i];   
                                            
                        $tradetalle->save();

                        if(substr($request["proalmacen_x_".$i],0,2)=="MP")
                        //if($request["mpid_".$i]=="1")
                        {
                            $stock=Res_stock_mp::where('id',"=",substr($request["proalmacen_x_".$i],2))->first();
                            $total=0;
                            $total1=0;
                            //if($stock->producto->nombre_especifico=="TELAS")
                            //{                        
                                //if($request["estpro_x_".$i]=="NDPB")
                                    $total= $stock->cantidad + $request["roll2_".$i];
                                    $total1= $stock->peso_neto + $request["peso2_".$i];
                                //else
                                //    $totalF+= $request["peso2_".$i];

                                Res_stock_mp::where('id',"=",substr($request["proalmacen_x_".$i],2))
                                ->update([
                                'peso_neto' => $total1,
                                'cantidad' => $total
                                //'nProdAlmStockFalla' => $totalF
                                ]);
                           /* }                                                           
                            else{
                                //if($request["estpro_x_".$i]=="NDPB")
                                    $total+=$stock->rollos + $request["roll2_".$i];
                                //else
                                //    $totalF+=$request["roll2_".$i];                                    

                                Res_Stock_Telas::where('id',"=",$request["proalmacen_x_".$i])
                                ->update([
                                'rollos' => $total,
                                //'nProdAlmStockFalla' => $totalF
                                ]);
                            }  */
                        }
                        else
                        {
                            if(substr($request["proalmacen_x_".$i],0,2)=="TC")
                            //if($request["tcid_".$i]=="0")
                            {   
                                $stock=Res_Stock_Telas::where('id',"=",substr($request["proalmacen_x_".$i],2))->first();
                                $total=0;
                                $total1=0;
                                //if($stock->producto->nombre_especifico=="TELAS")
                                //{                        
                                    //if($request["estpro_x_".$i]=="NDPB")
                                        $total= $stock->rollos + $request["roll2_".$i];
                                        $total1= $stock->cantidad + $request["peso2_".$i];
                                    //else
                                    //    $totalF+= $request["peso2_".$i];

                                    Res_Stock_Telas::where('id',"=",substr($request["proalmacen_x_".$i],2))
                                    ->update([                                    
                                    'cantidad' => $total1,
                                    'rollos' => $total
                                    //'nProdAlmStockFalla' => $totalF
                                    ]);
                                //}                                                           
                                /*else{
                                    //if($request["estpro_x_".$i]=="NDPB")
                                        $total=$stock->rollos + $request["roll2_".$i];
                                        $total1= $stock->cantidad + $request["peso2_".$i];
                                    //else
                                    //    $totalF+=$request["roll2_".$i];                                    

                                    Res_Stock_Telas::where('id',"=",substr($request["proalmacen_x_".$i],2))
                                    ->update([
                                    'cantidad' => $total,
                                    'rollos' => $total
                                    //'nProdAlmStockFalla' => $totalF
                                    ]);
                                } */                             
                                
                            }
                            else
                            {
                                //------------------ Inicio devuelto stock --------------------
                                //if($request->key_stock=="0")
                                //{ 
                                //return $i;
                                
                                $stock=ProductoAlmacen::where('nProAlmCod',"=",$request["proalmacen_x_".$i])->first();
                                //return $stock->nProdAlmStock;
                                $total=$stock->nProdAlmStock;
                                $totalF=$stock->nProdAlmStockFalla;        
                                if($stock->producto->nombre_especifico=="TELAS")
                                {                        
                                    if($request["estpro_x_".$i]=="NDPB")
                                        $total+= $request["peso2_".$i];
                                    else
                                        $totalF+= $request["peso2_".$i];
                                }                                                           
                                else{
                                    if($request["estpro_x_".$i]=="NDPB")
                                        $total+=$request["roll2_".$i];
                                    else
                                        $totalF+=$request["roll2_".$i];                                    
                                }
                                                                                        
                                ProductoAlmacen::where('nProAlmCod',"=",$request["proalmacen_x_".$i])
                                ->update([
                                    'nProdAlmStock' => $total,
                                    'nProdAlmStockFalla' => $totalF
                                ]);

                                //----------------------- Fin devuelto stock ---------------------  
                            }
                        }
                        ventadetalle::where('nDetVtaCod',"=",$request["cod_epd_".$i])
                        ->update([
                            'bDevuelto' => "1"
                        ]);
                            
                        //}
                    }
                    else
                    {
                       
                    }
                }//dif empty
            } //end for


            for($i=1;$i<=$request->conta;$i++)
            {
                if($request["cod_ndi_".$i]!="")
                {
                    if($request["cod_ndi_".$i]=="0")//si es diferente no se ingresa
                    {

                        $tradetalle2=new DetalleVentaDev;               
                        $tradetalle2->cDevCod=$cad;    
                        
                        if(substr($request["proalm_".$i],0,2)=="TC")
                        {
                            $tradetalle2->nCodTC=substr($request["proalm_".$i],2);
                        }
                        else if(substr($request["proalm_".$i],0,2)=="MP")
                        {   
                            $tradetalle2->nCodMP=substr($request["proalm_".$i],2);
                        }
                        else{
                            $tradetalle2->nProAlmCod=$request["proalm_".$i];   
                        }
                        
                        $tradetalle2->nDvtaCB=$request["codb_".$i];  
                        $tradetalle2->nVtaDevPeso=$request["canti_".$i];                        
                        $tradetalle2->nVtaDevCant=$request["rollo_".$i];
                        $tradetalle2->nVtaDevPrecioU=$request["puni_".$i];
                        $tradetalle2->nVtaDevPreST=$request["stotal_".$i];
                                            
                        $tradetalle2->save();


                        if(substr($request["proalm_".$i],0,2)=="TC")
                        {
                            /*********** inicio actualizar stock alm1 ******************/            

                            $stock=Res_Stock_Telas::where('id',"=",substr($request["proalm_".$i],2))->first();

                            $save_mov=new Movimiento_Tela;
                            $save_mov->planeacion_id=1;
                            $save_mov->producto_id=$stock->producto_id;
                            $save_mov->proveedor_id=$stock->proveedor_id;
                            $save_mov->nro_lote=$stock->nro_lote;
                            $save_mov->rollos=$request["rollo_".$i];
                            $save_mov->cantidad=$request["canti_".$i];
                            $save_mov->descripcion="Devolución";
                            $save_mov->estado=0;

                            $save_mov->save();


                            Res_Stock_Telas::where('id',"=",substr($request["proalm_".$i],2))
                            ->update([
                                'cantidad' => ($stock->cantidad - $request["canti_".$i]),
                                'rollos' => ($stock->rollos - $request["rollo_".$i])
                            ]);
                            /*********** fin actualizar stock ******************/
                        }
                        else if(substr($request["proalm_".$i],0,2)=="MP")
                        {
                            /*********** inicio actualizar stock alm1 ******************/            

                            $stock=Res_stock_mp::where('id',"=",substr($request["proalm_".$i],2))->first();
                            
                            $fecha_aux=Carbon::now()->format('Y-m-d');

                            $save_movMP=new Movimiento_MP;
                            $save_movMP->fecha=$fecha_aux;
                            //$save_movMP->compra_id=1;                                
                            $save_movMP->proveedor_id=$stock->proveedor_id;
                            $save_movMP->lote=$stock->lote;
                            $save_movMP->titulo_id=$stock->titulo_id;                                
                            $save_movMP->cantidad=$request["canti_".$i];
                            $save_movMP->estado=1;
                            $save_movMP->insumo_id=$stock->insumo_id; 
                            $save_movMP->accesorio_id=$stock->accesorio_id; 
                            $save_movMP->descripcion="Devolución";
                            $save_movMP->peso_neto=$stock->peso_neto; 

                            $save_movMP->save();


                            Res_stock_mp::where('id',"=",substr($request["proalm_".$i],2))
                            ->update([                                
                                'peso_neto' => ($stock->peso_neto - $request["canti_".$i]),
                                'cantidad' => ($stock->cantidad - $request["rollo_".$i])
                            ]);
                            /*********** fin actualizar stock ******************/
                        }
                        else
                        {
                            /*********** inicio actualizar stock alm1 ******************/            

                            //if($request->key_stock=="0")
                            //{
                                
                                $stock=ProductoAlmacen::where('nProAlmCod',"=",$request["proalm_".$i])->first();
                                
                                $total=0;        
                                if($stock->producto->nombre_especifico=="TELAS")                            
                                    $total=$stock->nProdAlmStock - $request["canti_".$i];                                                           
                                else
                                    $total=$stock->nProdAlmStock - $request["rollo_".$i];                                                           
                                                                                        
                                ProductoAlmacen::where('nProAlmCod',"=",$request["proalm_".$i])->update(['nProdAlmStock' => $total]);
                                
                            //}
                            /*********** fin actualizar stock ******************/
                        }



                        
                    }
                    else
                    {
                       
                    }
                }//dif empty
            } //end for


        } 
        catch (Exception $e) 
        {

            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return redirect()->route('notadevolucion.index')->with('info','La nota de devolución se registró correctamente.'); 
    }

    public function show($id)
    {
        //SI YA SE GENERO SU NOTA NO DEBE INGRESA
        if(!Devoluciones::where('nVtaCod',$id)->exists())
            return redirect()->route('notadevolucion.index')->with('info','La venta '.$id.' NO TIENE NOTA DE DEVOLUCIÓN.');
            
        /************ Datos de Venta *****************/
        $fac=venta::where("nVtaCod",$id)->first();
        $dventa=ventadetalle::where("nVtaCod",$id)->paginate(5);

        $almacen=Almacen::where("nAlmCod",$fac->nAlmCod)->first();//sólo para cerrar el sistema a un almacén

        $almacen1=Almacen::orderby("nAlmCod","asc")->first();
        if($almacen1=="")
            return redirect()->route('venta.index')->with('info','No se puede mostrar la Nota de Devolución porque no han registrado los almacenes.');
        
        $productoAlmacen=DB::table("productoalmacens")
            ->Join("productos","productoalmacens.cProdCod","=","productos.id")            
            ->Join("color","productoalmacens.id_color","=","color.id")          
            ->where('productoalmacens.nAlmCod','=',$almacen1->nAlmCod)            
            ->select("color.nombre","productos.nombre_generico","productos.nombre_especifico","productoalmacens.nProAlmCod")
            ->distinct()
            ->get();
 
        $fecha=Carbon::now()->format('Y-m-d');
        $hora=Carbon::now()->format('h:i:s');

        $devv=Devoluciones::where("nVtaCod",$id)->first();

        $pc=DetalleVentaDev::where("cDevCod",$fac->devolucion['cDevCod'])->get();

        $pd=Prodevolver::where("cDevCod",$fac->devolucion['cDevCod'])->get();

        return view("comercializacion.notadevolucion.show",compact("fecha","hora","id","almacen","productoAlmacen","fac","dventa","pc","pd","devv"));
    }

    public function create($id)
    {    
        //SI YA SE GENERO SU NOTA NO DEBE INGRESA
        if(Devoluciones::where('nVtaCod',$id)->exists())
            return redirect()->route('notadevolucion.index')->with('info','No se puede mostrar PORQUE '.$id.' YA TIENE SU NOTA DE DEVOLUCION.');
            
    	$fac=venta::where("nVtaCod",$id)->first();

        //SI ES A CREDITO NO DEBE INGRESAR
        /*if($fac->nForPagCod==3)
            return redirect()->route('notadevolucion.index')->with('info','No se puede mostrar '.$id.' PORQUE ES UNA VENTA A CREDITO.');*/

        $id_user=access()->user()->id;
        $userX=Users::where("id",$id_user)->first(); 
        $manauto=$userX->rolsareas[0]->areasalmacen->areas->bForPag;

    	$dventa=ventadetalle::where("nVtaCod",$id)->paginate(5);
        //$almacen=Almacen::where("nAlmCod",$fac->nAlmCod)->first();//sólo para cerrar el sistema a un almacén
    	//$almacen1=Almacen::orderby("nAlmCod","asc")->first();

        $codalm=$userX->rolsareas[0]->areasalmacen->almacen->nAlmCod;
        $almacen=Almacen::where("nAlmCod",$codalm)->orderby("nAlmCod","asc")->first();//sólo para cerrar el sistema a un almacén]      
        //$almacen1=Almacen::orderby("nAlmCod","asc")->first();

        if($almacen=="")
            return redirect()->route('venta.index')->with('info','No se puede mostrar la Nota de Devolución porque no han registrado los almacenes.');
    	
        $results = array();

        //$productos=ProductoAlmacen::where("nAlmCod",$almacen1->nAlmCod)->get();
        $productoAlmacen=DB::table("productoalmacens")
            ->Join("productos","productoalmacens.cProdCod","=","productos.id")            
            ->Join("color","productoalmacens.id_color","=","color.id")          
            ->where('productoalmacens.nAlmCod','=',$almacen->nAlmCod)            
            ->select("color.nombre","productos.nombre_generico","productos.nombre_especifico","productoalmacens.nProAlmCod")
            ->distinct()
            ->get();

        foreach ($productoAlmacen as $palm)
        {
            $results[] = [ 'nProAlmCod' => $palm->nProAlmCod, 'value' => $palm->nombre_generico.' '.$palm->nombre, 'nombre_generico' =>  $palm->nombre_generico, "nombre_especifico" => $palm->nombre_especifico, "tc" => "0", "mp" => "0", "cantidad" => ''];
        }
        
        $queries = DB::table("resumen_stock_telas")
        ->Join("productos","resumen_stock_telas.producto_id","=","productos.id")            
        ->select("productos.nombre_generico","productos.nombre_especifico","resumen_stock_telas.producto_id")
        //->where("productos.nombre_generico",'like',"%".$busca."%")
        ->whereNull('resumen_stock_telas.deleted_at')
        ->distinct()
        //->take(5)
        ->get();
        
        foreach ($queries as $query)
        {
            $results[] = [ 'nProAlmCod' => $query->producto_id, 'value' => $query->nombre_generico.' (TC)', 'nombre_generico' =>  $query->nombre_generico, "nombre_especifico" => "TELAS", "tc" => "1", "mp" => "0", "cantidad" => ''];
        }

        $queries3 = DB::table("resumen_stock_materiaprima")
        ->Join("insumos","resumen_stock_materiaprima.insumo_id","=","insumos.id") 
        ->Join("titulos","resumen_stock_materiaprima.titulo_id","=","titulos.id")            
        ->select("insumos.nombre_generico","insumos.nombre_especifico","resumen_stock_materiaprima.id","resumen_stock_materiaprima.cantidad","resumen_stock_materiaprima.peso_neto","titulos.nombre")
        //->where("insumos.nombre_generico",'like',"%".$busca."%")
        ->where("resumen_stock_materiaprima.insumo_id",'<>',"0")
        ->where("resumen_stock_materiaprima.cantidad",'>',"0")
        ->whereNull('resumen_stock_materiaprima.deleted_at')
        ->distinct()
        ->take(5)
        ->get();
        
        foreach ($queries3 as $query3)
        {
            $results[] = [ 'nProAlmCod' => 'MP'.$query3->id, 'value' => $query3->nombre_generico.' '.$query3->nombre.' (I)', 'nombre_generico' =>  $query3->nombre_generico, "nombre_especifico" => $query3->nombre_especifico, "tc" => "0", "mp" => "1", "cantidad" => $query3->peso_neto, "rollos" => $query3->cantidad];
        }

        $queries4 = DB::table("resumen_stock_materiaprima")
        ->Join("accesorios","resumen_stock_materiaprima.accesorio_id","=","accesorios.id")            
        ->select("accesorios.nombre","resumen_stock_materiaprima.id","resumen_stock_materiaprima.cantidad")
        //->where("accesorios.nombre",'like',"%".$busca."%")
        ->where("resumen_stock_materiaprima.accesorio_id",'<>',"0")
        ->where("resumen_stock_materiaprima.cantidad",'>',"0")
        ->whereNull('resumen_stock_materiaprima.deleted_at')
        ->distinct()
        ->take(5)
        ->get();
        
        foreach ($queries4 as $query4)
        {
            $results[] = [ 'nProAlmCod' => 'MP'.$query4->id, 'value' => $query4->nombre.' (A)', 'nombre_generico' =>  $query4->nombre, "nombre_especifico" => '', "tc" => "0", "mp" => "1", "cantidad" => $query4->cantidad, "rollos" => "0"];
        }
   
        $fecha=Carbon::now()->format('Y-m-d');
        $hora=Carbon::now()->format('h:i:s');

        

        /************* numeración factura *************/

        /*$num_fac=$almacen->nAlmNumAct;

        $cad2="";
        for($i=strlen(++$num_fac);$i<6;$i++)
        {
            $cad2.="0";
        }
        $cad2.=$num_fac;*/

    	return view("comercializacion.notadevolucion.create",compact("fecha","hora","id","almacen","results","fac","dventa","manauto"));       
    }

    public function index(Request $request)
    {
        $userLogueado = Auth::user();
        $userLogueado =Users::with("rolsareas", "rolsareas.areaAlmacen", "rolsareas.areaAlmacen.almacen")->find($userLogueado->id);
        $rolsAreas = $userLogueado->rolsareas;
        //echo "<pre>";
        //print_r($rolsAreas);
        //echo "</pre>";
        //dd();
        $codAlmacen = "";
        if (isset($rolsAreas[0])) {
            $codAlmacen = $rolsAreas[0]->nAreAlmCod;
        }
        $rq=$request;
        $fecha=Carbon::now()->format('Y-m-d');
        if(trim($request->ngven_b)==""){
            if(trim($request->fech_b)=="")
                $request->fech_b=$fecha;
            
            $vfacturas=venta::orderBy("nVtaCod","desc")->name($request->fech_b,$request->tdoc_b,$request->fpago_b/*,$request->ngven_b*/,$request->rdni,$request->tie_b);
            if ($codAlmacen !="") {
                $vfacturas->where("nAlmCod", $codAlmacen);
            }
            $vfacturas = $vfacturas->paginate(10); 
            //print_r($vfacturas->toArray());
            //dd();
        }
        else{
           $vfacturas=venta::orderBy("nVtaCod","desc")->credito($request->ngven_b);
           if ($codAlmacen !="") {
                dd("else");
                $vfacturas->where("nAlmCod", $codAlmacen);
            }
           $vfacturas = $vfacturas->paginate(10);       
        }
    	
        $tdoc=tipodocv::all();
        $fpago=forpago::all();
        $cli=Cliente::all();  	

    	return view("comercializacion.notadevolucion.index",compact("vfacturas","rq","tdoc","fpago","cli"));
    }

    function reporte($id)
    {
        //SI YA SE GENERO SU NOTA NO DEBE INGRESA
        if(!Devoluciones::where('nVtaCod',$id)->exists())
            return redirect()->route('notadevolucion.index')->with('info','La venta '.$id.' NO TIENE NOTA DE DEVOLUCIÓN.');
        
        //$fac=venta::where("nVtaCod",$id)->first();
        $vfacturas=venta::where("anulado","0")->where("nVtaCod",$id)->first();
        
        $devv=Devoluciones::where("nVtaCod",$id)->first();

        $pc=DetalleVentaDev::where("cDevCod",$vfacturas->devolucion['cDevCod'])->get();
        $pd=Prodevolver::where("cDevCod",$vfacturas->devolucion['cDevCod'])->get();

        $dia=Carbon::now()->format('d');  
        $mes=Carbon::now()->format('m');  
        $anio=Carbon::now()->format('Y');  

        $mes_n="";
        switch ($mes) {
            case '01': $mes_n="Enero";break;
            case '02': $mes_n="Febrero";break;
            case '03': $mes_n="Marzo";break;
            case '04': $mes_n="Abril";break;
            case '05': $mes_n="Mayo";break;
            case '06': $mes_n="Junio";break;
            case '07': $mes_n="Julio";break;
            case '08': $mes_n="Agosto";break;
            case '09': $mes_n="Setiembre";break;
            case '10': $mes_n="Octubre";break;
            case '11': $mes_n="Noviembre";break;
            case '12': $mes_n="Diciembre";break;
        }

 
        $view =  \View::make('comercializacion.notadevolucion.reporte', compact("vfacturas","dia","mes","anio","mes_n","devv","pc","pd"))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper([5, -33, 375.465, 715.276], 'landscape');//margen izq.- arriba, alto,ancho//->setPaper('a4', 'landscape');
        return $pdf->stream('invoice');

        /*$date = date('Y-m-d');      
        //$view =  \View::make('pdf.invoice', compact('data', 'date', 'invoice'))->render();
        $view =  \View::make('comercializacion.notadevolucion.reporte', compact("date"))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a5', 'landscape');
        return $pdf->stream('invoice');*/
    }

    public function recusuarios(Request $request)
    {
        if($request->ajax()){
            $cliente_x=cliente::all();
            return response()->json($cliente_x);
        }
    }

    public function bbancos(Request $request)
    {
        if($request->ajax()){
            $banco_x=Banco::all();
            return response()->json($banco_x);
        }
    }

    /*public function prostock(Request $request,$id)
    {
        if($request->ajax()){
            $prod_x=ProductoAlmacen::select("nProdAlmStock","nProdAlmPuni")->where('nProAlmCod',$id)->first();
            return response()->json($prod_x);
        }
    }*/

    Public function prostock(Request $request,$id,$cli)
    {
        if($request->ajax()){
            
            $registro_x=DB::table("ventas")
            ->Join("detalleventa","detalleventa.nVtaCod","=","ventas.nVtaCod")                        
            ->where('ventas.nClieCod','=',$cli)            
            ->where('detalleventa.nProAlmCod','=',$id)            
            ->select("detalleventa.nVtaPrecioU")            
            ->first();
            //return $registro_x;
            if($registro_x=="")
            {
                $prod_x=ProductoAlmacen::select("nProdAlmStock","nProdAlmPuni")->where('nProAlmCod',$id)->first();    
            }
            else
            {
                $prod_x=DB::table("ventas")
                ->Join("detalleventa","detalleventa.nVtaCod","=","ventas.nVtaCod")                        
                ->Join("productoalmacens","productoalmacens.nProAlmCod","=","detalleventa.nProAlmCod")                        
                ->where('ventas.nClieCod','=',$cli)            
                ->where('detalleventa.nProAlmCod','=',$id)            
                ->select("detalleventa.nVtaPrecioU as nProdAlmPuni","productoalmacens.nProdAlmStock")
                ->orderby("nDetVtaCod","desc")            
                ->first();
            }

            return response()->json($prod_x);
        }
    }

    public function productoalm(Request $request,$id)
    {
        if($request->ajax()){

            $results = array();

            //$productos=ProductoAlmacen::where("nAlmCod",$almacen1->nAlmCod)->get();
            $prod_x=DB::table("productoalmacens")
            ->Join("productos","productoalmacens.cProdCod","=","productos.id")            
            ->Join("color","productoalmacens.id_color","=","color.id")          
            ->where('productoalmacens.nAlmCod','=',$id)            
            ->select("color.nombre","productos.nombre_generico","productos.nombre_especifico","productoalmacens.nProAlmCod")
            ->distinct()
            ->get();

            foreach ($prod_x as $palm)
            {
                $results[] = [ 'nProAlmCod' => $palm->nProAlmCod, 'value' => $palm->nombre_generico.' '.$palm->nombre, 'nombre_generico' =>  $palm->nombre_generico, "nombre_especifico" => $palm->nombre_especifico, "tc" => "0", "mp" => "0", "cantidad" => ''];
            }
            
            $queries = DB::table("resumen_stock_telas")
            ->Join("productos","resumen_stock_telas.producto_id","=","productos.id")            
            ->select("productos.nombre_generico","productos.nombre_especifico","resumen_stock_telas.producto_id")
            //->where("productos.nombre_generico",'like',"%".$busca."%")
            ->whereNull('resumen_stock_telas.deleted_at')
            ->distinct()
            //->take(5)
            ->get();
            
            foreach ($queries as $query)
            {
                $results[] = [ 'nProAlmCod' => $query->producto_id, 'value' => $query->nombre_generico.' (TC)', 'nombre_generico' =>  $query->nombre_generico, "nombre_especifico" => $query->nombre_especifico, "tc" => "1", "mp" => "0", "cantidad" => ''];
            }

            $queries3 = DB::table("resumen_stock_materiaprima")
            ->Join("insumos","resumen_stock_materiaprima.insumo_id","=","insumos.id")  
            ->Join("titulos","resumen_stock_materiaprima.titulo_id","=","titulos.id")            
            ->select("insumos.nombre_generico","insumos.nombre_especifico","resumen_stock_materiaprima.id","resumen_stock_materiaprima.cantidad","resumen_stock_materiaprima.peso_neto","titulos.nombre")
            //->where("insumos.nombre_generico",'like',"%".$busca."%")
            ->where("resumen_stock_materiaprima.insumo_id",'<>',"0")
            ->where("resumen_stock_materiaprima.cantidad",'>',"0")
            ->whereNull('resumen_stock_materiaprima.deleted_at')
            ->distinct()
            ->take(5)
            ->get();
            
            foreach ($queries3 as $query3)
            {
                $results[] = [ 'nProAlmCod' => 'MP'.$query3->id, 'value' => $query3->nombre_generico.' '.$query3->nombre.' (I)', 'nombre_generico' =>  $query3->nombre_generico, "nombre_especifico" => $query3->nombre_especifico, "tc" => "0", "mp" => "1", "cantidad" => $query3->peso_neto, "rollos" => $query3->cantidad];
            }

            $queries4 = DB::table("resumen_stock_materiaprima")
            ->Join("accesorios","resumen_stock_materiaprima.accesorio_id","=","accesorios.id")            
            ->select("accesorios.nombre","resumen_stock_materiaprima.id","resumen_stock_materiaprima.cantidad")
            //->where("accesorios.nombre",'like',"%".$busca."%")
            ->where("resumen_stock_materiaprima.accesorio_id",'<>',"0")
            ->where("resumen_stock_materiaprima.cantidad",'>',"0")
            ->whereNull('resumen_stock_materiaprima.deleted_at')
            ->distinct()
            ->take(5)
            ->get();
            
            foreach ($queries4 as $query4)
            {
                $results[] = [ 'nProAlmCod' => 'MP'.$query4->id, 'value' => $query4->nombre.' (A)', 'nombre_generico' =>  $query4->nombre, "nombre_especifico" => '', "tc" => "0", "mp" => "1", "cantidad" => $query4->cantidad, "rollos" => "0"];
            }

            return response()->json($results);           
        }
    }

    public function promedida(Request $request,$id)
    {
        if($request->ajax()){
            $prodmedida_x=DB::table('productos')
            ->leftJoin("medidaproductos","productos.cProdCod","=","medidaproductos.cProdCod")
            ->leftJoin("medidas","medidaproductos.nMedCod","=","medidas.nMedCod")
            ->select("medidas.nMedCod","medidaproductos.nMedProCod","medidas.cMedNom","medidaproductos.nMedProCanUni","medidaproductos.dMedProPor","medidas.cMedUnis")
            ->where('medidaproductos.cProdCod',$id)
            ->where('medidaproductos.bMedProIntExt',"1")
            ->where('medidaproductos.activo',"1")
            ->get();
            return response()->json($prodmedida_x);         
        }
    }

    public function ofermedida(Request $request,$id)
    {
        if($request->ajax()){
            $ofermedida_x=DB::table('medidaproductos')
            ->leftJoin("productos","productos.cProdCod","=","medidaproductos.cProdCod")
            //->leftJoin("medidas","medidaproductos.nMedCod","=","medidas.nMedCod")
            ->select("medidaproductos.dMedProPor","productos.dProdPpubl")
            ->where('medidaproductos.nMedProCod',$id)
            ->first();

            return response()->json($ofermedida_x);         
        }
    }

     public function veri_codbarra($id,$alm1)
    {
        $datos=DB::table('detalle_nota_ingreso')
        ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso.nProAlmCod")
        ->leftJoin("productos","productos.id","=","productoalmacens.cProdCod")
        ->leftJoin("color","color.id","=","productoalmacens.id_color")
        ->leftJoin("almacens","almacens.nAlmCod","=","productoalmacens.nAlmCod")
        ->where(DB::raw("REPLACE(detalle_nota_ingreso.cod_barras,'-','')"),"=",$id)
        ->orWhere("detalle_nota_ingreso.cod_barras","=",$id)
        //->where("detalle_nota_ingreso.cod_barras","=",$id)
        ->where("productoalmacens.nAlmCod","=",$alm1)   
        ->select("productoalmacens.nProAlmCod","productos.nombre_generico","productos.nombre_especifico","color.nombre","productoalmacens.nProdAlmStock","detalle_nota_ingreso.peso_cant","almacens.nAlmCod","almacens.cAlmNom","almacens.cAlmUbi","detalle_nota_ingreso.rollo","productoalmacens.nAlmCod","productoalmacens.nProdAlmPuni")
        ->get();        
        $dat=json_decode($datos, true);
        if(empty($dat))
        {           
            //$datos=detalle_nota_ingreso_a::where("cod_barras","=",$id)->get();
            $datos=DB::table('detalle_nota_ingreso_a')
            ->leftJoin("productoalmacens","productoalmacens.nProAlmCod","=","detalle_nota_ingreso_a.nProAlmCod")
            ->leftJoin("productos","productos.id","=","productoalmacens.cProdCod")
            ->leftJoin("color","color.id","=","productoalmacens.id_color")
            ->leftJoin("almacens","almacens.nAlmCod","=","productoalmacens.nAlmCod")
            ->where(DB::raw("REPLACE(detalle_nota_ingreso_a.cod_barras,'-','')"),"=",$id)
            ->orWhere("detalle_nota_ingreso_a.cod_barras","=",$id)
            //->where("detalle_nota_ingreso_a.cod_barras","=",$id)
            ->where("productoalmacens.nAlmCod","=",$alm1)   
            ->select("productoalmacens.nProAlmCod","productos.nombre_generico","productos.nombre_especifico","color.nombre","productoalmacens.nProdAlmStock","detalle_nota_ingreso_a.peso_cant","almacens.nAlmCod","almacens.cAlmNom","almacens.cAlmUbi","detalle_nota_ingreso_a.rollo","productoalmacens.nAlmCod","productoalmacens.nProdAlmPuni")
            ->get(); 
        }

        return $datos;
    }

    public function combo_lotes(Request $request,$id)
    {
        if($request->ajax()){
            $prod_x=DB::table("resumen_stock_telas")  
            ->Join("productos","resumen_stock_telas.producto_id","=","productos.id")                  
            ->where('producto_id','=',$id)   
            ->where('cantidad','>','0')             
            ->select("resumen_stock_telas.id","resumen_stock_telas.cantidad","resumen_stock_telas.rollos","resumen_stock_telas.nro_lote")//,"productos.dPreUniPro")
            //->distinct()
            ->get();

            return response()->json($prod_x);           
        }
    }


}
