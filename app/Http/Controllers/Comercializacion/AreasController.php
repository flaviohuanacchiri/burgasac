<?php

namespace App\Http\Controllers\Comercializacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Areas;
use App\Modulos;
USE App\RolAreaModulo;
use DB;

class AreasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$areas=Areas::orderBy("nAreCod","ASC")->paginate();
    	return view("comercializacion.area.index",compact("areas"));
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
            'nombre'       => 'required'
        ]);

        DB::beginTransaction();

        try 
        { 
        	$area=new Areas;
            $area->cAreNom=$request->nombre;
            $area->bForPag=$request->tipdoc;     
            //$tipo->tTipoFMod=$request->fmod;   
            $area->save();

            $codarea=Areas::orderBy("nAreCod","desc")->first();

            for($i=1;$i<=$request->conta;$i++)
            {
                if($request["cod_ndi_".$i]!="")
                {
                    if($request["cod_ndi_".$i]==0)//si es diferente no se ingresa
                    {                    
                        $tradetalle=new RolAreaModulo;
                        $tradetalle->nAreCod=$codarea->nAreCod;                   
                        $tradetalle->nCodMod=$request["mod_".$i];
                        
                        $tradetalle->save();
                    }
                    else
                    {
                       
                    }
                }//dif empty
            }//end for

        } 
        catch (Exception $e) 
        {

            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return redirect()->route('area.index')->with('info','El área fue creado.');  
    }

    public function update(Request $request,$id)
    {

    	$this->validate($request, [
            'nombre'       => 'required'
        ]);

        try 
        { 
            //eliminar los registro que se quitaron y actualizar stock
            $arrayElim=explode(",", $request->eliminados);

            for($j=1;$j<count($arrayElim);$j++)
            {              
                RolAreaModulo::where('nRolAreMod','=',$arrayElim[$j])->delete(); 
            }

            Areas::where('nAreCod',"=",$id)
            ->update([
                'cAreNom' => $request->nombre,
                'bForPag' => $request->tipdoc
                //'tTipoFMod' => $request->fmod
                ]); 
        
            for($i=1;$i<=$request->conta;$i++)
            {
                if($request["cod_ndi_".$i]!="")
                {
                    if($request["cod_ndi_".$i]==0)//si es diferente no se ingresa
                    {                    
                        $tradetalle=new RolAreaModulo;
                        $tradetalle->nAreCod=$id;                   
                        $tradetalle->nCodMod=$request["mod_".$i];
                        
                        $tradetalle->save();
                    }
                    else
                    {
                       
                    }
                }//dif empty
            }//end for

        } 
        catch (Exception $e) 
        {

            DB::rollback();
            return redirect()->route('area.edit',$id)->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return redirect()->route('area.edit',$id)->with('info','El área fue actualizado.');  
    }

    public function show($id)
    {
    	$ar=Areas::where('nAreCod','=',$id)->get()->first();
    	return view("comercializacion.area.show",compact("ar"));
    }

    public function edit($id)
    {
    	$ar=Areas::where('nAreCod','=',$id)->get()->first();
        $mod=Modulos::all();
        $det=RolAreaModulo::where("nAreCod",$id)->get();

    	return view("comercializacion.area.edit",compact("ar","mod","det"));
    }

    public function destroy($id)
    {
        DB::beginTransaction();

        try 
        { 
            $arX=Areas::where('nAreCod','=',$id)->first();

            $a=DB::table("areas_almacen")
            //->Join("areas_almacen","rols_areas.nAreAlmCod","=","areas_almacen.nAreAlmCod") 
            ->where('nAreCod','=',$id)->select("nAreAlmCod")->get();
            
            if(sizeof($a)!=0)                                
                return redirect()->route('area.index',$id)->with('info','Error: No se pudo eliminar porque el AREA('.$arX->cAreNom.') está asignado a un almacén.');

            $v=DB::table("rols_areas")
            ->Join("areas_almacen","rols_areas.nAreAlmCod","=","areas_almacen.nAreAlmCod") 
            ->where('areas_almacen.nAreCod','=',$id)->select("rols_areas.nRolAreCod")->get();
            
            if(sizeof($v)!=0)                                
                return redirect()->route('area.index',$id)->with('info','Error: No se pudo eliminar porque el AREA('.$arX->cAreNom.') está relacionado con algún usuario.');
            
            $det=RolAreaModulo::where("nAreCod",$id);
            $det->delete();

            $ar=Areas::where('nAreCod','=',$id);
            $ar->delete();
        } 
        catch (Exception $e) 
        {

            DB::rollback();
            return redirect()->route('area.index',$id)->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();
        
        return redirect()->route('area.index',$id)->with('info','El Áreas fue eliminado.');
    }

    public function create()
    {
        $mod=Modulos::all();
    	return view("comercializacion.area.create",compact("mod"));
    }
}
