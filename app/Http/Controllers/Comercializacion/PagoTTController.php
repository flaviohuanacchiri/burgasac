<?php

namespace App\Http\Controllers\Comercializacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\pago_compra;
use App\Proveedor;
use App\CompraTelat;
use App\detalle_pago_compra;
use App\Users;
use App\CajaApertura;
use DB;
use Carbon\Carbon;

class PagoTTController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
    	$pc=CompraTelat::where("nCodProv","like","%".$request->proveedor."%")
        ->whereNull("deleted_at")
        ->paginate(10);
    	

    	$proveedores = Proveedor::all();
    	$datosant = $request;

    	return view("comercializacion.pagoTT.index",compact("pc","proveedores","datosant"));
    }

    public function create($id)
    {
    	$id_user=access()->user()->id;
		$userX=Users::where("id",$id_user)->first();   
		$apertura=CajaApertura::where("nRolAreCod",$userX->rolsareas[0]->nRolAreCod)->where("bCajCieAbierto","0")->first();

        if($apertura!="")//evluar si ya aperturo una caja, si si redirigirlo a cierre
        {     
            $saldo=$apertura->dCajApeMonTotVen - $apertura->dCajApeTotTraSal + $apertura->dCajApeTotTraIng - $apertura->dCajApeTotEgreso;
        }
        else
	    {
	        return redirect()->route('caja.abrircaja')->with('info','Debe aperturar la caja primero.');
	    }

    	$pc=pago_compra::where("nCodPagComp",$id)->first();
  
        return view("comercializacion.pagoTT.create",compact("pc","id","saldo"));
    }

    public function show($id)
    {
        $pc=pago_compra::where("nCodPagComp",$id)->first();
        return view("comercializacion.pagoTT.show",compact("pc"));
    }

    public function store(Request $request)
    {     
    	
        DB::beginTransaction();

        try 
        {   
            if($request["tot_g_i"]<=0)
            {
                DB::rollback();
                return redirect()->back()->with('info','No se puede registrar si el total es 0.00');  
            }

            $fechahora=Carbon::now()->format('Y-m-d h:i:s');
        
            $codpagcom=pago_compra::where("nCodPagComp","=",$request["id"])->first();

            pago_compra::where("nCodPagComp","=",$request["id"])
            ->update([
            'dPagoTotal' => $codpagcom->dPagoTotal + $request["tot_g_i"],
            'dPagoSaldo' => $codpagcom->dPagoSaldo - $request["tot_g_i"]
            ]); 

            $id_user=access()->user()->id;
            $userX=Users::where("id",$id_user)->first();   
            $apertura=CajaApertura::where("nRolAreCod",$userX->rolsareas[0]->nRolAreCod)->where("bCajCieAbierto","0")->first();

            for($j=1;$j<=$request->conta;$j++)
            {
                if($request["cod_ndi_".$j]!="")
                {
                    if($request["cod_ndi_".$j]=="0")
                    {
                        $detpagcom=new detalle_pago_compra;                                          
                        $detpagcom->tFecha=$fechahora;
                        $detpagcom->dMontoPag=$request["monP_".$j];                        
                        $detpagcom->bCaja=($request["caja_i"]=="1")?"1":"0";
                        $detpagcom->cCaja=$apertura->cCajApeCod;
                        $detpagcom->name=$userX->name.'|'.$userX->rolsareas[0]->areasalmacen->areas->cAreNom;
                        $detpagcom->nCodPagComp=$request["id"];
                                            
                        $detpagcom->save();   
                    }
                }
            }
            
            if($request["caja_i"]=="1")           
            {   

                if($apertura!="")//evluar si ya aperturo una caja, si si redirigirlo a cierre
                {     
                    $saldo=$apertura->dCajApeMonTotVen - $apertura->dCajApeTotTraSal + $apertura->dCajApeTotTraIng - $apertura->dCajApeTotEgreso;
                    if($saldo>=$request["tot_g_i"])
                    {
                        CajaApertura::where("nRolAreCod",$userX->rolsareas[0]->nRolAreCod)->where("bCajCieAbierto","0")->first()
                        ->update([
                            'dCajApeTotEgreso' => $apertura->dCajApeTotEgreso + $request["tot_g_i"]
                            ]); 
                    }
                    else
                    {
                        DB::rollback();
                        return redirect()->route('pagoTT.index')->with('info','Su saldo disponible en Caja es: S/.'.$saldo.', No es suficiente, no se efectuó el registro.');  
                    }
                }
                else
                {
                    DB::rollback();
                    return redirect()->route('caja.abrircaja')->with('info','Debe aperturar la caja primero.');             
                }
            }
            
        } 
        catch (Exception $e) 
        {
            DB::rollback();
            return redirect()->back()->with('info','Ocurrió un error vuelva a intentarlo.');    
        }

        DB::commit();

        return redirect()->route('pagoTT.index')->with('info',"Se registró correctamente..."); 

    }
    
}
