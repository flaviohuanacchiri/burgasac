<?php

namespace App\Http\Controllers\Comercializacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modulos;
use App\RolAreaModulo;
use DB;

class ModulosControllers extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$modulosx=Modulos::orderBy("nCodMod","ASC")->paginate(10);
        
    	return view("comercializacion.modulos.index",compact("modulosx"));
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
            'nombre'       => 'required',
            'urlx'       => 'required'
        ]);

    	$mod=new Modulos;
        $mod->cNomMod=trim($request->nombre);
        $mod->cDesMod=trim($request->desc);
        $mod->url=trim($request->urlx);
      
        $mod->save();

        return redirect()->route('modulos.index')->with('info','El Módulo fue creado.');  
    }

    public function update(Request $request,$id)
    {

    	$this->validate($request, [
            'nombre'       => 'required',
            'urlx'       => 'required'
        ]);

        Modulos::where('nCodMod',"=",$id)
        ->update([
        	'cNomMod' => trim($request->nombre),
        	'cDesMod' => trim($request->desc),
        	'url' => trim($request->urlx)
        	]); 

        return redirect()->route('modulos.show',$id)->with('info','El Módulo fue actualizado.');  
    }

    public function show($id)
    {
    	$ar=Modulos::where('nCodMod','=',$id)->get()->first();
        
    	return view("comercializacion.modulos.show",compact("ar"));
    }

    public function edit($id)
    {
    	$ar=Modulos::where('nCodMod','=',$id)->get()->first();
        
    	return view("comercializacion.modulos.edit",compact("ar"));
    }

    public function destroy($id)
    {         
        if(RolAreaModulo::where("nCodMod",$id)->exists())                                
            return redirect()->route('modulos.index')->with('info','Error: No se pudo eliminar el módulo porque está asignado a alguna(s) AREA.');

        $ar=Modulos::where('nCodMod','=',$id);
        $ar->delete();
        return redirect()->route('modulos.index')->with('info','El Módulo fue eliminado.');
    }

    public function create()
    {        
    	return view("comercializacion.modulos.create");
    }
}
