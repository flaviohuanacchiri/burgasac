<?php

namespace App\Http\Controllers\Comercializacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Producto;
use App\venta;
use Carbon\Carbon;
use App\Almacen;
use App\Cliente;
use App\Color;
use App\ProductoAlmacen;
use App\tipodocv;
use DB;

class ReporteComercialController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function stockcolores_create()
    {
        $productos=Producto::all();
        return view("comercializacion.reporteria.stockcolores",compact("productos"));
    }

	function stockcolores(Request $request)
    {    	
    	$nompro=Producto::where("id",$request->selprod)->first();
    	$prodalm_Bus=DB::table('productoalmacens')
        ->join("productos","productos.id","=","productoalmacens.cProdCod")
        ->join("color","color.id","=","productoalmacens.id_color")
        ->select("productos.nombre_generico","productos.nombre_especifico","color.nombre","color.id","productoalmacens.nProdAlmStock","productoalmacens.nAlmCod")
        ->where("productos.id","=",$request->selprod)
        ->distinct("productos.id")
        ->get();

        $color_Bus=DB::table('productoalmacens')
        ->join("productos","productos.id","=","productoalmacens.cProdCod")
        ->join("color","color.id","=","productoalmacens.id_color")
        ->select("color.nombre","color.id","productos.nombre_especifico")
        ->where("productos.id","=",$request->selprod)
        ->distinct("color.id")
        ->get();

        $tienda=Almacen::orderBy("nAlmCod","asc")->get();

        $fecha=Carbon::now()->format('Y-m-d h:i:s');
  
        $view =  \View::make('comercializacion.reporteria.rpt_stockcolores', compact("prodalm_Bus","fecha","nompro","tienda","color_Bus"))->render();
        $pdf = \App::make('dompdf.wrapper');
        //$pdf->loadHTML($view)->setPaper([5, 0, 630, 630], 'landscape');//->setPaper('a4', 'potrait');
        $pdf->loadHTML($view)->setPaper('a4', 'landscape');
        return $pdf->stream('invoice');   
              
    }  

    public function ventaproducto_create()
    {
        $productos=Producto::all();
        $cli=Cliente::all();
        $tiendas=Almacen::all();
        return view("comercializacion.reporteria.ventaproducto",compact("productos","cli","tiendas"));
    }

	function ventaproducto(Request $request)
    {    	
    	/*$this->validate($request, [
            'prod'       => 'required',
            //'alm2'       => 'required'
        ]);*/

        $nuevafecha1 = strtotime ( '-1 day' , strtotime ( $request->fech_i ) ) ;
        $nuevafecha1 = date ( 'Y-m-j' , $nuevafecha1 );
    
        $nuevafecha2 = strtotime ( '+1 day' , strtotime ( $request->fech_f ) ) ;
        $nuevafecha2 = date ( 'Y-m-j' , $nuevafecha2 );

    	$res_venta=DB::table('ventas')
        ->join("detalleventa","ventas.nVtaCod","=","detalleventa.nVtaCod")
        ->join("productoalmacens","detalleventa.nProAlmCod","=","productoalmacens.nProAlmCod")
        ->join("almacens","productoalmacens.nAlmCod","=","almacens.nAlmCod")
        ->join("clientes","ventas.nClieCod","=","clientes.nClieCod")
        ->join("tipodocpago","ventas.nTipPagCod","=","tipodocpago.nTipPagCod")
        ->join("formapago","ventas.nForPagCod","=","formapago.nForPagCod")
        ->join("productos","productoalmacens.cProdCod","=","productos.id")
        ->join("color","productoalmacens.id_color","=","color.id")

        ->where("ventas.dVFacFemi",">=", $nuevafecha1 )
        ->where("ventas.dVFacFemi","<=",$nuevafecha2)
        ->where("ventas.nClieCod","like","%".$request->rdni."%")
        ->where("almacens.nAlmCod","like","%".$request->tien."%")
        ->where("productos.id","like","%".$request->prod."%")

        ->select(
        	"ventas.dVFacFemi",
        	"ventas.nVtaCod",
        	"clientes.cClieDesc",
        	"almacens.cAlmNom",
        	"tipodocpago.cDescTipPago",
        	"formapago.cDescForPag",
        	"productos.nombre_generico",
        	"productos.material",
        	"color.nombre",
        	"detalleventa.nVtaPeso",
        	"detalleventa.nVtaCant",
        	"detalleventa.nVtaPrecioU",
        	"detalleventa.nVtaPreST",
        	"productos.nombre_especifico"
        )               
        //->distinct("productos.id")
        ->get();

        //return compact("res_venta");

        $fecha=Carbon::now()->format('Y-m-d h:i:s');

        if(trim($request->tien)=="")
        {
        	$v_tienda="Todas las Tiendas.";	
        }
        else
        {
		    $v_tienda_x=Almacen::where("nAlmCod",$request->tien)->first();
		    $v_tienda=$v_tienda_x->cAlmNom;
    	}

    	if(trim($request->prod)=="")
        {
        	$v_prod="Todos los Productos.";	
        }
        else
        {
		    $v_prod_x=Producto::where("id",$request->prod)->first();
  			$v_prod=$v_prod_x->nombre_generico.' - '.$v_prod_x->nombre_especifico;
    	}
        

        $view =  \View::make('comercializacion.reporteria.rpt_ventaproducto', compact("res_venta","fecha","request","v_tienda","v_prod"))->render();
        $pdf = \App::make('dompdf.wrapper');
        //$pdf->loadHTML($view)->setPaper([5, 0, 630, 630], 'landscape');//->setPaper('a4', 'potrait');
        $pdf->loadHTML($view)->setPaper('a4', 'landscape');
        return $pdf->stream('invoice');   
              
    }    

    public function productoalm(Request $request,$id)
    {
    	if($request->ajax()){
    		$prod_x=DB::table("productoalmacens")
            ->Join("productos","productoalmacens.cProdCod","=","productos.id")            
            ->Join("color","productoalmacens.id_color","=","color.id")          
            ->where('productoalmacens.nAlmCod','=',$id)            
            ->select("color.nombre","productos.nombre_generico","productos.nombre_especifico","productoalmacens.nProAlmCod","productoalmacens.nProdAlmStock","productos.id")
            ->distinct()
            ->get();

    		return response()->json($prod_x);    		
    	}
    } 

    public function resumenventames_create()
    {
        $cli=Cliente::all();     
        return view("comercializacion.reporteria.resumenventames",compact("cli"));
    }

	function resumenventames(Request $request)
    {   
    	$nomclie="";

    	if(trim($request->clie)=="")
    	{
    		$nomclie="Todos los clientes";
    	}
    	else
    	{
    		$c_x=Cliente::where("nClieCod",$request->clie)->first();   	
    		$nomclie=$c_x->cClieDesc;
    	}

    	$array;

    	$result=Cliente::where("nClieCod","like","%".trim($request->clie)."%")->get();
    	foreach ($result as $key => $value) {
    		$array_p;    	
	    	//$c=Cliente::where("nClieCod",$request->clie)->first();
	    	$array_p[0]=$value->cClieDesc;
	    	for ($i=1; $i <= 12; $i++) 
	    	{ 
	    		$res=venta::select(DB::raw("SUM(dVFacVTot) as total"))->where("nClieCod",$value->nClieCod)->where((DB::raw("MONTH(dVFacFemi)")),"=",$i)->where((DB::raw("YEAR(dVFacFemi)")),"=",$request->anio)->first();	
	    		$array_p[$i]=((trim($res->total)=="")?0:$res->total);
	    	}

	    	$array[$key]=($array_p);
    	}
    	    
        $fecha=Carbon::now()->format('Y-m-d h:i:s');

        $view =  \View::make('comercializacion.reporteria.rpt_resumenventames', compact("array","request","fecha","nomclie"))->render();
        $pdf = \App::make('dompdf.wrapper');        
        $pdf->loadHTML($view)->setPaper('a4', 'landscape');
        return $pdf->stream('invoice');   
              
    }   

    public function resumentienda_create()
    {
        $alm=Almacen::all();     
        return view("comercializacion.reporteria.resumentienda",compact("alm"));
    }

	function resumentienda(Request $request)
    {   
    	$nomclie="";

    	if(trim($request->tie)=="")
    	{
    		$nomclie="Todos las Tiendas";
    	}
    	else
    	{
    		$c_x=Almacen::where("nAlmCod",$request->tie)->first();   	
    		$nomclie=$c_x->cAlmNom;
    	}

    	$array;

    	$result=Almacen::where("nAlmCod","like","%".trim($request->tie)."%")->get();
    	foreach ($result as $key => $value) {
    		$array_p;    	
	    	
	    	$array_p[0]=$value->cAlmNom;
	    	for ($i=1; $i <= 12; $i++) 
	    	{ 
	    		$res=venta::select(DB::raw("SUM(dVFacVTot) as total"))->where("nAlmCod",$value->nAlmCod)->where((DB::raw("MONTH(dVFacFemi)")),"=",$i)->where((DB::raw("YEAR(dVFacFemi)")),"=",$request->anio)->first();	
	    		$array_p[$i]=((trim($res->total)=="")?0:$res->total);
	    	}

	    	$array[$key]=($array_p);
    	}
    	    
        $fecha=Carbon::now()->format('Y-m-d h:i:s');

        $view =  \View::make('comercializacion.reporteria.rpt_resumentienda', compact("array","request","fecha","nomclie"))->render();
        $pdf = \App::make('dompdf.wrapper');        
        $pdf->loadHTML($view)->setPaper('a4', 'landscape');
        return $pdf->stream('invoice');   
              
    } 

    public function detvenprod_create()
    {
        $productos=Producto::all();
        $cli=Cliente::all();
        $tiendas=Almacen::all();
        $color=Color::all();
        return view("comercializacion.reporteria.detvenprod",compact("productos","cli","tiendas","color"));
    }

	function detvenprod(Request $request)
    {  
    	/*$array;

    	$result=Almacen::where("nAlmCod","like","%".trim($request->tie)."%")->get();
    	foreach ($result as $key => $value) {
    		$array_p;    	
	    	
	    	$array_p[0]=$value->cAlmNom;
	    	 
    		$res=venta::select(DB::raw("SUM(dVFacVTot) as total"))
    		->where("nAlmCod",$value->nAlmCod)
    		->where((DB::raw("MONTH(dVFacFemi)")),"=",$i)
    		->where((DB::raw("YEAR(dVFacFemi)")),"=",$request->anio)
    		->first();	
    		$array_p[$i]=((trim($res->total)=="")?0:$res->total);
    	
	    	$array[$key]=($array_p);
    	}*/

        $nuevafecha1 = strtotime ( '-1 day' , strtotime ( $request->fech_i ) ) ;
        $nuevafecha1 = date ( 'Y-m-j' , $nuevafecha1 );
    
        $nuevafecha2 = strtotime ( '+1 day' , strtotime ( $request->fech_f ) ) ;
        $nuevafecha2 = date ( 'Y-m-j' , $nuevafecha2 );
        

    	$res_venta=DB::table('ventas')
        ->join("detalleventa","ventas.nVtaCod","=","detalleventa.nVtaCod")
        ->join("productoalmacens","detalleventa.nProAlmCod","=","productoalmacens.nProAlmCod")
        ->join("almacens","productoalmacens.nAlmCod","=","almacens.nAlmCod")
        ->join("productos","productoalmacens.cProdCod","=","productos.id")
        ->join("color","productoalmacens.id_color","=","color.id")

        ->where("ventas.dVFacFemi",">=", $nuevafecha1)
        ->where("ventas.dVFacFemi","<=", $nuevafecha2)
        ->where("color.id","like","%".$request->col."%")
        ->where("almacens.nAlmCod","like","%".$request->tien."%")
        ->where("productos.id","like","%".$request->prod."%")

        ->select(
        	"ventas.dVFacFemi",
        	"almacens.cAlmNom",
        	"productos.nombre_generico",
        	"productos.material",
        	"color.nombre",
        	"detalleventa.nVtaPeso",
        	"detalleventa.nVtaCant",
        	"detalleventa.nVtaPreST",
        	"productos.nombre_especifico"
        )->get();

        $fecha=Carbon::now()->format('Y-m-d h:i:s');

        if(trim($request->tien)=="")
        {
        	$v_tienda="Todas las Tiendas.";	
        }
        else
        {
		    $v_tienda_x=Almacen::where("nAlmCod",$request->tien)->first();
		    $v_tienda=$v_tienda_x->cAlmNom;
    	}

    	if(trim($request->prod)=="")
        {
        	$v_prod="Todos los Productos.";	
        }
        else
        {
		    $v_prod_x=Producto::where("id",$request->prod)->first();
  			$v_prod=$v_prod_x->nombre_generico.' - '.$v_prod_x->nombre_especifico;
    	}

    	if(trim($request->col)=="")
        {
        	$v_color="Todos los Colores.";	
        }
        else
        {
		    $v_color_x=Color::where("id",$request->col)->first();
  			$v_color=$v_color_x->nombre;
    	}        

        $view =  \View::make('comercializacion.reporteria.rpt_detvenprod', compact("res_venta","fecha","request","v_tienda","v_prod","v_color"))->render();
        $pdf = \App::make('dompdf.wrapper');
        //$pdf->loadHTML($view)->setPaper([5, 0, 630, 630], 'landscape');//->setPaper('a4', 'potrait');
        $pdf->loadHTML($view)->setPaper('a4', 'landscape');
        return $pdf->stream('invoice');   
              
    }  


    public function resventaporproducto_create()
    {
        $pro=Producto::all();     
        return view("comercializacion.reporteria.resventaporproducto",compact("pro"));
    }

    function resventaporproducto(Request $request)
    {   
        $nompro="";

        if(trim($request->pro_v)=="")
        {
            $nompro="Todos los Productos.";
            $result=Producto::all();
        }
        else
        {
            $c_x=Producto::where("id",$request->pro_v)->first();      
            $nompro=$c_x->nombre_generico;
            $result=Producto::where("id",$request->pro_v)->get();
        }

        $array;
        
        foreach ($result as $key => $value) {
            $array_p;       
            
            $array_p[0]=$value->nombre_generico;
            for ($i=1; $i <= 12; $i++) 
            { 
                $res=DB::table('ventas')
                ->join("detalleventa","ventas.nVtaCod","=","detalleventa.nVtaCod")
                ->join("productoalmacens","detalleventa.nProAlmCod","=","productoalmacens.nProAlmCod")           
                ->join("productos","productoalmacens.cProdCod","=","productos.id")
                ->join("color","productoalmacens.id_color","=","color.id")
                ->select("productos.nombre_generico",DB::raw("SUM(detalleventa.nVtaPreST) as totalmonto"),DB::raw("SUM(detalleventa.nVtaPeso) as totalpeso"))
                ->where((DB::raw("MONTH(ventas.dVFacFemi)")),"=",$i)
                ->where((DB::raw("YEAR(ventas.dVFacFemi)")),"=",$request->anio)
                ->where("productos.id",$value->id)
                ->groupBy("productos.nombre_generico")                
                ->first();
                
                if($res==""){
                    $array_p[$i]="0|0";
                }
                else{
                    $array_p[$i]=(trim($res->totalmonto)=="")?0:$res->totalmonto.'|'.((trim($res->totalpeso)=="")?0:$res->totalpeso);
                }                
            }

            $array[$key]=($array_p);
        }
        /*$cad="122.25|58.25";
        $ene=0;
        $ene_p=0;
        $ene+=substr($cad, 0, strpos($cad,"|")); 
        $ene_p=substr($cad,strpos($cad,"|")+1);
        return $ene_p;*/
        //return compact("array");
            
        $fecha=Carbon::now()->format('Y-m-d h:i:s');

        $view =  \View::make('comercializacion.reporteria.rpt_resventaporproducto', compact("array","request","fecha","nompro"))->render();
        $pdf = \App::make('dompdf.wrapper');        
        $pdf->loadHTML($view)->setPaper('a4', 'landscape');
        return $pdf->stream('invoice');   
        /*
SELECT productos.nombre_generico,sum(detalleventa.nVtaPreST),sum(detalleventa.nVtaPeso) FROM `ventas` 
inner join detalleventa on ventas.nVtaCod=detalleventa.nVtaCod
inner join productoalmacens on detalleventa.nProAlmCod=productoalmacens.nProAlmCod
inner join productos on productos.id=productoalmacens.cProdCod
INNER JOIN color on productoalmacens.id_color=color.id
WHERE MONTH(ventas.dVFacFemi)=11 and YEAR(ventas.dVFacFemi)=2017
GROUP BY productos.nombre_generico
        */
              
    }   

    public function cuentaspcobrar_create()
    {
        $clie=Cliente::all();    
        $tpag=tipodocv::all(); 
        return view("comercializacion.reporteria.cuentaspcobrar",compact("clie","tpag"));
    }

    function cuentaspcobrar(Request $request)
    {   
        $nomclie="";
        $nomtdoc="";
        $codcli="";
        $codt_doc="";

        if(trim($request->cli_v)=="")
        {
            $nomclie="Todos los Clientes.";            
            $codcli="%";
        }
        else
        {
            $c=Cliente::where("nClieCod",$request->cli_v)->first();
            $nomclie=$c->cClieDesc;                
            $codcli=$request->cli_v;
        }

        if(trim($request->t_doc)=="")
        {
            $nomtdoc="Todos los Documentos.";            
            $codt_doc="%";
        }
        else
        {
            $td=tipodocv::where("nTipPagCod",$request->t_doc)->first();
            $nomtdoc=$td->cDescTipPago;  
            $codt_doc=$request->t_doc;              
        }

        $datos=venta::where("nForPagCod","3")
        ->where("nTipPagCod","like",$codt_doc)
        ->where("nClieCod","like",$codcli)
        ->where("saldo",">",0)        
        ->get();

        $cont_cli=venta::where("nForPagCod","3")
        ->where("nTipPagCod","like",$codt_doc)
        ->where("nClieCod","like",$codcli)
        ->where("saldo",">",0)        
        ->select("nClieCod")
        ->distinct("nClieCod")
        ->get();

        //return compact("cont_cli");

        $fecha=Carbon::now()->format('Y-m-d h:i:s');

        $view =  \View::make('comercializacion.reporteria.rpt_cuentaspcobrar', compact("datos","cont_cli","request","fecha","nomclie","nomtdoc"))->render();
        $pdf = \App::make('dompdf.wrapper');        
        $pdf->loadHTML($view)->setPaper('a4', 'landscape');
        return $pdf->stream('invoice');   
             
    } 
}
