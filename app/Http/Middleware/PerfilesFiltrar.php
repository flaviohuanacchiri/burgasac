<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Session;
class PerfilesFiltrar
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //$url=substr($request,strpos($request,'url')+4,strpos($request,'HTTP')-strpos($request,'url')-5);
        //echo $request;
        //$url=trim(substr($request,strpos($request,' ')+2,strpos($request,'HTTP')-6));        
        $pru=explode(" ", $request);
        $url=substr($pru[1],1);
        //echo "ddddddddddddd".$url;
        $url=(trim(substr($url,0,strpos($url,'?')))=="")?$url:trim(substr($url,0,strpos($url,'?')));
        //echo $url."-----";
        if (!Session::get("restriccioncomercializacion")) {
            $modulos=DB::table("rols_areas")
            ->Join("areas_almacen","rols_areas.nAreAlmCod","=","areas_almacen.nAreAlmCod")            
            ->Join("areas","areas_almacen.nAreCod","=","areas.nAreCod") 
            ->Join("rols_areas_modulos","areas.nAreCod","=","rols_areas_modulos.nAreCod") 
            ->Join("modulos","rols_areas_modulos.nCodMod","=","modulos.nCodMod")
            ->where('rols_areas.id','=',access()->user()->id)
            //->where('modulos.url','like',$url)
            ->select("modulos.cNomMod","modulos.cDesMod","modulos.url")
            ->distinct()
            ->get();

            Session::set("restriccioncomercializacion", $modulos);
        } else {
            $modulos = Session::get("restriccioncomercializacion");
        }
        $key=1;

        foreach ($modulos as $key => $value) 
        {
            $po1 = explode("/", $value->url);
            $po2 = explode("/", $url);

            $z_p1=sizeof($po1);
            $z_p2=sizeof($po2);
            $key=1;
            if($z_p1==$z_p2)
            {
                for($i=0;$i<$z_p1;$i++)
                {                
                    if($po1[$i]!=$po2[$i] && $po1[$i]!="?")
                    {
                        $key=0;
                        break;
                    }
                }
            }
            else
                $key=0;

            if($key==1)
                break;
        }

        //falta verificar bien la url
        //if(sizeof($modulos)==0)                                
        if($key==0)                                
          return redirect()->route('admin.dashboard')->with('info','Adv: No tiene los permisos necesarios. -> '.$url);
        else
            return $next($request);
    }
}
