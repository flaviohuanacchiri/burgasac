<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Almacen extends Model
{    
    protected $table = 'almacens';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nAlmCod';

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cAlmNom',
        'cAlmUbi',
        'nAlmSerAct',
        'nAlmNumAct'
    ];

    public function productoAlmacen()
    {
        return $this->hasMany('App\ProductoAlmacen', 'nAlmCod');
    }

    public function areasAlmacen()
    {
        return $this->hasMany('App\Areas_almacen', 'nAlmCod');
    }
}
