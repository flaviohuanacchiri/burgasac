<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pago_compra extends Model
{
    protected $table = 'pago_compra';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nCodPagComp';
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'dPagoTotal',
        'dPagoSaldo',
        'nCodComTel'      
    ];

    /**
     * Get insumos associated with proveedor
     */

    public function CompraTelat()
    {
        return $this->belongsTo('App\CompraTelat', 'nCodComTel');
    }
    public function detalles(){
      return $this->hasMany('App\detalle_pago_compra','nCodPagComp');
  }
}
