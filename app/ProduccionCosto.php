<?php

namespace App;

class ProduccionCosto extends BaseModel
{
    //

    protected $table = "produccion_costo";
    protected $primaryKey = 'id';

    protected $fillable=[
        'id',
        'planeamiento_id',
        'costo_proveedor_producto_id',
        'lote',
        'kilos',
        'precio',
        'total',
        'user_created_at',
        'user_updated_at',
        'user_deleted_at',
        'userid_created_at',
        'userid_updated_at',
        'userid_deleted_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function produccion()
    {
        return $this->belongsTo('App\Planeamiento');
    }
    public function costoProveedorProducto()
    {
        return $this->belongsTo('App\CostoProveedorProducto');
    }
}
