<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetCompraTelat extends Model
{
    protected $table = 'detalle_compra_telat';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nDetCodComTel';
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nProAlmCod',
        'dPeso',
        'dPrecio',
        'dSubtotal',
        'nCodComTel'
    ];

    /**
     * Get insumos associated with proveedor
     */
  
    public function compratela()
    {
        return $this->belongsTo('App\CompraTelat', 'nCodComTel');//de muchos a uno
    }
    public function ProductoAlmacen()
    {
        return $this->belongsTo('App\ProductoAlmacen', 'nProAlmCod');//de muchos a uno
    }
}
