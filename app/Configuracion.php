<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuracion extends Model
{
    protected $table = 'configuracion';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'cConfCod';

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'dCamDolar',
        'dIgv'
    ];
}
