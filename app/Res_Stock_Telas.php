<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Res_Stock_Telas extends Model
{
    protected $table = 'resumen_stock_telas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';    

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $fillable = [  
    	'producto_id',
		'proveedor_id',
		'nro_lote',
        'cantidad',
        'rollos',          
        'estado',
        'created_at',
        'updated_at',
        'deleted_at',
        'userid_created_at',
        'userid_updated_at',
        'userid_deleted_at',
        'user_created_at',
        'user_updated_at',
        'user_deleted_at'
    ];

    public function producto(){
      return $this->belongsTo('App\Producto','producto_id');
    }

    public function proveedor(){
      return $this->belongsTo('App\Proveedor','proveedor_id');
    }

    public function ventadetalle()
    {
        return $this->hasOne('App\ventadetalle','id');
    }
}
