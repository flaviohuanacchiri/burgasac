<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rols_areas extends Model
{
    protected $table = 'rols_areas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nRolAreCod';

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nAreAlmCod',
        'id',
        'bRolAreAct'
    ];

    public function areasalmacen()
    {
        return $this->belongsTo('App\Areas_almacen', 'nAreAlmCod');
    }

    /*public function rolsuser()
    {
        return $this->belongsTo('App\Rols_user', 'id');
    }*/

    public function user()
    {
        return $this->belongsTo('App\Users', 'id');
    }

    public function areaAlmacen()
    {
        return $this->hasOne("App\Areas_almacen", "nAreAlmCod", "nAreAlmCod");
    }

    /*public function almacen()
    {
        return $this->belongsTo('App\Almacen', 'nAlmCod');
    }*/
}
