<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolAreaModulo extends Model
{
    protected $table = 'rols_areas_modulos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nRolAreMod';

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nAreCod',
        'nCodMod'
    ];

    public function modulos()
    {
        return $this->belongsTo('App\Modulos', 'nCodMod');//de muchos a uno
    }

    public function areas()
    {
        return $this->belongsTo('App\Areas', 'nAreCod');//de muchos a uno
    }
}
