<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompraTelat extends Model
{
    protected $table = 'compra_telat';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nCodComTel';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tFecha',
        'dMonTotal',
        'cMoneda',
        'nCodProv',
        'nCodTdoc',
        'nCodForP',
        'cDatDocFac',
        'cObs',
        'deleted_at'
    ];

    /**
     * Get insumos associated with proveedor
     */
    public function DetComTel()
    {
        return $this->hasMany('App\DetCompraTelat','nCodComTel');
    }

    public function proveedor()
    {
        return $this->belongsTo('App\Proveedor', 'nCodProv');//de muchos a uno
    }
    public function tipodoc()
    {
        return $this->belongsTo('App\tipodocv', 'nCodTdoc');//de muchos a uno
    }
    public function formapago()
    {
        return $this->belongsTo('App\forpago', 'nCodForP');//de muchos a uno
    }

    public function PagoCompra()
    {
        return $this->hasMany('App\pago_compra', 'nCodComTel');
    }
}
