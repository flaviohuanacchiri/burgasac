<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle_Ingreso extends Model
{
    protected $table = 'detalle_ingreso';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nDetIngCod';
    //public $incrementing = false;

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $fillable = [  
    	'nIngCod',
    	'cCodBarra',
    	'cDetalle',
    	'dPesoUni',
        'dCanUni'
    ];

    public function ingreso()
    {
        return $this->belongsTo('App\Ingreso', 'nIngCod');//de muchos a uno
    }

}
