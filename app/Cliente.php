<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
	protected $table = 'clientes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nClieCod';
    public $timestamps = false;
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

	protected $fillable = [
        'nTdocCod', 'cClieNdoc', 'cClieDesc', 'cClieDirec', 'cClieObs','anulado','dFecAct','dFecElim'
    ];

    public function tipodoc()
    {
        return $this->belongsTo('App\Tipodoc', 'nTdocCod');
    }
}