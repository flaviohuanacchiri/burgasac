<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DespachoTintoreriaPlaneamiento extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'despacho_tintoreria_planeamiento';

  /**
  * The database primary key value.
  *
  * @var string
  */
  protected $primaryKey = 'id';

  /**
   * Attributes that should be mass-assignable.
   *
   * @var array
   */
  protected $fillable = [
    'despacho_tintoreria_id',
    'detalles_despacho_tintoreria_id',
    'planeamiento_id',
    'peso'
  ];
}
