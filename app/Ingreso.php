<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingreso extends Model
{
    protected $table = 'ingreso';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nIngCod';
    //public $incrementing = false;

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $fillable = [  
    	'nProAlmCod',
    	'cInvCod',
    	'nStockLogTotal',
    	'nStockFisTotal',
        'nVentaHoy',
        'nCodTC',
        'nCodMP'
    ];

    public function inventario()
    {
        return $this->belongsTo('App\Inventario', 'cInvCod');//de muchos a uno
    }

    public function detalleingreso()
    {
        return $this->hasMany('App\Detalle_Ingreso', 'nIngCod');
    }

    public function proalm()
    {
        return $this->belongsTo('App\ProductoAlmacen', 'nProAlmCod');//de muchos a uno
    }
    public function resStocktelas()
    {
        return $this->belongsTo('App\Res_Stock_Telas', 'nCodTC');//de muchos a uno
    }
    public function resStockMP()
    {
        return $this->belongsTo('App\Res_stock_mp', 'nCodMP');//de muchos a uno
    }
}
