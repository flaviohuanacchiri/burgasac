<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class venta extends Model
{
    protected $table = 'ventas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nVtaCod';
    public $incrementing = false;

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $fillable = [  
    	'nClieCod',
		'nTipPagCod',
        'nAlmCod',
		'nForPagCod',
        'nProAlmCod',
        'dDniContacto',
        'cContacto',          
        'cBanco',
        'cNCheque',
        'cNoperacion',
        'nTotal_sol',
        'nTotal_dol',
        'nTipCam',          
        'dVFacFemi',
        'bSV',
        'dVFacSTot',
        'dVFacIgv',
        'dVFacVTot',
        'cFacNumFac',

        'nCodUsu',
        'nNomUsu',
        'nCarUsu',
        /*'dTotMonFavor',
        'dTotMonContra',*/
        'bDevuelto',
        'pago',
        'saldo',
        'cNumGuia',
        'dFecAct',
        'dFecElim'
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Cliente', 'nClieCod');//de muchos a uno
    }

    public function devolucion()
    {
        return $this->hasOne('App\Devoluciones', 'nVtaCod');
    }

    public function pagoscreditos()
    {
        return $this->hasMany('App\Pagos_creditos', 'nVtaCod');
    }

    public function almacen()
    {
        return $this->belongsTo('App\Almacen', 'nAlmCod');//de muchos a uno
    }

    public function forpago()
    {
        return $this->belongsTo('App\forpago', 'nForPagCod');//de muchos a uno
    }

    public function tipodocv()
    {
        return $this->belongsTo('App\tipodocv', 'nTipPagCod');//de muchos a uno
    }

    public function productoalmacen()
    {
        return $this->belongsTo('App\ProductoAlmacen', 'nProAlmCod');//de muchos a uno
    }

    public function ventadetalle()
    {
        return $this->hasMany('App\ventadetalle', 'nVtaCod');//de muchos a uno
    }
    public function scopeName($query,$fecha,$tdoc,$fpago,$rdni,$tie)
    {
            $query
            ->Where('dVFacFemi',"like","%".$fecha."%")
            ->Where('nTipPagCod',"like","%".$tdoc."%")
            ->Where('nForPagCod',"like","%".$fpago."%")
            ->Where('nClieCod',"like","%".$rdni."%")
            ->Where('nAlmCod',"like","%".$tie."%");
    }

    public function scopeCredito($query,$nventa)
    {
            $query
            ->where('nVtaCod',"like","%".$nventa."%")
            ->orWhere('cFacNumFac',"like","%".$nventa."%");
    }

    public function scopeCliente($query,$rdni)
    {
            $query->Where('nClieCod',"=",$rdni);
    }
}
