<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transferencias extends Model
{
    protected $table = 'transferencia';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'cTraCod';
     public $incrementing = false;
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'nAlmCod1',
    	'nAlmCod2',
    	'tTraFechReg',
    	'tTraFecUlt',
    	'cTraObs',
        'inventario'
    ];

    public function transferenciadetalle()
    {
        return $this->hasMany('App\Transferencias_detalle', 'cTraCod');
    }

    public function almacen1()
    {
        return $this->belongsTo('App\Almacen', 'nAlmCod1');
    }

    public function almacen2()
    {
        return $this->belongsTo('App\Almacen', 'nAlmCod2');
    }

    public function scopeName($query,$fec,$alm1,$alm2)
    {        
        $query->Where(function($querys,$fec,$alm1,$alm2)
        {
            $querys->orWhere('tTraFechReg', 'like', "%".$fec."%")
                  ->orWhere('nAlmCod1', 'like', "%".$alm1."%")
                  ->orWhere('nAlmCod2', 'like', "%".$alm2."%");
        });
    }

}
