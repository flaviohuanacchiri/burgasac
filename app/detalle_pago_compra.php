<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detalle_pago_compra extends Model
{
    protected $table = 'detalle_pago_compra';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nDCodPC';
    
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tFecha',
        'dMontoPag',
        'bCaja',
        'cCaja',
        'name',
        'nCodPagComp'
    ];

    /**
     * Get insumos associated with proveedor
     */
 
    public function PagoCompra()
    {
        return $this->belongsTo('App\pago_compra', 'nCodPagComp');//de muchos a uno
    }
}
