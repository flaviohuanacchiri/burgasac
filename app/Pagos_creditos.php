<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pagos_creditos extends Model
{
    protected $table = 'pagos_creditos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'cPagCre';
    //public $incrementing = false;
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nVtaCod',
        'dPagCreMonto',
        'tPagCreFecReg',
        'nCodUsu',
        'nNomUsu'
    ];
  
    public function ventas()
    {
        return $this->belongsTo('App\venta', 'nVtaCod');
    }
}
