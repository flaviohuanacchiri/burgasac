<?php

namespace App;

use App\Planeamiento;
use App\DespachoTerceroPlaneamiento;

class DespachoTerceros extends BaseModel
{
    protected $table = "despacho_terceros";

    protected $fillable=[
        'id',
        'proveedor_id',
        'planeamiento_id',
        'fecha',
        'nroguia',
        'direccionpartida',
        'direccionllegada',
        'user_created_at',
        'user_updated_at',
        'user_deleted_at',
        'userid_created_at',
        'userid_updated_at',
        'userid_deleted_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function detalles()
    {
        return $this->hasMany('App\DetallesDespachoTerceros', 'despacho_id', 'id');
    }
    public function transportista()
    {
        return $this->hasOne('App\TransportistaDespachoTercero', 'despacho_id', 'id');
    }
    public function proveedor()
    {
        return $this->belongsTo('App\Proveedor');
    }
    public function planeamiento()
    {
        return $this->belongsTo('App\Planeamiento');
    }
    public function consumirPlaneamientos($nroLote, $totaldespachado = 0, $detalleDespachoId = 0)
    {
        $planeamientos = Planeamiento::select("planeamientos.*")->where("estado", "=", 1)->where("completadodespacho", "=", 0)
            ->leftJoin("detalle_planeamientos AS dp", "dp.planeamiento_id", "=", "planeamientos.id")
            ->where("dp.lote_insumo", "=", $nroLote)
            ->groupBy("dp.planeamiento_id")
            ->orderBy("planeamientos.fecha", "ASC")
            ->get();

        if (count($planeamientos) > 0 && $totaldespachado > 0) {
            foreach ($planeamientos as $key => $value) {
                $kgTotal = $value->kg_producidos - $value->kg_falla;
                if ($kgTotal > 0 && $value->completadodespacho == 0) {
                    if ($kgTotal > $value->kg_despachado) {
                        $diferencia = $kgTotal - $value->kg_despachado;
                        if ($totaldespachado >= $diferencia) {
                            $totalconsumido = $diferencia;
                            $totaldespachado-=$diferencia;
                            $value->kg_despachado = $kgTotal;
                            $value->completadodespacho = 1;
                        } else {
                            $totalconsumido = $totaldespachado;
                            $totaldespachado  = 0;
                            $value->kg_despachado+=$totalconsumido;
                            $value->completadodespacho = 0;
                        }
                        $update = [
                            "kg_despachado" => $value->kg_despachado,
                            "completadodespacho" => $value->completadodespacho
                        ];
                        Planeamiento::find($value->id)->update($update);
                        DespachoTerceroPlaneamiento::insert([
                            "despacho_tercero_id" => $this->id,
                            "detalles_despacho_tercero_id" => $detalleDespachoId,
                            "planeamiento_id" => $value->id,
                            "peso" => $totalconsumido
                        ]);
                    }
                }
            }
        }
    }
}
