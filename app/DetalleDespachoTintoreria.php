<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleDespachoTintoreria extends Model
{
    protected $table = "detalles_despacho_tintoreria";

    protected $fillable = ["color_id","producto_id","cantidad","proveedor_id","descripcion",'despacho_id','rollos', 'nro_lote', 'estado'];

    public function producto()
    {
        return $this->belongsTo('App\Producto');
    }

    public function color()
    {
        return $this->belongsTo('App\Color');
    }
    //
    public function proveedor(){
        return $this->belongsTo('App\Proveedor');
    }
    public function despachotintoreria(){
        return $this->belongsTo('App\DespachoTintoreria', 'id', 'id');
    }

   }
