<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\UpdaterTrait;

class DetalleCompra extends Model
{
    use SoftDeletes;
    use UpdaterTrait;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'detalle_compras';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nro_lote',
        'titulo_id',
        'peso_bruto',
        'peso_tara',
        'cantidad',
        'observaciones',
        'insumo_id',
        'accesorio_id',
        'compra_id',
        'producto_id',
        'total',
        'precio_unitario',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function insumo()
    {
        return $this->belongsTo('App\Insumo');
    }

    public function accesorio()
    {
        return $this->belongsTo('App\Accesorio');
    }

    public function tela()
    {
        return $this->belongsTo('App\Producto', 'producto_id', 'id');
    }
    public function titulo()
    {
        return $this->belongsTo('App\Titulo');
    }

    public function resumen()
    {
        return $this->hasMany("App\Resumen_Stock_MP");
    }

    public function detalleplaneamiento()
    {
        return $this->belongsTo('App\DetallePlaneamiento', 'nro_lote', 'lote_insumo')->where("lote_insumo", "<>", "0")->join(
            'planeamientos as p',
            'p.id',
            '=',
            'detalle_planeamientos.planeamiento_id'
        )->whereRaw("p.deleted_at IS NULL");
    }
}
