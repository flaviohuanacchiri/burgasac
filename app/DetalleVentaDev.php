<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleVentaDev extends Model
{
    protected $table = 'detalleventadev';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nDetVtaDevCod';

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nProAlmCod',
        'cDevCod',
        'nDvtaCB',
        'nVtaDevPeso',
        'nVtaDevCant',
        'nVtaDevPreST',
        'nVtaDevPrecioU',
        'nCodTC',
        'nCodMP'
    ];

    public function devolucion()
    {
        return $this->belongsTo('App\Devoluciones', 'cDevCod');
    }

    public function productoalmacen()
    {
        return $this->belongsTo('App\ProductoAlmacen', 'nProAlmCod');//de muchos a uno   
    }
    public function resStocktelas()
    {
        return $this->belongsTo('App\Res_Stock_Telas', 'nCodTC');//de muchos a uno
    }

    public function resStockMP()
    {
        return $this->belongsTo('App\Res_stock_mp', 'nCodMP');//de muchos a uno
    }
}
