<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{
    protected $table = 'inventario';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'cInvCod';
    public $incrementing = false;

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $fillable = [  
    	'dInvFecHor', 
        'nAlmCod'   	
    ];

    public function ingreso()
    {
        return $this->hasMany('App\Ingreso', 'cInvCod');
    }

    public function almacen()
    {
        return $this->belongsTo('App\Almacen', 'nAlmCod');//de muchos a uno
    }
}
