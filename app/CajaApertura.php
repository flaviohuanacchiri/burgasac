<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CajaApertura extends Model
{
    protected $table = 'caja_apertura';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'cCajApeCod';
    public $incrementing = false;
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fCajFecApe',
        'dCajMonApe',
        'nRolAreCod',
        'bCajCieAbierto',
        'nameUserLog',
        'nameRolLog',        
        'dCajApeMonTotVen',
        'dCajApeTotTraSal',
        'dCajApeTotTraIng',
        'dCajApeTotEgreso'
    ];

    public function rolarea()
    {
        return $this->belongsTo('App\Rols_areas', 'nRolAreCod');
    }

    public function cajacierre()
    {
        return $this->hasMany('App\CajaCierre', 'cCajApeCod');
    }

}
