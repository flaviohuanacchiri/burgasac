<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ventadetalle extends Model
{
    protected $table = 'detalleventa';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nDetVtaCod';
    //public $incrementing = false;

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $fillable = [                
    	'nProAlmCod',
    	'nVtaCod',
        'nDvtaCB',
		'nVtaPeso',		
		'nVtaCant',
		'nVtaPrecioU',
        'bDevuelto',
        'nCodTC',
        'nCodMP'
    ];

    public function venta()
    {
        return $this->belongsTo('App\venta', 'nVtaCod');//de muchos a uno
    }

    public function productoAlmacen()
    {
        return $this->belongsTo('App\ProductoAlmacen', 'nProAlmCod');//de muchos a uno
    }
  
    public function prodevolver()
    {
        return $this->hasOne('App\Prodevolver','nDetVtaCod');
    }

    public function resStocktelas()
    {
        return $this->belongsTo('App\Res_Stock_Telas', 'nCodTC');//de muchos a uno
    }

    public function resStockMP()
    {
        return $this->belongsTo('App\Res_stock_mp', 'nCodMP');//de muchos a uno
    }
/*
    public function productoalmacen()
    {
        return $this->belongsTo('App\productoAlmacen', 'nProAlmCod');//de muchos a uno
    }*/
}
