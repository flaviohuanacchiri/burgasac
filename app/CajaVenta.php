<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CajaVenta extends Model
{
    protected $table = 'caja_venta';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nCajVenCod';
    public $incrementing = false;
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cCajApeCod',
        'nVtaCod'
    ];

    public function cajaapertura()
    {
        return $this->belongsTo('App\CajaApertura', 'cCajApeCod');
    }

    public function ventas()
    {
        return $this->belongsTo('App\venta', 'nVtaCod');
    }

   
}
