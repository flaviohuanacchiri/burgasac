<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tmpimpresion1 extends Model
{
    protected $table = 'temp_impresion';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'cod';

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'campo1',
        'campo2',
        'campo3',
        'campo4',
        'nt',
        'campo5'
    ];

   }
