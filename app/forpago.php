<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class forpago extends Model
{
    protected $table = 'formapago';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'nForPagCod';
    //public $incrementing = false;

    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $fillable = [  
    	'cDescForPag'
    ];

    public function venta()
    {
        return $this->hasMany('App\venta', 'nForPagCod');//de muchos a uno
    }
}
