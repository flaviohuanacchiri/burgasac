<?php
namespace App\ObjectViews;

use App\TipoProveedor;
use App\Producto;
use App\Proveedor;
use App\Color;
use App\DespachoTintoreria;
use App\DespachoTerceros;
use App\ProveedorDespachoTintoreriaDeuda;
use Illuminate\Support\Facades\DB;
use App\Planeamiento;

class ReporteTela
{
    public function telaTintoreria($filtro = [])
    {
        $resultado = DespachoTintoreria::select(
            "despacho_tintoreria.*",
            "despacho_tintoreria.id as iddespacho",
            "ddt.id as detalle",
            "pv.nombre_comercial as proveedor",
            "c.nombre as color",
            "pr.nombre_generico as producto",
            "ddt.cantidad",
            "ddt.rollos",
            "ddt.nro_lote as lote",
            DB::raw("IFNULL(dni.fecha, '') as fechaingreso"),
            DB::raw("IFNULL(dni.peso_cant, 0.00) as pesotienda"),
            DB::raw("IFNULL(dni.rollo, 0.00) as cantidadtienda")
        )->join("detalles_despacho_tintoreria as ddt", "ddt.despacho_id", "=", "despacho_tintoreria.id")
          ->join("proveedores as pv", "pv.id", "=", "despacho_tintoreria.proveedor_id")
          ->join("productos as pr", "pr.id", "=", "ddt.producto_id")
          ->join("color as c", "c.id", "=", "ddt.color_id")
          ->leftJoin("nota_ingreso as ni", "ni.desptint_id", "=", "ddt.id")
          ->leftJoin("detalle_nota_ingreso as dni", "dni.ning_id", "=", "ni.ning_id");
        if (isset($filtro["fecha"])) {
            $resultado->whereBetween("despacho_tintoreria.fecha", $filtro["fecha"]);
        }
        if (isset($filtro["proveedor"])) {
            $resultado->where("despacho_tintoreria.proveedor_id", "=", $filtro["proveedor"]);
        }
        if (isset($filtro["color"])) {
            $resultado->where("ddt.color_id", "=", $filtro["color"]);
        }
        if (isset($filtro["producto"])) {
            $resultado->where("ddt.producto_id", "=", $filtro["producto"]);
        }
        $resultado->whereRaw("despacho_tintoreria.deleted_at IS NULL");

        return $resultado;
    }

    public function telaDeudaTintoreria($filtro = [])
    {
        $resultado = ProveedorDespachoTintoreriaDeuda::select(
            "proveedor_despacho_tintoreria_deuda.*",
            "pv.nombre_comercial as proveedor",
            "c.nombre as color",
            "pr.nombre_generico as producto",
            "dt.nroguia"
        )->leftJoin("proveedores as pv", "pv.id", "=", "proveedor_despacho_tintoreria_deuda.proveedor_id")
        ->leftJoin("productos as pr", "pr.id", "=", "proveedor_despacho_tintoreria_deuda.producto_id")
        ->leftJoin("color as c", "c.id", "=", "proveedor_despacho_tintoreria_deuda.color_id")
        ->leftJoin("despacho_tintoreria as dt", "dt.id", "=", "proveedor_despacho_tintoreria_deuda.despacho_id");
        if (isset($filtro["fecha"])) {
            $resultado->whereBetween("proveedor_despacho_tintoreria_deuda.created_at", $filtro["fecha"]);
        }
        if (isset($filtro["proveedor"])) {
            $resultado->where("proveedor_despacho_tintoreria_deuda.proveedor_id", "=", $filtro["proveedor"]);
        }
        if (isset($filtro["color"])) {
            $resultado->where("proveedor_despacho_tintoreria_deuda.color_id", "=", $filtro["color"]);
        }
        if (isset($filtro["producto"])) {
            $resultado->where("proveedor_despacho_tintoreria_deuda.producto_id", "=", $filtro["color"]);
        }
        $resultado->whereRaw("proveedor_despacho_tintoreria_deuda.deleted_at IS NULL");

        return $resultado;
    }

    public function telaDespacho($filtro = [])
    {
        $tipoProveedorBurga = \Config::get("sistema.tipo_proveedor_burgasac_id");
        $proveedorBurga = \DB::table("proveedor_tipo as pt")
            ->join("proveedores as p", "p.id", "=", "pt.proveedor_id")
            ->where("pt.tipo_proveedor_id", "=", $tipoProveedorBurga)
            ->first();
        $resultado = Planeamiento::select("planeamientos.*")
            ->with(
                [
                    "despachoTercero" => function ($query) {
                        $query->where("peso", ">", 0);
                    },
                    "despachoTercero.despachoTercero",
                    "despachoTercero.despachoTerceroDetalle",
                    "producto" => function ($query) {
                        $query->select("id", "nombre_generico", "nombre_especifico");
                    },
                    "proveedor" => function ($query) {
                        $query->select("id", "nombre_comercial", "razon_social", "ruc");
                    },
                ]
            );

        if (isset($filtro["fecha"])) {
            $resultado->whereBetween("planeamientos.fecha", $filtro["fecha"]);
        }
        if (isset($filtro["proveedor"])) {
            $proveedorId = $filtro["proveedor"];
            $resultado->where("planeamientos.proveedor_id", "=", $proveedorId);
        }
        if (isset($filtro["producto"])) {
            $productoId = $filtro["producto"];
            $resultado->where("planeamientos.producto_id", "=", $productoId);
        }
        $resultado->where("planeamientos.proveedor_id", "<>", $proveedorBurga->proveedor_id);
        return $resultado;
    }
}
