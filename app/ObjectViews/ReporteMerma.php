<?php
namespace App\ObjectViews;

use App\TipoProveedor;
use App\Producto;
use App\Proveedor;
use App\Color;
use App\DespachoTintoreria;
use App\DespachoTerceros;
use App\ProveedorDespachoTintoreriaDeuda;
use Illuminate\Support\Facades\DB;

class ReporteMerma
{
    public function tintoreria($filtro = [])
    {
        $where = "";
        if (isset($filtro["fecha"])) {
        	$where.= " AND p.fecha BETWEEN '{$filtro["fecha"][0]}' AND '{$filtro["fecha"][1]}' ";
        }
        if (isset($filtro["producto"]) && $filtro["producto"]!="") {
        	$where.= " AND p.producto_id = {$filtro['producto']} ";
        }
        $query = "SELECT b.*,
			SUM(IFNULL(dni.peso_cant, 0)) AS pesoingresotienda,
			SUM(IFNULL(dni.rollo, 0)) AS cantidadingresotienda
			FROM (
			SELECT a.*,
				dt.id AS despacho_id,
				ddt.id AS detalle_despacho_id,
				SUM(IFNULL(ddt.cantidad, 0)) AS pesotintoreria,
				SUM(IFNULL(ddt.rollos, 0)) AS rollostintoreria
				FROM(
				SELECT 
					p.id AS planeamiento_id,
					p.fecha,
					(p.kg_producidos - p.kg_falla) AS pesoneto,
					(p.rollos - p.rollos_falla) AS cantidadneta,
					pr.nombre_generico AS producto,
					IFNULL(
						(SELECT GROUP_CONCAT(lote_insumo) 
						AS lotes FROM detalle_planeamientos AS dp 
						WHERE dp.planeamiento_id = p.id  AND dp.lote_insumo <> 0 
					GROUP BY dp.planeamiento_id), '') AS lotes
					FROM planeamientos AS p
					LEFT JOIN productos AS pr ON pr.id = p.producto_id
					WHERE estado = 1 {$where}) AS a
				LEFT JOIN despacho_tintoreria_planeamiento AS dtp ON dtp.planeamiento_id = a.planeamiento_id
				LEFT JOIN despacho_tintoreria AS dt ON (dt.id = dtp.despacho_tintoreria_id AND dtp.peso > 0)
				LEFT JOIN detalles_despacho_tintoreria AS ddt ON (ddt.despacho_id = dtp.despacho_tintoreria_id AND ddt.despacho_id = dt.id)
				WHERE a.lotes <> ''
			GROUP BY dtp.planeamiento_id, dtp.despacho_tintoreria_id) AS b
		LEFT JOIN nota_ingreso AS ni ON ni.desptint_id = b.despacho_id
		LEFT JOIN detalle_nota_ingreso AS dni ON dni.ning_id = ni.ning_id
		GROUP BY b.planeamiento_id, b.despacho_id" ;

		$resultado = DB::select($query);

		return $resultado;
	}
}