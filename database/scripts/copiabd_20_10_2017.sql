-- MySQL dump 10.13  Distrib 5.6.35, for Linux (x86_64)
--
-- Host: localhost    Database: burgasac
-- ------------------------------------------------------
-- Server version	5.6.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abonos`
--

DROP TABLE IF EXISTS `abonos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abonos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `observaciones` text COLLATE utf8_unicode_ci,
  `tipoabono_id` int(10) unsigned NOT NULL,
  `compra_id` int(10) unsigned NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `abonos_tipoabono_id_foreign` (`tipoabono_id`),
  KEY `abonos_compra_id_foreign` (`compra_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abonos`
--

LOCK TABLES `abonos` WRITE;
/*!40000 ALTER TABLE `abonos` DISABLE KEYS */;
/*!40000 ALTER TABLE `abonos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accesorios`
--

DROP TABLE IF EXISTS `accesorios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accesorios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_id` int(10) unsigned NOT NULL,
  `proveedor_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accesorios_titulo_id_foreign` (`titulo_id`),
  KEY `accesorios_proveedor_id_foreign` (`proveedor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accesorios`
--

LOCK TABLES `accesorios` WRITE;
/*!40000 ALTER TABLE `accesorios` DISABLE KEYS */;
INSERT INTO `accesorios` VALUES (2,'AGUJA',16,3,'2017-04-24 18:14:17','2017-10-19 19:47:12'),(3,'AGUJA',17,3,'2017-10-18 11:32:09','2017-10-19 19:47:23'),(4,'AGUJA',18,3,'2017-10-18 11:32:24','2017-10-19 19:47:48');
/*!40000 ALTER TABLE `accesorios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almacens`
--

DROP TABLE IF EXISTS `almacens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almacens` (
  `nAlmCod` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cAlmNom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cAlmUbi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nAlmSerAct` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `nAlmNumAct` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`nAlmCod`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almacens`
--

LOCK TABLES `almacens` WRITE;
/*!40000 ALTER TABLE `almacens` DISABLE KEYS */;
INSERT INTO `almacens` VALUES (8,'Tienda la Grande.','Calle. Real 150','001','000001'),(9,'Fibras y Telas','jr. Lima','002','000000');
/*!40000 ALTER TABLE `almacens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bancos`
--

DROP TABLE IF EXISTS `bancos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bancos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bancos`
--

LOCK TABLES `bancos` WRITE;
/*!40000 ALTER TABLE `bancos` DISABLE KEYS */;
INSERT INTO `bancos` VALUES (1,'BCP','2017-10-17 19:58:22','2017-10-17 19:58:22'),(2,'BANCO CONTINENTAL','2017-10-17 19:58:34','2017-10-17 19:58:34'),(3,'BANCO INTERBANK','2017-10-17 19:58:43','2017-10-17 19:58:43');
/*!40000 ALTER TABLE `bancos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargo`
--

DROP TABLE IF EXISTS `cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci,
  `estado` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_created_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_updated_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_deleted_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_created_at` smallint(6) DEFAULT NULL,
  `userid_updated_at` smallint(6) DEFAULT NULL,
  `userid_deleted_at` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo`
--

LOCK TABLES `cargo` WRITE;
/*!40000 ALTER TABLE `cargo` DISABLE KEYS */;
INSERT INTO `cargo` VALUES (1,'Asistente','Asistente descripcion editado',1,'2017-04-29 23:11:46','2017-04-29 23:18:33',NULL,'Rodolfo','Rodolfo',NULL,4,4,NULL);
/*!40000 ALTER TABLE `cargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `nClieCod` int(11) NOT NULL AUTO_INCREMENT,
  `nTdocCod` int(11) NOT NULL,
  `cClieNdoc` varchar(11) NOT NULL,
  `cClieDesc` varchar(250) NOT NULL,
  `cClieDirec` varchar(250) NOT NULL,
  `cClieObs` varchar(250) NOT NULL,
  PRIMARY KEY (`nClieCod`),
  KEY `nTdocCod` (`nTdocCod`),
  CONSTRAINT `clientes_ibfk_1` FOREIGN KEY (`nTdocCod`) REFERENCES `tipodocs` (`nTdocCod`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (4,1,'12345678','No empresa','jr. lima 1250','mm'),(5,2,'46153858','Cliente desde punto de venta','ninguna','Ninguna'),(6,2,'41316021','Rodolfo D\'Brot Tovar','JR FRANCISCO MONCLOA 3220, JR FRANCISCO MONCLOA 3220','Ninguna'),(7,2,'40301393','LUCERO GRANDA','EUCALIPTO 453','Ninguna'),(8,2,'41232312','JUAN MENDOZA','CL LIMA','Ninguna'),(9,2,'40301231','JUAN LOPEZ','CALLAO','Ninguna'),(10,2,'43211232','LUIS MENDOZA','CERCADO DE LIMA','Ninguna'),(11,1,'20536667521','DBROT GRANDA E.I.R.L','S.J.L','Ninguna'),(12,2,'45781254','JUAN CELESTE','CALLAO','Ninguna'),(13,2,'41005809','JAVIER GUEVARA','LIMA','Ninguna');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `color`
--

DROP TABLE IF EXISTS `color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `estado` tinyint(2) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_created_at` varchar(45) DEFAULT NULL,
  `user_updated_at` varchar(45) DEFAULT NULL,
  `user_deleted_at` varchar(45) DEFAULT NULL,
  `userid_created_at` int(10) DEFAULT NULL,
  `userid_updated_at` int(10) DEFAULT NULL,
  `userid_deleted_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `color`
--

LOCK TABLES `color` WRITE;
/*!40000 ALTER TABLE `color` DISABLE KEYS */;
INSERT INTO `color` VALUES (1,'NEGRO DIRECTO',1,'2017-10-19 18:36:37','2017-10-19 13:36:37',NULL,NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL),(2,'NEGRO REACTIVO',1,'2017-10-19 18:36:18','2017-10-19 13:36:18',NULL,NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL),(3,'VERDE PERICO DIRECTO',1,'2017-10-19 18:34:40','2017-10-19 13:34:40',NULL,NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL),(4,'VERDE PERICO DIRECTO',1,'2017-10-19 18:33:42','2017-10-19 13:33:42',NULL,'Rodolfo','Rodolfo D\'Brot',NULL,4,4,NULL),(5,'AZUL DIRECTO',1,'2017-10-19 18:33:14','2017-10-19 13:33:14',NULL,'Rodolfo','Rodolfo D\'Brot',NULL,4,4,NULL),(6,'AZUL REACTIVO',1,'2017-10-19 18:35:52','2017-10-19 13:35:52',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL),(7,'VINO DIRECTO',1,'2017-10-19 13:37:24','2017-10-19 13:37:24',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(8,'VINO REACTIVO',1,'2017-10-19 13:37:44','2017-10-19 13:37:44',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(9,'AZULINO DIRECTO',1,'2017-10-19 18:46:58','2017-10-19 13:46:58',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL),(10,'AZULINO REACTIVO',1,'2017-10-19 18:45:41','2017-10-19 13:45:41',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL),(11,'APACHE REACTIVO',0,'2017-10-19 13:40:14','2017-10-19 13:40:14',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(12,'APACHE DIRECTO',1,'2017-10-19 18:45:20','2017-10-19 13:45:20',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL),(13,'ORO REACTIVO',1,'2017-10-19 18:45:10','2017-10-19 13:45:10',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL),(14,'ORO DIRECTO',1,'2017-10-19 18:44:59','2017-10-19 13:44:59',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL),(15,'CELESTE BB DIRECTO',1,'2017-10-19 18:44:44','2017-10-19 13:44:44',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL),(16,'CELESTE BB REACTIVO',1,'2017-10-19 13:44:26','2017-10-19 13:44:26',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(17,'ROJO DIRECTO',1,'2017-10-19 13:47:55','2017-10-19 13:47:55',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(18,'ROJO REACTIVO',1,'2017-10-19 13:48:30','2017-10-19 13:48:30',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(19,'AMARILLO BRASIL REACTIVO',1,'2017-10-19 18:50:12','2017-10-19 13:50:12',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL),(20,'AMARILLO BRASIL DIRECTO',1,'2017-10-19 13:50:31','2017-10-19 13:50:31',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(21,'FUXIA REACTIVO',1,'2017-10-19 13:51:24','2017-10-19 13:51:24',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(22,'FUXIA DIRECTO',1,'2017-10-19 13:52:01','2017-10-19 13:52:01',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(23,'VERDE MILITAR DIRECTO',1,'2017-10-19 14:01:58','2017-10-19 14:01:58',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(24,'VERDE MILITAR RX',1,'2017-10-19 14:02:30','2017-10-19 14:02:30',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(25,'CORAL BB DIRECTO',1,'2017-10-19 19:03:30','2017-10-19 14:03:30',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL),(26,'CORAL BB RX',1,'2017-10-19 14:03:21','2017-10-19 14:03:21',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(27,'VERDE GRAS DIRECTO',1,'2017-10-19 14:03:59','2017-10-19 14:03:59',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(28,'VERDE GRAS RX',1,'2017-10-19 14:04:27','2017-10-19 14:04:27',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(29,'PLOMO PLATA DIRECTO',1,'2017-10-19 14:32:26','2017-10-19 14:32:26',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(30,'PLOMO PLATA RX',0,'2017-10-19 14:32:42','2017-10-19 14:32:42',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(31,'AZUL ACERO DIRECTO',0,'2017-10-19 14:34:24','2017-10-19 14:34:24',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(32,'AZUL ACERO RX',1,'2017-10-19 19:34:53','2017-10-19 14:34:53',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL),(33,'CELESTE CRISTAL DIRECTO',1,'2017-10-19 19:35:55','2017-10-19 14:35:55',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL),(34,'CELESTE CRISTAL RX',1,'2017-10-19 14:36:26','2017-10-19 14:36:26',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(35,'VERDE TELEFÓNICA DIRECTO',0,'2017-10-19 14:37:44','2017-10-19 14:37:44',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(36,'VERDE TELEFÓNICA  RX',1,'2017-10-19 14:38:04','2017-10-19 14:38:04',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(37,'LONDON DIRECTO ',1,'2017-10-19 14:39:05','2017-10-19 14:39:05',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(38,'LONDON RX',1,'2017-10-19 14:39:20','2017-10-19 14:39:20',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(39,'PIZARRA DIRECTO',1,'2017-10-19 14:40:10','2017-10-19 14:40:10',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(40,'PIZARRA RX',1,'2017-10-19 14:40:24','2017-10-19 14:40:24',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(41,'NARANJA DIRECTO',1,'2017-10-19 14:40:39','2017-10-19 14:40:39',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(42,'NARANJA RX',1,'2017-10-19 14:40:50','2017-10-19 14:40:50',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(43,'MORADO DIRECTO',1,'2017-10-19 14:42:21','2017-10-19 14:42:21',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(44,'MORADO RX',1,'2017-10-19 14:42:41','2017-10-19 14:42:41',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(45,'TURQUEZA DIRECTO',1,'2017-10-19 14:43:08','2017-10-19 14:43:08',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL);
/*!40000 ALTER TABLE `color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compra_estados`
--

DROP TABLE IF EXISTS `compra_estados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compra_estados` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compra_estados`
--

LOCK TABLES `compra_estados` WRITE;
/*!40000 ALTER TABLE `compra_estados` DISABLE KEYS */;
INSERT INTO `compra_estados` VALUES (1,'Transición',NULL,NULL),(2,'Recepcionado',NULL,NULL);
/*!40000 ALTER TABLE `compra_estados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compras`
--

DROP TABLE IF EXISTS `compras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compras` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `codigo` int(11) DEFAULT NULL,
  `nro_comprobante` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nro_guia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observaciones` text COLLATE utf8_unicode_ci,
  `proveedor_id` int(10) unsigned NOT NULL,
  `procedencia_id` int(10) unsigned NOT NULL,
  `estado` int(10) unsigned NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `compras_proveedor_id_foreign` (`proveedor_id`),
  KEY `compras_procedencia_id_foreign` (`procedencia_id`),
  KEY `compras_estado_foreign` (`estado`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compras`
--

LOCK TABLES `compras` WRITE;
/*!40000 ALTER TABLE `compras` DISABLE KEYS */;
INSERT INTO `compras` VALUES (1,'2017-10-19 21:30:35',1,'','GUIA12333',NULL,5,1,2,4,4,'2017-10-18 17:22:31','2017-10-19 16:30:35','2017-10-19 16:30:35'),(2,'2017-10-18 00:00:00',2,'','GUIA232442',NULL,5,1,2,4,4,'2017-10-18 17:23:58','2017-10-18 17:23:58',NULL),(3,'2017-08-14 00:00:00',3,'','3186',NULL,5,1,2,4,4,'2017-10-19 16:36:24','2017-10-19 16:36:24',NULL),(4,'2017-10-19 23:34:52',4,'','GUI2002',NULL,5,1,2,4,4,'2017-10-19 18:27:05','2017-10-19 18:34:52','2017-10-19 18:34:52'),(5,'2017-09-23 00:00:00',5,'','003-0029100',NULL,5,3,2,4,4,'2017-10-19 18:34:18','2017-10-19 18:34:18',NULL),(6,'2017-10-19 00:00:00',6,'','0001-000979',NULL,8,5,2,4,4,'2017-10-19 18:49:02','2017-10-19 18:49:02',NULL),(7,'2017-10-10 00:00:00',7,'','008-0002555',NULL,5,1,2,4,4,'2017-10-19 18:50:33','2017-10-19 18:50:33',NULL),(8,'2017-10-04 00:00:00',8,'','2017',NULL,3,2,2,4,4,'2017-10-19 19:50:58','2017-10-19 19:50:58',NULL),(9,'2017-10-04 00:00:00',9,'','2018',NULL,3,2,2,4,4,'2017-10-19 19:51:36','2017-10-19 19:51:36',NULL),(10,'2017-10-04 00:00:00',10,'','2019',NULL,3,2,2,4,4,'2017-10-19 19:52:05','2017-10-19 19:52:05',NULL);
/*!40000 ALTER TABLE `compras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cronogramas`
--

DROP TABLE IF EXISTS `cronogramas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cronogramas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cuotas` int(11) DEFAULT NULL,
  `tipo_de_pago` tinyint(1) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `monto` decimal(10,2) NOT NULL,
  `observaciones` text COLLATE utf8_unicode_ci,
  `banco_id` int(10) unsigned DEFAULT NULL,
  `tipopago_id` int(10) unsigned DEFAULT NULL,
  `compra_id` int(10) unsigned NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cronogramas_banco_id_foreign` (`banco_id`),
  KEY `cronogramas_tipopago_id_foreign` (`tipopago_id`),
  KEY `cronogramas_compra_id_foreign` (`compra_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cronogramas`
--

LOCK TABLES `cronogramas` WRITE;
/*!40000 ALTER TABLE `cronogramas` DISABLE KEYS */;
/*!40000 ALTER TABLE `cronogramas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `despacho_terceros`
--

DROP TABLE IF EXISTS `despacho_terceros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `despacho_terceros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `proveedor_id` int(10) DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_created_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_updated_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_deleted_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_created_at` int(10) DEFAULT NULL,
  `userid_updated_at` int(10) DEFAULT NULL,
  `userid_deleted_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `despacho_terceros`
--

LOCK TABLES `despacho_terceros` WRITE;
/*!40000 ALTER TABLE `despacho_terceros` DISABLE KEYS */;
/*!40000 ALTER TABLE `despacho_terceros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `despacho_tintoreria`
--

DROP TABLE IF EXISTS `despacho_tintoreria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `despacho_tintoreria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `proveedor_id` int(10) DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_created_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_updated_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_deleted_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_created_at` int(10) DEFAULT NULL,
  `userid_updated_at` int(10) DEFAULT NULL,
  `userid_deleted_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `despacho_tintoreria`
--

LOCK TABLES `despacho_tintoreria` WRITE;
/*!40000 ALTER TABLE `despacho_tintoreria` DISABLE KEYS */;
INSERT INTO `despacho_tintoreria` VALUES (1,5,'2017-10-20 01:00:11','2017-10-18 22:19:30','2017-10-19 20:00:11','2017-10-19 20:00:11','Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(2,5,'2017-10-19 16:55:00','2017-10-18 22:39:59','2017-10-19 11:55:00','2017-10-19 11:55:00','Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(3,5,'2017-10-19 16:54:50','2017-10-19 07:00:21','2017-10-19 11:54:50','2017-10-19 11:54:50','Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(4,7,'2017-10-20 01:00:06','2017-10-19 13:20:52','2017-10-19 20:00:06','2017-10-19 20:00:06','Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL);
/*!40000 ALTER TABLE `despacho_tintoreria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_abonos`
--

DROP TABLE IF EXISTS `detalle_abonos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_abonos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `monto` decimal(8,2) NOT NULL,
  `observaciones` text COLLATE utf8_unicode_ci,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `peso_bruto` decimal(8,2) NOT NULL,
  `peso_tara` decimal(8,2) NOT NULL,
  `cantidad_paquetes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `producto_id` int(10) unsigned NOT NULL,
  `abono_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detalle_abonos_producto_id_foreign` (`producto_id`),
  KEY `detalle_abonos_abono_id_foreign` (`abono_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_abonos`
--

LOCK TABLES `detalle_abonos` WRITE;
/*!40000 ALTER TABLE `detalle_abonos` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_abonos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_compras`
--

DROP TABLE IF EXISTS `detalle_compras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_compras` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nro_lote` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `peso_bruto` decimal(8,2) DEFAULT NULL,
  `peso_tara` decimal(8,2) DEFAULT NULL,
  `cantidad` decimal(8,2) DEFAULT NULL,
  `observaciones` text COLLATE utf8_unicode_ci,
  `titulo_id` int(10) unsigned NOT NULL,
  `insumo_id` int(10) unsigned DEFAULT NULL,
  `accesorio_id` int(10) unsigned DEFAULT NULL,
  `compra_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detalle_compras_titulo_id_foreign` (`titulo_id`),
  KEY `detalle_compras_insumo_id_foreign` (`insumo_id`),
  KEY `detalle_compras_accesorio_id_foreign` (`accesorio_id`),
  KEY `detalle_compras_compra_id_foreign` (`compra_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_compras`
--

LOCK TABLES `detalle_compras` WRITE;
/*!40000 ALTER TABLE `detalle_compras` DISABLE KEYS */;
INSERT INTO `detalle_compras` VALUES (2,'LOTE11313',1200.00,0.00,40.00,NULL,1,1,NULL,2,'2017-10-18 17:23:58','2017-10-18 17:23:58'),(3,'212',20412.00,0.00,450.00,NULL,10,2,NULL,3,'2017-10-19 16:36:24','2017-10-19 16:36:24'),(5,'F287-A',507.80,0.00,15.00,NULL,12,4,NULL,5,'2017-10-19 18:34:18','2017-10-19 18:34:18'),(6,'2017',528.00,0.00,20.00,NULL,13,12,NULL,6,'2017-10-19 18:49:02','2017-10-19 18:49:02'),(7,'3030',15331.68,0.00,338.00,NULL,2,6,NULL,7,'2017-10-19 18:50:33','2017-10-19 18:50:33'),(8,NULL,NULL,NULL,100.00,NULL,16,NULL,2,8,'2017-10-19 19:50:58','2017-10-19 19:50:58'),(9,NULL,NULL,NULL,100.00,NULL,17,NULL,2,9,'2017-10-19 19:51:36','2017-10-19 19:51:36'),(10,NULL,NULL,NULL,100.00,NULL,18,NULL,4,10,'2017-10-19 19:52:05','2017-10-19 19:52:05');
/*!40000 ALTER TABLE `detalle_compras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_devoluciones`
--

DROP TABLE IF EXISTS `detalle_devoluciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_devoluciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `observaciones` text COLLATE utf8_unicode_ci,
  `detalle_compra_id` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nro_lote` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `peso_bruto` decimal(8,2) NOT NULL,
  `peso_tara` decimal(8,2) NOT NULL,
  `cantidad_paquetes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `insumo_id` int(11) DEFAULT NULL,
  `accesorio_id` int(11) DEFAULT NULL,
  `devolucion_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detalle_devoluciones_devolucion_id_foreign` (`devolucion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_devoluciones`
--

LOCK TABLES `detalle_devoluciones` WRITE;
/*!40000 ALTER TABLE `detalle_devoluciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_devoluciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_nota_ingreso`
--

DROP TABLE IF EXISTS `detalle_nota_ingreso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_nota_ingreso` (
  `dNotIng_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ning_id` int(10) unsigned NOT NULL DEFAULT '0',
  `nProAlmCod` int(10) unsigned NOT NULL DEFAULT '0',
  `cod_barras` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `peso_cant` double(15,2) DEFAULT NULL,
  `rollo` int(11) DEFAULT '0',
  `impreso` tinyint(4) NOT NULL DEFAULT '1',
  `fecha` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`dNotIng_id`),
  KEY `FK_ning_id` (`ning_id`),
  KEY `FK_nProAlmCod` (`nProAlmCod`) USING BTREE,
  CONSTRAINT `FK_nProAlmCod` FOREIGN KEY (`nProAlmCod`) REFERENCES `productoalmacens` (`nProAlmCod`),
  CONSTRAINT `FK_ning_id` FOREIGN KEY (`ning_id`) REFERENCES `nota_ingreso` (`ning_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_nota_ingreso`
--

LOCK TABLES `detalle_nota_ingreso` WRITE;
/*!40000 ALTER TABLE `detalle_nota_ingreso` DISABLE KEYS */;
INSERT INTO `detalle_nota_ingreso` VALUES (1,161,43,'T-LOTE1213-23545-1',23.50,1,1,'2017-10-19 00:00:00'),(2,161,43,'T-LOTE1213-23545-2',24.50,1,1,'2017-10-19 00:00:00'),(3,161,43,'T-LOTE1213-23545-3',23.20,1,1,'2017-10-19 00:00:00'),(4,161,43,'T-LOTE1213-23545-4',24.20,1,1,'2017-10-19 00:00:00'),(5,161,43,'T-LOTE1213-23545-5',24.50,1,1,'2017-10-19 00:00:00'),(6,161,43,'T-LOTE1213-23545-6',24.20,1,1,'2017-10-19 00:00:00');
/*!40000 ALTER TABLE `detalle_nota_ingreso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_nota_ingreso_a`
--

DROP TABLE IF EXISTS `detalle_nota_ingreso_a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_nota_ingreso_a` (
  `dNotInga_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ninga_id` int(10) unsigned NOT NULL DEFAULT '0',
  `nProAlmCod` int(10) unsigned NOT NULL DEFAULT '0',
  `cod_barras` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `peso_cant` double(15,2) DEFAULT NULL,
  `rollo` int(11) DEFAULT '0',
  `impreso` tinyint(4) NOT NULL DEFAULT '1',
  `estado` tinyint(4) NOT NULL DEFAULT '1',
  `fecha` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`dNotInga_id`),
  KEY `FK_ninga_id` (`ninga_id`),
  KEY `FK_nProAlmCoda` (`nProAlmCod`) USING BTREE,
  CONSTRAINT `FK_nProAlmCoda` FOREIGN KEY (`nProAlmCod`) REFERENCES `productoalmacens` (`nProAlmCod`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ninga_id` FOREIGN KEY (`ninga_id`) REFERENCES `nota_ingreso_a` (`ninga_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_nota_ingreso_a`
--

LOCK TABLES `detalle_nota_ingreso_a` WRITE;
/*!40000 ALTER TABLE `detalle_nota_ingreso_a` DISABLE KEYS */;
INSERT INTO `detalle_nota_ingreso_a` VALUES (1,43,45,'1-2017-10-19',21.00,1,1,0,'2017-10-19 00:00:00');
/*!40000 ALTER TABLE `detalle_nota_ingreso_a` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_planeamientos`
--

DROP TABLE IF EXISTS `detalle_planeamientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_planeamientos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `planeamiento_id` int(10) unsigned NOT NULL,
  `titulo_id` int(10) unsigned NOT NULL,
  `lote_insumo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `insumo_id` int(11) NOT NULL DEFAULT '0',
  `cajas` int(11) NOT NULL,
  `kg` double(8,2) NOT NULL,
  `mp_producida` double(8,2) NOT NULL,
  `accesorio_id` int(11) NOT NULL DEFAULT '0',
  `lote_accesorio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cantidad` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `detalle_planeamientos_planeamiento_id_foreign` (`planeamiento_id`),
  KEY `detalle_planeamientos_titulo_id_foreign` (`titulo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_planeamientos`
--

LOCK TABLES `detalle_planeamientos` WRITE;
/*!40000 ALTER TABLE `detalle_planeamientos` DISABLE KEYS */;
INSERT INTO `detalle_planeamientos` VALUES (1,'2017-10-18',1,5,'0',NULL,'2017-10-18 17:24:33','2017-10-18 17:25:03',0,0,0.00,10.00,2,'undefined',5),(2,'2017-10-18',1,1,'LOTE11313',NULL,'2017-10-18 17:24:33','2017-10-18 17:25:03',1,0,10.00,10.00,0,'undefined',0),(3,'2017-10-18',2,5,'0',NULL,'2017-10-18 22:36:29','2017-10-18 22:36:43',0,0,0.00,450.00,2,'undefined',5),(4,'2017-10-18',2,1,'LOTE11313',NULL,'2017-10-18 22:36:29','2017-10-18 22:36:43',1,0,450.00,450.00,0,'undefined',0),(5,'2017-10-06',3,16,'0',NULL,'2017-10-19 19:54:33','2017-10-19 19:54:33',0,0,0.00,0.00,2,'undefined',0),(6,'2017-10-06',3,10,'212',NULL,'2017-10-19 19:54:33','2017-10-19 19:54:33',2,0,0.00,0.00,0,'undefined',0),(7,'2017-10-05',4,16,'0',NULL,'2017-10-19 19:56:20','2017-10-19 19:56:20',0,0,0.00,0.00,2,'undefined',0),(8,'2017-10-05',4,10,'212',NULL,'2017-10-19 19:56:20','2017-10-19 19:56:20',2,0,0.00,0.00,0,'undefined',0),(9,'2017-10-19',5,16,'0',NULL,'2017-10-19 20:03:43','2017-10-19 20:03:43',0,0,0.00,0.00,2,'undefined',0),(10,'2017-10-19',5,10,'212',NULL,'2017-10-19 20:03:43','2017-10-19 20:03:43',2,0,0.00,0.00,0,'undefined',0),(11,'2017-10-05',6,16,'0',NULL,'2017-10-19 20:08:59','2017-10-19 20:08:59',0,0,0.00,0.00,2,'undefined',1),(12,'2017-10-05',6,10,'212',NULL,'2017-10-19 20:08:59','2017-10-19 20:08:59',2,0,0.00,0.00,0,'undefined',0),(13,'2017-10-19',7,16,'0',NULL,'2017-10-19 20:14:59','2017-10-19 20:14:59',0,0,0.00,0.00,2,'undefined',1),(14,'2017-10-19',7,10,'212',NULL,'2017-10-19 20:14:59','2017-10-19 20:14:59',2,0,0.00,0.00,0,'undefined',0),(15,'2017-10-05',8,16,'0',NULL,'2017-10-19 20:16:41','2017-10-19 20:16:41',0,0,0.00,0.00,2,'undefined',1),(16,'2017-10-05',8,10,'212',NULL,'2017-10-19 20:16:41','2017-10-19 20:16:41',2,0,0.00,0.00,0,'undefined',0);
/*!40000 ALTER TABLE `detalle_planeamientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalles_despacho_terceros`
--

DROP TABLE IF EXISTS `detalles_despacho_terceros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalles_despacho_terceros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `producto_id` int(10) unsigned NOT NULL,
  `color_id` int(11) DEFAULT '0',
  `cantidad` double(8,2) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `proveedor_id` int(10) unsigned NOT NULL,
  `despacho_id` int(10) unsigned NOT NULL,
  `rollos` double(8,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalles_despacho_terceros`
--

LOCK TABLES `detalles_despacho_terceros` WRITE;
/*!40000 ALTER TABLE `detalles_despacho_terceros` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalles_despacho_terceros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalles_despacho_tintoreria`
--

DROP TABLE IF EXISTS `detalles_despacho_tintoreria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalles_despacho_tintoreria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `color_id` int(10) NOT NULL DEFAULT '0',
  `producto_id` int(10) unsigned NOT NULL,
  `nro_lote` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cantidad` double(8,2) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `proveedor_id` int(10) unsigned NOT NULL,
  `despacho_id` int(10) unsigned NOT NULL,
  `rollos` double(8,2) NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalles_despacho_tintoreria`
--

LOCK TABLES `detalles_despacho_tintoreria` WRITE;
/*!40000 ALTER TABLE `detalles_despacho_tintoreria` DISABLE KEYS */;
INSERT INTO `detalles_despacho_tintoreria` VALUES (9,1,2,'LOTE1213',110.00,'2017-10-17 13:41:35','2017-10-17 13:41:35',5,1,10.00,0),(10,1,2,'LOTE11313',8.00,'2017-10-18 22:19:30','2017-10-18 22:19:30',5,1,4.00,1),(11,4,2,'LOTE11313',40.00,'2017-10-18 22:39:59','2017-10-18 22:39:59',5,2,2.00,1),(12,1,2,'LOTE11313',1.00,'2017-10-19 07:00:21','2017-10-19 07:00:21',5,3,1.00,1),(13,4,2,'LOTE11313',2.00,'2017-10-19 07:00:21','2017-10-19 07:00:21',5,3,2.00,1),(14,1,2,'LOTE11313',88.00,'2017-10-19 13:20:52','2017-10-19 13:20:52',7,4,4.00,1);
/*!40000 ALTER TABLE `detalles_despacho_tintoreria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalleventa`
--

DROP TABLE IF EXISTS `detalleventa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalleventa` (
  `nDetVtaCod` int(11) NOT NULL AUTO_INCREMENT,
  `nProAlmCod` int(10) NOT NULL,
  `nVtaCod` varchar(10) NOT NULL,
  `nVtaPeso` double(15,2) NOT NULL,
  `nVtaCant` double(15,2) NOT NULL,
  `nVtaPrecioU` double(15,2) NOT NULL,
  `nVtaPreST` double(15,2) NOT NULL,
  PRIMARY KEY (`nDetVtaCod`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalleventa`
--

LOCK TABLES `detalleventa` WRITE;
/*!40000 ALTER TABLE `detalleventa` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalleventa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devoluciones`
--

DROP TABLE IF EXISTS `devoluciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devoluciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `tipo_devolucion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `observaciones` text COLLATE utf8_unicode_ci,
  `compra_id` int(10) unsigned NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `devoluciones_compra_id_foreign` (`compra_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devoluciones`
--

LOCK TABLES `devoluciones` WRITE;
/*!40000 ALTER TABLE `devoluciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `devoluciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado_cargo`
--

DROP TABLE IF EXISTS `empleado_cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado_cargo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empleado_id` int(11) DEFAULT NULL,
  `cargo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado_cargo`
--

LOCK TABLES `empleado_cargo` WRITE;
/*!40000 ALTER TABLE `empleado_cargo` DISABLE KEYS */;
/*!40000 ALTER TABLE `empleado_cargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleados`
--

DROP TABLE IF EXISTS `empleados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleados` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observaciones` text COLLATE utf8_unicode_ci,
  `cargo_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_created_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_updated_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_deleted_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_created_at` int(11) DEFAULT NULL,
  `userid_updated_at` int(11) DEFAULT NULL,
  `userid_deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleados`
--

LOCK TABLES `empleados` WRITE;
/*!40000 ALTER TABLE `empleados` DISABLE KEYS */;
INSERT INTO `empleados` VALUES (2,'GRIMANIEL','LEON SUXE','1987-09-27','gimaleon2787@hotmail.com','996476722','','1','2017-04-24 17:12:11','2017-10-19 18:18:08',NULL,NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL),(4,'JUAN','PEREZ','2017-10-19','','','','1','2017-10-19 16:50:00','2017-10-19 16:50:04','2017-10-19 16:50:04','Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(5,'EBER','PALLA CANELOS','1990-01-02','','','','1','2017-10-19 18:13:45','2017-10-19 18:13:45',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(6,'JOSE FRANKLIN','LEON PALOMINO','1988-07-04','','','','1','2017-10-19 18:15:55','2017-10-19 18:15:55',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(7,'JHONATHAN','JHONATHAN','1996-05-16','','','','1','2017-10-19 18:17:10','2017-10-19 18:17:10',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(8,'EDWIN','EDWIN','1992-02-04','','','','1','2017-10-19 18:17:50','2017-10-19 18:17:50',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL);
/*!40000 ALTER TABLE `empleados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formapago`
--

DROP TABLE IF EXISTS `formapago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formapago` (
  `nForPagCod` int(11) NOT NULL AUTO_INCREMENT,
  `cDescForPag` varchar(50) NOT NULL,
  PRIMARY KEY (`nForPagCod`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formapago`
--

LOCK TABLES `formapago` WRITE;
/*!40000 ALTER TABLE `formapago` DISABLE KEYS */;
INSERT INTO `formapago` VALUES (1,'Contado'),(2,'Deposito'),(3,'Crédito'),(4,'Cheque');
/*!40000 ALTER TABLE `formapago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `assets` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `history_type_id_foreign` (`type_id`),
  KEY `history_user_id_foreign` (`user_id`),
  CONSTRAINT `history_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `history_types` (`id`) ON DELETE CASCADE,
  CONSTRAINT `history_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history`
--

LOCK TABLES `history` WRITE;
/*!40000 ALTER TABLE `history` DISABLE KEYS */;
INSERT INTO `history` VALUES (1,1,4,3,'trash','bg-maroon','trans(\"history.backend.users.deleted\") Default User',NULL,'2017-09-11 22:20:46','2017-09-11 22:20:46'),(2,1,4,1,'times','bg-yellow','trans(\"history.backend.users.deactivated\") Franck Mercado',NULL,'2017-09-12 21:15:29','2017-09-12 21:15:29'),(3,1,4,1,'check','bg-green','trans(\"history.backend.users.reactivated\") Franck Mercado',NULL,'2017-09-12 21:15:34','2017-09-12 21:15:34'),(4,1,4,1,'times','bg-yellow','trans(\"history.backend.users.deactivated\") Franck Mercado',NULL,'2017-09-19 19:28:27','2017-09-19 19:28:27');
/*!40000 ALTER TABLE `history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history_types`
--

DROP TABLE IF EXISTS `history_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history_types`
--

LOCK TABLES `history_types` WRITE;
/*!40000 ALTER TABLE `history_types` DISABLE KEYS */;
INSERT INTO `history_types` VALUES (1,'User','2017-04-07 10:08:33','2017-04-07 10:08:33'),(2,'Role','2017-04-07 10:08:33','2017-04-07 10:08:33');
/*!40000 ALTER TABLE `history_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `indicador`
--

DROP TABLE IF EXISTS `indicador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `indicador` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) NOT NULL,
  `insumo_id` int(11) NOT NULL,
  `titulo_id` int(10) DEFAULT '0',
  `valor` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `indicador`
--

LOCK TABLES `indicador` WRITE;
/*!40000 ALTER TABLE `indicador` DISABLE KEYS */;
INSERT INTO `indicador` VALUES (18,2,1,10,100.00,'2017-10-19 19:11:59','2017-10-19 19:11:59'),(25,7,6,2,0.98,'2017-10-19 19:14:32','2017-10-19 19:14:32'),(26,7,12,13,0.02,'2017-10-19 19:14:32','2017-10-19 19:14:32'),(27,4,2,10,0.97,'2017-10-19 19:15:05','2017-10-19 19:15:05'),(28,4,4,12,0.03,'2017-10-19 19:15:05','2017-10-19 19:15:05');
/*!40000 ALTER TABLE `indicador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insumos`
--

DROP TABLE IF EXISTS `insumos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insumos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `proveedor_id` int(10) unsigned NOT NULL DEFAULT '0',
  `titulo_id` int(10) unsigned NOT NULL,
  `nombre_generico` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_especifico` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `material` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_created_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_updated_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_deleted_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_created_at` int(10) DEFAULT NULL,
  `userid_updated_at` int(10) DEFAULT NULL,
  `userid_deleted_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `insumos_titulo_id_foreign` (`titulo_id`),
  KEY `insumos_proveedor_id_foreign` (`proveedor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insumos`
--

LOCK TABLES `insumos` WRITE;
/*!40000 ALTER TABLE `insumos` DISABLE KEYS */;
INSERT INTO `insumos` VALUES (1,0,10,'HILO ALGODÓN CARDADO ','HILO ALGODÓN CARDADO ','HILO','esto es Hilo','2017-04-23 05:28:50','2017-10-19 12:36:15',NULL,'Rodolfo','Rodolfo D\'Brot',NULL,4,4,NULL),(2,0,10,'HILO ALGODÓN PEINADO','HILO ALGODÓN PEINADO','HILO','esto es hilo','2017-04-23 05:31:23','2017-10-19 12:37:38',NULL,'Rodolfo','Rodolfo D\'Brot',NULL,4,4,NULL),(3,0,2,'HILO POLYCOTTON  CARDADO  52/48','HILO POLYCOTTON  CARDADO  52/48','HILO','','2017-04-24 18:01:26','2017-04-24 18:01:26',NULL,'Rodolfo',NULL,NULL,4,NULL,NULL),(4,0,12,'LICRA USA','LICRA USA','FILAMENTO','','2017-04-24 18:08:48','2017-10-19 14:54:07',NULL,'Rodolfo','Rodolfo D\'Brot',NULL,4,4,NULL),(5,0,10,'RIB LICRADO ','RIB LICRADO','HILO Y LICRA','','2017-10-19 12:08:13','2017-10-19 12:08:47','2017-10-19 12:08:47','Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(6,0,2,'HILO ALGODÓN CARDADO ','HILO ALGODÓN CARDADO ','HILO','','2017-10-19 14:52:36','2017-10-19 14:56:05',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL),(7,0,2,'HILO ALGODÓN PEINADO','HILO ALGODÓN PEINADO','HILO','','2017-10-19 14:55:44','2017-10-19 14:55:44',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(8,0,2,'HILO MELANSH 10% CARDADO','HILO MELANSH 10% CARDADO','HILO','','2017-10-19 15:09:05','2017-10-19 15:09:05',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(9,0,2,'HILO MELANSH 3% CARDADO ','HILO MELANSH 3% CARDADO','HILO','','2017-10-19 15:09:56','2017-10-19 15:09:56',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(10,0,11,'HILO ALGODÓN PEINADO','HILO ALGODÓN PEINADO','HILO','','2017-10-19 15:10:58','2017-10-19 15:10:58',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(11,0,11,'HILO ALGODÓN CARDADO','HILO ALGODÓN CARDADO ','HILO','','2017-10-19 15:11:30','2017-10-19 15:11:30',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(12,0,13,'LICRA USA','LICRA USA','FILAMENTO','','2017-10-19 18:38:25','2017-10-19 18:38:52',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL);
/*!40000 ALTER TABLE `insumos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locales`
--

DROP TABLE IF EXISTS `locales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ciudad` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `distrito` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observaciones` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locales`
--

LOCK TABLES `locales` WRITE;
/*!40000 ALTER TABLE `locales` DISABLE KEYS */;
/*!40000 ALTER TABLE `locales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maquinas`
--

DROP TABLE IF EXISTS `maquinas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maquinas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observaciones` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maquinas`
--

LOCK TABLES `maquinas` WRITE;
/*!40000 ALTER TABLE `maquinas` DISABLE KEYS */;
INSERT INTO `maquinas` VALUES (2,'PILOTELLI','MAQ-01','','2017-04-24 17:46:53','2017-10-19 15:30:59'),(3,'ORIZIO','MAQ-02','Utiliza el mismo código de aguja que MAQ-03','2017-10-18 11:04:47','2017-10-18 11:05:28'),(4,'ORIZIO','MAQ-03','','2017-10-19 15:25:19','2017-10-19 15:25:19'),(5,'FUKAHAMA','MAQ-04','','2017-10-19 15:26:02','2017-10-19 15:26:02'),(6,'JUMBERCA','MAQ-05','','2017-10-19 15:27:46','2017-10-19 15:27:46'),(7,'FUKAHAMA','MAQ-06','','2017-10-19 15:28:06','2017-10-19 15:28:06'),(8,'PILOTELLY','MAQ-07','','2017-10-19 15:30:44','2017-10-19 15:30:44'),(9,'ORIZIO','MAQ-08','','2017-10-19 15:31:31','2017-10-19 15:31:31'),(10,'FUKAHARA','MAQ-09','','2017-10-19 15:31:59','2017-10-19 15:31:59'),(11,'FUKAHAMA','MAQ-10','','2017-10-19 15:33:55','2017-10-19 15:33:55'),(12,'ORIZIO JHON C','MAQ-11','','2017-10-19 15:34:44','2017-10-19 15:34:44'),(13,'PAILUNG','MAQ-12','','2017-10-19 15:35:21','2017-10-19 15:35:21');
/*!40000 ALTER TABLE `maquinas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marcas`
--

DROP TABLE IF EXISTS `marcas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marcas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marcas`
--

LOCK TABLES `marcas` WRITE;
/*!40000 ALTER TABLE `marcas` DISABLE KEYS */;
/*!40000 ALTER TABLE `marcas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matenimiento`
--

DROP TABLE IF EXISTS `matenimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matenimiento` (
  `nManCod` int(11) NOT NULL AUTO_INCREMENT,
  `cManMonGen` varchar(3) NOT NULL,
  `cManMonEsp` varchar(15) NOT NULL,
  `dManMonCam` double(15,3) NOT NULL,
  PRIMARY KEY (`nManCod`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matenimiento`
--

LOCK TABLES `matenimiento` WRITE;
/*!40000 ALTER TABLE `matenimiento` DISABLE KEYS */;
INSERT INTO `matenimiento` VALUES (1,'D','Dolar',3.721);
/*!40000 ALTER TABLE `matenimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (98,'2014_10_12_000000_create_users_table',1),(99,'2014_10_12_100000_create_password_resets_table',1),(100,'2015_12_28_171741_create_social_logins_table',1),(101,'2015_12_29_015055_setup_access_tables',1),(102,'2016_07_03_062439_create_history_tables',1),(103,'2016_12_29_234847_create_empleados_table',1),(104,'2016_12_29_234908_create_proveedores_table',1),(105,'2016_12_29_235907_create_titulos_table',1),(106,'2016_12_29_235916_create_marcas_table',1),(107,'2016_12_29_235917_create_insumos_table',1),(108,'2016_12_30_000221_create_locales_table',1),(109,'2016_12_30_002126_create_maquinas_table',1),(110,'2017_01_03_005453_create_accesorios_table',1),(111,'2017_01_06_205718_create_procedencias_table',1),(112,'2017_01_06_355329_create_compra_estados_table',1),(113,'2017_01_06_385329_create_compras_table',1),(114,'2017_01_06_390705_create_detalle_compras_table',1),(115,'2017_01_10_021510_create_bancos_table',1),(116,'2017_01_10_021709_create_tipos_pagos_table',1),(117,'2017_01_10_022658_create_cronogramas_table',1),(118,'2017_01_13_142659_create_devoluciones_table',1),(119,'2017_01_13_150751_create_detalle_devoluciones_table',1),(120,'2017_01_17_223449_create_tipos_abonos_table',1),(121,'2017_01_17_223450_create_productos_table',1),(122,'2017_01_17_223536_create_abonos_table',1),(123,'2017_01_17_223540_create_detalle_abonos_table',1),(124,'2017_02_11_212142_create_planeamiento_table',1),(125,'2017_02_11_220614_create_detalle_planeamiento_table',1),(126,'2017_02_12_014331_alter_planeamiento_table_add_turno',1),(127,'2017_02_13_165355_alter_table_planeamiento_remove_empleado_turno_maquina',1),(128,'2017_02_13_165416_alter_table_detalle_planeamiento_add_empleado_turno_maquina',1),(129,'2017_02_14_145408_alter_planeamiento_table_add_estado',1),(130,'2017_02_15_220034_alter_table_detalle_planeamiento_add_turno',1),(131,'2017_02_17_144233_alter_planeamientos_table_add_fields',1),(132,'2017_02_17_144649_alter_table_detalle_planeamiento_remove_fields',1),(133,'2017_02_17_145011_alter_table_detalle_planeamientos_add_fields',1),(134,'2017_02_17_145257_alter_table_detalle_planeamientos_remove_fields',1),(135,'2017_02_17_150633_alter_table_planeamientos_remove_insumo_id',1),(136,'2017_02_17_150728_alter_table_planeamientos_add_accesorio_id',1),(137,'2017_02_18_012812_alter_table_detalle_add_fields',1),(138,'2017_02_18_022256_alter_table_planeamiento_add_data',1),(139,'2017_02_18_194314_alter_table_detalle_planeamiento_remove_data',1),(140,'2017_02_18_194453_alter_table_planeamientos_add_data',1),(141,'2017_02_23_203128_create_table_movimiento_materiaprima',1),(142,'2017_02_23_203629_create_table_resumen_stock_materiaprima',1),(143,'2017_02_23_203804_create_table_movimiento_tela',1),(144,'2017_02_23_203918_create_table_resumen_stock_telas',1),(145,'2017_02_23_204826_alter_table_movimiento_tela_add_proveedor_id',1),(146,'2017_02_24_032044_create_table_indicador',1),(147,'2017_02_24_141025_alter_table_movimiento_tela_cantidad',1),(148,'2017_02_24_141302_alter_table_resumen_stock_tela_cantidad',1),(149,'2017_02_24_151730_alter_table_movimiento_mp_cantidad',1),(150,'2017_02_24_151854_alter_table_resumen_stock_mp_cantidad',1),(151,'2017_02_24_200730_alter_table_planeamiento_mp_producida',1),(152,'2017_02_24_202032_alter_table_planeamiento_mp_producida_remove',1),(153,'2017_02_24_202216_alter_table_detalles_planeamiento_add_materia_prima',1),(154,'2017_02_25_212331_alter_detalle_compras_add_estado',1),(155,'2017_02_25_222653_alter_table_movimiento_tela_add_rollos',1),(156,'2017_02_25_222719_alter_table_resumen_stock_telas_add_rollos',1),(157,'2017_02_27_145812_alter_resumen_stock_mp_materia_prima_add_insumo_id_accesorio_id',1),(158,'2017_02_27_145834_alter_movimiento_mp_materia_prima_add_insumo_id_accesorio_id',1),(159,'2017_02_27_171922_alter_table_movimiento_mp_add_descripcion',1),(160,'2017_02_27_171957_alter_table_resumen_stock__mp_add_descripcion',1),(161,'2017_02_27_173317_alter_table_movimiento_mp_change_description',1),(162,'2017_02_27_173748_alter_table_resumen_mp_stock_remove_descripcion',1),(163,'2017_02_27_181033_create_table_recepcion_mp',1),(164,'2017_02_27_181045_create_table_detalle_recepcion_mp',1),(165,'2017_02_27_220816_alter_table_recepcion_mp_detalles_remove_marca',1),(166,'2017_03_02_041503_alter_table_planeamientos_remove_accesorio',1),(167,'2017_03_02_041526_alter_table_detalle_planeamientos_add_accesorio',1),(168,'2017_03_02_072134_alter_table_planeamiento_remove_lote_accesorio',1),(169,'2017_03_02_072150_alter_table_detalle_planeamiento_add_accesorio',1),(170,'2017_03_02_110736_alter_table_detalle_planeamiento_add_accesorio_change',1),(171,'2017_03_02_201717_create_table_despacho_tintoreria',1),(172,'2017_03_02_210441_create_table_detalles_despacho_tintoreria',1),(173,'2017_03_02_221212_alter_detalles_despacho_add_proveedor_id',1),(174,'2017_03_03_000419_alter_table_movimiento_telas_add_description',1),(175,'2017_03_03_003206_alter_table_detalles_despacho_tintoreria_add_despacho_id',1),(176,'2017_03_12_015238_alter_movimiento_mp_add_peso_neto',1),(177,'2017_03_12_021257_alter_resumen_mp_add_peso_neto',1),(178,'2017_03_16_210250_delete_cantidad',1),(179,'2017_03_16_215225_delete_agujas',1),(180,'2017_03_17_020725_alter_detalle_planeamiento_add_cantidad_table',1),(181,'2017_03_17_021016_delete_address',1),(182,'2017_03_17_111029_alter_detalle_planeamientos_default_value',1),(183,'2017_03_25_045330_alter_detalle_despacho_table_add_rollos',1),(184,'2017_03_26_020805_create_despacho_terceros_table',1),(185,'2017_03_26_020820_create_detalles_despacho_terceros_table',1),(186,'2017_03_26_025450_alter_table_detalle_despacho_terceros',1),(187,'2017_03_26_025843_alter_table_detalle_despachos_add_proveedor_id',1),(188,'2017_03_28_042039_Alter_tipo_cantidad_detalle_planeamiento',1),(189,'2017_03_28_163338_alter_table_planeamiento_add_rollos_falla',1),(190,'2017_03_28_163718_alter_table_planeamiento_add_rollos_falla_default',1),(191,'2017_03_28_172059_alter_table_indicador_change_insumo_id',1),(192,'2017_03_28_181108_alter_table_movimiento_tela_add_mp_producida',1),(193,'2017_04_01_040245_alter_table_mp_remove_mp_producida',1),(194,'2017_04_04_171250_alter_table_insumo_add_descripcion',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimiento_materiaprima`
--

DROP TABLE IF EXISTS `movimiento_materiaprima`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimiento_materiaprima` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `compra_id` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `lote` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_id` int(11) NOT NULL,
  `cantidad` double NOT NULL,
  `estado` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `insumo_id` int(11) NOT NULL,
  `accesorio_id` int(11) NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `peso_neto` double(8,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimiento_materiaprima`
--

LOCK TABLES `movimiento_materiaprima` WRITE;
/*!40000 ALTER TABLE `movimiento_materiaprima` DISABLE KEYS */;
INSERT INTO `movimiento_materiaprima` VALUES (1,'2017-10-18',1,5,'0',5,100,0,'2017-10-18 17:22:31','2017-10-19 16:30:35',0,2,'Compra',0.00),(2,'2017-10-18',2,5,'LOTE11313',1,40,1,'2017-10-18 17:23:58','2017-10-18 17:23:58',1,0,'Compra',1200.00),(3,'2017-10-18',1,5,'0',5,-100,0,'2017-10-19 16:30:35','2017-10-19 16:30:35',0,2,'Eliminar compra',0.00),(4,'2017-08-14',3,5,'212',10,450,1,'2017-10-19 16:36:24','2017-10-19 16:36:24',2,0,'Compra',20412.00),(5,'2017-10-19',4,5,'LOTE2332',2,55,0,'2017-10-19 18:27:05','2017-10-19 18:34:52',3,0,'Compra',1200.00),(6,'2017-09-23',5,5,'F287-A',12,15,1,'2017-10-19 18:34:18','2017-10-19 18:34:18',4,0,'Compra',507.80),(7,'2017-10-19',4,5,'LOTE2332',2,-55,0,'2017-10-19 18:34:52','2017-10-19 18:34:52',3,0,'Eliminar compra',1200.00),(8,'2017-10-19',6,8,'2017',13,20,1,'2017-10-19 18:49:02','2017-10-19 18:49:02',12,0,'Compra',528.00),(9,'2017-10-10',7,5,'3030',2,338,1,'2017-10-19 18:50:33','2017-10-19 18:50:33',6,0,'Compra',15331.68),(10,'2017-10-04',8,3,'0',16,100,1,'2017-10-19 19:50:58','2017-10-19 19:50:58',0,2,'Compra',0.00),(11,'2017-10-04',9,3,'0',17,100,1,'2017-10-19 19:51:36','2017-10-19 19:51:36',0,2,'Compra',0.00),(12,'2017-10-04',10,3,'0',18,100,1,'2017-10-19 19:52:05','2017-10-19 19:52:05',0,4,'Compra',0.00);
/*!40000 ALTER TABLE `movimiento_materiaprima` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimiento_tela`
--

DROP TABLE IF EXISTS `movimiento_tela`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimiento_tela` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `planeacion_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `nro_lote` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rollos` int(11) NOT NULL,
  `cantidad` double NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `estado` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimiento_tela`
--

LOCK TABLES `movimiento_tela` WRITE;
/*!40000 ALTER TABLE `movimiento_tela` DISABLE KEYS */;
INSERT INTO `movimiento_tela` VALUES (1,1,2,3,'LOTE11313',5,10,'Planeamiento de Tela',0,'2017-10-18 17:25:03','2017-10-18 17:25:03'),(2,2,2,3,'LOTE11313',0,450,'Planeamiento de Tela',0,'2017-10-18 22:36:43','2017-10-18 22:36:43'),(3,0,2,5,'LOTE11313',1,1,'Eliminacion de espacho a tintorerias',0,'2017-10-19 11:54:50','2017-10-19 11:54:50'),(4,0,2,5,'LOTE11313',2,2,'Eliminacion de espacho a tintorerias',0,'2017-10-19 11:54:50','2017-10-19 11:54:50'),(5,0,2,5,'LOTE11313',1,1,'Eliminacion de espacho a tintorerias',0,'2017-10-19 11:54:53','2017-10-19 11:54:53'),(6,0,2,5,'LOTE11313',2,2,'Eliminacion de espacho a tintorerias',0,'2017-10-19 11:54:53','2017-10-19 11:54:53'),(7,0,2,5,'LOTE11313',2,40,'Eliminacion de espacho a tintorerias',0,'2017-10-19 11:55:00','2017-10-19 11:55:00'),(8,0,2,7,'LOTE11313',4,88,'Eliminacion de espacho a tintorerias',0,'2017-10-19 20:00:06','2017-10-19 20:00:06'),(9,0,2,5,'LOTE1213',10,110,'Eliminacion de espacho a tintorerias',0,'2017-10-19 20:00:11','2017-10-19 20:00:11'),(10,0,2,5,'LOTE11313',4,8,'Eliminacion de espacho a tintorerias',0,'2017-10-19 20:00:11','2017-10-19 20:00:11');
/*!40000 ALTER TABLE `movimiento_tela` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nota_ingreso`
--

DROP TABLE IF EXISTS `nota_ingreso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nota_ingreso` (
  `ning_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `desptint_id` int(10) unsigned NOT NULL DEFAULT '0',
  `partida` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ning_id`),
  KEY `FK_desptint` (`desptint_id`),
  KEY `partida_2` (`partida`),
  FULLTEXT KEY `partida_3` (`partida`),
  FULLTEXT KEY `partida_4` (`partida`),
  FULLTEXT KEY `partida_5` (`partida`),
  CONSTRAINT `FK_desptint` FOREIGN KEY (`desptint_id`) REFERENCES `detalles_despacho_tintoreria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nota_ingreso`
--

LOCK TABLES `nota_ingreso` WRITE;
/*!40000 ALTER TABLE `nota_ingreso` DISABLE KEYS */;
INSERT INTO `nota_ingreso` VALUES (161,9,'23545');
/*!40000 ALTER TABLE `nota_ingreso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nota_ingreso_a`
--

DROP TABLE IF EXISTS `nota_ingreso_a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nota_ingreso_a` (
  `ninga_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `color_id` int(10) NOT NULL DEFAULT '0',
  `producto_id` int(10) unsigned NOT NULL DEFAULT '0',
  `proveedor_id` int(10) unsigned NOT NULL DEFAULT '0',
  `partida` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ninga_id`),
  KEY `FK_color_id` (`color_id`),
  KEY `FK_producto_id` (`producto_id`),
  KEY `FK_proveedor_id` (`proveedor_id`),
  CONSTRAINT `FK_color_id` FOREIGN KEY (`color_id`) REFERENCES `color` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_producto_id` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_proveedor_id` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nota_ingreso_a`
--

LOCK TABLES `nota_ingreso_a` WRITE;
/*!40000 ALTER TABLE `nota_ingreso_a` DISABLE KEYS */;
INSERT INTO `nota_ingreso_a` VALUES (43,3,3,3,'PA00000000001','2017-10-19 00:00:00');
/*!40000 ALTER TABLE `nota_ingreso_a` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_foreign` (`permission_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1,2),(2,2,2);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'view-backend','View Backend',1,'2017-04-07 10:08:33','2017-04-07 10:08:33'),(2,'manage-users','Manage Users',2,'2017-04-07 10:08:33','2017-04-07 10:08:33'),(3,'manage-roles','Manage Roles',3,'2017-04-07 10:08:33','2017-04-07 10:08:33');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `planeamientos`
--

DROP TABLE IF EXISTS `planeamientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planeamientos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `proveedor_id` int(10) unsigned NOT NULL,
  `producto_id` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `estado` smallint(6) NOT NULL,
  `empleado_id` int(11) NOT NULL,
  `turno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `maquina_id` int(11) NOT NULL,
  `rollos` int(11) NOT NULL,
  `kg_producidos` double(8,2) NOT NULL,
  `kg_falla` double(8,2) NOT NULL,
  `rollos_falla` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `planeamientos_proveedor_id_foreign` (`proveedor_id`),
  KEY `planeamientos_producto_id_foreign` (`producto_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `planeamientos`
--

LOCK TABLES `planeamientos` WRITE;
/*!40000 ALTER TABLE `planeamientos` DISABLE KEYS */;
INSERT INTO `planeamientos` VALUES (1,'2017-10-18',3,2,NULL,'2017-10-18 17:24:33','2017-10-18 17:25:03',1,2,'Mañana',2,5,10.00,0.00,0),(2,'2017-10-18',3,2,NULL,'2017-10-18 22:36:29','2017-10-18 22:36:43',1,2,'Mañana',2,0,450.00,0.00,0),(3,'2017-10-06',3,2,'2017-10-19 19:54:59','2017-10-19 19:54:33','2017-10-19 19:54:59',0,2,'Mañana',5,0,0.00,0.00,0),(4,'2017-10-05',3,2,'2017-10-19 20:07:08','2017-10-19 19:56:20','2017-10-19 20:07:08',0,2,'Mañana',5,0,0.00,0.00,0),(5,'2017-10-19',3,2,'2017-10-19 20:05:38','2017-10-19 20:03:43','2017-10-19 20:05:38',0,2,'Noche',5,0,0.00,0.00,0),(6,'2017-10-05',3,2,'2017-10-19 20:10:24','2017-10-19 20:08:59','2017-10-19 20:10:24',0,2,'Noche',5,0,0.00,0.00,0),(7,'2017-10-19',3,2,NULL,'2017-10-19 20:14:59','2017-10-19 20:14:59',0,2,'Mañana',5,0,0.00,0.00,0),(8,'2017-10-05',3,2,'2017-10-19 20:17:41','2017-10-19 20:16:41','2017-10-19 20:17:41',0,2,'Mañana',5,0,0.00,0.00,0);
/*!40000 ALTER TABLE `planeamientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `procedencias`
--

DROP TABLE IF EXISTS `procedencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procedencias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procedencias`
--

LOCK TABLES `procedencias` WRITE;
/*!40000 ALTER TABLE `procedencias` DISABLE KEYS */;
INSERT INTO `procedencias` VALUES (1,'INDIA','2017-04-23 06:44:02','2017-04-23 06:44:02'),(2,'NACIONAL','2017-10-18 11:13:08','2017-10-18 11:13:08'),(3,'AMERICANA','2017-10-18 12:02:17','2017-10-18 12:02:17'),(4,'CHINA','2017-10-19 13:26:14','2017-10-19 13:26:14'),(5,'ASIÁTICA ','2017-10-19 18:36:36','2017-10-19 18:36:36');
/*!40000 ALTER TABLE `procedencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productoalmacens`
--

DROP TABLE IF EXISTS `productoalmacens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productoalmacens` (
  `nProAlmCod` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cProdCod` int(10) unsigned NOT NULL,
  `id_color` int(11) NOT NULL,
  `nAlmCod` int(10) unsigned NOT NULL,
  `nProdAlmMin` int(11) NOT NULL,
  `nProdAlmMax` int(11) NOT NULL,
  `nProdAlmStock` double(15,2) NOT NULL DEFAULT '0.00',
  `nProdAlmPuni` double(15,2) NOT NULL,
  PRIMARY KEY (`nProAlmCod`),
  KEY `productoalmacens_cprodcod_foreign` (`cProdCod`),
  KEY `productoalmacens_nalmcod_foreign` (`nAlmCod`),
  KEY `id_color` (`id_color`) USING BTREE,
  CONSTRAINT `productoalmacens_cprodcod_foreign` FOREIGN KEY (`cProdCod`) REFERENCES `productos` (`id`),
  CONSTRAINT `productoalmacens_ibfk_1` FOREIGN KEY (`id_color`) REFERENCES `color` (`id`),
  CONSTRAINT `productoalmacens_nalmcod_foreign` FOREIGN KEY (`nAlmCod`) REFERENCES `almacens` (`nAlmCod`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productoalmacens`
--

LOCK TABLES `productoalmacens` WRITE;
/*!40000 ALTER TABLE `productoalmacens` DISABLE KEYS */;
INSERT INTO `productoalmacens` VALUES (38,2,2,8,0,9,194.35,25.00),(39,2,3,8,0,12,0.00,10.00),(43,2,1,8,0,152,819.11,18.00),(45,3,3,9,0,1000,21.00,0.00),(47,2,2,9,0,6,293.20,0.00),(48,2,4,9,0,0,0.00,0.00),(51,2,3,9,0,4,0.00,0.00),(53,2,1,9,0,12,0.00,0.00),(56,2,5,8,0,150,0.00,12.25),(57,2,4,8,0,150,0.00,12.50);
/*!40000 ALTER TABLE `productoalmacens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_generico` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_especifico` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `material` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observaciones` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_created_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_updated_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_deleted_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_created_at` int(10) DEFAULT NULL,
  `userid_updated_at` int(10) DEFAULT NULL,
  `userid_deleted_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (1,'FULL LICRA','TELAS','HILO',NULL,NULL,'2017-04-23 06:26:45','2017-04-24 17:47:30','2017-04-24 17:47:30','Rodolfo',NULL,NULL,4,NULL,NULL),(2,'YERSEY','YERSEY','HILO',NULL,NULL,'2017-04-24 17:50:27','2017-10-19 19:11:58',NULL,'Rodolfo','Rodolfo D\'Brot',NULL,4,4,NULL),(3,'FULL LICRA','TELAS','HILO',NULL,NULL,'2017-04-24 18:10:26','2017-10-19 19:12:51','2017-10-19 19:12:51','Rodolfo',NULL,NULL,4,NULL,NULL),(4,'FULL LICRA 30/1','FULL LICRA 30/1','HILO Y LICRA',NULL,NULL,'2017-10-18 10:57:05','2017-10-18 10:57:43',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL),(5,'YERSEY','YERSEY','HILO',NULL,NULL,'2017-10-19 14:59:23','2017-10-19 15:04:39','2017-10-19 15:04:39','Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(6,'YERSEY','YERSEY','HILO',NULL,NULL,'2017-10-19 15:00:13','2017-10-19 15:04:50','2017-10-19 15:04:50','Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(7,'RIB LICRADO','RIB LICRADO','HILO Y LICRA',NULL,NULL,'2017-10-19 15:06:46','2017-10-19 15:06:46',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor_color`
--

DROP TABLE IF EXISTS `proveedor_color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proveedor_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `codigo` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `estado` tinyint(2) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_created_at` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `user_updated_at` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `user_deleted_at` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `userid_created_at` int(10) DEFAULT NULL,
  `userid_updated_at` int(10) DEFAULT NULL,
  `userid_deleted_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor_color`
--

LOCK TABLES `proveedor_color` WRITE;
/*!40000 ALTER TABLE `proveedor_color` DISABLE KEYS */;
INSERT INTO `proveedor_color` VALUES (1,5,1,'01122',0,'2017-05-09 00:59:07','2017-05-09 00:59:07','2017-05-09 00:59:07',NULL,NULL,NULL,NULL,NULL,NULL),(2,5,2,'01322',0,'2017-05-09 00:59:07','2017-05-09 00:59:07','2017-05-09 00:59:07',NULL,NULL,NULL,NULL,NULL,NULL),(3,5,3,'01312',0,'2017-05-09 00:59:07','2017-05-09 00:59:07','2017-05-09 00:59:07',NULL,NULL,NULL,NULL,NULL,NULL),(4,5,1,'01122',0,'2017-05-09 01:00:20','2017-05-09 01:00:20','2017-05-09 01:00:20',NULL,NULL,NULL,NULL,NULL,NULL),(5,5,2,'01322',0,'2017-05-09 01:00:20','2017-05-09 01:00:20','2017-05-09 01:00:20',NULL,NULL,NULL,NULL,NULL,NULL),(6,5,3,'01312',0,'2017-05-09 01:00:20','2017-05-09 01:00:20','2017-05-09 01:00:20',NULL,NULL,NULL,NULL,NULL,NULL),(7,5,1,'01122',0,'2017-05-09 05:02:02','2017-05-09 05:02:02','2017-05-09 05:02:02',NULL,NULL,NULL,NULL,NULL,NULL),(8,5,2,'01322',0,'2017-05-09 05:02:02','2017-05-09 05:02:02','2017-05-09 05:02:02',NULL,NULL,NULL,NULL,NULL,NULL),(9,5,3,'01312',0,'2017-05-09 05:02:02','2017-05-09 05:02:02','2017-05-09 05:02:02',NULL,NULL,NULL,NULL,NULL,NULL),(10,4,1,'10100',0,'2017-05-09 01:56:57','2017-05-09 01:56:57','2017-05-09 01:56:57',NULL,NULL,NULL,NULL,NULL,NULL),(11,4,2,'10200',0,'2017-05-09 01:56:57','2017-05-09 01:56:57','2017-05-09 01:56:57',NULL,NULL,NULL,NULL,NULL,NULL),(12,4,3,'10302',0,'2017-05-09 01:56:57','2017-05-09 01:56:57','2017-05-09 01:56:57',NULL,NULL,NULL,NULL,NULL,NULL),(13,4,1,'10100',0,'2017-05-10 02:09:59','2017-05-10 02:09:59','2017-05-10 02:09:59',NULL,NULL,NULL,NULL,NULL,NULL),(14,4,2,'10200',0,'2017-05-10 02:09:59','2017-05-10 02:09:59','2017-05-10 02:09:59',NULL,NULL,NULL,NULL,NULL,NULL),(15,4,3,'10302',0,'2017-05-10 02:09:59','2017-05-10 02:09:59','2017-05-10 02:09:59',NULL,NULL,NULL,NULL,NULL,NULL),(16,5,1,'01122',0,'2017-05-10 02:15:11','2017-05-10 02:15:11','2017-05-10 02:15:11',NULL,NULL,NULL,NULL,NULL,NULL),(17,5,2,'01322',0,'2017-05-10 02:15:11','2017-05-10 02:15:11','2017-05-10 02:15:11',NULL,NULL,NULL,NULL,NULL,NULL),(18,5,3,'01312',0,'2017-05-10 02:15:11','2017-05-10 02:15:11','2017-05-10 02:15:11',NULL,NULL,NULL,NULL,NULL,NULL),(19,4,1,'10100',0,'2017-05-10 09:10:43','2017-05-10 09:10:43','2017-05-10 09:10:43',NULL,NULL,NULL,NULL,NULL,NULL),(20,4,2,'10200',0,'2017-05-10 09:10:43','2017-05-10 09:10:43','2017-05-10 09:10:43',NULL,NULL,NULL,NULL,NULL,NULL),(21,4,3,'10302',0,'2017-05-10 09:10:43','2017-05-10 09:10:43','2017-05-10 09:10:43',NULL,NULL,NULL,NULL,NULL,NULL),(22,5,1,'01122',0,'2017-05-10 02:19:07','2017-05-10 02:19:07','2017-05-10 02:19:07',NULL,NULL,NULL,NULL,NULL,NULL),(23,5,2,'01322',0,'2017-05-10 02:19:07','2017-05-10 02:19:07','2017-05-10 02:19:07',NULL,NULL,NULL,NULL,NULL,NULL),(24,5,3,'01312',0,'2017-05-10 02:19:07','2017-05-10 02:19:07','2017-05-10 02:19:07',NULL,NULL,NULL,NULL,NULL,NULL),(25,5,4,'232323',0,'2017-05-10 02:19:07','2017-05-10 02:19:07','2017-05-10 02:19:07',NULL,NULL,NULL,NULL,NULL,NULL),(26,5,2,'01322',0,'2017-05-10 08:35:37','2017-05-10 08:35:37','2017-05-10 08:35:37',NULL,NULL,NULL,NULL,NULL,NULL),(27,5,3,'01312',0,'2017-05-10 08:35:37','2017-05-10 08:35:37','2017-05-10 08:35:37',NULL,NULL,NULL,NULL,NULL,NULL),(28,5,4,'232323',0,'2017-05-10 08:35:37','2017-05-10 08:35:37','2017-05-10 08:35:37',NULL,NULL,NULL,NULL,NULL,NULL),(29,5,1,'011222',0,'2017-10-19 20:45:46','2017-10-19 15:45:46',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(30,5,2,'01322',0,'2017-10-19 20:45:46','2017-10-19 15:45:46',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(31,5,3,'01312',0,'2017-10-19 20:45:46','2017-10-19 15:45:46',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(32,5,4,'232323',0,'2017-10-19 20:45:46','2017-10-19 15:45:46',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(33,4,1,'10100',1,'2017-05-10 09:10:43',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(34,4,2,'10200',1,'2017-05-10 09:10:43',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(35,4,3,'10302',1,'2017-05-10 09:10:43',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(36,4,4,'32311',1,'2017-05-10 09:10:43',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(37,7,1,'',0,'2017-10-18 17:23:32','2017-10-18 12:23:32','2017-10-18 12:23:32',NULL,NULL,NULL,NULL,NULL,NULL),(38,7,2,'',0,'2017-10-18 17:23:32','2017-10-18 12:23:32','2017-10-18 12:23:32',NULL,NULL,NULL,NULL,NULL,NULL),(39,7,1,'028',1,'2017-10-18 17:23:32',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(40,7,2,'029',1,'2017-10-18 17:23:32',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `proveedor_color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor_color_producto`
--

DROP TABLE IF EXISTS `proveedor_color_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor_color_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proveedor_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT '1',
  `color_id` int(11) DEFAULT NULL,
  `moneda_id` int(11) DEFAULT NULL,
  `precio` double(12,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_created_at` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `user_updated_at` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `user_deleted_at` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `userid_created_at` int(10) DEFAULT NULL,
  `userid_updated_at` int(10) DEFAULT NULL,
  `userid_deleted_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor_color_producto`
--

LOCK TABLES `proveedor_color_producto` WRITE;
/*!40000 ALTER TABLE `proveedor_color_producto` DISABLE KEYS */;
INSERT INTO `proveedor_color_producto` VALUES (1,5,2,1,2,25.00,'2017-05-08 20:54:04','2017-05-08 20:54:04',NULL,'Rodolfo','Rodolfo',NULL,4,4,NULL),(2,3,3,1,1,40.00,'2017-05-05 23:40:12','2017-05-05 23:40:12',NULL,'Rodolfo',NULL,NULL,4,NULL,NULL),(3,5,3,3,1,25.00,'2017-05-08 21:09:51','2017-05-08 21:09:51',NULL,'Rodolfo',NULL,NULL,4,NULL,NULL),(4,5,2,4,1,20.00,'2017-05-10 02:15:39','2017-05-10 02:15:39',NULL,'Rodolfo',NULL,NULL,4,NULL,NULL),(5,5,3,1,1,10.00,'2017-05-10 08:36:24','2017-05-10 08:36:24',NULL,'Rodolfo',NULL,NULL,4,NULL,NULL),(6,4,3,1,1,10.00,'2017-05-10 09:11:19','2017-05-10 09:11:19',NULL,'Rodolfo',NULL,NULL,4,NULL,NULL),(7,7,2,1,2,1.00,'2017-10-18 10:43:43','2017-10-18 10:43:43',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL);
/*!40000 ALTER TABLE `proveedor_color_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor_despacho_tintoreria_deuda`
--

DROP TABLE IF EXISTS `proveedor_despacho_tintoreria_deuda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor_despacho_tintoreria_deuda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proveedor_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT '1',
  `color_id` int(11) DEFAULT NULL,
  `moneda_id` int(11) DEFAULT NULL,
  `despacho_id` int(11) DEFAULT NULL,
  `detalle_despacho_id` int(11) DEFAULT NULL,
  `preciounitario` double(12,2) DEFAULT NULL,
  `total` double(12,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_created_at` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `user_updated_at` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `user_deleted_at` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `userid_created_at` int(10) DEFAULT NULL,
  `userid_updated_at` int(10) DEFAULT NULL,
  `userid_deleted_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor_despacho_tintoreria_deuda`
--

LOCK TABLES `proveedor_despacho_tintoreria_deuda` WRITE;
/*!40000 ALTER TABLE `proveedor_despacho_tintoreria_deuda` DISABLE KEYS */;
INSERT INTO `proveedor_despacho_tintoreria_deuda` VALUES (1,5,2,1,2,1,10,25.00,200.00,'2017-10-20 01:00:11','2017-10-19 20:00:11','2017-10-19 20:00:11','Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(2,5,2,4,1,2,11,20.00,800.00,'2017-10-19 16:55:00','2017-10-19 11:55:00','2017-10-19 11:55:00','Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(3,5,2,1,2,3,12,25.00,25.00,'2017-10-19 16:54:50','2017-10-19 11:54:50','2017-10-19 11:54:50','Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(4,5,2,4,1,3,13,20.00,40.00,'2017-10-19 16:54:50','2017-10-19 11:54:50','2017-10-19 11:54:50','Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(5,7,2,1,2,4,14,1.00,88.00,'2017-10-20 01:00:06','2017-10-19 20:00:06','2017-10-19 20:00:06','Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL);
/*!40000 ALTER TABLE `proveedor_despacho_tintoreria_deuda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor_tipo`
--

DROP TABLE IF EXISTS `proveedor_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proveedor_id` int(11) DEFAULT NULL,
  `tipo_proveedor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor_tipo`
--

LOCK TABLES `proveedor_tipo` WRITE;
/*!40000 ALTER TABLE `proveedor_tipo` DISABLE KEYS */;
INSERT INTO `proveedor_tipo` VALUES (2,1,2),(3,1,3),(4,1,4),(5,2,5),(6,2,6),(50,4,2),(51,4,4),(52,6,3),(53,6,5),(54,6,6),(56,7,4),(59,8,2),(60,9,2),(61,10,2),(62,3,2),(63,3,3),(64,3,7),(65,5,2);
/*!40000 ALTER TABLE `proveedor_tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_comercial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `razon_social` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ruc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion_secundaria` text COLLATE utf8_unicode_ci,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ciudad` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observaciones` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_created_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_updated_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_deleted_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_created_at` int(11) DEFAULT NULL,
  `userid_updated_at` int(11) DEFAULT NULL,
  `userid_deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedores`
--

LOCK TABLES `proveedores` WRITE;
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
INSERT INTO `proveedores` VALUES (1,'HILOS S.A','HILOS S.A','35353535','SEBASTIAN BARRANCA 1860',NULL,'consultoriadbrot@gmail.com','956475200','0','','2017-04-23 06:34:02','2017-04-24 17:23:55','2017-04-24 17:23:55','Rodolfo',NULL,NULL,4,NULL,NULL),(2,'RODOLFO SAC','RODOLFO SAC','3456789','SJL-EL BOSQUE',NULL,'admin@corralito.com','99999999','0','','2017-04-23 11:31:24','2017-04-24 17:23:59','2017-04-24 17:23:59','Rodolfo',NULL,NULL,4,NULL,NULL),(3,'BURGA SAC','BURGA SAC','20511794553','MAZ J LT 17 A1 URB CANTO GRANDE S.J.L','','textilburga@hotmail.com','324-1224','0','','2017-04-24 06:12:52','2017-04-26 19:26:05',NULL,'Rodolfo','Rodolfo',NULL,4,4,NULL),(4,'FARIDE ALGODON DEL PERU S.R.L','FARIDE ALGODON DEL PERU S.R.L','20263804300','CALLE ICARO MZ KLT 6 URB LA CAMPIÑA CHORILLOS LIMA-LIMA','','','','0','','2017-04-24 17:31:59','2017-10-18 10:04:58','2017-10-18 10:04:58','Rodolfo','Rodolfo',NULL,4,4,NULL),(5,'JAS IMPORT & EXPORT S.R.L','JAS IMPORT & EXPORT S.R.L','20338048905','AV. LOS LAURELES MZ A LT 20-25B C.P SANTA MARIA DE HUACHIPA LURIGANCHO-LIMA-LIMA','AV. LOS LAURELES MZ A LT 20-25B C.P SANTA MARIA DE HUACHIPA LURIGANCHO-LIMA-LIMA','','','0','','2017-04-24 17:56:11','2017-04-26 19:29:30',NULL,'Rodolfo','Rodolfo',NULL,4,4,NULL),(6,'JJ TELA','JJ TELA','2113131','','','','','0','','2017-05-09 10:13:35','2017-10-19 12:12:59','2017-10-19 12:12:59','Rodolfo',NULL,NULL,4,NULL,NULL),(7,'TEXTIL LUIS ROMERO E.I.R.L','TEXTIL LUIS ROMERO E.I.R.L','20536405322','AV.LOS CANARIOS MZ D2 INT01 LT 3A HUACHIPA SEGUNDA ETAPA LIMA-LURIGANCHO','HUACHIPA','','','0','','2017-10-18 10:34:54','2017-10-18 10:34:54',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(8,'CORP ALBANTEX S..A.C','CORP ALBANTEX S.A.C','20392552431','AV LA PAZ N° 1703 PROV CONST DEL CALLAO','PROV. CONST.DEL CALLAO LA PERLA ','','','0','SPANDEX ACEPORA 40 DN','2017-10-19 15:52:03','2017-10-19 15:52:03',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(9,'TEJIDOS JORGITO S.A.C','TEJIDOS JORGITO S.A.C','20101717098','CALLE LOS TELARES NRO 150 URB IND . VULCANO ATE -LIMA-LIMA','','','','0','','2017-10-19 15:56:26','2017-10-19 15:56:26',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL),(10,'KCTEX INTERNATIONAL LTD','KCTEX INTERNATIONAL LTD','20172018201','INDIA','INDIA','','','0','','2017-10-19 16:28:27','2017-10-19 16:28:27',NULL,'Rodolfo D\'Brot',NULL,NULL,4,NULL,NULL);
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recepcion_mp`
--

DROP TABLE IF EXISTS `recepcion_mp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recepcion_mp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `codigo` int(11) DEFAULT NULL,
  `nro_guia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `observaciones` text COLLATE utf8_unicode_ci,
  `proveedor_id` int(10) unsigned NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recepcion_mp_proveedor_id_foreign` (`proveedor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recepcion_mp`
--

LOCK TABLES `recepcion_mp` WRITE;
/*!40000 ALTER TABLE `recepcion_mp` DISABLE KEYS */;
INSERT INTO `recepcion_mp` VALUES (1,'2017-07-21 06:30:46',1,'GUIA113133',NULL,6,4,4,'2017-07-21 01:30:30','2017-07-21 01:30:46','2017-07-21 01:30:46'),(2,'2017-07-21 06:49:27',2,'GUIA10',NULL,6,4,4,'2017-07-21 01:39:56','2017-07-21 01:49:27','2017-07-21 01:49:27'),(3,'2017-10-14 10:50:28',3,'69779797',NULL,6,4,4,'2017-10-14 05:50:28','2017-10-14 05:50:28',NULL),(4,'2017-10-14 10:50:29',4,'69779797',NULL,6,4,4,'2017-10-14 05:50:29','2017-10-14 05:50:29',NULL);
/*!40000 ALTER TABLE `recepcion_mp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recepcion_mp_detalles`
--

DROP TABLE IF EXISTS `recepcion_mp_detalles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recepcion_mp_detalles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nro_lote` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `peso_bruto` decimal(8,2) NOT NULL,
  `peso_tara` decimal(8,2) NOT NULL,
  `cantidad_paquetes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `observaciones` text COLLATE utf8_unicode_ci,
  `insumo_id` int(10) unsigned DEFAULT NULL,
  `accesorio_id` int(10) unsigned DEFAULT NULL,
  `recepcion_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recepcion_mp_detalles_insumo_id_foreign` (`insumo_id`),
  KEY `recepcion_mp_detalles_accesorio_id_foreign` (`accesorio_id`),
  KEY `recepcion_mp_detalles_recepcion_id_foreign` (`recepcion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recepcion_mp_detalles`
--

LOCK TABLES `recepcion_mp_detalles` WRITE;
/*!40000 ALTER TABLE `recepcion_mp_detalles` DISABLE KEYS */;
INSERT INTO `recepcion_mp_detalles` VALUES (1,'2017-07-21 06:30:30','LOTE5','1',0.00,0.00,'61',NULL,1,NULL,1,'2017-07-21 01:30:30','2017-07-21 01:30:30'),(2,'2017-07-21 06:39:57','LOTE5','1',0.00,0.00,'15',NULL,1,NULL,2,'2017-07-21 01:39:57','2017-07-21 01:39:57'),(3,'2017-10-14 10:50:28','LOTE11','1',0.00,0.00,'13',NULL,1,NULL,3,'2017-10-14 05:50:28','2017-10-14 05:50:28'),(4,'2017-10-14 10:50:29','LOTE11','1',0.00,0.00,'13',NULL,1,NULL,4,'2017-10-14 05:50:29','2017-10-14 05:50:29');
/*!40000 ALTER TABLE `recepcion_mp_detalles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resumen_despacho_tintoreria`
--

DROP TABLE IF EXISTS `resumen_despacho_tintoreria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resumen_despacho_tintoreria` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `producto_id` int(10) DEFAULT NULL,
  `color_id` int(10) DEFAULT NULL,
  `rollos` int(11) DEFAULT NULL,
  `peso` decimal(12,2) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_created_at` varchar(45) DEFAULT NULL,
  `user_updated_at` varchar(45) DEFAULT NULL,
  `user_deleted_at` varchar(45) DEFAULT NULL,
  `userid_created_at` int(10) DEFAULT NULL,
  `userid_updated_at` int(10) DEFAULT NULL,
  `userid_deleted_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resumen_despacho_tintoreria`
--

LOCK TABLES `resumen_despacho_tintoreria` WRITE;
/*!40000 ALTER TABLE `resumen_despacho_tintoreria` DISABLE KEYS */;
INSERT INTO `resumen_despacho_tintoreria` VALUES (1,2,1,0,0.00,'2017-10-18','2017-10-18 22:19:30','2017-10-19 20:00:11',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL),(2,2,4,0,0.00,'2017-10-18','2017-10-18 22:39:59','2017-10-19 11:55:00',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL),(3,2,1,-1,-1.00,'2017-10-19','2017-10-19 07:00:21','2017-10-19 20:00:06',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL),(4,2,4,-2,-2.00,'2017-10-19','2017-10-19 07:00:21','2017-10-19 11:54:53',NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL,4,4,NULL);
/*!40000 ALTER TABLE `resumen_despacho_tintoreria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resumen_stock_materiaprima`
--

DROP TABLE IF EXISTS `resumen_stock_materiaprima`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resumen_stock_materiaprima` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lote` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `titulo_id` int(10) DEFAULT '0',
  `cantidad` double NOT NULL,
  `estado` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `insumo_id` int(11) NOT NULL,
  `accesorio_id` int(11) NOT NULL,
  `peso_neto` double(8,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resumen_stock_materiaprima`
--

LOCK TABLES `resumen_stock_materiaprima` WRITE;
/*!40000 ALTER TABLE `resumen_stock_materiaprima` DISABLE KEYS */;
INSERT INTO `resumen_stock_materiaprima` VALUES (1,'0',5,5,-10,1,'2017-10-18 17:22:31','2017-10-19 16:30:35',0,2,0.00),(2,'LOTE11313',3,1,40,1,'2017-10-18 17:23:58','2017-10-18 22:36:43',1,0,740.00),(3,'212',5,10,450,1,'2017-10-19 16:36:24','2017-10-19 16:36:24',2,0,20412.00),(5,'F287-A',5,12,15,1,'2017-10-19 18:34:18','2017-10-19 18:34:18',4,0,507.80),(6,'2017',8,13,20,1,'2017-10-19 18:49:02','2017-10-19 18:49:02',12,0,528.00),(7,'3030',5,2,338,1,'2017-10-19 18:50:33','2017-10-19 18:50:33',6,0,15331.68),(8,'0',3,16,100,1,'2017-10-19 19:50:58','2017-10-19 19:50:58',0,2,0.00),(9,'0',3,17,100,1,'2017-10-19 19:51:36','2017-10-19 19:51:36',0,2,0.00),(10,'0',3,18,100,1,'2017-10-19 19:52:05','2017-10-19 19:52:05',0,4,0.00);
/*!40000 ALTER TABLE `resumen_stock_materiaprima` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resumen_stock_telas`
--

DROP TABLE IF EXISTS `resumen_stock_telas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resumen_stock_telas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `nro_lote` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cantidad` double NOT NULL,
  `rollos` int(11) NOT NULL,
  `estado` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `userid_created_at` int(10) DEFAULT NULL,
  `userid_updated_at` int(10) DEFAULT NULL,
  `userid_deleted_at` int(10) DEFAULT NULL,
  `user_created_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_updated_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_deleted_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resumen_stock_telas`
--

LOCK TABLES `resumen_stock_telas` WRITE;
/*!40000 ALTER TABLE `resumen_stock_telas` DISABLE KEYS */;
INSERT INTO `resumen_stock_telas` VALUES (1,2,3,'LOTE11313',321,-8,0,'2017-10-18 17:25:03','2017-10-19 13:20:52',NULL,4,4,NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL),(2,2,5,'LOTE11313',54,12,0,'2017-10-19 11:54:50','2017-10-19 20:00:11',NULL,4,4,NULL,'Rodolfo D\'Brot','Rodolfo D\'Brot',NULL),(3,2,7,'LOTE11313',88,4,0,'2017-10-19 20:00:06','2017-10-19 20:00:06',NULL,4,NULL,NULL,'Rodolfo D\'Brot',NULL,NULL),(4,2,5,'LOTE1213',110,10,0,'2017-10-19 20:00:11','2017-10-19 20:00:11',NULL,4,NULL,NULL,'Rodolfo D\'Brot',NULL,NULL);
/*!40000 ALTER TABLE `resumen_stock_telas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1,1),(2,2,2),(3,3,3),(4,4,1),(5,5,3),(6,6,3),(7,7,3),(8,8,3);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `all` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Administrator',1,1,'2017-04-07 10:08:32','2017-04-07 10:08:32'),(2,'Executive',0,2,'2017-04-07 10:08:32','2017-04-07 10:08:32'),(3,'User',0,3,'2017-04-07 10:08:32','2017-04-07 10:08:32');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_logins`
--

DROP TABLE IF EXISTS `social_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_logins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `provider` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `social_logins_user_id_foreign` (`user_id`),
  CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_logins`
--

LOCK TABLES `social_logins` WRITE;
/*!40000 ALTER TABLE `social_logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tienda`
--

DROP TABLE IF EXISTS `tienda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tienda` (
  `tienda_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cTieNom` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `cTieUbi` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`tienda_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tienda`
--

LOCK TABLES `tienda` WRITE;
/*!40000 ALTER TABLE `tienda` DISABLE KEYS */;
/*!40000 ALTER TABLE `tienda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_proveedor`
--

DROP TABLE IF EXISTS `tipo_proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_proveedor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_created_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_updated_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_deleted_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid_created_at` smallint(6) DEFAULT NULL,
  `userid_updated_at` smallint(6) DEFAULT NULL,
  `userid_deleted_at` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_proveedor`
--

LOCK TABLES `tipo_proveedor` WRITE;
/*!40000 ALTER TABLE `tipo_proveedor` DISABLE KEYS */;
INSERT INTO `tipo_proveedor` VALUES (1,'Compra edit',1,'2017-04-16 07:54:15','2017-04-16 09:01:59','2017-04-16 09:01:59','Rodolfo','Rodolfo',NULL,4,4,NULL),(2,'Compra',1,'2017-04-16 09:17:59','2017-04-16 09:18:15',NULL,'Rodolfo','Rodolfo',NULL,4,4,NULL),(3,'Planeamiento',1,'2017-04-16 11:44:08','2017-04-16 11:44:08',NULL,'Rodolfo',NULL,NULL,4,NULL,NULL),(4,'Tintoreria',1,'2017-04-23 06:28:59','2017-04-23 06:28:59',NULL,'Rodolfo',NULL,NULL,4,NULL,NULL),(5,'Despacho de Terceros',1,'2017-04-23 11:23:27','2017-04-23 11:23:27',NULL,'Rodolfo',NULL,NULL,4,NULL,NULL),(6,'Recepcion Materia Prima',1,'2017-04-23 11:23:44','2017-04-23 11:23:44',NULL,'Rodolfo',NULL,NULL,4,NULL,NULL),(7,'BURGA SAC',1,'2017-04-24 06:10:47','2017-04-24 06:10:47',NULL,'Rodolfo',NULL,NULL,4,NULL,NULL);
/*!40000 ALTER TABLE `tipo_proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipodocpago`
--

DROP TABLE IF EXISTS `tipodocpago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodocpago` (
  `nTipPagCod` int(11) NOT NULL AUTO_INCREMENT,
  `cDescTipPago` varchar(50) NOT NULL,
  PRIMARY KEY (`nTipPagCod`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipodocpago`
--

LOCK TABLES `tipodocpago` WRITE;
/*!40000 ALTER TABLE `tipodocpago` DISABLE KEYS */;
INSERT INTO `tipodocpago` VALUES (1,'Guía Venta'),(2,'Boleta'),(3,'Factura');
/*!40000 ALTER TABLE `tipodocpago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipodocs`
--

DROP TABLE IF EXISTS `tipodocs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodocs` (
  `nTdocCod` int(11) NOT NULL AUTO_INCREMENT,
  `cTdocSigla` varchar(3) NOT NULL,
  `cTdocDesc` varchar(40) NOT NULL,
  PRIMARY KEY (`nTdocCod`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipodocs`
--

LOCK TABLES `tipodocs` WRITE;
/*!40000 ALTER TABLE `tipodocs` DISABLE KEYS */;
INSERT INTO `tipodocs` VALUES (1,'RUC','Registro Único de Contribuyente'),(2,'DNI','Documento Nacional de Identidad');
/*!40000 ALTER TABLE `tipodocs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos_abonos`
--

DROP TABLE IF EXISTS `tipos_abonos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipos_abonos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipos_abonos`
--

LOCK TABLES `tipos_abonos` WRITE;
/*!40000 ALTER TABLE `tipos_abonos` DISABLE KEYS */;
INSERT INTO `tipos_abonos` VALUES (1,'Concepto Abono 01',NULL,NULL),(2,'Concepto Abono 02',NULL,NULL),(3,'Concepto Abono 03',NULL,NULL),(4,'Concepto Abono 04',NULL,NULL);
/*!40000 ALTER TABLE `tipos_abonos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos_pagos`
--

DROP TABLE IF EXISTS `tipos_pagos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipos_pagos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipos_pagos`
--

LOCK TABLES `tipos_pagos` WRITE;
/*!40000 ALTER TABLE `tipos_pagos` DISABLE KEYS */;
INSERT INTO `tipos_pagos` VALUES (1,'Credito',NULL,NULL),(2,'Contado',NULL,NULL);
/*!40000 ALTER TABLE `tipos_pagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `titulos`
--

DROP TABLE IF EXISTS `titulos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `titulos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `materia_prima` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `titulos`
--

LOCK TABLES `titulos` WRITE;
/*!40000 ALTER TABLE `titulos` DISABLE KEYS */;
INSERT INTO `titulos` VALUES (2,'30/1','insumo','2017-04-23 05:08:42','2017-04-23 05:08:42'),(10,'20/1','insumo','2017-10-19 12:03:33','2017-10-19 12:31:13'),(11,'50/1','insumo','2017-10-19 12:05:26','2017-10-19 12:31:28'),(12,'20-DN','insumo','2017-10-19 12:05:50','2017-10-19 12:31:51'),(13,'40-DN','insumo','2017-10-19 12:32:17','2017-10-19 12:32:17'),(16,'G001','accesorio','2017-10-19 19:46:36','2017-10-19 19:46:36'),(17,'G002','accesorio','2017-10-19 19:46:45','2017-10-19 19:46:45'),(18,'G003','accesorio','2017-10-19 19:46:55','2017-10-19 19:46:55');
/*!40000 ALTER TABLE `titulos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transferencia`
--

DROP TABLE IF EXISTS `transferencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transferencia` (
  `cTraCod` varchar(11) NOT NULL,
  `nAlmCod1` int(10) NOT NULL,
  `nAlmCod2` int(10) NOT NULL,
  `tTraFechReg` date NOT NULL,
  `tTraFecUlt` date NOT NULL DEFAULT '0000-00-00',
  `cTraObs` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`cTraCod`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transferencia`
--

LOCK TABLES `transferencia` WRITE;
/*!40000 ALTER TABLE `transferencia` DISABLE KEYS */;
INSERT INTO `transferencia` VALUES ('T0000000001',8,9,'2017-10-02','2017-10-02',''),('T0000000002',8,9,'2017-10-02','2017-10-02',''),('T0000000003',8,9,'2017-10-19','2017-10-19','');
/*!40000 ALTER TABLE `transferencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transferencia_detalle`
--

DROP TABLE IF EXISTS `transferencia_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transferencia_detalle` (
  `nTraDetCod` int(11) NOT NULL AUTO_INCREMENT,
  `nProAlmCod` int(10) NOT NULL,
  `cod_barras` varchar(20) NOT NULL,
  `dTraCant` double(15,2) NOT NULL,
  `bTraDev` tinyint(1) NOT NULL DEFAULT '0',
  `cTraCod` varchar(11) NOT NULL,
  PRIMARY KEY (`nTraDetCod`),
  KEY `cTraCod` (`cTraCod`),
  CONSTRAINT `transferencia_detalle_ibfk_1` FOREIGN KEY (`cTraCod`) REFERENCES `transferencia` (`cTraCod`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transferencia_detalle`
--

LOCK TABLES `transferencia_detalle` WRITE;
/*!40000 ALTER TABLE `transferencia_detalle` DISABLE KEYS */;
INSERT INTO `transferencia_detalle` VALUES (1,38,'',100.00,5,'T0000000003');
/*!40000 ALTER TABLE `transferencia_detalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `confirmation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Franck Mercado','franckmercado@gmail.com','$2y$10$TM96BQfzwk.6K6x368Df2eljgMlldvaeNaRT43IGo59XBpN60O34S',0,'a2b31ed9ab1831a335fbfa5202080411',1,'DyR9PoDjro6DnI0MC6RRDKDZhGYFKDCCqRX9WHRScO7Rf79B333iKempWzPK','2017-04-07 10:08:32','2017-09-19 19:28:27',NULL),(2,'Backend User','executive@executive.com','$2y$10$RwNTNlaW3YwkSzxjwlod/uNLTw5KRzDr.EzPLeba1Z.GKXMmUa6uO',1,'009b535bd1479946294c1cd1ea06707e',1,NULL,'2017-04-07 10:08:32','2017-04-07 10:08:32',NULL),(3,'Default User','user@user.com','$2y$10$.6sLbnQ2BtkSog8qiLKwjuewyH.x8oqxDpjXTSpn/x9stJ3ZqBWoG',1,'e509e7eafa59472ab03480be01391d94',1,NULL,'2017-04-07 10:08:32','2017-09-11 22:20:45','2017-09-11 22:20:45'),(4,'Rodolfo D\'Brot','consultoriadbrot@gmail.com','$2y$10$tArrwz042wFutts5lJegke9.WI3b/TXRG3mb4iYRTGcyHnyWPYyNW',1,'c45db330de617c77643f452078087617',1,'CVGRw25JDGtvkAiKeZnsSafLSGngsH0PwloM6T4G1kKqhUaQ8zGm7CbgccCC','2017-04-07 10:08:32','2017-10-20 06:04:55',NULL),(5,'prueba','prueba@gmail.com','$2y$10$5vko2Fn9B3TKzPlWlEOvc.MJuUNNKqCCDUowl6rYl3KtdlFob1rqa',1,'2dfa38b4d3edacfc8454b1f471fbe287',1,'yLv9GNuGbqQ03Nw4PUhh1MDqKflXnVmiCDGbMUQn7goaKNtHBTDlEJYyVS33','2017-09-01 22:49:50','2017-09-08 04:13:48',NULL),(6,'prueba100','prueba100@gmail.com','$2y$10$We3CJLyLFB/T3nkSf4unYOZw15nobH5Oc4mSct64G8TaQZ/RYClSq',1,'9b26db3430fdf4fa3ad8508a5777bf85',1,'raW3N3aaOeDYWIAIHfDWApRfG8Pw08OByusExJKJUKSgFygVqLsi3dwUbzhk','2017-09-09 05:11:30','2017-09-11 06:25:44',NULL),(7,'Irwin Lizandro Yauri Orihuela','wirwinw@gmail.com','$2y$10$VvLaJ8F8zyxj.7xWaZ3/iOZM0Mfk5eqUXfDqCEpDvxh7E0PzFqXm2',1,'e0310017738cc2dc5bee671e508ff110',1,NULL,'2017-09-17 03:19:36','2017-09-17 03:19:36',NULL),(8,'Irwin Lizandro Yauri Orihuela','prueba1@gmail.com','$2y$10$3QX18Ltq4uDqiXsP2E1yiOqFeWD54SMMDG0YyUbqUYCR2ZT1qIIe2',1,'763da3fd2c0e16470aff0cf715828028',1,'SXoWxD8vOz5SEqtSM1FApAaoIkpDU66vnpOzspBlhuf7sWw7eh6LPRRWMiUe','2017-09-17 03:20:56','2017-09-17 03:23:13',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ventas`
--

DROP TABLE IF EXISTS `ventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventas` (
  `nVtaCod` varchar(10) NOT NULL,
  `nClieCod` int(11) NOT NULL,
  `nTipPagCod` int(11) NOT NULL,
  `nForPagCod` int(11) NOT NULL,
  `dDniContacto` varchar(11) NOT NULL,
  `cContacto` varchar(250) NOT NULL,
  `cBanco` varchar(250) NOT NULL,
  `cNCheque` varchar(20) NOT NULL,
  `cNoperacion` varchar(20) NOT NULL,
  `nTotal_sol` double(15,2) NOT NULL,
  `nTotal_dol` double(15,2) NOT NULL,
  `nTipCam` double(15,2) NOT NULL,
  `dVFacFemi` datetime NOT NULL,
  `bSV` char(1) NOT NULL DEFAULT '0',
  `dVFacSTot` double(15,2) NOT NULL,
  `dVFacIgv` double(15,2) NOT NULL,
  `dVFacVTot` double(15,2) NOT NULL,
  `anulado` tinyint(1) NOT NULL DEFAULT '0',
  `cFacNumFac` varchar(10) NOT NULL,
  PRIMARY KEY (`nVtaCod`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ventas`
--

LOCK TABLES `ventas` WRITE;
/*!40000 ALTER TABLE `ventas` DISABLE KEYS */;
/*!40000 ALTER TABLE `ventas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-20 13:50:28
