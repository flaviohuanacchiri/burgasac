ALTER TABLE `burgasac`.`despacho_tintoreria`   
  ADD COLUMN `planeamiento_id` INT NULL AFTER `proveedor_id`;
ALTER TABLE `burgasac`.`detalles_despacho_terceros`   
  DROP COLUMN `color_id`, 
  CHANGE `despacho_id` `despacho_id` INT(10) UNSIGNED NOT NULL  AFTER `producto_id`,
  CHANGE `proveedor_id` `proveedor_id` INT(10) UNSIGNED NOT NULL  AFTER `despacho_id`,
  CHANGE `cantidad` `cantidad` DOUBLE(8,2) UNSIGNED NOT NULL  AFTER `proveedor_id`,
  CHANGE `rollos` `rollos` DOUBLE(8,2) NOT NULL  AFTER `cantidad`,
  CHANGE `created_at` `created_at` TIMESTAMP NULL  AFTER `rollos`,
  CHANGE `updated_at` `updated_at` DATETIME NULL;


ALTER TABLE `burgasac`.`despacho_terceros`   
  ADD COLUMN `planeamiento_id` INT NULL AFTER `proveedor_id`;

ALTER TABLE `burgasac`.`despacho_terceros`   
  ADD COLUMN `direccionpartida` VARCHAR(300) NULL AFTER `fecha`,
  ADD COLUMN `direccionllegada` VARCHAR(300) NULL AFTER `direccionpartida`;
