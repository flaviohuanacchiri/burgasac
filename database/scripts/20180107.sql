ALTER TABLE `burgasac`.`compras`   
  ADD COLUMN `precio` DECIMAL(10,2) DEFAULT 0.00  NULL AFTER `procedencia_id`,
  ADD COLUMN `moneda` TINYINT(1) DEFAULT 2  NULL  COMMENT '1:soles, 2: dolares' AFTER `precio`,
  ADD COLUMN `total` DECIMAL(10,2) DEFAULT 0.00  NULL AFTER `moneda`;