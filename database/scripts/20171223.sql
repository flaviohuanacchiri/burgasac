CREATE TABLE `burgasac`.`costo_proveedor_producto`(  
  `id` INT NOT NULL AUTO_INCREMENT,
  `producto_id` INT UNSIGNED NOT NULL,
  `titulo_id` INT UNSIGNED NOT NULL,
  `moneda_id` TINYINT DEFAULT 2,
  `costo` DECIMAL(10,2) DEFAULT 0.00,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME,
  `deleted_at` DATETIME,
  `user_created_at` VARCHAR(45),
  `user_updated_at` VARCHAR(45),
  `user_deleted_at` VARCHAR(45),
  `userid_created_at` INT,
  `userid_updated_at` INT,
  `userid_deleted_at` INT,
  PRIMARY KEY (`id`),
  INDEX `IND_PRODUCTO_ID` (`producto_id`),
  INDEX `IND_TITULO_ID` (`titulo_id`),
  CONSTRAINT `fk_costo_producto_id` FOREIGN KEY (`producto_id`) REFERENCES `burgasac`.`productos`(`id`) ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT `fk_costo_titulo_id` FOREIGN KEY (`titulo_id`) REFERENCES `burgasac`.`titulos`(`id`) ON UPDATE RESTRICT ON DELETE RESTRICT
) ENGINE=INNODB;


ALTER TABLE `burgasac`.`costo_proveedor_producto`   
  ADD COLUMN `proveedor_id` INT UNSIGNED NOT NULL AFTER `id`, 
  ADD  INDEX `IND_PROVEEDOR_ID` (`proveedor_id`),
  ADD CONSTRAINT `fk_costo_proveedor_id` FOREIGN KEY (`proveedor_id`) REFERENCES `burgasac`.`proveedores`(`id`);

ALTER TABLE `burgasac`.`costo_proveedor_producto`   
  CHANGE `costo` `precio` DECIMAL(10,2) DEFAULT 0.00  NULL;

CREATE TABLE produccion_costo LIKE costo_proveedor_producto;

ALTER TABLE `burgasac`.`produccion_costo`   
  DROP COLUMN `producto_id`, 
  DROP COLUMN `titulo_id`, 
  DROP COLUMN `moneda_id`, 
  DROP COLUMN `precio`, 
  ADD COLUMN `planeamiento_id` INT UNSIGNED NOT NULL AFTER `id`,
  CHANGE `proveedor_id` `costo_proveedor_producto_id` INT(11) UNSIGNED NOT NULL,
  ADD COLUMN `lote` VARCHAR(20) NULL AFTER `costo_proveedor_producto_id`,
  ADD COLUMN `kilos` DECIMAL(10,2) DEFAULT 0.00  NULL AFTER `lote`,
  ADD COLUMN `total` DECIMAL(10,2) DEFAULT 0.00  NULL AFTER `kilos`, 
  DROP INDEX `IND_PRODUCTO_ID`,
  DROP INDEX `IND_TITULO_ID`,
  DROP INDEX `IND_PROVEEDOR_ID`,
  ADD CONSTRAINT `FK_PRODUCCION_PLANEAMIENTO_ID` FOREIGN KEY (`planeamiento_id`) REFERENCES `burgasac`.`planeamientos`(`id`);

ALTER TABLE `burgasac`.`produccion_costo`   
  ADD COLUMN `precio` DECIMAL(10,2) DEFAULT 0.00  NULL AFTER `kilos`;
