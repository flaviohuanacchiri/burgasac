CREATE TABLE `burgasac`.`despacho_tintoreria_planeamiento`(  
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `planeamiento_id` INT,
  `despacho_tintoreria_id` INT,
  `peso` DECIMAL(10,2) DEFAULT 0.00,
  PRIMARY KEY (`id`)
);
ALTER TABLE `burgasac`.`despacho_tintoreria_planeamiento`   
  ADD COLUMN `detalles_despacho_tintoreria_id` INT(11) NULL AFTER `despacho_tintoreria_id`;

  USE burgasac;
CREATE TABLE despacho_tercero_planeamiento LIKE despacho_tintoreria_planeamiento;
ALTER TABLE `burgasac`.`despacho_tintoreria_planeamiento`   
  CHANGE `despacho_tintoreria_id` `despacho_tercero_id` INT(11) NULL,
  CHANGE `detalles_despacho_tintoreria_id` `detalles_despacho_tercero_id` INT(11) NULL;
ALTER TABLE `burgasac`.`despacho_tercero_planeamiento`   
  CHANGE `despacho_tintoreria_id` `despacho_tercero_id` INT(11) NULL,
  CHANGE `detalles_despacho_tintoreria_id` `detalles_despacho_tercero_id` INT(11) NULL;