ALTER TABLE `burgasac`.`proveedor_despacho_tercero_deuda`   
  CHANGE `total` `total` DOUBLE(12,2) DEFAULT 0.00  NULL,
  ADD COLUMN `totalpagado` DOUBLE(12,2) DEFAULT 0.00  NULL AFTER `total`,
  ADD COLUMN `estado` TINYINT(2) DEFAULT 0  NULL  COMMENT '0: Pendiente, 1: Incompleto, 2: Completo' AFTER `totalpagado`;
  
  ALTER TABLE `burgasac`.`proveedor_despacho_tintoreria_deuda`   
  ADD COLUMN `totalpagado` DOUBLE(12,2) DEFAULT 0.00  NULL AFTER `total`,
  ADD COLUMN `estado` TINYINT(2) DEFAULT 0  NULL  COMMENT '0: Pendiente, 1: Incompleto, 2: Completo' AFTER `totalpagado`;

  ALTER TABLE `burgasac`.`compras`   
  ADD COLUMN `totalpagado` DECIMAL(10,2) DEFAULT 0.00  NULL AFTER `total`,
  ADD COLUMN `estado_pago` TINYINT(2) DEFAULT 0  NULL  COMMENT '0:Pendiente, 1: Incompleto, 2: Completo' AFTER `estado`;

  CREATE TABLE pago LIKE proveedor_despacho_tintoreria_deuda;
  ALTER TABLE `burgasac`.`pago`   
  DROP COLUMN `despacho_id`, 
  DROP COLUMN `preciounitario`, 
  DROP COLUMN `totalpagado`, 
  DROP COLUMN `estado`, 
  CHANGE `color_id` `compra_id` INT(11) NULL  AFTER `id`,
  CHANGE `proveedor_id` `despacho_tercero_id` INT(11) NULL  AFTER `compra_id`,
  CHANGE `producto_id` `despacho_tintoreria_id` INT(11) NULL,
  CHANGE `detalle_despacho_id` `detalle_despacho_id` INT(11) NULL  AFTER `despacho_tintoreria_id`,
  CHANGE `moneda_id` `moneda_id` INT(11) NULL  AFTER `detalle_despacho_id`,
  CHANGE `total` `total` DOUBLE(12,2) DEFAULT 0.00  NULL  AFTER `moneda_id`;

ALTER TABLE `burgasac`.`pago`   
  ADD COLUMN `nro_comprobante` VARCHAR(20) NULL AFTER `moneda_id`,
  ADD COLUMN `tipo_comprobante` TINYINT NULL AFTER `nro_comprobante`;
