<?php
/*
 * Comercializacion
 */
route::group(['middleware'=> 'perfiles'],function(){

Route::get('comercializacion/comercializacion/reporte','Comercializacion\\ComercializacionController@genExcel');
Route::resource('comercializacion/comercializacion','Comercializacion\\ComercializacionController');
route::resource("comercializacion/notaingresoatipico",'Comercializacion\\NotaIngresoAController');
Route::resource('comercializacion/almacen','Comercializacion\\AlmacenController');
Route::resource('comercializacion/transferencia','Comercializacion\\TransferenciaController');
Route::resource('comercializacion/cliente','Comercializacion\\ClienteController');
Route::get('comercializacion/notaingreso/create/{id}', 'Comercializacion\\NotaIngresoController@create')->name('notaingreso.create');
Route::get('comercializacion/notaingreso/editar/{id}', 'Comercializacion\\NotaIngresoController@editar')->name('notaingreso.editar');
Route::get('comercializacion/notaingreso/show/{id}', 'Comercializacion\\NotaIngresoController@show')->name('notaingreso.show');
Route::post('comercializacion/notaingreso/store', 'Comercializacion\\NotaIngresoController@store')->name('notaingreso.store');
Route::get('comercializacion/notaingreso/impresion/{id}', 'Comercializacion\\NotaIngresoController@impresion')->name('notaingreso.impresion');
Route::get('comercializacion/notaingresoatipico/i/impresion', 'Comercializacion\\NotaIngresoAController@impresion')->name('notaingresoatipico.i.impresion');
Route::get('comercializacion/almacen/proalm_create/{id}', 'Comercializacion\\AlmacenController@proalm_create')->name('almacen.proalm_create');
Route::post('comercializacion/almacen/proalm_store/{id}', 'Comercializacion\\AlmacenController@proalm_store')->name('almacen.proalm_store');
Route::delete('comercializacion/almacen/proalm_destroy/{id}', 'Comercializacion\\AlmacenController@proalm_destroy')->name('almacen.proalm_destroy');
Route::get('comercializacion/almacen/proalm_edit/{id}', 'Comercializacion\\AlmacenController@proalm_edit')->name('almacen.proalm_edit');
Route::put('comercializacion/almacen/proalm_update/{id}', 'Comercializacion\\AlmacenController@proalm_update')->name('almacen.proalm_update');
Route::resource('venta','Comercializacion\\VFacturaController');

Route::resource('area','Comercializacion\\AreasController');
Route::resource('areaalmacen','Comercializacion\\Areaalmacen');

Route::resource('comercializacion/inventario','Comercializacion\\InventarioController');
Route::get('comercializacion/inventario/pdf/reportecierre/{id}', 'Comercializacion\\InventarioController@reportecierre')->name('inventario.reportecierre');
Route::get('comercializacion/inventario/pdf/detalleingreso/{id}', 'Comercializacion\\InventarioController@detalleingreso')->name('inventario.detalleingreso');

Route::get('comercializacion/areaalmacen/proalm_create/{id}', 'Comercializacion\\Areaalmacen@proalm_create')->name('areaalmacen.proalm_create');
Route::post('comercializacion/areaalmacen/proalm_store/{id}', 'Comercializacion\\Areaalmacen@proalm_store')->name('areaalmacen.proalm_store');
Route::delete('comercializacion/areaalmacen/proalm_destroy/{id}', 'Comercializacion\\Areaalmacen@proalm_destroy')->name('areaalmacen.proalm_destroy');
Route::get('comercializacion/areaalmacen/proalm_edit/{id}', 'Comercializacion\\Areaalmacen@proalm_edit')->name('areaalmacen.proalm_edit');
Route::put('comercializacion/areaalmacen/proalm_update/{id}', 'Comercializacion\\Areaalmacen@proalm_update')->name('areaalmacen.proalm_update');


/************************** OPEN CAJA ***************************/
Route::get('comercializacion/caja/abrircaja', 'Comercializacion\\CajaController@abrircaja')->name('caja.abrircaja');
Route::post('comercializacion/caja/abrircaja_store/{id}', 'Comercializacion\\CajaController@abrircaja_store')->name('caja.abrircaja_store');
Route::get('comercializacion/caja/cerrarcaja/{id}', 'Comercializacion\\CajaController@cerrarcaja')->name('caja.cerrarcaja');
Route::post('comercializacion/caja/cerrarcaja_store/{id}', 'Comercializacion\\CajaController@cerrarcaja_store')->name('caja.cerrarcaja_store');
Route::get('comercializacion/caja/transferencia', 'Comercializacion\\CajaController@transferencia')->name('caja.transferencia');
Route::get('comercializacion/caja/index', 'Comercializacion\\CajaController@index')->name('caja.index');
Route::post('comercializacion/caja/transferencia_store/{id}', 'Comercializacion\\CajaController@transferencia_store')->name('caja.transferencia_store');
Route::delete('comercializacion/caja/transferencia_destroy/{id}', 'Comercializacion\\CajaController@transferencia_destroy')->name('caja.transferencia_destroy');
Route::get('comercializacion/caja/reportecajacierre/{id}', 'Comercializacion\\CajaController@reportecajacierre')->name('caja.reportecajacierre');
Route::get('comercializacion/caja/cajaoperaeditar/{id}', 'Comercializacion\\CajaController@cajaoperaeditar')->name('caja.cajaoperaeditar');

Route::put('comercializacion/caja/cajaoperaeditar_put/{id}', 'Comercializacion\\CajaController@cajaoperaeditar_put')->name('caja.cajaoperaeditar_put');

Route::delete('comercializacion/caja/operacion_destroy/{id}', 'Comercializacion\\CajaController@operacion_destroy')->name('caja.operacion_destroy');

Route::resource('comercializacion/notadevolucion','Comercializacion\\NotadevController');
Route::get('comercializacion/notadevolucion/create/{id}', 'Comercializacion\\NotadevController@create')->name('notadevolucion.create');

Route::get('comercializacion/notadevolucion/reporte/{id}', 'Comercializacion\\NotadevController@reporte')->name('notadevolucion.reporte');
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/************************** END CAJA ***************************/
/********** COMPRA DE TELA TEÑIDA ***********/
Route::resource('comercializacion/compraTT','Comercializacion\\CompraTTController');
Route::resource('comercializacion/pagoTT','Comercializacion\\PagoTTController');
Route::get('comercializacion/pagoTT/create/{id}', 'Comercializacion\\PagoTTController@create')->name('pagoTT.create');
/************* END DE COMPRA DE TELA TEÑIDA******/

Route::resource('comercializacion/credito','Comercializacion\\CreditoController');
Route::resource('comercializacion/modulos','Comercializacion\\ModulosControllers');
//Route::get('abono/abonos/create/{compra_id}', 'Abono\\AbonosController@create')->name('abono.compras.create');

//Route::post('comercializacion/notaingreso', 'Comercializacion\\NotaIngresoController@store');
//Reporteria Comercial ----------------
Route::get('comercializacion/reporteria/stockcolores', 'Comercializacion\\ReporteComercialController@stockcolores')->name('reporteria.stockcolores');
Route::get('comercializacion/reporteria/ventaproducto', 'Comercializacion\\ReporteComercialController@ventaproducto')->name('reporteria.ventaproducto');
Route::get('comercializacion/reporteria/resumenventames', 'Comercializacion\\ReporteComercialController@resumenventames')->name('reporteria.resumenventames');
Route::get('comercializacion/reporteria/resumentienda', 'Comercializacion\\ReporteComercialController@resumentienda')->name('reporteria.resumentienda');
Route::get('comercializacion/reporteria/detvenprod', 'Comercializacion\\ReporteComercialController@detvenprod')->name('reporteria.detvenprod');

Route::get('comercializacion/reporteria/resventaporproducto', 'Comercializacion\\ReporteComercialController@resventaporproducto')->name('reporteria.resventaporproducto');
Route::get('comercializacion/reporteria/cuentaspcobrar', 'Comercializacion\\ReporteComercialController@cuentaspcobrar')->name('reporteria.cuentaspcobrar');
//------------------------------------

});

//Reporteria Comercial ----------------
Route::get('comercializacion/reporteria/stockcolores_create', 'Comercializacion\\ReporteComercialController@stockcolores_create')->name('reporteria.stockcolores_create');
Route::get('comercializacion/reporteria/ventaproducto_create', 'Comercializacion\\ReporteComercialController@ventaproducto_create')->name('reporteria.ventaproducto_create');
Route::get('comercializacion/reporteria/resumenventames_create', 'Comercializacion\\ReporteComercialController@resumenventames_create')->name('reporteria.resumenventames_create');
Route::get('comercializacion/reporteria/resumentienda_create', 'Comercializacion\\ReporteComercialController@resumentienda_create')->name('reporteria.resumentienda_create');
Route::get('comercializacion/reporteria/detvenprod_create', 'Comercializacion\\ReporteComercialController@detvenprod_create')->name('reporteria.detvenprod_create');

Route::get('comercializacion/reporteria/resventaporproducto_create', 'Comercializacion\\ReporteComercialController@resventaporproducto_create')->name('reporteria.resventaporproducto_create');
Route::get('comercializacion/reporteria/cuentaspcobrar_create', 'Comercializacion\\ReporteComercialController@cuentaspcobrar_create')->name('reporteria.cuentaspcobrar_create');
//ajax --------------------------------
/*Route::get('comercializacion/reporteria/productoalm/{id}', 'Comercializacion\\ReporteComercialController@productoalm')->name('reporteria.productoalm');*/
//------------------------------------


Route::get('comercializacion/credito/reporte/{id}', 'Comercializacion\\CreditoController@reporte')->name('credito.reporte');
Route::get('comercializacion/notaingreso/veri_partida/{id}/{par}', 'Comercializacion\\NotaIngresoController@veri_partida')->name('notaingreso.veri_partida');
Route::get('comercializacion/notaingresoatipico/productoalm/{id}', 'Comercializacion\\NotaIngresoAController@productoalm')->name('notaingresoatipico.productoalm');
Route::get('comercializacion/transferencia/veri_codbarra/{id}/{alm1}', 'Comercializacion\\TransferenciaController@veri_codbarra')->name('transferencia.veri_codbarra');

Route::get('comercializacion/transferencia/productoalm/{id}/{id2}', 'Comercializacion\\TransferenciaController@productoalm')->name('transferencia.productoalm');
Route::get('comercializacion/transferencia/almacenalm/{id}', 'Comercializacion\\TransferenciaController@almacenalm')->name('transferencia.almacenalm');


Route::get('comercializacion/notaingresoatipico/colorAlm/{id}/{col}', 'Comercializacion\\NotaIngresoAController@colorAlm')->name('notaingresoatipico.colorAlm');

Route::get('comercializacion/transferencia/almacenProStock/{id}', 'Comercializacion\\TransferenciaController@almacenProStock')->name('transferencia.almacenProStock');


Route::get('venta/buscli/{id}', 'Comercializacion\\VFacturaController@buscli')->name('venta.buscli');
Route::get('venta/b/recusuarios', 'Comercializacion\\VFacturaController@recusuarios')->name('venta.recusuarios');
Route::get('venta/b/bbancos', 'Comercializacion\\VFacturaController@bbancos')->name('venta.bbancos');

Route::get('venta/prostock/{id}/{cli}', 'Comercializacion\\VFacturaController@prostock')->name('venta.prostock');


Route::get('venta/productoalm/{id}', 'Comercializacion\\VFacturaController@productoalm')->name('venta.productoalm');

Route::get('venta/combo_lotes/{id}', 'Comercializacion\\VFacturaController@combo_lotes')->name('venta.combo_lotes');

Route::get('venta/promedida/{id}', 'Comercializacion\\VFacturaController@promedida')->name('venta.promedida');

Route::get('venta/ofermedida/{id}', 'Comercializacion\\VFacturaController@ofermedida')->name('venta.ofermedida');
Route::get('venta/reporte/{id}', 'Comercializacion\\VFacturaController@reporte')->name('venta.reporte');

Route::get('venta/reportesologuia/{id}', 'Comercializacion\\VFacturaController@reportesologuia')->name('venta.reportesologuia');
Route::get('comercializacion/inventario/productoalm/{id}', 'Comercializacion\\InventarioController@productoalm')->name('inventario.productoalm');
Route::get('comercializacion/inventario/autocomplete_producto/{busca}/{id}', 'Comercializacion\\InventarioController@autocomplete_producto')->name('inventario.autocomplete_producto');
Route::get('comercializacion/inventario/veri_codbarra/{id}', 'Comercializacion\\InventarioController@veri_codbarra')->name('inventario.veri_codbarra');

Route::get('comercializacion/caja/tiendaare/{id}', 'Comercializacion\\CajaController@tiendaare')->name('caja.tiendaare');
Route::get('comercializacion/caja/tiendaare2/{id}', 'Comercializacion\\CajaController@tiendaare2')->name('caja.tiendaare2');
Route::get('comercializacion/caja/cajavendedor/{id}', 'Comercializacion\\CajaController@cajavendedor')->name('caja.cajavendedor');
Route::get('comercializacion/notadevolucion/b/recusuarios', 'Comercializacion\\NotadevController@recusuarios')->name('notadevolucion.recusuarios');
Route::get('comercializacion/notadevolucion/b/bbancos', 'Comercializacion\\NotadevController@bbancos')->name('notadevolucion.bbancos');
Route::get('comercializacion/notadevolucion/prostock/{id}/{cli}', 'Comercializacion\\NotadevController@prostock')->name('notadevolucion.prostock');
Route::get('comercializacion/notadevolucion/productoalm/{id}', 'Comercializacion\\NotadevController@productoalm')->name('notadevolucion.productoalm');
Route::get('comercializacion/notadevolucion/promedida/{id}', 'Comercializacion\\NotadevController@promedida')->name('notadevolucion.promedida');
Route::get('comercializacion/notadevolucion/ofermedida/{id}', 'Comercializacion\\NotadevController@ofermedida')->name('notadevolucion.ofermedida');
Route::get('comercializacion/notadevolucion/veri_codbarra/{id}/{alm1}', 'Comercializacion\\NotadevController@veri_codbarra')->name('notadevolucion.veri_codbarra');

Route::get('comercializacion/notadevolucion/combo_lotes/{id}', 'Comercializacion\\NotadevController@combo_lotes')->name('notadevolucion.combo_lotes');

Route::get('venta/veri_codbarra/{id}/{alm1}', 'Comercializacion\\VFacturaController@veri_codbarra')->name('venta.veri_codbarra');

Route::get('venta/autocomplete/{id}', 'Comercializacion\\VFacturaController@autocomplete')->name('venta.autocomplete');

Route::get('venta/autoTipoDoc/{id}', 'Comercializacion\\VFacturaController@autoTipoDoc')->name('venta.autoTipoDoc');
Route::get('venta/autofORpaG/{id}', 'Comercializacion\\VFacturaController@autofORpaG')->name('venta.autofORpaG');

Route::get('venta/a/autocomplete_pru', 'Comercializacion\\VFacturaController@autocomplete_pru')->name('venta.autocomplete_pru');
Route::get('venta/autocomplete_producto/{nAlmCod}/{busca}', 'Comercializacion\\VFacturaController@autocomplete_producto')->name('venta.autocomplete_producto');

/*Route::get('venta/autocomplete_producto_TC/{busca}', 'Comercializacion\\VFacturaController@autocomplete_producto_TC')->name('venta.autocomplete_producto_TC');*/

Route::get('comercializacion/areaalmacen/detallearea/{id}', 'Comercializacion\\Areaalmacen@detallearea')->name('areaalmacen.detallearea');
Route::get('comercializacion/almacen/productocol/{id}/{pro}', 'Comercializacion\\AlmacenController@productocol')->name('almacen.productocol');
Route::get('comercializacion/notaingresoatipico/productoCol/{id}', 'Comercializacion\\NotaIngresoAController@productoCol')->name('notaingresoatipico.productoCol');
Route::get('comercializacion/almacen/productocolSi/{id}/{pro}', 'Comercializacion\\AlmacenController@productocolSi')->name('almacen.productocolSi');

Route::post('venta/regcliente/{codigo}/{nombre}/{direc}/{obs}', 'Comercializacion\\VFacturaController@regcliente')->name('venta.regcliente');