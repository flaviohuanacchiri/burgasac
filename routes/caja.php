<?php
Route::resource('caja/pago', 'Caja\PagoController');
Route::post('caja/pagar', 'Caja\PagoController@pagar');
Route::get('caja/pagodetalle', 'Caja\PagoController@listarDetalle');
